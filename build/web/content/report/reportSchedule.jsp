<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.MailSchedule" text="Mail Scheduler"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Master" text="Master"/></a></li>
            <li class=""><spring:message code="hrms.label.MailScheduler" text="Mail Scheduler"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <form name="rep"  method="POST">
                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover" style="width:100%">
                        <tr>
                            <td>
                                Report Type 
                            </td>
                            <td>
                                <select id="reportType" name="reportType" class="form-control" style="width:220px;height:40px;">
                                    <option value="DSR">DSR</option>
                                    <option value="NOD">NOD</option>
                                </select>
                            </td>

                            <td>
                                Customer
                            </td>
                            <td>
                                <select class="form-control" name="customerId" id="customerId" style="width:220px;height:40px;">
                                    <option value='0'>--Select--</option>
                                    <c:if test="${customerList != null}">
                                        <c:forEach items="${customerList}" var="cust">
                                            <option value='<c:out value="${cust.custId}"/>'><c:out value="${cust.custName}"/></option>                                            
                                        </c:forEach>
                                    </c:if>

                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                To-Mail Id
                            </td>
                            <td>
                                <input style="width:220px;height:40px;" type="text" id="mailIdTo" name="mailIdTo" value="">
                            </td>

                            <td>
                                Cc-Mail Id
                            </td>
                            <td>
                                <input style="width:220px;height:40px;" type="text" id="mailIdCc" name="mailIdCc" value="">
                            </td>
                        </tr>

                        <tr>
                            <td>Active</td>
                            <td>
                                <select class="form-control" style="width:220px;height:40px;" id="ActiveInd" name="active_ind">
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>

                            </td>
                            <td>Fleet Center</td>
                            <td>
                                 <select class="form-control" style="width:220px;height:40px;" id="fleetCenterId" name="fleetCenterId">
                                       
                                            <option value="0">--Select--</option>
                                            <c:forEach items="${companyList}" var="companyList">
                                                <option value='<c:out value="${companyList.cmpId}"/>'><c:out value="${companyList.name}"/></option>
                                            </c:forEach>
                                    </select>
                                 <input type="hidden" style="width: 184px;" name="idNo" id="idNo"  value="" />     
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">

                                <input type="button"  class="btn btn-info" value="Save" id="saveReport" name="Submit" onClick="myFunction()">
                            </td>
                        </tr>
                    </table>
                    <script>
                        function myFunction() {

                            var custId = document.getElementById("customerId").value;
                            var mailIdTo = document.getElementById("mailIdTo").value;

                            if (custId == 0) {
                                alert("Please select the Customer");
                                document.getElementById("customerId").focus();
                                return false;
                            } else if (mailIdTo == "") {
                                alert("Please enter the To mailId");
                                document.getElementById("mailIdTo").focus();
                                return false;
                            } else {
                                document.rep.action = "/throttle/saveMailAlert.do";
                                document.rep.submit();
                            }
                        }
                    </script>

                    <br>
                    <br>
                    <table class="table table-info mb30 table-hover" id="table" >
                        <thead><tr>
                                <th style="text-align:center">Sno</th>
                                <th style="text-align:center">Fleet Center</th>
                                <th style="text-align:center">Report Type</th>
                                <th style="text-align:center">Customer</th>
                                <th style="text-align:center">To-MailId</th>
                                <th style="text-align:center">Cc-MailId</th>
                                <th style="text-align:center">Active</th>
                                <th style="text-align:center">Update</th>
                            </tr>
                        </thead>
                        <%int index = 1;%>
                        <c:if test = "${reportList != null}">
                            <c:forEach items="${reportList}" var="rep">
                                <tr align="center">
                                    <td><%=index%></td>
                                    <td><c:out value="${rep.companyName}"/></td>
                                    <td><c:out value="${rep.reporttype}" /></td>
                                    <td width="50%"><c:out value="${rep.custName}" /></td>
                                    <td width="20%"><c:out value="${rep.mailIdTo}" /></td>
                                    <td width="20%"><c:out value="${rep.mailIdCc}" /></td>
                                    <td><c:out value="${rep.activeInd}" /></td>
                                    <td> <input type="radio" name="reportId" onclick="setValues('<c:out value="${rep.reportId}" />', '<c:out value="${rep.reporttype}" />', '<c:out value="${rep.mailIdTo}" />', '<c:out value="${rep.mailIdCc}" />', '<c:out value="${rep.activeInd}" />', '<c:out value="${rep.customerId}" />', '<c:out value="${rep.companyId}" />');" />
                                    </td>
                                </tr>
                                <%index++;%>
                                </c:forEach>
                       </c:if>    
                    </table>
                    <script>
                        function setValues(reportId, reporttype, mailIdTo, mailIdCc, activeInd, custId, fleetCenterId) {
                            //alert(custId);
                            document.getElementById("fleetCenterId").value = fleetCenterId;
                            document.getElementById("customerId").value = custId;
                            document.getElementById("idNo").value = reportId;
                            document.getElementById("reportType").value = reporttype;
                            document.getElementById("fleetCenterId").value = fleetCenterId;
                            document.getElementById("mailIdTo").value = mailIdTo;
                            document.getElementById("mailIdCc").value = mailIdCc;
                            document.getElementById("activeInd").value = activeInd;
                        }
                    </script>
                        
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5"  selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>

                    <div id="myMap" style="width: 1000px; height: 400px; margin-top:20px;"></div>            
                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>