
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="java.util.*" %>
         <SCRIPT LANGUAGE="Javascript" SRC="/throttle/js/FusionCharts.js"></SCRIPT>
    </head>
    
    <script language="javascript">

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}


       function submitPage(){
           var chek=validation();
           if(chek=='true'){
                document.salesBillTrend.action='/throttle/vendorTrendGraph.do';
                document.salesBillTrend.submit();
           }
       }
       function validation(){
               if(textValidation(document.salesBillTrend.fromDate,'From Date')){
                   document.salesBillTrend.fromDate.focus();
                   return 'false';
               }
               else if(textValidation(document.salesBillTrend.toDate,'To Date')){
                   document.salesBillTrend.toDate.focus();
                   return 'false';
               }
               return 'true';
          }
       function setValues(){
           var a='<%=request.getAttribute("fromDate")%>';
           var b='<%=request.getAttribute("vendorName")%>';
           var c='<%=request.getAttribute("itemName")%>';
           var d='<%=request.getAttribute("itemCode")%>';
           if(a!='null'){
               
                document.salesBillTrend.fromDate.value='<%=request.getAttribute("fromDate")%>';
                document.salesBillTrend.toDate.value='<%=request.getAttribute("toDate")%>';
           }
           if(b!='null'){
                document.salesBillTrend.vendorName.value='<%=request.getAttribute("vendorName")%>';
               }
           if(c!='null'){
                document.salesBillTrend.itemName.value='<%=request.getAttribute("itemName")%>';
               }
           if(d!='null'){
                document.salesBillTrend.itemCode.value='<%=request.getAttribute("itemCode")%>';
               }
       }
       
       
        function getItemNames(){
        var oTextbox = new AutoSuggestControl(document.getElementById("itemName"),new ListSuggestions("itemName","/throttle/handleItemSuggestions.do?"));
        
        //getVehicleDetails(document.getElementById("regno"));
        
        
            }
            
            function getVendorName()
            {
                var oTextbox1 = new AutoSuggestControl(document.getElementById("vendorName"),new ListSuggestions("vendorName","/throttle/vendorName.do?"));
                }
    </script>
    <body onload="setValues();">
        <form  name="salesBillTrend" method="post">
            <%@ include file="/content/common/path.jsp" %>                 
            <%@ include file="/content/common/message.jsp" %>
            <table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
<tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
</h2></td>
<td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
</tr>
<tr id="exp_table" >
<td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
    <div class="tabs" align="left" style="width:850;">
<ul class="tabNavigation">
        <li style="background:#76b3f1">Sales Bill Trend</li>
</ul>
<div id="first">
<table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
<tr>
    <td><font color="red">*</font>From Date</td>
    <td><input name="fromDate" class="textbox" type="text" value="" size="20">
    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.salesBillTrend.fromDate,'dd-mm-yyyy',this)"/></td>
   <td height="25"><font color="red">*</font>To Date</td>
    <td><input name="toDate" class="textbox" type="text" value="" size="20">
    <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.salesBillTrend.toDate,'dd-mm-yyyy',this)"/></td>
<td colspan="1">Vendor Name</td>
   <td colspan="3">
       <input id="vendorName" size="24"  name="vendorName"  type="text" class="textbox" value="" onkeypress="getVendorName();">
   </td>
</tr>
<tr>
   <td>Item Code</td>
   <td><input type="text" size="24" class="textbox" name="itemCode"></td>
   <td>Item Name</td>
   <td><input id="itemName" size="24"  name="itemName" type="text" class="textbox" value="" onkeypress="getItemNames();"></td>
   <td colspan="2"><input type="button" class="button" value="Search" onclick="submitPage();" /></td>
</tr>

</table>
</div></div>
</td>
</tr>
</table>
            
            <%
            
            ArrayList salesTrendGraph= (ArrayList) request.getAttribute("salesTrendGraph");
            if(request.getAttribute("salesTrendGraph")!=null)
                {
            if(salesTrendGraph.size()!=0 ){
                System.out.println(salesTrendGraph.size());
            int size = salesTrendGraph.size(); 
            String[][] arrData = new String[size][2];
            int i = 0;   
            Iterator itr = salesTrendGraph.iterator();
            ReportTO repTO = new ReportTO();
            while(itr.hasNext()){
                repTO = (ReportTO) itr.next();
                arrData[i][0] = repTO.getMonthName();
                arrData[i][1] = String.valueOf(repTO.getAmount());
                i++;
            }
            String  strXML = "<graph caption='Vendor Sales Summary'  xAxisName='Month' yAxisMinValue='0' yAxisName='Sales' decimalPrecision='0' formatNumberScale='0' numberPrefix='Rs' showNames='1' showValues='0'  showAlternateHGridColor='1' AlternateHGridColor='ff5904' divLineColor='ff5904' divLineAlpha='20' alternateHGridAlpha='5' >";            
            
            String set="";
            for(int j=0;j<arrData.length;j++){
                set+="<set name='" + arrData[j][0] + "' value='" + arrData[j][1] + "' hoverText='" + arrData[j][0] + "'/>";
            }
                  
            strXML += set + "</graph>";      
            System.out.println("----**************************************----"+strXML);
       
            %>
            <p>
            <table align="center" style="margin-left:10px;" >
            <tr>
                <td >
                 <jsp:include page="FusionChartsRenderer.jsp" flush="true"> 
                <jsp:param name="chartSWF" value="/throttle/swf/FCF_Line.swf" /> 
                <jsp:param name="strURL" value="" /> 
                <jsp:param name="strXML" value="<%=strXML %>" /> 
                <jsp:param name="chartId" value="productSales" /> 
                <jsp:param name="chartWidth" value="800" /> 
                <jsp:param name="chartHeight" value="450" />
                <jsp:param name="debugMode" value="false" /> 	
                <jsp:param name="registerWithJS" value="false" /> 
        </jsp:include>
                </td>
            </tr>
            
        </table>
          
            
    </p>


<%}}%>
        </form>
    </body>
</html>
