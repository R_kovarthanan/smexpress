<%-- 
    Document   : displayTripSettlement
    Created on : Jun 11, 2013, 11:52:27 PM
    Author     : Entitle
--%>

<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->

        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
         <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="ets.domain.security.business.SecurityTO" %>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script src="/throttle/js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script src="/throttle/js/TableSort.js" language="javascript" type="text/javascript"></script>
        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {

                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>
        <script>
            function submitPage(value){
               if(value == 'Fetch Data'){
               var param = 'No';

               } else{
               var param = 'ExportExcel';
               }
               //alert('param is::;'+param);
                if(textValidation(document.tripsettlementwise.fromDate,'From Date')){
                    return;
                }else if(textValidation(document.tripsettlementwise.toDate,'To Date')){
                    return;
                }
                //alert(check);
                document.tripsettlementwise.action="/throttle/dieselReportDetails.do?param="+param;
                document.tripsettlementwise.submit();

            }
            function setFocus(){
                var tripId='<%=request.getAttribute("tripId")%>';
                var vehicleNo='<%=request.getAttribute("vehicleNo")%>';
                var sdate='<%=request.getAttribute("fromDate")%>';
                var edate='<%=request.getAttribute("toDate")%>';
                if(tripId!='null'){
                document.tripsettlementwise.tripId.value=tripId;
                }
                if(vehicleNo!='null'){
                document.tripsettlementwise.vehicleNo.value=vehicleNo;
                }

                if(sdate!='null' && edate!='null'){
                    document.tripsettlementwise.fromDate.value=sdate;
                    document.tripsettlementwise.toDate.value=edate;
                }
            }
        </script>
    </head>
    <body onload="setFocus()" >
        <form name="tripsettlementwise" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
        <%@ include file="/content/common/message.jsp" %>
         <br/>
            <table cellpadding="0" cellspacing="2" align="center" border="0" width="700" id="report" bgcolor="#97caff" style="margin-top:0px;">
              <tr>
                    <td><b>Diesel Report</b></td>
                    <td align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;">Close</span>&nbsp;</td>
                </tr>
                <tr id="exp_table"  style="display: block;">
                    <td colspan="2" style="padding:15px;" align="right">
                        <div class="tabs" align="center" style="width:800px">
                            <table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                <tr>
                                    <td  height="30">Trip Id:   </td>
                                    <td  height="30"><input type="text" name="tripId" id="tripId" class="textbox" size="20" onfocus="getTripIds();" autocomplete="off" onPaste="return false" onDrag="return false" onDrop="return false"></td>
                                    <td  height="30">Vehicle No:   </td>
                                    <td  height="30"><input type="text" name="vehicleNo" class="textbox" id="vehicleNo" size="20" onfocus="getVehicleNumbers();" autocomplete="off" onPaste="return false" onDrag="return false" onDrop="return false"></td>
                                </tr>
                                <tr>
                                    <td  height="30"><font color="red">*</font>From Date :</td>
                                    <td  height="30"><input type="text" name="fromDate" id="fromDate" class="datepicker" value="" />  </td>
                                    <td  height="30"><font color="red">*</font>To Date : </td>
                                    <td  height="30"><input type="text" name="toDate" id="toDate" class="datepicker" value="" /></td>
                                </tr>

                                 <tr>
                                     <td  height="30" colspan="5" align="center" >
                                            <input type="button"  value="Fetch Data"  class="button" name="Fetch Data" onClick="submitPage(this.value)">&nbsp;&nbsp;&nbsp;
                                            <input type="button"  value="Export to Excel"  class="button" name="excelExport" onClick="submitPage(this.value)">
                                    </td>

                                </tr>

                            </table>

                        </div>
                    </td>
                </tr>
         </table>
         <%
                            DecimalFormat df = new DecimalFormat("0.00##");
                            String tripId = "",tripDate = "",destination = "",vehicleNumber = "",regNo = "",totalExpenses = "";
                            String departureName = "",liter = "",totalTonnage = "",distance = "",totalFreightAmount = "",driverName = "";
                            double totalFreight = 0,subTotalFreight = 0,totalSettlement = 0;
                            double grandTotalTonnage = 0;
                            double grandTotalDistance = 0;
                            double grandTotalFuel = 0;
                            String className = "text2",settlementAmount = "";
                            int index = 0;
                            int sno = 0;
%> <br>
                <table  border="0" class="border" align="center" width="950" cellpadding="0" style="margin-left:30px" cellspacing="0" id="bg">
                    <c:if test = "${dieselReportList != null}" >
                        <%
                        ArrayList dieselReportList = (ArrayList) request.getAttribute("dieselReportList");
                        Iterator detailsItr = dieselReportList.iterator();
                        ReportTO repTO = null;
                         if (dieselReportList.size() != 0) {
                            %>
                            <tr>
                                <td class="contentsub" height="30">S.No</td>
                                <td class="contentsub" height="30">Trip Id</td>
                                <td class="contentsub" height="30">Vehicle No</td>
                                <td class="contentsub" height="30">Trip Date</td>
                                <td class="contentsub" height="30">Driver Name</td>
                                <td class="contentsub" height="30">Route Name</td>
                                <td class="contentsub" height="30">Tonnage</td>
                                <td class="contentsub" height="30">Fuel Liters</td>
                                <td class="contentsub" height="30">KM</td>
                                </tr>
                    <%
                     while (detailsItr.hasNext()) {
                         index++;
                         sno++;
                           if(index%2 == 0){
                            className = "text2";

                            }else {
                            className = "text1";
                        }
                         repTO = new ReportTO();
                         repTO = (ReportTO) detailsItr.next();
                         tripId  = repTO.getTripId();
                         driverName  = repTO.getDriverName();
                         regNo  = repTO.getRegno();
                        if(tripId == null){
                            tripId = "";
                            }
                        tripDate  = repTO.getTripDate();
                        if(tripDate == null){
                            tripDate = "";
                            }
                            destination  = repTO.getDestination();
                        if(destination == null){
                            destination = "";
                            }

                            totalTonnage  = repTO.getTotalTonnage();
                            if(totalTonnage == null){
                                totalTonnage = "0";
                                }
                            grandTotalTonnage += Double.parseDouble(totalTonnage);

                            liter  = repTO.getTripTotalLitres();
                            if(liter == null){
                                liter = "0";
                                }
                            grandTotalFuel += Double.parseDouble(liter);

                            distance  = repTO.getDistance();
                            if(distance == null){
                                distance = "0";
                                }
                            grandTotalDistance += Double.parseDouble(distance);
                            
                        %>
                        <tr>
                          <td class="<%=className%>"  height="30"><%=sno%></td>
                          <td class="<%=className%>"  height="30"><%=tripId%></td>
                          <td class="<%=className%>"  height="30"><%=regNo%></td>
                          <td class="<%=className%>"  height="30"><%=tripDate%></td>
                          <td class="<%=className%>"  height="30"><%=driverName%></td>
                          <td class="<%=className%>"  height="30"><%=destination%></td>
                          <td class="<%=className%>"  align="right"  height="30"><%=totalTonnage%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="<%=className%>"  align="right" height="30"><%=liter%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="<%=className%>"  align="right"  height="30"><%=distance%>&nbsp;&nbsp;&nbsp;&nbsp;</td>

                        </tr>
                    
                        <%
                     }

                             }
%>
                    
                    <tr>
                          <td colspan="6" class="<%=className%>"  align="right"  height="30">Total</td>
                          <td class="<%=className%>"  align="right"  height="30"><%=df.format(grandTotalTonnage)%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="<%=className%>"  align="right"  height="30"><%=df.format(grandTotalFuel)%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="<%=className%>"  align="right" height="30"><%=df.format(grandTotalDistance)%>&nbsp;&nbsp;&nbsp;&nbsp;</td>

                        </tr>
</c:if>
                </table>
        </form>
    </body>
</html>
<script type="text/javaScript">
        function getTripIds(){
            var oTextbox = new AutoSuggestControl(document.getElementById("tripId"),new ListSuggestions("tripId","/throttle/tripIdSuggestions.do?"));
        }
        function getVehicleNumbers(){
            var oTextbox = new AutoSuggestControl(document.getElementById("vehicleNo"),new ListSuggestions("vehicleNo","/throttle/vehicleNumberSuggestions.do?"));
        //alert('oTextbox is::'+oTextbox);
        }

    </script>