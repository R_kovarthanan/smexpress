<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>
    </head>

    <script>
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

        var httpReq;
        var temp = "";
        function ajaxData()
        {
            if(document.serviceCost.mfrId.value !='0') {
                var url = "/throttle/getModels1.do?mfrId="+document.serviceCost.mfrId.value;
                if (window.ActiveXObject)
                {
                    httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest)
                {
                    httpReq = new XMLHttpRequest();
                }
                httpReq.open("GET", url, true);
                httpReq.onreadystatechange = function() { processAjax(); } ;
                httpReq.send(null);
            }
        }
        function processAjax()
        {
            if (httpReq.readyState == 4)
            {
                if(httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    setOptions(temp,document.serviceCost.modelId);
                    if('<%=request.getAttribute("modelId")%>'!='null'){
                        document.serviceCost.modelId.value='<%=request.getAttribute("modelId")%>';
                    }

                }
                else
                {
                    alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                }
            }
        }
        function submitPag(val){

        }



        function searchSubmit(){

            if(document.serviceCost.fromDate.value==''){
                alert("Please Enter From Date");
                return;
            }else if(document.serviceCost.toDate.value==''){
                alert("Please Enter to Date");
                return;
            }
            else if(document.serviceCost.reportType.value==0){
                alert("Select Desired Date Wise Search");
                return;
            }
            document.serviceCost.action = '/throttle/vehicleTaxWiseServiceCostReport.do'
            document.serviceCost.submit();

        }

        function getVehicleNos(){
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
        }


        function rateDetails(indx){
            var regNo = document.getElementsByName("vehicleNo");
            var fromDate=document.getElementsByName("fromDate");
            var toDate=document.getElementsByName("toDate");
            var url = '/throttle/serviceCostBillDetails.do?regNo='+regNo[indx].value+'&fromDate='+document.serviceCost.fromDate.value+'&toDate='+document.serviceCost.toDate.value;
            window.open(url , 'PopupPage', 'height=450,width=650,scrollbars=yes,resizable=yes');
        }
        function setValues(){
            document.serviceCost.regNo.focus();
            if('<%=request.getAttribute("fromDate")%>'!='null'){
                document.serviceCost.fromDate.value ='<%=request.getAttribute("fromDate")%>';
                document.serviceCost.toDate.value ='<%=request.getAttribute("toDate")%>';
            }if('<%=request.getAttribute("regNo")%>' !='null'){
                document.serviceCost.regNo.value='<%=request.getAttribute("regNo")%>';
            }if('<%=request.getAttribute("custId")%>' != 'null'){
                document.serviceCost.custId.value='<%=request.getAttribute("custId")%>';
            }
            if('<%= request.getAttribute("reportType")%>' != 'null'){
                document.serviceCost.reportType.value='<%= request.getAttribute("reportType")%>';
            }


        }

        function printPage()
        {
            var DocumentContainer = document.getElementById("printPage");
            var WindowObject = window.open('', "TrackHistoryData",
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            //WindowObject.close();
        }


        function submitForExcel(){

            if(document.serviceCost.fromDate.value==''){
                alert("Please Enter From Date");
                return;
            }else if(document.serviceCost.toDate.value==''){
                alert("Please Enter to Date");
                return;
            }
            else if(document.serviceCost.reportType.value==0){
                alert("Select Desired Date Wise Search");
                return;
            }
            document.serviceCost.action = '/throttle/vehicleTaxWiseServiceCostExcelReport.do'
            document.serviceCost.submit();

        }
    </script>

    <body onload="getVehicleNos();setValues();">

        <form name="serviceCost"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">TaxWise Vehicle Service Cost</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr height="30">
            <td align="left">Customer</td>            
            <td align="left"><select class="textbox" name="custId"  style="width:125px;">
                            <option value="">---Select---</option>
                            <option value="0">Others</option>
                            <c:if test = "${customerList != null}" >
                                <c:forEach items="${customerList}" var="cust">
                                    <option value='<c:out value="${cust.custId}" />'><c:out value="${cust.custName}" /></option>
                                </c:forEach >
                            </c:if>
                        </select></td>
            <td align="left">Vehicle</td>
            <td align="left"><input name="regNo" id="regno" type="text" class="textbox"  size="20"></td>
            <td align="left">Type</td>
            <td align="left"><select name="reportType" class="textbox">
                            <option value="0">--Select--</option>
                            <option value="1">Job Card Date</option>
                            <option value="2">Bill Date</option>
                        </select></td>
    </tr>
    <tr>
        <td height="30">From Date </td>
        <td height="30"><input type="text" name="fromDate" class="textbox" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.serviceCost.fromDate,'dd-mm-yyyy',this)"/> </td>
        <td height="30">To Date </td>
        <td height="30"><input name="toDate" type="text" class="textbox" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.serviceCost.toDate,'dd-mm-yyyy',this)"/></td>
        <td colspan="2"><input type="button" class="button" readonly name="search" value="search" onClick="searchSubmit();" ></td>
    </tr>
    </table>
    </div></div>
    </td>
    </tr>
    </table>

            <c:set var="total" value="0"/>
            <c:set var="spareAmount" value="0" />
            <c:set var="laborAmount" value="0" />

            <c:set var="totalAmount"  value="0"/>
            <% int index = 0;
                        String classText = "";
                        int oddEven = 0;
            %>
            <c:if test = "${serviceCostList != null}" >

                <center>
                    <input type="button" class="button" name="print" value="print" onClick="printPage();" > &nbsp;&nbsp;&nbsp;
                    <input type="button" class="button" name="print" value="Export To Excel" onClick="submitForExcel();" >
                </center>
                <br>
                <div id="printPage" >
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="1294" id="bg" class="border">

                        <tr >
                            <td width="24"  class="contentsub">Sno</td>
                            <td width="101"  height="30" class="contentsub">Customer Name</td>
                            <td width="63"  height="30" class="contentsub">Veh No</td>
                            <td width="57"  height="30" class="contentsub">JobCard</td>


                            <c:forEach items="${VatValues}" var="ser">


                                <td width="37"  height="30" class="contentsub">Vat@<c:out value="${ser.acd}"/></td>
                                <td width="31"  height="30" class="contentsub">Nett </td>
                            </c:forEach>
                            <td width="63"  height="30" class="contentsub">Contr.Tax</td>
                            <td width="81"  height="30" class="contentsub"> Contr. Amnt </td>
                            <td width="78"  height="30" class="contentsub">Spare Amnt </td>
                            <td width="86"  height="30" class="contentsub"> Labour Amnt </td>
                            <td width="120"  height="30" class="contentsub"> Service Tax (10%) </td>
                            <td width="98"  height="30" class="contentsub"> Edu Cess (2%) </td>
                            <td width="122"  height="30" class="contentsub"> Hr. Edu Cess (1%) </td>

                            <td width="31"  height="30" class="contentsub"> Total</td>

                        </tr>

                        <c:set var="totJcd" value="0" />

                        <c:forEach items="${serviceCostList}" var="service">
                            <%

                                        oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>


                            <tr>
                                <td class="<%=classText%>" height="30"> <%= index + 1%> </td>
                                <td class="<%=classText%>" height="30"><c:out value="${service.custName}"/></td>
                                <td class="<%=classText%>" height="30">
                                    <input type="hidden" name="vehicleNo" value='<c:out value="${service.regNo}"/>' />
                                    <c:out value="${service.regNo}"/>
                                </td>
                                <td class="<%=classText%>" height="30"><c:out value="${service.totJc}"/>
                                <c:set var="totJcd" value="${totJcd + service.totJc}" />
                                </td>
                                <c:forEach items="${service.vatValues}"  var="vat">
                                    <td class="<%=classText%>" height="30" >
                                        <fmt:formatNumber value="${vat.taxAmount}" pattern="##.00"/>
                                    </td>
                                    <td class="<%=classText%>" height="30"  ><c:out value="${vat.amount}"/></td>

                                </c:forEach>


                                <td class="<%=classText%>" height="30"  ><c:out value="${service.spareAmount}"/></td>
                                <td class="<%=classText%>" height="30"  ><c:out value="${service.laborAmount}"/></td>


                                    <c:if test = "${service.tax != '0.00'}" >
                                    <td class="<%=classText%>" height="30"  >
                                        <fmt:formatNumber value="${service.laborAmount * 10/100}" pattern="##.00"/>
                                    </td>
                                    <td class="<%=classText%>" height="30"  >
                                        <fmt:formatNumber value="${service.laborAmount * 0.2/100}" pattern="##.00"/>

                                    </td>
                                    <td class="<%=classText%>" height="30"  >
                                        <fmt:formatNumber value="${service.laborAmount * 0.1/100}" pattern="##.00"/>

                                    </td>
                                    </c:if>
                                    <c:if test = "${service.tax == '0.00'}" >
                                    <td class="<%=classText%>" height="30"  ><c:out value="${service.tax}"/>a</td>
                                    <td class="<%=classText%>" height="30"  ><c:out value="${service.tax}"/></td>
                                    <td class="<%=classText%>" height="30"  ><c:out value="${service.tax}"/></td>
                                    </c:if>



                                <td width="8" height="30" class="<%=classText%>" >
                                    <c:set var="total" value="${service.laborAmount+service.spareAmount + service.contractAmnt}"/>
                                    <fmt:formatNumber value="${total}" pattern="##.00"/>
                                <c:set var="totalAmount" value="${totalAmount + total}" />
                                    <c:set var="total" value="0"/>
                                </td>
                                <c:set var="spareAmount" value="${spareAmount + service.spareAmount}" />
                                <c:set var="laborAmount" value="${laborAmount + service.laborAmount}" />
                            </tr>


                            <%
                                        index++;
                            %>
                        </c:forEach>

                  </table>
                </div>

                 <br>
                  <table  align="center" border="0" cellpadding="0" cellspacing="0" width="500" id="bg" class="border">
                      <tr>
                      <td class="text1" height="30"> Total Jobcards </td>
                      <td class="text1" height="30"> <b><c:out value="${totJcd}"/> </b> </td>
                      </tr>
                      <tr>
                      <td class="text1" height="30"> Spare Amount </td>
                      <td class="text1" height="30"> <fmt:setLocale value="en_US" /><b>Rs: <fmt:formatNumber value="${spareAmount}" pattern="##.00"/></b> </td>
                      </tr>
                      <tr>
                      <td class="text1" height="30"> Labour Amount </td>
                      <td class="text1" height="30"> <fmt:setLocale value="en_US" /><b>Rs: <fmt:formatNumber value="${laborAmount}" pattern="##.00"/></b></td>
                      </tr>
                      <tr>
                      <td class="text1" height="30"> Total Amount </td>
                      <td class="text1" height="30"> <fmt:setLocale value="en_US" /><b>Rs: <fmt:formatNumber value="${totalAmount}" pattern="##.00"/></b></td>
                      </tr>

                  </table>
            </c:if>

        </form>

    </body>
</html>
