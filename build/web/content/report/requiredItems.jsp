
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.mrs.business.MrsTO" %> 
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
        
        <title>MRSList</title>
    </head>
    <script>                         
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

function submitPage(val){
       
        if(val=='GoTo'){
            var temp=document.mpr.GoTo.value;                 
            if(temp!='null'){
            document.mpr.pageNo.value=temp;
            }
        }        
        document.mpr.button.value=val;        
        document.mpr.action = '/throttle/reqItemsReport.do'
        document.mpr.submit();        
}

function setValues()
{
    if('<%= request.getAttribute("mfrCode") %>' !='null'){
        document.mpr.mfrCode.value = '<%= request.getAttribute("mfrCode") %>';
    }
    if('<%= request.getAttribute("paplCode") %>' !='null'){
        document.mpr.paplCode.value = '<%= request.getAttribute("paplCode") %>';
    }
    if('<%= request.getAttribute("categoryId") %>' !='null'){
        document.mpr.categoryId.value = '<%= request.getAttribute("categoryId") %>';
    }
    if('<%= request.getAttribute("searchAll") %>' !='null'){
        document.mpr.searchAll.value = '<%= request.getAttribute("searchAll") %>';
    }
}    





        </script>
        
        
        
        
    <body onLoad="setValues();" >        
        <form name="mpr" method="post">                    
            <%@ include file="/content/common/path.jsp" %>            
            <!-- pointer table -->
             <!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>    

            <%
                    int pageIndex = (Integer)request.getAttribute("pageNo");
                    int index1 = ((pageIndex-1)*10)+1 ;
            %>

<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Search Parts</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
    <tr>
        <td height="30">Item Code</td>
        <td height="30"><input name="mfrCode" type="text" class="textbox" value="" ></td>
        <td height="30">PAPL Code</td>
        <td height="30"><input name="paplCode" type="text" class="textbox" value=""></td>
        <td height="30">Category</td>
        <td height="30"><select class="textbox" name="categoryId" style="width:125px;" >
                <option value="0">---Select---</option>
                <c:if test = "${CategoryList != null}" >
                <c:forEach items="${CategoryList}" var="Dept">
                <option value='<c:out value="${Dept.categoryId}" />'><c:out value="${Dept.categoryName}" /></option>
                </c:forEach >
                </c:if>
                </select>
        </td>
        <td height="30">Search Type</td>
        <td height="30"><select class="textbox" name="searchAll" style="width:125px;" >
                <option value="">---Select---</option>
                <option value="Y">All</option>
                <option value="N">Required Items</option>
                </select></td>
        <td height="30" rowspan="2" valign="middle"><input type="button" class="button" name="Search" value="Search" onClick="submitPage(this.value)" > </td>
    </tr>

    </table>
    </div></div>
    </td>
    </tr>
    </table>
          
                <c:if test = "${requiredItemsList != null}" >   
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="776" id="bg" class="border">              
                <%
            String classText = "";
            int oddEven = 0;
            int index = 0;
                %>

             
                        <tr>
                            <td width="25" height="30" class="contenthead"><b>Sno</b></td>						
                            <td width="69" height="30" class="contenthead"><b>Mfr Code</b></td>
                            <td width="71" height="30" class="contenthead"><b>Papl Code</b></td>                       
                            <td width="144" height="30" class="contenthead"><b>Item Name</b></td>
                            <td width="31" height="30" class="contenthead"><b>Uom</b></td>
                            <td width="60" height="30" class="contenthead"><b>Reorder Level</b> </td>                                                                                                                                               
                            <td width="58" height="30" class="contenthead"><b>Stock Balance</b> </td>                                                                                                                                                                                                                                                                                                                
                            <td width="69" height="30" class="contenthead"><b>Po Raised Qty</b> </td>                                                                                                                                                                          
                        </tr>                
                    <c:forEach items="${requiredItemsList}" var="item"> 
                        <%

            oddEven = index1 % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr>
                            <td class="<%=classText %>" height="30"><%= index1 %></td>						
                            <td class="<%=classText %>" height="30"><c:out value="${item.mfrCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.paplCode}"/></td>                       
                            <td class="<%=classText %>" height="30"><c:out value="${item.itemName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.uomName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.roLevel}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.stockBalance}"/></td>                            
                            <td class="<%=classText %>" height="30"><c:out value="${item.poRaisedQty}"/> </td>
                           
                        </tr>
                        <%
            index++;
            index1++;
                        %>
                    </c:forEach>
                  
            </table>

            <br>           
                <input type="hidden" name="purchaseType" value="" >          
            <br>               
</c:if>     
     
<br>
 <%@ include file="/content/common/pagination.jsp"%>      
<br>

        </form>
    </body>

