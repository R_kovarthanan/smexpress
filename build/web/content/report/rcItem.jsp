
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>          
    </head>
    <script language="javascript">

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

        function rcDetails(){
            var chek=validation();     
      if(chek=='true'){
            document.rcItem.action='/throttle/rcItemList.do';
            document.rcItem.submit();
            
        }
        }
        function validation(){
            if(document.rcItem.companyId.value==0){
                alert("Please Select Service Point");
                document.rcItem.companyId.focus();
                return 'false';
            }           
            else  if(textValidation(document.rcItem.fromDate,'From Date')){
                return 'false';
            }            
            else  if(textValidation(document.rcItem.toDate,'TO Date')){
                return 'false';
            }            
            return 'true';
        }
   function rcHistory(indx){
       rcId=document.getElementsByName("rcNo");
       var url='/throttle/rcHistory.do?rcId='+rcId[indx].value;
       window.open(url , 'PopupPage', 'height=450,width=650,scrollbars=yes,resizable=yes');   
       }
        function getItemNames(){
            var oTextbox = new AutoSuggestControl(document.getElementById("itemName"),new ListSuggestions("itemName","/throttle/handleItemSuggestions.do?"));
            
        } 
  function setValues(){
        if('<%=request.getAttribute("companyId")%>'!='null'){
                document.rcItem.companyId.value='<%=request.getAttribute("companyId")%>';                
                document.rcItem.fromDate.value='<%=request.getAttribute("fromDate")%>';                
                document.rcItem.toDate.value='<%=request.getAttribute("toDate")%>';                
            }
            if('<%=request.getAttribute("categoryId")%>'!='null'){
                document.rcItem.categoryId.value='<%=request.getAttribute("categoryId")%>';
            }
                               
            if('<%=request.getAttribute("paplCode")%>'!='null'){
                document.rcItem.paplCode.value='<%=request.getAttribute("paplCode")%>';
            }
            if('<%=request.getAttribute("itemName")%>'!='null'){
                document.rcItem.itemName.value='<%=request.getAttribute("itemName")%>';
            } 
            if('<%=request.getAttribute("rcId")%>'!='null'){
                document.rcItem.rcId.value='<%=request.getAttribute("rcId")%>';
            } 
      
      }
    </script>
    <body onload="getItemNames();setValues();">
        <form name="rcItem" method="post" >
            <%@ include file="/content/common/path.jsp" %>           
            <%@ include file="/content/common/message.jsp" %>



<table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">RC ITEM LIST</li>
    </ul>
    <div id="first">
    <table width="900" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
  <tr>
                    <td height="30">Category</td>
                    <td  height="30"><select name="categoryId" class="textbox" style="width:125px">
                            <option value="0">-select-</option>
                            <c:if test = "${categoryList != null}" >
                                <c:forEach items="${categoryList}" var="category">
                                    <option value='<c:out value="${category.categoryId}"/>'><c:out value="${category.categoryName}"/></option>
                                </c:forEach>
                            </c:if>

                    </select></td>
                    <td height="30">Service Point</td>
                    <td height="30"><select  class="textbox" name="companyId" style="width:125px">
                            <c:if test = "${companyId == 1011}" >
                            <option value="0">-select-</option>
                             </c:if>
                            <c:if test = "${locationList != null}" >
                                <c:forEach items="${locationList}" var="company">
                                    <c:choose>
                                        <c:when test="${company.companyTypeId==1012}" >
                                            <c:if test = "${company.cmpId== companyId && companyId != 1011}" >
                                                <option selected value='<c:out value="${company.cmpId}"/>'><c:out value="${company.name}"/></option>
                                            </c:if>
                                            <c:if test = "${companyId==1011}" >
                                             <option value='<c:out value="${company.cmpId}"/>'><c:out value="${company.name}"/></option>
                                            </c:if>
                                        </c:when>
                                    </c:choose>
                                </c:forEach>
                            </c:if>

                    </select></td>
                    <td height="30">RC Number</td>
                    <td height="30"><input type="textbox" class="textbox" name="rcId"  ></td>
                    <td height="30">Papl Code</td>
                    <td height="30"><input type="textbox" class="textbox" name="paplCode"  ></td>
                </tr>

                <tr>

                    <td height="30">Item Name</td>
                    <td height="30"><input type="textbox" class="textbox" name="itemName" id="itemName"  ></td>
                    <td height="30"><font color="red">*</font>From Date</td>
                  <td><input name="fromDate" type="text" class="textbox" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onClick="displayCalendar(document.rcItem.fromDate,'dd-mm-yyyy',this)"/> </td>
                    <td><font color="red">*</font>To Date</td>
                  <td height="30"><input name="toDate" type="text" class="textbox" value="" size="20">
                    <img src="/throttle/images/cal.gif" name="Calendar"  onClick="displayCalendar(document.rcItem.toDate,'dd-mm-yyyy',this)"/> </td>
                  <td colspan="2"><input type="button" class="button" value="Search" onclick="rcDetails();" ></td>
                </tr>

    </table>
    </div></div>
    </td>
    </tr>
    </table>
            <br>

            
            
            <center></center>
            <br>
             <c:if test = "${RcItemList != null}" >
            <table align="center" width="700" border="0" cellspacing="0" cellpadding="0" class="border">
                <tr>
                    <td  class="contenthead" height="30">RC number</td>
                    <td  class="contenthead" height="30">Papl Code</td>
                    <td  class="contenthead" height="30">Item Name</td>
                    <td  class="contenthead" height="30">Category</td>
                    <td  class="contenthead" height="30">No of RC's</td>                    
                    <td  class="contenthead" height="30">&nbsp;</td>
                </tr>
                 <% int index = 0;%>
                  <c:forEach items="${RcItemList}" var="rc"> 
                        <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                <tr>
                    <td  class="<%=classText %>"  height="30"><input type="hidden" name="rcNo" value='<c:out value="${rc.rcId}" />'><c:out value="${rc.rcId}" /></td>                    
                    <td  class="<%=classText %>"  height="30"><c:out value="${rc.paplCode}" /></td>
                    <td  class="<%=classText %>"  height="30"><c:out value="${rc.itemName}" /></td>
                    <td  class="<%=classText %>"  height="30"><c:out value="${rc.categoryName}" /></td>
                    <td  class="<%=classText %>"  height="30"><c:out value="${rc.rcTime}" /></td>                    
                    <td  class="<%=classText %>"  height="30"><a href="#" onClick="rcHistory(<%=index%>);">RC History </a></td>
                </tr>
                <%index++;%>
            </c:forEach>
            </table>
        </c:if>
        </form>
    </body>
    

