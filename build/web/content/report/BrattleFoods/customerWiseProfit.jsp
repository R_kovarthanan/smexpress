<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>


    <script type="text/javascript">
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            // alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script type="text/javascript">
        function submitPage(value) {
            if (document.getElementById('fromDate').value == '') {
                alert("please select from date");
                document.getElementById('fromDate').focus();
            } else if (document.getElementById('toDate').value == '') {
                alert("please select to date");
                document.getElementById('toDate').focus();
            } else {
                if (value == 'ExportExcel') {
                    document.accountReceivable.action = '/throttle/handleCustomerWiseProfit.do?param=ExportExcel';
                    document.accountReceivable.submit();
                } else {
                    document.accountReceivable.action = '/throttle/handleCustomerWiseProfit.do?param=Search';
                    document.accountReceivable.submit();
                }
            }
        }

        function setValue() {
            if ('<%=request.getAttribute("page")%>' != 'null') {
                var page = '<%=request.getAttribute("page")%>';
                if (page == 1) {
                    submitPage('search');
                }
            }
        }


        function viewCustomerProfitDetails(tripIds) {
            //alert(tripIds);
            window.open('/throttle/viewCustomerWiseProfitDetails.do?tripId=' + tripIds + "&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
        }
    </script>

    <style type="text/css">





        .container {width: 960px; margin: 0 auto; overflow: hidden;}
        .content {width:800px; margin:0 auto; padding-top:50px;}
        .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

        /* STOP ANIMATION */



        /* Second Loadin Circle */

        .circle1 {
            background-color: rgba(0,0,0,0);
            border:5px solid rgba(100,183,229,0.9);
            opacity:.9;
            border-left:5px solid rgba(0,0,0,0);
            /*	border-right:5px solid rgba(0,0,0,0);*/
            border-radius:50px;
            /*box-shadow: 0 0 15px #2187e7; */
            /*	box-shadow: 0 0 15px blue;*/
            width:40px;
            height:40px;
            margin:0 auto;
            position:relative;
            top:-50px;
            -moz-animation:spinoffPulse 1s infinite linear;
            -webkit-animation:spinoffPulse 1s infinite linear;
            -ms-animation:spinoffPulse 1s infinite linear;
            -o-animation:spinoffPulse 1s infinite linear;
        }

        @-moz-keyframes spinoffPulse {
            0% { -moz-transform:rotate(0deg); }
            100% { -moz-transform:rotate(360deg);  }
        }
        @-webkit-keyframes spinoffPulse {
            0% { -webkit-transform:rotate(0deg); }
            100% { -webkit-transform:rotate(360deg);  }
        }
        @-ms-keyframes spinoffPulse {
            0% { -ms-transform:rotate(0deg); }
            100% { -ms-transform:rotate(360deg);  }
        }
        @-o-keyframes spinoffPulse {
            0% { -o-transform:rotate(0deg); }
            100% { -o-transform:rotate(360deg);  }
        }



    </style>
</head>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageUser" text="Customer Wise Profit"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Settings" text="default text"/></a></li>
            <li class=""><spring:message code="hrms.label.ManageUser" text="default text"/></li>

        </ol>
    </div>
</div>
<body onload="setValue();
                sorter.size(10);">
    <form name="accountReceivable" action=""  method="post">
        <%@ include file="/content/common/message.jsp" %>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">


                    <table class="table table-info mb10 table-hover" id="bg" >
                        <thead>
                            <tr>
                                <th colspan="2" height="30" >Customer Wise Profit </th>
                            </tr>
                        </thead>
                    </table>
                    <table class="table table-info mb30 table-hover" id="report" >
                        <tr>
                            <td><font color="red">*</font>Customer</td>
                            <td height="30">
                                <select style="width:250px;height:40px"  class="form-control"  name="customerId" id="customerId"  style="width:125px;">
                                    <option value="">---Select---</option>
                                    <c:forEach items="${customerList}" var="customerList">
                                        <option value='<c:out value="${customerList.custId}"/>'><c:out value="${customerList.custName}"/></option>
                                    </c:forEach>
                                </select>
                                <script>
                                    document.getElementById('customerId').value = <c:out value="${customerId}"/>;
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:250px;height:40px" value="<c:out value="${fromDate}"/>" ></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" value="<c:out value="${toDate}"/>"></td>
                            <td><input type="button"  class="btn btn-success"name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);"></td>
                            <td><input type="button"  class="btn btn-success"name="Search"   value="Search" onclick="submitPage(this.name);"></td>
                        </tr>
                    </table>
                

            <c:if test = "${customerWiseProfitList == null && customerWiseProfitListSize != null}" >
                <center>
                    <font color="blue">Please Wait Your Request is Processing</font>
                    <div class="container">
                        <div class="content">
                            <div class="circle"></div>
                            <div class="circle1"></div>
                        </div>
                    </div>
                </center>
            </c:if>
            <br>
            <br>
            <br>

            <c:if test = "${customerWiseProfitList != null && customerWiseProfitListSize == null}" >
                <table class="table table-info mb30 table-hover" id="table" >
                    <thead>
                        <tr height="80" >
                            <th>S.No</th>
                            <th>Customer Name</th>
                            <th>Billing Type</th>
                            <th>Revenue</th>
                            <th>Expense</th>
                            <th>Profit</th>
                            <th>Profit %</th>
                            <th>No Of Trip</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:set var="profitPercent" value="${0}"/>
                        <c:forEach items="${customerWiseProfitList}" var="csList">

                            <tr height="30">

                                <td align="center"><%=sno%></td>
                                <td align="left"><c:out value="${csList.customerName}"/></td>
                                <td align="left"><c:out value="${csList.billingType}"/></td>
                                <td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.freightAmount}" /></td>
                                <td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.totalExpenses}" /></td>
                                <c:if test="${csList.customerName != 'Empty Trip'}">
                                    <c:if test="${csList.profitValue <= 0}">
                                        <td align="right">
                                            <font color="red">
                                                <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.profitValue}" />
                                            </font>
                                        </td>
                                    </c:if>
                                    <c:if test="${csList.profitValue > 0}">
                                        <td align="right">
                                            <font color="green">
                                                <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.profitValue}" />
                                            </font>
                                        </td>
                                    </c:if>
                                    <c:if test="${csList.profitPercent <= 0}">
                                        <td align="right">
                                            <font color="red">
                                                <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.profitPercent}" />
                                            </font>
                                        </td>
                                    </c:if>
                                    <c:if test="${csList.profitPercent > 0}">
                                        <td align="right">
                                            <font color="green">
                                                <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${csList.profitPercent}" />
                                            </font>
                                        </td>
                                    </c:if>
                                </c:if>
                                <c:if test="${csList.customerName == 'Empty Trip'}">
                                    <td align="right"><font color="red">0.00</font></td>
                                    <td align="right"><font color="red">0.00</font></td>
                                        </c:if>

                                <td align="center"><c:out value="${csList.tripNos}"/></td>
                                    <c:set var="totalRevenue" value="${csList.freightAmount + totalRevenue}"/>
                                    <c:set var="expenses" value="${csList.totalExpenses + expenses}"/>
                                    <c:set var="profit" value="${csList.profitValue + profit}"/>
                                    <c:set var="totalTripNos" value="${csList.tripNos + totalTripNos}"/>
                                    <c:set var="perc" value="${profit/totalRevenue}"/>
                                    <c:set var="profitPercent" value="${perc * 100}"/>
                            </tr>
                            <%
                                       index++;
                                       sno++;
                            %>
                        </c:forEach>

                    </tbody>
                </table>


                <br/>
                <br/>
                <c:if test = "${customerWiseProfitList != null}" >
                    <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="1" >
                        <tr height="25" align="right">
                            <td style="background-color: #6374AB; color: #ffffff">Total Trips Carried Out</td>
                            <td width="150" align="right"><fmt:formatNumber type="number"  value="${totalTripNos}" /></td>
                        </tr>
                        <tr height="25" >
                            <td style="background-color: #6374AB; color: #ffffff">Total Income</td>
                            <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalRevenue}" /></td>
                        </tr>
                        <tr height="25" >
                            <td style="background-color: #6374AB; color: #ffffff">Total Fixed Expenses</td>
                            <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${expenses}" /></td>
                        </tr>
                        <tr height="25" >
                            <td style="background-color: #6374AB; color: #ffffff">Total Profit </td>
                            <td width="150" align="right">
                                <c:if test="${profit > 0}">
                                    <font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profit}" /></font>
                                    </c:if>
                                    <c:if test="${profit <= 0}">
                                    <font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profit}" /></font>
                                    </c:if>
                            </td>
                        </tr>
                        <tr height="25">
                            <td style="background-color: #6374AB; color: #ffffff">Total Profit % </td>
                            <td width="150" align="right">
                                <c:if test="${perc/count > 0}">
                                    <font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font>
                                    </c:if>
                                    <c:if test="${perc/count <= 0}">
                                    <font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font>
                                    </c:if>
                            </td>
                        </tr>
                    </table>
                </c:if>
            </c:if>

</div>

            </div>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>    
        </div>
    </form>
</body>                            

<%@ include file="/content/common/NewDesign/settings.jsp" %>
