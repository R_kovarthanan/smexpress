<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js" type="text/javascript"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
            function submitPage(value) {
                if(value == 'ExportExcel'){
                    document.marginWiseTripSummary.action = '/throttle/handleMonthWiseEmptyRunSummaryReport.do?param=ExportExcel';
                    document.marginWiseTripSummary.submit();
                }else{
                    document.marginWiseTripSummary.action = '/throttle/handleMonthWiseEmptyRunSummaryReport.do?param=Search';
                    document.marginWiseTripSummary.submit();
                }
            }

            function setValue(){
                if('<%=request.getAttribute("page")%>' !='null'){
                    var page = '<%=request.getAttribute("page")%>';
                    if(page == 1){
                        submitPage('search');
                    }
                   }
            }
            
            function viewCustomerProfitDetails(tripIds) {
            //alert(tripIds);
            window.open('/throttle/viewCustomerWiseProfitDetails.do?tripId='+tripIds+"&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
            }
        </script>
        <style type="text/css">





            .container {width: 960px; margin: 0 auto; overflow: hidden;}
            .content {width:800px; margin:0 auto; padding-top:50px;}
            .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

            /* STOP ANIMATION */



            /* Second Loadin Circle */

            .circle1 {
                background-color: rgba(0,0,0,0);
                border:5px solid rgba(100,183,229,0.9);
                opacity:.9;
                border-left:5px solid rgba(0,0,0,0);
                /*	border-right:5px solid rgba(0,0,0,0);*/
                border-radius:50px;
                /*box-shadow: 0 0 15px #2187e7; */
                /*	box-shadow: 0 0 15px blue;*/
                width:40px;
                height:40px;
                margin:0 auto;
                position:relative;
                top:-50px;
                -moz-animation:spinoffPulse 1s infinite linear;
                -webkit-animation:spinoffPulse 1s infinite linear;
                -ms-animation:spinoffPulse 1s infinite linear;
                -o-animation:spinoffPulse 1s infinite linear;
            }

            @-moz-keyframes spinoffPulse {
                0% { -moz-transform:rotate(0deg); }
            100% { -moz-transform:rotate(360deg);  }
            }
            @-webkit-keyframes spinoffPulse {
                0% { -webkit-transform:rotate(0deg); }
            100% { -webkit-transform:rotate(360deg);  }
            }
            @-ms-keyframes spinoffPulse {
                0% { -ms-transform:rotate(0deg); }
            100% { -ms-transform:rotate(360deg);  }
            }
            @-o-keyframes spinoffPulse {
                0% { -o-transform:rotate(0deg); }
            100% { -o-transform:rotate(360deg);  }
            }



        </style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.MOnth Wise Empty Run Summary" text="MOnth Wise Empty Run Summary"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Reports"/></a></li>
            <li class=""><spring:message code="hrms.label.MOnth Wise Empty Run Summary" text=" MOnth Wise Empty Run Summary"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">    <body onload="setValue();">
        <form name="marginWiseTripSummary" action=""  method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
                                <table class="table table-info mb30 table-hover">
                                    <thead><tr><th colspan="4">MOnth Wise Empty Run Summary</th></tr></thead>
                                    <tr>
                                        <td><font color="red">*</font>Trip Start Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker form-control" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>Trip End Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker form-control" value="<c:out value="${toDate}"/>"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right"><input type="button" class="btn btn-success" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                        <td colspan="2"><input type="button" class="btn btn-success" name="Search"   value="Search" onclick="submitPage(this.name);"></td>
                                    </tr>
                                </table>

            <c:if test = "${monthWiseEmptyRunSummary == null}" >
                <center>
                    <font color="blue">Please Wait Your Request is Processing</font>
                    <div class="container">
                        <div class="content">
                            <div class="circle"></div>
                            <div class="circle1"></div>
                        </div>
                    </div>
                </center>
            </c:if>
            <c:if test = "${monthWiseEmptyRunSummary != null}" >
                <table class="table table-info mb30 table-hover"  id="table" class="sortable">
                    <thead>
                        <tr height="40">
                            <th><h3></h3></th>
                            <th colspan="5"><h3><c:out value="${fromDate}"/>&nbsp;to&nbsp;<c:out value="${toDate}"/>&nbsp;Month wise Empty Run Summary</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%int sno = 1;%>
                    <thead>  <tr>
                            <th>S No</th>
                            <th>Month Name</th>
                            <th>Trip Count</th>
                            <th>Run Km</th>
                            <th>Amount Spend</th>
                        </tr></thead>
                            <c:forEach items="${monthWiseEmptyRunSummary}" var="month">
                            <tr>
                                <td><%=sno%></td>
                                <td><c:out value="${month.monthName}"/></td>
                                <td><c:out value="${month.tripCount}"/></td>
                                <td><c:out value="${month.runKm}"/></td>
                                <td><c:out value="${month.amountSpend}"/></td>
                            </tr>
                            <%sno++;%>
                            </c:forEach>

                    </tbody>

                </table>
            </c:if>
        </form>

    </body>    
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>