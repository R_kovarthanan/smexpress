<%-- 
    Document   : vehicleUtilizationReportExcel
    Created on : Dec 23, 2013, 3:59:10 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "RnmSummary-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
                  <c:if test="${rnmList != null }">
                  <table align="center" border="1" id="table" class="sortable" style="width:1000px;" >

                    <thead>
                    <tr style="border-top:1px solid white;height:50px">
                    <th style="border-left:1px solid white"><h3>S.No</h3></th>
                    <th style="border-left:1px solid white"><h3>Vehicle no</h3></th>
                    <th colspan="6" align="center" style="border-left:1px solid white"><center><h3><c:out value="${firstmonth}"/></h3></center></th>
                    <th colspan="6" align="center" style="border-left:1px solid white"><center><h3><c:out value="${secondMonth}"/></h3></center></th>
                    <th colspan="6" align="center" style="border-left:1px solid white"><center><h3><c:out value="${thirdMonth}"/></h3></center></th>



                    </tr>
                    <tr  style="height:50px">
                    <th style="border-left:1px solid white"><h3></h3></th>
                    <th style="border-left:1px solid white"><h3></h3></th>

                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>Accidental</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>Asset</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>BREAK DOWN REEFER WORKS</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>BREAK DOWN VEHICLE WORK</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>REGULAR REEFER SERVICE</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>REGULAR VEHICLE SERVICE</h3></th>

                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>Accidental</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>Asset</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>BREAK DOWN REEFER WORKS</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>BREAK DOWN VEHICLE WORK</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>REGULAR REEFER SERVICE</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>REGULAR VEHICLE SERVICE</h3></th>

                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>Accidental</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>Asset</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>BREAK DOWN REEFER WORKS</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>BREAK DOWN VEHICLE WORK</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>REGULAR REEFER SERVICE</h3></th>
                    <th style="border-left:1px solid white;border-top:1px solid white"><h3>REGULAR VEHICLE SERVICE</h3></th>



                                        </tr>
                    </thead>
                    <tbody>

                        <% int index = 1;%>
                                 <c:forEach items="${rnmList}" var="vehicle">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>




                             <c:set var="totalmonth1Accidental" value="${totalmonth1Accidental+vehicle.month1Accidental}"/>
                             <c:set var="totalmonth2Accidental" value="${totalmonth2Accidental+vehicle.month2Accidental}"/>
                             <c:set var="totalmonth3Accidental" value="${totalmonth3Accidental+vehicle.month3Accidental}"/>
                             <c:set var="totalmonth1Asset" value="${totalmonth1Asset+vehicle.month1Asset}"/>
                             <c:set var="totalmonth2Asset" value="${totalmonth2Asset+vehicle.month2Asset}"/>
                             <c:set var="totalmonth3Asset" value="${totalmonth3Asset+vehicle.month3Asset}"/>
                             <c:set var="totalmonth1BreakDownReeferWorks" value="${totalmonth1BreakDownReeferWorks+vehicle.month1BreakDownReeferWorks}"/>
                             <c:set var="totalmonth2BreakDownReeferWorks" value="${totalmonth2BreakDownReeferWorks+vehicle.month2BreakDownReeferWorks}"/>
                             <c:set var="totalmonth3BreakDownReeferWorks" value="${totalmonth3BreakDownReeferWorks+vehicle.month3BreakDownReeferWorks}"/>
                              <c:set var="totalmonth1BreakDownVehicleWorks" value="${totalmonth1BreakDownVehicleWorks+vehicle.month1BreakDownVehicleWorks}"/>
                             <c:set var="totalmonth2BreakDownVehicleWorks" value="${totalmonth2BreakDownVehicleWorks+vehicle.month2BreakDownVehicleWorks}"/>
                             <c:set var="totalmonth3BreakDownVehicleWorks" value="${totalmonth3BreakDownVehicleWorks+vehicle.month3BreakDownVehicleWorks}"/>
                             <c:set var="totalmonth1RegularReeferService" value="${totalmonth1RegularService+vehicle.month1RegularReeferService}"/>
                             <c:set var="totalmonth2RegularReeferService" value="${totalmonth2RegularReeferService+vehicle.month2RegularReeferService}"/>
                             <c:set var="totalmonth3RegularReeferService" value="${totalmonth3RegularReeferService+vehicle.month3RegularReeferService}"/>
                             <c:set var="totalmonth1RegularVehicleService" value="${totalmonth1RegularVehicleService+vehicle.month1RegularVehicleService}"/>
                             <c:set var="totalmonth2RegularVehicleService" value="${totalmonth2RegularVehicleService+vehicle.month2RegularVehicleService}"/>
                             <c:set var="totalmonth3RegularVehicleService" value="${totalmonth3RegularVehicleService+vehicle.month3RegularVehicleService}"/>
                                 <tr>
                                <td width="10" class="text1"><%=index++%></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.regNo}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month1Accidental}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month1Asset}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month1BreakDownReeferWorks}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month1BreakDownVehicleWorks}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month1RegularReeferService}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month1RegularVehicleService}"/></td>

                                <td  width="80" class="text1"><c:out value="${vehicle.month2Accidental}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month2Asset}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month2BreakDownReeferWorks}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month2BreakDownVehicleWorks}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month2RegularReeferService}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month2RegularVehicleService}"/></td>

                                <td  width="80" class="text1"><c:out value="${vehicle.month3Accidental}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month3Asset}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month3BreakDownReeferWorks}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month3BreakDownVehicleWorks}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month3RegularReeferService}"/></td>
                                <td  width="80" class="text1"><c:out value="${vehicle.month3RegularVehicleService}"/></td>


                            </tr>
                        </c:forEach>

                  <tr>
                                 <td  width="30" class="text1"  align="center"><b>&nbsp;Grand</b></td>
                                 <td  width="30" class="text1"  align="center"><b>&nbsp;Total</b></td>
                                 <td  width="30" class="text1"><b> <fmt:formatNumber pattern="##0.00"  value="${totalmonth1Accidental}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth1Asset}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth1BreakDownReeferWorks}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth1BreakDownVehicleWorks}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth1RegularReeferService}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth1RegularVehicleService}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth2Accidental}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth2Asset}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth2BreakDownReeferWorks}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth2BreakDownVehicleWorks}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth2RegularReeferService}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth2RegularVehicleService}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth3Accidental}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth3Asset}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth3BreakDownReeferWorks}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth3BreakDownVehicleWorks}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth3RegularReeferService}"/></b></td>
                                 <td  width="30" class="text1"><b><fmt:formatNumber pattern="##0.00" value="${totalmonth3RegularVehicleService}"/></b></td>

                            </tr>

                                          </tbody>
                </table>

                
            </c:if>
                     
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        </form>
    </body>
</html>
