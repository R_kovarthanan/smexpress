<%--
    Document   : viewTripSheet
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Throttle
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">


        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

        <script type="text/javascript">
            function submitPage(value) {
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                } else {
                    if (value == 'ExportExcel') {
                        document.tripSheet.action = '/throttle/handleTripWiseProfitEndStatus.do?param=ExportExcel';
                        document.tripSheet.submit();
                    } else {
                        document.tripSheet.action = '/throttle/handleTripWiseProfitEndStatus.do?param=Search';
                        document.tripSheet.submit();
                    }
                }
            }
            function setValue() {
                if ('<%=request.getAttribute("page")%>' != 'null') {
                    var page = '<%=request.getAttribute("page")%>';
                    if (page == 1) {
                        submitPage('search');
                    }
                }
                if ('<%=request.getAttribute("tripId")%>' != 'null') {
                    document.getElementById('tripId').value = '<%=request.getAttribute("tripId")%>';
                }
                if ('<%=request.getAttribute("fleetCenterId")%>' != 'null') {
                    document.getElementById('fleetCenterId').value = '<%=request.getAttribute("fleetCenterId")%>';
                }
            }
            function viewVehicleDetails(vehicleId) {
                //alert(vehicleId);
                window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }
            function viewTripDetails(tripId) {
                //alert(tripId);
                window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }
        </script>
    </head>

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageUser" text="Trip Wise Profitability "/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Settings" text="default text"/></a></li>
                <li class=""><spring:message code="hrms.label.ManageUser" text="default text"/></li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">

                <body onload="setValue();
                        sorter.size(20);">
                    <form name="tripSheet" method="post">


                        <%@include file="/content/common/message.jsp" %>
                        <%@ include file="/content/common/message.jsp" %>
                        <table class="table table-info mb10 table-hover" id="bg" >
                            <thead>
                                <tr>
                                    <th colspan="2" height="30" >Trip Wise Profitability </th>
                                </tr>
                            </thead>
                        </table>

                        <table class="table table-info mb30 table-hover" id="report" >
                            <tr>
                                <td align="center"><font color="red"></font>Trip Code</td>
                                <td height="30">
                                    <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>"/>
                                    <input type="text" name="tripId" id="tripId" value="" class="form-control" style="width:250px;height:40px"/>
                                </td>
                                <td >Fleet Center</td>
                                <td height="30"> <select name="fleetCenterId" id="fleetCenterId" class="form-control" style="width:250px;height:40px" style="height:20px; width:122px;" >
                                        <c:if test="${companyList != null}">
                                            <option value="" selected>--Select--</option>
                                            <c:forEach items="${companyList}" var="companyList">
                                                <option value='<c:out value="${companyList.cmpId}"/>'><c:out value="${companyList.name}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select></td>
                            </tr>
                            <tr>
                                <td align="center"><font color="red">*</font>From Date</td>
                                <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:250px;height:40px"  value="<c:out value="${fromDate}"/>" ></td>
                                <td><font color="red">*</font>To Date</td>
                                <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" value="<c:out value="${toDate}"/>"></td>
                            </tr>
                            <tr>
                                <td colspan="3" align="right"><input type="button" class="btn btn-success"  name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                <td colspan="3"><input type="button" class="btn btn-success"  name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);"></td>
                            </tr>
                        </table>
                        </td>
                        </tr>
                        </table>
                        <br>
                        <br>
                        <c:if test="${tripDetails != null}">
                            <table class="table table-info mb30 table-hover" id="table" >
                                <thead>
                                    <tr height="40" >
                                        <th rowspan="2">S.No</th>
                                        <th rowspan="2">Fleet Center</th>
                                        <th rowspan="2">Customer Name </th>                         
                                        <th rowspan="2">Billing Type </th>
                                        <th rowspan="2">Consignment No</th>
                                        <th rowspan="2">Trip Code</th>
                                        <th rowspan="2">Vehicle Type </th>                         
                                        <th rowspan="2">Vehicle Contract </th>                         
                                        <th rowspan="2">Vehicle No </th>
                                        <th rowspan="2">Route </th>
                                        <th rowspan="2">Estimated Revenue </th>
                                        <th rowspan="2">Expense </th>
                                        <th rowspan="2">Profit </th>
                                        <th rowspan="2">Profit %</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% int index = 1;%>
                                    <c:set var="totalTrips" value="0" />
                                    <c:set var="totalEarnings" value="0" />
                                    <c:set var="requestedAdvance" value="0" />
                                    <c:set var="earnings" value="0" />
                                    <c:set var="expense" value="0" />
                                    <c:forEach items="${tripDetails}" var="tripDetails">
                                        <%
                                                       String classText = "";
                                                       int oddEven = index % 2;
                                                       if (oddEven > 0) {
                                                           classText = "text2";
                                                       } else {
                                                           classText = "text1";
                                                       }
                                        %>

                                        <c:set var="totalTrips" value="${totalTrips + 1}"/>
                                        <c:set var="totalEarnings" value="${totalEarnings + tripDetails.revenue}"/>
                                        <tr >
                                            <td class="<%=classText%>"  align="center"><%=index++%></td>
                                            <td class="<%=classText%>"  align="center" ><c:out value="${tripDetails.companyName}"/></td>
                                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.customerName}"/></td>
                                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.billingType}"/></td>
                                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.consignmentName}"/></td>
                                            <td class="<%=classText%>"  align="center">
                                                <a href="#" onclick="viewTripDetails('<c:out value="${tripDetails.tripSheetId}"/>');"><c:out value="${tripDetails.tripCode}"/></a>
                                            </td>
                                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.vehicleTypeName}"/></td>
                                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.vendorType}"/></td>
                                            <td width="150" class="<%=classText%>" align="center" ><a href="#" onclick="viewVehicleDetails('<c:out value="${tripDetails.vehicleId}"/>')"><c:out value="${tripDetails.vehicleNo}"/></a></td>
                                            <td class="<%=classText%>"  align="center"><c:out value="${tripDetails.routeName}"/></td>
                                            <td class="<%=classText%>"  align="center"><fmt:formatNumber pattern="##0.00" value="${tripDetails.revenue}"/></td>
                                            <td class="<%=classText%>"  align="center"><fmt:formatNumber pattern="##0.00" value="${tripDetails.estimatedExpenses}"/></td>

                                            <c:set var="earnings" value="${tripDetails.revenue}" />
                                            <c:set var="expense" value="${tripDetails.estimatedExpenses}" />
                                            <c:set var="profit" value="${earnings - expense}" />
                                            <td class="<%=classText%>"  align="right">
                                                <c:if test="${profit <= 0 && tripDetails.customerName != 'Empty Trip'}">
                                                    <font color="red"><fmt:formatNumber pattern="##0.00"  value="${profit}" /></font>
                                                    </c:if>
                                                    <c:if test="${profit gt 0}">
                                                    <font color="green"><fmt:formatNumber pattern="##0.00"  value="${profit}" /></font>
                                                </c:if></td>

                                            <c:set var="profitValue" value="0" />
                                            <c:set var="percentageprofit" value="0" />
                                            <c:if test="${earnings != '0.00' }">
                                                <c:set var="profitValue" value="${profit/earnings}" />
                                                <c:set var="percentageprofit" value="${profitValue*100}" />
                                            </c:if>
                                            <c:if test="${earnings == '0.00' }">
                                                <c:set var="profitValue" value="0" />
                                                <c:set var="percentageprofit" value="0" />
                                            </c:if>

                                            <td class="<%=classText%>"  align="right">
                                                <c:if test="${percentageprofit <= 0 && tripDetails.customerName != 'Empty Trip'}">
                                                    <font color="red"><fmt:formatNumber pattern="##0.00"  value="${percentageprofit}" />&nbsp;%</font>
                                                    </c:if>
                                                    <c:if test="${percentageprofit > 0}">
                                                    <font color="green"><fmt:formatNumber pattern="##0.00"  value="${percentageprofit}" />&nbsp;%</font>
                                                    </c:if>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </c:if>


                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" >5</option>
                                    <option value="10">10</option>
                                    <option value="20" selected="selected">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 0);
                        </script>
                        <br/>
                        <br/>
                        <br/>

                        <br/>
                        <br/>
                        <br/>
                    </form>
                </body>

            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>