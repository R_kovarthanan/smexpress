<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="BPCLTransaction" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "BrokerCustomer-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

<c:if test="${brokerCustomerDetails != null}">
    <table align="center" border="0" id="table" class="sortable" width="100%" >

        <thead>
            <tr height="50">
                <th width="150" align="center"><h3>S.No</h3></th>
                <th align="center"><h3>Customer Name</h3></th>
                <th width="250" align="center"><h3>No of Trips</h3></th>
                <th width="250" align="center"><h3>Revenue</h3></th>
                <th width="150" align="center"><h3>Cost</h3></th>
                <th width="150" align="center"><h3>WFU Hrs</h3></th>
               
                
                
                            </tr>
        </thead>
        <tbody>
            <% int index = 1;%>
            <c:forEach items="${brokerCustomerDetails}" var="BPCLTD">
                <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                %>

                              <c:set var="vehicleUtalizePercentage" value="${(BPCLTD.tripTransitHours/BPCLTD.availableDays)*100 }"/>
                             <c:set var="availableDays" value="${(BPCLTD.availableDays /24) }"/>
                <tr>
                    <td class="<%=classText%>"><%=index++%></td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.custName}"/>

                                <td width="30" class="<%=classText%>"><c:out value="${BPCLTD.noOfTrips}"/></td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.estimatedRevenue}"/></td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.estimatedExpenses}"/></td>
                                <td width="30" class="<%=classText%>" ><c:out value="${BPCLTD.wfuHours}"/></td>
                    
                </tr>

            </c:forEach>
        </tbody>
    </table>
   </c:if>
        </form>
    </body>
</html>