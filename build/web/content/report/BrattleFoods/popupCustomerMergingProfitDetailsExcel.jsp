 <%--
    Document   : popupCustomerProfitDetailsExcel
    Created on : Dec 29, 2013, 3:13:56 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
<!--        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
         <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
<!--        <title>JSP Page</title>-->
    </head>
    <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>

        <form name="customerWiseProfitDetailExcel" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "customerWiseProfitDetailExcel-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
            <c:if test = "${popupCustomerProfitReport != null}" >
               <table style="width: 1100px" align="center" border="0" >
                    <thead>
                        <tr height="80">
                            <th class="contentsub"><h3>S.No</h3></th>
                            <th class="contentsub"><h3>Trip No</h3></th>
                            <th class="contentsub"><h3>Merging Code</h3></th>
                            <th class="contentsub"><h3>C Note</h3></th>
                            <th class="contentsub"><h3>Trip Start date</h3></th>
                            <th class="contentsub"><h3>Trip End date</h3></th>
                            <th class="contentsub"><h3>Customer</h3></th>
                            <th class="contentsub"><h3>Route</h3></th>
                            <th class="contentsub"><h3>Vehicle No</h3></th>
                            <th class="contentsub"><h3>Revenue</h3></th>
                            <th class="contentsub"><h3>Expense</h3></th>
                            <th class="contentsub"><h3>Profit</h3></th>
                            <th class="contentsub"><h3>profit %</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>

                        <c:forEach items="${popupCustomerProfitReport}" var="popCus">
                             <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <% if (oddEven > 0) { %>

                            <tr height="30">
                                <td class="text2" align="left" ><%=sno%></td>
                                <td class="text2" align="left" ><c:out value="${popCus.tripCode}"/> </td>
                                <td align="left" class="text2">
                                    <c:if test="${popCus.tripMergingId != ''}">
                                    MC-<c:out value="${popCus.tripMergingId}"/>/<c:out value="${popCus.orderSequence}"/>
                                    </c:if>
                                    <c:if test="${popCus.tripMergingId == ''}">
                                    &nbsp;
                                    </c:if>
                                </td>
                                <c:if test="${popCus.customerName != 'Empty Trip'}">
                                <td align="left" ><c:out value="${popCus.consignmentNoteNo}"/></td>
                                </c:if>
                                <c:if test="${popCus.customerName == 'Empty Trip'}">
                                <td align="left" >&nbsp;</td>
                                </c:if>
                                <td class="text2" align="left" ><c:out value="${popCus.tripStartDate}"/></td>
                                <td class="text2" align="left" ><c:out value="${popCus.tripEndDate}"/></td>
                                <td class="text2" align="left" ><c:out value="${popCus.customerName}"/></td>
                                <td class="text2" align="left" ><c:out value="${popCus.routeName}"/></td>
                                <td class="text2" align="left" ><c:out value="${popCus.regno}"/></td>
                                <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${popCus.revenue}" /></td>
                                <td class="text2" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${popCus.expenses}" /></td>
                                <c:if test="${popCus.customerName != 'Empty Trip'}">
                                <c:set var="profitValue" value="${popCus.revenue - popCus.expenses}"/>
                                <c:set var="profit" value="${profitValue/popCus.revenue}"/>
                                <c:set var="profitPercent" value="${profit*100}"/>
                                <c:if test="${profitValue > 0}" >
                                    <td class="text2" align="right">
                                    <font color="green">
                                     <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitValue}" />
                                    </font>
                                    </td>
                                </c:if>
                                <c:if test="${profitValue <= 0}" >
                                    <td class="text2" align="right">
                                    <font color="red">
                                     <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitValue}" />
                                    </font>
                                    </td>
                                </c:if>
                                     <c:if test="${profitPercent > 0}" >
                                    <td class="text2" align="right">
                                    <font color="green">
                                     <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />
                                    </font>
                                    </td>
                                </c:if>
                                <c:if test="${profitPercent <= 0}" >
                                    <td class="text2" align="right">
                                    <font color="red">
                                     <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />
                                    </font>
                                    </td>
                                </c:if>
                                </c:if>
                                <c:if test="${popCus.customerName == 'Empty Trip'}">
                                    <td class="text2" align="right"><font color="red">0.00</font></td>
                                    <td class="text2" align="right"><font color="red">0.00</font></td>
                                </c:if>
                            </tr>
                             <%}else{%>
                            <tr height="30">
                                <td class="text1" align="left" ><%=sno%></td>
                                <td class="text1" align="left" ><c:out value="${popCus.tripCode}"/> </td>
                                <td align="left" class="text1">
                                    <c:if test="${popCus.tripMergingId != ''}">
                                    MC-<c:out value="${popCus.tripMergingId}"/>/<c:out value="${popCus.orderSequence}"/>
                                    </c:if>
                                    <c:if test="${popCus.tripMergingId == ''}">
                                    &nbsp;
                                    </c:if>
                                </td>
                                <c:if test="${popCus.customerName != 'Empty Trip'}">
                                <td align="left"  class="text1"><c:out value="${popCus.consignmentNoteNo}"/></td>
                                </c:if>
                                <c:if test="${popCus.customerName == 'Empty Trip'}">
                                <td align="left" >&nbsp;</td>
                                </c:if>
                                <td class="text1" align="left" ><c:out value="${popCus.tripStartDate}"/></td>
                                <td class="text1" align="left" ><c:out value="${popCus.tripEndDate}"/></td>
                                <td class="text2" align="left" ><c:out value="${popCus.customerName}"/></td>
                                <td class="text1" align="left" ><c:out value="${popCus.routeName}"/></td>
                                <td class="text1" align="left" ><c:out value="${popCus.regno}"/></td>
                                <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${popCus.revenue}" /></td>
                                <td class="text1" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${popCus.expenses}" /></td>
                                <c:if test="${popCus.customerName != 'Empty Trip'}">
                                <c:set var="profitValue" value="${popCus.revenue - popCus.expenses}"/>
                                <c:set var="profit" value="${profitValue/popCus.revenue}"/>
                                <c:set var="profitPercent" value="${profit*100}"/>
                                <c:if test="${profitValue > 0}" >
                                    <td align="right">
                                    <font color="green">
                                     <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitValue}" />
                                    </font>
                                    </td>
                                </c:if>
                                <c:if test="${profitValue <= 0}" >
                                    <td align="right">
                                    <font color="red">
                                     <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitValue}" />
                                    </font>
                                    </td>
                                </c:if>
                                     <c:if test="${profitPercent > 0}" >
                                    <td align="right">
                                    <font color="green">
                                     <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />
                                    </font>
                                    </td>
                                </c:if>
                                <c:if test="${profitPercent <= 0}" >
                                    <td align="right">
                                    <font color="red">
                                     <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />
                                    </font>
                                    </td>
                                </c:if>
                               </c:if>
                               <c:if test="${popCus.customerName == 'Empty Trip'}">
                                   <td align="right"  class="text1"><font color="red">0.00</font></td>
                                   <td align="right"  class="text1"><font color="red">0.00</font></td>
                               </c:if>
                            </tr>
                            <%
                            }
                                       index++;
                                       sno++;
                            %>
                        </c:forEach>

                    </tbody>
                </table>
            </c:if>
        </form>
    </body>
</html>