<%-- 
    Document   : marginWiseTripSummary
    Created on : Jun 12, 2014, 01:31:16 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
        <form name="marginWiseTripSummary" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "MarginWiseSummary-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
            
            <c:if test = "${showDetail != null}" >
                <table style="width: 1100px" align="center" border="1" id="table" class="sortable">
                    <thead>
                        <tr height="40">
                            <th><h3></h3></th>
                            <th colspan="2"><h3><c:out value="${fromDate}"/>&nbsp;to&nbsp;<c:out value="${toDate}"/>&nbsp;Margin wise Summary
                                    <c:if test="${customerName != null && customerName != '--Select--'}">
                                        For <c:out value="${customerName}"/>
                                    </c:if>
                                </h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Sno</td>
                            <td align="center">NO Of Trips</td>
                            <td align="center">Margin In Range</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td align="center">
                                <c:out value="${margin40}"/>
                            </td>
                            <td align="center">Above 40%</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td align="center">
                                    <c:out value="${margin30}"/>
                            </td>
                            <td align="center">30-40%</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td align="center">
                                <c:out value="${margin20}"/>
                            </td>
                            <td align="center">20-30%</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td align="center">
                                <c:out value="${margin10}"/>
                            </td>
                            <td align="center">10-20%</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td align="center">
                                <c:out value="${margin0}"/>
                            </td>
                            <td align="center">0-10%</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td align="center">
                            <c:out value="${margin100}"/>
                            </td>
                            <td align="center">Below 0%</td>
                        </tr>

                    </tbody>

                </table>
            </c:if>
        </form>

    </body>    
</html>