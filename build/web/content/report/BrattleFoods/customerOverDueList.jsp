
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">



    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js" type="text/javascript"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
            function submitPage(value) {
                if(value == 'ExportExcel'){
                document.accountReceivable.action = '/throttle/handleAccountsReceivable.do?param=ExportExcel';
                document.accountReceivable.submit();
                }else{
                document.accountReceivable.action = '/throttle/handleAccountsReceivable.do?param=Search';
                document.accountReceivable.submit();
                }
            }
            
            function setValue(){
                if('<%=request.getAttribute("page")%>' !='null'){
                var page = '<%=request.getAttribute("page")%>';
                    if(page == 1){
                      submitPage('search');
                    }
                }
            }
            
            
         function viewInvoiceDetails(customerId) {
         var fromDate = document.getElementById("fromDate").value;
         var toDate = document.getElementById("toDate").value;
         var invoiceType = document.getElementById("invoiceType").value;
            window.open('/throttle/handleAccountsReceivableDetails.do?customerId='+customerId+'&param=Search&fromDate='+fromDate+'&toDate='+toDate, 'PopupPage', 'height = 800, width = 1150, scrollbars = yes, resizable = yes');
        }
        </script>
<style type="text/css">





.container {width: 960px; margin: 0 auto; overflow: hidden;}
.content {width:800px; margin:0 auto; padding-top:50px;}
.contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

/* STOP ANIMATION */



/* Second Loadin Circle */

.circle1 {
	background-color: rgba(0,0,0,0);
	border:5px solid rgba(100,183,229,0.9);
	opacity:.9;
	border-left:5px solid rgba(0,0,0,0);
/*	border-right:5px solid rgba(0,0,0,0);*/
	border-radius:50px;
	/*box-shadow: 0 0 15px #2187e7; */
/*	box-shadow: 0 0 15px blue;*/
	width:40px;
	height:40px;
	margin:0 auto;
	position:relative;
	top:-50px;
	-moz-animation:spinoffPulse 1s infinite linear;
        -webkit-animation:spinoffPulse 1s infinite linear;
	-ms-animation:spinoffPulse 1s infinite linear;
	-o-animation:spinoffPulse 1s infinite linear;
}

@-moz-keyframes spinoffPulse {
	0% { -moz-transform:rotate(0deg); }
	100% { -moz-transform:rotate(360deg);  }
}
@-webkit-keyframes spinoffPulse {
	0% { -webkit-transform:rotate(0deg); }
	100% { -webkit-transform:rotate(360deg);  }
}
@-ms-keyframes spinoffPulse {
	0% { -ms-transform:rotate(0deg); }
	100% { -ms-transform:rotate(360deg);  }
}
@-o-keyframes spinoffPulse {
	0% { -o-transform:rotate(0deg); }
	100% { -o-transform:rotate(360deg);  }
}



</style>
    </head>
    <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageUser" text="Accounts Receivable Report"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Settings" text="default text"/></a></li>
                    <li class=""><spring:message code="hrms.label.ManageUser" text="default text"/></li>

                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">

    <body onload="setValue();">
        <form name="accountReceivable" action=""  method="post">
            <%@ include file="/content/common/message.jsp" %>
                 <table class="table table-info mb10 table-hover" id="bg" >
		    <thead>
		<tr>
		    <th colspan="2" height="30" >Accounts Receivable Report </th>
		</tr>
		    </thead>
    </table>
          
                                
           <table class="table table-info mb30 table-hover" id="report" >
                              
                                    <tr>
                                        <td><font color="red">*</font>Customer</td>
                                        <td height="30">
                                            <select     class="form-control" style="width:250px;height:40px" name="customerId" id="customerId"  style="width:125px;">
                                                <option value="">---Select---</option>
                                                <c:forEach items="${customerList}" var="customerList">
                                                    <option value='<c:out value="${customerList.custId}"/>'><c:out value="${customerList.custName}"/></option>
                                                </c:forEach>
                                            </select>
                                            <script>
                                                document.getElementById('customerId').value = <c:out value="${customerId}"/> ;
                                            </script>
                                        </td>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:250px;height:40px" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" value="<c:out value="${toDate}"/>"></td>
                                    </tr>
                                    <c:if test="${invoiceType == null}">
                                    <tr>
                                        <td><font color="red"></font>Paid Invoice</td>
                                        <td height="30"><input name="invoiceType" id="invoiceType" type="radio" value="paid"></td>
                                        <td><font color="red"></font>Over Due Invoice</td>
                                        <td height="30"><input name="invoiceType" id="invoiceType" type="radio" value="overDue"></td>
                                        <td colspan="2"><font color="red"></font></td>
                                    </tr>
                                    </c:if>
                                    <c:if test="${invoiceType != null && invoiceType == 'paid'}">
                                        <tr>
                                        <td><font color="red"></font>Paid Invoice</td>
                                        <td height="30"><input name="invoiceType" id="invoiceType" type="radio" value="paid" checked></td>
                                        <td><font color="red"></font>Over Due Invoice</td>
                                        <td height="30"><input name="invoiceType" id="invoiceType" type="radio" value="overDue"></td>
                                        <td colspan="2"><font color="red"></font></td>
                                    </tr>
                                    </c:if>    
                                    <c:if test="${invoiceType != null && invoiceType == 'overDue'}">
                                        <tr>
                                        <td><font color="red"></font>Paid Invoice</td>
                                        <td height="30"><input name="invoiceType" id="invoiceType" type="radio" value="paid"></td>
                                        <td><font color="red"></font>Over Due Invoice</td>
                                        <td height="30"><input name="invoiceType" id="invoiceType" type="radio" value="overDue" checked></td>
                                        <td colspan="2"><font color="red"></font></td>
                                    </tr>
                                    </c:if>    
                                    <tr>
                                        <td colspan="3" align="right"><input type="button" class="btn btn-success" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                        <td colspan="3"><input type="button" class="btn btn-success" name="Search"   value="Search" onclick="submitPage(this.name);"></td>
                                    </tr>


                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
                                    

            <br>
            <br>
            <br>
            
            <c:if test = "${customerOverDueList == null && customerOverDueListSize == null}" >
                <center>
                <font color="blue">Please Wait Your Request is Processing</font>
                <div class="container">
                 <div class="content">
                <div class="circle"></div>
                <div class="circle1"></div>
                </div>
            </div>
                </center>
            </c:if>
            <c:if test = "${customerOverDueList != null}" >
                
                     <table class="table table-info mb30 table-hover" id="table" >
                    <thead>
                        <tr height="80">
                            <th>S.No</th>
                            <th>Customer Name</th>
                            <th>Total Amount Receivable</th>
                            <th>Receivable OverDue</th>
                            <th>View Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${customerOverDueList}" var="odList">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                                <c:if test="${invoiceType == null || invoiceType == 'paid'}">
                                    <tr height="30">
                                    <td align="left" class="text2"><%=sno%></td>
                                    <td align="left" class="text2"><c:out value="${odList.customerName}"/> </td>
                                    <td align="right" class="text2"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${odList.overDueGrandTotal}" /></td>
                                    <c:set var="overDue" value="0.00"/>
                                    <c:if test="${odList.overDueGrandTotal > odList.creditLimit && odList.creditLimit >  0.00}">
                                    <c:set var="overDue" value="${odList.overDueGrandTotal - odList.creditLimit}"/>
                                    </c:if>
                                    <c:if test="${odList.overDueGrandTotal == odList.creditLimit && odList.overDueGrandTotal < odList.creditLimit}">
                                    <c:set var="overDue" value="${0.00}"/>
                                    </c:if>
                                    <c:if test="${odList.overDueGrandTotal > odList.creditLimit && odList.creditLimit ==  0.00}">
                                    <c:set var="overDue" value="${odList.overDueGrandTotal}"/>
                                    </c:if>
                                    <td align="right" class="text2"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${overDue}" /></td>
                                    </td>
                                    <td align="left" class="text2"><a href="#" onclick="viewInvoiceDetails('<c:out value="${odList.customerId}"/>')"><c:out value="${odList.customerCode}"/></a></td>
                                    </tr>
                                     <%
                                       index++;
                                       sno++;
                                    %>
                                </c:if>
                                <c:if test="${invoiceType != null && invoiceType == 'overDue'}">
                                    <c:if test="${odList.overDueGrandTotal > odList.creditLimit && odList.creditLimit >  0.00}">
                                    <c:set var="overDue" value="${odList.overDueGrandTotal - odList.creditLimit}"/>
                                    </c:if>
                                    <c:if test="${odList.overDueGrandTotal == odList.creditLimit && odList.overDueGrandTotal < odList.creditLimit}">
                                    <c:set var="overDue" value="${0.00}"/>
                                    </c:if>
                                    <c:if test="${odList.overDueGrandTotal > odList.creditLimit && odList.creditLimit ==  0.00}">
                                    <c:set var="overDue" value="${odList.overDueGrandTotal}"/>
                                    </c:if>
                                    
                                    <c:if test="${overDue > 0}">
                                    <tr height="30">    
                                    <td align="left" class="text2"><%=sno%></td>
                                    <td align="left" class="text2"><c:out value="${odList.customerName}"/> </td>
                                    <td align="right" class="text2"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${odList.overDueGrandTotal}" /></td>
                                    <td align="right" class="text2"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="3" value="${overDue}" /></td>
                                    </td>
                                    <td align="left" class="text2"><a href="#" onclick="viewInvoiceDetails('<c:out value="${odList.customerId}"/>')"><c:out value="${odList.customerCode}"/></a></td>
                                    </tr>
                                     <%
                                       index++;
                                       sno++;
                                    %>
                                    </c:if>
                                </c:if>
                                
                           
                        </c:forEach>

                    </tbody>
                </table>
            
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <center>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
            </center>
                <div id="text" align="right">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
            
        </form>
        </c:if>
    </body>    
    
    </div>
</div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>