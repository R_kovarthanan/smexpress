<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="BPCLTransaction" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "CustomerOutstandingReport-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>

            <% int sno1 = 0;
                int index1 = 1;%>

            <div>
                <table class="table table-info mb30 table-hover"  >
                    <thead>
                        <tr align="center"  height="30">
                            <th height="30">S.No</th>
                            <th height="30">Vendor Name</th>
                            <th height="30">Expense</th>
                            <th height="30">Initial Advance </th>
                            <th height="30">Invoice Amount </th>
                            <th height="30">Outstanding Amount</th>

                        </tr>
                    </thead>
                    <tbody>
                        <c:set var="outstandingAmount" value="${0}"/>
                        <table class="table table-info mb30 table-hover"  >
                            <thead>
                                <tr align="center"  height="30">
                                    <th height="30">S.No</th>
                                    <th height="30">Customer Name</th>
                                    <th height="30">Expense</th>
                                    <th height="30">Freight Amount </th>
                                    <th height="30">Outstanding Amount</th>

                                </tr>
                            </thead>
                            <tbody>

                                <c:forEach items="${custommerOutStandingReport}" var="list">
                                    <c:set var="outstandingAmount" value="${invoiceAmount-list.freightAmount}"/>
                                    <%

                                        String classText = "";
                                        int oddEven = index1 % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>


                                    <tr  width="208" height="40" > 
                                        <td height="20"><%=index1%></td>
                                        <td height="20"><c:out value="${list.customerName}"/></td>
                                        <td height="20"><c:out value="${list.invoiceAmount}"/></td>
                                        <td height="20"><c:out value="${list.freightAmount}"/></td>
                                        <td height="20"><c:out value="${outstandingAmount}"/></td>


                                    </tr>
                                <script>setActiveIndHire(<%=index1%>);</script>
                                <%
                                    index1++;
                                    sno1++;
                                %>
                            </c:forEach>
                            <input type="hidden" name="count" id="count" value="<%=sno1%>"/>

                            </tbody>
                        </table>
            </div>
        </form>
    </body>
</html>