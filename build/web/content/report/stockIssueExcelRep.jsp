<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    </head>

    <body >
        <form name="IssueReport" method="post">

<%

String fileName = "attachment;filename=IssueReport.xls";
response.setContentType("application/vnd.ms-excel;charset=UTF-8");
response.setHeader("Content-disposition",fileName);
%>
            <br>


<table  border="0" class="border" align="center" width="820" cellpadding="0" cellspacing="0" id="bg">
                <tr>
                    <td class="contenthead" height="30" colspan="8">Material Issue  Report </td>
                </tr>
                
                
                <tr>

                    <td width="80" height="30" class="text1"><font color="red">*</font>From Date</td>
                    <td width="182" class="text1">
                    <%=request.getAttribute("fromDate")%></td>
                    <td width="148" class="text1"><font color="red">*</font>To Date</td>
                    <td width="172" height="30" class="text1"><%=request.getAttribute("toDate")%></td>
                    <!--<td width="80" height="30" class="text1">Workorder No</td>
                    <td width="80" height="30" class="text1"><%=request.getAttribute("rcWorkorderId")%></td> -->
                </tr>
                
            </table>

                    <c:set var="totalValue" value="0"/>
                    <c:set var="totalProfit" value="0"/>
                    <c:set var="totalNettProfit" value="0"/>
                    <c:set var="totalUnbilledValue" value="0"/>
                    <c:set var="totalUnbilledProfit" value="0"/>
                    <c:set var="totalUnbilledNettProfit" value="0"/>
                    <c:set var="totalQty" value="0"/>
            <c:if test = "${stokIssueList != null}" >


                    <% int index = 0; String type="Billed";%>
                    <c:forEach items="${stokIssueList}" var="issue">

                        <c:if test = "${issue.type == 'unbilled'}" >
                            <% 
                            if(type.equals("Billed") ){
                                index = 0; type="UnBilled";
                                %></table><%
                            }
                            %>

                        </c:if>
                    <%if (index == 0){%>

                    <table width="1200" border="1" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                        <tr><td colspan="23" align="left" ><%=type%></td></tr>
                    <tr class="contenthead">
                        <td class="contentsub">Sno</td>
                        <td height="30" class="contentsub">MFR</td>
                        <td height="30" class="contentsub">Model</td>
                        <td height="30" class="contentsub">REG NO</td>
                        <td height="30" class="contentsub">JOBCARD NO</td>
                        <td height="30" class="contentsub">RCWO NO</td>
                        <td height="30" class="contentsub">COUNTER NO</td>
                        <td height="30" class="contentsub">MRSNo </td>
                        <td height="30" class="contentsub">MANUALMRSNo </td>
                        <td height="30" class="contentsub">MRSDATE</td>
                        <td height="30" class="contentsub">IssueDATE</td>
                        <td class="contentsub">PAPL CODE</td>
                        <td height="30" class="contentsub">ITEM NAME</td>
                        <td class="contentsub">Tech Name</td>
                        <td class="contentsub">User</td>
                        <td height="30" class="contentsub">Iss Qty</td>
                        <td height="30" class="contentsub">Ret Qty</td>
                        <td height="30" class="contentsub">Net Qty</td>
                        <td height="30" class="contentsub">BuyPrice</td>
                        <td height="30" class="contentsub">SellPrice</td>
                        <td height="30" class="contentsub">Profit</td>
                        <td height="30" class="contentsub">Tax</td>
                        <td height="30" class="contentsub">NettProfit</td>

                    </tr>

            <%
            }
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>

                        <tr>

                            <td class="<%=classText%>" height="30"><%=index+1%></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.mfrName}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.modelName}"/></td>
                            <td class="<%=classText%>"><c:out value="${issue.regNo}" /></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.jobCardId}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.rcWorkorderId}"/></td>
<!--                            <td class="<%=classText%>" height="30"><c:out value="${issue.counterId}"/></td>-->
                            <td class="<%=classText%>" height="30">-</td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.mrsId}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.manualMrsNo}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.manualMrsDate}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.issueDate}"/></td>
                            <td class="<%=classText%>"><c:out value="${issue.paplCode}" /></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.itemName}"/></td>

                            <td class="<%=classText%>" height="30"><c:out value="${issue.empName}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.user}"/></td>
                            <td class="<%=classText%>" height="30" align="right" ><c:out value="${issue.issueQty}"/></td>
                            <td class="<%=classText%>" height="30" align="right" ><c:out value="${issue.retQty}"/></td>
                            <td class="<%=classText%>" height="30" align="right" ><c:out value="${issue.itemQty}"/></td>
                            <td class="<%=classText%>" height="30" align="right" ><c:out value="${issue.buyPrice}"/></td>
                            <td class="<%=classText%>" height="30" align="right" ><c:out value="${issue.sellPrice}"/></td>
                            <td class="<%=classText%>" height="30" align="right" ><c:out value="${issue.profit}"/></td>
                            <td class="<%=classText%>" height="30" align="right" ><c:out value="${issue.tax}"/></td>
                            
                            <td class="<%=classText%>" height="30" align="right" ><c:out value="${issue.nettProfit}"/></td>

<!--                         <td class="<%=classText%>" height="30"><c:out value="${issue.categoryName}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.itemType}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.itemPrice}"/></td>
                            <td class="<%=classText%>" height="30"><c:out value="${issue.itemQty}"/></td>-->

                            <c:if test = "${issue.type == 'billed'}" >
                              <c:set var="totalValue" value="${(issue.sellPrice * issue.itemQty) + totalValue}" />
                              <c:set var="totalProfit" value="${issue.profit + totalProfit}" />
                              <c:set var="totalNettProfit" value="${issue.nettProfit + totalNettProfit}" />
                            </c:if>
                              <c:if test = "${issue.type == 'unbilled'}" >
                                  <c:set var="totalUnbilledValue" value="${(issue.sellPrice * issue.itemQty) + totalUnbilledValue}" />
                                  <c:set var="totalUnbilledProfit" value="${issue.profit + totalUnbilledProfit}" />
                                  <c:set var="totalUnbilledNettProfit" value="${issue.nettProfit + totalUnbilledNettProfit}" />
                              </c:if>
                              <c:set var="totalQty" value="${issue.itemQty + totalQty}" />
                        </tr>
                        <% index++;%>
                    </c:forEach>
               <table>
                   <tr><td class="text2" colspan="3" align="left" ><b>Billed</b></td></tr>
                    <tr>
                        <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Total Selling Amount</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>Rs: <fmt:formatNumber value="${totalValue}" pattern="##.00"/></b></td>
                    
                    </tr>
                    <tr>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Total Profit</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>Rs: <fmt:formatNumber value="${totalProfit}" pattern="##.00"/></b>  </td>
                    

                    </tr>
                    <tr>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Total Nett Profit</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>Rs: <fmt:formatNumber value="${totalNettProfit}" pattern="##.00"/></b>  </td>
                    
                    </tr>
                    <tr><td class="text2" colspan="3" align="left" ><b>UnBilled</b></td></tr>
                    <tr>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Total Selling Amount</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>Rs: <fmt:formatNumber value="${totalUnbilledValue}" pattern="##.00"/></b></td>
                    </tr>

                    <tr>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Total Profit</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>Rs: <fmt:formatNumber value="${totalUnbilledProfit}" pattern="##.00"/></b>  </td>                    
                    </tr>

                    <tr>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Total Nett Profit</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>Rs: <fmt:formatNumber value="${totalUnbilledNettProfit}" pattern="##.00"/></b>  </td>
                    </tr>

                    <tr><td class="text2" colspan="3" align="left" ><b>Summary</b></td></tr>
                    <tr>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Nett Selling Amount</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>Rs: <fmt:formatNumber value="${totalValue + totalUnbilledValue}" pattern="##.00"/></b></td>

                    </tr>
                    <tr>
                    <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Total Profit</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>Rs: <fmt:formatNumber value="${totalProfit + totalUnbilledProfit}" pattern="##.00"/></b>  </td>

                    </tr>
                    <tr>
                        <td class="text2" height="30">&nbsp;</td>
                    <td class="text2" height="30"><b>Total Nett Profit</b> </td>
                    <td class="text2" height="30" align="right" >
                    <fmt:setLocale value="en_US" /><b>Rs: <fmt:formatNumber value="${totalNettProfit + totalUnbilledNettProfit}" pattern="##.00"/></b>  </td>

                    </tr>
                </table>
    <br>
            </c:if>
        </form>
    </body>

</html>

