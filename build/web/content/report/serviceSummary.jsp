

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>       
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
        
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>        
        <title>MRSList</title>
    </head>
    
    
    
    <script>
function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

        function addRow(val){
            //if(document.getElementById(val).innerHTML == ""){
            document.getElementById(val).innerHTML = "<input type='text' size='7' class='textbox' >";
            //}else{
            //document.getElementById(val).innerHTML = "";
            //}
        }
        
        function showTable()
        {
            var selectedMrs = document.getElementsByName("selectedIndex");
            var counter=0;
            for(var i=0;i<selectedMrs.length;i++){
                if(selectedMrs[i].checked == 1){
                    counter++;
                    break;
                }       
            }    
            if(counter ==0){
                alert("Please Select MRS");    
            }else{
            document.bill.action = "/throttle/mrsSummary.do";
            document.bill.submit();
        }
    }
    function newWO(){
        window.open('/throttle/content/stores/OtherServiceStockAvailability.html', 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }
    
    function submitPag(val){
        if(validate() == '0'){
            return;
        }else if(val=='purchaseOrder'){
        document.bill.purchaseType.value="1012"
        document.bill.action = '/throttle/generateMpr.do'
        document.bill.submit();
    }else if(val == 'localPurchase'){
    document.bill.purchaseType.value="1011"
    document.bill.action = '/throttle/generateMpr.do'
    document.bill.submit();    
}
}   



function searchSubmit(){
    
    if(document.bill.fromDate.value==''){
        alert("Please Enter From Date");
        return;
    }else if(document.bill.toDate.value==''){
    alert("Please Enter to Date");
    return;
} 
document.bill.action = '/throttle/serviceSummary.do'
document.bill.submit();    

}    


function validate()
{
    var selectedMrs = document.getElementsByName("selectedIndex1");
    var reqQtys = document.getElementsByName("reqQtys");
    var vendorIds = document.getElementsByName("vendorIds");
    var counter=0;
    for(var i=0;i<selectedMrs.length;i++){
        if(selectedMrs[i].checked == 1){
            counter++;
            if( parseInt(reqQtys[i].value)== 0  ||  isDigit(reqQtys[i].value) ){
                alert("Please Enter Valid Required Quantity");
                reqQtys[i].select();
                reqQtys[i].focus();
                return '0'
            }
            if( vendorIds[i].value=='0' ){
                alert("Please Select Vendor");
                vendorIds[i].focus();                
                return '0'
            }            
        }       
    }
    if(counter==0){
        alert("Please Select Atleast one item");
        return '0';
    }     
    return counter;
}


function selectBox(val){
    var checkBoxs = document.getElementsByName("selectedIndex1");
    checkBoxs[val].checked=1; 
}    



function setDate(fDate,tDate,serviceType,opId){
    if(fDate != 'null'){
        document.bill.fromDate.value=fDate;
    }
    if(tDate != 'null'){
        document.bill.toDate.value=tDate;
    }
    if(serviceType != 'null'){
        document.bill.serviceType.value=serviceType;
    }    
    if(opId != 'null'){
        document.bill.opId.value=opId;
    }    
    if('<%= request.getAttribute("usageTypeId") %>' != 'null'){
        document.bill.usageId.value='<%= request.getAttribute("usageTypeId") %>';
    }
    if('<%= request.getAttribute("custId") %>' != 'null'){
        document.bill.custId.value='<%= request.getAttribute("custId") %>';
    }    
}



    </script>
    
    <body onLoad="setDate('<%= request.getAttribute("fromDate") %>','<%= request.getAttribute("toDate") %>','<%= request.getAttribute("serviceType") %>','<%= request.getAttribute("opId") %>' )">        
        
        <form name="bill"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
    <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
    </h2></td>
    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
    </tr>
    <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:850;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Service Summary</li>
    </ul>
    <div id="first">
    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
     <tr>
            <td height="30">Usage Type</td>
            <td><select class="textbox" name="usageId"  style="width:125px;">
                    <option value="0">---Select---</option>
                    <c:if test = "${UsageList != null}" >
                        <c:forEach items="${UsageList}" var="Dept">
                            <option value='<c:out value="${Dept.usageId}" />'><c:out value="${Dept.usageName}" /></option>
                        </c:forEach >
                    </c:if>
            </select></td>
            <td height="30">Service Type</td>
            <td height="30"><select class="textbox" name="serviceType">
                    <option selected   value=0>---Select---</option>
                    <c:if test = "${serviceTypes != null}" >
                        <c:forEach items="${serviceTypes}" var="mfr">
                            <c:if test = "${mfr.servicetypeId ==1 }" >
                                <option value='<c:out value="${mfr.servicetypeId}" />'>
                                <c:out value="${mfr.servicetypeName}" />
                            </c:if>
                            <c:if test = "${mfr.servicetypeId !=1 }" >
                                <option  value='<c:out value="${mfr.servicetypeId}" />'>
                                <c:out value="${mfr.servicetypeName}" />
                            </c:if>
                        </c:forEach >
                    </c:if>
                </select>
            </td>
            <td height="30">Customer</td>
            <td height="30">
                <select class="textbox" name="custId"  style="width:125px;">
                    <c:if test = "${customerList != null}" >
                        <c:forEach items="${customerList}" var="cust">
                            <option value='<c:out value="${cust.custId}" />'><c:out value="${cust.custName}" /></option>
                        </c:forEach >
                    </c:if>
                </select>
            </td>
            <td rowspan="2"><input type="button" class="button" readonly name="search" value="search" onClick="searchSubmit();" ></td>
        </tr>

        <tr>
            <td height="30">From Date </td>
            <td height="30"><input type="text" name="fromDate" class="textbox" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.bill.fromDate,'dd-mm-yyyy',this)"/> </td>
            <td height="30">To Date </td>
            <td height="30"><input name="toDate" type="text" class="textbox" >
            <img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.bill.toDate,'dd-mm-yyyy',this)"/></td>
            <td height="30">Location</td>
            <td><select class="textbox" name="opId"  style="width:125px;">
                    <option value="0">---Select---</option>
                    <option value='EXTERNAL SERVICE' > EXTERNAL SERVICE </option>
                    <c:if test = "${operationPointList != null}" >
                        <c:forEach items="${operationPointList}" var="Dept">
                            <option value='<c:out value="${Dept.opId}" />'> <c:out value="${Dept.opName}" /></option>
                        </c:forEach >

                    </c:if>
                </select>
            </td>
        </tr>

    </table>
    </div></div>
    </td>
    </tr>
    </table>

  <br>
            
            <% int index = 0;
            String classText = "";
            int oddEven = 0;
            %>    
            <c:if test = "${serviceList != null}" >
                
                
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="800" id="bg" class="border">
                    
                    <tr class="contenthead">
                        <td  class="contentsub">Sno</td>
                        <td  height="30" class="contentsub">Vehicle Number </td>
                        <td  class="contentsub">Service Type </td>
                        <td  class="contentsub">Location </td>
                        <td  height="30" class="contentsub">Customer</td>
                        <td  class="contentsub">Usage Type</td>
                        <td  height="30" class="contentsub">Total<br> Complaints </td>
                        <td  height="30" class="contentsub">Attended<br> Complaints </td>
                        <td  height="30" class="contentsub"> Status </td>                        
                        <td  class="contentsub"> Total Days<br> Taken </td>
                    </tr>
                    
                    <c:forEach items="${serviceList}" var="service">    
                        <%

            oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        
            
                        <tr>
                            <td class="<%=classText %>" height="30"> <%= index+1 %> </td>
                            <td class="<%=classText %>" height="30"><c:out value="${service.regNo}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${service.serviceName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${service.companyName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${service.custName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${service.usageType}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${service.raisedProp}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${service.completedProp}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${service.problemStatus}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${service.day}"/></td>

                            
                            
                        </tr>
                        
                        
                        <%
            index++;
                        %>
                    </c:forEach>
                    
                </table>
            </c:if>
        </form>
        
        
    </body>
</html>
