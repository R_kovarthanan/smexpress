<%-- 
Document   : addDesig
Created on : Nov 03, 2008, 2:57:39 PM
Author     : Vijay
--%>


<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    
    <%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<head>
<!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
<title>PAPL</title>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>

<script language="javascript">

function validate(value){
if(document.addDesignation.searchStatus.value == '1'){
alert("Designation Name Already Exist");
document.addDesignation.desigName.focus();
}else if(isEmpty(document.addDesignation.desigName.value)){
alert("Designation Name Field Should Not Be Empty");
document.addDesignation.desigName.focus();
}else if(isSpecialCharacter(document.addDesignation.desigName.value)){
alert("Special Characters are not Allowed");
document.addDesignation.desigName.focus();
}else if(isChar(document.addDesignation.desigName.value)){
alert("Enter Valid Character ");
document.addDesignation.desigName.focus();
}else if(textValidation(document.addDesignation.desigName,'Designation Name')){
               return;
           }else if(textValidation(document.addDesignation.description,'Discription')){
               return;
           }
else if(value == 'add'){    
document.addDesignation.action = '/throttle/desigAdd.do';
document.addDesignation.submit();
}
}

    
    
function setFocus(){
document.addDesignation.desigName.focus();
}
window.onLoad = setFocus;

function isChar(s){
if(!(/^-?\d+$/.test(s))){
return false;
}
return true;
}

var httpRequest;
function getProfile(desigName)
{

var url = '/throttle/checkDesigName.do?desigName='+ desigName;
if (window.ActiveXObject)
{
httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
}
else if (window.XMLHttpRequest)
{
httpRequest = new XMLHttpRequest();
}

httpRequest.open("GET", url, true);

httpRequest.onreadystatechange = function() { processRequest(); } ;

httpRequest.send(null);
}


function processRequest()
{
if (httpRequest.readyState == 4)
{
if(httpRequest.status == 200)
{  
    
if(trim(httpRequest.responseText)!="") {   
document.addDesignation.desigName.focus();
document.addDesignation.searchStatus.value = "1";
document.getElementById("userNameStatus").innerHTML=httpRequest.responseText+" Already Exsts";
}else {
document.addDesignation.searchStatus.value = "0";
document.getElementById("userNameStatus").innerHTML="";
}


}
else
{
alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
}
}
}


</script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
</head>

<!--[if lte IE 7]>
<style type="text/css">

#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
#fixme {display:block;
top:0px; left:0px;  position:fixed;  }
* html #fixme  {position:absolute;}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
/*<![CDATA[*/ 
html {overflow-x:auto; overflow-y:hidden;}
/*]]>*/
</style>
<![endif]-->
    
                    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.AddDesignation" text="AddDesignation"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="HRMS"/></a></li>
		                    <li class=""><spring:message code="hrms.label.AddDesignation" text="AddDesignation"/></li>
		
		                </ol>
		            </div>
        </div>
                                    
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">

<body onLoad="document.addDesignation.desigName.focus()">
<form name="addDesignation" method="post">
<%--@ include file="/content/common/path.jsp" %>
<%@ include file="/content/common/message.jsp" --%>
<%@ include file="/content/common/message.jsp" %>

   
<font color="#FF0033" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; "><div align="center" id="userNameStatus" style="height:25px; "></div></font>   
<table class="table table-info mb30 table-hover" id="bg" >
    <thead>
     <tr height="30">
                    <th colspan="5" > Add Designation</th>
     </tr>
    </thead>
    
<tr>
    <td width="150" height="40"><font color=red>*</font>Designation Name</td>
<td width="150" height="40"><input type="text" name="desigName" class="form-control" style="width:250px;height:40px" maxlength="100" onChange="getProfile(this.value)"></td>

<td width="150"  height="40"><font color=red>*</font>Description</td>
<td width="150"  height="40"><textarea name="description" class="form-control" style="width:250px;height:40px"></textarea> </td>
</tr>
</table>
<center>

<input type="button" value="Add" name="add" onClick="validate(this.name)" class="btn btn-success">
&emsp;<input type="reset" class="btn btn-success" value="Clear">
<input type="hidden" name="searchStatus" value="">
</center>

</form>
</body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>
