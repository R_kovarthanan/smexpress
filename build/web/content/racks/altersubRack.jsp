<%-- 
Document   : altersubRack
Created on : Nov 6, 2008, 7:17:36 PM
Author     : shankar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.racks.business.RackTO" %> 
</head>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script>
    function submitPage()
    {
        var checValidate = selectedItemValidation();           
    }
    function setSelectbox(i){
    var selected=document.getElementsByName("selectedIndex") ;
    selected[i].checked = 1;
    } 
    function selectedItemValidation(){
var index = document.getElementsByName("selectedIndex");
var subRackName = document.getElementsByName("subRackNameList");
var rackName = document.getElementsByName("rackIdList");
var subRackDescription = document.getElementsByName("subRackDescriptionList");
var subRackStatus = document.getElementsByName("subRackStatusList");
var chec=0;

for(var i=0;(i<index.length && index.length!=0);i++){
if(index[i].checked){
chec++;
if(textValidation(subRackName[i],'SubRack Name ')){       
        return;
   }     
 if(textValidation(rackName[i],'Rack Name')){ 
        return;
   }     
 if(textValidation(subRackDescription[i],'SubRack Description')){ 
        return;
   }     
 if(textValidation(subRackStatus[i],'SubRack Status')){ 
        return;
   }  
        document.alterSubRack.action='/throttle/handleUpdateSubRack.do';
        document.alterSubRack.submit();      
}
}
if(chec == 0){
alert("Please Select Any One And Then Proceed");
custName[0].focus();
}
}
</script>
<body>
<form name="alterSubRack" method="post" >
<%@ include file="/content/common/path.jsp" %>

<%@ include file="/content/common/message.jsp" %>

<table width="603" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">

<tr>
<td width="89" height="30" class="contentsub"><div class="contentsub">Sub Rack Id</div></td>
<td width="45" height="30" class="contentsub"><div class="contentsub">Rack Name</div></td>
<td width="183" height="30" class="contentsub"><div class="contentsub">Sub Rack Name</div></td>
<td width="169" height="30" class="contentsub"><div class="contentsub">Rack Description</div></td>
<td width="77" height="30" class="contentsub"><div class="contentsub">Status</div></td>
<td width="40" height="30" class="contentsub"><div class="contentsub">Select</div></td>
</tr>
<% int index=0; %>
  <c:if test = "${subRackLists != null}" >
      <c:forEach items="${subRackLists}" var="subRack"> 
<%
String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text2";
} else {
classText = "text1";
}
%>
<tr>
<td class="<%=classText %>" height="30"><input type="hidden" name="subRackIdList" onchange="setSelectbox('<%= index %>');" value="<c:out value="${subRack.subRackId}"/>"><c:out value="${subRack.subRackId}"/></td>
<td class="<%=classText %>" height="30"><select class="textbox" style="width:125px" name="rackIdList" onchange="setSelectbox('<%= index %>');">
<c:forEach items="${rackLists}" var="subRack1">  
<c:choose>
   <c:when test="${subRack.rackId==subRack1.rackId}" > 
<option value="<c:out value="${subRack1.rackId}"/>" onchange="setSelectbox('<%= index %>');" selected ><c:out value="${subRack1.rackName}"/></option> 
   </c:when>
    <c:otherwise>
<option value="<c:out value="${subRack1.rackId}"/>" onchange="setSelectbox('<%= index %>');" ><c:out value="${subRack1.rackName}"/></option>         
    </c:otherwise> 
</c:choose>
</c:forEach>    
</select></td>
<td class="<%=classText %>" height="30"><input type="text" class="textbox" name="subRackNameList" value="<c:out value="${subRack.subRackName}"/>"onchange="setSelectbox('<%= index %>');" ></td>
<td class="<%=classText %>" height="30"><textarea class="textbox" name="subRackDescriptionList" onchange="setSelectbox('<%= index %>');"><c:out value="${subRack.subRackDescription}"/></textarea></td>
<td class="<%=classText %>" height="30"><select class="textbox" name="subRackStatusList" onchange="setSelectbox('<%= index %>');">
 <c:if test="${(subRack.subRackStatus=='n') || (subRack.subRackStatus=='N')}" >
 <option value="Y" >Active</option><option value="N" selected>InActive</option>                           
 </c:if>   
<c:if test="${(subRack.subRackStatus=='y') || (subRack.subRackStatus=='Y')}" >
 <option value="Y" selected>Active</option><option value="N">InActive</option>
</c:if>
</select></td>
<td width="40" height="30" class="<%=classText %>"><input type="checkbox" name="selectedIndex" value='<%= index %>'></td>
</tr>
 <%
  index++;
  %>
</c:forEach>
</c:if>   
</table>
<br>
<center>
<input type="button" value="Save" class="button" onclick="submitPage();">
</center>
</form>
</body>
</html>
