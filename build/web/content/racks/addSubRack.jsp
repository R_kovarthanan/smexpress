<%-- 
Document   : addSubRack
Created on : Nov 6, 2008, 7:12:48 PM
Author     : shankar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 
<%@ page import="ets.domain.racks.business.RackTO" %>
<title>JSP Page</title>
</head>
<script language="javascript" src="/throttle/js/validate.js"></script> 
<script>
    function submitPage()
    {
        if(textValidation(document.addSubRack.rackId,' Rack Name')){   
            return;
         }  
         if(textValidation(document.addSubRack.subRackName,'Sub Rack Name')){   
            return;
        }  
        if(textValidation(document.addSubRack.subRackDescription,'Sub Rack Description')){   
            return;
        } 
         
        document.addSubRack.action='/throttle/handleAddSubRack.do';
        document.addSubRack.submit();
    }
    function setFocus(){
        document.addSubRack.rackId.focus();
        }
</script>
<body onload="setFocus();">
<form name="addSubRack"  method="post" > 
<%@ include file="/content/common/path.jsp" %>


<%@ include file="/content/common/message.jsp" %>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="300" id="bg" class="border">
<tr>
<td colspan="2" class="contenthead" height="30"><div class="contenthead">Add Sub Rack Types</div></td>
</tr>
  
<tr>
<td class="text2" height="30"><font color="red">*</font>Rack Name</td>
<td class="text2" height="30"><select class="textbox" name="rackId" style="width:125px">
<c:if test = "${rackLists != null}" >   
<c:forEach items="${rackLists}" var="rack">   
<option value="<c:out value="${rack.rackId}"/>"><c:out value="${rack.rackName}"/></option> 
</c:forEach>
</c:if>  
</select>
</td>
</tr>
 
<tr>
<td class="text1" height="30"><font color="red">*</font>Sub Rack Name</td>
<td class="text1" height="30"><input type="text" name="subRackName" class="textbox"></td>
</tr>
<tr>
<td class="text2" height="30"><font color="red">*</font>Rack Description</td>
<td class="text2" height="30"><textarea class="textbox" name="subRackDescription"></textarea></td>
</tr>
</table>
<br>
<center>
<input type="button" value="Save" class="button" onclick="submitPage();">
&emsp;<input type="reset" class="button" value="Clear">
</center>
</form>
</body>
</html>
