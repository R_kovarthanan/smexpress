 <%@ include file="/content/common/NewDesign/header.jsp" %>
	<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
	
<%--<%@page contentType="text/html" import="java.sql.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>--%>
<!DOCTYPE html>
<html>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <title>Vehicle Road Tax Update</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>
        <script language="javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }
            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }
            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }



            var httpRequest;
            function getVehicleDetails(regNo)
            {
                //                document.VehicleRoadTax.regNo.focus();
                if(regNo != "") {
                    var url = "/throttle/getVehicleDetailsInsurance.do?regNo1="+ regNo;
                    url = url+"&sino="+Math.random();
                    if (window.ActiveXObject)  {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)  {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() { processRequest(); } ;
                    httpRequest.send(null);
                }
            }


            function processRequest()
            {
                if (httpRequest.readyState == 4)  {

                    if(httpRequest.status == 200) {
                        if(httpRequest.responseText.valueOf()!=""){
                            var detail = httpRequest.responseText.valueOf();
                            if(detail != "null"){
                                var vehicleValues = detail.split("~");
                                document.VehicleRoadTax.vehicleId.value = vehicleValues[0];
                                document.VehicleRoadTax.chassisNo.value = vehicleValues[1];
                                document.VehicleRoadTax.engineNo.value = vehicleValues[2];
                                document.VehicleRoadTax.vehicleMake.value = vehicleValues[4];
                                document.VehicleRoadTax.vehicleModel.value = vehicleValues[5];
                                if(parseInt(vehicleValues[9]) > 0){
                                    document.getElementById('exMsg').innerHTML="Vehicle Road Tax Entry is Already Existing";
                                    document.getElementById('detail').style.display="none";
                                }
                            }else{
                                document.VehicleRoadTax.vehicleId.value = "";
                                document.VehicleRoadTax.chassisNo.value = "";
                                document.VehicleRoadTax.engineNo.value = "";
                                document.VehicleRoadTax.vehicleMake.value = "";
                                document.VehicleRoadTax.vehicleModel.value = "";
                                document.getElementById('exMsg').innerHTML = "";
                            }
                        }
                    }
                    else
                    {
                        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                    }
                }
            }


            function submitPage()
            {
                if(textValidation(document.VehicleRoadTax.regno,'Vehicle No'));
                else{
                    document.VehicleRoadTax.action = '/throttle/saveVehicleRoadTax.do';
                    document.VehicleRoadTax.submit();
                }
            }

            function getEvents(e,val){
                var key;
                if(window.event){
                    key = window.event.keyCode;
                }else {
                    key = e.which;
                }
                if(key == 0) {
                    getVehicleDetails(val);
                }else{
                    getVehicleDetails(val);
                }
            }


        </script>
        
        <script type="text/javascript">
            //auto com

            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#regno').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getRegistrationNo.do",
                            dataType: "json",
                            data: {
                                regno: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#vehicleId').val(tmp[0]);
                        $('#regno').val(tmp[1]);
                        getVehicleDetails(tmp[1]);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };
            });


        </script>
        

    </head>
<div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Vehicle Details" text="Vehicle Details"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Vehicle Details" text="Vehicle Details"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">
    <body onLoad="document.VehicleRoadTax.regno.focus(); getVehicleNos();">
        <form name="VehicleRoadTax" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            <h2 align="center">Vehicle Road Tax Details </h2>
            <!--<table width="800" align="center" class="table2" cellpadding="0" cellspacing="0">-->
                <table class="table table-info mb30 table-hover"  >	
                    <thead>
                <tr>
                    <th class="contenthead" colspan="4">Vehicle Details</th>
                </tr>
                    </thead>
                <tr>
                    <td class="texttitle1">Vehicle No</td>
                    <td class="text1">
                        <input type="text" name="regNo" id="regno"class="form-control" style="width:240px;height:40px" onkeypress="getEvents(event,this.value);" onBlur="getVehicleDetails(this.value);" autocomplete="off" />
                        <!--                        <input type="text" name="regNo" id="regno" class="textbox"  onchange="getVehicleDetails(this.value);" onkeypress="getVehicleDetails(this.value);" autocomplete="off" />-->
                        <input type="hidden" name="vehicleId" id="vehicleId" /></td>
                    <td class="texttitle1">Make</td>
                    <td class="text1"><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" style="width:240px;height:40px" readonly  /></td>
                </tr>
                <tr>
                    <td class="texttitle2">Model</td>
                    <td class="text2"><input type="text" name="vehicleModel" id="vehicleModel" class="form-control" style="width:240px;height:40px" readonly /></td>
                    <td class="texttitle2">Usage</td>
                    <td class="text2"><input type="text" name="vehicleUsage" id="vehicleUsage" class="form-control" style="width:240px;height:40px" readonly  /></td>
                </tr>
                <tr>
                    <td class="texttitle1">Engine No</td>
                    <td class="text1"><input type="text" name="engineNo" id="engineNo" class="form-control" style="width:240px;height:40px" readonly /></td>
                    <td class="texttitle1">Chassis No</td>
                    <td class="text1"><input type="text" name="chassisNo" id="chassisNo" class="form-control" style="width:240px;height:40px" readonly /></td>
                </tr>
                <tr>
                    <td class="texttitle2" colspan="4">
                        <center><label id="exMsg" style="color: green; text-align: center; font-weight: bold; font-size: medium;"></label></center>
                    </td>
                </tr>
                <table class="table table-info mb30 table-hover"  >
                <thead>
                <tr>
                    <th class="contenthead" colspan="4">Road Tax Details</th>
                </tr>
                </thead>
                <tr>
                    <td colspan="4" align="center">
                        <div id="detail" style="display: block;">
                            <table width="100%">
                                <tr>
                                    <td class="texttitle1">Receipt No</td>
                                    <td class="text1"><input type="text" name="roadTaxReceiptNo" id="roadTaxReceiptNo" class="form-control" style="width:240px;height:40px" onclick="getVehicleDetails(regno.value);" /></td>
                                    <td class="texttitle1">Receipt Date</td>
                                    <td class="text1"><input type="text" name="roadTaxReceiptDate" id="roadTaxReceiptDate" class="datepicker form-control" style="width:240px;height:40px " /><!--&nbsp;<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.VehicleRoadTax.roadTaxReceiptDate,'dd-mm-yyyy',this)"/>--></td>
                                </tr>
                                <tr>
                                    <td class="texttitle2">Location</td>
                                    <td class="text2"><input type="text" name="roadTaxPaidLocation" id="roadTaxPaidLocation" class="form-control" style="width:240px;height:40px" /></td>
                                    <td class="texttitle2">Amount</td>
                                    <td class="text2"><input type="text" name="roadTaxPaidAmount" id="roadTaxPaidAmount" class="form-control" style="width:240px;height:40px" /></td>
                                </tr>
                                <tr>
                                    <td class="texttitle1">Period</td>
                                    <td class="text1"><select name="roadTaxPeriod" id="roadTaxPeriod" class="form-control" style="width:240px;height:40px">
                                            <option>Quarterly</option>
                                            <option>Half Yearly</option>
                                            <option>Yearly</option>
                                        </select>
                                    </td>
                                    <td class="texttitle1">Road Tax From Date</td>
                                    <td class="text1"><input type="text" name="roadTaxFromDate" id="roadTaxFromDate" class="datepicker form-control" style="width:240px;height:40px " /></td>
                                </tr>
                                <tr>
                                    <td class="texttitle1">Road Tax To Date</td>
                                    <td class="text1"><input type="text" name="nextRoadTaxDate" id="nextRoadTaxDate" class="datepicker form-control" style="width:240px;height:40px" /></td>
                                    <td class="texttitle1">Remarks</td>
                                    <td class="text1"><input type="text" name="roadTaxRemarks" id="roadTaxRemarks" class="form-control" style="width:240px;height:40px" /></td>
                                </tr>
                            </table>
                            <br>
                            <center>
                                <!--Upload Copy Of Bills&emsp;<input type="file" />
                                <br>-->
                                <br>
                                <input type="button" value="Update" name="generate" id="generate" class="btn btn-success" onclick="submitPage();">
                                                <input type="reset" class="btn btn-success" value=" Clear " />
                            </center>
                        </div>
                    </td>
                </tr>
            </table>

        </form>
    </body>
    <script type="text/javascript">
        function getVehicleNos(){
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
            //alert("call ajax");
            //getVehicleDetails(document.getElementById("regno").value);
        }
    </script>

</div>
	</div>
        </div>
        <%@ include file="/content/common/NewDesign/settings.jsp" %>
