<%-- 
    Document   : searchVehicleInsurance
    Created on : May 31, 2016, 10:58:22 PM
    Author     : hp
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        
           <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">

            function show_src() {
                document.getElementById('exp_table').style.display = 'none';
            }

            function show_exp() {
                document.getElementById('exp_table').style.display = 'block';
            }

            function show_close() {
                document.getElementById('exp_table').style.display = 'none';
            }
        </script>
<script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
    </head>
    <!--setImages(1,0,0,0,0,0);-->
    <body onLoad="getVehicleNos();
            setImages(0, 0, 0, 0, 0, 0);
            setDefaultVals('<%= request.getAttribute("regNo")%>', '<%= request.getAttribute("typeId")%>', '<%= request.getAttribute("mfrId")%>', '<%= request.getAttribute("usageId")%>', '<%= request.getAttribute("groupId")%>');">
        <form name="viewVehicleDetails"  method="post" >
            <%@ include file="/content/common/path.jsp" %>


            <%@ include file="/content/common/message.jsp" %>


            <table  cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;width:auto">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:700px;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">View Vehicle Search</li>
                            </ul>
                            <div id="first">
                                <table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr>
                                        <td>Vehicle Number</td><td><input type="text" id="regno" name="regno" value='<c:out value="${regNo}" />' class="form-control" ></td>
                                        
                                        <td>Company Name</td><td><select class="form-control" name="companyId" id="companyId" width="200px">
                                                <option value="">---Select---</option>
                                                <c:if test = "${vendorListCompliance != null}" >
                                                    <c:forEach items="${vendorListCompliance}" var="list">
                                                        <option value='<c:out value="${list.vendorId}" />'><c:out value="${list.vendorName}" /></option>
                                                    </c:forEach >
                                                </c:if>
                                            </select>
                                             <script>
                                                 document.getElementById("companyId").value ='<c:out value="${companyId}"/>'
                                             </script>
                                        </td>
                                    </tr>
                                    
                                    <tr><td>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);" value=""</td>
                                        <td>To Date</td><td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);" value=""></td>
                                    </tr>
                                    <tr>
                                    <td colspan="2" align="right"><input type="button" class="button" name="Search" value="Search" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                    </tr>
<!--                                    <tr>
                                       

                                    </tr>-->
<!--                                    <tr align="center">
                                        <td colspan="3" align="right"><input type="button" class="button" name="Search" value="Search" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                       &nbsp;&nbsp;&nbsp;&nbsp; <td colspan="3"><input type="button" class="button" name="ExportExcel"  value="ExportExcel" onclick="submitPage(this.name);"></td>
                                    </tr>-->
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <br>

            <%
                        int index = 1;

            %>


            <c:if test="${vehiclesFCList == null }" >
                <br>
                <center><font color="red" size="2"> no records found </font></center>
            </c:if>
            <c:if test="${vehiclesFCList != null }" >
                <table align="center" border="0" id="table" class="sortable" style="width:auto">
                    <thead>
                        <tr height="50">
                            <th  style="width: 10px"><h3>Sno</h3></th>
                            <th><h3>Trailer Number</h3></th>
                            <th><h3>Company Name</h3></th>
                            <th><h3>FC Date</h3></th>
                            <th><h3>FC Renewable Date</h3></th>
                            <th><h3>RTO Details</h3></th>
                            <th><h3>FC Amount</h3></th>
                            <th><h3>Elapsed Days</h3></th>
                            <th><h3>FC Action</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                    <%
                    String style = "text1";%>
                    <c:forEach items="${vehiclesFCList}" var="veh" >
                        <%
                                    if ((index % 2) == 0) {
                                        style = "text1";
                                    } else {
                                        style = "text2";
                                    }%>
                        <tr>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "> <%= index++%> </td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "> <c:out value="${veh.regNo}" /> </td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "> <c:out value="${veh.companyId}" /> </td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${veh.fcDate}" /></td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${veh.fcExpiryDate}" /></td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${veh.fcAmount}" /></td>
                            <td class="<%= style%>" height="30" style="padding-left:30px; "><c:out value="${veh.rtoDetail}" /></td>
                            <c:if test="${veh.elapsedDays < '0' }">
                            <td  class="<%= style%>" height="30" style="padding-left:30px;"><font color="red" size="2"> <c:out value="${veh.elapsedDays}" /> </font></td>
                            </c:if>
                            <c:if test="${veh.elapsedDays >= '0' }">
                            <td  class="<%= style%>" height="30" style="padding-left:30px;"><font color="green" size="2"> <c:out value="${veh.elapsedDays}" /> </font></td>
                            </c:if>
                            <td class="<%= style%>" height="30" style="padding-left:50px; ">
                                <a href='/throttle/trailerDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&editStatus=1&fleetTypeId=2&listId=3' >
                               <img src="/throttle/images/addlogos.png" height="20px;"/> </a>
<!--                                <a href='/throttle/trailerDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&editStatus=0&fleetTypeId=2&listId=3'  >view </a>
                                &nbsp;
                                &nbsp;-->
                            </td>
                        </tr>
                    </c:forEach>
                        </tbody>
                </table>
            </c:if>
            <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 7);
        </script>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script type="text/javascript">
        function submitPage(value) {
         
            if (value == 'add') {
                document.viewVehicleDetails.action = '/throttle/addVehiclePage.do?fleetTypeId=1';
                document.viewVehicleDetails.submit();
            } else {
                if (value == 'ExportExcel') {
                  
                    document.viewVehicleDetails.action = '/throttle/vehicleList.do?param=ExportExcel';
                document.viewVehicleDetails.submit();
                } else {
                    document.viewVehicleDetails.action = '/throttle/searchTrailerPage.do?listId=3';
                    document.viewVehicleDetails.submit();
            }
        }
        }


        function setDefaultVals(regNo, typeId, mfrId, usageId, groupId) {

            if (regNo != 'null') {
                document.viewVehicleDetails.regNo.value = regNo;
            }
            if (typeId != 'null') {
                document.viewVehicleDetails.typeId.value = typeId;
            }
            if (mfrId != 'null') {
                document.viewVehicleDetails.mfrId.value = mfrId;
            }
            if (usageId != 'null') {
                document.viewVehicleDetails.usageId.value = usageId;
            }
            if (groupId != 'null') {
                document.viewVehicleDetails.groupId.value = groupId;
            }
        }

       function getVehicleNos() {
            //onkeypress='getList(sno,this.id)'
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
        }

    </script>
</html>


