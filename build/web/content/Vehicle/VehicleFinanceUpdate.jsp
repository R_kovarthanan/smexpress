<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%--<%@page contentType="text/html" import="java.sql.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>--%>
<!DOCTYPE html>

    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <title>Vehicle Finance Update</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
        <%@page import="java.util.Date"%>
        <%@page import="java.text.SimpleDateFormat"%>
        <%@page import="java.text.DateFormat"%>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
         <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
       <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
        
     <script type="text/javascript">  

      function alphanumeric_only(e) {

            var keycode;
            if (window.event) keycode = window.event.keyCode;
            else if (event) keycode = event.keyCode;
            else if (e) keycode = e.which;

            else return true;
           if ((keycode >= 47 && keycode <= 57) || (keycode >= 65 && keycode <= 90) || (keycode >= 95 && keycode <= 122)) {

                return true;
            }

            else {
                alert("Please do not use special characters")
                return false;
            }

            return true;
        }

</script>

        <script type="text/javascript">
            //auto com

            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#regno').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getRegistrationNo.do",
                            dataType: "json",
                            data: {
                                regno: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#vehicleId').val(tmp[0]);
                        $('#regno').val(tmp[1]);
                        getVehicleDetails(tmp[1]);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };
            });


        </script>
 <script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script> 

        <script type="text/javascript">

            function show_src() {
                document.getElementById('exp_table').style.display = 'none';
            }
            function show_exp() {
                document.getElementById('exp_table').style.display = 'block';
            }
            function show_close() {
                document.getElementById('exp_table').style.display = 'none';
            }



            function validate() {
                var errMsg = "";
                if (isEmpty(document.VehicleFinance.regNo.value)) {
                    errMsg = errMsg + "Vehicle Registration No is not filled\n";
                }
                if (isEmpty(document.VehicleFinance.bankerName.value)) {
                    errMsg = errMsg + "Banker Name is not filled\n";
                }
                if (isEmpty(document.VehicleFinance.bankerAddress.value)) {
                    errMsg = errMsg + "Banker Address is not filled\n";
                }
                if (isEmpty(document.VehicleFinance.financeAmount.value)) {
                    errMsg = errMsg + "Finance Amount is not filled\n";
                }
                if (isEmpty(document.VehicleFinance.roi.value)) {
                    errMsg = errMsg + "Rate Of Interest is not filled\n";
                }
                if (isEmpty(document.VehicleFinance.emiMonths.value)) {
                    errMsg = errMsg + "EMI Total Months is not filled\n";
                }
                if (isEmpty(document.VehicleFinance.emiAmount.value)) {
                    errMsg = errMsg + "EMI Amount is not filled\n";
                }
                if (isEmpty(document.VehicleFinance.emiStartDate.value)) {
                    errMsg = errMsg + "EMI Start Date is not filled\n";
                }
                if (isEmpty(document.VehicleFinance.emiEndDate.value)) {
                    errMsg = errMsg + "EMI End Date is not filled\n";
                }

                if (errMsg != "") {
                    alert(errMsg);
                    return false;
                } else {
                    return true;
                }
            }


            var httpRequest;
            function getVehicleDetails(regNo)
            {
                //alert("ajax");
                if (regNo != "") {
                    var url = "/throttle/getVehicleDetailsInsurance.do?regNo1=" + regNo;
                    url = url + "&sino=" + Math.random();
                    if (window.ActiveXObject) {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest) {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() {
                        processRequest();
                    };
                    httpRequest.send(null);
                }
            }


            function processRequest()
            {
                if (httpRequest.readyState == 4) {

                    if (httpRequest.status == 200) {
                        if (httpRequest.responseText.valueOf() != "") {
                            var detail = httpRequest.responseText.valueOf();
                            if (detail != "null") {
                                var vehicleValues = detail.split("~");
                                document.VehicleFinance.vehicleId.value = vehicleValues[0];
                                document.VehicleFinance.chassisNo.value = vehicleValues[1];
                                document.VehicleFinance.engineNo.value = vehicleValues[2];
                                document.VehicleFinance.vehicleMake.value = vehicleValues[4];
                                document.VehicleFinance.vehicleModel.value = vehicleValues[5];
                                if (parseInt(vehicleValues[6]) > 0) {
                                    document.getElementById('exMsg').innerHTML = "Vehicle Finance Entry is Already Existing";
                                    document.getElementById('detail').style.display = "none";
                                }
                            } else {
                                document.VehicleFinance.vehicleId.value = "";
                                document.VehicleFinance.chassisNo.value = "";
                                document.VehicleFinance.engineNo.value = "";
                                document.VehicleFinance.vehicleMake.value = "";
                                document.VehicleFinance.vehicleModel.value = "";
                                document.getElementById('exMsg').innerHTML = "";
                            }

                        }
                    }
                    else
                    {
                        alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                    }
                }
            }

            /*var httpRequest1;
             function getVehicleExisting(regNo)
             {
             //alert("Existing ajax");
             if(regNo != "") {
             var url = "/throttle/getVehicleExisting.do?regNo1="+ regNo;
             url = url+"&sino="+Math.random();
             if (window.ActiveXObject)  {
             httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
             }
             else if (window.XMLHttpRequest)  {
             httpRequest1 = new XMLHttpRequest();
             }
             httpRequest1.open("GET", url, true);
             httpRequest1.onreadystatechange = function() { processRequest1(); } ;
             httpRequest1.send(null);
             }
             }
             
             
             function processRequest1() {
             if (httpRequest1.readyState == 4)  {
             if(httpRequest1.status == 200) {
             if(httpRequest1.responseText.valueOf()!=""){
             var detail = httpRequest1.responseText.valueOf();
             if(detail != "null"){
             document.VehicleFinance.exMsg.innerHTML = detail;
             }else{
             document.VehicleFinance.exMsg.innerHTML = "";
             }
             
             }
             }
             else
             {
             alert("Error loading page\n"+ httpRequest1.status +":"+ httpRequest1.statusText);
             }
             }
             }*/


            function submitPage()
            {
                if (textValidation(document.VehicleFinance.regno, 'Vehicle No'))
                    ;
                else {
                    document.VehicleFinance.action = '/throttle/saveVehicleFinance.do';
                    document.VehicleFinance.submit();
                }
            }


            function getEvents(e, val) {
                var key;
                if (window.event) {
                    key = window.event.keyCode;
                } else {
                    key = e.which;
                }
                if (key == 0) {
                    getVehicleDetails(val);
                    //getVehicleExisting(val);
                } else {
                    getVehicleDetails(val);
                    //getVehicleExisting(val);
                }
            }


        </script>

    </head>

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Vehicle Finance" text="Vehicle Finance"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Vehicle" text="Vehicle"/></a></li>
                <li class=""><spring:message code="hrms.label.Vehicle Finance" text="Vehicle Finance"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">

                <body onLoad="getVehicleNos();
                        document.VehicleInsurance.regno.focus();">
                    <form name="VehicleFinance" method="post">
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <!-- pointer table -->
                        <!-- message table -->
                        <%@ include file="/content/common/message.jsp"%>
                        <!--            <span id="exMsg" style="color: green; text-align: center; font-weight: bold;"></span>-->
                        <!--<h2 align="center">Vehicle Finance Details</h2>-->
                        <!--            <table class="table table-info mb30 table-hover" id="bg"   >
                                            <thead>
                                        <tr>
                                            <th colspan="4" height="30" >Vehicle Finance Details</th>
                                        </tr>
                                            </thead>
                                    </table>-->

                        <table class="table table-info mb30 table-hover">
                            <thead>
                                <tr>
                                    <th  colspan="4">Vehicle Details</th>
                                </tr>
                            </thead>
                            <tr>
                                <td >Vehicle No</td>
                                <td >
                                    <input type="text" name="regNo" id="regno" class="form-control" onkeypress="return alphanumeric_only(this);" maxlength="12" onclick="getVehicleDetails(this.value);
                                            getVehicleExisting(this.value);" autocomplete="off" style="width:240px;height:40px"  />
                                    <input type="hidden" name="vPAge" id="vPAge" class="form-control" value="Finance" />
                                    <input type="hidden" name="vehicleId" id="vehicleId" value=""/></td>
                                <td >Make</td>
                                <td ><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" style="width:240px;height:40px"  readonly /></td>
                            </tr>
                            <tr>
                                <td >Model</td>
                                <td ><input type="text" name="vehicleModel" id="vehicleModel" readonly class="form-control" onclick="getVehicleDetails(regno.value);" readonly style="width:240px;height:40px"  /></td>
                                <td >Usage</td>
                                <td ><input type="text" name="vehicleUsage" id="vehicleUsage" readonly class="form-control" onclick="getVehicleDetails(regno.value);" readonly style="width:240px;height:40px"  /></td>
                            </tr>
                            <tr>
                                <td >Engine No</td>
                                <td ><input type="text" name="engineNo" id="engineNo" readonly class="form-control" onclick="getVehicleDetails(regno.value);" readonly  style="width:240px;height:40px" /></td>
                                <td >Chassis No</td>
                                <td ><input type="text" name="chassisNo" id="chassisNo" readonly class="form-control" onclick="getVehicleDetails(regno.value);" readonly  style="width:240px;height:40px" /></td>
                            </tr>
                            <tr>
                                <td  colspan="4">
                            <center><label id="exMsg" style="color: green; text-align: center; font-weight: bold; font-size: medium;"></label></center>
                            </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    <div id="detail" style="display: block;">
                                        <table class="table table-info mb30 table-hover">
                                            <thead>
                                                <tr>
                                                    <th  colspan="4">Finance Details</th>
                                                </tr>
                                            </thead>
                                            <tr>
                                                <td >Banker</td>
                                                <td ><input type="text" name="bankerName" id="bankerName" class="form-control" onclick="getVehicleDetails(regno.value);" style="width:240px;height:40px"  /></td>
                                                <td >Banker Address</td>
                                                <td ><textarea name="bankerAddress" class="form-control" style="width:240px;height:40px" ></textarea></td>

                                            </tr>
                                            <tr>
                                                <td >Finance Amount</td>
                                                <td ><input type="text" name="financeAmount" id="financeAmount" class="form-control" style="width:240px;height:40px"  /></td>
                                                <td >&nbsp;</td>
                                                <td >&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td >Rate of Interest</td>
                                                <td ><input type="text" name="roi" id="roi" class="form-control" style="width:240px;height:40px"  /></td>
                                                <td >Interest Type</td>
                                                <td >
                                                    <select name="interestType" class="form-control" style="width:240px;height:40px" >
                                                        <option value="Simple Interest">Simple Interest</option>
                                                        <option value="Compound Interest">Compound Interest</option>
                                                        <option value="Diminishing Balance Interest">Diminishing Balance Interest</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >EMI Total Months</td>
                                                <td ><input type="text" name="emiMonths" id="emiMonths" class="form-control" style="width:240px;height:40px"  /></td>
                                                <td >EMI Amount</td>
                                                <td ><input type="text" name="emiAmount" id="emiAmount" class="form-control" style="width:240px;height:40px"  /></td>
                                            </tr>
                                            <tr>
                                                <td >EMI Start Date</td>
                                                <td ><input type="text" name="emiStartDate" id="emiStartDate" class="datepicker form-control"  style="width:240px;height:40px" /><!--&nbsp;<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.VehicleFinance.premiumPaidDate,'dd-mm-yyyy',this)"/>--></td>
                                                <td >EMI End Date</td>
                                                <td ><input type="text" name="emiEndDate" id="emiEndDate" class="datepicker form-control" style="width:240px;height:40px"  /><!--&nbsp;<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.VehicleFinance.premiumPaidDate,'dd-mm-yyyy',this)"/>--></td>
                                            </tr>
                                            <tr>
                                                <td >EMI Monthly Pay By Date</td>
                                                <td >
                                                    <select name="emiPayDay" class="form-control" style="width:240px;height:40px" >
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="10">11</option>
                                                        <option value="12">12</option>
                                                        <option value="13">13</option>
                                                        <option value="14">14</option>
                                                        <option value="15">15</option>
                                                        <option value="16">16</option>
                                                        <option value="17">17</option>
                                                        <option value="18">18</option>
                                                        <option value="19">19</option>
                                                        <option value="20">20</option>
                                                        <option value="21">21</option>
                                                        <option value="22">22</option>
                                                        <option value="23">23</option>
                                                        <option value="24">24</option>
                                                        <option value="25">25</option>
                                                        <option value="26">26</option>
                                                        <option value="27">27</option>
                                                        <option value="28">28</option>
                                                        <option value="29">29</option>

                                                    </select>
                                                </td>
                                                <td >Pay Mode</td>
                                                <td >
                                                    <select name="payMode" class="form-control" style="width:240px;height:40px" >
                                                        <option value="ECS">ECS</option>
                                                        <option value="PDC">PDC</option>
                                                        <option value="PDC">RTGS</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </table>

                                        <br>
                                        <center>
                                            <input type="button" value="Update" name="generate" id="generate" class="btn btn-success" onclick="submitPage();">
                                            <input type="reset" class="btn btn-success" value=" Clear " />
                                        </center>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
                <script type="text/javascript">
                    function getVehicleNos() {
                        var oTextbox = new AutoSuggestControl(document.getElementById("regno"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
                        //alert("call ajax");
                        //getVehicleDetails(document.getElementById("regno").value);
                    }
                </script>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>  
