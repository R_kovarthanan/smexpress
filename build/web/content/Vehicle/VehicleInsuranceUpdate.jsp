<%@ include file="/content/common/NewDesign/header.jsp" %>
	<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <title>Vehicle Insurance Update</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        
        <script type="text/javascript">
            //auto com

            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#regno').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getRegistrationNo.do",
                            dataType: "json",
                            data: {
                                regno: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#vehicleId').val(tmp[0]);
                        $('#regno').val(tmp[1]);
                        getVehicleDetails(tmp[1]);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };
            });


        </script>

        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>

        <script type="text/javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }
            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }
            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }



            function validate(){
                var errMsg = "";
                if(document.VehicleInsurance.regNo.value){
                    errMsg = errMsg+"Vehicle Registration No is not filled\n";
                }
                if(document.VehicleInsurance.insuranceCompanyName.value){
                    errMsg = errMsg+"Insurance Company is not filled\n";
                }
                if(document.VehicleInsurance.premiumNo.value){
                    errMsg = errMsg+"Premium No is not filled\n";
                }
                if(document.VehicleInsurance.premiumPaidAmount.value){
                    errMsg = errMsg+"Premium paid Amount is not filled\n";
                }
                if(document.VehicleInsurance.premiumPaidDate.value){
                    errMsg = errMsg+"Premium paid Date is not filled\n";
                }
                if(document.VehicleInsurance.vehicleValue.value){
                    errMsg = errMsg+"Vehicle Value is not filled\n";
                }
                if(document.VehicleInsurance.premiumExpiryDate.value){
                    errMsg = errMsg+"Premium expiry Date is not filled\n";
                }
                if(errMsg != "") {
                    alert(errMsg);
                    return false;
                }else {
                    return true;
                }
            }


            var httpRequest;
            function getVehicleDetails(regNo)
            {
                
                // alert("ajax");
                // alert(regNo);
                if(regNo != "") {
                    var url = "/throttle/getVehicleDetailsInsurance.do?regNo1="+ regNo;
                    url = url+"&sino="+Math.random();
                    if (window.ActiveXObject)  {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)  {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() { processRequest(); } ;
                    httpRequest.send(null);
                    
                }
                
            }


            function processRequest()
            {
                //alert("in proc");
                if (httpRequest.readyState == 4)  {

                    if(httpRequest.status == 200) {
                        if(httpRequest.responseText.valueOf()!=""){
                            var detail = httpRequest.responseText.valueOf();
                            //                            alert(detail);
                            if(detail != "null"){
                                var vehicleValues = detail.split("~");
                                document.VehicleInsurance.vehicleId.value = vehicleValues[0];
                                document.VehicleInsurance.chassisNo.value = vehicleValues[1];
                                document.VehicleInsurance.engineNo.value = vehicleValues[2];
                                document.VehicleInsurance.vehicleMake.value = vehicleValues[4];
                                document.VehicleInsurance.vehicleModel.value = vehicleValues[5];
                                if(parseInt(vehicleValues[7]) > 0){
                                    document.getElementById('exMsg').innerHTML="Vehicle Insurance Entry is Already Existing";
                                    document.getElementById('detail').style.display="none";
                                }
                                // alert(vehicleValues[8]);
                            }else {
                                document.VehicleInsurance.vehicleId.value = "";
                                document.VehicleInsurance.chassisNo.value = "";
                                document.VehicleInsurance.engineNo.value = "";
                                document.VehicleInsurance.vehicleMake.value = "";
                                document.VehicleInsurance.vehicleModel.value = "";
                                document.getElementById('exMsg').innerHTML = "";
                            }
                        }
                       
                    }

                    else
                    {
                        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                    }
                }
               
            }


            function submitPage()
            {
                if(textValidation(document.VehicleInsurance.regno,'Vehicle No'));
                else{
                    document.VehicleInsurance.action = '/throttle/saveVehicleInsurance.do';
                    document.VehicleInsurance.submit();
                }
            }


            function getEvents(e,val){
                var key;
                if(window.event){
                    key = window.event.keyCode;
                }else {
                    key = e.which;
                }
                if(key == 0) {
                    getVehicleDetails(val);
                }else{
                    getVehicleDetails(val);
                }
            }

        </script>

    </head>
    
    
      <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.VehicleInsurance" text="VehicleInsurance"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Vehicle" text="Vehicle"/></a></li>
		                    <li class=""><spring:message code="hrms.label.VehicleInsurance" text="VehicleInsurance"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">

    <body onLoad="getVehicleNos();document.VehicleInsurance.regno.focus();">
        <form name="VehicleInsurance" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>


            <%@ include file="/content/common/message.jsp" %>
            <!--<h2 align="center">Vehicle Insurance Details</h2>-->
            <table width="800" align="center" class="table table-info mb30 table-hover" cellpadding="0" cellspacing="0">
                <thead>
                <tr>
                    <th  colspan="4">Vehicle Details</th>
                </tr>
                </thead>
                <tr>
                    <td >Vehicle No</td>
                    <td >
                        <input type="text" name="regNo" id="regno" class="form-control" onkeypress="getEvents(event,this.value);" onblur="getVehicleDetails(this.value);" autocomplete="off" style="width:240px;height:40px" />
                        <!--                            <input type="text" name="regNo" id="regno" class="form-control"  onchange="getVehicleDetails(this.value);" onkeypress="getVehicleDetails(this.value);" autocomplete="off" />-->
                        <input type="hidden" name="vehicleId" id="vehicleId" /></td>
                    <td >Make</td>
                    <td ><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" readonly style="width:240px;height:40px" /></td>
                </tr>
                <tr>
                    <td >Model</td>
                    <td ><input type="text" name="vehicleModel" id="vehicleModel" class="form-control" readonly style="width:240px;height:40px" /></td>
                    <td >Usage</td>
                    <td ><input type="text" name="vehicleUsage" id="vehicleUsage" class="form-control" readonly style="width:240px;height:40px" /></td>
                </tr>
                <tr>
                    <td >Engine No</td>
                    <td ><input type="text" name="engineNo" id="engineNo" class="form-control" readonly style="width:240px;height:40px"/></td>
                    <td >Chassis No</td>
                    <td ><input type="text" name="chassisNo" id="chassisNo" class="form-control" readonly style="width:240px;height:40px" /></td>
                </tr>
                <tr>
                    <td  colspan="4">
                        <center><label id="exMsg" style="color: green; text-align: center; font-weight: bold; font-size: medium;"></label></center>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <div id="detail" style="display: block;">
                            <table width="100%">
                                <thead>
                                <tr>
                                    <th  colspan="4">Insurance Details</th>
                                </tr>
                                 </thead>
                                <tr>
                                    <td >Insurance Company</td>
                                    <td ><input type="text" name="insuranceCompanyName" id="insuranceCompanyName" class="form-control" onclick="getVehicleDetails(regno.value);" style="width:240px;height:40px" /></td>
                                    <td >Premium No</td>
                                    <td ><input type="text" name="premiumNo" id="premiumNo" class="form-control" style="width:240px;height:40px" /></td>
                                </tr>
                                <tr>
                                    <td >Premium Amount</td>
                                    <td ><input type="text" name="premiumPaidAmount" id="premiumPaidAmount" class="form-control" style="width:240px;height:40px" /></td>
                                    <td >Premium Paid Date</td>
                                    <td ><input type="text" name="premiumPaidDate" id="premiumPaidDate" class="datepicker form-control" style="width:240px;height:40px" /><!--&nbsp;<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.VehicleInsurance.premiumPaidDate,'dd-mm-yyyy',this)"/>--></td>
                                </tr>
                                <tr>
                                    <td >Vehicle Value</td>
                                    <td ><input type="text" name="vehicleValue" id="vehicleValue" class="form-control" style="width:240px;height:40px" /></td>
                                    <td >Expiry Date</td>
                                    <td ><input type="text" name="premiumExpiryDate" id="premiumExpiryDate" class="datepicker form-control" style="width:240px;height:40px" /><!--&nbsp;<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.VehicleInsurance.premiumExpiryDate,'dd-mm-yyyy',this)"/>--></td>
                                </tr>
                                <tr>
                                    <td >NCB</td>
                                    <td ><input type="text" name="insuarnceNCB" id="insuarnceNCB" class="form-control"  style="width:240px;height:40px"/></td>
                                    <td >Remarks</td>
                                    <td ><input type="text" name="insuarnceRemarks" id="insuarnceRemarks" class="form-control" style="width:240px;height:40px" /></td>
                                </tr>

                                <tr>
                                    <td align="center" colspan="4"></td>
                                </tr>
                            </table>
                            <br>
                            <center>
<!--                                Upload Copy Of Bills&emsp;<input type="file" />
                                <br>
                                <br>-->
                                <input type="button" value="Update" name="generate" id="generate" class="btn btn-success" onclick="submitPage();">
                                                <input type="reset" class="btn btn-success" value=" Clear " />
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script type="text/javascript">
        function getVehicleNos(){
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
            //getVehicleDetails(document.getElementById("regno").value);        
        }
    </script>

</div>
	</div>
        </div>
        <%@ include file="/content/common/NewDesign/settings.jsp" %>
         
        