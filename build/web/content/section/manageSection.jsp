
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    </head>
    <script language="javascript">
        function submitPage(value)
        {
            if (value=='add')
                {
                    document.desigDetail.action ='/throttle/addSectionPage.do';
                }else if(value == 'modify'){
                
                document.desigDetail.action ='/throttle/alterSectionPage.do';
            }
            document.desigDetail.submit();
        }
    </script>
    
    <body>
        
        <form method="post" name="desigDetail">
             <%@ include file="/content/common/path.jsp" %>
      

<%@ include file="/content/common/message.jsp" %>

 <% int index = 0;  %>    
            <br>
            <c:if test = "${SectionList != null}" >
                <table width="600" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                    
 
                    <tr align="center">
                        <td height="30" class="contentsub"><div class="contentsub">S.No</div></td>
                        <td height="30" class="contentsub"><div class="contentsub">Section Code</div></td>
                        <td height="30" class="contentsub"><div class="contentsub">Section Name</div></td>
                        <td height="30" class="contentsub"><div class="contentsub">Description</div></td>
                        <td height="30" class="contentsub"><div class="contentsub">Service Type</div></td>
                        <td height="30" class="contentsub"><div class="contentsub">Status</div></td>
                       
                    </tr>
                    <%

                    %>
                    
                    <c:forEach items="${SectionList}" var="list"> 	
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr   > 
                            <td class="<%=classText %>" height="30"><%=index + 1%></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.sectionCode}"/></td>
                            <td class="<%=classText %>" height="30"><input type="hidden" name="sectionId" value='<c:out value="${list.sectionId}"/>'> <c:out value="${list.sectionName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${list.description}"/></td>
                            <td class="<%=classText %>" height="30">
                            <c:if test="${serviceTypeList != null}">
                            <c:forEach items="${serviceTypeList}" var="service">
                                <c:if test="${service.serviceTypeId == list.serviceTypeId}">
                                    <c:out value="${service.serviceTypeName}"/>
                                </c:if>
                                </c:forEach>
                            </c:if>
                            </td>
                            <td class="<%=classText %>" height="30">                                
                                <c:if test = "${list.activeInd == 'Y'}" >
                                    Active
                                </c:if>
                                <c:if test = "${list.activeInd == 'N'}" >
                                    InActive
                                </c:if>
                            </td>
                             
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    
                </table>
            </c:if> 
            <center>
                <br>
                <input type="button" name="add" value="Add" onClick="submitPage(this.name)" class="button">
                <c:if test = "${SectionList != null}" >
                    <input type="button" value="Alter" name="modify" onClick="submitPage(this.name)" class="button">
                </c:if>
                <input type="hidden" name="reqfor" value="">
            </center>
            <br>
        </form>
    </body>
</html>
