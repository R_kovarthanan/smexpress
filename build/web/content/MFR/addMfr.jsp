
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
     <%@page language="java" contentType="text/html; charset=UTF-8"%>
 <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>


<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<!--FOR google City Starts-->

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

<script type="text/javascript">
    function submitpage()
    {
       if(textValidation(document.dept.mfrName,"ManufactureName")){
           return;
       }
       if(textValidation(document.dept.description,"description")){
           return;
       }
       if(floatValidation(document.dept.perc,"Percentage")){
           return;
       }
       document.dept.action= "/throttle/addMfr.do";
       document.dept.submit();
    }
    
      
    

    function setValues(sno,mfrId,mfrName,bodyWorkGroupId,activityGroupId,description,activeInd,vehicleMfr,tyreMfr,trailerMfr,batteryMfr){
        var count = parseInt(document.getElementById("count").value);
        //document.getElementById('inActive').style.display = 'block';
        document.dept.status = activeInd;
        for (i = 1; i <= count; i++) {
            if(i != sno) {
                document.getElementById("edit"+i).checked = false;
            } else {
                document.getElementById("edit"+i).checked = true;
            }
        }
        document.getElementById("mfrId").value = mfrId;
        document.getElementById("mfrName").value = mfrName;
        document.getElementById("bodyGroupId").value = bodyWorkGroupId;
        document.getElementById("groupId").value = activityGroupId;
        document.getElementById("description").value = description;
        if(vehicleMfr == 1) {
               document.getElementById("vehicleMfr").checked = true;
            } else {
                document.getElementById("vehicleMfr").checked = false;
            }
        if(tyreMfr == 1) {
               document.getElementById("tyreMfr").checked = true;
            } else {
                document.getElementById("tyreMfr").checked = false;
            }
        if(trailerMfr == 1) {
               document.getElementById("trailerMfr").checked = true;
            } else {
                document.getElementById("trailerMfr").checked = false;
            }
        if(batteryMfr == 1) {
               document.getElementById("batteryMfr").checked = true;
            } else {
                document.getElementById("batteryMfr").checked = false;
            }
        document.getElementById("activeInd").value = activeInd;
//        document.getElementById("vehicleMfr").checked = vehicleMfr;
    }
</script>


<div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.Mfr"  text="Make"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="trucks.label.FleetName"  text="Fleet"/></a></li>
          <li class="active"><spring:message code="stores.label.Mfr"  text="Make"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
<body>
<form name="dept"  method="post">
 
<%@ include file="/content/common/message.jsp" %>

<table class="table table-info mb30 table-hover">
    <thead>
<tr>
<th colspan="2"  ><div ><spring:message code="trucks.label.AddManufacturer"  text="default text"/></div></th>
</tr>
</thead>
<tr>
<td  ><font color=red>*</font><spring:message code="trucks.label.ManufacturerName"  text="default text"/></td>
<td  ><input name="mfrName" maxlength="35" id="mfrName" type="text" class="form-control" style="width:260px;height:40px;" ></td>
</tr>

<input name="perc" maxlength="10" type="hidden" class="form-control" style="width:260px;height:40px;" value="0">
<tr>

<td  ><font color=red>*</font><spring:message code="trucks.label.Description"  text="default text"/> </td>
<td  ><textarea class="form-control" style="width:260px;height:40px;" id="description" name="description"></textarea></td>
</tr>
<tr style="display:none;">
<td  ><font color=red>*</font><spring:message code="trucks.label.ActivityGroup"  text="default text"/></td>
<td  >
    <select  style='width:200px;' class="form-control" style="width:260px;height:40px;" id="groupId"  name="groupId" >
    <option  value=0>---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
    <option  selected value=1><spring:message code="trucks.label.VolvoGroup"  text="default text"/></option>
    <option  value=2><spring:message code="trucks.label.NonVolvoGroup"  text="default text"/></option>
    </select>
</td>
</tr>
<tr style="display:none;" >
<td  ><font color=red>*</font><spring:message code="trucks.label.BodyWorksGroup"  text="default text"/></td>
<td  >
    <select  style='width:200px;' class="form-control" style="width:260px;height:40px;" id="bodyGroupId"  name="bodyGroupId" >
    <option  value=0>---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
    <option  value=3><spring:message code="trucks.label.Cargo"  text="default text"/></option>
    <option  value=4><spring:message code="trucks.label.HighEnd(Luxury)"  text="default text"/></option>
    <option  selected value=5><spring:message code="trucks.label.Ordinary"  text="default text"/></option>
    </select>
</td>
</tr>
<tr>
    <td ><spring:message code="trucks.label.Status"  text="default text"/></td>
    <td >
        <select   class="form-control" style="width:260px;height:40px;" name="status" >
            <option value='Y'><spring:message code="trucks.label.Active"  text="default text"/></option>
            <option value='N'><spring:message code="trucks.label.InActive"  text="default text"/></option>
        </select>
    </td>
</tr>

</table>
<table class="table table-bordered">
<tr>
    <td  ><spring:message code="trucks.label.Vehicle"  text="default text"/></td>
    <td  ><input type="checkbox" name="vehicleMfr" id="vehicleMfr" value="1"  style="margin-left: -20px"/>
    </td>
    <td  ><spring:message code="trucks.label.Trailer"  text="default text"/></td>
    <td  ><input type="checkbox" name="trailerMfr" id="trailerMfr" value="1"  style="margin-left: -20px"/>
    </td>

<!--    <td  ><spring:message code="trucks.label.Tyre"  text="default text"/></td>
    <td  ><input type="checkbox" name="tyreMfr" id="tyreMfr" value="1"  style="margin-left: -20px"/>
    </td>
    
    <td  ><spring:message code="trucks.label.Battery"  text="default text"/></td>
    <td  ><input type="checkbox" name="batteryMfr" id="batteryMfr" value="1"  style="margin-left: -20px"/>
    </td>-->
</tr>
</table>
<center>
<input type="button" value="<spring:message code="trucks.label.Save"  text="default text"/>" class="btn btn-success" onclick="submitpage();">
&emsp;<input type="reset" class="btn btn-success" value="<spring:message code="trucks.label.Clear"  text="default text"/>">
</center>
<br>

<table class="table table-info mb30 table-hover " id="table">

                <thead>

                    <tr >
                        <th><spring:message code="trucks.label.SNo"  text="default text"/></th>
                        <th><spring:message code="trucks.label.ManufactureName"  text="default text"/> </th>
                        <th><spring:message code="trucks.label.ActivityGroup"  text="default text"/> </th>
                        <th><spring:message code="trucks.label.BodyWorksGroup"  text="default text"/></th>
                        <th><spring:message code="trucks.label.Description"  text="default text"/> </th>
                        <th><spring:message code="trucks.label.Status"  text="default text"/></th>
                        <th align="right">&emsp;&emsp;<spring:message code="trucks.label.Edit"  text="default text"/></th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
                    <c:if test = "${MfrList != null}">
                        <c:forEach items="${MfrList}" var="cml">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td   align="left"> <%= sno%> </td>
                                <td   align="left"><c:out value="${cml.mfrName}"/></td>
                                <td class="<%=className %>" align="left">                                
                                <c:if test = "${cml.activityGroupId == 1}" >
                                   <spring:message code="trucks.label.VolvoGroup"  text="default text"/> 
                                </c:if>
                                <c:if test = "${cml.activityGroupId == 2}" >
                                   <spring:message code="trucks.label.NonVolvoGroup"  text="default text"/> 
                                </c:if>
                                </td>
                                <td class="<%=className %>" >
                                <c:if test = "${cml.bodyWorkGroupId == 3}" >
                                    <spring:message code="trucks.label.Cargo"  text="default text"/>
                                </c:if>
                                <c:if test = "${cml.bodyWorkGroupId == 4}" >
                                   <spring:message code="trucks.label.HighEnd(Luxury)"  text="default text"/> 
                                </c:if>
                                <c:if test = "${cml.bodyWorkGroupId == 5}" >
                                    <spring:message code="trucks.label.Ordinary"  text="default text"/>
                                </c:if>
                                </td>
                                <td   align="left"> <c:out value="${cml.description}" /></td>
                                <td   align="left"><c:out value="${cml.activeInd}"/></td>
                                <td  align="left"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${cml.mfrId}" />', '<c:out value="${cml.mfrName}" />', '<c:out value="${cml.bodyWorkGroupId}" />', '<c:out value="${cml.activityGroupId}" />', '<c:out value="${cml.description}" />', '<c:out value="${cml.activeInd}"/>','<c:out value="${cml.vehicleMfr}"/>','<c:out value="${cml.tyreMfr}"/>','<c:out value="${cml.trailerMfr}"/>','<c:out value="${cml.batteryMfr}"/>' );" /></td>
                            </tr>
                                
                      </c:forEach>
                    </tbody>
                    <input type="hidden" name="count" id="count" value="<%=sno%>" />
                </c:if>
                <input type="hidden" name="mfrId" id="mfrId" > 
            </table>
                    <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span><spring:message code="trucks.label.EntriesPerPage"  text="default text"/></span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text"><spring:message code="trucks.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="trucks.label.of"  text="default text"/> <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
            <br>
            <!--<div id="myMap" style="width: 1000px; height: 400px; margin-top:20px;"></div>-->
           
</body>
</div>


    </div>
    </div>





<%@ include file="/content/common/NewDesign/settings.jsp" %>