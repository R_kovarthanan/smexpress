<%-- 
    Document   : alterLedger
    Created on : 25 Oct, 2012, 6:58:30 PM
    Author     : ASHOK
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Ledger Alter</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>

        <script language="javascript" src="/throttle/js/validate.js"></script>
    </head>
    <script>
        function submitPage()
        {

            if(textValidation(document.alter.ledgerName,'Ledger Name')){
                return;
            }
            document.alter.action='/throttle/saveAlterLedger.do';
            document.alter.submit();
        }


    </script>
    <body>
        <form name="alter" method="post">
            <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>
            <c:if test="${ledgeralterList != null}">
                <c:forEach items="${ledgeralterList}" var="LL">
                    <table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="border">
                        <tr height="30">
                            <Td colspan="2" class="contenthead">Edit Ledger</Td>
                        </tr>
                        <tr height="30">
                            <td class="text2"><font color="red">*</font>Ledger Name</td>
                            <td class="text2"><input name="ledgerName" type="text" class="textbox" value="<c:out value="${LL.ledgerName}"/>" maxlength="10" size="20">
                                <input type="hidden" name="ledgerID" value="<c:out value="${LL.ledgerID}"/>"> </td>
                        </tr>
                        <tr height="30">
                            <td class="text1"><font color="red">*</font>Group Code</td>
                            <td class="text1">
                                <select name="groupCode" class="textbox" style="width:125px" >
                                    <c:if test="${groupLists!=null}">
                                        <c:forEach items="${groupLists}" var="groupLists" >
                                            <c:choose>
                                                <c:when test="${groupLists.groupcode==LL.groupCode}" >
                                                    <option value="<c:out value="${groupLists.groupcode}"/>" selected > <c:out value="${groupLists.groupname}"/> </option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="<c:out value="${groupLists.groupcode}"/>" > <c:out value="${groupLists.groupname}"/> </option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="text2"><font color="red">*</font>Amount Type</td>
                            <td class="text2">
                                <select name="amountType" class="textbox" style="width:125px" >
                                    <c:choose>
                                        <c:when test="${LL.amountType =='CREDIT'}" >
                                            <option value="CREDIT" selected > CREDIT </option>
                                            <option value="DEBIT" > DEBIT </option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="CREDIT" > CREDIT </option>
                                            <option value="DEBIT" selected > DEBIT </option>
                                        </c:otherwise>
                                    </c:choose>
                                </select>
                            </td>
                        </tr>

                    </table>
                </c:forEach>
            </c:if>

            <br>
            <br>
            <center><input type="button" class="button" value="Save" onclick="submitPage();" /></center>
        </form>
    </body>
</html>

