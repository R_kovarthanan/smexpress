<%-- 
    Document   : alterState
    Created on : 8 Nov, 2012, 11:08:28 AM
    Author     : ASHOK
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Ledger Alter</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>

        <script language="javascript" src="/throttle/js/validate.js"></script>
    </head>
    <script>
        function submitPage()
        {
//            alert("1");

            if(textValidation(document.alter.stateName,'stateName')){
                return;
            }
            if(textValidation(document.alter.stateCode,'stateCode')){
                return;
            }
            if(textValidation(document.alter.description,'description')){
                return;
            }
//            alert("2");

            document.alter.action='/throttle/saveAlterState.do';
            document.alter.submit();
        }


    </script>
    <body>
        <form name="alter" method="post">
            <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>
            <c:if test="${statealterList != null}">
                <c:forEach items="${statealterList}" var="SL">
                    <table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="border">
                        <tr height="30">
                            <Td colspan="2" class="contenthead">Edit State</Td>
                        </tr>
                        <tr height="30">
                            <td class="text2"><font color="red">*</font>Name</td>
                            <td class="text2"><input name="stateName" type="text" class="textbox" value="<c:out value="${SL.stateName}"/>" maxlength="10" size="20">
                                <input type="hidden" name="stateID" value="<c:out value="${SL.stateID}"/>"> </td>
                        </tr>
                        <tr height="30">
                            <td class="text2"><font color="red">*</font>Code</td>
                            <td class="text2"><input name="stateCode" type="text" class="textbox" value="<c:out value="${SL.stateCode}"/>" maxlength="15" size="20"></td>
                        </tr>
                        <tr height="30">
                            <td class="text1"><font color="red">*</font>Country</td>
                            <td class="text1">
                                <select name="countryID" class="textbox" style="width:125px" >
                                    <c:if test="${CountryLists!=null}">
                                        <c:forEach items="${CountryLists}" var="CL" >
                                            <c:choose>
                                                <c:when test="${CL.countryID==SL.countryID}" >
                                                    <option value="<c:out value="${CL.countryID}"/>" selected > <c:out value="${CL.countryName}"/> </option>
                             </c:when>
                          <c:otherwise>
                         <option value="<c:out value="${CL.countryID}"/>" > <c:out value="${CL.countryName}"/> </option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="text2"><font color="red">*</font>Description</td>
                            <td class="text2"><input name="description" type="text" class="textbox" value="<c:out value="${SL.description}"/>" maxlength="10" size="20"></td>
                        </tr>
                    </table>
                </c:forEach>
            </c:if>

            <br>
            <br>
            <center><input type="button" class="button" value="Save" onclick="submitPage();" /></center>
        </form>
    </body>
</html>

