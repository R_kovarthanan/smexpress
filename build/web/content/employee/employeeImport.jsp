<%-- 
    Document   : tripPlanningImport
    Created on : Nov 4, 2013, 11:17:44 PM
    Author     : Throttle
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE html>
<!--<html>-->
    <!--<head>-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <title></title>
        <script>
            function submitPage() {
                document.employee.action = '/throttle/handleUploadEmployee.do';
                document.employee.submit();
            }
        </script>
    <!--</head>-->
      <style>
    #index td {
   color:white;
}
</style>
    <% 
    //String menuPath = "Operations >>  Upload Trip Planning";
        //request.setAttribute("menuPath", menuPath);
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String curDate = dateFormat.format(date);
    %>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ManageEmployee" text="default text"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
                <li class="active"><spring:message code="hrms.label.ManageEmployee" text="default text"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
    <body>
        <form name="employee" method="post" enctype="multipart/form-data">
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>
            <table class="table table-info mb30 table-hover" border="0" cellpadding="0" cellspacing="0" width="780" align="center">
                <tr id="index">
                    <td colspan="4" style="background-color:#5BC0DE;">Upload Employee Details</td>
                </tr>
                <tr>
                    <td>Select file</td>
                    <td><input type="file" name="importCnote" id="importCnote" class="form-control" style="width:400px;height:40px;"></td>
                    <td>Upload Date</td>
                    <td><input type="text" name="planDate" id="planDate" class="form-control" value="<%=curDate%>" readonly style="width:200px;height:40px;"></td>
                </tr>
                <tr >
                <td align="center" colspan="4"><input type="button"class="btn btn-success"  value="Submit" name="Search" onclick="submitPage()" style="width:100px;height:35px;">
                </tr>
            </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
  </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>


