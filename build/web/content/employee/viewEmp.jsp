<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>

<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script language="javascript">






    function submitPage(value) {

        if (value == "search") {

            if (isEmpty(document.lecture.staffCode.value) && isEmpty(document.lecture.staffName.value)) {
                alert("please enter the staff code or staff name and search");
                document.lecture.staffCode.focus();
            } else if (document.lecture.staffCode.value != "" && document.lecture.staffName.value != "") {
                alert("please enter the any one and serach");
                document.lecture.staffCode.value = "";
                document.lecture.staffName.value = "";
            } else {
                document.lecture.action = '/throttle/searchViewEmp.do';
                document.lecture.submit();
            }
        } else if (value == 'add') {
            document.lecture.action = '/throttle/addEmployeePage.do';
            document.lecture.submit();
        } else if (value == 'modify') {

            document.lecture.action = '/throttle/searchAlterEmp.do';
            document.lecture.submit();

        }
    }
    function setValues() {
        var staffCode = '<%=request.getAttribute("staffCode")%>';
        var staffName = '<%=request.getAttribute("staffName")%>';
        //                alert(staffCode);
        //                alert(staffName);
        if (staffCode != "null" && staffCode != "") {
            document.lecture.staffCode.value = staffCode;
            $("#empCodeLabel").text(staffCode);
        }
        if (staffCode == "null") {
            document.lecture.staffCode.value = "";
            $("#empCodeLabel").text();
        }
        if (staffName != "null" && staffName != "") {
            document.lecture.staffName.value = staffName;
            $("#empNameLabel").text(staffName);
        }
        if (staffName == "null") {
            document.lecture.staffName.value = "";
            $("#empNameLabel").text();
        }
    }




    function addPage()
    {
        document.lecture.action = '/throttle/addEmployeePage.do';
        document.lecture.submit();
    }

    function alterPage()
    {
        document.lecture.action = '/throttle/searchAlterEmp.do';
        document.lecture.submit();
    }

    function codeSearch(mode) {
        if (mode == 0) {
            var oTextbox = new AutoSuggestControl(document.getElementById("staffCode"), new ListSuggestions("staffCode", "/throttle/handleGetEmpCode.do?"));
        } else {
            var oTextbox = new AutoSuggestControl(document.getElementById("staffName"), new ListSuggestions("staffName", "/throttle/EmpNameSuggests.do?"));
        }

    }



    function viewEmployeePhotos(staffId) {
        window.open('/throttle/content/employee/displayBlobData.jsp?staffId=' + staffId + '&empPhoto=1', 'PopupPage', 'height = 500, width = 500, scrollbars = yes, resizable = yes');
    }
    function viewEmployeeLicense(staffId) {
        window.open('/throttle/content/employee/displayBlobData.jsp?staffId=' + staffId + '&empPhoto=2', 'PopupPage', 'height = 500, width = 500, scrollbars = yes, resizable = yes');
    }
</script>
<script type="text/javascript" language="javascript">
	    $(document).ready(function() {
		$("#tabs").tabs();
	    });
	</script>


<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
                  setValues();
                  getVehicleNos();">
    </c:if>

    <!--	  <span style="float: right">
                    <a href="?paramName=en">English</a>
                    |
                    <a href="?paramName=ar">Ø§Ù„Ø¹Ø±Ø¨ÙŠØ©</a>
              </span>-->

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.EmployeeDetails" text="default text"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.HRMS" text="default text"/></a></li>
                <li class="active"><spring:message code="hrms.label.EmployeeDetails" text="default text"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">  
                <!--[if lte IE 7]>
            <style type="text/css">
            
            #fixme {display:block;
            top:0px; left:0px;  position:fixed;  }
            </style>
            <![endif]-->

                <!--[if lte IE 6]>
                <style type="text/css">
                body {margin:0; padding:0; border:0; height:100%; overflow-y:auto;}
                #fixme {display:block;
                top:0px; left:0px;  position:fixed;  }
                * html #fixme  {position:absolute;}
                </style>
                <![endif]-->

                <!--[if lte IE 6]>
                <style type="text/css">
                /*<![CDATA[*/
                html {overflow-x:auto; overflow-y:hidden;}
                /*]]>*/
                </style>
                <![endif]-->


                <body onLoad="setValues();
                    setImages(1, 1, 0, 0, 0, 0);"><!--setImages(1,1,0,0,0,0);-->
                    <form name="lecture" method="post" >
                        <table  class="table table-info mb30 table-hover" >
                            <thead>
                                <tr>
                                    <th colspan="6">Alter Employee</th>
                                </tr>
                            </thead>

                            <tr>
                                <td ><spring:message code="hrms.label.EmployeeCode" text="default text"/></td>
                                <td ><input typ="text" class="form-control" style="width:240px;height:40px" id="staffCode" name="staffCode" autocomplete="off" onFocus="codeSearch(0)" /></td>
                                <td ><spring:message code="hrms.label.EmployeeName" text="default text"/></td>
                                <td ><input type="text" class="form-control" style="width:240px;height:40px" id="staffName"  name="staffName" autocomplete="off" onFocus="codeSearch(1)"  /></td>
                                <td ><input type="button" class="btn btn-success" value="Search" name="search" onClick="submitPage(this.name)" /></td>
                            </tr>
                            <!--<tr>-->
                            <td colspan="5" align="center" > &nbsp; </td> 
                            <!--
                            <tr>
                                <td colspan="5" align="center" > 
                                    <input type="button" class="btn btn-success" name="Add" onClick="addPage();" value="Add" > &nbsp;
                                    <input type="button" class="btn btn-success" name="Alter" onClick="alterPage();" value="Alter" >
                                </td> 
                            </tr>
                            -->

                        </table>


                        <center>

                            <input type="hidden" value="" name="reqfor" />
                        </center>



                        <%
                            if (request.getAttribute("StaffList") != null) {

                        %>

                        <table class="table table-info mb30 table-hover">
                            <thead>
                                <tr>
                                    <th  ><spring:message code="hrms.label.EmployeeCode" text="default text"/> : <label id="empCodeLabel"></label></th>
                                    <th  ><spring:message code="hrms.label.EmployeeName" text="default text"/> :<label id="empNameLabel"></label></th>
                                </tr>
                            </thead>
                        </table>

                        <div id="tabs">
                            <ul  class="nav nav-tabs">
                                <li class="active"><a href="#personalDetails" data-toggle="tab"><span><spring:message code="hrms.label.PersonalDetails" text="default text"/></span></a></li>
                                <li><a href="#addressDetails" data-toggle="tab"><span><spring:message code="hrms.label.AddressDetails" text="default text"/></span></a></li>
                                <li><a href="#officialDetails" data-toggle="tab"><span><spring:message code="hrms.label.OfficialDetails" text="default text"/></span></a></li>
                            </ul>

                            <div id="personalDetails" class="tab-pane active">
                                <table class="table table-info mb30 table-hover" id="bg" >    
                                    <c:choose>   
                                        <c:when test = "${StaffList != null}">
                                            <c:forEach items="${StaffList}" var="lec">

                                                <thead>
                                                    <tr>
                                                        <th colspan="6" ><spring:message code="hrms.label.EmployeeDetails" text="default text"/></th>
                                                    </tr>
                                                </thead>

                                                <tr>
                                                    <td ><spring:message code="hrms.label.EmployeeCode" text="default text"/>:</td>
                                                    <td >
                                                        <a onclick="viewEmployeePhotos('<c:out value="${lec.staffId}"/>')" ><c:out value="${lec.empCode}"/></a>
                                                    </td>
                                                    <td ><spring:message code="hrms.label.Name" text="default text"/> :</td>
                                                    <td >
                                                        <a onclick="viewEmployeePhotos('<c:out value="${lec.staffId}"/>')" ><c:out value="${lec.name}"/></a>
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td><spring:message code="hrms.label.Employee Id" text="Employee Id"/> :</td>
                                                    <td><c:out value="${lec.orgEmpId}"/></td>
                                                    <td colspan="2"></td>
                                                </tr>

                                                <tr >
                                                    <td > <spring:message code="hrms.label.DateOfBirth" text="default text"/> :</td>
                                                    <td ><c:out value="${lec.dateOfBirth}"/></td>
                                                    <td > <spring:message code="hrms.label.DateOfJoining" text="default text"/> :</td>
                                                    <td  colspan="4"><c:out value="${lec.DOJ}"/></td>
                                                </tr>

                                                <tr >
                                                    <td ><spring:message code="hrms.label.Gender" text="default text"/> :</td>
                                                    <td >
                                                        <c:choose>
                                                            <c:when test="${lec.gender =='Male'}">
                                                                <spring:message code="hrms.label.Male" text="default text"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <spring:message code="hrms.label.Female" text="default text"/>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </td>
                                                    <td > <spring:message code="hrms.label.BloodGroup" text="default text"/> :</td>
                                                    <td ><c:out value="${lec.bloodGrp}"/></td>
                                                </tr>

                                                <tr >
                                                    <td ><spring:message code="hrms.label.MartialStatus" text="default text"/> :</td>
                                                    <td ><c:out value="${lec.martialStatus}"/></td>
                                                    <td > <spring:message code="hrms.label.FatherName" text="default text"/> :</td>
                                                    <td ><c:out value="${lec.fatherName}"/></td>
                                                </tr>


                                                <tr>
                                                    <td > <spring:message code="hrms.label.MobileNo" text="default text"/> :</td>
                                                    <td ><c:out value="${lec.mobile}"/></td>   
                                                    <td > <spring:message code="hrms.label.PhoneNo" text="default text"/>:</td>
                                                    <td ><c:out value="${lec.phone}"/></td>
                                                </tr>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.Qualification" text="default text"/> :</td>
                                                    <td ><c:out value="${lec.qualification}"/></td>
                                                    <td ><spring:message code="hrms.label.Email" text="default text"/> :</td>
                                                    <td ><c:out value="${lec.email}"/></td>

                                                </tr>

                                                <tr>
                                                    <td   colspan="4"><spring:message code="hrms.label.DrivingLicenseDetail" text="default text"/></td>
                                                </tr>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.DrivingLicenseNo" text="default text"/></td>
                                                    <td >
                                                        <a onclick="viewEmployeeLicense('<c:out value="${lec.staffId}"/>')" ><c:out value="${lec.drivingLicenseNo}"/></a>
                                                    </td>
                                                    <td ><spring:message code="hrms.label.LicenseType" text="default text"/></td>
                                                    <td ><c:out value="${lec.licenseType}"/></td>
                                                </tr>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.RenewalDate" text="default text"/></td>
                                                    <td ><c:out value="${lec.licenseDate}"/></td>
                                                    <td ><spring:message code="hrms.label.State" text="default text"/></td>
                                                    <td ><c:out value="${lec.licenseState}"/></td>
                                                </tr>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.DriverType" text="default text"/></td>
                                                    <td ><c:out value="${lec.driverType}"/></td>
                                                    <td ><spring:message code="hrms.label.VendorCompany/Introducer" text="default text"/></td>
                                                    <td ><c:out value="${lec.vendorCompany}"/></td>
                                                </tr>   

                                                <tr>
                                                    <td   colspan="4" style="height: 10px"><spring:message code="hrms.label.EmployeeNomineeDetail" text="default text"/></td>
                                                </tr>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.Name" text="default text"/></td>
                                                    <td ><c:out value="${lec.nomineeName}"/></td>
                                                    <td ><spring:message code="hrms.label.Relation" text="default text"/></td>
                                                    <td >
                                                        <c:if test = "${relationList != null}" >
                                                            <c:forEach  items="${relationList}" var="rel">
                                                                <c:if test="${rel.relationId == lec.nomineeRelation}">
                                                                    <c:out value="${rel.relationName}"/>
                                                                </c:if>
                                                            </c:forEach >
                                                        </c:if>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.DateOfBirth" text="default text"/></td>
                                                    <td ><c:out value="${lec.nomineeDob}"/></td>
                                                    <td ><spring:message code="hrms.label.Age" text="default text"/></td>
                                                    <td ><c:out value="${lec.nomineeAge}"/></td>
                                                </tr>
                                                <c:if test="${familyList != null}">
                                                    <tr>
                                                        <td   colspan="4"><spring:message code="hrms.label.EmployeeFamilyDetail" text="default text"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td ><spring:message code="hrms.label.Name" text="default text"/></td>
                                                        <td ><spring:message code="hrms.label.Relation" text="default text"/></td>
                                                        <td ><spring:message code="hrms.label.DateOfBirth" text="default text"/></td>
                                                        <td ><spring:message code="hrms.label.Age" text="default text"/></td>
                                                    </tr>
                                                    <c:forEach items="${familyList}" var="family">
                                                        <tr>
                                                            <td ><c:out value="${family.relationNames}"/></td>
                                                            <td ><c:out value="${family.relationType}"/></td>
                                                            <td ><c:out value="${family.relationDobs}"/></td>
                                                            <td ><c:out value="${family.relationAges}"/></td>
                                                        </c:forEach>
                                                    </tr>
                                                </c:if>
                                            </c:forEach>
                                        </c:when>  
                                    </c:choose>
                                </table>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.Next" text="Next"/>" name="Save" ></a>
                                </center>
                            </div>

                            <div id="addressDetails"  class="tab-pane">
                                <table class="table table-info mb30 table-hover" id="bg" >    
                                    <c:choose>   
                                        <c:when test = "${StaffList != null}">
                                            <c:forEach items="${StaffList}" var="lec"> 
                                                <thead>
                                                    <tr>
                                                        <th   colspan="4"><spring:message code="hrms.label.PresentAddress" text="default text"/></th>
                                                    </tr>
                                                </thead>
                                                <tr >
                                                    <td > <spring:message code="hrms.label.Address" text="default text"/>:</td>
                                                    <td > <c:out value="${lec.addr}"/></td>
                                                    <td > <spring:message code="hrms.label.City" text="default text"/> :</td>
                                                    <td ><c:out value="${lec.city}"/></td>
                                                </tr>

                                                <tr>
                                                    <td > <spring:message code="hrms.label.State" text="default text"/> :</td>
                                                    <td ><c:out value="${lec.state}"/></td>
                                                    <td > <spring:message code="hrms.label.Pincode" text="default text"/> : </td>
                                                    <td ><c:out value="${lec.pincode}"/></td>
                                                </tr>

                                                <tr>
                                                    <td   colspan="4"><spring:message code="hrms.label.PermanentAddress" text="default text"/></td>
                                                </tr>

                                                <tr >
                                                    <td > <spring:message code="hrms.label.Address" text="default text"/>:</td>
                                                    <td > <c:out value="${lec.addr1}"/></td>
                                                    <td > <spring:message code="hrms.label.City" text="default text"/> :</td>
                                                    <td ><c:out value="${lec.city1}"/></td>
                                                </tr>

                                                <tr>
                                                    <td > <spring:message code="hrms.label.State" text="default text"/> :</td>
                                                    <td ><c:out value="${lec.state1}"/></td>
                                                    <td ><spring:message code="hrms.label.Pincode" text="default text"/> : </td>
                                                    <td ><c:out value="${lec.pincode1}"/></td>
                                                </tr>

                                            </c:forEach>
                                        </c:when>  
                                    </c:choose>
                                </table>  <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.Next" text="Next"/>" name="Save" style="width:100px;height:35px;"></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="hrms.label.Prev" text="default text"/>" name="Prev" style="width:100px;height:35px;"/></a>
                                </center>
                            </div>


                            <div id="officialDetails" class="tab-pane">
                                <table class="table table-info mb30 table-hover" id="bg" >    
                                    <c:choose>   
                                        <c:when test = "${StaffList != null}">
                                            <c:forEach items="${StaffList}" var="lec"> 
                                                <thead>
                                                    <tr>
                                                        <th   colspan="4"><spring:message code="hrms.label.OfficialDetails" text="default text"/></th>
                                                    </tr>
                                                </thead>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.DepartmentName" text="default text"/></td>
                                                    <td ><c:out value="${lec.deptName}"/></td>
                                                    <td ><spring:message code="hrms.label.DesignationName" text="default text"/></td>
                                                    <td ><c:out value="${lec.desigName}"/></td>
                                                </tr>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.GradeName" text="default text"/></td>
                                                    <td ><c:out value="${lec.gradeName}"/></td>
                                                    <td ><spring:message code="hrms.label.Company" text="default text"/></td>
                                                    <td ><c:out value="${lec.companyName}"/></td>
                                                </tr>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.HourlyRate" text="default text"/></td>
                                                    <td  colspan="3" ><c:out value="${lec.hourlyRate}"/></td>
                                                </tr>
                                                <tr>
                                                    <td   colspan="4"><spring:message code="hrms.label.SalaryDetails" text="default text"/></td>
                                                </tr>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.SalaryType" text="default text"/></td>
                                                    <td ><c:out value="${lec.salaryType}"/> <spring:message code="hrms.label.Basis" text="default text"/></td>
                                                    <td ><spring:message code="hrms.label.BasicSalary" text="default text"/></td>
                                                    <td ><c:out value="${lec.basicSalary}"/></td>
                                                </tr>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.IsEligibleForESI" text="default text"/></td>
                                                    <td ><c:if test="${lec.esiEligible == 'Y'}"><spring:message code="hrms.label.Yes" text="default text"/></c:if><c:if test="${lec.esiEligible == 'Y'}"><spring:message code="hrms.label.No" text="default text"/></c:if></td>
                                                    <td ><spring:message code="hrms.label.IsEligibleForPF" text="default text"/></td>
                                                    <td ><c:if test="${lec.pfEligible == 'Y'}"><spring:message code="hrms.label.Yes" text="default text"/></c:if><c:if test="${lec.pfEligible == 'Y'}"><spring:message code="hrms.label.No" text="default text"/></c:if></td>
                                                    </tr>
                                                    <tr>
                                                            <td ><spring:message code="hrms.label.BankAccountNo" text="default text"/></td>
                                                    <td ><c:out value="${lec.bankAccountNo}"/></td>
                                                    <td ><spring:message code="hrms.label.BankName" text="default text"/></td>
                                                    <td ><c:out value="${lec.bankName}"/></td>
                                                </tr>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.BankBranchName" text="default text"/></td>
                                                    <td ><c:out value="${lec.bankBranchName}"/></td>
                                                    <td ><spring:message code="hrms.label.AccountHolderName" text="default text"/></td>
                                                    <td ><c:out value="${lec.accountHolderName}"/></td>
                                                </tr>

                                                <tr>
                                                    <td   colspan="4"><spring:message code="hrms.label.PreviousExperienceandEmployerDetails" text="default text"/></td>
                                                </tr>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.YearsOfExperience" text="default text"/></td>
                                                    <td ><c:out value="${lec.yearOfExperience}"/></td>
                                                    <td ><spring:message code="hrms.label.Designation" text="default text"/></td>
                                                    <td ><c:out value="${lec.prevEmpDesignation}"/></td>
                                                </tr>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.Name" text="default text"/></td>
                                                    <td ><c:out value="${lec.prevCompanyName}"/></td>
                                                    <td ><spring:message code="hrms.label.Address" text="default text"/></td>
                                                    <td ><c:out value="${lec.prevCompanyAddress}"/></td>
                                                </tr>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.City" text="default text"/></td>
                                                    <td ><c:out value="${lec.prevCompanyCity}"/></td>
                                                    <td ><spring:message code="hrms.label.State" text="default text"/></td>
                                                    <td ><c:out value="${lec.prevEmpState}"/></td>
                                                </tr>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.Pincode" text="default text"/></td>
                                                    <td ><c:out value="${lec.prevEmployeePincode}"/></td>
                                                    <td ><spring:message code="hrms.label.ContactNo" text="default text"/></td>
                                                    <td ><c:out value="${lec.prevCompanyContact}"/></td>
                                                </tr>

                                                <tr>
                                                    <td   colspan="4"><spring:message code="hrms.label.CurrentWorkingStatus" text="default text"/></td>
                                                </tr>
                                                <tr>
                                                    <td ><spring:message code="hrms.label.WorkingStatus" text="default text"/></td>
                                                    <td ><c:if test="${lec.terminateFlag =='Y'}"><spring:message code="hrms.label.NotWorking" text="default text"/></c:if><c:if test="${lec.terminateFlag =='N'}">Working</c:if></td>
                                                    <td ><spring:message code="hrms.label.Remarks" text="default text"/></td>
                                                    <td >--</td>
                                                </tr>
                                            </table>         
                                        </c:forEach>
                                    </c:when>  
                                </c:choose>
                                </table>
                                   <center>
				<a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="hrms.label.Prev" text="default text"/>" name="Prev" style="width:100px;height:35px;"/></a>

			    </center>

                           
                            </div>
                                 </div>
                                <script>
                                  
                        $('.btnNext').click(function(){
                        $('.nav-tabs > .active').next('li').find('a').trigger('click');
                        });

                        $('.btnPrevious').click(function(){
                        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                        });
                                </script>
                       
                        <%                        }
                        %>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>