<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.ArrayList,java.util.Iterator,ets.domain.operation.business.OperationTO,ets.domain.operation.business.TripAllowanceTO,ets.domain.operation.business.TripFuelDetailsTO"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Trip Sheet</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });
            $(function() {               
                $( ".datepicker" ).datepicker({                    
                    changeMonth: true,changeYear: true
                });
            });
        </script>
        <script type="text/javascript">
            function bataCalculation(sno){
                var inKm = 0;
                var outKm = 0;
                var totKm = 0;	
                if(document.getElementById("stageIdExp"+sno).value == "Bata") {
                    if(document.getElementById("kmsIn").value != ""){
                        inKm = parseInt(document.getElementById("kmsIn").value);
                    }
                    if(document.getElementById("kmsOut").value != ""){
                        outKm = parseInt(document.getElementById("kmsOut").value);
                    }
                }
                totKm = (inKm - outKm);
                document.getElementById("tripExpensesAmount"+sno).value = (totKm * 0.75).toFixed(0);
                calculateAmount();
	
            }
            function calculateAmount(){
                var totalAllowance = 0;
                var totalExpenses = 0;
                var balanceAmount = 0;
                var expEtmTotal = 0;
                if(document.getElementById("expEtmTotal").value != "") {
                    expEtmTotal = parseFloat(document.getElementById("expEtmTotal").value);
                }      
                for(var i=0; i<11;i++)
                    if(document.getElementById("tripAllowanceAmount"+i) && document.getElementById("tripAllowanceAmount"+i).value != ""){
                        totalAllowance += parseFloat(document.getElementById("tripAllowanceAmount"+i).value);
                    }                
                for(var i=0; i<11;i++)
                    if(document.getElementById("tripExpensesAmount"+i) && document.getElementById("tripExpensesAmount"+i).value != ""){
                        totalExpenses += parseFloat(document.getElementById("tripExpensesAmount"+i).value);
                    }
                if(document.getElementById("totalFuelAmount").value != ""){
                    totalExpenses += parseFloat(document.getElementById("totalFuelAmount").value);
                }
                totalExpenses += expEtmTotal;
                balanceAmount = (totalAllowance - totalExpenses );
                document.getElementById("balanceAmount").value = balanceAmount.toFixed(2);
                document.getElementById("totalExpenses").value = totalExpenses.toFixed(2);
                document.getElementById("totalAllowance").value = totalAllowance.toFixed(2);
            }
            var poItems = 0;
            var rowCount='';
            var sno='';
            var snumber = '';
            function addAllowanceRow() {
                if(sno < 9){
                    sno++;
                    var tab = document.getElementById("allowanceTBL");
                    var rowCount = tab.rows.length;
                    snumber = parseInt(rowCount)-1;
                    var newrow = tab.insertRow( parseInt(rowCount)-1) ;
                    newrow.height="30px";

                    var cell = newrow.insertCell(0);
                    var cell0 = "<td><input type='hidden'  name='itemId' /> "+snumber+"</td>";                    
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(1);
                    cell0 = "<td class='text1'><select class='textbox' id='stageId' style='width:130px'  name='stageId"+sno+"'><option selected value=0>---Select---</option><c:if test = "${opLocation != null}" ><c:forEach items="${opLocation}" var="opl"><option  value='<c:out value="${opl.locationId}" />'><c:out value="${opl.locationName}" /> </c:forEach ></c:if> </select></td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(2);
                    cell0 = "<td class='text2'><input name='tripAllowanceDate' id='tripAllowanceDate"+snumber+"' type='text' class='datepicker' id='tripAllowanceDate' class='Textbox' /></td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(3);
                    cell0 = "<td class='text1'><input name='tripAllowanceAmount' id='tripAllowanceAmount"+snumber+"' type='text' class='textbox' onkeyup='calculateAmount();' class='Textbox' /></td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(4);
                    cell0 = "<td class='text1'><select class='textbox' id='tripAllowancePaidBy' style='width:125px'  name='tripAllowancePaidBy"+sno+"'><option selected value=0>---Select---</option><c:if test = "${paidBy != null}" ><c:forEach items="${paidBy}" var="paid"><option  value='<c:out value="${paid.issuerId}" />'><c:out value="${paid.issuerName}" /> </c:forEach ></c:if> </select></td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(5);
                    cell0 = "<td class='text1'><input name='tripAllowanceRemarks' type='text' class='textbox' id='tripAllowanceRemarks' class='Textbox' /></td>";
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(6);
                    var cell1 = "<td><input type='checkbox' name='deleteItem' value='"+snumber+"'   /> </td>";
                    cell.innerHTML = cell1;

                    $( ".datepicker" ).datepicker({
                        /*altField: "#alternate",
                        altFormat: "DD, d MM, yy"*/
                        changeMonth: true,changeYear: true
                    });
                }
            }
            function delAllowanceRow() {
                try {
                    var table = document.getElementById("allowanceTBL");
                    rowCount = table.rows.length-1;
                    for(var i=2; i<rowCount; i++) {
                        var row = table.rows[i];
                        var checkbox = row.cells[6].childNodes[0];
                        if(null != checkbox && true == checkbox.checked) {
                            if(rowCount <= 1) {
                                alert("Cannot delete all the rows");
                                break;
                            }
                            table.deleteRow(i);
                            rowCount--;
                            i--;
                            sno--;             
                        }
                    }sumAllowanceAmt();
                }catch(e) {
                    alert(e);
                }
            }
            function validateSubmit() {
                if(isEmpty(document.getElementById("routeId").value)){
                    alert("Select Route");
                    document.getElementById("routeId").focus();
                    return false;
                } else if(isEmpty(document.getElementById("vehicleId").value)){
                    alert("Select Vehicle");
                    document.getElementById("vehicleId").focus();
                    return false;
                } else if(isEmpty(document.getElementById("tripCode").value)){
                    alert("Trip Code is not filled");
                    document.getElementById("tripCode").focus();
                    return false;
                }else if(isEmpty(document.getElementById("kmsOut").value)){
                    alert("Kms Out is not filled");
                    document.getElementById("kmsOut").focus();
                    return false;
                }else if(isDigit(document.getElementById("kmsOut").value)){
                    alert("Kms Out accepts only numeric values");
                    document.getElementById("kmsOut").focus();
                    return false;
                }else if(isEmpty(document.getElementById("kmsIn").value)){
                    document.getElementById("kmsIn").focus();
                    alert("Kms Out is not filled");
                    return false;
                }else if(isDigit(document.getElementById("kmsIn").value)){
                    alert("Kms In accepts only numeric values");
                    document.getElementById("kmsIn").focus();
                    return false;
                }/*else if(parseFloat(document.getElementById("kmsIn").value) == 0){
                    alert("Kms In Should not be 0");
                    document.getElementById("kmsIn").focus();
                    return false;
                }*/else if(isEmpty(document.getElementById("arrivalDate").value)){
                    alert("Arrival Date is not filled");
                    document.getElementById("arrivalDate").focus();
                    return false;
                }/*else if(document.getElementById('totalKms').value="null"){
                    alert("totalKms is not null");
                    document.getElementById("kmsIn").focus();
                    return false;
                }*/
            }
        </script>
    </head>
    <body>
        <form name="tripSheet" action="updateTripSheet.do" method="post" onsubmit="return validateSubmit();" >
            <%@ include file="/content/common/message.jsp" %>
            <%
                        OperationTO operationTo = new OperationTO();
                        ArrayList tripDetailsAL = (ArrayList) request.getAttribute("tripDetails");
                        if (tripDetailsAL != null && tripDetailsAL.size() > 0) {
                            operationTo = (OperationTO) tripDetailsAL.get(0);
            %>
            <center><h2> Trip Sheet</h2><input type="hidden" name="tripSheetIdParam" value="<%=request.getParameter("tripSheetIdParam")%>" /></center>
            <div style="padding-left: 60px;">
            <table cellpadding="0" cellspacing="4" border="0" width="90%">
                <tr>
                    <td class="contenthead">Trip Details </td>
                </tr>
                <tr>
                    <td align="center">
                        <table name="mainTBL" class="TableMain" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td width="20%" class="texttitle1"> Route</td>
                                <td width="31%" class="text1">
                                    <input name="routeId" id="routeId" type="hidden" class="textbox" value="<%=operationTo.getTripRouteId()%>"  />
                                    <input name="routeName" type="text" class="textbox" value="<%=operationTo.getRouteName()%>" readonly="true" id="routeName"  />
                                </td>                                
                                <td width="20%" class="texttitle1">
                                    Vehicle
                                </td>
                                <td width="25%" class="text1">
                                    <input name="vehicleId" type="hidden" class="textbox" value="<%=operationTo.getTripVehicleId()%>" id="vehicleId"  />
                                    <select name="vehicleId1" onchange="fillData()" id="vehicleId1" class="textbox" style="width:120px;" disabled>
                                        <option value=""> -Select- </option>
                                        <c:if test = "${vehicleRegNos != null}" >
                                            <c:forEach items="${vehicleRegNos}" var="vd">
                                                <option value='<c:out value="${vd.vehicleId}" />'><c:out value="${vd.regNo}" /></option>
                                            </c:forEach >
                                        </c:if>
                                    </select>
                                </td>
                                <td width="4%" class="TableRowNew">
                                </td>
                            </tr>
                            <script type="text/javascript">
                                function visibleVehicle(){
                                    var x=document.getElementById("vehicleId");
                                    x.disabled=false;
                                }
                                function fillData(){                                 
                                    var currentDate = new Date();
                                    var day = currentDate.getDate();
                                    var month = currentDate.getMonth() + 1;
                                    var year = currentDate.getFullYear();
                                    var myDate= day + "-" + month + "-" + year;
                                    document.getElementById("departureDate").value=myDate;
                                    document.getElementById("tripDate").value=myDate;
                                    day = currentDate.getDate()+1;
                                    myDate= day +  "-" + month + "-" + year;
                                    document.getElementById("arrivalDate").value=myDate;
                                }
                                document.getElementById('routeId').value = "<%=operationTo.getTripRouteId()%>";                                
                                document.getElementById('vehicleId1').value = "<%=operationTo.getTripVehicleId()%>";
                            </script>
                            <tr>
                                <td class="texttitle2">
                                    Trip Id
                                </td>
                                <td class="text2">
                                    <input name="tripCode" type="text" class="textbox" value="<%=operationTo.getTripSheetId()%>" readonly="readonly" id="tripCode"  />
                                    <input name="requestCmd" type="hidden"  id="requestCmd" value="save" />
                                </td>                              
                                <td class="texttitle2">Trip Date</td>
                                <td class="text2">
                                    <input name="tripDate" type="text" class="datepicker" value="<%=operationTo.getTripDate()%>"  readonly="readonly" id="tripDate" />                                    
                                </td>
                                <td class="texttitle2">
                                </td>
                            </tr>
                            <tr>
                                <td class="texttitle1"> Departure Date</td>
                                <td class="text1">
                                    <input name="departureDate" type="text" class="datepicker"  value="<%=operationTo.getTripDepartureDate()%>" id="departureDate" readonly="true" />
                                    <select class="textbox" name="dHours" id="dHours" style="width:45px;">
                                        <option value="00">00</option>
                                        <option value="01">01</option><option value="02">02</option><option value="03">03</option>
                                        <option value="04">04</option><option value="05">05</option><option value="06">06</option>
                                        <option value="07">07</option><option value="08">08</option><option value="09">09</option>
                                        <option value="10">10</option><option value="11">11</option><option value="12">12</option>
                                        <option value="13">13</option><option value="13">13</option><option value="14">14</option>
                                        <option value="15">15</option><option value="16">16</option><option value="17">17</option>
                                        <option value="18">18</option><option value="19">19</option><option value="20">20</option>
                                        <option value="21">21</option><option value="22">22</option><option value="23">23</option>
                                        <option value="24">24</option>
                                    </select>
                                    <script type="text/javascript">
                                        document.getElementById('dHours').value = "<%=operationTo.getDepHours()%>";
                                    </script>
                                    <select class="textbox" name="dMints" id="dMints" style="width:45px;">
                                        <option value="00">00</option>
                                        <option value="01">01</option><option value="02">02</option><option value="03">03</option>
                                        <option value="04">04</option><option value="05">05</option><option value="06">06</option>
                                        <option value="07">07</option><option value="08">08</option><option value="09">09</option>
                                        <option value="10">10</option><option value="11">11</option><option value="12">12</option>
                                        <option value="13">13</option><option value="13">13</option><option value="14">14</option>
                                        <option value="15">15</option><option value="16">16</option><option value="17">17</option>
                                        <option value="18">18</option><option value="19">19</option><option value="20">20</option>
                                        <option value="21">21</option><option value="22">22</option><option value="23">23</option>
                                        <option value="24">24</option><option value="25">25</option><option value="26">26</option>
                                        <option value="27">27</option><option value="28">28</option><option value="29">29</option>
                                        <option value="30">30</option><option value="31">31</option><option value="32">32</option>
                                        <option value="33">33</option><option value="34">34</option><option value="35">35</option>
                                        <option value="36">36</option><option value="37">37</option><option value="38">38</option>
                                        <option value="39">39</option><option value="40">40</option><option value="41">41</option>
                                        <option value="42">42</option><option value="43">43</option><option value="44">44</option>
                                        <option value="45">45</option><option value="46">46</option><option value="47">47</option>
                                        <option value="48">48</option><option value="49">49</option><option value="50">50</option>
                                        <option value="51">51</option><option value="52">52</option><option value="53">53</option>
                                        <option value="54">54</option><option value="55">55</option><option value="56">56</option>
                                        <option value="57">57</option><option value="58">58</option><option value="59">59</option>
                                    </select>
                                    <script type="text/javascript">
                                        document.getElementById('dMints').value = "<%=operationTo.getDepMints()%>";
                                    </script>
                                </td>                             
                                <td class="texttitle1">Arrival Date</td>
                                <td class="text1">
                                    <input name="arrivalDate" type="text" class="datepicker" value="<%=operationTo.getTripArrivalDate()%>" id="arrivalDate" readonly="true"  />
                                    <select class="textbox" name="aHours" id="aHours" style="width:45px;">
                                        <option value="00">00</option>
                                        <option value="01">01</option><option value="02">02</option><option value="03">03</option>
                                        <option value="04">04</option><option value="05">05</option><option value="06">06</option>
                                        <option value="07">07</option><option value="08">08</option><option value="09">09</option>
                                        <option value="10">10</option><option value="11">11</option><option value="12">12</option>
                                        <option value="13">13</option><option value="13">13</option><option value="14">14</option>
                                        <option value="15">15</option><option value="16">16</option><option value="17">17</option>
                                        <option value="18">18</option><option value="19">19</option><option value="20">20</option>
                                        <option value="21">21</option><option value="22">22</option><option value="23">23</option>
                                        <option value="24">24</option>
                                    </select>
                                    <script type="text/javascript">
                                        document.getElementById('aHours').value = "<%=operationTo.getArrHours()%>";
                                    </script>
                                    <select class="textbox" name="aMints" id="aMints" style="width:45px;">
                                        <option value="00">00</option>
                                        <option value="01">01</option><option value="02">02</option><option value="03">03</option>
                                        <option value="04">04</option><option value="05">05</option><option value="06">06</option>
                                        <option value="07">07</option><option value="08">08</option><option value="09">09</option>
                                        <option value="10">10</option><option value="11">11</option><option value="12">12</option>
                                        <option value="13">13</option><option value="13">13</option><option value="14">14</option>
                                        <option value="15">15</option><option value="16">16</option><option value="17">17</option>
                                        <option value="18">18</option><option value="19">19</option><option value="20">20</option>
                                        <option value="21">21</option><option value="22">22</option><option value="23">23</option>
                                        <option value="24">24</option><option value="25">25</option><option value="26">26</option>
                                        <option value="27">27</option><option value="28">28</option><option value="29">29</option>
                                        <option value="30">30</option><option value="31">31</option><option value="32">32</option>
                                        <option value="33">33</option><option value="34">34</option><option value="35">35</option>
                                        <option value="36">36</option><option value="37">37</option><option value="38">38</option>
                                        <option value="39">39</option><option value="40">40</option><option value="41">41</option>
                                        <option value="42">42</option><option value="43">43</option><option value="44">44</option>
                                        <option value="45">45</option><option value="46">46</option><option value="47">47</option>
                                        <option value="48">48</option><option value="49">49</option><option value="50">50</option>
                                        <option value="51">51</option><option value="52">52</option><option value="53">53</option>
                                        <option value="54">54</option><option value="55">55</option><option value="56">56</option>
                                        <option value="57">57</option><option value="58">58</option><option value="59">59</option>
                                    </select>
                                    <script type="text/javascript">
                                        document.getElementById('aMints').value = "<%=operationTo.getArrMints()%>";
                                    </script>
                                </td>
                                <td class="TableRowNew">
                                </td>
                            </tr>
                            <tr>
                                <td class="texttitle2">Kms Out</td>
                                <td class="text2">
                                    <input name="kmsOut" type="text" class="textbox"  value="<%=operationTo.getTripKmsOut()%>" id="kmsOut"  OnKeyPress="NumericOnly();" />
                                </td>
                            
                                <td class="texttitle2">
                                    Driver Name
                                </td>
                                <td class="texttitle2">
                                    <input name="driverNameId" type="hidden" class="textbox" value="<%=operationTo.getTripDriverId()%>" id="driverNameId"  />
                                    <select class='textbox' style="width:123px; " id="driverNameId1"  name="driverNameId1" disabled>
                                        <option selected  value="0">---Select---</option>
                                        <c:if test = "${driverName != null}" >
                                            <c:forEach items="${driverName}" var="dri">
                                                <option  value='<c:out value="${dri.empId}" />'>
                                                    <c:out value="${dri.empName}" />
                                                </c:forEach >
                                            </c:if>
                                    </select>
                                    <script type="text/javascript">
                                        document.getElementById('driverNameId1').value = "<%=operationTo.getTripDriverId()%>";
                                    </script>
                                </td>
                                <td class="texttitle2">
                                </td>
                            </tr>
                            <tr>
                                <td class="texttitle1">
                                    Km In
                                </td>
                                <td class="texttitle1">
                                    <input name="kmsIn" type="text" class="textbox"  value="<%=operationTo.getTripKmsIn()%>" id="kmsIn" onblur="totalKmsFunc();setBalance();bataCalculation(1)" OnKeyPress="NumericOnly();" />
                                </td>
                           
                                <td class="texttitle1">
                                    Status
                                </td>
                                <td class="texttitle1">
                                    <select class="textbox" name="status" id="status" style="width:100px;">
                                        <option value="Open">Open</option>
                                        <option value="Close">Close</option>
                                    </select>
                                    <script type="text/javascript">
                                        document.getElementById('status').value = "<%=operationTo.getTripStatus()%>";
                                        //alert(document.getElementById('status').value);
                                    </script>
                                </td>
                                <td class="texttitle1">
                                </td>
                            </tr>
                            <tr>
                                <td class="texttitle2">
                                    Driver Bata / Ton
                                </td>
                                <td class="texttitle2">
                                    <input name="driverBata" type="text" readonly class="textbox"  id="driverBata" />
                                </td>
                         
                                <td class="texttitle2">
                                    Toll Fee
                                </td>
                                <td class="texttitle2">
                                    <input name="routeToll" type="text" readonly class="textbox"  id="routeToll" />
                                </td>
                                <td class="texttitle2">
                                </td>
                            </tr>
                            <tr>
                                <td class="texttitle1">
                                    Tonnage
                                </td>
                                <td class="texttitle1">
                                    <input name="tonnage" type="text" onChange="callAjax();"  class="textbox"  id="tonnage" />
                                </td>
                        
                                <td class="texttitle1">
                                    Expected Revenue from this Trip
                                </td>
                                <td class="texttitle1">
                                    <input name="tripRevenue" type="text" readonly class="textbox"  id="tripRevenue" />
                                </td>
                                <td class="text1">
                                </td>
                            </tr>
                            <tr>
                                <td class="texttitle2">
                                    Loaded Tonnage
                                </td>
                                <td class="texttitle2">
                                    <input name="loadedTonnage" type="text" class="textbox"  id="loadedTonnage" value="<%=operationTo.getTotalTonnage()%>" />
                                </td>
                    
                                <td class="texttitle2">
                                    Delivered Tonnage
                                </td>
                                <td class="texttitle2">
                                    <input name="deliveredTonnage" type="text" class="textbox"  id="deliveredTonnage" value="<%=operationTo.getDeliveredTonnage()%>" />
                                </td>
                                <td class="texttitle2">
                                </td>
                            </tr>
                            <tr>
                                <td class="texttitle1">
                                    Cleaner Status
                                </td>
                                <td class="texttitle1">
                                    <select name="cleanerStatus" class="textbox"  id="cleanerStatus">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                    <script type="text/javascript">
                                        document.getElementById('cleanerStatus').value = "<%=operationTo.getCleanerStatus()%>";
                                    </script>
                                </td>
                           
                                <td class="texttitle1">
                                    Trip Type
                                </td>
                                <td class="texttitle1">
                                    <select name="tripType" class="textbox"  id="tripType">
                                        <option value="1">Loaded Trip</option>
                                        <option value="0">Empty Trip</option>
                                    </select>
                                    <script type="text/javascript">
                                        document.getElementById('tripType').value = "<%=operationTo.getTripType()%>";
                                    </script>
                                </td>
                                <td class="texttitle1">
                                </td>
                            </tr>
                            <tr>
                                <td class="texttitle2">
                                    Vehicle IN/OUT
                                </td>
                                <td class="texttitle2">
                                    <select name="vehicleInOut" id="vehicleInOut" class="textbox" style="width:120px;">
                                        <option value="0">In</option>
                                        <option value="1">Out</option>
                                    </select>
                                    <script type="text/javascript">
                                        document.getElementById('vehicleInOut').value = "<%=operationTo.getInOutIndication()%>";
                                    </script>
                                </td>
                                                          
                                <td class="texttitle2">
                                    Vehicle In/Out Location
                                </td>
                                <td class="texttitle2">
                                    <select name="vehicleInOutLoc" id="vehicleInOutLoc" class="textbox" style="width:120px;">
                                        <option value="">--Select--</option>
                                        <c:if test = "${opLocation != null}" >
                                            <c:forEach items="${opLocation}" var="vl">
                                                <option value='<c:out value="${vl.locationId}" />'><c:out value="${vl.locationName}" /></option>
                                            </c:forEach >
                                        </c:if>
                                    </select>
                                    <script type="text/javascript">
                                        document.getElementById('vehicleInOutLoc').value = "<%=operationTo.getLocationId()%>";
                                    </script>
                                </td>
                                <td class="texttitle2">
                                </td>
                            </tr>
                            <tr>
                                <td class="texttitle1">
                                    Vehicle In/Out Date
                                </td>
                                <td class="texttitle1">
                                    <input name="vehicleInOutDate" type="text" readonly class="textbox"  id="vehicleInOutDate" value="<%=operationTo.getInOutDateTime()%>" />
                                </td>
                             
                                <td class="texttitle1">
                                </td>                                
                                <td class="texttitle1">                                    
                                </td>
                                <td class="text1">
                                </td>
                            </tr>
                            <tr style="height: 10px;">
                                <td align="right" class="TableColumn" colspan="5">
                                </td>
                            </tr>
                            <tr align="center">
                                <td align="center"  colspan="5" rowspan="1">
                                    <h2>Payment Details</h2>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="TableColumn" colspan="5" rowspan="1" style="height: 0%">
                                    <div>
                                        <table cellpadding="0" cellspacing="0" style="width: 100%; left: 30px;" class="border" cellpadding="0" cellspacing="0" rules="all" id="allowanceTBL" name="allowanceTBL">
                                            <tr style="width:50px;">
                                                <th width="50" class="contenthead">S No&nbsp;</th>
                                                <th class="contenthead">Location</th>
                                                <th class="contenthead">Date</th>
                                                <th class="contenthead">Amount</th>
                                                <th class="contenthead">Paid Person</th>
                                                <th class="contenthead">Remarks</th>
                                                <th class="contenthead">Delete</th>
                                            </tr>
                                            <%
                                                int snoAllo = 0;

                                                TripAllowanceTO allowanceTo = null;
                                                ArrayList allowanceAL = (ArrayList) request.getAttribute("tripAllowanceDetails");
                                                if (allowanceAL != null) {
                                                    Iterator it = allowanceAL.iterator();
                                                    while (it.hasNext()) {
                                                        allowanceTo = new TripAllowanceTO();
                                                        allowanceTo = (TripAllowanceTO) it.next();
                                                        snoAllo++;
                                            %>
                                            <tr>
                                                <td><%=snoAllo%></td>
                                                <td>
                                                    <select class='textbox' style="width:130px; " id='stageId<%=snoAllo%>'  name='stageId' >
                                                        <option selected  value="0">---Select---</option>
                                                        <c:if test = "${opLocation != null}" >
                                                            <c:forEach items="${opLocation}" var="opl">
                                                                <option  value='<c:out value="${opl.locationId}" />'> <c:out value="${opl.locationName}" />
                                                                </c:forEach >
                                                            </c:if>
                                                    </select>
                                                    <script type="text/javascript">
                                                        document.getElementById("stageId<%=snoAllo + 1%>").value = '<c:out value="${opl.locationId}"/>';
                                                    </script>
                                                </td>
                                                <td>
                                                    <input name="tripAllowanceDate" type="text" class="datepicker"  id="tripAllowanceDate" value ="<%=allowanceTo.getTripAllowanceDate()%>"  />
                                                </td>
                                                <td>
                                                    <input name="tripAllowanceAmount" type="text" class="textbox" id="tripAllowanceAmount<%=snoAllo%>" value ="<%=allowanceTo.getTripAllowanceAmount()%>" onkeyup="calculateAmount();"  OnKeyPress="NumericOnly()"  />
                                                </td>
                                                <td>
                                                    <script type="text/javascript">
                                                        document.getElementById('stageId<%=snoAllo%>').value = "<%=allowanceTo.getTripStageId()%>";
                                                        function sumAllowanceAmt(){
                                                            var sumAmt=0;
                                                            var totAmt=0;
                                                            sumAmt= document.getElementsByName('tripAllowanceAmount');
                                                            for(i=0;i<sumAmt.length;i++){
                                                                totAmt=parseInt(totAmt)+parseInt(sumAmt[i].value);
                                                                document.getElementById('totalAllowance').value=parseInt(totAmt);
                                                            }
                                                        }
                                                        function sumExpenses(){
                                                            var sumAmt=0;
                                                            sumAmt= parseInt(document.getElementById('totalAllowance').value)+parseInt(document.getElementById('totalFuelAmount').value);
                                                            document.getElementById('totalExpenses').value=parseInt(sumAmt);
                                                        }
                                                        function setBalance(){
                                                            var sumAmt=0;
                                                            sumAmt= parseInt(document.getElementById('totalAllowance').value)-parseInt(document.getElementById('totalFuelAmount').value);
                                                            document.getElementById('balanceAmount').value=parseInt(sumAmt);
                                                        }
                                                    </script>
                                                    <select class='textbox' style="width:123px; " id='tripAllowancePaidBy<%=snoAllo%>' name='tripAllowancePaidBy' >
                                                        <option selected  value="0">---Select---</option>
                                                        <c:if test = "${paidBy != null}" >
                                                            <c:forEach items="${paidBy}" var="paid">
                                                                <option  value='<c:out value="${paid.issuerId}" />'> <c:out value="${paid.issuerName}" />
                                                                </c:forEach >
                                                            </c:if>
                                                    </select>
                                                    <script type="text/javascript">
                                                        document.getElementById("tripAllowancePaidBy<%=snoAllo%>").value = "<%=allowanceTo.getTripAllowancePaidById()%>" ;
                                                    </script>
                                                </td>
                                                <td>
                                                    <input name="tripAllowanceRemarks" type="text" class="textbox" id="tripAllowanceRemarks" value ="<%=allowanceTo.getTripAllowanceRemarks()%>"   />
                                                    <input type="hidden" name="tripAllowanceId" id="tripAllowanceId" />
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <%
                                                    }

                                                }
                                            %>
                                            <tr>
                                                <td colspan="6" align="center">
                                                    <input type="button" name="add" value="Add"  onclick="addAllowanceRow()" id="add" class="button" />
                                                    &nbsp;&nbsp;&nbsp;

                                                    <input type="button" name="delete" value="Delete" onclick="delAllowanceRow()" id="delete" class="button" />

                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr align="center">
                                <td align="right" class="TableColumn">
                                </td>
                                <td align="left" class="TableColumn">
                                </td>
                       
                                <td align="right" class="TableColumn">
                                    Total
                                </td>
                                <td align="left" class="TableColumn">
                                    <input name="totalAllowance" type="text" class="textbox" value="<%=operationTo.getTripTotalAllowances()%>" id="totalAllowance" readonly  />
                                </td>
                                <td class="TableRowNew">
                                </td>
                            </tr>
                            <tr id="ctl00_ContentPlaceHolder1_Tr1" align="center">
                                <td align="center" class="TableColumn" colspan="5" rowspan="1">
                                    <h2>Fuel Details</h2>
                                </td>
                            </tr>
                            <tr id="ctl00_ContentPlaceHolder1_Tr2">
                                <td align="center" class="TableColumn" colspan="5" rowspan="1" style="height: 0%">
                                    <div>
                                        <table cellpadding="0" cellspacing="0" class="border" id="fuelTBL" name="fuelTBL" style="width: 100%; left: 30px;" class="border">

                                            <tr style="width:40px;">
                                                <th class="contenthead">S No</th>
                                                <th class="contenthead">Bunk Name</th>
                                                <th class="contenthead">Place</th>
                                                <th class="contenthead">Date</th>
                                                <th class="contenthead">Amount</th>
                                                <th class="contenthead">Ltrs</th>
                                                <!--<th class="contenthead">Person</th>-->
                                                <th class="contenthead">Remarks</th>
                                                <th class="contenthead">Delete</th>
                                            </tr>
                                            <%
                                                int snoFuel = 0;
                                                TripFuelDetailsTO fuelTo = new TripFuelDetailsTO();
                                                ArrayList fuelAL = (ArrayList) request.getAttribute("fuelDetails");
                                                if (fuelAL != null) {
                                                    Iterator itFuel = fuelAL.iterator();
                                                    while (itFuel.hasNext()) {
                                                        fuelTo = new TripFuelDetailsTO();
                                                        fuelTo = (TripFuelDetailsTO) itFuel.next();
                                                        snoFuel++;
                                            %>
                                            <tr>

                                                <td> <%=snoFuel%> </td>

                                                <td>                                                    
                                                    <select class='textbox' style="width:123px; " id='bunkName<%=snoFuel%>' name='bunkName' >
                                                        <option selected  value="0">---Select---</option>
                                                        <c:if test = "${bunkList != null}" >
                                                            <c:forEach items="${bunkList}" var="bunk">
                                                                <option  value='<c:out value="${bunk.bunkId}" />'> <c:out value="${bunk.bunkName}" />
                                                                </c:forEach >
                                                            </c:if>
                                                    </select>
                                                    <script type="text/javascript">
                                                        document.getElementById("bunkName<%=snoFuel%>").value = "<%=fuelTo.getFuelLocatId()%>" ;
                                                    </script>
                                                </td><td>

                                                    <input name="bunkPlace" type="text" class="textbox" id="bunkPlace" value="<%=fuelTo.getTripBunkPlace()%>"  />

                                                </td><td>

                                                    <input name="fuelDate" id="fuelDate" type="text" class="datepicker" value="<%=fuelTo.getTripFuelDate()%>" id="fuelDate"  style="width:80px;" />

                                                </td><td>

                                                    <input name="fuelAmount" type="text" class="textbox" id="fuelAmount" value="<%=fuelTo.getTripFuelAmounts()%>" onblur="sumFuel();" OnKeyPress="NumericOnly()" style="width:80px;" />

                                                </td><td>

                                                    <input name="fuelLtrs" type="text" class="textbox" id="fuelLtrs" value="<%=fuelTo.getTripFuelLtrs()%>"  OnKeyPress="NumericOnly()" onblur="sumFuel();" style="width:80px;" />

                                                </td>
                                                <!--<td>
                                                    <input name="fuelFilledBy" type="text" class="textbox" id="fuelFilledBy" value="<%=fuelTo.getTripFuelPerson()%>"  />
                                                </td>-->
                                                <td>

                                                    <input name="fuelRemarks" type="text" class="textbox" id="fuelRemarks" value="<%=fuelTo.getTripFuelRemarks()%>"  />

                                                    <input type="hidden" name="HfId" id="HfId" />

                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <%
                                                    }

                                                }
                                            %>
                                            <tr>
                                                <td colspan="8" align="center">
                                                    <input type="button" name="add" value="Add" onclick="addFuelRow()" id="add" class="button" />
                                                    &nbsp;&nbsp;&nbsp;
                                                    <input type="button" name="delete" value="Delete" onclick="delFuelRow()" id="delete" class="button" />
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>

                                        </table>
                                    </div>
                                    <script type="text/javascript">
                                        var poItems = 0;
                                        var rowCount='';
                                        var sno='';
                                        var snumber = '';

                                        function addFuelRow()
                                        {
                                            if(sno < 9){
                                                sno++;
                                                var tab = document.getElementById("fuelTBL");
                                                var rowCount = tab.rows.length;

                                                snumber = parseInt(rowCount)-1;
                                                //                    if(snumber == 1) {
                                                //                        snumber = parseInt(rowCount);
                                                //                    }else {
                                                //                        snumber++;
                                                //                    }

                                                var newrow = tab.insertRow( parseInt(rowCount)-1) ;
                                                newrow.height="30px";
                                                // var temp = sno1-1;
                                                var cell = newrow.insertCell(0);
                                                var cell0 = "<td><input type='hidden'  name='fuelItemId' /> "+snumber+"</td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                 cell = newrow.insertCell(1);
                                                cell0 = "<td class='text2'><select class='textbox' id='bunkName' style='width:170px'  name='bunkName"+sno+"'><option selected value=0>---Select---</option><c:if test = "${bunkList != null}" ><c:forEach items="${bunkList}" var="bunk"><option  value='<c:out value="${bunk.bunkId}" />'><c:out value="${bunk.bunkName}" /> </c:forEach ></c:if> </select></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(2);
                                                cell0 = "<td class='text2'><input name='bunkPlace' type='text' class='textbox' id='bunkPlace' class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(3);
                                                cell0 = "<td class='text1'><input name='fuelDate' id='fuelDate"+snumber+"' type='text' class='datepicker' style='width:80px;' id='fuelDate' class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(4);
                                                cell0 = "<td class='text1'><input name='fuelAmount' onBlur='sumFuel();' type='text' class='textbox' value='0' style='width:80px;' id='fuelAmount' class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(5);
                                                cell0 = "<td class='text1'><input name='fuelLtrs' onBlur='sumFuel();' type='text' class='textbox' id='fuelLtrs' value='0' style='width:80px;' class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                /*cell = newrow.insertCell(6);
                   cell0 = "<td class='text1'><input name='fuelFilledBy' type='text' class='textbox' id='fuelFilledBy' class='Textbox' /></td>";
                   //cell.setAttribute(cssAttributeName,"text1");
                   cell.innerHTML = cell0;*/

                                                cell = newrow.insertCell(6);
                                                cell0 = "<td class='text1'><input name='fuelRemarks' type='text' class='textbox' id='fuelRemarks' class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(7);
                                                var cell1 = "<td><input type='checkbox' name='deleteItem' value='"+snumber+"'   /> </td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell1;
                                                // rowCount++;


                                                $( ".datepicker" ).datepicker({
                                                    /*altField: "#alternate",
                       altFormat: "DD, d MM, yy"*/
                                                    changeMonth: true,changeYear: true
                                                });
                                            }
                                        }


                                        function delFuelRow() {
                                            try {
                                                var table = document.getElementById("fuelTBL");
                                                rowCount = table.rows.length-1;
                                                for(var i=2; i<rowCount; i++) {
                                                    var row = table.rows[i];
                                                    var checkbox = row.cells[7].childNodes[0];
                                                    if(null != checkbox && true == checkbox.checked) {
                                                        if(rowCount <= 1) {
                                                            alert("Cannot delete all the rows");
                                                            break;
                                                        }
                                                        table.deleteRow(i);
                                                        rowCount--;
                                                        i--;
                                                        sno--;
                                                        // snumber--;
                                                    }
                                                }sumFuel();
                                            }catch(e) {
                                                alert(e);
                                            }
                                        }


                                        function sumFuel(){
                                            var totFuel=0;
                                            var totltr=0;
                                            var totAmount=0;
                                            var totAmt=0;
                                            totFuel= document.getElementsByName('fuelLtrs');

                                            totAmount= document.getElementsByName('fuelAmount');
                                            for(i=0;i<totFuel.length;i++){
                                                totltr=parseInt(totltr)+parseInt(totFuel[i].value);
                                                document.getElementById('totalFuelLtrs').value=parseInt(totltr);
                                            }
                                            for(i=0;i<totAmount.length;i++){
                                                totAmt=parseInt(totAmt)+parseInt(totAmount[i].value);
                                                document.getElementById('totalFuelAmount').value=parseInt(totAmt);
                                            }
                                        }
                                        function NumericOnly(){

                                        }

                                        function totalKmsFunc(){
                                            var kmDiff=parseInt(document.getElementById('kmsIn').value)-parseInt(document.getElementById('kmsOut').value);
                                            if(kmDiff<0){kmDiff=0;}
                                            document.getElementById('totalKms').value=kmDiff;
                                        }

                                    </script>
                                </td>
                            </tr>
                            <tr align="center">
                                <td align="right" class="TableColumn">
                                    Total Liters
                                </td>
                                <td align="left" class="TableColumn">
                                    <input name="totalFuelLtrs" type="text" class="textbox"  value="<%=operationTo.getTripTotalLitres()%>" id="totalFuelLtrs" readonly  />
                                </td>
                        
                                <td align="right" class="TableColumn">
                                    Total
                                </td>
                                <td align="left" class="TableColumn">
                                    <input name="totalFuelAmount" type="text" class="textbox" value="<%=operationTo.getTripFuelAmount()%>" id="totalFuelAmount"  readonly  />
                                </td>
                                <td class="TableRowNew">
                                </td>
                            </tr>
                            <tr align="center">
                                <td align="center"  colspan="5" rowspan="1">
                                    <h2>Trip Expenses (Fixed & via ETM)</h2>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="TableColumn" colspan="5" rowspan="1" style="height: 0%">
                                    <div>
                                        <table cellpadding="0" cellspacing="0" style="width: 100%; left: 30px;" class="border" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <th width="50" class="contenthead">S No&nbsp;</th><th class="contenthead">Expenses</th><th class="contenthead">Amount</th><th class="contenthead">Liters(if Fuel)</th><th class="contenthead">Date</th><th class="contenthead">Remarks</th><th class="contenthead">Mode</th>
                                            </tr>
                                            <%
                                                double expTotal = 0;
                                                int expSno = 0;
                                                TripFuelDetailsTO expTo = new TripFuelDetailsTO();
                                                ArrayList expAL = (ArrayList) request.getAttribute("etmExpDetails");
                                                if (expAL != null) {
                                                    Iterator itExp = expAL.iterator();
                                                    while (itExp.hasNext()) {
                                                        expSno++;
                                                        expTo = new TripFuelDetailsTO();
                                                        expTo = (TripFuelDetailsTO) itExp.next();
                                                        expTotal += Integer.parseInt(expTo.getAmount());
                                            %>
                                            <tr style="width:50px;">
                                                <td width="50"><%=expSno%></td><td><%=expTo.getCategory()%></td><td><%=expTo.getAmount()%></td><td><%=expTo.getLiter()%></td><td><%=expTo.getTdate()%></td><td><%=expTo.getRemark()%></td><td><%=expTo.getMode()%></td>
                                            </tr>
                                            <%


                                                    }
                                                }

                                            %>
                                            <tr style="width:50px;">
                                                <td width="50">&nbsp;</td><td><input type="hidden" name="expEtmTotal" id="expEtmTotal" value="<%=expTotal%>" />&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                            </tr>
                                            <tr style="width:50px;">
                                                <td width="50">&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="TableColumn" colspan="5" rowspan="1" style="height: 0%">
                                    <div>
                                          <table cellpadding="0" cellspacing="0" style="width: 100%; left: 30px;" class="border" cellpadding="0" cellspacing="0">
                                              <tr align="center">
                                                <td align="center"  colspan="6" rowspan="1">
                                                    <h2>Trip Expenses</h2>
                                                </td>
                                            </tr>
                                            <script type="text/javascript">
                                                var poItems = 0;
                                                var rowCount='0';
                                                var sno='0';
                                                var snumber = '0';

                                                function addExpensesRow()
                                                {
                                                    if(sno < 9){
                                                        sno++;
                                                        var tab = document.getElementById("expensesTBL");
                                                        var rowCount = tab.rows.length;

                                                        snumber = parseInt(rowCount)-1;
                                                        //                    if(snumber == 1) {
                                                        //                        snumber = parseInt(rowCount);
                                                        //                    }else {
                                                        //                        snumber++;
                                                        //                    }

                                                        var newrow = tab.insertRow( parseInt(rowCount)-1) ;
                                                        newrow.height="30px";
                                                        // var temp = sno1-1;
                                                        var cell = newrow.insertCell(0);
                                                        var cell0 = "<td><input type='hidden'  name='itemId' /> "+snumber+"</td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(1);
                                                        cell0 = "<td class='text1'><select name='stageIdExp' id='stageIdExp"+snumber+"' class='textbox' onChange='bataCalculation("+snumber+")' style='width:150px;'><option>-Select-</option><option>Fuel</option><option>Bata</option><option>Toll</option><option>Spares</option><option>Consumables</option><option>Mechanic Charges</option><option>Mammul Loading Charges</option><option>Parking Charges</option><option>Others</option></select></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(2);
                                                        cell0 = "<td class='text2'><input name='tripExpensesDate' id='tripExpensesDate"+snumber+"' type='text' class='datepicker' id='tripAllowanceDate' class='Textbox' /></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(3);
                                                        cell0 = "<td class='text1'><input name='tripExpensesAmount' id='tripExpensesAmount"+snumber+"' type='text' class='textbox'  onkeyup='calculateAmount();' class='Textbox' /></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(4);
                                                        cell0 = "<td class='text1'><input name='tripExpensesPaidBy' id='tripExpensesPaidBy"+snumber+"' type='text' class='textbox'  /></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(5);
                                                        cell0 = "<td class='text1'><input name='tripExpensesRemarks'id='tripExpensesRemarks"+snumber+"' type='text' class='textbox'  /></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(6);
                                                        var cell1 = "<td><input type='checkbox' name='deleteItemExp' value='"+snumber+"'   /> </td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell1;
                                                        // rowCount++;



                                                        $( ".datepicker" ).datepicker({
                                                            /*altField: "#alternate",
                                                                    altFormat: "DD, d MM, yy"*/
                                                            changeMonth: true,changeYear: true
                                                        });

                                                    }
                                                }


                                                function delExpensesRow() {
                                                    try {
                                                        var table = document.getElementById("expensesTBL");
                                                        rowCount = table.rows.length-1;
                                                        for(var i=2; i<rowCount; i++) {
                                                            var row = table.rows[i];
                                                            var checkbox = row.cells[6].childNodes[0];
                                                            if(null != checkbox && true == checkbox.checked) {
                                                                if(rowCount <= 1) {
                                                                    alert("Cannot delete all the rows");
                                                                    break;
                                                                }
                                                                table.deleteRow(i);
                                                                rowCount--;
                                                                i--;
                                                                sno--;
                                                                // snumber--;
                                                            }
                                                        }sumAllowanceAmt();
                                                    }catch(e) {
                                                        alert(e);
                                                    }
                                                }


                                            </script>
                                            <tr>
                                                <td align="center" class="TableColumn" colspan="6" rowspan="1" style="height: 0%">
                                                    <div>
                                                        <table cellpadding="0" cellspacing="0" style="width: 100%; left: 30px;" class="border" cellpadding="0" cellspacing="0" rules="all" id="expensesTBL" name="expensesTBL">
                                                            <tr>
                                                                <th width="50" class="contenthead">S No&nbsp;</th><th class="contenthead">Expenses</th><th class="contenthead">Date</th><th class="contenthead">Amount</th><th class="contenthead">Paid Person</th><th class="contenthead">Remarks</th><th class="contenthead">&nbsp;</th>
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>
                                                                    <select name="stageIdExp" id="stageIdExp<%=snoAllo%>" class="textbox" onChange="bataCalculation('<%=snoAllo%>')" style="width:150px;">
                                                                        <option value=""> -Select- </option>
                                                                        <option value="Bata">Bata</option>
                                                                        <option value="Fuel">Fuel</option>
                                                                        <option value="Toll">Toll</option>
                                                                        <option value="Spares">Spares</option>
                                                                        <option value="Consumables">Consumables</option>
                                                                        <option value="Mechanic Charges">Mechanic Charges</option>
                                                                        <option value="Mammul Loading Charges">Mammul Loading Charges</option>
                                                                        <option value="Parking Charges">Parking Charges</option>
                                                                        <option value="Others">Others</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <input name="tripExpensesDate" type="text" class="datepicker"  id="tripExpensesDate"   />
                                                                </td>

                                                                <td>
                                                                    <input name="tripExpensesAmount" type="text" class="textbox" id="tripExpensesAmount<%=snoAllo%>" onkeyup="calculateAmount();" OnKeyPress="NumericOnly()"  />
                                                                </td>


                                                            <td>
                                                                <input name="tripExpensesPaidBy" type="text" class="textbox" id="tripExpensesPaidBy" onkeyup="PaidPerson(this);" />
                                                            </td>
                                                            <td>
                                                                <input name="tripExpensesRemarks" type="text" class="textbox" id="tripExpensesRemarks"   />
                                                                <input type="hidden" name="tripExpensesId" id="tripExpenses" />
                                                            </td>
                                                            <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="6" align="center">
                                                                    <input type="button" name="add" value="Add" onclick="addExpensesRow()" id="add" class="button" />
                                                                    &nbsp;&nbsp;&nbsp;

                                                                    <input type="button" name="delete" value="Delete" onclick="delExpensesRow()" id="delete" class="button" />

                                                                </td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr align="center">
                                                <td align="right" class="TableColumn">
                                                </td>

                                                <td align="left" class="TableColumn">
                                                </td>

                                                <td class="TableRowNew">
                                                </td>

                                                <td align="right" class="TableColumn">
                                                    Total Allowances
                                                </td>

                                                <td align="left" class="TableColumn">
                                                    <input name="totalExpensesTab" type="text" class="textbox" value="<%=operationTo.getTripTotalAllowances()%>" id="totalExpensesTab" readonly  />
                                                </td>
                                                <td class="TableRowNew">
                                                </td>
                                            </tr>
                                            <tr id="TrTotalDetails" align="center">

                                                <td align="right" class="TableColumn">

                                                    &nbsp;

                                                </td>

                                                <td align="left" class="TableColumn">
                                                </td>

                                                <td class="TableRowNew">
                                                </td>

                                                <td align="right" class="TableColumn">
                                                    Total Expenses
                                                </td>

                                                <td align="left" class="TableColumn">

                                                    <input name="totalExpenses" type="text" class="textbox" value="<%=operationTo.getTripTotalExpenses()%>" id="totalExpenses" readonly  />

                                                </td>

                                                <td class="TableRowNew">
                                                    <input type="hidden" name="hfETMTotal" id="hfETMTotal" value="0" />
                                                </td>

                                            </tr>
                                            <tr id="TrBalance" align="center">

                                                <td align="right" class="TableColumn">
                                                    Total Kms
                                                </td>

                                                <td align="left" class="TableColumn">
                                                    <input name="totalKms" type="text" class="textbox" value="<%=operationTo.getTripTotalKms()%>" id="totalKms" readonly  />
                                                </td>

                                                <td class="TableRowNew">

                                                </td>

                                                <td align="right" class="TableColumn">

                                                    Balance Amount

                                                </td>

                                                <td align="left" class="TableColumn">
                                                    <input name="balanceAmount" type="text" class="textbox" value="<%=operationTo.getTripBalanceAmount()%>" id="balanceAmount" readonly  />
                                                </td>
                                                <td class="TableRowNew">

                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 189px; height: 19px">
                                </td>
                                <td align="center" colspan="2">
                                </td>
                                <td align="center" style="height: 19px; width: 76px;">
                                </td>
                                <td align="center" style="width: 76px; height: 19px">
                                </td>
                            </tr>
                        </table>
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <center>
                            <input type="submit" name="save" value="Save" onclick="return Mandatory();" id="save" class="button" style="width:120px;" />
                        </center>
                    </td>
                </tr>
            </table>
        </div>
            <%
                        }
            %>
<script type="text/javascript">
    calculateAmount();
</script>
</form>

</body>
</html>
