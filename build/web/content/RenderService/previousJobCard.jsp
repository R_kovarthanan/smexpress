<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>    
<%@ page import="ets.domain.operation.business.OperationTO" %>      
<%@ page import="java.util.*" %>      
<%@ page import="java.http.*" %>      
<%@ page import="ets.domain.mrs.business.MrsTO" %>      
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">    
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

</head>
 <script language="javascript">
        
    function setValues() {
        
                
               document.generateJobCard.totTask.value=document.generateJobCard.tot.value;
               document.generateJobCard.totcomp.value=document.generateJobCard.comple.value;
               document.generateJobCard.totPlan.value=document.generateJobCard.plan.value;
               document.generateJobCard.totUn.value=document.generateJobCard.unP.value;
               document.generateJobCard.totVal.value=document.generateJobCard.totV.value;
               //document.generateJobCard.pPend.value=document.generateJobCard.plan.value-document.generateJobCard.comple.value;
                
                

        }
    </script>
<body onload="setValues()">
<form name="generateJobCard" method="post">
<table align="center" width="80%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
<table align="left" width="70%" border="0" cellspacing="0" cellpadding="0">

<tr>
<td colspan="4" class="contenthead" height="30">Job Card Details</td>
</tr>
<tr>
<td class="text1" height="30">Job Card No</td>
<td class="text1" height="30"><%=request.getAttribute("jcMYFormatNo")%> </td>
<td class="text1" height="30">Work Order No</td>
<td class="text1" height="30"><%=request.getAttribute("workOrderId")%> </td>
</tr>

</table>
<table width="30%" align="right" border="0" cellpadding="0" cellspacing="0" class="border">
<tr> 
<td colspan="2" height="30" class="blue" align="center" style="border:1px;border-color:#FFFFFF;border-bottom-style:dashed;">Summary</td>
</tr>

<tr> 
<td width="121" height="30"  class="bluetext">Total Tasks</td>
<td width="120" height="30"  class="bluetext"><input name="totTask"    size="5" class="bluetextbox" type="text" readonly value="0"></td>
</tr>

<tr> 
<td width="121" height="30"  class="bluetext">Completed</td>
<td width="120" height="30"  class="bluetext"><input name="totcomp"    size="5" class="bluetextbox" type="text" readonly value="0"></td>
</tr>
<tr> 
<td width="121" height="30" class="bluetext">Planned</td>
<td width="120" height="30" class="bluetext"><input name="totPlan"    size="5" class="bluetextbox" type="text" readonly value="0"></td>
</tr>
<!--
<tr> 
<td width="121" height="30" class="bluetext">Planned Pending</td>
<td width="86" height="30" class="bluetext"><input name="pPend"    size="5" class="bluetextbox" type="text" readonly value="0"></td>
</tr>
-->
<tr> 
<td width="121" height="30" class="bluetext">Not Planned</td>
<td width="120" height="30"  class="bluetext"><input name="totUn"    size="5" class="bluetextbox" type="text" readonly value="0"></td>
</tr>
<tr> 
    
    
        
<td width="121" height="30" class="bluetext">Mrs Serviced Rate</td>

<td width="120" height="30"  class="bluetext"><fmt:formatNumber value="${servicedRate}" pattern="##.00"/>%</td>
</tr><tr>
<td width="121" height="30" class="bluetext">Total Amount</td>

<td width="120" height="30"  class="bluetext">Rs.<input name="totVal"    size="5" class="bluetextbox" type="text" readonly value="0"></td>
</tr>
</table>
</td>
</tr>
</table>


<%
    
    
  int   index=0;
    %>
    <c:set var="total" value="0"></c:set>
    <c:set var="unPlanned" value="0"></c:set>
    <c:set var="planned" value="0"></c:set>
    <c:set var="mrsValue" value="0"></c:set>
    <c:set var="completed" value="0"></c:set>
    
<c:if test = "${workOrderProblemDetails != null}" >      
<div align="center" class="contenthead"> Work Order Complaints</div><br>
<table id="bg" align="center" border="0" cellpadding="0" cellspacing="0" width="80%">
  <!--DWLayoutTable-->
<tbody><tr>
<td width="41" height="30" class="contenthead">SNo</td>
<td width="64" height="30" class="contenthead">Section</td>
<td width="76" height="30" class="contenthead">Compliants</td>
<td width="58" height="30" class="contenthead">Symptoms </td>
<td width="92" height="30" class="contenthead">Severity </td>
<td width="82" height="30" class="contenthead">Technician</td>
<td width="130" height="30" class="contenthead">Status</td>
</tr>
<c:forEach items="${workOrderProblemDetails}" var="prob"> 

<%  
String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text1";
} else {
classText = "text2";
}
%>
<tr>
    <td height="30" class="<%=classText%>"><%=index+1%></td>
      <td height="30" class="<%=classText%>"><c:out value="${prob.secName}"/></td>
      <input name="probId" type="hidden" value='<c:out value="${prob.probId}"/>'>
      <td height="30" class="<%=classText%>"><c:out value="${prob.probName}"/></td>
    <!--<td height="30" class="<%=classText%>"><c:out value="${prob.desc}"/></td>-->
    <td height="30" class="<%=classText%>"><c:out value="${prob.symptoms}"/></td>
    <c:if test = "${prob.severity ==1}" >      
    <td height="30" class="<%=classText%>">Low</td>
    </c:if>      
    <c:if test = "${prob.severity ==2}" >
    <td height="30" class="<%=classText%>">Medium</td>
    </c:if>      
    <c:if test = "${prob.severity ==3}" >
    <td height="30" class="<%=classText%>">High</td>
    </c:if>      
<td class="<%=classText%>" height="30">
<c:if test = "${prob.empId ==1}" >    
       External Technician        
</c:if>
<c:if test = "${prob.empId ==0}" >    
       Not Assigned
</c:if>
    
    <c:if test = "${prob.empId !=1 && prob.empId !=0}" >
    <c:if test = "${technicians != null}">
        <c:forEach items="${technicians}" var="mfr">        
        <c:if test = "${prob.empId ==mfr.empId}" >
        <c:out value="${mfr.empName}" />                
        </c:if>         
</c:forEach >
</c:if> 
    
</c:if> 
    </td> 
<td class="<%=classText%>" height="30" >
<c:if test = "${prob.status =='N'}" >
    <c:set var="unPlanned" value="${unPlanned+1}"></c:set>
    <c:set var="total" value="${total+1}"></c:set>
        Not Planned

</c:if>
 <c:if test = "${prob.status =='P'}" >
       Planned
       <c:set var="total" value="${total+1}"></c:set>
       <c:set var="planned" value="${planned+1}"></c:set>
    
</c:if>
<c:if test = "${prob.status =='C'}" >
Completed
<c:set var="total" value="${total+1}"></c:set>
        <c:set var="completed" value="${completed+1}"></c:set>
</c:if>
</td>

</tr>
<%
  index++;
  %>
</c:forEach>

</tbody></table>
</c:if>





<br>


<% String classText="";
    index = 0;
%>
                <br>
               <c:if test = "${mrsItemStatus != null}" >
                <div align="center"  class="contenthead">MRS Item Status </div>
                <br>
                <table id="bg"  align="center" border="0" cellpadding="0" cellspacing="0" width="80%">

                    <td height="30" class="contenthead">SNo</td>
                    <td  height="30" class="contenthead">Mfr Code</td>
                    <td  height="30" class="contenthead">Papl Code</td>
                    <td  height="30" width="90" class="contenthead">Item Name</td>
                    <td  height="30" class="contenthead">Uom</td>
                    <td  height="30" class="contenthead">Req Qty</td>
                    <td  height="30" class="contenthead">Issue Qty</td>

                    <c:forEach items="${mrsItemStatus}" var="mrsItem">
                        <%

            int oddEven1 = index % 2;
            if (oddEven1 > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>

                        <tr>

                            <td class="<%=classText %>" height="30"><%= index+1 %> </td>
                            <td class="<%=classText %>" height="30"><c:out value="${mrsItem.mrsItemMfrCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${mrsItem.mrsPaplCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${mrsItem.mrsItemName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${mrsItem.uomName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${mrsItem.mrsIssueQuantity}"/></td>
                            <td class="<%=classText %>" height="30">
                                <c:if test = "${mrsItem.totalIssued < mrsItem.mrsIssueQuantity}" >
                                    <font color="red"><c:out value="${mrsItem.totalIssued}"/></font>
                                </c:if>
                                <c:if test = "${mrsItem.totalIssued == mrsItem.mrsIssueQuantity}" >
                                    <font color="green"><c:out value="${mrsItem.totalIssued}"/></font>
                                </c:if>
                            </td>
                        </tr>
                        <%
                        index++;
                        %>
                    </c:forEach>
                </table>
            </c:if>

<br>







<% classText="";
index = 0;
%>
                <br>
               <c:if test = "${itemList != null}" > 
                <div align="center"  class="contenthead">Issued/Returned Items List </div>
                <br>
                <table id="bg"  align="center" border="0" cellpadding="0" cellspacing="0" width="80%">
                    
                    <td height="30" class="contenthead">SNo</td>
                    <td  height="30" class="contenthead">MrsNo</td>
                    <td  height="30" class="contenthead">Technician Name</td>
                    <td  height="30" class="contenthead">Mfr Code</td>
                    <td  height="30" class="contenthead">Papl Code</td>
                    <td  height="30" width="90" class="contenthead">Item Name</td>
                    <td  height="30" class="contenthead">Tyre No</td>
                    <td  height="30" class="contenthead">Uom</td>
                    <td  height="30" class="contenthead">Quantity</td>
                    <td  height="30" class="contenthead">Amount</td>
                    <td  height="30" class="contenthead">Stores Action </td>
                    <td  height="30" class="contenthead">Issued Date</td>
                    
                    <c:forEach items="${itemList}" var="item">                     
                        <%
            int oddEven1 = index % 2;
            if (oddEven1 > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
<c:if test = "${item.actionType =='RETURNED'}" >
    <c:set var="mrsValue" value="${mrsValue - (item.price*item.quantityRI)}"></c:set>
</c:if>
<c:if test = "${item.actionType !='RETURNED'}" >
    <c:set var="mrsValue" value="${mrsValue + (item.price*item.quantityRI)}"></c:set>
</c:if>
                        <tr>
                            
                            <td class="<%=classText %>" height="30"><%= index+1 %> </td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.mrsNumber}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.mrsTechnician}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.mrsItemMfrCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.mrsPaplCode}"/></td>                       
                            <td class="<%=classText %>" width="90" height="30"><c:out value="${item.mrsItemName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.tyreNo}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.uomName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.quantityRI}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.price * item.quantityRI}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.actionType}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.issuedOn}"/></td>
                            <!--<td class="<%=classText %>" height="30"><c:out value="${item.faultStatus}"/></td>  --> 
                        </tr>
                        <%
                        index++;
                        %>
                    </c:forEach>                       
                </table>
            </c:if>                 
                
<br>


<div align="center"  class="contenthead">Identified Complaints </div>
<br>

 <table id="addRows" align="center" border="0" cellpadding="0" cellspacing="0" width="80%">
   <!--DWLayoutTable-->
<tbody>

<%
int sno=1;
%>
<c:if test = "${jobCardProblemDetails != null}" >     

<tr>
<td  class="contenthead" height="30" >SNo</td>
<td height="30"  class="contenthead">Section</td>
<td  height="30"  class="contenthead">Problem</td>
<td   height="30" class="contenthead">Symptoms</td>
<td  height="30" class="contenthead" >Severity</td>
<td  height="30"  class="contenthead">Technician</td>
<td  height="30"  class="contenthead">Status</td>

</tr>
<c:forEach items="${jobCardProblemDetails}" var="prob"> 

<%  
 int oddEven = index % 2;
if (oddEven > 0) {
classText = "text1";
} else {
classText = "text2";
}

%>
<tr>
    <c:if test = "${prob.identifiedby =='Technician'}" >
    <td height="30" class="<%=classText%>"><%=sno%></td>
       <td height="30" class="<%=classText%>"><c:out value="${prob.secName}"/></td>
      <input name="probId" type="hidden" value='<c:out value="${prob.probId}"/>'>
      <td height="30" class="<%=classText%>"><c:out value="${prob.probName}"/></td>    
    <td height="30" class="<%=classText%>"><c:out value="${prob.symptoms}"/></td>
    <c:if test = "${prob.severity ==1}" >      
    <td height="30" class="<%=classText%>">Low</td>
    </c:if>      
    <c:if test = "${prob.severity ==2}" >
    <td height="30" class="<%=classText%>">Medium</td>
    </c:if>      
    <c:if test = "${prob.severity ==3}" >
    <td height="30" class="<%=classText%>">High</td>
    </c:if>      
<td class="<%=classText%>" height="30">
<c:if test = "${prob.empId ==1}" >    
       External Technician        
</c:if>
<c:if test = "${prob.empId ==0}" >    
       Not Assigned
</c:if>
    
    <c:if test = "${prob.empId !=1 && prob.empId !=0}" >
    <c:if test = "${technicians != null}" >
        <c:forEach items="${technicians}" var="mfr">        
        <c:if test = "${prob.empId ==mfr.empId}" >
        <c:out value="${mfr.empName}" />                
        </c:if>         
</c:forEach >
</c:if> 
        
</c:if> 
    </td> 
<td class="<%=classText%>" height="30" >
<c:if test = "${prob.status =='N'}" >
    <c:set var="unPlanned" value="${unPlanned+1}"></c:set>
<c:set var="total" value="${total+1}"></c:set>    
        Not Planned

</c:if>
 <c:if test = "${prob.status =='P'}" >
       Planned
       <c:set var="planned" value="${planned+1}"></c:set>
    <c:set var="total" value="${total+1}"></c:set>
</c:if>
<c:if test = "${prob.status =='C'}" >
Completed
    <c:set var="completed" value="${completed+1}"></c:set>
    <c:set var="total" value="${total+1}"></c:set>
</c:if>
</td>
</tr>
<%
  index++;
  sno++;
  %>
  </c:if>
</c:forEach>
</c:if>
<input name="tot" type="hidden" value=' <c:out value="${total}" /> '>
<input name="unP" type="hidden" value=' <c:out value="${unPlanned}" /> '>
<input name="totV" type="hidden" value=' <c:out value="${mrsValue}" /> '>
<input name="comple" type="hidden" value=' <c:out value="${completed}" /> '>
<input name="plan" type="hidden" value=' <c:out value="${planned}" /> '>

</tbody></table>

<br>

</form>
</body>


</html>
