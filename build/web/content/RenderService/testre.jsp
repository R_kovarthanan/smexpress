<%--
    Document   : tripMisReport
    Created on : 8 Mar, 2012, 6:46:28 PM
    Author     : arun
--%>

<%@page contentType="text/html" import="java.sql.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Mis Report</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }
            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }
            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }
            function submitpage(){

                var fromDate = document.viewTripSheet.tripFromDate.value;
                var toDate = document.viewTripSheet.tripToDate.value;
                var status = true;
                if(fromDate == ''){
                    alert("please enter from date");
                    status = false;
                }else if(toDate == ''){
                    alert("please enter to date");
                    status = false;
                }else{
                    var oneDay =24*60*60*1000;

                    var dd_start = parseFloat(fromDate.split("-")[0]);
                    var mm_start = parseFloat(fromDate.split("-")[1])-1;
                    var yy_start = parseFloat(fromDate.split("-")[2]);

                    var dd_end = parseFloat(toDate.split("-")[0]);
                    var mm_end = parseFloat(toDate.split("-")[1])-1;
                    var yy_end = parseFloat(toDate.split("-")[2]);
                    var fromDt = new Date(yy_start,mm_start,dd_start);
                    var toDt = new Date(yy_end,mm_end,dd_end);


                    var diff = toDt.getTime() - fromDt.getTime();
                    var dayDiff = (diff / oneDay).toFixed(0);
                    //alert("dayDiff is "+dayDiff);

                    if(dayDiff > 31){
                        alert("day difference between the chosen date cannot be more than 31 days");
                        status = false;
                    }
                }
                if(status){

                    document.viewTripSheet.action="testre.jsp";
                    document.viewTripSheet.method = "post";
                    document.viewTripSheet.submit();
                }
            }
        </script>

    </head>
    <body>

        <form name="viewTripSheet" >

            <%!    DecimalFormat df2 = new DecimalFormat("#0.00");

                double getFleetExpenses(Statement stm, String vehicleNo, String tripDate) {
                    double fleetExpenses = 0;

                    try {

                        ResultSet res = stm.executeQuery("select c.total_amount  as fleetExp "
                                + "from papl_vehicle_master a, papl_direct_jobcard b, papl_jobcard_bill_master c, "
                                + "papl_vehicle_reg_no d where a.vehicle_id = b.vehicle_id and  "
                                + "a.vehicle_id = d.vehicle_id and d.active_ind='Y' and b.job_card_id = c.job_card_id  "
                                + " and date_format(c.Created_On,'%d-%m-%Y')  = '" + tripDate + "' and d.reg_no in ('" + vehicleNo + "')");
                        while (res.next()) {
                            fleetExpenses = res.getDouble("fleetExp");
                        }
                    } catch (Exception e) {
                        System.out.println("Exception in getting Fleet Amount" + e.getMessage());

                    }
                    return fleetExpenses;

                }

            %>


            <%


String tripFromDate = request.getParameter("tripFromDate");
if (tripFromDate == null) {
    tripFromDate = "";
}

String tripToDate = request.getParameter("tripToDate");
if (tripToDate == null) {
    tripToDate = "";
}



                        try {
                            Class.forName("com.mysql.jdbc.Driver").newInstance();
                            Connection connThtottle = DriverManager.getConnection("jdbc:mysql://localhost:3306/throttleclplnew1", "root", "admin");
                            Statement stm = connThtottle.createStatement();
                            Statement stm1 = connThtottle.createStatement();
                            Statement stmfun = connThtottle.createStatement();


                            ResultSet resVehicle = stm.executeQuery("SELECT distinct vr.vehicle_id,vr.reg_no FROM papl_vehicle_reg_no vr, ra_trip_open_close_epos ts WHERE ts.vehicleid = vr.vehicle_id AND vr.active_ind='Y'");
                            ResultSet resRoute = stm1.executeQuery("select route_id, route_code,  concat(from_location,'-',to_location) as route_name,km,toll_amount, driverbata from ra_route_master where active_ind='Y'");
                           
            %>

            <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="/throttle/images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="/throttle/images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Trip Mis Report</li>
                            </ul>
                            <div id="first">
                                <table width="900" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr>
                                        <td>Vehicle No</td><td>
                                            <select name="vehicleId"  id="vehicleId" class="textbox" style="width:120px;">
                                                <option value=""> -Select- </option>
                                                <%
                                                    while (resVehicle.next()) {
                                                %>
                                                <option value="<%=resVehicle.getString("vr.vehicle_id")%>"><%=resVehicle.getString("vr.reg_no")%></option>
                                                <%
                                                    }
                                                %>
                                            </select>
                                        </td>
                                        <td>Route</td><td>
                                            <select name="routeId" id="routeId" class="textbox" style="width:120px;">
                                                <option value=""> -Select- </option>
                                                <%
                                                    while (resRoute.next()) {
                                                %>
                                                <option value="<%=resRoute.getString("route_id")%>"><%=resRoute.getString("route_name")%></option>
                                                <%
                                                    }
                                                %>
                                            </select>
                                        </td>
                                        <td>Status</td><td>
                                            <select class="textbox" name="status" >
                                                <option value="">---Select---</option>
                                                <option value="Open">Open</option>
                                                <option value="Close">Close</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td><td><input name="tripFromDate" id="tripFromDate"  value="<%=tripFromDate%>" readonly  class="textbox"  type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.viewTripSheet.tripFromDate,'dd-mm-yyyy',this)"/></td>
                                        <td><font color="red">*</font>To Date</td><td><input name="tripToDate" id="tripToDate" value="<%=tripToDate%>" readonly  class="textbox"  type="text" value="" size="20"><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.viewTripSheet.tripToDate,'dd-mm-yyyy',this)"/></td>
                                        <td colspan="2"><input type="button" name="search" value="View Report" onClick="submitpage(this.name)" class="button" /></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <%


                String vehicleId = request.getParameter("vehicleId");
                if (vehicleId == null) {
                    vehicleId = "";
                }
                String routeId = request.getParameter("routeId");
                if (routeId == null) {
                    routeId = "";
                }

                String status = request.getParameter("status");
                if (status == null) {
                    status = "";
                }


                String queryConcat = "";
                if (vehicleId.length() > 0) {
                    queryConcat = queryConcat + " AND  tsl.vehicleid = '" + vehicleId + "'";
                }
                if (routeId.length() > 0) {
                    queryConcat = queryConcat + " AND  tsl.routeid = '" + routeId + "'";
                }
                if (status.equals("Open")) {
                    queryConcat = queryConcat + " AND  tsl.status = 'Open'";
                }
                if (status.equals("Close")) {
                    queryConcat = queryConcat + " AND  tsl.status <> 'Close'";
                }
                if (tripFromDate.length() > 0 && tripToDate.length() > 0) {
                    queryConcat = queryConcat + "  and tsl.tripdate between str_to_date('" + tripFromDate + "','%d-%m-%Y') and str_to_date('" + tripToDate + "','%d-%m-%Y')";

                    String qry = "SELECT  tsl.tripid  FROM ra_trip_open_close_epos tsl, papl_vehicle_reg_no vr"
                            + " WHERE 1 = 1 and tsl.vehicleid = vr.Vehicle_Id and vr.active_ind='Y'" + queryConcat;
                   // out.println(qry);
                    String tripCondition = " ";
                    ResultSet res = stm.executeQuery(qry);
                    out.println(qry);
                    int cntr = 0;
                    while (res.next()) {
                        if (cntr == 0) {
                            tripCondition = tripCondition + res.getInt(1);
                        } else {
                            tripCondition = tripCondition + "," + res.getInt(1);
                        }
                        cntr++;
                    }
                    tripCondition = tripCondition + " ";



                    String newQuery = "select l.tripid,routeid, routename, reg_no, tripdate, outkm, inkm, status, totaltonamount, n.advance, o.liters,fuelamount,m.expenses from "
                                    + "(SELECT ts1.tripid,ts1.routeid,concat(from_location,'-',to_location) as routeName, ts1.vehicleid,vr.reg_no, date_format(ts1.tripdate,'%d-%m-%Y') as tripdate, ts1.driverid, outkm, inkm, ts1.status, ifnull(ts1.totaltonamount,0) as totaltonamount FROM ra_trip_open_close_epos ts1, papl_vehicle_reg_no vr, ra_route_master WHERE 1 = 1 AND ts1.vehicleid = vr.vehicle_id AND ts1.routeid = route_id and vr.active_ind='Y' and ts1.tripid in (tripvalue)) as l,"
                                    + "(SELECT a.tripid, sum(ifnull(b.amount,0)) as expenses FROM ra_trip_open_close_epos a left join trip_expenses_details b on (a.tripid = b.tripsheetid) where a.tripid in (tripvalue) group by tripid) as m,"
                                    + "(SELECT c.tripid, sum(ifnull(e.amount,0)) as advance FROM ra_trip_open_close_epos c left join   ra_trip_advance_epos e on (c.tripid = e.tripid) where   c.tripid in (tripvalue) group by tripid) as n,"
                                    + "(SELECT c.tripid, sum(ifnull(e.liters,0)) as liters, sum(ifnull(e.fuelamount,0)) as fuelamount FROM ra_trip_open_close_epos c left join ra_trip_fuel_epos e on (c.tripid = e.tripid) where c.tripid in (tripvalue) group by tripid) as o "
                                    + "where l.tripid = m.tripid and l.tripid = n.tripid and l.tripid = o.tripid ";
                /*    String newQuery = "select l.tripid,routeid, routename, reg_no, tripdate, outkm, inkm, status, totaltonamount, "
                            + "advance, liters, fuelamount,expenses "
                            + "from "
                            + "(SELECT "
                            + "ts1.tripid,ts.routeid,concat(from_location,'-', to_location) as routeName, "
                            + "ts1.vehicleid,vr.reg_no, date_format(ts.tripdate,'%d-%m-%Y') as tripdate, "
                            + "ts1.driverid, outkm, inkm, ts1.status, ifnull(ts1.totaltonamount,0) as totaltonamount "
                            + "FROM ra_trip_open_close_epos ts1, "
                            + "tripsheet ts,papl_vehicle_reg_no vr, ra_route_master "
                            + "WHERE 1 = 1 AND ts.vehicleid = vr.vehicle_id AND ts.routeid = route_id "
                            + "and vr.active_ind='Y' "
                            + "and ts.epostripid = ts1.tripid and ts1.tripid in (tripvalue)) as l, "
                            + "( SELECT a.epostripid as tripid, "
                            + "sum(ifnull(b.amount,0)) as expenses "
                            + "FROM "
                            + "tripsheet a left join trip_expenses_details b on (a.tripsheetid = b.tripsheetid) "
                            + "where  a.epostripid in (tripvalue) "
                            + "group by epostripid) as m, "
                            + " "
                            + "(SELECT c.tripid, "
                            + "sum(ifnull(e.amount,0)) as advance "
                            + "FROM "
                            + "ra_trip_open_close_epos c "
                            + "left join ra_trip_advance_epos e on (c.tripid = e.tripid) "
                            + "where "
                            + "c.tripid in (tripvalue) "
                            + "group by tripid) as n, "
                            + " "
                            + "(SELECT c.tripid, "
                            + "sum(ifnull(e.liters,0)) as liters, "
                            + "sum(ifnull(e.fuelamount,0)) as fuelamount "
                            + "FROM "
                            + "ra_trip_open_close_epos c "
                            + "left join ra_trip_fuel_epos e on (c.tripid = e.tripid) "
                            + "where "
                            + "c.tripid in (tripvalue) "
                            + "group by tripid) as o "
                            + "where "
                            + "l.tripid = m.tripid "
                            + "and l.tripid = n.tripid "
                            + "and l.tripid = o.tripid "; */

                    newQuery = newQuery.replaceAll("tripvalue", tripCondition);
                   // out.println(newQuery+"<br><br><br><br>");
                    res = stm.executeQuery(newQuery);
            %>


            <script language="javascript">
    document.getElementById("tripFromDate").value = "<%=tripFromDate%>";
    document.getElementById("tripToDate").value = "<%=tripToDate%>";
    document.getElementById("vehicleId").value = "<%=vehicleId%>";
    document.getElementById("routeId").value = "<%=routeId%>";
            </script>


            <%


                double kmIn = 0, kmOut = 0, totalKm = 0, totalAllowance = 0, totalRevenue = 0, totalLiters = 0, totalFuelAmount = 0, totalExpense = 0, balanceAmount = 0, profitLoss = 0, fleetExp = 0;

                double overallKM = 0;
                double overallAllowance = 0;
                double overallExpenses = 0;
                double overallFuelLtrs = 0;
                double overallFuelAmount = 0;
                double overallRevenueAmount = 0;
                double overallExpenseAmount = 0;
                double overallProfitLossAmount = 0;
                double overallBalanceAmount = 0;
                double overallFleetAmount = 0;
            %>
            <table width="110%" cellpadding="5" cellspacing="0">
                <tr>
                    <td  class="contenthead">S.No</td>
                    <td  class="contenthead">Trip Code</td>
                    <td  class="contenthead">Route</td>
                    <td  class="contenthead">Vehicle</td>
                    <td  class="contenthead">Trip Date</td>
                    <td  class="contenthead">KM Out</td>
                    <td  class="contenthead">KM In</td>
                    <td  class="contenthead">Revenue Generated</td>
                    <td  class="contenthead">Total Allowance</td>
                    <td  class="contenthead">Fuel Total Ltrs</td>
                    <td  class="contenthead">Fuel Total Amount</td>
                    <td  class="contenthead">Fixed Expenses (Bata, Toll)</td>
                    <td  class="contenthead">Profit / Loss</td>
                    <td  class="contenthead">Status</td>
                </tr>
                <%

                    String rowClass = "text1";
                    int sno = 0;
                    while (res.next()) {
                        sno++;
                        if ((sno % 2) == 0) {
                            rowClass = "text2";
                        } else {
                            rowClass = "text1";
                        }


                        kmIn = res.getDouble("inkm");
                        kmOut = res.getDouble("outkm");
                        totalKm = kmOut - kmIn;
                        overallKM += totalKm;

                        totalAllowance = res.getDouble("advance");
                        overallAllowance += totalAllowance;

                        totalRevenue = res.getDouble("totaltonamount");
                        overallRevenueAmount += totalRevenue;


                        totalLiters = res.getDouble("liters");
                        overallFuelLtrs += totalLiters;

                        totalFuelAmount = res.getDouble("fuelamount");
                        overallFuelAmount += totalFuelAmount;

                        totalExpense = res.getDouble("expenses");
                        overallExpenseAmount += totalExpense;


                        if (totalRevenue > 0) {
                            profitLoss = totalRevenue + totalAllowance - totalFuelAmount - totalExpense;
                        } else {
                            profitLoss = (totalAllowance - totalFuelAmount - totalExpense);
                        }
                        overallProfitLossAmount += profitLoss;





    //double overallExpenses = 0;

                        fleetExp = getFleetExpenses(stmfun, res.getString("vehicleid"), res.getString("tripdate"));
                        //fleetExp = getFleetExpenses(stmfun, "TN01AH 6688", "23-06-2011");
                        overallFleetAmount += fleetExp;
                %>
                <tr>
                    <td class="<%=rowClass%>"><%=sno%></td>
                    <td class="<%=rowClass%>"><%=res.getString("tripid")%></td>
                    <td class="<%=rowClass%>"><%=res.getString("routename")%></td>
                    <td class="<%=rowClass%>"><%=res.getString("reg_no")%></td>
                    <td class="<%=rowClass%>"><%=res.getString("tripdate")%></td>
                    <td class="<%=rowClass%>"><%=res.getString("outkm")%></td>
                    <td class="<%=rowClass%>"><%=res.getString("inkm")%></td>
                    <td class="<%=rowClass%>"><%=df2.format(res.getDouble("totaltonamount"))%></td>

                    <td class="<%=rowClass%>"><%=res.getString("advance")%></td>
                    <td class="<%=rowClass%>"><%=res.getString("liters")%></td>
                    <td class="<%=rowClass%>"><%=res.getString("fuelamount")%></td>
                    <td class="<%=rowClass%>"><%=res.getString("expenses")%></td>
                    <td class="<%=rowClass%>"><%=df2.format(profitLoss)%></td>
                    <td class="<%=rowClass%>"><%=res.getString("status")%></td>
                </tr>

                <%
                    }

                    if (connThtottle != null) {
                        connThtottle.close();
                    }
                %>
            </table>



            <br>
            <br>
            <table width="900" cellpadding="5" cellspacing="0">
                <tr>
                    <td class="contenthead">Overall Total KM</td>
                    <td class="contenthead">Overall Revenue Generated</td>
                    <td class="contenthead">Overall Total Allowance</td>
                    <td class="contenthead">Overall Fuel Total(Ltrs) </td>
                    <td class="contenthead">Overall Fuel Total Amount</td>
                    <td class="contenthead">Overall Fleet Expenses</td>
                    <td class="contenthead">Overall Profit / Loss Amount</td>
                </tr>

                <tr>
                    <td><%=overallKM%></td>
                    <td><%=df2.format(overallRevenueAmount)%></td>
                    <td><%=df2.format(overallAllowance)%></td>
                    <td><%=overallFuelLtrs%></td>
                    <td><%=df2.format(overallFuelAmount)%></td>
                    <td><%=df2.format(overallExpenseAmount)%></td>
                    <td><%=df2.format(overallProfitLossAmount)%></td>
                </tr>
            </table>

            <%
                            }

                        } catch (Exception e) {
                            out.println(e.getMessage());
                        }
            %>

        </form>

    </body>
</html>
