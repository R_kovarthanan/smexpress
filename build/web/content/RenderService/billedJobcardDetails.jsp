
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>


        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <style type="text/css" title="currentStyle">
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });
        </script>
    </head>
    <script language="javascript">
        function submitPage(value)  {
            var checkBillDetails = false;
            checkBillDetails = billDetailsValidation();
            if(isEmpty(document.jobCardBill.invoiceNo.value)){
                alert("please enter invoice no");
                document.jobCardBill.invoiceNo.focus();
            }else if(isEmpty(document.jobCardBill.invoiceRemarks.value)){
                alert("please enter invoice invoiceRemarks");
                document.jobCardBill.invoiceRemarks.focus();
            }else if(checkBillDetails && confirm("Are you sure to submit")){
                document.jobCardBill.action = "/throttle/saveJobCardBilldetails.do";
                document.jobCardBill.submit();
            }
        }

        function billDetailsValidation(){
            var groupList = document.getElementsByName("groupList");
            var subGroupList = document.getElementsByName("subGroupList");
            var expenseRemarks = document.getElementsByName("expenseRemarks");
            var cost = document.getElementsByName("cost");
            var labour = document.getElementsByName("labour");
            var total = document.getElementsByName("total");
            for(var i=0; i<groupList.length; i++){
                if(groupList[i].value == 0){
                    alert("Please select group list for row "+i+1);
                    groupList[i].focus();
                    return false;
                }else if(subGroupList[i].value == 0){
                    alert("Please select sub group list for row "+i+1);
                    subGroupList[i].focus();
                    return false;
                }else if(expenseRemarks[i].value == 0){
                    alert("Please enter remarks for row "+i+1);
                    expenseRemarks[i].focus();
                    return false;
                }else if(cost[i].value == 0){
                    alert("Please cost for row "+i+1);
                    cost[i].focus();
                    return false;
                }else if(labour[i].value < 0){
                    alert("Please labour value for row "+i+1);
                    labour[i].focus();
                    return false;
                }else{
                    return true;
                }
            }
        }
        function calcluateTotalAmt(){
            document.getElementById("totalValue").innerHTML =
                parseFloat(document.jobCardBill.sparesAmt.value) + parseFloat(document.jobCardBill.consumableAmt.value) + parseFloat(document.jobCardBill.laborAmt.value) +
                parseFloat(document.jobCardBill.othersAmt.value) + parseFloat(document.jobCardBill.vatAmt.value) + parseFloat(document.jobCardBill.serviceTaxAmt.value);
            document.getElementById("totalValue").innerHTML= parseFloat(document.getElementById("totalValue").innerHTML).toFixed(2);
            document.jobCardBill.totalAmt.value = parseFloat(document.getElementById("totalValue").innerHTML).toFixed(2);
        }

    </script>

    <body onload="">

        <form method="post" name="jobCardBill" action= "jobCardBillStore.do">
            <!-- copy there from end -->
            <div id="print" >
                <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                    <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                        <!-- pointer table -->
                        <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                            <tr>
                                <td >
                                    <%@ include file="/content/common/path.jsp" %>
                                </td></tr></table>
                        <!-- pointer table -->

                    </div>
                </div>

                <!-- message table -->
                <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                    <tr>
                        <td >
                            <%@ include file="/content/common/message.jsp" %>
                        </td>
                    </tr>
                </table>
                <!-- message table -->
                <!-- copy there  end -->
                <%
                            int c = 0;
                %>
                <c:if test = "${jcList != null}" >
                    <c:forEach items="${jcList}" var="list">
                        <br>
                        <br>
                        <br>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" class="border">
                            <tbody><tr>
                                    <td class="contenthead" colspan="6" height="30"><div class="contenthead">Record Vendor Bill Details</div></td>
                                </tr>

                                <tr>

                                    <td class="text1" height="30"><b>JobCardNo</b></td>
                                    <td class="text1" height="30"><c:out value="${list.jcMYFormatNo}"/>
                                        <input type="hidden" name="jcNo" value='<c:out value="${list.jcMYFormatNo}"/>'/>
                                        <input type="hidden" name="jcCreatedDate" value='<c:out value="${list.createdDate}"/>'/>
                                        <input type="hidden" name="jcModelName" value='<c:out value="${list.modelName}"/>'/>
                                        <input type="hidden" name="jcMfrName" value='<c:out value="${list.mfrName}"/>'/>
                                        <input type="hidden" name="jcVehicleNo" value='<c:out value="${list.vehicleNo}"/>'/>
                                        <input type="hidden" name="jcCustomerName" value='<c:out value="${list.customerName}"/>'/>

                                    </td>

                                    <td class="text1" height="30"><b>Vehicle No</b></td>
                                    <td class="text1" height="30"><c:out value="${list.vehicleNo}"/></td>
                                  <!--  <td class="text1" height="30"><b>Customer</b></td>
                                    <td class="text1" height="30"><c:out value="${list.customerName}"/></td>  -->
                                    <td class="text1" height="30"><b>Vendor</b></td>
                                    <td class="text1" height="30"><c:out value="${list.vendorName}"/></td>
                                </tr>

                            </tbody></table>
                        <input type="hidden" name="jobCardId" value='<c:out value="${list.jobCardId}"/>'/>


                    </c:forEach >

                </c:if>

                <br>
                    <c:if test = "${billDetails != null}" >
                    <c:forEach items="${billDetails}" var="bill">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" class="border">
                    <tbody><tr>
                            <td class="text2" align="center" colspan="8" height="30"><strong>Vendor Bill Particulars</strong></td>
                        </tr>
                        <tr height="30">
                            <td class="text1" align="left"><b>Invoice No</b></td>
                            <td class="text1" align="left"><input type="text" readonly name="invoiceNo" id="invoiceNo" value="<c:out value="${bill.invoiceNo}"/>" class="textbox" /> </td>
                            <td class="text1" align="left"><b>Invoice Remarks</b></td>
                            <td class="text1" align="left"><textarea name="invoiceRemarks" readonly  id="invoiceRemarks" rowspan="3" colspan="6" class="textbox"><c:out value="${bill.invoiceRemarks}"/></textarea></td>
                            <td class="text1" align="left"><b>Invoice Amount</b></td>
                            <td class="text1" align="left">
                                <input type="text" name="invoiceAmount" id="invoiceAmount" value="<c:out value="${bill.invoiceAmount}"/>"  class="textbox" readonly/>
                            </td>
                            <td class="text1" align="left"><b>Invoice Date</b></td>
                            <td class="text1" align="left">
                                <input type="text" name="invoiceDate" readonly  id="invoiceDate" value="<c:out value="${bill.invoiceDate}"/>"  />
                            </td>
                            <!--                            <td class="text1" align="left"><b>Total Amount</b></td>
                                                        <td class="text1" align="left">
                                                            <input type="text" name="totalAmount" id="totalAmount" value="0" class="textbox" onKeyPress='return onKeyPressBlockCharacters(event);'/>
                                                            <input type="text" name="totalAmountTemp" id="totalAmountTemp" value="0" class="textbox" onKeyPress='return onKeyPressBlockCharacters(event);'/>
                                                        </td>-->
                        </tr>
                </table>
                    </c:forEach>
                    </c:if>
                <br>
                <c:if test = "${billGroupDetails != null}" >
                <table class="border" width="100%" border="0" cellpadding="0"  id="suppExpenseTBL" >
                    <tr >
                        <th width="50" class="contenthead">S No&nbsp;</th>
                        <th class="contenthead"><font color='red'>*</font>Service Type</th>
                        <th class="contenthead"><font color='red'>*</font>Group</th>
                        <th class="contenthead"><font color='red'>*</font>Sub Group</th>
                        <th class="contenthead"><font color='red'>*</font>Part Description</th>
                        <th class="contenthead"><font color='red'>*</font>Quantity</th>
                        <th class="contenthead"><font color='red'>*</font>Cost</th>
                        <th class="contenthead"><font color='red'>*</font>Labour</th>
                        <th class="contenthead"><font color='red'>*</font>Total</th>
                    </tr>
                     
                    <c:forEach items="${billGroupDetails}" var="gpbill">
                         <% int index = 0,sno = 1;%>
                    <tr>
                       
                            <td colspan="0" class="text1" align="center"><%=sno%> </td>
                             <td class="text1" height="30"><c:out value="${gpbill.serviceName}"/></td>
                              <td class="text1" height="30"><c:out value="${gpbill.billGroupName}"/></td>
                               <td class="text1" height="30"><c:out value="${gpbill.billSubGroupName}"/></td>
                               <td class="text1" height="30"><c:out value="${gpbill.groupRemarks}"/></td>
                                <td class="text1" height="30"><c:out value="${gpbill.quantity1}"/></td>
                                 <td class="text1" height="30"><c:out value="${gpbill.cost1}"/></td>
                                  <td class="text1" height="30"><c:out value="${gpbill.labour1}"/></td>
                                   <td class="text1" height="30"><c:out value="${gpbill.total1}"/></td>
                           <!-- <input class="button" type="button" value="Add Row" onClick="addRow(2);"/> -->
                        
                    </tr>
                           <%
                                       index++;
                                       sno++;
                            %>
                           </c:forEach>
                </table>
                </c:if>
                <br>
                     <c:if test = "${billDetails != null}" >
                    <c:forEach items="${billDetails}" var="bill">
                <table class="border" width="100%" border="0" cellpadding="0">
                    <tr>
                        <td>Total Cost</td>
                        <td ><input type="text" name="totalCost" readonly  id="totalCost" class="textbox" value="<c:out value="${bill.totalCost}"/>"/></td>
                    </tr>
                    <tr>
                        <td>Total Labour</td>
                        <td ><input type="text" name="totalLabour" readonly  id="totalLabour" class="textbox" value="<c:out value="${bill.totalLabour}"/>"/></td>
                        <td>Labour Remarks</td>
                        <td ><textarea name="labourRemarks" readonly  id="labourRemarks" ><c:out value="${bill.labourRemarks}"/></textarea></td>
                    </tr>
                    <tr>
                        <td>Total Amount</td>
                        <td ><input type="text" name="totalAmount" readonly  id="totalAmount" class="textbox" value="<c:out value="${bill.totalAmount}"/>"/></td>
                    </tr>
                    <tr>
                        <td>VAT Amount</td>
                        <td ><input type="text" name="vatAmount" readonly  id="vatAmount" class="textbox" onkeyup="calculateInvoiceAmount()" value="<c:out value="${bill.vatAmount}"/>"/></td>
                        <td>VAT Remarks</td>
                        <td ><textarea name="vatRemarks" id="vatRemarks" readonly  ><c:out value="${bill.vatRemarks}"/></textarea></td>
                    </tr>
                    <tr>
                        <td>Service Tax Amount</td>
                        <td ><input type="text" name="serviceTaxAmount" id="serviceTaxAmount" readonly  class="textbox" onkeyup="calculateInvoiceAmount()" value="<c:out value="${bill.serviceTaxAmount}"/>"/></td>
                        <td>Service Tax Remarks</td>
                        <td ><textarea name="serviceTaxRemarks" id="serviceTaxRemarks" readonly  ><c:out value="${bill.serviceTaxRemarks}"/></textarea></td>
                    </tr>
                    <tr><td colspan="20">&nbsp;</td></tr>
                    <tr>
              <!--          <td colspan="11" align="center"><input class="button" type="button" value="Save" onClick="submitPage();"/></td> -->
                    </tr>
                </table>
                    </c:forEach>
                     </c:if>
                <script>


                    var httpRequest1;
                    function getSubGroupList(billGroupId, sno) {
                        if (billGroupId != 'null') {
                            var list1 = eval("document.jobCardBill.subGroupList" + sno);
                            while (list1.childNodes[0]) {
                                list1.removeChild(list1.childNodes[0])
                            }

                            var url = '/throttle/getBillSubGroupDetails.do?billGroupId=' + billGroupId;

                            if (window.ActiveXObject)
                            {
                                httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
                            }
                            else if (window.XMLHttpRequest)
                            {
                                httpRequest1 = new XMLHttpRequest();
                            }
                            httpRequest1.open("POST", url, true);
                            httpRequest1.onreadystatechange = function() {
                                go(sno);
                            };
                            httpRequest1.send(null);
                        }
                    }

                    function go(sno) {
                        if (httpRequest1.readyState == 4) {
                            if (httpRequest1.status == 200) {
                                var response = httpRequest1.responseText;
                                var list = eval("document.jobCardBill.subGroupList" + sno);
                                var details = response.split(',');
                                var probId = 0;
                                var probName = '--select--';
                                var x = document.createElement('option');
                                var name = document.createTextNode(probName);
                                x.appendChild(name);
                                x.setAttribute('value', probId)
                                list.appendChild(x);
                                for (i = 1; i < details.length; i++) {
                                    temp = details[i].split('-');
                                    probId = temp[0];
                                    probName = temp[1];
                                    x = document.createElement('option');
                                    name = document.createTextNode(probName);
                                    x.appendChild(name);
                                    x.setAttribute('value', probId)
                                    list.appendChild(x);
                                }
                            }
                        }


                    }



                    var rowCount = 1;
                    var sno = 0;
                    var rowCount1 = 1;
                    var sno1 = 0;
                    var httpRequest;
                    var httpReq;
                    var styl = "";

                    function addRow(val) {
                        if (parseInt(rowCount1) % 2 == 0)
                        {
                            styl = "text2";
                        } else {
                            styl = "text1";
                        }

                        var sn = sno-1;
                        if(sn >= 0 && $("#netExpense"+sn).val() == ''){
                            alert("Please enter the expense");
                            $("#netExpense"+sn).focus();
                        }else{
                            sno1++;
                            var tab = document.getElementById("suppExpenseTBL");
                            //find current no of rows
                            var rowCountNew = document.getElementById('suppExpenseTBL').rows.length;
                            rowCountNew--;
                            var newrow = tab.insertRow(rowCountNew);
                            cell = newrow.insertCell(0);
                            var cell0 = "<td class='text1' height='25' style='width:10px;'> <input type='hidden' name='editId' id='editId" + sno + "' value=''/>" + sno1 + "</td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;
                            cell = newrow.insertCell(1);
                             cell0 = "<td class='text1' height='25'><select class='textbox' id='serviceList" + sno + "' style='width:125px'  name='serviceList'  onchange='getSubGroupList(this.value," + sno + ")'><option selected value=0>---Select---</option> <c:if test="${serviceList != null}" ><c:forEach items="${serviceList}" var="sl"><option  value='<c:out value="${sl.serviceTypeId}" />'><c:out value="${sl.serviceTypeName}" /> </c:forEach > </c:if> </select></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;
                            cell = newrow.insertCell(2);

                            cell0 = "<td class='text1' height='25'><select class='textbox' id='groupList" + sno + "' style='width:125px'  name='groupList'  onchange='getSubGroupList(this.value," + sno + ")'><option selected value=0>---Select---</option> <c:if test="${groupList != null}" ><c:forEach items="${groupList}" var="gl"><option  value='<c:out value="${gl.billGroupId}" />'><c:out value="${gl.billGroupName}" /> </c:forEach > </c:if> </select></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;
                            cell = newrow.insertCell(3);

                            cell0 = "<td class='text1' height='25'><select class='textbox' id='subGroupList" + sno + "' style='width:125px'  name='subGroupList'><option selected value=0>---Select---</option> </select></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;
                            cell = newrow.insertCell(4);
                            cell0 = "<td class='text1' height='25' ><textarea rows='3' cols='30' class='textbox' name='expenseRemarks' id='expenseRemarks" + sno + "'></textarea></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;
                            cell = newrow.insertCell(5);
                            var cell0 = "<input type='text' name='quantity' id='quantity" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' onkeyup='calculateTotal("+sno+")'></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;
                            cell = newrow.insertCell(6);
                            var cell0 = "<input type='text' name='cost' id='cost" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' onkeyup='calculateTotal("+sno+")'></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;
                            cell = newrow.insertCell(7);
                            var cell0 = "<input type='text' name='labour' id='labour" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' onkeyup='calculateTotal("+sno+")' ></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;
                            cell = newrow.insertCell(8);
                            var cell0 = "<input type='text' name='total' id='total" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);'   value='' readonly ></td>";
                            cell.setAttribute("className", styl);
                            cell.innerHTML = cell0;
                            $(document).ready(function() {
                                $("#datepicker").datepicker({
                                    showOn: "button",
                                    buttonImage: "calendar.gif",
                                    buttonImageOnly: true

                                });
                            });
                            $(function() {
                                $(".datepicker").datepicker({
                                    changeMonth: true, changeYear: true
                                });
                            });
                            rowCount1++;
                            sno++;
                        }
                    }


                    function calculateTotal(sno){
                        var cost = document.getElementById("cost"+sno).value;
                        var labour = document.getElementById("labour"+sno).value;
                        var total = document.getElementById("total"+sno).value;
                        if(cost != '' && labour == ''){
                            document.getElementById("total"+sno).value = cost;
//                            calculateTotalCost();
//                            calculateTotalAmount();
                        }else if(cost == '' && labour != ''){
                            document.getElementById("total"+sno).value = labour;
//                            calculateTotalLabour();
//                            calculateTotalAmount();

                        }else if(cost != '' && labour != ''){
                            document.getElementById("total"+sno).value = parseInt(labour)+parseInt(cost);
                        }
                            calculateTotalCost();
                            calculateTotalLabour();
                            calculateTotalAmount();
                    }

                    function calculateTotalCost(){
                        var cost = document.getElementsByName("cost");
                        var totalCost = 0;
                        for(var i=0; i<cost.length; i++){
                            if(cost[i].value != ""){
                            totalCost = parseFloat(cost[i].value)+parseFloat(totalCost);
                            }
                        }
                        document.getElementById("totalCost").value = parseFloat(totalCost);
                    }
                    function calculateTotalLabour(){
                        var labour = document.getElementsByName("labour");
                        var totalLabour = 0;
                        for(var i=0; i<labour.length; i++){
                            if(labour[i].value != ""){
                            totalLabour = parseFloat(labour[i].value)+parseFloat(totalLabour);
                            }
                        }
                        document.getElementById("totalLabour").value = parseFloat(totalLabour);
                    }
                    function calculateTotalAmount(){
                        var total = document.getElementsByName("total");
                        var totalAmount = 0;
                        for(var i=0; i<total.length; i++){
                            if(total[i].value != ""){
                            totalAmount = parseFloat(total[i].value)+parseFloat(totalAmount);
                            }
                        }
                        document.getElementById("totalAmount").value = parseFloat(totalAmount);
                    }
                    function calculateInvoiceAmount(){
                        var vatAmount = document.getElementById("vatAmount").value;
                        var serviceTaxAmount = document.getElementById("serviceTaxAmount").value;
                        var totalAmount = document.getElementById("totalAmount").value;
                        if(vatAmount == ""){
                            vatAmount = 0;
                        }
                        if(serviceTaxAmount == ""){
                            serviceTaxAmount = 0;
                        }
                        if(totalAmount == ""){
                            totalAmount = 0;
                        }
                        document.getElementById("invoiceAmount").value = parseFloat(vatAmount)+parseFloat(serviceTaxAmount)+parseFloat(totalAmount);

                    }


                </script>





                <br>

            </div>

        </form>
    </body>
</html>
