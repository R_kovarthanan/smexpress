<!DOCTYPE html>
<%@page import="java.text.SimpleDateFormat" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<head>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ page import="java.util.* "%>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>

    <script type="text/javascript" src="/throttle/js/suest"></script>
    <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
    <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
    <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
    <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>

    <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
    <!--raju-->
    <script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>
    <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
    <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

    <link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

    <link rel="stylesheet" type="text/css" href="/throttle/css/select2-bootstrap.css"/>

    <!--    <style>
            .form-control:focus{border-color: #FF0000;  box-shadow: none; -webkit-box-shadow: none;}
            .has-error .form-control:focus{box-shadow: none; -webkit-box-shadow: none;}
        </style>-->


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>


    <c:if test="${consignmentPoint != null}">
        <c:forEach items="${consignmentPoint}" var="routeOne">
            <c:if test="${routeOne.pointSequence == '1' && routeOne.consignmentPointType == 'Pick Up'}" >
                <c:set var="consignmentRouteCourseId1" value="${routeOne.consignmentRouteCourseId}" />
                <c:set var="consignmentOrderId1" value="${routeOne.consignmentOrderId}" />
                <c:set var="consignmentPointId1" value="${routeOne.consignmentPointId}" />
                <c:set var="consignmentPointType1" value="${routeOne.consignmentPointType}" />
                <c:set var="pointAddress1" value="${routeOne.pointAddress}" />
                <c:set var="pointSequence1" value="${routeOne.pointSequence}" />
                <c:set var="routeId1" value="${routeOne.routeId}" />
                <c:set var="pointDate1" value="${routeOne.pointDate}" />
                <c:set var="pointPlanTime1" value="${routeOne.pointPlanTime}" />
                <c:set var="remarks1" value="${routeOne.remarks}" />
                <c:set var="consignorMobile1" value="${routeOne.consignorMobile}" />
                <c:set var="consignorName1" value="${routeOne.consignorName}" />
                <c:set var="consignorId1" value="${routeOne.consignorId}" />
                <c:set var="consigneeName1" value="${routeOne.consigneeName}" />
                <c:set var="consigneeId1" value="${routeOne.consigneeId}" />
                <c:set var="mobileNo1" value="${routeOne.mobileNo}" />
                <c:set var="delAdress1" value="${routeOne.delAdress}" />
                <c:set var="customerReference1" value="${routeOne.customerReference}" />
                <c:set var="gcnNo1" value="${routeOne.gcnNo}" />
                <c:set var="consignmentDate1" value="${routeOne.consignmentDate}" />
                <c:set var="custRefDate1" value="${routeOne.custRefDate}" />
                <c:set var="grossWeights1" value="${routeOne.grossWeights}" />
                <c:set var="weightKG1" value="${routeOne.KG}" />
            </c:if>
        </c:forEach>
    </c:if>
    <c:if test="${consignmentPoint != null}">
        <c:forEach items="${consignmentPoint}" var="routeTwo">
            <c:if test="${routeTwo.pointSequence == consignmentPointSize && routeTwo.consignmentPointType =='Drop'}">
                <c:set var="consignmentRouteCourseId2" value="${routeTwo.consignmentRouteCourseId}" />
                <c:set var="consignmentOrderId2" value="${routeTwo.consignmentOrderId}" />
                <c:set var="consignmentPointId2" value="${routeTwo.consignmentPointId}" />
                <c:set var="consignmentPointType2" value="${routeTwo.consignmentPointType}" />
                <c:set var="pointAddress2" value="${routeTwo.pointAddress}" />
                <c:set var="pointSequence2" value="${routeTwo.pointSequence}" />
                <c:set var="routeId2" value="${routeTwo.routeId}" />
                <c:set var="pointDate2" value="${routeTwo.pointDate}" />
                <c:set var="pointPlanTime2" value="${routeTwo.pointPlanTime}" />
                <c:set var="remarks2" value="${routeTwo.remarks}" />
                <c:set var="consignorMobile2" value="${routeTwo.consignorMobile}" />
                <c:set var="consignorName2" value="${routeTwo.consignorName}" />
                <c:set var="consignorId2" value="${routeTwo.consignorId}" />
                <c:set var="consigneeName2" value="${routeTwo.consigneeName}" />
                <c:set var="consigneeId2" value="${routeTwo.consigneeId}" />
                <c:set var="mobileNo2" value="${routeTwo.mobileNo}" />
                <c:set var="delAdress2" value="${routeTwo.delAdress}" />
                <c:set var="customerReference2" value="${routeTwo.customerReference}" />
                <c:set var="gcnNo2" value="${routeTwo.gcnNo}" />
                <c:set var="consignmentDate2" value="${routeTwo.consignmentDate}" />
                <c:set var="custRefDate2" value="${routeTwo.custRefDate}" />
                <c:set var="grossWeight2" value="${routeTwo.grossWeight}" />
            </c:if>
        </c:forEach>
    </c:if>

    <c:if test="${consignmentPoint != null}">
        <c:forEach items="${consignmentPoint}" var="routeThree">
            <c:if test="${routeThree.pointSequence != consignmentPointSize && routeThree.pointSequence != 1}">
                <c:set var="consignmentRouteCourseId3" value="${routeThree.consignmentRouteCourseId}" />
                <c:set var="consignmentOrderId3" value="${routeThree.consignmentOrderId}" />
                <c:set var="consignmentPointId3" value="${routeThree.consignmentPointId}" />
                <c:set var="consignmentPointType3" value="${routeThree.consignmentPointType}" />
                <c:set var="pointAddress3" value="${routeThree.pointAddress}" />
                <c:set var="pointSequence3" value="${routeThree.pointSequence}" />
                <c:set var="routeId3" value="${routeThree.routeId}" />
                <c:set var="pointDate3" value="${routeThree.pointDate}" />
                <c:set var="pointPlanTime3" value="${routeThree.pointPlanTime}" />
                <c:set var="remarks3" value="${routeThree.remarks}" />
                <c:set var="consignorMobile3" value="${routeThree.consignorMobile}" />
                <c:set var="consignorName3" value="${routeThree.consignorName}" />
                <c:set var="consignorId3" value="${routeThree.consignorId}" />
                <c:set var="consigneeName3" value="${routeThree.consigneeName}" />
                <c:set var="consigneeId3" value="${routeThree.consigneeId}" />
                <c:set var="mobileNo3" value="${routeThree.mobileNo}" />
                <c:set var="delAdress3" value="${routeThree.delAdress}" />
                <c:set var="customerReference3" value="${routeThree.customerReference}" />
                <c:set var="gcnNo3" value="${routeThree.gcnNo}" />
                <c:set var="consignmentDate3" value="${routeThree.consignmentDate}" />
                <c:set var="custRefDate3" value="${routeThree.custRefDate}" />
                <c:set var="grossWeight3" value="${routeThree.grossWeight}" />
            </c:if>
        </c:forEach>
    </c:if>


    <script type="text/javascript">

        function alphanumeric_only(e) {

            var keycode;
            if (window.event)
                keycode = window.event.keyCode;
            else if (event)
                keycode = event.keyCode;
            else if (e)
                keycode = e.which;

            else
                return true;
            if ((keycode >= 47 && keycode <= 57) || (keycode >= 65 && keycode <= 90) || (keycode >= 95 && keycode <= 122)) {

                return true;
            }

            else {
                alert("Please do not use special characters")
                return false;
            }

            return true;
        }



        function viewConsignorDetails() {
            var custName = document.getElementById("customerName").value;
            var custId = document.getElementById("customerId").value;
            window.open('/throttle/handleConsignorUpdate.do?custId=' + custId + "&custName=" + custName, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }

        function viewConsigneeDetails() {
            var custName = document.getElementById("customerName").value;
            var custId = document.getElementById("customerId").value;
            window.open('/throttle/handleConsigneeUpdate.do?custId=' + custId + "&custName=" + custName, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
    </script>
    <script type="text/javascript">

        //this autocomplete is used for consignee and consignor names
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#consignorName').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getConsignorName.do",
                        dataType: "json",
                        data: {
                            consignorName: request.term,
                            customerId: document.getElementById('customerId').value

                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }

                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    $('#consignorName').val(value);
                    $('#consignorPhoneNo').val(ui.item.Mobile);
                    $('#consignorAddress').val(ui.item.Address);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
            $('#consigneeName').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getConsigneeName.do",
                        dataType: "json",
                        data: {
                            consigneeName: request.term,
                            customerId: document.getElementById('customerId').value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    $('#consigneeName').val(value);
                    $('#consigneePhoneNo').val(ui.item.Mobile);
                    $('#consigneeAddress').val(ui.item.Address);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            }

        });</script>
    <script type="text/javascript">

        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true,
                dateFormat: "dd-mm-yy",
                altFormat: "dd-mm-yy"


            });
        });
        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });</script>

    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("#tabs").tabs();
        });</script>
    <script type="text/javascript">

        function convertStringToDate(y, m, d, h, mi, ss) {
            //1
            //var dateString = "2013-08-09 20:02:03";
            var dateString = y + '-' + m + '-' + d + ' ' + h + ':' + mi + ':' + ss;
            //alert(dateString);
            var reggie = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/;
            var dateArray = reggie.exec(dateString);
            var dateObject = new Date(
                    (+dateArray[1]),
                    (+dateArray[2]) - 1, // Careful, month starts at 0!
                    (+dateArray[3]),
                    (+dateArray[4]),
                    (+dateArray[5]),
                    (+dateArray[6])
                    );
            //alert(dateObject);
            return dateObject;
            /*
             //2
             var today = new Date();
             var otherDay = new Date("12/30/2013");
             alert(today >= otherDay);
             
             //3
             var time1 = new Date();
             alert(time1);
             var time1ms= time1.getTime(time1); //i get the time in ms
             var time2 = otherDay;
             alert(time2);
             var time2ms= time2.getTime(time2);
             var difference= time2ms-time1ms;
             var hours = Math.floor(difference / 36e5);
             var minutes = Math.floor(difference % 36e5 / 60000);
             var seconds = Math.floor(difference % 60000 / 1000);
             alert(hours/24);
             alert(minutes);
             alert(seconds);
             
             var lapse = new Date(difference);
             var tz_correction_minutes = new Date().getTimezoneOffset() - lapse.getTimezoneOffset();
             lapse.setMinutes(time2.getMinutes() + tz_correction_minutes);
             alert(lapse.getDate()-1+' days and '  +lapse.getHours()+':'+lapse.getMinutes()+':'+lapse.getSeconds());
             */

        }

        function viewCustomerContract() {
            window.open('/throttle/viewCustomerContract.do?custId=' + $("#customerId").val(), 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }


        function setOriginDestination() {



            var tempVar = document.cNote.contractRoute.value;
            //alert(tempVar);
            var tempSplit = tempVar.split("-");
            document.cNote.vehTypeId.value = tempSplit[3];
            document.cNote.vehicleTypeId.value = tempSplit[3];
            //            document.cNote.vehTypeIdTemp.value = tempSplit[3];
            //alert(document.cNote.vehTypeId.value);
            document.cNote.origin.value = tempSplit[1];
            document.cNote.destination.value = tempSplit[2];
            //alert(document.cNote.origin.value);
            //alert(document.cNote.destination.value);
            document.getElementById("routeContractId").value = tempSplit[0];
            var routeContractId = tempSplit[0];
            //fetch route course
            var pointTypeVal = "";
            var cntr = 0;
            $.ajax({
                url: '/throttle/getContractRouteCourse.do',
                data: {routeContractId: routeContractId},
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(i, data) {
                        //data.Id).html(data.Name)
                        var tempId = data.Id.split("~");
                        var tempName = data.Name.split("~");
                        for (var x = 0; x < tempId.length; x++) {
                            cntr++;
                            if (x == 0) {
                                pointTypeVal = 'Pick Up';
                            } else if (x == (tempId.length - 1)) {
                                pointTypeVal = 'Drop';
                            } else {
                                pointTypeVal = 'select';
                            }
                            //alert(tempId[x]);
                            var tempIdSplit = tempId[x].split("-");
                            addRouteCourse1a(tempIdSplit[0], tempName[x], cntr, pointTypeVal, tempIdSplit[1]);
                        }
                    });
                }
            });
        }

        function checkRoute(val) {
            if (val > 1) {
                var pointOrderValue = document.getElementById("pointOrder" + val).value
                var pointIdValue = document.getElementById("pointId" + val).value
                var prevVal = val - 1;
                var nextVal = val + 1;
                var pointIdValue = document.getElementById("pointId" + val).value
                var pointIdValue = document.getElementById("pointId" + val).value
            }

        }
        function checkOrderSequence(val) {
            var pointOrderValue = document.getElementById("pointOrder" + val).value;
            if (pointOrderValue == '') {
                alert("please fill in the route order sequence");
                document.getElementById("pointOrder" + val).focus();
                document.getElementById("pointName" + val).value = '';
            } else {
                var pointOrderValue = document.getElementsByName("pointOrder");
                var prevPointSeq = 0;
                for (var m = 1; m <= pointOrderValue.length; m++) {
                    if (document.getElementById("pointOrder" + m).value == prevPointSeq) {
                        alert("please crosscheck and correct the  route order sequence");
                        document.getElementById("pointOrder" + val).focus();
                    }
                    prevPointSeq = document.getElementById("pointOrder" + m).value;
                }
            }
        }

        function validateRoute(val) {
            //            alert(val);
            var contractType = document.cNote.billingTypeId.value;
            //alert(contractType);
            if (contractType != '1' && contractType != '2') {

                var pointName = document.getElementById("pointName" + val).value;
                var pointIdValue = document.getElementById("pointId" + val).value;
                var pointOrderValue = document.getElementById("pointOrder" + val).value;
                var pointOrder = document.getElementsByName("pointOrder");
                var pointNames = document.getElementsByName("pointName");
                var pointIdPrev = 0;
                var pointIdNext = 0;
                var pointNamePrev = '';
                var pointNameNext = '';
                //            alert(pointOrderValue);
                //            alert(pointOrder.length);
                var prevPointOrderVal = parseInt(pointOrderValue) - 1;
                var nextPointOrderVal = parseInt(pointOrderValue) + 1;
                for (var m = 1; m <= pointOrder.length; m++) {
                    //                    alert("loop:"+m+" :"+document.getElementById("pointOrder"+m).value +"pointOrderValue:"+pointOrderValue+"prevPointOrderVal:"+prevPointOrderVal+"nextPointOrderVal:"+nextPointOrderVal);
                    if (document.getElementById("pointOrder" + m).value == prevPointOrderVal) {
                        pointIdPrev = document.getElementById("pointId" + m).value
                        pointNamePrev = document.getElementById("pointName" + m).value
                        //                        alert("pointIdPrev:"+pointIdPrev);
                    }
                    if (document.getElementById("pointOrder" + m).value == nextPointOrderVal) {
                        //                        alert("am here..");
                        pointIdNext = document.getElementById("pointId" + m).value
                        pointNameNext = document.getElementById("pointName" + m).value
                        //                        alert("pointIdNext:"+pointIdNext);
                    }
                }
                //            alert(pointIdValue);
                //            alert(pointIdPrev);
                //            alert(pointIdNext);
                if (document.getElementById('customerTypeId').value != 2) {
                    ajaxRouteCheck(pointIdValue, pointIdPrev, pointIdNext, val, pointName, pointNamePrev, pointNameNext);
                }
            }
            //            else{
            //                var pointName = document.getElementById("pointName"+val).value;
            //                var pointIdValue = document.getElementById("pointId"+val).value;
            //                //alert(pointIdValue)
            //                if(pointIdValue == ''){
            //                    alert('please enter valid interim point');
            //                    document.getElementById("pointName"+val).value = '';
            //                    document.getElementById("pointName"+val).focus();
            //                }
            //            }

        }



//        var podRowCount1 = 1;
//        var podSno1 = 0;
//        function addRouteCourse1(id, name, order, type, routeId, routeKm, routeReeferHr, startDate) {
//            var date = document.cNote.startDate.value;
//            var timer = 24;
//            new Date($.now());
//            var dt = new Date();
//            var time = dt.getHours();
//            var time1 = dt.getMinutes();
//            var time2 = dt.getSeconds();
//            
//            var curr_hour, curr_minute;
//            curr_hour = dt.getHours();
//            curr_minute = dt.getMinutes();
//            if(parseInt(curr_hour) < 10){
//                curr_hour = "0"+curr_hour;
//            }
//            if(parseInt(curr_minute) < 10){
//                curr_minute = "0"+curr_minute;
//            }
//        
//            if (parseInt(podRowCount1) % 2 == 0)
//            {
//                styl = "text2";
//            } else {
//                styl = "text1";
//            }
//            podSno1++;
//            var tab = document.getElementById("routePlan");
//            var newrow = tab.insertRow(podRowCount1);
//            //            var cell = newrow.insertCell(0);
//            //            var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
//            //            cell.setAttribute("className", styl);
//            //            cell.innerHTML = cell0;
//
//
//
//            cell = newrow.insertCell(0);
//            var cell0 = "<td><input type='text' readonly  id='pointOrder" + podSno1 + "' name='pointOrder' class='form-control' value='" + order + "' style='width:60px'></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(1);
//            var cell0 = "<td><input type='text' readonly  id='pointName" + podSno1 + "' name='pointName' class='form-control' value='" + name + "' style='width:120px'><input type='hidden' id='pointTransitHrs" + podSno1 + "'  name='pointTransitHrs' value='' ><input type='hidden' id='pointRouteKm" + podSno1 + "'  name='pointRouteKm' value='" + routeKm + "' ><input type='hidden' id='pointRouteReeferHr" + podSno1 + "'  name='pointRouteReeferHr' value='" + routeReeferHr + "' ><input type='hidden' id='pointRouteId" + podSno1 + "'  name='pointRouteId' value='" + routeId + "' ><input type='hidden' id='pointId" + podSno1 + "'  name='pointId' value='" + id + "' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            if (type == 'select') {
//                cell = newrow.insertCell(2);
//                var cell0 = "<td><select name='pointType' id='pointType" + podSno1 + "' class='form-control' style='width:120px'><option value='Pick Up'>Pick Up</option><option value='Drop'>Drop</option></select></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//            } else {
//                cell = newrow.insertCell(2);
//                var cell0 = "<td><input type='text' readonly  id='pointType" + podSno1 + "'  name='pointType' class='form-control' value='" + type + "'  style='width:120px'></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//            }
//            if (type == 'Pick Up') {
//                cell = newrow.insertCell(3);
//                var cell0 = "<td><input type='text'  value='<c:out value="${consignorId1}"/>'  name='consignorIds'  id='consignorIds" + podSno1 + "'   class='form-control' style='width:120px'><input type='text' value='<c:out value="${consignorName1}"/>'    name='consignorNames'  id='consignorNames" + podSno1 + "'   class='form-control' style='width:120px'><div id='con1" + podSno1 + "' style='display:none'><a href='#' onclick='viewConsignorDetails();'>Manage Bill To</a></div><input type='hidden' name='consignorPhoneNos' id='consignorPhoneNos" + podSno1 + "' value='NA'><input type='hidden' name='consignorAddresss' id='consignorAddresss" + podSno1 + "' value='NA'></td>\n\
//                <td><input type='hidden' value='0'  name='consigneeIds'  id='consigneeIds" + podSno1 + "'   class='form-control' style='width:120px'><input type='hidden'  value='NA'    name='consigneeNames'  id='consigneeNames" + podSno1 + "'   class='form-control' style='width:120px'><input type='hidden' name='consigneePhoneNos' id='consigneePhoneNos" + podSno1 + "' value='NA'><input type='hidden' name='consigneeAddresss' id='consigneeAddresss" + podSno1 + "' value='NA'></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//                callConsignorAjax(podSno1);
//            } else {
//                cell = newrow.insertCell(3);
//                var cell0 = "<td><input type='text'  value=''  name='consigneeIds'  id='consigneeIds" + podSno1 + "'   class='form-control' style='width:120px'><input type='text'  value=''    name='consigneeNames'  id='consigneeNames" + podSno1 + "'   class='form-control' style='width:120px'><div id='con2" + podSno1 + "' style='display:none'><div id=<a href='#' onclick='viewConsigneeDetails();'>Manage Ship To</a></div><input type='hidden' name='consigneePhoneNos' id='consigneePhoneNos" + podSno1 + "' value='NA'><input type='hidden' name='consigneeAddresss' id='consigneeAddresss" + podSno1 + "' value='NA'></td>\n\
//                     <td><input type='hidden'  value='0'  name='consignorIds'  id='consignorIds" + podSno1 + "'   class='form-control' style='width:120px'><input type='hidden' value='NA'    name='consignorNames'  id='consignorNames" + podSno1 + "'   class='form-control' style='width:120px'><input type='hidden' name='consignorPhoneNos' id='consignorPhoneNos" + podSno1 + "' value='NA'><input type='hidden' name='consignorAddresss' id='consignorAddresss" + podSno1 + "' value='NA'></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//                callConsigneeAjax(podSno1);
//            }
//
//
//
//            cell = newrow.insertCell(4);
//            var cell0 = "<td><input type='hidden' name='deliveryAddress' value='' id='deliveryAddress" + podSno1 + "'  rows='2' cols='20' class='form-control' style='width:120px' ><textarea name='pointAddresss' id='pointAddresss" + podSno1 + "' rows='2' cols='20' class='form-control' style='width:120px' onchange='setConsignor(" + podSno1 + ");setConsignee(" + podSno1 + ");'><c:out value="${pointAddress1}"/></textarea></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//
//            cell = newrow.insertCell(5);
//            var cell0 = "<td><input type='text' value='<c:out value="${grossWeights1}"/>' id='grossWG" + podSno1 + "'  name='grossWG' class='form-control'  style='width:120px' onKeyPress='return onKeyPressBlockCharacters(event);'><input type='hidden'  id='phoneNos" + podSno1 + "'  name='phoneNos' class='form-control' value='0' style='width:120px' onKeyPress='return onKeyPressBlockCharacters(event);'></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(6);
//            var cell0 = "<td><input type='text'   value='<c:out value="${weightKG1}"/>'    name='weightKG'  id='weightKG" + podSno1 + "'   class='form-control ' style='width:120px' onKeyPress='return onKeyPressBlockCharacters(event);' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(7);
//            var cell0 = "<td><input type='text'   value='<c:out value="${gcnNo1}"/>'    name='gcnNos'  id='gcnNos" + podSno1 + "'   class='form-control ' style='width:120px' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(8);
//            var cell0 = "<td><input type='text'   value='<c:out value="${consignmentDate1}"/>" + date + "'    name='consigDate'  id='consigDate" + podSno1 + "'   class='form-control datepicker' style='width:120px' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(9);
//            var cell0 = "<td><input type='text'   value='<c:out value="${customerReference1}"/>'    name='invoicedNo'  id='invoicedNo" + podSno1 + "'   class='form-control ' style='width:120px' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(10);
//            var cell0 = "<td><input type='text'   value='<c:out value="${consignmentDate1}"/>" + date + "'    name='custRefDate'  id='custRefDate" + podSno1 + "'   class='form-control datepicker' style='width:120px' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            //            $('.datepicker').datepicker();
//
//            cell = newrow.insertCell(11);
//            var cell0 = "<td><input type='text' value='<c:out value="${pointDate1}"/>" + startDate + "'   name='pointPlanDate'  id='pointPlanDate" + podSno1 + "'   class='datepicker form-control' style='width:120px' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            $('.datepicker').datepicker();
//            cell = newrow.insertCell(12);
//            var cell0 = "<td>HH:<select name='pointPlanHour'  id='pointPlanHour" + podSno1 + "'   class='textbox' style='width:60px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute" + podSno1 + "'   class='textbox' style='width:60px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
////            $('#pointPlanHour' + podSno1).val(dt.getHours());
//            $('#pointPlanHour' + podSno1).val(curr_hour);
////            $('#pointPlanMinute' + podSno1).val(dt.getMinutes());
//            $('#pointPlanMinute' + podSno1).val(curr_minute);
//            $('.datepicker').datepicker();
//            //            cell = newrow.insertCell(7);
//            //            var cell0 = "<td class='text1' height='25' >&nbsp;</td>";
//            //            cell.setAttribute("className", styl);
//            //            cell.innerHTML = cell0;
//            setConlinks(podSno1);
//            podRowCount1++;
//            $(document).ready(function() {
//                $("#datepicker").datepicker({
//                    showOn: "button",
//                    buttonImage: "calendar.gif",
//                    buttonImageOnly: true
//
//                });
//            });
//            $(function() {
//                $(".datepicker").datepicker({
//                    changeMonth: true, changeYear: true
//                });
//            });
//
//
//        }


        function setConlinks(val) {

            var billingTypeId = document.getElementById("billingTypeId").value;
            var pointType = document.getElementById("pointType" + val).value;
            if (billingTypeId == 3) {
                if (pointType == 'Pick Up') {
                    document.getElementById('con1' + val).style.display = 'block';
                } else {
                    document.getElementById('con2' + val).style.display = 'block';
                }
            } else {
                if (pointType == 'Pick Up') {
                    document.getElementById('con1' + val).style.display = 'none';
                } else {
                    document.getElementById('con2' + val).style.display = 'none';
                }
            }
        }

        function setConsignor(val) {
            var custType = document.getElementById("customerTypeId").value;
            var pointType = document.getElementById('pointType' + val).value;
            if (custType == 1) {
//                var temp = document.getElementById("consignorIdstemp" + val).value;
//                alert("1234===" + temp);
//                var tempSplit = temp.split('~');
//                document.getElementById("consignorIds" + val).value = tempSplit[0];
//                document.getElementById("consignorPhoneNos" + val).value = tempSplit[2];
//                document.getElementById("consignorAddresss" + val).value = tempSplit[3];
//                document.getElementById("phoneNos" + val).value = tempSplit[2];
//                document.getElementById("pointAddresss" + val).value = tempSplit[3];
//                document.getElementById("deliveryAddress" + val).value = tempSplit[3];
//                document.getElementById("consignorName").value = tempSplit[1];
//                document.getElementById("consignorPhoneNo").value = tempSplit[2];
//                document.getElementById("consignorAddress").value = tempSplit[3];
            } else {
                var conName = document.getElementById("consignorNames" + val).value;
                var conMobile = document.getElementById("phoneNos" + val).value;
                var conAddress = document.getElementById("pointAddresss" + val).value;
                if (pointType == "Pick Up") {
                    document.getElementById("consignorName").value = conName;
                    document.getElementById("consignorPhoneNo").value = conMobile;
                    document.getElementById("consignorAddress").value = conAddress;
                }
            }

        }
        function setConsignee(val) {
            var custType = document.getElementById("customerTypeId").value;
            var pointType = document.getElementById('pointType' + val).value;
            if (custType == 1) {
                var temp = document.getElementById("consigneeIdstemp" + val).value;
                var tempSplit = temp.split('~');
                document.getElementById("consigneeIds" + val).value = tempSplit[0];
                document.getElementById("consigneeNames" + val).value = tempSplit[1];
                document.getElementById("consigneePhoneNos" + val).value = tempSplit[2];
                document.getElementById("consigneeAddresss" + val).value = tempSplit[3];
                document.getElementById("pointAddresss" + val).value = tempSplit[3];
                document.getElementById("deliveryAddress" + val).value = tempSplit[3];
                document.getElementById("phoneNos" + val).value = tempSplit[2];
                document.getElementById("consigneeName").value = tempSplit[1];
                document.getElementById("consigneePhoneNo").value = tempSplit[2];
                document.getElementById("consigneeAddress").value = tempSplit[3];
                document.getElementById("deliveryAddress").value = tempSplit[3];
            } else {

                var conName = document.getElementById("consigneeNames" + val).value;
                var conMobile = document.getElementById("phoneNos" + val).value;
                var conAddress = document.getElementById("pointAddresss" + val).value;
                if (pointType == "Drop") {
                    document.getElementById("consigneeName").value = conName;
                    document.getElementById("consigneePhoneNo").value = conMobile;
                    document.getElementById("consigneeAddress").value = conAddress;
                }
            }
        }

//        function addRouteCourse3(id, name, order, type, routeId, routeKm, routeReeferHr, startDate) {
//
//
//            var custName = document.getElementById("customerName").value;
//            var custId = document.getElementById("customerId").value;
//
//            var timer = 24;
//            new Date($.now());
//            var dt = new Date();
//            var time = dt.getHours();
//            var time1 = dt.getMinutes();
//            var time2 = dt.getSeconds();
//            //            alert(time1);
//            var date = document.cNote.startDate.value;
//            
//            var curr_hour, curr_minute;
//            curr_hour = dt.getHours();
//            curr_minute = dt.getMinutes();
//            if(parseInt(curr_hour) < 10){
//                curr_hour = "0"+curr_hour;
//            }
//            if(parseInt(curr_minute) < 10){
//                curr_minute = "0"+curr_minute;
//            }
//
//            if (parseInt(podRowCount1) % 2 == 0)
//            {
//                styl = "text2";
//            } else {
//                styl = "text1";
//            }
//            podSno1++;
//            var tab = document.getElementById("routePlan");
//            var newrow = tab.insertRow(podRowCount1);
//            cell = newrow.insertCell(0);
//            var cell0 = "<td><input type='text' readonly  id='pointOrder" + podSno1 + "' name='pointOrder' class='form-control' value='" + order + "' style='width:60px'></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(1);
//            var cell0 = "<td><input type='text' readonly  id='pointName" + podSno1 + "' name='pointName' class='form-control' value='" + name + "' style='width:120px'><input type='hidden' id='pointTransitHrs" + podSno1 + "'  name='pointTransitHrs' value='' ><input type='hidden' id='pointRouteKm" + podSno1 + "'  name='pointRouteKm' value='" + routeKm + "' ><input type='hidden' id='pointRouteReeferHr" + podSno1 + "'  name='pointRouteReeferHr' value='" + routeReeferHr + "' ><input type='hidden' id='pointRouteId" + podSno1 + "'  name='pointRouteId' value='" + routeId + "' ><input type='hidden' id='pointId" + podSno1 + "'  name='pointId' value='" + id + "' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            if (type == 'select') {
//                cell = newrow.insertCell(2);
//                var cell0 = "<td><select name='pointType' id='pointType" + podSno1 + "' class='form-control'  style='width:120px'  onchange='hideColoumns(" + podSno1 + ",this.value)><option value='Pick Up'>Pick Up</option><option value='Drop'>Drop</option></select></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//            } else {
//                cell = newrow.insertCell(2);
//                var cell0 = "<td><input type='text' readonly  id='pointType" + podSno1 + "'  name='pointType' class='form-control' value='" + type + "' style='width:120px'></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//            }
//            if (type == 'Pick Up') {
//                cell = newrow.insertCell(3);
//                var cell0 = "<td><input type='text' value=''    name='consignorNames'  id='consignorNames" + podSno1 + "' onChange='setConsignor(" + podSno1 + ");'  class='form-control' style='width:120px'><input type='hidden'  value='' name='consignorIdstemp'  id='consignorIdstemp" + podSno1 + " class='form-control' style='width:120px'><a href='#' onclick='viewConsignorDetails();'>Manage Bill To</a>\n\
//    <input type='hidden' name='consignorIds' id='consignorIds" + podSno1 + "' value=''><input type='hidden' name='consignorPhoneNos' id='consignorPhoneNos" + podSno1 + "' value=''><input type='hidden' name='consignorAddresss' id='consignorAddresss" + podSno1 + "' value=''></td>\n\
//                <td><input type='hidden' name='consigneeIds' id='consigneeIds" + podSno1 + "' value='0'><input type='hidden'  value='NA'    name='consigneeNames'  id='consigneeNames" + podSno1 + "'   class='form-control' style='width:120px'><input type='hidden' name='consigneePhoneNos' id='consigneePhoneNos" + podSno1 + "' value='NA'><input type='hidden' name='consigneeAddresss' id='consigneeAddresss" + podSno1 + "' value='NA'></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//                callConsignorAjax(podSno1);
//            } else {
//                cell = newrow.insertCell(3);
//                var cell0 = "<td><td><input type='text'  value=''    name='consigneeNames'  id='consigneeNames" + podSno1 + "'   class='form-control' style='width:120px'><input type='hidden'   name='consigneeIdstemp'  id='consigneeIdstemp" + podSno1 + "' onChange='setConsignee(" + podSno1 + ");'  class='form-control' style='width:120px'><a href='#' onclick='viewConsigneeDetails();'>Manage Ship To</a>\n\
//            <input type='hidden' name='consigneeIds' id='consigneeIds" + podSno1 + "' value=''><input type='hidden' name='consigneePhoneNos' id='consigneePhoneNos" + podSno1 + "' value=''><input type='hidden' name='consigneeAddresss' id='consigneeAddresss" + podSno1 + "' value=''></td>\n\
//                <td><input type='hidden' name='consignorIds' id='consignorIds" + podSno1 + "' value='0'><input type='hidden' value='NA'    name='consignorNames'  id='consignorNames" + podSno1 + "'   class='form-control' style='width:120px'><input type='hidden' name='consignorPhoneNos' id='consignorPhoneNos" + podSno1 + "' value='NA'><input type='hidden' name='consignorAddresss' id='consignorAddresss" + podSno1 + "' value='NA'></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//                callConsigneeAjax(podSno1);
//            }
//
//
//            cell = newrow.insertCell(4);
//            var cell0 = "<td><input type='hidden' name='deliveryAddress' id='deliveryAddress" + podSno1 + "'  rows='2' cols='20' class='form-control' style='width:120px' ><textarea name='pointAddresss' id='pointAddresss" + podSno1 + "'  rows='2' cols='20' class='form-control' style='width:120px'></textarea></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//
//            cell = newrow.insertCell(5);
//            var cell0 = "<td><input type='text'  id='grossWG" + podSno1 + "'  name='grossWG' class='form-control' value='0' style='width:120px' onKeyPress='return onKeyPressBlockCharacters(event);'><input type='hidden'  id='phoneNos" + podSno1 + "'  name='phoneNos' class='form-control' value='0' style='width:120px' onKeyPress='return onKeyPressBlockCharacters(event);'></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//
//            cell = newrow.insertCell(6);
//            var cell0 = "<td><input type='text'   value=''    name='weightKG'  id='weightKG" + podSno1 + "'   class='form-control ' style='width:120px' onKeyPress='return onKeyPressBlockCharacters(event);' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(7);
//            var cell0 = "<td><input type='text'   value=''    name='gcnNos'  id='gcnNos" + podSno1 + "'   class='form-control ' style='width:120px' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(8);
//            var cell0 = "<td><input type='text'   value='" + date + "'    name='consigDate'  id='consigDate" + podSno1 + "'   class='form-control datepicker' style='width:120px' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(9);
//            var cell0 = "<td><input type='text'   value=''    name='invoicedNo'  id='invoicedNo" + podSno1 + "'   class='form-control ' style='width:120px' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(10);
//            var cell0 = "<td><input type='text'   value='" + date + "'    name='custRefDate'  id='custRefDate" + podSno1 + "'   class='form-control datepicker' style='width:120px' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            $('.datepicker').datepicker();
//            if (type == 'Pick Up') {
//                cell = newrow.insertCell(11);
//                var cell0 = "<td><input type='text' onChange='validateTripSchedule();'  value='" + startDate + "'    name='pointPlanDate'  id='pointPlanDate" + podSno1 + "'   class='form-control datepicker' style='width:120px'></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//                $('.datepicker').datepicker();
//            } else {
//                cell = newrow.insertCell(11);
//                var cell0 = "<td><input type='text' onChange='validateTransitTime(" + podSno1 + ");'  value='" + startDate + "'    name='pointPlanDate'  id='pointPlanDate" + podSno1 + "'   class='form-control datepicker' style='width:120px'></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//                $('.datepicker').datepicker();
//            }
//
//            if (type == 'Pick Up') {
//                cell = newrow.insertCell(12);
//                var cell0 = "<td style='width:180px'>HH:<select name='pointPlanHour' onChange='validateTripSchedule();'   id='pointPlanHour" + podSno1 + "'   class='selectbox' style='width:60px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute" + podSno1 + "'   class='selectbox' style='width:60px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option></select></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
////                $('#pointPlanHour' + podSno1).val(dt.getHours());
////                $('#pointPlanMinute' + podSno1).val(dt.getMinutes());
//                $('#pointPlanHour' + podSno1).val(curr_hour);
//                $('#pointPlanMinute' + podSno1).val(curr_minute);
//            } else {
//                cell = newrow.insertCell(12);
//                var cell0 = "<td style='width:180px'>HH:<select name='pointPlanHour' onChange='validateTransitTime(" + podSno1 + ");'   id='pointPlanHour" + podSno1 + "'   class='selectbox' style='width:60px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute" + podSno1 + "'   class='selectbox' style='width:60px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option></select></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
////                $('#pointPlanHour' + podSno1).val(dt.getHours());
////                $('#pointPlanMinute' + podSno1).val(dt.getMinutes());
//                $('#pointPlanHour' + podSno1).val(curr_hour);
//                $('#pointPlanMinute' + podSno1).val(curr_minute);
//            }
//
//
//            
//            podRowCount1++;
//            $(document).ready(function() {
//                $("#datepicker").datepicker({
//                    showOn: "button",
//                    buttonImage: "calendar.gif",
//                    buttonImageOnly: true
//
//                });
//            });
//            $(function() {
//                $(".datepicker").datepicker({
//                    changeMonth: true, changeYear: true
//                });
//            });
//
//        }
//        function addRouteCourse1a(id, name, order, type, routeId) {
//
//            if (parseInt(podRowCount1) % 2 == 0)
//            {
//                styl = "text2";
//            } else {
//                styl = "text1";
//            }
//            podSno1++;
//            var tab = document.getElementById("routePlan");
//            var newrow = tab.insertRow(podRowCount1);
//            //            var cell = newrow.insertCell(0);
//            //            var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
//            //            cell.setAttribute("className", styl);
//            //            cell.innerHTML = cell0;
//
//
//
//            cell = newrow.insertCell(0);
//            var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointOrder" + podSno1 + "' name='pointOrder' class='form-control' value='" + order + "' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(1);
//            var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointName" + podSno1 + "' name='pointName' class='form-control' value='" + name + "' ><input type='hidden' id='pointTransitHrs" + podSno1 + "'  name='pointTransitHrs' value='' ><input type='hidden' id='pointRouteKm" + podSno1 + "'  name='pointRouteKm' value='' ><input type='hidden' id='pointRouteReeferHr" + podSno1 + "'  name='pointRouteReeferHr' value='' ><input type='hidden' id='pointRouteId" + routeId + "'  name='pointRouteId' value='" + id + "' ><input type='hidden' id='pointId" + podSno1 + "'  readonly name='pointId' class='form-control' value='" + id + "' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            if (type == 'select') {
//                cell = newrow.insertCell(2);
//                var cell0 = "<td class='text1' height='25' ><select name='pointType' id='pointType" + podSno1 + "' class='form-control'><option value='Pick Up'>Pick Up</option><option value='Drop'>Drop</option></select></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//            } else {
//                cell = newrow.insertCell(2);
//                var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointType" + podSno1 + "'  name='pointType' class='form-control' value='" + type + "' ></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//            }
//            cell = newrow.insertCell(3);
//            var cell0 = "<td class='text1' height='25' ><textarea name='pointAddresss' rows='2' cols='20' class='form-control'  style='width:120px' ></textarea></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(4);
//            var cell0 = "<td><textarea name='deliveryAddress' id='deliveryAddress" + podSno1 + "'  rows='2' cols='20' class='form-control' style='width:120px' ></textarea></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(5);
//            var cell0 = "<td><input type='text'   value=''    name='weightKG'  id='weightKG" + podSno1 + "'   class='form-control ' style='width:120px' onKeyPress='return onKeyPressBlockCharacters(event);' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(6);
//            var cell0 = "<td><input type='text'   value=''    name='invoicedNo'  id='invoicedNo" + podSno1 + "'   class='form-control ' style='width:120px'></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(7);
//            var cell0 = "<td class='text1' height='25' ><input type='text'  value='" + startDate + "'   readonly name='pointPlanDate'  id='pointPlanDate" + podSno1 + "'   class='datepicker' ></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            cell = newrow.insertCell(8);
//            var cell0 = "<td class='text1' height='25' >HH:<select name='pointPlanHour'  id='pointPlanHour" + podSno1 + "'   class='form-control'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute" + podSno1 + "'   class='form-control'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;
//            podRowCount1++;
//            $(document).ready(function() {
//                $("#datepicker").datepicker({
//                    showOn: "button",
//                    buttonImage: "calendar.gif",
//                    buttonImageOnly: true
//
//                });
//            });
//            $(function() {
//                $(".datepicker").datepicker({
//                    changeMonth: true, changeYear: true
//                });
//            });
//        }

        function setType(val) {
            var pointType = document.getElementById('pointType' + val).value;
            var custType = document.getElementById("customerTypeId").value;
            if (pointType == "Pick Up") {
                if (custType == 1) {
                    document.getElementById('con1' + val).style.display = 'block';
                    document.getElementById('con2' + val).style.display = 'none';
                    document.getElementById('consigneeNames' + val).style.display = 'none';
                    document.getElementById('consignorNames' + val).style.display = 'block';
                    callConsignorAjax(val);
                } else {
                    document.getElementById('consigneeNames' + val).style.display = 'none';
                    document.getElementById('consignorNames' + val).style.display = 'block';
                }
            } else if (pointType == "Drop") {
                if (custType == 1) {
                    document.getElementById('consigneeNames' + val).style.display = 'block';
                    document.getElementById('consignorNames' + val).style.display = 'none';
                    document.getElementById('con1' + val).style.display = 'none';
                    document.getElementById('con2' + val).style.display = 'block';
                    callConsigneeAjax(val);
                } else {
                    document.getElementById('consigneeNames' + val).style.display = 'block';
                    document.getElementById('consignorNames' + val).style.display = 'none';
                }
            } else {
                if (custType == 1) {
                    document.getElementById('consigneeNames' + val).style.display = 'none';
                    document.getElementById('consignorNames' + val).style.display = 'none';
                } else {
                    document.getElementById('consigneeNames' + val).style.display = 'none';
                    document.getElementById('consignorNames' + val).style.display = 'none';
                }
            }
        }

//        function addRouteCourse2() {
//            var custType = document.getElementById("customerTypeId").value;
//            var billingTypeId = document.getElementById("billingTypeId").value;
//            var startDate = document.cNote.startDate.value;
//            //alert(podRowCount1);
//            var timer = 24;
//            new Date($.now());
//            var dt = new Date();
//            var time = dt.getHours();
//            var time1 = dt.getMinutes();
//            var time2 = dt.getSeconds();
//            
//            var curr_hour, curr_minute;
//            curr_hour = dt.getHours();
//            curr_minute = dt.getMinutes();
//            if(parseInt(curr_hour) < 10){
//                curr_hour = "0"+curr_hour;
//            }
//            if(parseInt(curr_minute) < 10){
//                curr_minute = "0"+curr_minute;
//            }
//            
//            
//            if (parseInt(podRowCount1) % 2 == 0)
//            {
//                styl = "text2";
//            } else {
//                styl = "text1";
//            }
//
//            var pointOrderValue = document.getElementById("pointOrder2").value;
//            var prevRowRouteNameValue = document.getElementById("pointName" + parseInt(podRowCount1 - 1)).value;
//            var prevRowPointIdValue = document.getElementById("pointId" + parseInt(podRowCount1 - 1)).value;
//            var prevRowRouteOrderValue = document.getElementById("pointOrder" + parseInt(podRowCount1 - 1)).value;
//            if ((prevRowRouteNameValue.trim() == '' || prevRowPointIdValue == '') && billingTypeId != 2) {
//                alert('please enter the enroute name for route order ' + prevRowRouteNameValue);
//                document.getElementById("pointName" + parseInt(podRowCount1 - 1)).focus();
//            } else {
//
//                podSno1++;
//                var tab = document.getElementById("routePlan");
//                var newrow = tab.insertRow(podRowCount1 - 1);
//                //alert(pointOrderValue);
//                pointOrderValue = parseInt(pointOrderValue) + 1;
//                document.getElementById("pointOrder2").value = pointOrderValue;
//                pointOrderValue = parseInt(pointOrderValue) - 1;
//                //            var cell = newrow.insertCell(0);
//                //            var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
//                //            cell.setAttribute("className", styl);
//                //            cell.innerHTML = cell0;
//
//
//                cell = newrow.insertCell(0);
//                //alert("podSno1:"+podSno1);
//                var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointOrder" + podSno1 + "' name='pointOrder' class='form-control' value='" + pointOrderValue + "' ></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//                cell = newrow.insertCell(1);
//                var cell0 = "<td class='text1' height='25' ><input type='text' id='pointName" + podSno1 + "' onChange='validateRoute(" + podSno1 + ");'  name='pointName' class='form-control' value='' ><input type='hidden' id='pointTransitHrs" + podSno1 + "'  name='pointTransitHrs' value='0' ><input type='hidden' id='pointRouteKm" + podSno1 + "'  name='pointRouteKm' value='0' ><input type='hidden' id='pointRouteReeferHr" + podSno1 + "'  name='pointRouteReeferHr' value='0' ><input type='hidden' id='pointRouteId" + podSno1 + "'  name='pointRouteId' value='0' ><input type='hidden' id='pointId" + podSno1 + "'  readonly name='pointId' class='form-control' value='0' ></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//                callAjax(podSno1);
//                cell = newrow.insertCell(2);
//                var cell0 = "<td class='text1' height='25' ><select name='pointType' id='pointType" + podSno1 + "' class='form-control' onChange='setType(" + podSno1 + ");hideColoumns(" + podSno1 + ",this.value)'><option value='NA'>---Select----</option><option value='Pick Up'>Pick Up</option><option value='Drop'>Drop</option></select></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//                if (custType == 1) {
//                    cell = newrow.insertCell(3);
//                    var cell0 = "<td  style='display:none'>\n\
//                    <input type='text' value='NA'    name='consignorNames'  id='consignorNames" + podSno1 + "'   class='form-control' style='width:120px'><input type='hidden'  value='' name='consignorIdstemp'  id='consignorIdstemp" + podSno1 + "'  onChange='setConsignor(" + podSno1 + ");' class='form-control' style='width:120px'><div id='con1" + podSno1 + "' style='display:none'><a href='#' onclick='viewConsignorDetails();'>Manage Bill To</a></div><input type='hidden'  value='0'  name='consignorIds'  id='consignorIds" + podSno1 + "'   class='form-control' style='width:120px'>\n\
//                    <input type='hidden' name='consignorPhoneNos' id='consignorPhoneNos" + podSno1 + "' value='NA'>\n\
//                    <input type='hidden' name='consignorAddresss' id='consignorAddresss" + podSno1 + "' value='NA'></td>\n\
//                    <td  style='display:none'><input type='text'  value='NA'  name='consigneeNames'  id='consigneeNames" + podSno1 + "'   class='form-control' style='width:120px'><input type='hidden'  value='' name='consigneeIdstemp'  id='consigneeIdstemp" + podSno1 + "'  onChange='setConsignee(" + podSno1 + ");' class='form-control' style='width:120px'><div id='con2" + podSno1 + "' style='display:none'><a href='#' onclick='viewConsigneeDetails();'>Manage Ship To</a></div><input type='hidden'  value='0'  name='consigneeIds'  id='consigneeIds" + podSno1 + "'   class='form-control' style='width:120px'><input type='hidden' name='consigneePhoneNos' id='consigneePhoneNos" + podSno1 + "' value='NA'><input type='hidden' name='consigneeAddresss' id='consigneeAddresss" + podSno1 + "' value='NA'></td>";
//                    cell.setAttribute("className", styl);
//                    cell.innerHTML = cell0;
//                } else {
//                    cell = newrow.insertCell(3);
//                    var cell0 = "<td  style='display:none'>\n\
//           <input type='hidden' name='consignorIds' id='consignorIds" + podSno1 + "' value='0'><input type='text' value='NA'    name='consignorNames'  id='consignorNames" + podSno1 + "'   class='form-control' style='width:120px'><input type='hidden' name='consignorPhoneNos' id='consignorPhoneNos" + podSno1 + "' value='NA'><input type='hidden' name='consignorAddresss' id='consignorAddresss" + podSno1 + "' value='NA'></td>\n\
//                <td  style='display:none'><input type='hidden' name='consigneeIds' id='consigneeIds" + podSno1 + "' value='0'><input type='text'  value='NA'    name='consigneeNames'  id='consigneeNames" + podSno1 + "'   class='form-control' style='width:120px'><input type='hidden' name='consigneePhoneNos' id='consigneePhoneNos" + podSno1 + "' value='NA'><input type='hidden' name='consigneeAddresss' id='consigneeAddresss" + podSno1 + "' value='NA'></td>";
//                    cell.setAttribute("className", styl);
//                    cell.innerHTML = cell0;
//                }
//                cell = newrow.insertCell(4);
//                var cell0 = "<td class='text1' height='25' ><input type='hidden' name='deliveryAddress' id='deliveryAddress" + podSno1 + "'  rows='2' cols='20' class='form-control' style='width:120px' ><textarea id='pointAddresss" + podSno1 + "' name='pointAddresss' rows='2' cols='20' class='form-control' ></textarea></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//
//                cell = newrow.insertCell(5);
//                var cell0 = "<td ><input type='text'  id='grossWG" + podSno1 + "'  name='grossWG' class='form-control' value='0' style='width:120px' onKeyPress='return onKeyPressBlockCharacters(event);'><input type='hidden'  value='0'    name='phoneNos'  id='phoneNos" + podSno1 + "'   class='form-control' style='width:120px' onKeyPress='return onKeyPressBlockCharacters(event);'></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//
//                cell = newrow.insertCell(6);
//                var cell0 = "<td><input type='text'   value=''    name='weightKG'  id='weightKG" + podSno1 + "'   class='form-control ' style='width:120px' onKeyPress='return onKeyPressBlockCharacters(event);' ></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//                cell = newrow.insertCell(7);
//                var cell0 = "<td><input type='text'   value=''    name='gcnNos'  id='gcnNos" + podSno1 + "'   class='form-control ' style='width:120px' ></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//                cell = newrow.insertCell(8);
//                var cell0 = "<td><input type='text'   value='" + startDate + "'    name='consigDate'  id='consigDate" + podSno1 + "'   class='form-control datepicker' style='width:120px' ></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//                cell = newrow.insertCell(9);
//                var cell0 = "<td><input type='text'   value=''    name='invoicedNo'  id='invoicedNo" + podSno1 + "'   class='form-control ' style='width:120px' ></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//                cell = newrow.insertCell(10);
//                var cell0 = "<td><input type='text'   value='" + startDate + "'    name='custRefDate'  id='custRefDate" + podSno1 + "'   class='form-control datepicker' style='width:120px' ></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//                //                $('.datepicker').datepicker();
//
//
//                cell = newrow.insertCell(11);
//                var cell0 = "<td class='text1' height='25' ><input type='text'  value=''  onChange='validateTransitTime(" + podSno1 + ");'      name='pointPlanDate'  id='pointPlanDate" + podSno1 + "'   class='form-control datepicker' style='width:120px'></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//                $('.datepicker').datepicker();
//                cell = newrow.insertCell(12);
//                var cell0 = "<td class='text1' height='25' >HH:<select name='pointPlanHour'  onChange='validateTransitTime(" + podSno1 + ");'    id='pointPlanHour" + podSno1 + "'   class='selectbox' style='width:60px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute" + podSno1 + "'   class='selectbox' style='width:60px'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//
//                cell = newrow.insertCell(13);
//                var cell0 = "<td class='text1' height='25' align='left'><input type='button' class='btn btn-info'  style='width:90px;height:30px;font-weight: bold;padding: 1px;'  name='delete'  id='delete" + podSno1 + "'   value='Delete Row' onclick='deleteRow1(this);' ></td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//
//
////                $('#pointPlanHour' + podSno1).val(dt.getHours());
////                $('#pointPlanMinute' + podSno1).val(dt.getMinutes());
//                $('#pointPlanHour' + podSno1).val(curr_hour);
//                $('#pointPlanMinute' + podSno1).val(curr_minute);   
//                podRowCount1++;
//                document.getElementById('pointName' + podSno1).focus();
//                $(document).ready(function() {
//                    $("#datepicker").datepicker({
//                        showOn: "button",
//                        buttonImage: "calendar.gif",
//                        buttonImageOnly: true
//
//                    });
//                });
//                $(function() {
//                    $(".datepicker").datepicker({
//                        changeMonth: true, changeYear: true
//                    });
//                });
//            }
//        }





    </script>
    <script>
        function deleteRow1(src) {
            //alert("podSno1:"+podSno1);
            podSno1--;
            podRowCount1--;
            var oRow = src.parentNode.parentNode;
            var dRow = document.getElementById('routePlan');
            //alert(oRow.rowIndex);
            dRow.deleteRow(oRow.rowIndex);
            document.getElementById("pointOrder2").value--;
            //document.getElementById("selectedRowCount").value--;
        }
    </script>


    <script type="text/javascript">

        var httpReqRouteCheck;
        var temp = "";
        function ajaxRouteCheck(pointIdValue, pointIdPrev, pointIdNext, val, pointName, pointNamePrev, pointNameNext)
        {

            var url = "/throttle/checkForRoute.do?pointIdValue=" + pointIdValue + "&pointIdPrev=" + pointIdPrev + "&pointIdNext=" + pointIdNext;
            if (pointIdValue != '') {
                if (window.ActiveXObject)
                {
                    httpReqRouteCheck = new ActiveXObject("Microsoft.XMLHTTP");
                } else if (window.XMLHttpRequest)
                {
                    httpReqRouteCheck = new XMLHttpRequest();
                }
                httpReqRouteCheck.open("GET", url, true);
                httpReqRouteCheck.onreadystatechange = function() {
                    processAjaxRouteCheck(val, pointName, pointNamePrev, pointNameNext);
                };
                httpReqRouteCheck.send(null);
            } else {
                alert("invalid interim point: " + pointName);
                document.getElementById("pointName" + val).value = '';
                document.getElementById("pointName" + val).focus();
            }
        }

        function processAjaxRouteCheck(val, pointName, pointNamePrev, pointNameNext)
        {
            if (httpReqRouteCheck.readyState == 4)
            {
                if (httpReqRouteCheck.status == 200)
                {
                    temp = httpReqRouteCheck.responseText.valueOf();
                    //                    alert(temp);
                    var tempVal = temp.split('~');
                    if (tempVal[0] == 0) {
                        alert("valid route does not exists for the selected point: " + pointName);
                        document.getElementById("pointName" + val).value = '';
                        document.getElementById("pointName" + val).focus();
                    }
                    if (tempVal[0] == 1) {
                        alert("valid route does not exists between " + pointName + " and " + pointNameNext);
                        document.getElementById("pointName" + val).value = '';
                        document.getElementById("pointName" + val).focus();
                    }
                    if (tempVal[0] == 2) {
                        document.getElementById("pointOrder" + val).focus();
                        var tempValSplit = tempVal[1].split("-");
                        document.getElementById("pointRouteId" + (val - 1)).value = tempValSplit[0];
                        document.getElementById("pointRouteKm" + (val - 1)).value = tempValSplit[1];
                        document.getElementById("pointRouteReeferHr" + (val - 1)).value = tempValSplit[2];
                        document.getElementById("pointTransitHrs" + (val - 1)).value = tempValSplit[3];
                        tempValSplit = tempVal[2].split("-");
                        document.getElementById("pointRouteId" + (val + 1)).value = tempValSplit[0];
                        document.getElementById("pointRouteKm" + (val + 1)).value = tempValSplit[1];
                        document.getElementById("pointRouteReeferHr" + (val + 1)).value = tempValSplit[2];
                        document.getElementById("pointTransitHrs" + (val + 1)).value = tempValSplit[3];
                    }
                } else
                {
                    alert("Error loading page\n" + httpReqRouteCheck.status + ":" + httpReqRouteCheck.statusText);
                }
            }
        }




        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true,
                dateFormat: "dd-mm-yy",
                altFormat: "dd-mm-yy"

            });
        });
        $(function() {
            $(".datepicker").datepicker({
                changeMonth: true, changeYear: true
            });
        });</script>
    <script type="text/javascript" language="javascript">
        var poItems = 0;
        var rowCount = '';
        var snumber = '';
        function addAllowanceRow()
        {
            if (snumber < 19) {
                var tab = document.getElementById("expenseTBL");
                var rowCount = tab.rows.length;
                snumber = parseInt(rowCount) - 1;
                var newrow = tab.insertRow(parseInt(rowCount) - 1);
                newrow.height = "30px";
                // var temp = sno1-1;
                var cell = newrow.insertCell(0);
                var cell0 = "<td><input type='hidden'  name='itemId' /> " + snumber + "</td>";
                //cell.setAttribute(cssAttributeName,"text1");
                cell.innerHTML = cell0;
                cell = newrow.insertCell(1);
                cell0 = "<td class='text1'><input name='driName' type='text' class='form-control' id='driName' onkeyup='getDriverName(" + snumber + ")' /></td>";
                //cell.setAttribute(cssAttributeName,"text1");
                cell.innerHTML = cell0;
                cell = newrow.insertCell(2);
                cell0 = "<td class='text1'><input name='cleanerName' type='text' class='form-control' id='cleanerName' /></td>";
                //cell.setAttribute(cssAttributeName,"text1");
                cell.innerHTML = cell0;
                cell = newrow.insertCell(3);
                cell0 = "<td class='text1'><input name='date' type='text' id='date' class='datepicker' /></td>";
                //cell.setAttribute(cssAttributeName,"text1");
                cell.innerHTML = cell0;
                // rowCount++;

                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            }
            document.cNote.expenseId.value = snumber;
        }

        function delAllowanceRow() {
            try {
                var table = document.getElementById("expenseTBL");
                rowCount = table.rows.length - 1;
                for (var i = 2; i < rowCount; i++) {
                    var row = table.rows[i];
                    var checkbox = row.cells[6].childNodes[0];
                    if (null != checkbox && true == checkbox.checked) {
                        if (rowCount <= 1) {
                            alert("Cannot delete all the rows");
                            break;
                        }
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                        sno--;
                    }
                }
                document.cNote.expenseId.value = sno;
            } catch (e) {
                alert(e);
            }
        }
        function parseDouble(value) {
            if (typeof value == "string") {
                value = value.match(/^-?\d*/)[0];
            }
            return !isNaN(parseInt(value)) ? value * 1 : NaN;
        }

        //        function estimateFreight() {
        //            var billingTypeId = document.getElementById("billingTypeId").value;
        //            var customerTypeId = document.getElementById("customerTypeId").value;
        //            //customertypeid 1-> contract; 2-> walk in
        //            //billling type id 1 -> point to point; 2 -> point to point based on wt; 3 -> actual weight
        //            var totalKm = 0;
        //            var totalHr = 0;
        //            var rateWithReefer = 0;
        //            var rateWithoutReefer = 0;
        //            var minbaseWeight = 0;
        //            if (customerTypeId == 1 && billingTypeId == 1) {//point to point
        //                var temp = document.cNote.vehTypeIdContractTemp.value;
        //                if (temp == 0) {
        //                    alert("Freight cannot be estimated as the vehicle type is not chosen");
        //                } else {
        //                    var temp = document.cNote.vehTypeIdContractTemp.value;
        //                    //alert(temp);
        //                    var tempSplit = temp.split("-");
        //                    rateWithReefer = tempSplit[5];
        ////                    alert(rateWithReefer);
        //                    rateWithoutReefer = tempSplit[6];
        //                    totalKm = tempSplit[2];
        //                    totalHr = tempSplit[3];
        //                    minbaseWeight = tempSplit[9];
        //
        //                    if (document.cNote.reeferRequired.value == 'Yes') {
        //                        document.cNote.totFreightAmount.value = rateWithReefer;
        //                        document.cNote.editFreightAmount.value = rateWithReefer;
        //                    } else {
        //                        document.cNote.totFreightAmount.value = rateWithoutReefer;
        //                        document.cNote.editFreightAmount.value = rateWithoutReefer;
        //                    }
        //
        //
        //
        //                    document.cNote.totalKm.value = totalKm;
        //                    document.cNote.totalHours.value = totalHr;
        //                }
        //            } else if (customerTypeId == 1 && billingTypeId == 2) {//point to point weight
        //                var temp = document.cNote.vehTypeIdContractTemp.value;
        //                var totalWeight = document.cNote.totalWeightage.value;
        //                if (totalWeight.trim() == '') {
        //                    alert("Freight cannot be estimated as the order weight is not available");
        //                } else {
        //
        //                    if (temp == 0) {
        //
        //                        alert("Freight cannot be estimated as the vehicle type is not chosen");
        //                    } else {
        //                        var tempSplit = temp.split("-");
        //                        rateWithReefer = tempSplit[7];
        //                        rateWithoutReefer = tempSplit[8];
        //                        totalKm = tempSplit[2];
        //                        totalHr = tempSplit[3];
        //                        minbaseWeight = tempSplit[9];
        //
        //                        if (document.cNote.reeferRequired.value == 'Yes') {
        //                            if (parseFloat(totalWeight) <= parseFloat(minbaseWeight)) {
        //                                document.cNote.totFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(minbaseWeight)).toFixed(2);
        //                                document.cNote.editFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(minbaseWeight)).toFixed(2);
        //                            }
        //                            else {
        //                                document.cNote.totFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(totalWeight)).toFixed(2);
        //                                document.cNote.editFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(totalWeight)).toFixed(2);
        //                            }
        //                        } else {
        //                            if (parseFloat(totalWeight) <= parseFloat(minbaseWeight)) {
        //                                document.cNote.totFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(minbaseWeight)).toFixed(2);
        //                                document.cNote.editFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(minbaseWeight)).toFixed(2);
        //                            }
        //                            else {
        //                                document.cNote.totFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(totalWeight)).toFixed(2);
        //                                document.cNote.editFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(totalWeight)).toFixed(2);
        //                            }
        //                        }
        //                        document.cNote.totalKm.value = totalKm;
        //                        document.cNote.totalHours.value = totalHr;
        //                    }
        //                }
        //            } else if (customerTypeId == 1 && billingTypeId == 3) {//actual kms
        //                var pointRouteKm = document.getElementsByName("pointRouteKm");
        //                var pointRouteReeferHr = document.getElementsByName("pointRouteReeferHr");
        //
        //                //calculate total km and total hrs
        //                for (var i = 0; i < pointRouteKm.length; i++) {
        //                    totalKm = totalKm + parseFloat(pointRouteKm[i].value);
        //                    totalHr = totalHr + parseFloat(pointRouteReeferHr[i].value);
        //                }
        //
        //                var temp = document.cNote.vehTypeIdTemp.value;
        //                if (temp == 0) {
        //                    alert("Freight cannot be estimated as the vehicle type is not chosen");
        //                } else {
        //                    var tempSplit = temp.split("-");
        //                    var vehicleRatePerKm = tempSplit[1];
        //                    var reeferRatePerHour = tempSplit[2];
        //
        //
        //                    var vehicleFreight = parseFloat(totalKm) * parseFloat(vehicleRatePerKm);
        //                    var reeferFreight = parseFloat(totalHr) * parseFloat(reeferRatePerHour);
        //
        //                    if (document.cNote.reeferRequired.value == 'Yes') {
        //                        document.cNote.totFreightAmount.value = (parseFloat(vehicleFreight) + parseFloat(reeferFreight)).toFixed(2);
        //                    } else {
        //                        document.cNote.totFreightAmount.value = parseFloat(vehicleFreight).toFixed(2);
        //                    }
        //                }
        //                document.cNote.totalKm.value = totalKm;
        //                document.cNote.totalHours.value = totalHr;
        //            } else if (customerTypeId == 2) { //walk in
        //
        //                var pointRouteKm = document.getElementsByName("pointRouteKm");
        //                var pointRouteReeferHr = document.getElementsByName("pointRouteReeferHr");
        //                var walkInBillingTypeId = document.cNote.walkInBillingTypeId.value;
        ////                alert(walkInBillingTypeId);
        //
        //                //calculate total km and total hrs
        //                for (var i = 0; i < pointRouteKm.length; i++) {
        //                    totalKm = totalKm + parseFloat(pointRouteKm[i].value);
        //                    totalHr = totalHr + parseFloat(pointRouteReeferHr[i].value);
        ////                    alert("totalKm==" + totalKm);
        ////                    alert("totalHr===" + totalHr);
        //                }
        //                if (walkInBillingTypeId == 1) {//fixed rate
        //                    rateWithReefer = document.cNote.walkinFreightWithReefer.value;
        //                    rateWithoutReefer = document.cNote.walkinFreightWithoutReefer.value;
        //                    if (document.cNote.reeferRequired.value == 'Yes') {
        //                        document.cNote.totFreightAmount.value = parseFloat(rateWithReefer).toFixed(2);
        //                    } else {
        //                        document.cNote.totFreightAmount.value = parseFloat(rateWithoutReefer).toFixed(2);
        //                    }
        //                } else if (walkInBillingTypeId == 3) {//rate per km
        //                    rateWithReefer = document.cNote.walkinRateWithReeferPerKm.value;
        //                    rateWithoutReefer = document.cNote.walkinRateWithoutReeferPerKm.value;
        ////                    alert("rateWithReefer" + rateWithReefer);
        //
        //                    if (document.cNote.reeferRequired.value == 'Yes') {
        //                        document.cNote.totFreightAmount.value = (parseFloat(totalKm) * parseFloat(rateWithReefer)).toFixed(2);
        ////                        alert("sss==" + document.cNote.totFreightAmount.value);
        //                    } else {
        //                        document.cNote.totFreightAmount.value = (parseFloat(totalKm) * parseFloat(rateWithoutReefer)).toFixed(2);
        ////                        alert("no==" + document.cNote.totFreightAmount.value);
        //                    }
        //                } else if (walkInBillingTypeId == 2) {// rate per kg
        //                    var totalWeight = document.cNote.totalWeightage.value;
        //                    rateWithReefer = document.cNote.walkinRateWithReeferPerKg.value;
        //                    rateWithoutReefer = document.cNote.walkinRateWithoutReeferPerKg.value;
        //                    if (document.cNote.reeferRequired.value == 'Yes') {
        //                        document.cNote.totFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(totalWeight)).toFixed(2);
        //                    } else {
        //                        document.cNote.totFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(totalWeight)).toFixed(2);
        //                    }
        //
        //                }
        //                document.cNote.totFreightAmount1.value = document.cNote.totFreightAmount.value;
        //                document.cNote.totalKm.value = totalKm;
        //                document.cNote.totalHours.value = totalHr;
        //            }
        //        }



        function estimateFreight() {
            var billingTypeId = document.getElementById("billingTypeId").value;
            var customerTypeId = document.getElementById("customerTypeId").value;
            //customertypeid 1-> contract; 2-> walk in
            //billling type id 1 -> point to point; 2 -> point to point based on wt; 3 -> actual weight
            var totalKm = 0;
            var totalHr = 0;
            var rateWithReefer = 0;
            var rateWithoutReefer = 0;
            var minbaseWeight = 0;
            if (customerTypeId == 1 && billingTypeId == 1) {//point to point
                var temp = document.cNote.vehicleIDTemp.value;
                if (temp == 0) {
//                    alert("Freight cannot be estimated as the vehicle type is not chosen");
                } else {
                    var temp = document.cNote.vehicleIDTemp.value;
                    //alert(temp);
                    var tempSplit = temp.split("-");
                    rateWithReefer = tempSplit[5];
                    rateWithoutReefer = tempSplit[6];
                    totalKm = tempSplit[2];
                    totalHr = tempSplit[3];
                    if (document.cNote.reeferRequired.value == 'Yes') {
                        document.cNote.totFreightAmount.value = rateWithReefer;
                        document.cNote.editFreightAmount.value = rateWithReefer;
                    } else {
                        document.cNote.totFreightAmount.value = rateWithoutReefer;
                        document.cNote.editFreightAmount.value = rateWithoutReefer;
                    }

                    document.cNote.totalKm.value = totalKm;
                    document.cNote.totalHours.value = totalHr;
                }
            } else if (customerTypeId == 1 && billingTypeId == 2) {//point to point weight
                var temp = document.cNote.vehicleIDTemp.value;
                var totalWeight = document.cNote.totalWeightage.value;
                var orderType = document.cNote.orderType.value;
                if (totalWeight.trim() == '') {
                    //                    alert("Freight cannot be estimated as the order weight is not available");
                } else {

                    if (temp == 0 && orderType == 2) {

                        alert("Freight cannot be estimated as the vehicle type is not chosen");
                    } else {
                        var tempSplit = temp.split("-");
                        rateWithReefer = tempSplit[7];
                        rateWithoutReefer = tempSplit[8];
                        minbaseWeight = tempSplit[9];
                        //                        alert(minbaseWeight);
                        //                        alert(rateWithReefer);
                        //                        alert(rateWithoutReefer);
                        totalKm = tempSplit[2];
                        totalHr = tempSplit[3];
                        if (document.cNote.reeferRequired.value == 'Yes') {
                            if (parseFloat(totalWeight) <= parseFloat(minbaseWeight)) {
                                document.cNote.totFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(minbaseWeight)).toFixed(2);
                                document.cNote.editFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(minbaseWeight)).toFixed(2);
                            }
                            else {
                                document.cNote.totFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(totalWeight)).toFixed(2);
                                document.cNote.editFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(totalWeight)).toFixed(2);
                            }
                        } else {
                            if (parseFloat(totalWeight) <= parseFloat(minbaseWeight)) {
                                document.cNote.totFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(minbaseWeight)).toFixed(2);
                                document.cNote.editFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(minbaseWeight)).toFixed(2);
                            }
                            else {
                                document.cNote.totFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(totalWeight)).toFixed(2);
                                document.cNote.editFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(totalWeight)).toFixed(2);
                            }
                        }
                        document.cNote.totalKm.value = totalKm;
                        document.cNote.totalHours.value = totalHr;
                    }
                }
            } else if (customerTypeId == 1 && billingTypeId == 3) {//actual kms
                var pointRouteKm = document.getElementsByName("pointRouteKm");
                var pointRouteReeferHr = document.getElementsByName("pointRouteReeferHr");
                //calculate total km and total hrs
                for (var i = 0; i < pointRouteKm.length; i++) {
                    totalKm = totalKm + parseFloat(pointRouteKm[i].value);
                    totalHr = totalHr + parseFloat(pointRouteReeferHr[i].value);
                }

                var temp = document.cNote.vehTypeIdTemp.value;
                if (temp == 0) {
                    alert("Freight cannot be estimated as the vehicle type is not chosen");
                } else {
                    var tempSplit = temp.split("-");
                    var vehicleRatePerKm = tempSplit[1];
                    var reeferRatePerHour = tempSplit[2];
                    var vehicleFreight = parseFloat(totalKm) * parseFloat(vehicleRatePerKm);
                    var reeferFreight = parseFloat(totalHr) * parseFloat(reeferRatePerHour);
                    if (document.cNote.reeferRequired.value == 'Yes') {
                        document.cNote.totFreightAmount.value = (parseFloat(vehicleFreight) + parseFloat(reeferFreight)).toFixed(2);
                    } else {
                        document.cNote.totFreightAmount.value = parseFloat(vehicleFreight).toFixed(2);
                    }
                }
                document.cNote.totalKm.value = totalKm;
                document.cNote.totalHours.value = totalHr;
            } else if (customerTypeId == 2) { //walk in

                var pointRouteKm = document.getElementsByName("pointRouteKm");
                var pointRouteReeferHr = document.getElementsByName("pointRouteReeferHr");
                var walkInBillingTypeId = document.cNote.walkInBillingTypeId.value;
                //calculate total km and total hrs
                for (var i = 0; i < pointRouteKm.length; i++) {
                    totalKm = totalKm + parseFloat(pointRouteKm[i].value);
                    totalHr = totalHr + parseFloat(pointRouteReeferHr[i].value);
                }
                if (walkInBillingTypeId == 1) {//fixed rate
                    rateWithReefer = document.cNote.walkinFreightWithReefer.value;
                    rateWithoutReefer = document.cNote.walkinFreightWithoutReefer.value;
                    if (document.cNote.reeferRequired.value == 'Yes') {
                        document.cNote.totFreightAmount.value = parseFloat(rateWithReefer).toFixed(2);
                    } else {
                        document.cNote.totFreightAmount.value = parseFloat(rateWithoutReefer).toFixed(2);
                    }
                } else if (walkInBillingTypeId == 3) {//rate per km
                    rateWithReefer = document.cNote.walkinRateWithReeferPerKm.value;
                    rateWithoutReefer = document.cNote.walkinRateWithoutReeferPerKm.value;
                    if (document.cNote.reeferRequired.value == 'Yes') {
                        document.cNote.totFreightAmount.value = (parseFloat(totalKm) * parseFloat(rateWithReefer)).toFixed(2);
                    } else {
                        document.cNote.totFreightAmount.value = (parseFloat(totalKm) * parseFloat(rateWithoutReefer)).toFixed(2);
                    }
                } else if (walkInBillingTypeId == 2) {// rate per kg
                    var totalWeight = document.cNote.totalWeightage.value;
                    rateWithReefer = document.cNote.walkinRateWithReeferPerKg.value;
                    rateWithoutReefer = document.cNote.walkinRateWithoutReeferPerKg.value;
                    if (document.cNote.reeferRequired.value == 'Yes') {
                        document.cNote.totFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(totalWeight)).toFixed(2);
                    } else {
                        document.cNote.totFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(totalWeight)).toFixed(2);
                    }

                }
                document.cNote.totFreightAmount1.value = document.cNote.totFreightAmount.value;
                document.cNote.totalKm.value = totalKm;
                document.cNote.totalHours.value = totalHr;
            }
        }

        function saveExp(obj) {
            document.cNote.buttonName.value = "save";
            obj.name = "none";
            if (document.cNote.expenseId.value != "") {
                document.cNote.action = '/throttle/saveDriverExpenses.do';
                document.cNote.submit();
            }
        }

        function getDriverName(sno) {
            var oTextbox = new AutoSuggestControl(document.getElementById("driName"), new ListSuggestions("driName", "/throttle/handleDriverSettlement.do?"));
        }
    </script>
    <script language="">
        function print(val)
        {
            var DocumentContainer = document.getElementById(val);
            var WindowObject = window.open('', "TrackHistoryData",
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }
    </script>
</head>
<script>
    function deleteRow(src) {
        sno1--;
        var oRow = src.parentNode.parentNode;
        var dRow = document.getElementById('addTyres1');
        dRow.deleteRow(oRow.rowIndex);
        document.getElementById("selectedRowCount").value--;
    }

</script>
<input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>
<script type="text/javascript">
    var rowCount = 1;
    var sno = 0;
    var rowCount1 = 1;
    var sno1 = 0;
    var httpRequest;
    var httpReq;
    var styl = "";
    function addRow1() {
        if (parseInt(rowCount1) % 2 == 0)
        {
            styl = "text2";
        } else {
            styl = "text1";
        }
        sno1++;
        var tab = document.getElementById("addTyres1");
        //find current no of rows
        var rowCountNew = document.getElementById('addTyres1').rows.length;
        rowCountNew--;
        var newrow = tab.insertRow(rowCountNew);
        var cell = newrow.insertCell(0);
        var cell0 = "<td> " + sno1 + "</td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;
        // Positions
        cell = newrow.insertCell(1);
        var cell0 = "<td><input type='text' name='productCodes' class='form-control' >";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;
        // TyreIds
        var cell = newrow.insertCell(2);
        var cell0 = "<td><input type='hidden' name='productNames' class='form-control'  value='NA'><input type='text' name='batchCode' class='form-control' >";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(3);
        var cell1 = "<td><input type='text' name='packagesNos'   onKeyPress='return onKeyPressBlockCharacters(event);' id='packagesNos' class='form-control' value='' onkeyup='calcTotalPacks(this.value)'>";
        cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
        cell.setAttribute("className", "text1");
        cell.innerHTML = cell1;
        cell = newrow.insertCell(4);
        var cell1 = "<td><select class='form-control' name='uom' id='uom'><option value='1' selected >Box</option> <option value='2'>Bag</option><option value='3'>Pallet</option><option value='4'>Each</option></select> </td>";
        cell.setAttribute("className", "text1");
        cell.innerHTML = cell1;
        cell = newrow.insertCell(5);
        var cell0 = "<td><input type='text' name='weights'  readonly onKeyPress='return onKeyPressBlockCharacters(event);' id='weights' class='form-control' value='' >";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;
//        cell = newrow.insertCell(6);
//        var cell0 = "<td class='text1' height='25' align='left'><input type='button' class='btn btn-info'  style='width:90px;height:30px;font-weight: bold;padding: 1px;'  name='delete'  id='delete" + sno1 + "'   value='Delete Row' onclick='deleteRow(this);' ></td>";
//        cell.innerHTML = cell0;
        rowCount1++;
    }


    function calculateTravelHours(sno) {
        var endDates = document.getElementById('endDates' + sno).value;
        var endTimeIds = document.getElementById('endTimeIds' + sno).value;
        var tempDate1 = endDates.split("-");
        var tempTime1 = endTimeIds.split(":");
        var stDates = document.getElementById('stDates' + sno).value;
        var stTimeIds = document.getElementById('stTimeIds' + sno).value;
        var tempDate2 = stDates.split("-");
        var tempTime2 = stTimeIds.split(":");
        var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0], tempTime2[0], tempTime2[1]); // Feb 1, 2011
        var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0], tempTime1[0], tempTime1[1]); // now
        var difference = thisTime.getTime() - prevTime.getTime(); // now - Feb 1
        var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
        document.getElementById('timeDifferences' + sno).value = hoursDifference;
    }

    function goToImportPage() {
        document.cNote.action = '/throttle/cnoteUploadPage.do';
        document.cNote.submit();
    }
</script>
<script>
    //endingPointIds
    function resetAddRow(sno) {
        if (document.getElementById('endingPointIds' + sno).value == document.getElementById('destination').value) {
            //           addRow
            document.getElementById('addRowDetails').style.display = 'none';
        }
    }
</script>

<%--
    <script>
        var v = "Roseindia-1,Vhghg-2,hgdhjsgdjsd-3,hkjsdhksjdhk-4,jhdkjshdksd-5";
    </script>
    <% ArrayList list  new ArrayList();
        String st="<script>document.writeln(v)</script>";
        String[] temp = st.split(",");
        list.add(temp[0]);
        list.add(temp[1]);
        list.add(temp[2]);
        list.add(temp[3]);
        list.add(temp[4]);
        out.println("value="+list.size());
    %>
--%>

<script>
    //start ajax for vehicle Nos
    function callAjaxNew(val) {
        // Use the .autocomplete() method to compile the list based on input from user
        //alert(val);
        var pointNameId = 'pointName' + val;
        var pointIdId = 'pointId' + val;
        var prevPointId = 'pointId' + (parseInt(val) - 1);
        var pointOrder = 'pointOrder' + val;
        var pointOrderVal = $('#' + pointOrder).val();
        var prevPointOrderVal = parseInt(pointOrderVal) - 1;
        //alert(pointOrderVal);
        //alert(prevPointOrderVal);
        var prevPointId = 0;
        var pointIds = document.getElementsByName("pointId");
        var pointOrders = document.getElementsByName("pointOrder");
        for (var m = 0; m < pointIds.length; m++) {
            //alert("loop value:"+pointOrders[m].value +" : "+pointIds[m].value );
            if (pointOrders[m].value == prevPointOrderVal) {
                //alert("am here:"+pointOrders[m].value +" : "+prevPointOrderVal);
                prevPointId = pointIds[m].value;
            }
        }


        $.ajax({
            url: "/throttle/getCityList.do",
            dataType: "json",
            data: {
                cityName: request.term,
                prevPointId: prevPointId,
                pointOrder: $('#' + pointOrder).val(),
                billingTypeId: $('#billingTypeId').val(),
                contractId: $('#contractId').val(),
                textBox: 1
            },
            success: function(data) {
                $.each(data, function(i, data) {
                    $('#originTemp').append(
                            $('<option style="width:150px"></option>').val(data.Id).html(data.Name)
                            )
                });
            }
        });
    }
    function callAjax(val) {
        // Use the .autocomplete() method to compile the list based on input from user
        //alert(val);
        var pointNameId = 'pointName' + val;
        var pointIdId = 'pointId' + val;
        var prevPointId = 'pointId' + (parseInt(val) - 1);
        var pointOrder = 'pointOrder' + val;
        var pointOrderVal = $('#' + pointOrder).val();
        var prevPointOrderVal = parseInt(pointOrderVal) - 1;
        //alert(pointOrderVal);
        //alert(prevPointOrderVal);
        var prevPointId = 0;
        var pointIds = document.getElementsByName("pointId");
        var pointOrders = document.getElementsByName("pointOrder");
        for (var m = 0; m < pointIds.length; m++) {
            //alert("loop value:"+pointOrders[m].value +" : "+pointIds[m].value );
            if (pointOrders[m].value == prevPointOrderVal) {
                //alert("am here:"+pointOrders[m].value +" : "+prevPointOrderVal);
                prevPointId = pointIds[m].value;
            }
        }
        //alert(prevPointId);
        $('#' + pointNameId).autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityList.do",
                    dataType: "json",
                    data: {
                        cityName: request.term,
                        prevPointId: prevPointId,
                        pointOrder: $('#' + pointOrder).val(),
                        billingTypeId: $('#billingTypeId').val(),
                        contractId: $('#contractId').val(),
                        customerTypeId: $('#customerTypeId').val(),
                        textBox: 1
                    },
                    success: function(data, textStatus, jqXHR) {
                        var contractType = document.cNote.billingTypeId.value;
                        //alert(contractType);
                        if (contractType == '1') {
                            if (data == '') {
                                $('#' + pointIdId).val(0);
                                alert('please enter valid interim point');
                                $('#' + pointNameId).val('');
                                $('#' + pointNameId).focus();
                            }
                        }

                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {

                        //console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                //alert(id+" : "+value);
                $('#' + pointIdId).val(id);
                $('#' + pointNameId).val(value);
                //validateRoute(val,value);

                return false;
            }

            // Format the list menu output of the autocomplete
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            //alert(item);
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    }


    //walkincustomerAjax

    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#walkinCustomerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getWalkinCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term,
                        customerCode: document.getElementById('customerCode').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#walkinCustomerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('~');
                $('#walkinCustomerId').val(tmp[0]);
                $itemrow.find('#walkinCustomerName').val(tmp[1]);
                $itemrow.find('#walkinCustomerCode').val(tmp[2]);
                $('#walkinCustomerAddress').val(tmp[3]);
                $('#walkinPincode').val(tmp[4]);
                $('#walkinCustomerPhoneNo').val(tmp[5]);
                $('#walkinCustomerMobileNo').val(tmp[6]);
                $('#walkinMailId').val(tmp[7]);
                $('#state').val(tmp[8]);
                //                alert("dfs" + tmp[8]);
                $('#gstNo').val(tmp[9]);
                //                document.getElementById('walkinCustomerName').readOnly = true;
                document.getElementById('walkinCustomerCode').readOnly = true;
                document.getElementById('walkinCustomerAddress').readOnly = true;
                document.getElementById('walkinPincode').readOnly = true;
                document.getElementById('walkinCustomerMobileNo').readOnly = true;
                document.getElementById('walkinCustomerPhoneNo').readOnly = true;
                document.getElementById('walkinMailId').readOnly = true;
//                document.getElementById('state').disabled = true;
                document.getElementById('gstNo').readOnly = true;
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
    //end ajax for vehicle Nos

    $(document).ready(function() {
        //     $('#custDetails').show();
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term,
                        customerCode: document.getElementById('customerCode').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                        //                        $('#custDetails').show();
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('~');
                $itemrow.find('#customerId').val(tmp[0]);
                $itemrow.find('#customerName').val(tmp[1]);
                $itemrow.find('#customerCode').val(tmp[2]);
                $('#customerAddress').text(tmp[3]);
                $('#pincode').val(tmp[4]);
                $('#customerMobileNo').val(tmp[6]);
                $('#customerPhoneNo').val(tmp[5]);
                $('#mailId').val(tmp[7]);
                $('#billingTypeId').val(tmp[8]);
                $('#billingTypeName').text(tmp[9]);
                $('#billTypeName').text(tmp[9]);
                $('#contractNo').text(tmp[10]);
                $('#contractExpDate').text(tmp[11]);
                $('#contractId').val(tmp[12]);
                $('#paymentType').val(tmp[13]);
                //                $('#consignorName').val(tmp[1]);
                //                $('#consignorPhoneNo').val(tmp[6]);
                //                $('#consignorAddress').val(tmp[3]);
                //                $('#consigneeName').val(tmp[1]);
                //                $('#consigneePhoneNo').val(tmp[6]);
                //                $('#consigneeAddress').val(tmp[3]);

                $('#creditLimitTemp').text(tmp[14]);
                $('#creditDaysTemp').text(tmp[15]);
                $('#outStandingTemp').text(tmp[16]);
                $('#customerRankTemp').text(tmp[17]);
                if (tmp[18] == "0") {
                    $('#approvalStatusTemp').text("No");
                } else {
                    $('#approvalStatusTemp').text("Yes");
                }
                $('#outStandingDateTemp').text(tmp[19]);
                $('#creditLimit').val(tmp[14]);
                $('#creditDays').val(tmp[15]);
                $('#outStanding').val(tmp[16]);
                $('#customerRank').val(tmp[17]);
                $('#approvalStatus').val(tmp[18]);
                $('#outStandingDate').val(tmp[19]);
                $('#creditLimitTable').hide();
                //$('#ahref').attr('href', '/throttle/viewCustomerContract.do?custId=' + tmp[0]);
                $("#contractDetails").show();
                $("#custDetailsUnfreeze").hide();
                document.getElementById('customerCode').readOnly = true;
                $('#origin').empty();
                $('#origin').append(
                        $('<option style="width:150px"></option>').val(0).html('--Select--')
                        )
                if (tmp[8] == '3') {//actual kms contract
                    setOriginList(tmp[12]);
                    setVehicleTypeForActualKM(tmp[12]);
                    $('#vehicleTypeDiv').show();
                    $('#vehicleTypeContractDiv').hide();
                    $('#contractRouteDiv2').show();
                    $('#contractRouteDiv1').hide();
                    $('#onwardReturnDiv').hide();
                } else {
                    getContractRoutes(tmp[12]);
                    getContractRoutesOrigin(tmp[12]);
                    $('#vehicleTypeDiv').hide();
                    $('#vehicleTypeContractDiv').show();
                    $('#contractRouteDiv1').show();
                    $('#onwardReturnDiv').show();
                    $('#contractRouteDiv2').hide();
                }
                fetchCustomerProducts(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
        $('#customerCode').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerCode: request.term,
                        customerName: document.getElementById('customerName').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerCode").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#customerId').val(tmp[0]);
                $itemrow.find('#customerName').val(tmp[1]);
                $itemrow.find('#customerCode').val(tmp[2]);
                $itemrow.find('#customerAddress').val(tmp[3]);
                $itemrow.find('#customerMobileNo').val(tmp[4]);
                $itemrow.find('#customerPhoneNo').val(tmp[5]);
                $itemrow.find('#mailId').val(tmp[6]);
                $itemrow.find('#billingTypeId').val(tmp[7]);
                $('#billingTypeName').val(tmp[8]);
                $('#billTypeName').val(tmp[8]);
                $("#contractDetails").show();
                $("#custDetailsUnfreeze").hide();
                document.getElementById('customerName').readOnly = true;
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[2] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
    function setOriginList(val) {
        $.ajax({
            url: '/throttle/getContractRouteList.do',
            data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: 0},
            dataType: 'json',
            success: function(data) {
                $.each(data, function(i, data) {
                    $('#originTemp').append(
                            $('<option style="width:150px"></option>').val(data.Id).html(data.Name)
                            )
                });
            }
        });
    }
    function fetchCustomerProducts(val) {
        var custId = document.cNote.customerId.value;
        //alert(custId + ":::" +val );
        document.cNote.customerName.value = val;
        if (custId != '' && custId != '0') {
            $.ajax({
                url: '/throttle/fetchCustomerProducts.do',
                data: {customerId: custId},
                dataType: 'json',
                success: function(data) {
                    if (data != '') {
//                        $('#productCategoryIdTemp').empty();
                        $('#productCategoryIdTemp').append(
                                $('<option ></option>').val(0).html('--select--')
                                )
                        $.each(data, function(i, data) {

                            $('#productCategoryIdTemp').append(
                                    $('<option></option>').val(data.Id).html(data.Name)
                                    )

                        });
                    }
                }
            });
        }

    }
    function setVehicleTypeForActualKM(val) {
        $('#vehTypeIdTemp').empty();
        $('#vehTypeIdTemp').append(
                $('<option ></option>').val(0).html('--select--')
                )
        $.ajax({
            url: '/throttle/getVehicleTypeForActualKM.do',
            data: {contractId: val},
            dataType: 'json',
            success: function(data) {
                $.each(data, function(i, data) {
                    $('#vehTypeIdTemp').append(
                            $('<option ></option>').val(data.Id).html(data.Name)
                            )
                });
            }
        });
    }

    function getContractRoutes(val) {
        $.ajax({
            url: '/throttle/getContractRoutes.do',
            data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: 0},
            dataType: 'json',
            success: function(data) {
                $.each(data, function(i, data) {
                    $('#contractRoute').append(
                            $('<option ></option>').val(data.Id).html(data.Name)
                            )
                });
            }
        });
    }
    function getContractRoutesOrigin(val) {
//        alert("sss");
        $.ajax({
            url: '/throttle/getContractRoutesOrigin.do',
            data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: 0},
            dataType: 'json',
            success: function(data) {
                $('#originTemp').empty();
                $('#originTemp').append(
                        $('<option></option>').val(0).html('-select-')
                        )
                $.each(data, function(i, data) {
                    $('#originTemp').append(
                            $('<option ></option>').val(data.Id).html(data.Name)
                            )
                });
            }
        });
    }
    function setContractOriginDestination() {

        if (document.cNote.contractRouteOrigin.value == '0') {
            $('#contractRouteDestination').empty();
            $('#contractRouteDestination').append(
                    $('<option></option>').val(0).html('-select-')
                    )
            setContractRouteDetails();
        } else {

            $.ajax({
                url: '/throttle/getContractRoutesOriginDestination.do',
                data: {contractId: $('#contractId').val(), contractRouteOrigin: $('#contractRouteOrigin').val(), customerTypeId: 0},
                dataType: 'json',
                success: function(data) {
                    $('#contractRouteDestination').empty();
                    $('#contractRouteDestination').append(
                            $('<option></option>').val(0).html('-select-')
                            )
                    $.each(data, function(i, data) {
                        $('#contractRouteDestination').append(
                                $('<option ></option>').val(data.Id).html(data.Name)
                                )
                    });
                    setContractRouteDetails();
                }
            });
        }
    }


    var httpReq;
    var temp = "";
    function setDestination()
    {
        document.cNote.origin.value = document.cNote.originTemp.value;
        var billingTypeId = document.getElementById('billingTypeId').value;
        var url = "/throttle/getDestinationList.do?originId=" + document.cNote.originTemp.value + "&billingTypeId=" + document.getElementById('billingTypeId').value + "&customerTypeId=" + document.getElementById('customerTypeId').value + "&contractId=" + document.getElementById('contractId').value;
        if (window.ActiveXObject)
        {
            httpReq = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest)
        {
            httpReq = new XMLHttpRequest();
        }
        httpReq.open("GET", url, true);
        httpReq.onreadystatechange = function() {
            processAjax();
        };
        httpReq.send(null);
    }

    function processAjax()
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();
                setDestinationOptions(temp, document.cNote.destinationTemp);
            } else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }

    function setDestinationOptions(text, variab) {
        variab.options.length = 0;
        option0 = new Option("--select--", '0');
        variab.options[0] = option0;
        if (text != "") {
            var splt = text.split('~');
            var temp1;
            var id;
            var name;
            for (var i = 0; i < splt.length; i++) {
                temp1 = splt[i].split('#');
                //                    alert(temp1[0]);
                //                    alert(temp1[1]);
                /*
                 if (document.getElementById('customerTypeId').value == 2) {
                 id = temp1[0];
                 name = temp1[1];
                 //setVehicleType();
                 } else if (document.getElementById('billingTypeId').value == 3 && document.getElementById('customerTypeId').value == 1) {
                 id = temp1[0] + "-" + temp1[1];
                 name = temp1[1];
                 //setVehicleType();
                 } else if (document.getElementById('billingTypeId').value != 3 && document.getElementById('customerTypeId').value == 1) {
                 id = temp1[0];
                 name = temp1[1];
                 //setVehicleType();
                 }
                 */
                id = temp1[0];
                name = temp1[1];
                option1 = new Option(name, id)
                variab.options[i + 1] = option1;
            }
            setRouteDetails();
        }
    }
    function setFreezeReadOnly1() {

        //    $("#routePlanDetailsDiv :input").attr("disabled", true);
    }
    function setUnFreezeReadOnly() {

        // $("#routePlanDetailsDiv :input").removeAttr("disabled", true);
    }



    function hideColoumns10(value) {
//        alert(sno);
//        alert(value);
        if (value == 'Pick Up') {
            document.getElementById("weightKG1").value = 0;
            document.getElementById("weightKG1").readOnly = true;
            document.getElementById("gcnNos1").value = "NA";
            document.getElementById("gcnNos1").readOnly = true;
            document.getElementById("consigDate1").readOnly = true;
            document.getElementById("invoicedNo1").value = "NA";
            document.getElementById("invoicedNo1").readOnly = true;
            document.getElementById("custRefDate1").readOnly = true;
        } else {

            document.getElementById("weightKG1").value = "";
            document.getElementById("weightKG1").readOnly = false;
            document.getElementById("gcnNos1").value = "";
            document.getElementById("gcnNos1").readOnly = false;
            document.getElementById("consigDate1").readOnly = false;
            document.getElementById("invoicedNo1").value = "";
            document.getElementById("invoicedNo1").readOnly = false;
            document.getElementById("custRefDate1").readOnly = false;
        }

    }
    function hideColoumns0(sno, value) {
//        alert(sno);
//        alert(value);
        if (value == 'Pick Up') {
            document.getElementById("weightKG" + sno).value = "0";
            document.getElementById("weightKG" + sno).readOnly = true;
            document.getElementById("gcnNos" + sno).value = "NA";
            document.getElementById("gcnNos" + sno).readOnly = true;
            document.getElementById("consigDate" + sno).readOnly = true;
            document.getElementById("invoicedNo" + sno).value = "NA";
            document.getElementById("invoicedNo" + sno).readOnly = true;
            document.getElementById("custRefDate" + sno).readOnly = true;
        } else {

            document.getElementById("weightKG" + sno).value = "";
            document.getElementById("weightKG" + sno).readOnly = false;
            document.getElementById("gcnNos" + sno).value = "";
            document.getElementById("gcnNos" + sno).readOnly = false;
            document.getElementById("consigDate" + sno).readOnly = false;
            document.getElementById("invoicedNo" + sno).value = "";
            document.getElementById("invoicedNo" + sno).readOnly = false;
            document.getElementById("custRefDate" + sno).readOnly = false;
        }

    }

    function setVehicleType() {
        //alert();
        //alert();
        var url = "/throttle/getContractVehicleTypeList.do?contractId=" + document.getElementById('contractId').value + "&billingTypeId=" + document.getElementById('billingTypeId').value + "&customerTypeId=" + document.getElementById('customerTypeId').value + "&origin=" + document.getElementById('origin').value + "&destination=" + document.getElementById('destination').value;
        if (window.ActiveXObject) {
            httpReq = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest) {
            httpReq = new XMLHttpRequest();
        }
        httpReq.open("GET", url, true);
        httpReq.onreadystatechange = function() {
            processVehicleTypeAjax();
        };
        httpReq.send(null);
    }
    function getContractVehicleType(val) {
        if (document.getElementById('customerTypeId').value == 1 && document.getElementById('billingTypeId').value != 3) {
            if (document.cNote.contractRouteOrigin.value == '0') {
                //                alert(document.cNote.contractRouteOrigin.value);
                alert('please select consignment origin1');
                document.cNote.contractRouteOrigin.focus();
                return;
            }
            if (document.cNote.contractRouteDestination.value == '0') {
                alert('please select consignment destination');
                document.cNote.contractRouteDestination.focus();
                return;
            }
            if (document.cNote.orderType.value == '0') {
                alert('please select Order Type');
                document.cNote.orderType.focus();
                return;
            }
            if (document.cNote.productCategoryIdTemp.value == '0') {
                alert('please select Product Category');
                document.cNote.productCategoryIdTemp.focus();
                return;
            }
            $("#vehicleFreeze").show();
        }




        //        $('#consignorIds'+val).css('pointer-events','none');
        //        $('#consigneeIds'+val).css('pointer-events','none');
        //        document.getElementById("consignorIds"+val).css('pointer-events','none');
        //        document.getElementById("consigneeIds"+val).css('pointer-events','none');
        //        document.getElementById("phoneNos"+val).readOnly = true;
        //        document.getElementById("pointAddresss"+val).readOnly = true;
        //        document.getElementById("pointPlanDate"+val).readOnly = true;
        //        document.getElementById("pointPlanHour"+val).readOnly = true;
        //        document.getElementById("pointPlanMinute"+val).readOnly = true;



        //        $("#routePlanDetailsDiv :input").attr("disabled", true);

        var pointName = document.getElementsByName("pointName");
        var pointType = document.getElementsByName("pointType");
        var consignorIdstemp = document.getElementsByName("consignorIdstemp");
        var consigneeIdstemp = document.getElementsByName("consigneeIdstemp");
        var consignorNames = document.getElementsByName("consignorNames");
        var consigneeNames = document.getElementsByName("consigneeNames");
        var phoneNos = document.getElementsByName("phoneNos");
        var pointAddresss = document.getElementsByName("pointAddresss");
        var deliveryAddress = document.getElementsByName("deliveryAddress");
        var grossWG = document.getElementsByName("grossWG");
        var weightKG = document.getElementsByName("weightKG");
        var pointPlanDate = document.getElementsByName("pointPlanDate");
        var invoicedNo = document.getElementsByName("invoicedNo");
        var pointPlanHour = document.getElementsByName("pointPlanHour");
        var pointPlanMinute = document.getElementsByName("pointPlanMinute");
        var gcnNo = document.getElementsByName("gcnNos");


        for (var i = 0; (i < pointName.length && pointName.length != 0); i++) {

            if (pointName[i].value == '') {
                alert("please Enter The Point Name");
                pointName[i].focus();
                return;
            }

            if (pointType[i].value == 'NA' || pointType[i].value == 0) {
                alert("please select point Type");
                pointType[i].focus();
                return;
            }


            if (pointType[i].value == 'Pick Up' && (consignorNames[i].value == '' || consignorNames[i].value == 'NA')) {
                alert("please select Bill To");
                //                consignorIdstemp[i].focus();
                return;
            }



            if (pointType[i].value == 'Drop' && (consigneeNames[i].value == '' || consigneeNames[i].value == 'NA')) {
                alert("please select Ship To");
                //                consigneeIdstemp[i].focus();
                return;
            }


            //            if (phoneNos[i].value == '') {
            //                alert("please enter Mobile No");
            //                phoneNos[i].focus();
            //                return;
            //            }

            if (pointAddresss[i].value == '') {
                alert("please enter Billing Address");
                pointAddresss[i].focus();
                return;
            }
//            alert("deliveryAddress[i]"+deliveryAddress[i])
            if (deliveryAddress[i].value == '') {
                alert("please enter Delivery Address");
                deliveryAddress[i].focus();
                return;
            }

            if (grossWG[i].value == '') {
                alert("please enter Gross Weight");
                grossWG[i].focus();
                return;
            }
            if (weightKG[i].value == '') {
                alert("please enter Weight");
                weightKG[i].focus();
                return;
            }



            if (gcnNo[i].value == '') {
                alert("please enter Goods Consignment No");
                gcnNo[i].focus();
                return;
            }

            if (invoicedNo[i].value == '') {
                alert("please enter Customer Invoice No");
                invoicedNo[i].focus();
                return;
            }


            //            if (pointPlanDate[i].value == '') {
            //                alert("please enter Date");
            //                pointPlanDate[i].focus();
            //                return;
            //
            //            }
            if (pointPlanHour[i].value == '') {
                alert("please enter Hour");
                pointPlanHour[i].focus();
                return;
            }
            if (pointPlanMinute[i].value == '') {
                alert("please enter Minute");
                pointPlanMinute[i].focus();
                return;
            }

            document.getElementById('orderReferenceNo').value = invoicedNo[i].value;
            document.getElementById('gcnNo').value = gcnNo[i].value;

        }
        // $("#routePlanDetailsDiv :input").attr("disabled", true);

        $("#specialIns").show();
        //        $("#vehicleFreeze").show();
        $("#addTyres1").show();
        $("#bg").show();
        $("#bg1").show();
        $("#unFreezetRoute").hide();
        $("#con1").hide();
        $("#custDetailsUnfreeze").hide();
        $('#orderType').css('pointer-events', 'none');
        $('#productCategoryIdTemp').css('pointer-events', 'none');
        $('#contractRouteOrigin').css('pointer-events', 'none');
        $('#contractRouteDestination').css('pointer-events', 'none');
        $("#intermPo").show();
        if (document.getElementById('customerTypeId').value == 2) {
            $("#contractFreightTable").hide();
        } else {
            $("#contractFreightTable").show();
        }


        var contractType = document.cNote.billingTypeId.value;
        //alert(contractType);
        if (contractType == '1' || contractType == '4' || contractType == '2') {
            //alert("am here....");
            var pointIds = document.getElementsByName("pointId");
            //alert("am here...."+pointIds.length);
            var pointId = "";
            var j = 0;
            var statusCheck = true;
            for (var i = 0; i < pointIds.length; i++) {
                //alert(pointIds[i].value);
                j = i + 1;
                if (i != 0 && i != pointIds.length) {
                    j = j + 1;
                }
                if (contractType != '2') {
                    if (pointIds[i].value == '' || pointIds[i].value == 0) {
                        alert('please enter valid interim point');
                        //alert(document.getElementById("pointName"+j).value);
                        document.getElementById("pointName" + j).focus();
                        statusCheck = false;
                    }
                }
                if (i == 0) {
                    pointId = pointIds[i].value;
                } else {
                    pointId = pointId + "~" + pointIds[i].value;
                }
            }
            var billingTypeId = document.cNote.billingTypeId.value;
            //alert(pointId);
            if (statusCheck) {
                $.ajax({
                    url: '/throttle/getContractVehicleTypeForPointsList.do',
                    data: {contractId: $('#contractId').val(), pointId: pointId, billingTypeId: billingTypeId},
                    dataType: 'json',
                    success: function(data) {
                        if (data == '' || data == null) {
                            alert('chosen contract route does not exists. please check.');
                            $("#freezeRoute").show();
                            $("#resetRoute").show();
                            $("#unFreezetRoute").hide();
                            $("#intermPo").show();
                        } else {
                            //                            $("#routePlanAddRow").hide();
                            $("#freezeRoute").hide();
                            $("#resetRoute").hide();
                            $("#unFreezetRoute").show();
                            $("#intermPo").hide();
                            $('#vehTypeIdContractTemp').empty();
                            $('#vehTypeIdContractTemp').append(
                                    $('<option></option>').val(0).html('-select-')
                                    )
                            $.each(data, function(i, data) {
                                $('#vehTypeIdContractTemp').append(
                                        $('<option ></option>').val(data.Id).html(data.Name)
                                        )
                            });
                        }
                    }
                });
            }
        } else {
            //            $("#routePlanAddRow").hide();
            $("#freezeRoute").hide();
            $("#resetRoute").hide();
            $("#unFreezetRoute").show();
            $("#intermPo").hide();
        }

    }

    function unFreezetRouteFn() {
        $("#unFreezetRoute").hide();
        $("#intermPo").show();
        $("#routePlanAddRow").show();
        $("#freezeRoute").show();
        $("#resetRoute").show();
        $("#specialIns").hide();
//        $("#addTyres1").hide();
        $("#bg").hide();
        $("#bg1").hide();
        $("#unFreezetRoute").hide();
        $("#contractFreightTable").hide();
        $("#custDetailsUnfreeze").show();
        $('#orderType').css('pointer-events', 'visible ');
        $('#productCategoryIdTemp').css('pointer-events', 'visible ');
        $('#contractRouteOrigin').css('pointer-events', 'visible ');
        $('#contractRouteDestination').css('pointer-events', 'visible ');
        if (document.getElementById('customerTypeId').value == 2) {
            $("#contractFreightTable").hide();
        }

        var contractType = document.cNote.billingTypeId.value;
        //alert(contractType);
        if (contractType == '1' || contractType == '2') {
            $('#vehTypeIdContractTemp').empty();
            $('#vehTypeIdContractTemp').append(
                    $('<option></option>').val(0).html('-select-')
                    )
        }
    }
    function processVehicleTypeAjax() {
        if (httpReq.readyState == 4) {
            if (httpReq.status == 200) {
                temp = httpReq.responseText.valueOf();
                //alert(temp);
                $('#vehTypeId').append(
                        $('<option style="width:150px"></option>').val(0).html('--Select--')
                        )
                setVehicleTypeOptions(temp, document.cNote.vehTypeId);
            } else {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }

    function setVehicleTypeOptions(text, variab) {
        variab.options.length = 0;
        option0 = new Option("--select--", '0');
        variab.options[0] = option0;
        if (text != "") {
            var splt = text.split('~');
            var temp1;
            variab.options[0] = option0;
            for (var i = 0; i < splt.length; i++) {
                temp1 = splt[i].split('#');
                //alert(temp1[1]+"-"+temp1[0]);
                option0 = new Option(temp1[1], temp1[0])
                variab.options[i + 1] = option0;
            }
        }
    }


    function multiPickupShow() {
        var billingTypeId = document.getElementById('billingTypeId').value;
        if (document.getElementById('multiPickup').checked == true) {
            document.getElementById('multiPickupCharge').readOnly = false;
            document.getElementById('multiPickup').value = "Y";
            //alert(document.getElementById('multiPickup').value);
            //                if (billingTypeId == "3") {
            //                    $("#routeDetail").show();
            //                    $("#showRouteCourse").show();
            //                }
        } else if (document.getElementById('multiPickup').checked == false) {
            document.getElementById('multiPickupCharge').readOnly = true;
            document.getElementById('multiPickup').value = "N";
            //alert(document.getElementById('multiPickup').value);
            //                if (billingTypeId == "3") {
            //                    $("#routeDetail").hide();
            //                    $("#showRouteCourse").hide();
            //                }
        }
    }
    function multiDeliveryShow() {
        var billingTypeId = document.getElementById('billingTypeId').value;
        if (document.getElementById('multiDelivery').checked == true) {
            document.getElementById('multiDeliveryCharge').readOnly = false;
            document.getElementById('multiDelivery').value = "Y";
            //                if (billingTypeId == "3") {
            //                    $("#routeDetail").show();
            //                    $("#showRouteCourse").show();
            //                }
        } else if (document.getElementById('multiDelivery').checked == false) {
            document.getElementById('multiDeliveryCharge').readOnly = true;
            document.getElementById('multiDelivery').value = "N";
            //                if (billingTypeId == "3") {
            //                    $("#routeDetail").hide();
            //                    $("#showRouteCourse").hide();
            //                }
        }
    }

    function calcTotalPacks(val) {
        var totVal = 0;
        var packagesNos = document.getElementsByName('packagesNos');
        for (var i = 0; i < packagesNos.length; i++) {
            if (packagesNos[i].value != '') {
                totVal += parseInt(packagesNos[i].value);
            }
        }
        $('#totalPackages').text(totVal);
        $('#totalPackage').val(totVal);
    }


    function calcTotalWeights(val, sno) {

        var totVal = 0;
        var totweight = 0;
        var reeferRequired = document.getElementById('reeferRequired').value;
//        var weights = document.getElementsByName('weights');
        var weightKGs = document.getElementsByName('weightKG');
        var billingTypeId = document.getElementById('billingTypeId').value;
        var rateWithReefer = document.getElementById('rateWithReefer').value;
        var rateWithoutReefer = document.getElementById('rateWithoutReefer').value;
        var pointType = document.getElementsByName('pointTypes');
        for (var i = 0; i < weightKGs.length; i++) {
            if (weightKGs[i].value != '' && pointType[i].value == 'Pick Up') {
//                alert(pointType[i].value);
                totweight += parseInt(weightKGs[i].value);
//                alert(totweight);
            }
        }
//        alert("6565675765675" + totweight);

        $('#totalWeight').text(totweight);
        $('#weights').text(totweight);
        $('#weights').val(totweight);
        $('#totalWeightage').val(totweight);
        var freigthTotal = 0;
        if (billingTypeId == 2 && reeferRequired == 'Yes') {
            freigthTotal = parseInt(rateWithReefer) * parseInt(totVal);
            $('#freightAmount').text(freigthTotal)
            $('#totalCharges').val(freigthTotal)
        } else if (billingTypeId == 2 && reeferRequired == 'No') {
            freigthTotal = parseInt(rateWithoutReefer) * parseInt(totVal);
            $('#freightAmount').text(freigthTotal)
            $('#totalCharges').val(freigthTotal)
        }
    }


    function calcTotalWeightsKG(val) {
        var totVal = 0;
        var reeferRequired = document.getElementById('reeferRequired').value;
        var weights = document.getElementsByName('weights');
        var grossWG = document.getElementsByName('grossWG');
        var billingTypeId = document.getElementById('billingTypeId').value;
        var rateWithReefer = document.getElementById('rateWithReefer').value;
        var rateWithoutReefer = document.getElementById('rateWithoutReefer').value;
        for (var i = 0; i < grossWG.length; i++) {
            if (grossWG[i].value != '') {
                totVal += parseInt(grossWG[i].value);
            }
        }

        for (var i = 0; i < weights.length; i++) {
            if (weights[i].value != '') {
                totVal += parseInt(weights[i].value);
            }
        }
        $('#totalWeight').text(totVal);
        $('#weights').text(totVal);
        $('#weights').val(totVal);
        $('#totalWeightage').val(totVal);
        var freigthTotal = 0;
        if (billingTypeId == 2 && reeferRequired == 'Yes') {
            freigthTotal = parseInt(rateWithReefer) * parseInt(totVal);
            $('#freightAmount').text(freigthTotal)
            $('#totalCharges').val(freigthTotal)
        } else if (billingTypeId == 2 && reeferRequired == 'No') {
            freigthTotal = parseInt(rateWithoutReefer) * parseInt(totVal);
            $('#freightAmount').text(freigthTotal)
            $('#totalCharges').val(freigthTotal)
        }
        setFreightRate();
    }
    function resetRouteInfo() {
        var customerType = document.cNote.customerTypeId.value;
        var billingType = document.cNote.billingTypeId.value;
        if (customerType == '1' && (billingType == '1' || billingType == '2')) {
            setContractRouteDetails();
        } else {
            setRouteDetails();
        }
//        alert("test");
        hideColoumns1("Pick Up");
//        hideColoumns();
    }
    function validateTransitTime(val) {

        var transitHours = document.getElementById("pointTransitHrs" + val).value;
        if (transitHours == '') {
            var pointIdValue = document.getElementById("pointId" + val).value;
            var pointOrderValue = document.getElementById("pointOrder" + val).value;
            var pointOrder = document.getElementsByName("pointOrder");
            var pointNames = document.getElementsByName("pointName");
            var pointIdPrev = 0;
            var pointIdNext = 0;
            var prevPointOrderVal = parseInt(pointOrderValue) - 1;
            var nextPointOrderVal = parseInt(pointOrderValue) + 1;
            for (var m = 1; m <= pointOrder.length; m++) {
                if (document.getElementById("pointOrder" + m).value == prevPointOrderVal) {
                    pointIdPrev = document.getElementById("pointId" + m).value;
                }
                if (document.getElementById("pointOrder" + m).value == nextPointOrderVal) {
                    pointIdNext = document.getElementById("pointId" + m).value;
                }
            }
            //fetch transit hours via ajax for prepointid and current point id and set transit hours value to hidden field

        }

    }

    function validateTripSchedules() {
        document.cNote.vehicleRequiredDate.value = document.getElementById("pointPlanDate1").value;
        document.cNote.vehicleRequiredHour.value = document.getElementById("pointPlanHour1").value;
        document.cNote.vehicleRequiredMinute.value = document.getElementById("pointPlanMinute1").value;
        var scheduleDate = document.cNote.vehicleRequiredDate.value;
        var scheduleHour = document.cNote.vehicleRequiredHour.value;
        var scheduleMinute = document.cNote.vehicleRequiredMinute.value;
        var temp = scheduleDate.split("-");
        var tripScheduleTime = convertStringToDate(temp[2], temp[1], temp[0], scheduleHour, scheduleMinute, '00');
        var time1 = new Date();
        var time1ms = time1.getTime(time1); //i get the time in ms
        var time2 = tripScheduleTime;
        var time2ms = time2.getTime(time2);
        var difference = time2ms - time1ms;
        var hours = Math.floor(difference / 36e5);
        //alert(hours);
        //        if (hours < 0) {
        ////            alert('vehicle required time is in the past. please check ');
        //            //alert('Orders cannot be accepted as the cut off time has elapsed.');
        //            return false;
        //        } else {
        //            return true;
        //        }

    }
    function submitPage(value) {
//        alert("test");
        if (isEmpty(document.cNote.editFreightAmount.value)) {
            alert('please enter Freignt Amount');
            document.getElementById("editFreightAmount").focus();
            return;
        }
        if ((document.cNote.vehicleID.value) == 0) {
            alert('please enter vehicle type');
            document.getElementById("vehicleID").focus();
            return;
        }
//        alert("test1");
        document.cNote.action = "/throttle/updateConsignmentNote.do";
        document.cNote.submit();
    }

    function setProductCategoryValues() {
        var temp = document.cNote.productCategoryIdTemp.value;
        //alert(temp);
        if (temp != 0) {
            var tempSplit = temp.split('~');
            document.getElementById("temperatureInfo").innerHTML = 'Reefer Temp (deg Celcius): Min ' + tempSplit[1] + ' Max ' + tempSplit[2];
            document.cNote.productCategoryId.value = tempSplit[0];
            var reeferRequired = tempSplit[3];
            if (reeferRequired == 'Y') {
                reeferRequired = 'Yes';
            } else {
                reeferRequired = 'No';
            }
            document.cNote.reeferRequired.value = reeferRequired;
        } else {
            document.getElementById("temperatureInfo").innerHTML = '';
            document.cNote.productCategoryId.value = 0;
            document.cNote.reeferRequired.value = '';
        }
    }
    function checkFreightAcceptedStatus() {
        //        $("#vehicleUnFreeze").hide();
        //        estimateFreight();
        //alert("am here.."+document.cNote.freightAcceptedStatus.checked);
        //        document.getElementById('orderButton').style.display = 'block';
        //        alert("am here.."+document.getElementById('orderButton').style.display);
        var weights = document.getElementsByName("weights");
        var contractType = document.cNote.billingTypeId.value;
//                    alert(contractType);
        if (contractType == '2') {
            for (var i = 0; (i < weights.length && weights.length != 0); i++) {
                if (weights[i].value == '') {
//                                document.cNote.freightAcceptedStatus.checked = false;
//				        $("#freightAcceptedStatus").prop("checked", false);
                    $("#freightAcceptedStatus").attr("checked", false);

                    alert("please Enter The weights");
                    weights[i].focus();
                    return;
                }
            }
        }
        if (document.cNote.freightAcceptedStatus.checked) {
            //            alert("am here..");
            var billingTypeId = $("#billingTypeId").val();
            if (billingTypeId == 4) {
                //                alert("123")
                document.getElementById('orderButton').style.display = 'block';
            } else {
                //                alert("456")
                if (parseFloat(document.cNote.totFreightAmount.value) > 0) {
                    document.getElementById('orderButton').style.display = 'block';
                    //                    alert("789")
                } else {
                    //                    alert("000")
                    document.cNote.freightAcceptedStatus.checked = false;
                    alert("Invalid freight value");
                }

            }
        } else {
            document.getElementById('orderButton').style.display = 'block';
        }
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ConsignmentNote" text="ConsignmentNote"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="general-forms.html"> <spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.ConsignmentNote" text="ConsignmentNote"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">


            <body onload="getContractRoutesOrigin('<c:out value="${customerContractId}"/>');
                    enableContract(1);
                    addRow1();
                    testNew();
                    multiPickupShow();
                    multiDeliveryShow();
                    setOriginList();">
                <% String menuPath = "Operations >>  Create New Consignment Note";
                    request.setAttribute("menuPath", menuPath);
                %>
                <form name="cNote" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@include file="/content/common/message.jsp" %>
                    <%
                        Date today = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        String startDate = sdf.format(today);
                    %>
                    <input type="hidden" name="startDate" value='<%=startDate%>' />

                    <table class="table table-info mb30 table-hover">
                        <thead>
                            <tr>
                                <th  colspan="6">Consignment Note </th>
                            </tr>
                        </thead>
                        <tr>
                            <td >Entry Option</td>
                            <td ><input type="radio" name="entryType" value="1" checked="">Manual</td>
                            <td ><font color="red">*</font>Customer Type</td>
                            <td >
                                <c:if test="${customerTypeId ==1 }">
                                    Contract
                                </c:if>
                                <c:if test="${customerTypeId ==2 }">
                                    Walk In
                                </c:if>
                            </td>

                        </tr>
                        <tr>
                            <td  >Consignment No</td>
                            <td  ><c:out value="${consignmentNoteNo}"/></td>

                            <td >Consignment Date </td>
                            <td  ><c:out value="${curDate}"/></td>
                        </tr>
                        <tr>
                            <td >Customer Reference  Remarks</td>
                            <td ><textarea readonly name="orderReferenceRemarks" id="orderReferenceRemarks" class="form-control" style="width:250px;height:40px"><c:out value="${consignmentOrderRefRemarks}"/></textarea></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr style="display:none;">
                            <td ><font color="red">*</font>Goods Consignment No</td>
                            <td  ><input type="text" name="gcnNo" id="gcnNo" value="" class="form-control" style="width:250px;height:40px" onkeypress="return isNumber(event)" /></td>
                            <td >Consignment Date </td>
                            <td  ><input type="text" name="consignmentDate" id="consignmentDate" value="<c:out value="${curDate}"/>" class="datepicker form-control" style="width:250px;height:40px" /></td>
                            <td ><font color="red">*</font>Customer Reference No</td>
                            <td ><input type="text" name="orderReferenceNo" id="orderReferenceNo" value="<c:out value="${consignmentOrderRefRemarks}"/>" class="form-control" style="width:250px;height:40px" /></td>
                            <td >Customer Reference Date </td>
                            <td  ><input type="text" name="CustomerReference" id="CustomerReference" value="<c:out value="${curDate}"/>" class="datepicker form-control"style="width:250px;height:40px" /></td>
                        </tr>
                    </table>


                    <script type="text/javascript">

                        function enableContract(val) {
                            //alert(val);
                            $('#productCategoryIdTemp').css('pointer-events', 'none');
                            if (val == 1) {
                                $("#contractCustomerTable").show();
                                $("#walkinCustomerTable").hide();
                                //                                $("#contractFreightTable").show();
                                $("#walkinFreightTable").hide();
                                document.getElementById('customerAddress').readOnly = true;
                                document.getElementById('pincode').readOnly = true;
                                document.getElementById('customerMobileNo').readOnly = true;
                                document.getElementById('mailId').readOnly = true;
                                document.getElementById('customerPhoneNo').readOnly = true;
                                document.getElementById('walkinCustomerName').value = "";
                                document.getElementById('walkinCustomerCode').value = "";
                                document.getElementById('walkinCustomerAddress').value = "";
                                document.getElementById('walkinPincode').value = "";
                                document.getElementById('walkinCustomerMobileNo').value = "";
                                document.getElementById('walkinMailId').value = "NA";
                                document.getElementById('walkinCustomerPhoneNo').value = "0";
                            } else {
                                //                                alert(val);
                                $("#contractCustomerTable").hide();
                                $("#walkinCustomerTable").show();
                                $("#contractFreightTable").hide();
                                $("#walkinFreightTable").show();
                                $("#bg12").show();
                                $("#bg13").show();
                                document.getElementById('customerId').value = "";
                                document.getElementById('customerName').value = "";
                                document.getElementById('customerCode').value = "";
                                document.getElementById('customerAddress').value = "";
                                document.getElementById('pincode').value = "";
                                document.getElementById('customerMobileNo').value = "";
                                document.getElementById('mailId').value = "";
                                document.getElementById('customerPhoneNo').value = "";
                                setOriginList(0);
                                $('#vehicleTypeDiv').show();
                                $('#vehicleTypeContractDiv').hide();
                                $('#contractRouteDiv2').show();
                                $('#orderButton').show();
                                $('#freightAcceptedStatus').show();
                                $('#contractRouteDiv1').hide();
                                $('#onwardReturnDiv').hide();
                            }
                        }
                    </script>
                    <div id="tabs">
                        <ul>
                            <li><a href="#customerDetail"><span>Order Details</span></a></li>
                            <!--                    <li><a href="#routeDetail" id="showRouteCourse"><span>Route Course</span></a></li>-->
                            <!--                    <li><a href="#paymentDetails"><span>Invoice Info</span></a></li>-->

                        </ul>

                        <div id="customerDetail">
                            <table class="table table-info mb30 table-hover" id="contractCustomerTable" >
                                <thead>
                                    <tr>
                                        <th  colspan="4" >Contract Customer Details</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td ><font color="red">*</font>Billing Customer Name</td>
                                    <td ><input type="hidden" name="customerId" id="customerId" class="form-control" style="width:250px;height:40px" value="<c:out value="${customerId}"/>"/>
                                        <input type="text" readonly value="<c:out value="${customerName}"/>" name="customerName" onKeyPress="return onKeyPressBlockNumbers(event);"  id="customerName" class="form-control" style="width:250px;height:40px"  /></td>
                                    <td ><font color="red">*</font>Billing Customer Code</td>
                                    <td ><input type="text" readonly value="<c:out value="${customerCode}"/>" name="customerCode" id="customerCode" class="form-control" style="width:250px;height:40px"  /></td>
                                <input type="hidden" name="paymentType" value="0" id="paymentType" class="form-control" style="width:250px;height:40px" />
                                </tr>
                                <tr style="display:none;">
                                    <td ><font color="red">*</font>Billing Customer Address</td>
                                    <td ><textarea rows="1" cols="16" id="customerAddress" class="form-control" style="width:250px;height:40px" name="customerAddress"></textarea></td>
                                    <td ><font color="red">*</font>Pincode</td>
                                    <td ><input type="text"  name="pincode" id="pincode"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="form-control" style="width:250px;height:40px" /></td>
                                </tr>
                                <tr style="display:none;">
                                    <td ><font color="red">*</font>Mobile No</td>
                                    <td ><input type="text"  name="customerMobileNo"  onKeyPress="return onKeyPressBlockCharacters(event);"  id="customerMobileNo" class="form-control" style="width:250px;height:40px"  /></td>
                                    <td ><font color="red">*</font>E-Mail Id</td>
                                    <td ><input type="text"  name="mailId" id="mailId" class="form-control" style="width:250px;height:40px"  /></td>
                                </tr>
                                <tr style="display:none">

                                    <td >Phone No</td>
                                    <td ><input type="text" name="customerPhoneNo" id="customerPhoneNo"  onKeyPress="return onKeyPressBlockCharacters(event);"   class="form-control" style="width:250px;height:40px" maxlength="10"   /></td>

                                    <td >Billing Type</td>
                                    <td  id="billingType"><input type="text" name="billingTypeId" id="billingTypeId" class="form-control" style="width:250px;height:40px" value="<c:out value="${customerBillingType}"/>"/><label id="billingTypeName"></label></td>

                                </tr>
                            </table>
                            <table class="table table-info mb30 table-hover" id="walkinCustomerTable" >
                                <thead>
                                    <tr>
                                        <th  colspan="4" >Walkin Customer Details</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td ><font color="red">*</font> Customer Name</td>
                                    <td ><input type="hidden" name="walkinCustomerId" id="walkinCustomerId" class="form-control" style="width:250px;height:40px" value="0"  />
                                        <input type="text" name="walkinCustomerName" id="walkinCustomerName" class="form-control" style="width:250px;height:40px"   /></td>
                                    <td ><font color="red">*</font> Customer Code</td>
                                    <td ><input type="text" name="walkinCustomerCode" id="walkinCustomerCode" class="form-control" style="width:250px;height:40px" readonly /></td>
                                </tr>
                                <tr>
                                    <td ><font color="red">*</font>Address</td>
                                    <td ><textarea rows="1" cols="16" id="walkinCustomerAddress" name="walkinCustomerAddress" class="form-control" style="width:250px;height:40px"></textarea></td>
                                    <td ><font color="red">*</font>Mobile No</td>
                                    <td ><input type="text"    onKeyPress='return onKeyPressBlockCharacters(event);'   name="walkinCustomerMobileNo" id="walkinCustomerMobileNo" class="form-control" style="width:250px;height:40px"  /></td>

                                </tr>
                                <tr>
                                    <td ><font color="red">*</font>Pincode</td>
                                    <td ><input type="text"    onKeyPress='return onKeyPressBlockCharacters(event);'   name="walkinPincode" id="walkinPincode" class="form-control" style="width:250px;height:40px" /></td>

                                    <td >Phone No</td>
                                    <td ><input type="text"   onKeyPress='return onKeyPressBlockCharacters(event);'  name="walkinCustomerPhoneNo" id="walkinCustomerPhoneNo"  class="form-control" style="width:250px;height:40px" maxlength="10"   /></td>

                                </tr>
                                <tr>
                                    <td  ><font color="red">*</font>State</td>
                                    <td  >
                                        <select name="state" id="state"   class="form-control" style="width:240px;height:40px"  >
                                            <option value="32">--Select--</option>
                                            <c:if test="${getStateList != null}">
                                                <c:forEach items="${getStateList}" var="Type">
                                                    <option value='<c:out value="${Type.stateId}"/>' ><c:out value="${Type.stateName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                    </td>

                                    <td >E-Mail Id</td>
                                    <td ><input type="text"   name="walkinMailId" id="walkinMailId" class="form-control" style="width:250px;height:40px"  /></td>

                                </tr>
                                <tr>
                                    <td>GST No</td>
                                    <td ><input type="text"  value="NA" name="gstNo" id="gstNo" value="0" class="form-control" style="width:240px;height:40px" >
                                    </td>
                                    <td>PAN No</td>
                                    <td ><input name="panNo" id="panNo" value="NA" type="text" class="form-control" value="" maxlength="10" style="width:250px;height:40px" onBlur="validatePanNo()" placeholder="Type pan no..."  required></td>

                                </tr>
                            </table>
                          <div id="contractDetails" >
                                <table width="500">
                                    <tr>
                                        <td align="left">Contract No :<c:out value="${contractNo}"/></td>
                                        <input type="hidden" name="contractId" id="contractId" class="form-control" style="width:250px;height:40px" value='<c:out value="${customerContractId}"/>'/>
                                        <td align="right">Contract Expiry Date :<c:out value="${contractTo}"/></td>
                                        <!--<td ><font color="green"><b><label id="contractExpDate"></label></b></font></td>-->
                                        <td >&nbsp;&nbsp;&nbsp;</td>
                                        <!--                                <td ><a id="ahref" href="/throttle/BrattleFoods/customerContractMaster.jsp"><b>View Contract</b></a></td>-->
                                        <td >
                                            <!--                                    <input type="button" class="btn btn-success" value="View Contract" onclick="viewCustomerContract()">-->
                                            <a href="#" onclick="viewCustomerContract();">view</a>
                                        </td>
                                        <!--                                        <td ><input type="button" id="custDetails" class="btn btn-success" value="Customer Freeze"  onclick="custFreeze();">
                                                                                <td style="display:block"><input type="button" id="custDetailsUnfreeze" class="btn btn-success" value="Customer UnFreeze" onclick="custUnFreeze();" >-->
                                    </tr>
                                </table>
                            </div><br>
                            <script>
                                function custFreeze() {
                                    if (document.getElementById('customerTypeId').value == 1) {
                                        //                                        if (isEmpty(document.cNote.gcnNo.value)) {
                                        //                                            alert('please enter gcn no');
                                        //                                            document.getElementById("gcnNo").focus();
                                        //                                            return;
                                        //                                        }
                                        //                                        if (isEmpty(document.cNote.orderReferenceNo.value)) {
                                        //                                            alert('please enter orderReferenceNo');
                                        //                                            document.getElementById("orderReferenceNo").focus();
                                        //                                            return;
                                        //                                        }

                                        //                                        document.getElementById('gcnNo').readOnly = true;
                                        //                                        document.getElementById('orderReferenceNo').readOnly = true;
                                        document.getElementById('customerName').readOnly = true;
                                        //                                        document.getElementById('CustomerReference').readOnly = true;
                                        //                                        document.getElementById('consignmentDate').readOnly = true;
                                        $("#creditLimitTable").hide();
                                        $("#bg12").show();
                                        $("#bg13").show();
                                        $("#custDetailsUnfreeze").show();
                                        $("#contractRouteDiv2").show();
                                        $("#onwardReturnDiv").show();
                                        $("#routePlanDetailsDiv").show();
                                        $("#routePlan").show();
                                        $("#routePlanAddRow").show();
                                        $("#custDetails").hide();
                                        $("#orderReferenceRemarks").css('pointer-events', 'none');
                                        $('#customerTypeId').css('pointer-events', 'none');
                                    }
                                }
                                function custUnFreeze() {
                                    if (document.getElementById('customerTypeId').value == 1) {
                                        //                                        document.getElementById('gcnNo').readOnly = false;
                                        //                                        document.getElementById('orderReferenceNo').readOnly = false;
                                        document.getElementById('customerName').readOnly = false;
                                        //                                        document.getElementById('CustomerReference').readOnly = false;
                                        //                                        document.getElementById('consignmentDate').readOnly = false;
                                        $("#creditLimitTable").hide();
                                        $("#bg12").hide();
                                        $("#bg13").hide();
                                        $("#routePlan").hide();
                                        $("#custDetailsUnfreeze").hide();
                                        $("#contractRouteDiv2").hide();
                                        $("#onwardReturnDiv").hide();
                                        $("#routePlanDetailsDiv").hide();
                                        $("#routePlan").hide();
                                        $("#routePlanAddRow").hide();
                                        $("#custDetails").show();
                                        $("#routePlanAddRow").hide();
                                        $("#orderReferenceRemarks").css('pointer-events', 'visible');
                                        $('#customerTypeId').css('pointer-events', 'visible');
                                        document.getElementById('orderType').value = "0";
                                        document.getElementById('productCategoryIdTemp').value = "0";
                                        document.getElementById("temperatureInfo").innerHTML = '';
                                        document.getElementById('contractRouteOrigin').value = "0";
                                        document.getElementById('contractRouteDestination').value = "0";
                                    }
                                    resetRouteInfo();
                                }
                            </script>




                            <table class="table table-info mb30 table-hover" id="bg12"  >
                                <thead>

                                    <tr>
                                        <th  colspan="2" >Product Info</th>
                                    </tr>

                                </thead>

                                <tr>
                                    <td ><font color="red">*</font>Order Type </td>
                                    <td>
                                        <select name="orderType" id="orderType" class="form-control" style="width:250px;height:40px" >
                                            <!--<option value="0">--Select--</option>-->
                                            <option value="1">FTL</option>
                                            <option value="2">LTL</option>
                                        </select>
                                    </td>

                                <input type="hidden" name="productCategoryId" id="productCategoryId" class="form-control" style="width:250px;height:40px" />
                                <input type="hidden" name="productCategoryIdTemp" id="productCategoryIdTemp" class="form-control" style="width:250px;height:40px" />
                                <!--                                    <td ><font color="red">*</font>Product Category &nbsp;&nbsp;
                                <%--<c:if test="${productCategoryList != null}">--%>
                                    <select disable name="productCategoryIdTemp" id="productCategoryIdTemp" onchange="setProductCategoryValues();"  class="form-control" style="width:250px;height:40px" >
                                        <option value="0">--Select--</option>
                                <%--<c:forEach items="${productCategoryList}" var="proList">--%>
                                    <option value="<c:out value="${proList.productCategoryId}"/>"><c:out value="${proList.customerTypeName}"/></option>
                                <%--</c:forEach>--%>
                            </select>
                                <%--</c:if>--%>
                            </td>
                            <td  colspan="4" align="left" >&nbsp;<label id="temperatureInfo"></label></td>-->
                                </tr>
                            </table>

                            <table class="table table-info mb30 table-hover"  >
                                <thead>

                                    <tr>
                                        <th  colspan="6" >Consignment / Route Details</th>
                                    </tr>
                                </thead>
                                <tr >
                                    <td><font color="red">*</font>Origin</td>
                                    <td><c:out value="${origin}"/>
                                        <input type="hidden" id="contractRouteOrigin" name="contractRouteOrigin" value="<c:out value="${consigmentOrigin}"/>">

                                    </td>
                                    <td ><font color="red">*</font>Destination</td>
                                    <td>
                                    <td><c:out value="${destination}"/>
                                        <input type="hidden" id="contractRouteDestination" name="contractRouteDestination" value="<c:out value="${consigmentDestination}"/>">

                                    </td>
                                    <td  colspan="2" >&nbsp;</td>
                                    <!--    onchange='setContractRouteDetails();
                                                hideColoumns1("Pick Up")'                            <td ><font color="red">*</font>Contract Route</td>
                                                                    <td  colspan="3" >
                                                                        <select id="contractRoute" name="contractRoute" onchange='setOriginDestination();' style="width:350px">
                                                                            <option value="0">-Select-</option>
                                                                        </select>
                                                                    </td>-->
                                </tr>


                                <input type="hidden" name="origin" value="" />
                                <input type="hidden" name="destination" value="" />
                                <input type="hidden" name="destination" value="<c:out value="${consigmentOrigin}"/>" />
                                <script>
                                    document.getElementById("orderType").value = '<c:out value="${orderType}"/>';
                                    document.getElementById("productCategoryId").value = '<c:out value="${productCategoryId}"/>';
                                    document.getElementById("productCategoryIdTemp").value = '<c:out value="${productCategoryId}"/>';
                                    document.getElementById("contractRouteOrigin").value = '<c:out value="${consigmentOrigin}"/>';
                                    document.getElementById("contractRouteDestination").value = '<c:out value="${consigmentDestination}"/>';
                                    document.getElementById("vehTypeIdTemp").value = '<c:out value="${vehicleTypeId}"/>';
                                </script>
                                <tr  id="contractRouteDiv2" style="display:none;">
                                    <td ><font color="red">*</font>Origin</td>
                                    <td >
                                        <select id="originTemp" name="originTemp" onchange='setDestination();' class="form-control" style="width:250px;height:40px">
                                            <option value="0">-Select-</option>
                                        </select>
                                        <script>
                                            document.getElementById("originTemp").value = '<c:out value="${consigmentOrigin}"/>';
                                        </script>
                                    </td>
                                    <td ><font color="red">*</font>Destination</td>
                                    <td ><select id="destinationTemp" name="destinationTemp" class="form-control" onchange="setRouteDetails();
                                            hideColoumns1('Pick Up')"  style="width:250px;height:40px">
                                            <option value="0">-Select-</option>
                                        </select>
                                        <script>
                                            document.getElementById("destinationTemp").value = '<c:out value="${consigmentDestination}"/>';
                                        </script>
                                    </td>
                                </tr>
                                <tr  id="onwardReturnDiv" style="display:none" >
                                    <td >Onward / Return</td>
                                    <td class="text1" height="30">Onward &nbsp;&nbsp;: &nbsp;&nbsp;<input type="radio" name="onwardReturnType" value="1" checked="" ></td>
                                    <td class="text1" height="30">Return &nbsp; &nbsp;:&nbsp;&nbsp; <input type="radio" name="onwardReturnType" value="2"  > </td>                        
                                </tr>


                                <tr id="routeChart" style="display:none;" >
                                    <td colspan="6" align="left" >

                                        <!--                                    <a  class="previoustab" href="#"><input type="button" class="btn btn-success" value="<back" name="Save" /></a>-->

                                        <!--                                    <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="next>" name="Save" /></a>-->
                                        </center>



                                    </td>
                                </tr>
                            </table>
                            <div >
                                <table class="table table-info mb30 table-hover" id="routePlan1"  >
                                    <thead>
                                        <tr>
                                            <th style="width:60px"><font color="red">*</font>Order Sequence</th>
                                            <th style="width:120px"><font color="red">*</font>Point Name</th>
                                            <th style="width:120px"><font color="red">*</font>Point Type</th>
                                            <th style="width:120px"><font color="red">*</font>Bill To/Ship To</th>
                                            <th><font color="red">*</font>Address</th>
                                            <!--<th style="width:120px"><font color="red">*</font>Gross Weight</th>-->
                                            <!--<th><font color="red">*</font>Weight Chargeable(KG)</th>-->
                                            <!--<th><font color="red">*</font>GCN No</th>-->
                                            <!--<th><font color="red">*</font>GCN Date</th>-->
                                            <!--<th><font color="red">*</font>Customer Invoice No</th>-->
                                            <!--<th><font color="red">*</font>Customer Invoice Date</th>-->
                                            <th style="width:120px"><font color="red">*</font>Required Date</th>
                                            <th><font color="red"></font>Required Time</th>
                                        </tr>
                                    </thead>
                                    <% int test = 0;%>
                                    <input type="hidden" id="consignmentOrderId" name="consignmentOrderId" style="width:130px;height:40px" class="form-control" value="<c:out value="${consignmentOrderId}"/>">
                                    <c:if test="${consignmentPoint != null}">
                                        <c:forEach items="${consignmentPoint}" var="routeOne">
                                            <%
                                                        test++;
                                                        String className = "text1";
                                                        if ((test % 1) == 0) {
                                                            className = "text1";
                                                        } else {
                                                            className = "text2";
                                                        }
                                            %>



                                            <tr >
                                                <td align="left">  <%= test +0%> </td>
                                            <input type="hidden" id="pointTypes" class="form-control" name="pointTypes" style="width:130px;height:40px"  value="<c:out value="${routeOne.consignmentPointType}"/>" />
                                            <%--<c:if test="${routeOne.pointSequence == '1' && routeOne.consignmentPointType == 'Pick Up'}" >--%>
                                            <td><c:out value="${routeOne.pointNames}"/></td>
                                            <td><c:out value="${routeOne.consignmentPointType}"/></td>
                                            <td><c:out value="${routeOne.consignorName}"/></td>
                                            <td><c:out value="${routeOne.pointAddress}"/></td>
                                            <input type="hidden" id="grossWG<%= test +0%>" name="grossWG" style="width:130px;height:40px" class="form-control" value="<c:out value="${routeOne.grossWeights}"/>">
                                            <input type="hidden" id="weightKG<%= test +0%>" class="form-control" name="weightKG" style="width:130px;height:40px" onchange="calcTotalWeights(this.value,<%= test +0%>)" value="<c:out value="${routeOne.KG}"/>" />
                                            <input type="hidden" id="gcnNos<%= test +0%>" name="gcnNos" style="width:130px;height:40px" value="<c:out value="${routeOne.gcnNo}"/>">
                                            <input type="hidden" id="consigDate<%= test +0%>" name="consigDate" class="form-control datepicker" style="width:130px;height:40px" value="<c:out value="${routeOne.pointDate}"/>">
                                            <input type="hidden" id="invoicedNo<%= test +0%>" name="invoicedNo" class="form-control" style="width:130px;height:40px" value="<c:out value="${routeOne.customerReference}"/>">
                                            <input type="hidden" id="custRefDate<%= test +0%>" name="custRefDate" class="form-control datepicker" style="width:130px;height:40px" value="<c:out value="${routeOne.cusRefDate}"/>">
                                            <td><input type="text" id="pointPlanDate<%= test +0%>" name="pointPlanDate" class="form-control datepicker"  style="width:130px;height:40px" value="<c:out value="${routeOne.pointDate}"/>"></td>
                                            <td><c:out value="${routeOne.pointPlanTime}"/>
                                                <input type="hidden" name="consRouteCourseId" id="consRouteCourseId" value="<c:out value="${routeOne.consignmentPointId}"/>"></td>
                                            <script>

                                                $(document).ready(function() {
                                                    $("#datepicker").datepicker({
                                                        showOn: "button",
                                                        buttonImage: "calendar.gif",
                                                        buttonImageOnly: true,
                                                        dateFormat: "dd-mm-yy'",
                                                        altFormat: "dd-mm-yy"

                                                    });
                                                });
                                                $(function() {
                                                    $(".datepicker").datepicker({
                                                        changeMonth: true, changeYear: true
                                                    });
                                                });
                                            </script>

                                            <%--</c:if>--%>
                                        </c:forEach>
                                    </c:if>

                                    </tr>
                                </table>
                                <script>
                                    function calculateWeight() {
                                        var weightKGs = document.getElementById('weightKGs').value;
                                        if
                                    }
                                </script>
                            </div>
                            <!--                            <table id="routePlanAddRow" s  >
                                                            <tr>
                                                                <td>
                                                                    <input type="button" class="btn btn-success"  id="intermPo" value="Interim Point" name="save" onClick="addRouteCourse2();
                                                                            setType(podSno1);">
                                                                    &nbsp;&nbsp;<input type="button" class="btn btn-success" id="freezeRoute" style="display:none;"  value="Freeze Route" name="save" onClick="getContractVehicleType(podSno1);calcTotalWeights(podSno1);
                                                                            setFreezeReadOnly();">
                                                                    &nbsp;&nbsp;<input type="button" class="btn btn-success" id="resetRoute" style="display:none;"  value="Reset" name="save" onClick="resetRouteInfo();">
                            
                                                                    &nbsp;&nbsp;<input type="button" class="btn btn-success" id="unFreezetRoute" style="display:none;"  value="UnFreeze Route" name="save" onClick="unFreezetRouteFn();
                                                                            setUnFreezeReadOnly();">
                                                                </td>
                                                            </tr>
                                                        </table>-->
                            <table class="table table-info mb30 table-hover" style="display: none" >
                                <thead>
                                    <tr>
                                        <th colspan="2">Special Instruction</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>
                                        <textarea rows="1" cols="16" name="consignmentOrderInstruction"  class="form-control" style="width:250px;height:40px" id="consignmentOrderInstruction"><c:out value="${vehicleInstruction}"/></textarea>
                                        <input type="hidden" name="businessType" value="1" />
                                        <input type="hidden" class="form-control" style="width:250px;height:40px" name="multiPickup" id="multiPickup" onclick="multiPickupShow()" value="N">
                                        <input type="hidden" class="form-control" style="width:250px;height:40px" name="multiDelivery" id="multiDelivery" onclick="multiDeliveryShow()" value="N">
                                    </td>

                                </tr>
                            </table>

                            <table class="table table-info mb30 table-hover" style="display:none">
                                <thead>
                                    <tr>
                                        <th>Sno</th>
                                        <th>HSN Code</th>
                                        <th>Product/Article Name </th>
                                        <!--<th>Batch </th>-->
                                        <th><font color='red'>*</font>No of Packages</th>
                                        <th><font color='red'>*</font>Uom</th>
                                        <th><font color='red'>*</font>Total Weight (in Kg)</th>
                                        <!--<th>Delete </th>-->
                                    </tr>
                                </thead>


                                <tr>
                                    <td colspan="5" align="left">
                                        <!--<input type="button" class="btn btn-success" value="Add Row" name="save" onClick="addRow1()">-->
                                    </td>
                                </tr>
                            </table>

                            <input type="hidden" name="serviceType" value="1" />
                            <input type="hidden" name="vehTypeId" value="0" />
                            <input type="hidden" name="vehicleID" value="<c:out value="${vehicleTypeId}"/>" />
                            <input type="hidden" name="routeConsContractId" value="0" />

                            <table class="table table-info mb30 table-hover"  >
                                <thead>
                                    <tr>
                                        <th  colspan="6" >Vehicle (Required) Details</th>
                                    </tr>
                                </thead>

                                <tr id="vehicleTypeDiv" >
                                    <td >Vehicle Type</td>
                                    <td   colspan="5" > <select name="vehicleIDTemp" id="vehicleIDTemp" class="form-control" style="width:250px;height:40px;"  onchange="setFreightRate();">
                                            <c:if test="${editVehicleTypeList != null}">
                                                <!--<option value="0" selected>--Select--</option>-->
                                                <c:forEach items="${editVehicleTypeList}" var="vehicleType">
                                                    <option value="<c:out value="${vehicleType.vehicleTypeId}"/>"><c:out value="${vehicleType.vehicleTypeName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                        <script>
                                            document.getElementById("vehicleIDTemp").value = '<c:out value="${contractVehicleTypeId}"/>';
                                        </script>
                                    </td>                                        
                                </tr>
                                <tr id="vehicleTypeContractDiv" style="display:none;">
                                    <td >
                                        Vehicle Type &nbsp;&nbsp;

                                    </td>
                                    <td   colspan="5" > <select name="vehTypeIdContract" id="vehTypeIdContract"  onchange="setFreightRate1();" class="form-control" style="width:250px;height:40px;"  >
                                            <option value="0" selected>--Select--</option>
                                        </select></td>
                                </tr>
                                <!--<tr>-->
                                <!--                                    <td ><font color="red">*</font>Reefer Required</td>
                                                                    <td  colspan="5" >-->
                                <input type="hidden" readonly  name="reeferRequired" id="reeferRequired"  class="form-control" style="width:250px;height:40px"  value="" />
                                <!--                                <select name="reeferRequired" id="reeferRequired"  class="form-control" style="width:250px;height:40px"  style="width:120px;" onchange="calculateFreightTotal()">
                                                                    <option value="Yes" > Yes </option>
                                                                    <option value="No"> No </option>
                                                                </select>-->
                                <!--</td>-->
                                <!--</tr>-->

                                <script>
                                    function vehicleFreezes() {

                                        if (document.cNote.vehTypeIdContractTemp.value == '0') {
                                            alert('please select Vehicle Type');
                                            document.cNote.vehTypeIdContractTemp.focus();
                                            return;
                                        }
                                        $('#vehTypeIdContractTemp').css('pointer-events', 'none ');
                                        $("#unFreezetRoute").hide();
                                        $("#vehicleUnFreeze").show();
                                        $("#vehicleFreeze").hide();
                                    }
                                    function vehicleUnFreezes() {
                                        $("#vehicleFreeze").show();
                                        $("#unFreezetRoute").show();
                                        $("#vehicleUnFreeze").hide();
                                        $('#vehTypeIdContractTemp').css('pointer-events', 'visible ');
                                    }
                                    function setRouteDetails() {
                                        $("#routePlan").show();
                                        var temp = document.getElementById('destinationTemp').value;
                                        var destinationSelect = document.getElementById('destinationTemp');
                                        var destinationName = destinationSelect.options[destinationSelect.selectedIndex].text;
                                        var originSelect = document.getElementById('originTemp');
                                        var originName = originSelect.options[originSelect.selectedIndex].text;
                                        var tempVal = temp.split('-');
                                        document.cNote.destination.value = tempVal[1];
                                        //alert(document.cNote.destination.value);
                                        document.cNote.routeId.value = tempVal[0];
                                        document.cNote.routeBased.value = 'Y';
                                        //remove table rows and reset values
                                        var tab = document.getElementById("routePlan");
                                        //alert(tab.rows.length);
                                        //alert(podRowCount1);
                                        if (podRowCount1 > 1) {
                                            for (var x = 1; x < podRowCount1; x++) {
                                                document.getElementById("routePlan").deleteRow(1);
                                            }
                                        }
                                        podRowCount1 = 1;
                                        podSno1 = 0;
                                        //alert(document.cNote.destinationTemp.value);
                                        if ((document.cNote.destinationTemp.value != '0') && (document.cNote.destinationTemp.value != '')) {
                                            //alert("am here...");
                                            var startDate = document.cNote.startDate.value;
                                            addRouteCourse1(document.cNote.origin.value, originName, '1', 'Pick Up', 0, 0, 0, startDate);
                                            addRouteCourse1(document.cNote.destination.value, destinationName, '2', 'Drop', tempVal[0], tempVal[1], tempVal[2], '');
                                            $("#routePlanAddRow").show();
                                            $("#freezeRoute").show();
                                            $("#resetRoute").show();


                                        } else {
                                            $("#routePlanAddRow").hide();
                                            $("#freezeRoute").hide();
                                            $("#resetRoute").hide();
                                        }
                                    }

                                    function setContractRouteDetails() {

                                        $("#routePlan").show();
                                        var origin = document.getElementById('contractRouteOrigin').value;
                                        var destination = document.getElementById('contractRouteDestination').value;
                                        //                                        alert(origin + " ::: " + destination);
                                        var destinationSelect = document.getElementById('contractRouteDestination');
                                        var destinationName = destinationSelect.options[destinationSelect.selectedIndex].text;
                                        var originSelect = document.getElementById('contractRouteOrigin');
                                        var originName = originSelect.options[originSelect.selectedIndex].text;
                                        document.cNote.origin.value = origin;
                                        document.cNote.destination.value = destination;
                                        //remove table rows and reset values
                                        var tab = document.getElementById("routePlan");
                                        //alert(tab.rows.length);
                                        //alert(podRowCount1);
                                        if (podRowCount1 > 1) {
                                            for (var x = 1; x < podRowCount1; x++) {
                                                document.getElementById("routePlan").deleteRow(1);
                                            }
                                        }
                                        podRowCount1 = 1;
                                        podSno1 = 0;
                                        if (document.cNote.origin.value != '0' && document.cNote.destination.value != '0') {
                                            //                                            alert("am here...");
                                            //function addRouteCourse1(id, name, order, type,routeId,routeKm,routeReeferHr) {
                                            var startDate = document.cNote.startDate.value;
//                                            addRouteCourse3(document.cNote.origin.value, originName, '1', 'Pick Up', 0, 0, 0, startDate);
//                                            addRouteCourse3(document.cNote.destination.value, destinationName, '2', 'Drop', 0, 0, 0, '');
                                            $("#routePlanAddRow").show();
                                            $("#freezeRoute").show();
                                            $("#resetRoute").show();
                                            $("#addTyres1").show();
                                        } else {
                                            $("#routePlanAddRow").hide();
                                            $("#freezeRoute").hide();
                                            $("#resetRoute").hide();
                                            $("#addTyres1").hide();
                                        }


                                    }

                                    function setFreightRate() {
                                        var temp = document.getElementById("vehicleIDTemp").value;
//                                        alert(temp);
                                        //                                        var tempVal = temp.split("-");
                                        //                                         var tempVar = document.cNote.contractRoute.value;
                                        //            var tempSplit = tempVar.split("-");
                                        //            document.cNote.vehTypeId.value = tempSplit[3];
                                        //            document.cNote.vehicleTypeId.value = tempSplit[3];
//                                        alert("xz" + document.cNote.customerTypeId.value)
                                        if (document.cNote.customerTypeId.value == "2") {
                                            document.cNote.vehTypeId.value = temp;
                                            document.cNote.vehicleID.value = temp;
                                            document.cNote.vehicleTypeId.value = temp;
                                        } else {
                                            var tempSplit = temp.split("-");
                                            document.cNote.vehTypeId.value = tempSplit[1];
                                            document.cNote.vehicleID.value = tempSplit[1];
                                            document.cNote.vehicleTypeId.value = tempSplit[1];
                                            document.cNote.routeConsContractId.value = tempSplit[0];
                                        }

                                        //alert(document.cNote.vehTypeId.value);
                                        //alert(document.cNote.vehicleTypeId.value);

                                        var billingTypeId = document.getElementById("billingTypeId").value;
                                        var customerTypeId = document.getElementById("customerTypeId").value;
                                        var temp;
                                        estimateFreight();
                                    }
                                    function setFreightRate1() {
                                        var temp = document.getElementById("vehTypeIdContractTemp").value;
                                        var tempVal = temp.split("-");
                                        document.cNote.vehTypeId.value = tempVal[1];
                                        document.cNote.vehicleTypeId.value = tempVal[1];
                                        document.cNote.routeContractId.value = tempVal[0];
                                        //alert(document.cNote.vehTypeId.value);
                                        //alert(document.cNote.vehicleTypeId.value);

                                        var billingTypeId = document.getElementById("billingTypeId").value;
                                        var customerTypeId = document.getElementById("customerTypeId").value;
                                        var temp;
                                        estimateFreight();
                                    }

                                    function calculateFreightTotal() {
                                        var reeferRequired = document.getElementById('reeferRequired').value;
                                        var billingTypeId = document.getElementById('billingTypeId').value;
                                        var rateWithReefer = document.getElementById('rateWithReefer').value;
                                        var rateWithoutReefer = document.getElementById('rateWithoutReefer').value;
                                        var totalKm = document.getElementById('totalKm').value;
                                        var totalWeight = $("#totalWeight").text();
                                        var totalAmount = 0;
                                        if (reeferRequired == 'Yes' && billingTypeId == 1) {
                                            $('#freightAmount').text(rateWithReefer)
                                            $('#totFreightAmount').val(rateWithReefer)
                                            $('#totalCharges').val(rateWithReefer)
                                        } else if (reeferRequired == 'No' && billingTypeId == 1) {
                                            $('#freightAmount').text(rateWithoutReefer)
                                            $('#totFreightAmount').val(rateWithoutReefer)
                                            $('#totalCharges').val(rateWithoutReefer)
                                        } else if (reeferRequired == 'Yes' && billingTypeId == 2) {
                                            totalAmount = totalWeight * rateWithReefer;
                                            alert(totalAmount);
                                            $('#freightAmount').text(totalAmount)
                                            $('#totFreightAmount').val(totalAmount)
                                            $('#totalCharges').val(totalAmount)
                                        } else if (reeferRequired == 'No' && billingTypeId == 2) {
                                            totalAmount = totalWeight * rateWithReefer;
                                            alert(totalAmount);
                                            $('#freightAmount').text(totalAmount)
                                            $('#totFreightAmount').val(totalAmount)
                                            $('#totalCharges').val(totalAmount)
                                        } else if (reeferRequired == 'Yes' && billingTypeId == 3) {
                                            totalAmount = totalKm * rateWithReefer;
                                            //alert(totalAmount);
                                            $('#freightAmount').text(totalAmount)
                                            $('#totFreightAmount').val(totalAmount)
                                            $('#totalCharges').val(totalAmount)
                                        } else if (reeferRequired == 'No' && billingTypeId == 3) {
                                            totalAmount = totalKm * rateWithoutReefer;
                                            //alert(totalAmount);
                                            $('#freightAmount').text(totalAmount)
                                            $('#totFreightAmount').val(totalAmount)
                                            $('#totalCharges').val(totalAmount)
                                        }
                                    }
                                </script>
                                <tr>
                                    <!--                            <td ><font color="red">*</font>Vehicle Required Date</td>
                                                                <td >-->
                                <input type="hidden" class="form-control" style="width:250px;height:40px" name="destinationId" id="destinationId" >
                                <input type="hidden" class="form-control" style="width:250px;height:40px" name="routeContractId" id="routeContractId" >
                                <input type="hidden" class="form-control" style="width:250px;height:40px" name="routeId" id="routeId" >
                                <input type="hidden" class="form-control" style="width:250px;height:40px" name="routeBased" id="routeBased" >
                                <input type="hidden" class="form-control" style="width:250px;height:40px" name="contractRateId" id="contractRateId" >
                                <input type="hidden" class="form-control" style="width:250px;height:40px" name="totalKm" id="totalKm" >
                                <input type="hidden" class="form-control" style="width:250px;height:40px" name="totalHours" id="totalHours" >
                                <input type="hidden" class="form-control" style="width:250px;height:40px" name="totalMinutes" id="totalMinutes" >
                                <input type="hidden" class="form-control" style="width:250px;height:40px" name="totalPoints" id="totalPoints" >
                                <input type="hidden" class="form-control" style="width:250px;height:40px" name="vehicleTypeId" id="vehicleTypeId" >
                                <input type="hidden" class="form-control" style="width:250px;height:40px" name="rateWithReefer" id="rateWithReefer" >
                                <input type="hidden" class="form-control" style="width:250px;height:40px" name="rateWithoutReefer" id="rateWithoutReefer" >
                                <input type="hidden" readonly class="datepicker" name="vehicleRequiredDate" id="vehicleRequiredDate" >

                                <input type="hidden" readonly  name="vehicleRequiredHour" id="vehicleRequiredHour" >
                                <input type="hidden" readonly  name="vehicleRequiredMinute" id="vehicleRequiredMinute" >

                                <td >Special Instruction</td>
                                <td  colspan="5" ><textarea rows="1" cols="16" name="vehicleInstruction" class="form-control" style="width:250px;height:40px" id="vehicleInstruction"><c:out value="${vehicleInstruction}"/></textarea></td>
                                </tr>


                                <tr>
                                    <td style="display:none" id="vehicleFreeze">
                                        <!--                                        <input type="button" class="btn btn-success" value="vehicle Freeze"  onclick="vehicleFreezes();">-->
                                    </td>
                                    <td style="display:none" id="vehicleUnFreeze">
                                        <input type="button" class="btn btn-success" value="vehicle UnFreeze"  onclick="vehicleUnFreezes();">
                                    </td>

                                </tr>
                                <script>
                                    function calculateTime() {
                                        var date1 = new Date();
                                        var tempDate = $("#vehicleRequiredDate").val();
                                        var temp = tempDate.split("-");
                                        var requiredDay = temp[0];
                                        var requiredMonth = temp[1];
                                        var requiredYear = temp[2];
                                        var requiredHour = $("#vehicleRequiredHour").val();
                                        if (requiredHour == "") {
                                            requiredHour = "00";
                                        }
                                        var requiredMinute = $("#vehicleRequiredMinute").val();
                                        if (requiredMinute == "") {
                                            requiredMinute = "00";
                                        }
                                        var requiredSeconds = "00";
                                        var date2 = new Date(requiredYear, requiredMonth, requiredDay, requiredHour, requiredMinute, requiredSeconds)
                                        var diff = Math.abs(date1.getTime() - date2.getTime()) / 1000 / 60 / 60;
                                        alert(diff);
                                        var diffDays = diff.toFixed(2);
                                        alert(diffDays);
                                        if (diffDays < 10) {
                                            alert("No")
                                        } else {
                                            alert("Yes");
                                        }
                                    }
                                </script>
                            </table>


                            <table class="table table-info mb30 table-hover" style="display:none" >
                                <thead>
                                    <tr>
                                        <th   height="30" colspan="8">Consignor Details</th>
                                    </tr>
                                </thead>
                                <tr>

                                    <td ><font color="red">*</font>Bill To</td>
                                    <td ><input type="text" class="form-control" style="width:250px;height:40px;" id="consignorName" onKeyPress="return onKeyPressBlockNumbers(event);" name="consignorName" value="NA" ></td>
                                    <td ><font color="red">*</font>Mobile No</td>
                                    <td ><input type="text" class="form-control" style="width:250px;height:40px;" id="consignorPhoneNo" maxlength="12" onKeyPress="return onKeyPressBlockCharacters(event);"  name="consignorPhoneNo" value="0"></td>
                                    <td ><font color="red">*</font>Address</td>
                                    <td ><textarea rows="1" cols="16" name="consignorAddress" id="consignorAddress" style="width:250px;height:40px;" value="NA"></textarea> </td>
                                </tr>
                            </table>


                            <table class="table table-info mb30 table-hover" style="display:none" >
                                <thead>
                                    <tr>
                                        <th   height="30" colspan="6">Consignee Details</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td ><font color="red">*</font>Ship To</td>
                                    <td ><input type="text" class="form-control" style="width:250px;height:40px" id="consigneeName" onKeyPress="return onKeyPressBlockNumbers(event);"  name="consigneeName" value="NA"></td>
                                    <td ><font color="red">*</font>Mobile No</td>
                                    <td ><input type="text" class="form-control" style="width:250px;height:40px" id="consigneePhoneNo" maxlength="12" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"   name="consigneePhoneNo" ></td>
                                    <td ><font color="red">*</font>Address</td>
                                    <td ><textarea rows="1" cols="16" name="consigneeAddress" id="consigneeAddress" style="width:250px;height:40px" value="NA"></textarea> </td>
                                </tr>
                            </table>
                            <br/>
                            <br/>
                            <!--                    <center>
                                                    <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" ></a>
                                                </center>-->
                        </div>





                        <script>
                            $(".nexttab").click(function() {
                                var selected = $("#tabs").tabs("option", "selected");
                                $("#tabs").tabs("option", "selected", selected + 1);
                            });
                            $(".previoustab").click(function() {
                                var selected = $("#tabs").tabs("option", "selected");
                                $("#tabs").tabs("option", "selected", selected - 1);
                            });</script>
                        <!--                <div id="paymentDetails">-->
                        <table border="0" class="table table-info mb30 table-hover" align="center" width="97.5%" cellpadding="0" cellspacing="0"  id="creditLimitTable" style="display: none">
                            <thead> <tr>
                                    <th   height="30" colspan="6" >Credit Limit Details</th>
                                </tr>
                            </thead>
                            <tr>
                                <td >Credit Days &nbsp; </td>
                                <td ><label id="creditDaysTemp"></label><input type="hidden" name="creditDays" id="creditDays" value="0"/> </td>
                                <td  align="left" >Credit Limit &nbsp; </td>
                                <td ><label id="creditLimitTemp"></label><input type="hidden" name="creditLimit" id="creditLimit" value="0"/></td>
                            </tr>
                            <tr>
                                <td  align="left" >Customer Rank &nbsp; </td>
                                <td ><label id="customerRankTemp"></label><input type="hidden" name="customerRank" id="customerRank" value="0"/></td>
                                <td  align="left" >Approval Status&nbsp; </td>
                                <td ><label id="approvalStatusTemp"></label><input type="hidden" name="approvalStatus" id="approvalStatus" value="0"/></td>
                            </tr>
                            <tr>
                                <td  align="left" >Out Standing &nbsp; </td>
                                <td ><label id="outStandingTemp"></label><input type="hidden" name="outStanding" id="outStanding" value="0"/></td>
                                <td  align="left" >Out Standing Date&nbsp; </td>
                                <td ><label id="outStandingDateTemp"></label><input type="hidden" name="outStandingDate" id="outStandingDate" value=""/></td>
                            </tr>

                        </table>

                        <table class="table table-info mb30 table-hover" >
                            <thead>

                                <tr>
                                    <th  height="30" colspan="2" > Freight Details</th>
                                </tr>
                            </thead>
                                                        <tr>
                                <td>
                                    <label >Total No Packages :</label>&nbsp;&nbsp; </td>

                                <td>
                                    <input type="hidden" id="totalWeightage" name="totalWeightage" value="<c:out value="${totalWeight}"/>"/>
                                    <input type="text" id="totalPackage" name="totalPackage" value="<c:out value="${totalPackages}"/>"/>
                                </td>
<!--                                <td></td>
                                <td></td>-->

                            </tr>
                            <tr>
<!--                                <td >Billing Type</td>
                                <td ><label id="billTypeName"> </label></td>-->
                                <td  align="left" >Estimated Freight Charges (Contract) &nbsp; </td>
                                <td >INR.
                                    <input type="text" value="<c:out value="${totFreightCharges}"/>" readonly class="form-control" style="width:150px;height:40px" name="totFreightAmount" id="totFreightAmount" />
                                    <!--                                    &nbsp;
                                                                        &nbsp;
                                                                        &nbsp;
                                                                        Override Freight Charges (Manual Input)
                                                                        &nbsp;
                                                                        &nbsp;
                                                                        <input type="text"  class="form-control" style="width:150px;height:40px" name="editFreightAmount" id="editFreightAmount" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"/>
                                                                        &nbsp;
                                                                        &nbsp;
                                                                        Freight Agree &nbsp;&nbsp;
                                                                        <input type="checkbox" name="freightAcceptedStatus" id="freightAcceptedStatus" onclick="checkFreightAcceptedStatus();" class="form-control" style="width:80px;height:10px" />-->
                                </td>
                            <tr>
<!--                                <td></td>
                                <td></td>-->
                                <td>Override Freight Charges (Manual Input)</td>

                                <td><input type="text"  class="form-control" style="width:150px;height:40px" name="editFreightAmount" id="editFreightAmount" value="<c:out value="${freightCharges}"/>" onKeyPress="return onKeyPressBlockCharacters(event);"/>
                                    &nbsp;&nbsp;
                                    Freight Agree &nbsp;&nbsp;
                                    <input type="checkbox" name="freightAcceptedStatus" id="freightAcceptedStatus" onclick="checkFreightAcceptedStatus();" class="form-control" style="width:80px;height:10px" />

                                </td>
                            </tr>

                        </table>
                        <table class="table table-info mb30 table-hover" id="walkinFreightTable" >
                            <thead>
                                <tr>
                                    <th   height="30" colspan="6" >Walkin Freight Details</th>
                                </tr>
                            </thead>
                            <tr>
                                <td >Billing Type</td>
                                <td ><select name="walkInBillingTypeId" id="walkInBillingTypeId" class="form-control" style="width:250px;height:40px" onchange="displayWalkIndetails(this.value)" >
                                        <option value="0" selected>--Select--</option>
                                        <option value="1" >Fixed Freight</option>
                                        <option value="2" >Rate Per Kg</option>
                                        <option value="3" >Rate Per KM</option>
                                    </select></td>
                                <td >Freight Charges (INR.)</td>
                                <td >
                                    <input type="text" readonly class="form-control" style="width:250px;height:40px" name="totFreightAmount1" id="totFreightAmount1" value="0"/>
                                    &nbsp;
                                    &nbsp;
                                    &nbsp;

                                </td>
                            </tr>
                            <tr id="WalkInptp" style="display: none;">
                                <td >Freight With Reefer</td>
                                <td ><input type="text" name="walkinFreightWithReefer" id="walkinFreightWithReefer" class="form-control" style="width:250px;height:40px;"  onkeyup="calculateSubTotal()"/></td>
                                <td >Freight Without Reefer</td>
                                <td ><input type="text" name="walkinFreightWithoutReefer" id="walkinFreightWithoutReefer" class="form-control" style="width:250px;height:40px;"  onkeyup="calculateSubTotal()"/></td>
                            </tr>
                            <tr id="WalkInPtpw" style="display: none;">
                                <td >Rate With Reefer/Kg</td>
                                <td ><input type="text" name="walkinRateWithReeferPerKg" id="walkinRateWithReeferPerKg" class="form-control" style="width:250px;height:40px;"  onkeyup="calculateSubTotal()"/></td>
                                <td >Rate Without Reefer/Kg</td>
                                <td ><input type="text" name="walkinRateWithoutReeferPerKg" id="walkinRateWithoutReeferPerKg" class="form-control" style="width:250px;height:40px"  onkeyup="calculateSubTotal()"/></td>
                            </tr>
                            <tr id="WalkinKilometerBased" style="display: none;">
                                <td >Rate With Reefer/Km</td>
                                <td ><input type="text" name="walkinRateWithReeferPerKm" id="walkinRateWithReeferPerKm" class="form-control" style="width:250px;height:40px;"  onkeyup="calculateSubTotal()"/></td>
                                <td >Rate Without Reefer/Km</td>
                                <td ><input type="text" name="walkinRateWithoutReeferPerKm" id="walkinRateWithoutReeferPerKm" class="form-control" style="width:250px;height:40px;"  onkeyup="calculateSubTotal()"/></td>
                            </tr>
                        </table>
                        <script type="text/javascript">
                            function displayWalkIndetails(val) {
                                var reeferRequired = document.getElementById('reeferRequired').value;
                                var vehicleTypeId = document.getElementById('vehTypeIdTemp').value;
                                //alert(vehicleTypeId);
                                if (vehicleTypeId == '0') {
                                    alert("Please Choose Vehicle Type");
                                    document.cNote.walkInBillingTypeId.value = 0;
                                    document.getElementById('vehTypeIdTemp').focus();
                                } else if (reeferRequired == '0') {
                                    alert("Please Choose Reefer Details");
                                    document.getElementById('reeferRequired').focus();
                                } else {
                                    if (val == 1) {
                                        $("#WalkInptp").show();
                                        $("#WalkInPtpw").hide();
                                        $("#WalkinKilometerBased").hide();
                                        if (reeferRequired == "Yes") {
                                            $('#walkinFreightWithReefer').attr('readonly', false);
                                            $('#walkinFreightWithoutReefer').attr('readonly', true);
                                        } else if (reeferRequired == "No") {
                                            $('#walkinFreightWithReefer').attr('readonly', true);
                                            $('#walkinFreightWithoutReefer').attr('readonly', false);
                                        }
                                        $("#walkinRateWithReeferPerKm").val("");
                                        $("#walkinRateWithoutReeferPerKm").val("");
                                        $("#walkinRateWithReeferPerKg").val("");
                                        $("#walkinRateWithoutReeferPerKg").val("");
                                    } else if (val == 2) {
                                        $("#WalkInptp").hide();
                                        $("#WalkInPtpw").show();
                                        $("#WalkinKilometerBased").hide();
                                        if (reeferRequired == "Yes") {
                                            $('#walkinRateWithReeferPerKg').attr('readonly', false);
                                            $('#walkinRateWithoutReeferPerKg').attr('readonly', true);
                                        } else if (reeferRequired == "No") {
                                            $('#walkinRateWithReeferPerKg').attr('readonly', true);
                                            $('#walkinRateWithoutReeferPerKg').attr('readonly', false);
                                        }
                                        $("#walkinFreightWithReefer").val("");
                                        $("#walkinFreightWithoutReefer").val("");
                                        $("#walkinRateWithReeferPerKm").val("");
                                        $("#walkinRateWithoutReeferPerKm").val("");
                                    } else if (val == 3) {
                                        $("#WalkInptp").hide();
                                        $("#WalkInPtpw").hide();
                                        $("#WalkinKilometerBased").show();
                                        if (reeferRequired == "Yes") {
                                            $('#walkinRateWithReeferPerKm').attr('readonly', false);
                                            $('#walkinRateWithoutReeferPerKm').attr('readonly', true);
                                        } else if (reeferRequired == "No") {
                                            $('#walkinRateWithReeferPerKm').attr('readonly', true);
                                            $('#walkinRateWithoutReeferPerKm').attr('readonly', false);
                                        }
                                        $("#walkinFreightWithReefer").val("");
                                        $("#walkinFreightWithoutReefer").val("");
                                        $("#walkinRateWithReeferPerKg").val("");
                                        $("#walkinRateWithoutReeferPerKg").val("");
                                    }
                                }
                            }
                        </script>
                        <br/>
                        <br/>
                        <!--                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                                <tr>
                                                    <td   height="30" colspan="6">Standard / Additional Charges</td>
                                                </tr>
                                                <tr>
                                                    <td >Document Charges</td>
                                                    <td ><input type="text" class="form-control" style="width:250px;height:40px" id="docCharges" name="docCharges" onkeyup="calculateSubTotal()"></td>
                                                    <td >ODA Charges</td>
                                                    <td ><input type="text" class="form-control" style="width:250px;height:40px" id="odaCharges" name="odaCharges" onkeyup="calculateSubTotal()"></td>
                                                    <td >Multi Pickup Charges</td>
                                                    <td ><input type="text" class="form-control" style="width:250px;height:40px" id="multiPickupCharge" name="multiPickupCharge" readonly onkeyup="calculateSubTotal()"></td>
                                                </tr>
                                                <tr>
                                                    <td >Multi Delivery Charges</td>
                                                    <td ><input type="text" class="form-control" style="width:250px;height:40px" id="multiDeliveryCharge" name="multiDeliveryCharge" readonly onkeyup="calculateSubTotal()"></td>
                                                    <td >Handling Charges</td>
                                                    <td ><input type="text" class="form-control" style="width:250px;height:40px" id="handleCharges" name="handleCharges" onkeyup="calculateSubTotal()"></td>
                                                    <td >Other Charges</td>
                                                    <td ><input type="text" class="form-control" style="width:250px;height:40px" id="otherCharges" name="otherCharges" onkeyup="calculateSubTotal()"></td>
                                                </tr>
                                                <tr>
                                                    <td >Unloading Charges</td>
                                                    <td ><input type="text" class="form-control" style="width:250px;height:40px" id="unloadingCharges" name="unloadingCharges" onkeyup="calculateSubTotal()"></td>
                                                    <td >Loading Charges</td>
                                                    <td ><input type="text" class="form-control" style="width:250px;height:40px" id="loadingCharges" name="loadingCharges" onkeyup="calculateSubTotal()"></td>
                                                    <td >Remarks</td>
                                                    <td ><textarea rows="3" cols="20" nmae="standardChargeRemarks" id="standardChargeRemarks"></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td  colspan="4"></td>
                                                    <td >Sub Total</td>
                                                    <td ><input type="text" class="form-control" style="width:250px;height:40px" id="subTotal" name="subTotal"  value="0" readonly=""></td>
                                                </tr>
                                            </table>-->
                        <input type="hidden" class="form-control" style="width:250px;height:40px" id="subTotal" name="subTotal"  value="0" readonly="">
                        <script type="text/javascript">
                            function calculateSubTotal1() {
                                var customerTypeId = document.getElementById("customerTypeId").value;
                                if (customerTypeId == 2) {
                                    var fixedRateWithReefer = $("#walkinFreightWithReefer").val();
                                    var fixedRateWithoutReefer = $("#walkinFreightWithoutReefer").val();
                                    var rateWithReeferPerKm = $("#walkinRateWithReeferPerKm").val();
                                    var rateWithoutReeferPerKm = $("#walkinRateWithoutReeferPerKm").val();
                                    var rateWithReeferPerKg = $("#walkinRateWithReeferPerKg").val();
                                    var rateWithoutReeferPerKg = $("#walkinRateWithoutReeferPerKg").val();
                                }
                                var freightAmount = $('#freightAmount').text();
                                var docCharges = document.getElementById('docCharges').value;
                                var odaCharges = document.getElementById('odaCharges').value;
                                var multiPickupCharge = document.getElementById('multiPickupCharge').value;
                                var multiDeliveryCharge = document.getElementById('multiDeliveryCharge').value;
                                var handleCharges = document.getElementById('handleCharges').value;
                                var otherCharges = document.getElementById('otherCharges').value;
                                var unloadingCharges = document.getElementById('unloadingCharges').value;
                                var loadingCharges = document.getElementById('loadingCharges').value;
                                var total = 0;
                                var walkInTotal = 0;
                                var kmRate = 0;
                                var kgRate = 0;
                                if (customerTypeId == 2) {
                                    var billingTypeId = $("#walkInBillingTypeId").val();
                                    var totalKm = $("#totalKm").val();
                                    var totalWeight = $("#totalWeight").text();
                                    if (fixedRateWithReefer != '' && billingTypeId == 1) {
                                        walkInTotal += parseInt(fixedRateWithReefer);
                                    } else {
                                        walkInTotal += parseInt(0);
                                    }
                                    if (fixedRateWithoutReefer != '' && billingTypeId == 1) {
                                        walkInTotal += parseInt(fixedRateWithoutReefer);
                                    } else {
                                        walkInTotal += parseInt(0);
                                    }
                                    if (rateWithReeferPerKm != '' && billingTypeId == 3) {
                                        kmRate = parseInt(totalKm) * parseInt(rateWithReeferPerKm);
                                        walkInTotal += parseInt(kmRate);
                                    } else {
                                        walkInTotal += parseInt(0);
                                    }
                                    if (rateWithoutReeferPerKm != '' && billingTypeId == 3) {
                                        kmRate = parseInt(totalKm) * parseInt(rateWithoutReeferPerKm);
                                        walkInTotal += parseInt(kmRate);
                                    } else {
                                        walkInTotal += parseInt(0);
                                    }
                                    if (rateWithReeferPerKg != '' && billingTypeId == 2) {
                                        kgRate = parseInt(totalWeight) * parseInt(rateWithReeferPerKg);
                                        walkInTotal += parseInt(kgRate);
                                    } else {
                                        walkInTotal += parseInt(0);
                                    }
                                    if (rateWithoutReeferPerKg != '' && billingTypeId == 2) {
                                        kgRate = parseInt(totalWeight) * parseInt(rateWithoutReeferPerKg);
                                        walkInTotal += parseInt(kgRate);
                                    } else {
                                        walkInTotal += parseInt(0);
                                    }
                                }
                                if (docCharges != '') {
                                    total += parseInt(docCharges);
                                } else {
                                    total += parseInt(0);
                                }
                                if (odaCharges != '') {
                                    total += parseInt(odaCharges);
                                } else {
                                    total += parseInt(0);
                                }
                                if (multiPickupCharge != '') {
                                    total += parseInt(multiPickupCharge);
                                } else {
                                    total += parseInt(0);
                                }
                                if (multiDeliveryCharge != '') {
                                    total += parseInt(multiDeliveryCharge);
                                } else {
                                    total += parseInt(0);
                                }
                                if (handleCharges != '') {
                                    total += parseInt(handleCharges);
                                } else {
                                    total += parseInt(0);
                                }
                                if (otherCharges != '') {
                                    total += parseInt(otherCharges);
                                } else {
                                    total += parseInt(0);
                                }
                                if (unloadingCharges != '') {
                                    total += parseInt(unloadingCharges);
                                } else {
                                    total += parseInt(0);
                                }
                                if (loadingCharges != '') {
                                    total += parseInt(loadingCharges);
                                } else {
                                    total += parseInt(0);
                                }
                                document.getElementById('subTotal').value = total;
                                if (customerTypeId == 1) {
                                    if (freightAmount != '') {
                                        total += parseInt(freightAmount);
                                    } else {
                                        total += parseInt(0);
                                    }
                                } else if (customerTypeId == 2) {
                                    total += parseInt(walkInTotal);
                                }
                                document.getElementById('totalCharges').value = total;
                            }
                            function calculateSubTotal() {
                                estimateFreight();
                                var freightAmount = document.cNote.totFreightAmount.value;
                                //                                alert(freightAmount);
                                var docCharges = document.getElementById('docCharges').value;
                                var odaCharges = document.getElementById('odaCharges').value;
                                var multiPickupCharge = document.getElementById('multiPickupCharge').value;
                                var multiDeliveryCharge = document.getElementById('multiDeliveryCharge').value;
                                var handleCharges = document.getElementById('handleCharges').value;
                                var otherCharges = document.getElementById('otherCharges').value;
                                var unloadingCharges = document.getElementById('unloadingCharges').value;
                                var loadingCharges = document.getElementById('loadingCharges').value;
                                var total = 0;
                                var walkInTotal = 0;
                                var kmRate = 0;
                                var kgRate = 0;
                                if (docCharges != '') {
                                    total += parseInt(docCharges);
                                } else {
                                    total += parseInt(0);
                                }
                                if (odaCharges != '') {
                                    total += parseInt(odaCharges);
                                } else {
                                    total += parseInt(0);
                                }
                                if (multiPickupCharge != '') {
                                    total += parseInt(multiPickupCharge);
                                } else {
                                    total += parseInt(0);
                                }
                                if (multiDeliveryCharge != '') {
                                    total += parseInt(multiDeliveryCharge);
                                } else {
                                    total += parseInt(0);
                                }
                                if (handleCharges != '') {
                                    total += parseInt(handleCharges);
                                } else {
                                    total += parseInt(0);
                                }
                                if (otherCharges != '') {
                                    total += parseInt(otherCharges);
                                } else {
                                    total += parseInt(0);
                                }
                                if (unloadingCharges != '') {
                                    total += parseInt(unloadingCharges);
                                } else {
                                    total += parseInt(0);
                                }
                                if (loadingCharges != '') {
                                    total += parseInt(loadingCharges);
                                } else {
                                    total += parseInt(0);
                                }
                                document.getElementById('subTotal').value = total.toFixed(2);
                                total = total + parseFloat(freightAmount);
                                document.getElementById('totalCharges').value = total.toFixed(2);
                            }

                            function callConsignorAjax(val) {
                                // Use the .autocomplete() method to compile the list based on input from user
                                //alert(val);
                                var pointNameId = 'consignorNames' + val;
                                var pointIdId = 'consignorIds' + val;
                                var deliveryAddress = 'deliveryAddress' + val;
                                var pointAddresss = 'pointAddresss' + val;
                                var phoneNos = 'phoneNos' + val;
                                var consignorAddresss = 'consignorAddresss' + val;
                                var consignorPhoneNos = 'consignorPhoneNos' + val;


                                $('#' + pointNameId).autocomplete({
                                    source: function(request, response) {
                                        $.ajax({
                                            url: '/throttle/getConsignorName.do',
                                            dataType: "json",
                                            data: {
                                                consignorName: request.term,
                                                customerId: document.getElementById('customerId').value,
                                                textBox: 1
                                            },
                                            success: function(data, textStatus, jqXHR) {

                                                var items = data;
                                                response(items);
                                            },
                                            error: function(data, type) {

                                                //console.log(type);
                                            }
                                        });
                                    },
                                    minLength: 1,
                                    select: function(event, ui) {
                                        var value = ui.item.Name;
                                        var id = ui.item.Id;
                                        var mobile = ui.item.Mobile;
                                        var address = ui.item.Address;
//                                        alert(id + " : " + value);
                                        $('#' + pointNameId).val(value);
                                        $('#' + pointIdId).val(id);
                                        $('#' + deliveryAddress).val(address);
                                        $('#' + pointAddresss).val(address);
                                        $('#' + consignorAddresss).val(address);
                                        $('#' + phoneNos).val(mobile);
                                        $('#' + consignorPhoneNos).val(mobile);



                                        document.getElementById("consignorName").value = value;
                                        document.getElementById("consignorPhoneNo").value = mobile;
                                        document.getElementById("consignorAddress").value = address;

                                        return false;
                                    }

                                    // Format the list menu output of the autocomplete
                                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                    //alert(item);
                                    var itemVal = item.Name;
                                    itemVal = '<font color="green">' + itemVal + '</font>';
                                    return $("<li></li>")
                                            .data("item.autocomplete", item)
                                            .append("<a>" + itemVal + "</a>")
                                            .appendTo(ul);
                                };


//                                $.ajax({
//                                    url: '/throttle/getConsignorName.do',
//                                    data: {customerId: document.getElementById('customerId').value},
//                                    dataType: 'json',
//                                    success: function(data) {
//                                        if (data != '') {
//                                            $('#consignorIdstemp' + val).empty();
//                                            $('#consignorIdstemp' + val).append(
//                                                    $('<option ></option>').val(0).html('--select--')
//                                                    )
//                                            $.each(data, function(i, data) {
//
//                                                $('#consignorIdstemp' + val).append(
//                                                        $('<option></option>').val(data.temp).html(data.Name)
//                                                        )
//
//                                            });
//                                        }
//                                    }
//                                });
                            }

                            function callConsigneeAjax(val) {

                                var pointNameId = 'consigneeNames' + val;
                                var pointIdId = 'consigneeIds' + val;
                                var deliveryAddress = 'deliveryAddress' + val;
                                var pointAddresss = 'pointAddresss' + val;
                                var phoneNos = 'phoneNos' + val;
                                var consignorAddresss = 'consigneeAddresss' + val;
                                var consignorPhoneNos = 'consigneePhoneNos' + val;


                                $('#' + pointNameId).autocomplete({
                                    source: function(request, response) {
                                        $.ajax({
                                            url: '/throttle/getConsigneeName.do',
                                            dataType: "json",
                                            data: {
                                                consigneeName: request.term,
                                                customerId: document.getElementById('customerId').value,
                                                textBox: 1
                                            },
                                            success: function(data, textStatus, jqXHR) {

                                                var items = data;
                                                response(items);
                                            },
                                            error: function(data, type) {

                                                //console.log(type);
                                            }
                                        });
                                    },
                                    minLength: 1,
                                    select: function(event, ui) {
                                        var value = ui.item.Name;
                                        var id = ui.item.Id;
                                        var mobile = ui.item.Mobile;
                                        var address = ui.item.Address;
//                                        alert(id + " : " + value);
                                        $('#' + pointNameId).val(value);
                                        $('#' + pointIdId).val(id);
                                        $('#' + deliveryAddress).val(address);
                                        $('#' + pointAddresss).val(address);
                                        $('#' + consignorAddresss).val(address);
                                        $('#' + phoneNos).val(mobile);
                                        $('#' + consignorPhoneNos).val(mobile);



                                        document.getElementById("consigneeName").value = value;
                                        document.getElementById("consigneePhoneNo").value = mobile;
                                        document.getElementById("consigneeAddress").value = address;

                                        return false;
                                    }

                                    // Format the list menu output of the autocomplete
                                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                    //alert(item);
                                    var itemVal = item.Name;
                                    itemVal = '<font color="green">' + itemVal + '</font>';
                                    return $("<li></li>")
                                            .data("item.autocomplete", item)
                                            .append("<a>" + itemVal + "</a>")
                                            .appendTo(ul);
                                };


//                                $.ajax({
//                                    url: '/throttle/getConsigneeName.do',
//                                    data: {customerId: document.getElementById('customerId').value},
//                                    dataType: 'json',
//                                    success: function(data) {
//                                        if (data != '') {
//                                            $('#consigneeIdstemp' + val).empty();
//                                            $('#consigneeIdstemp' + val).append(
//                                                    $('<option ></option>').val(0).html('--select--')
//                                                    )
//                                            $.each(data, function(i, data) {
//
//                                                $('#consigneeIdstemp' + val).append(
//                                                        $('<option></option>').val(data.temp).html(data.Name)
//                                                        )
//
//                                            });
//                                        }
//                                    }
//                                });
                            }
                        </script>
                        <br/>
                        <!--                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                                <tr>
                                                    <td   height="30" colspan="60">&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </td>
                                                    <td   height="30" colspan="6" align="right">Total Charges</td>
                                                    <td   height="30" align="right">INR.<input align="right" value="" type="text" readonly class="form-control" style="width:250px;height:40px" id="totalCharges" name="totalCharges" ></td>
                                                </tr>

                                            </table>-->
                        <input align="right" value="" type="hidden" readonly class="form-control" style="width:250px;height:40px" id="totalCharges" name="totalCharges" >
                        <br/>
                        <br/>

                        <center>
                            <!--                        <input type="button" class="btn btn-success" name="Save" value="Estimate Freight" onclick="estimateFreight();" >-->
                            <div id="orderButton" style="display:none;">
                                <input type="button" class="btn btn-success" name="Save" value="Alter" id="createOrder" onclick="submitPage(this.value);" >
                            </div>
                        </center>
                        <!--                </div>-->
                    </div>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>