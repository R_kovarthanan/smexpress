<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true
        });
    });
</script>
<script type="text/javascript" language="javascript">
    function submitPage() {
        if (document.getElementById('consignmentStatusId').value == '0') {
            alert("please select status");
            document.getElementById('consignmentStatusId').focus();
        } else if (document.getElementById('cancelRemarks').value == '') {
            alert("please enter the remarks");
            document.getElementById('cancelRemarks').focus();
        } else {
            document.cNote.action = '/throttle/cancelConsignment.do';
            document.cNote.submit();
        }
    }

</script>
<script language="">
    function print(val)
    {
        var DocumentContainer = document.getElementById(val);
        var WindowObject = window.open('', "TrackHistoryData",
                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Trip Planning" text="Trip Planning"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.Trip Planning" text="Trip Planning"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">


            <body>
                <form name="cNote" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <table width="100%">
                        <tr></tr>
                    </table>
                    <br>
                    <c:set var="orderStatus" value="0" />
                    <c:if test="${consignmentList != null}">
                        <c:forEach items="${consignmentList}" var="cnote">
                            <table class="table table-info mb30 table-hover">
                                <thead><tr>
                                        <th  colspan="4">Consignment Note <input type="hidden" name="consignmentOrderId" id="consignmentOrderId" value="<c:out value="${cnote.consignmentOrderId}"/>"/></th>
                                    </tr></thead>
                                <tr>
                                    <td >Entry Option</td>
                                    <td >
                                        <c:if test="${cnote.entryType == 1}">
                                            <label>Manual</label>
                                        </c:if>
                                        <c:if test="${cnote.entryType == 2}">
                                            <label>Excel Upload</label>
                                        </c:if>
                                    </td>
                                    <td >Customer Type</td>
                                    <td >
                                        <c:if test="${customerTypeList != null}">
                                            <c:forEach items="${customerTypeList}" var="cusType">
                                                <c:if test="${cnote.customerTypeId == cusType.customerTypeId}">
                                                    <label><c:out value="${cusType.customerTypeName}"/></label>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </td>
                                </tr>
                                <tr>
                                    <td >Consignment Note </td>
                                    <td ><label><c:out value="${cnote.consignmentNoteNo}"/></label></td>
                                    <td >Consignment Date</td>
                                    <td ><label><c:out value="${cnote.consignmentOrderDate}"/></label></td>
                                            <c:set var="orderStatus" value="${cnote.consigmentOrderStatus}" />
                                </tr>
                                <!--<tr>-->
                                <!--<tr>-->
                                <!--<td >Customer Reference No</td>-->
                                <!--<td ><label>-->
                                <%--<c:out value="${cnote.consignmentRefNo}"/>--%>
                                <!--</label></td>-->
                                <!--<td >Customer Reference  Remarks</td>-->
                                <!--<td ><label>-->
                                <%--<c:out value="${cnote.consignmentOrderRefRemarks}"/>--%>
                                <!--</label></td>-->
                                <!--<tr>-->

                                <!--                            <td >Product Category</td>
                                                            <td >
                                <%--<c:if test="${productCategoryList != null}">--%>
                                <%--<c:forEach items="${productCategoryList}" var="proList">--%>
                                <%--<c:if test="${cnote.productCategoryId == proList.productCategoryId}">--%>
                                    <label>
                                <%--<c:out value="${proList.customerTypeName}"/>--%>
                        </label>
                                <%--</c:if>--%>
                                <%--</c:forEach>--%>
                                <%--</c:if>--%>
                            </td>-->
                            </table>
                            <br>
                            <br>

                            <div id="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active" data-toggle="tab"><a href="#customerDetail"><span>Basic Details</span></a></li>
                                    <li class="active" data-toggle="tab"><a href="#routeDetail" id="showRouteCourse"><span>Route Course</span></a></li>
                                    <!--<li class="active" data-toggle="tab"><a href="#paymentDetails"><span>Invoice Info</span></a></li>-->
                                        <c:if test="${orderStatus == 5 || orderStatus == 3 || orderStatus == 6}">
                                        <li class="active" data-toggle="tab"><a href="#action"><span>Action</span></a></li>
                                        </c:if>
                                </ul>

                                <div id="customerDetail">
                                    <table  class="table table-info mb30 table-hover" id="contractCustomerTable">
                                        <thead><tr>
                                                <th  colspan="4" >Contract Customer Details</th>
                                            </tr></thead>
                                        <tr>
                                            <td >Name</td>
                                            <td ><label><c:out value="${cnote.customerName}"/></label></td>
                                            <td >Code</td>
                                            <td ><label><c:out value="${cnote.customerCode}"/></label></td>
                                        </tr>
                                        <tr>
                                            <td >Address</td>
                                            <td ><label><c:out value="${cnote.customerAddress}"/></label></td>
                                            <td >Pincode</td>
                                            <td ><label><c:out value="${cnote.customerPincode}"/></label></td>
                                        </tr>
                                        <tr>
                                            <td >Mobile No</td>
                                            <td ><label><c:out value="${cnote.customerMobile}"/></label></td>
                                            <td >E-Mail ID</td>
                                            <td ><label><c:out value="${cnote.customerEmail}"/></label></td>
                                        </tr>
                                        <tr>
                                            <td >Phone No</td>
                                            <td ><label><c:out value="${cnote.customerPhone}"/></label></td>

                                            <td >Billing Type</td>
                                            <td >
                                                <c:if test="${billingTypeList != null}">
                                                    <c:forEach items="${billingTypeList}" var="billType">
                                                        <c:if test="${cnote.customerBillingType == billType.billingTypeId}">
                                                            <label><c:out value="${billType.billingTypeName}"/></label>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if>
                                            </td>

                                        </tr>
                                    </table>
                                    <c:if test="${cnote.customerTypeId == '1'}" >      
                                        <div id="contractDetails" style="display: none">
                                            <table>
                                                <tr>
                                                    <td >Contract No :</td>
                                                    <td ><label><c:out value="${cnote.customerContractId}"/></label></td>
                                                    <td >Contract Expiry Date :</td>
                                                    <td ><label><c:out value="${cnote.contractFrom}"/></label></td>
                                                    <td  >&nbsp;&nbsp;&nbsp;</td>
                                                    <td ><input type="button" class="btn btn-success" value="View Contract" onclick="viewCustomerContract()"></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <script>
                                            function viewCustomerContract() {
                                                window.open('/throttle/viewCustomerContract.do?custId=' +<c:out value="${cnote.customerContractId}"/>, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                                            }
                                        </script>
                                    </c:if>            
                                    <br>
                                    <br>
                                    <table class="table table-info mb30 table-hover" id="bg">

                                        <thead><tr>
                                                <th  colspan="4" >Consignment Details</th>
                                            </tr></thead>
                                        <tr>    
                                            <c:if test="${cnote.customerBillingType eq 3 && cnote.customerTypeId eq 1}">
                                                <td >Origin</td>
                                                <td ><label><c:out value="${cnote.origin}"/></label></td>
                                                <td >Destination</td>
                                                <td ><label><c:out value="${cnote.destination}"/></label></td>
                                                    </c:if>
                                                    <c:if test="${cnote.customerTypeId eq 2}">
                                                <td >Origin</td>
                                                <td ><label><c:out value="${cnote.origin}"/></label></td>
                                                <td >Destination</td>
                                                <td ><label><c:out value="${cnote.destination}"/></label></td>
                                                    </c:if>
                                                    <c:if test="${cnote.customerBillingType ne 3 && cnote.customerTypeId eq 1}">
                                                <td >Contract Route &ensp;&ensp;<label><c:out value="${cnote.routeContractCode}"/></label></td>
                                                <td>&nbsp;</td>
                                                <td >Remarks &ensp;&ensp;<label><c:out value="${cnote.consignmentOrderRefRemarks}"/></label></td>
                                                <td>&nbsp;</td>
                                            </c:if>
                                            <!--                                            <td >Business Type</td>
                                                                                        <td >
                                                                                            <label>
                                            <%--<c:if test="${cnote.budinessType == 1}">--%>
                                                Primary
                                            <%--</c:if>--%>
                                            <%--<c:if test="${cnote.budinessType == 2}">--%>
                                                Secondary
                                            <%--</c:if>--%>
                                        </label>
                                    </td>-->
                                        </tr>
                                        <tr style="display: none">
                                            <td >Multi Pickup</td>
                                            <td ><label>
                                                    <c:if test="${cnote.multiPickup == 'Y'}">
                                                        Yes
                                                    </c:if>
                                                    <c:if test="${cnote.multiPickup == 'N'}">
                                                        No
                                                    </c:if>
                                                </label></td>
                                            <td >Multi Delivery</td>
                                            <td ><label>
                                                    <c:if test="${cnote.multiDelivery == 'Y'}">
                                                        Yes
                                                    </c:if>
                                                    <c:if test="${cnote.multiDelivery == 'N'}">
                                                        No
                                                    </c:if>
                                                </label></td>
                                            <td >Special Instruction</td>
                                            <td ><label><c:out value="${cnote.consigmentInstruction}"/></label></td>
                                        </tr>

                                    </table>
                                    <table class="table table-info mb30 table-hover" id="addTyres1" style="display: none">
                                        <thead> <tr>
                                                <th width="20"  align="center" height="30" >Sno</th>
                                                <th  height="30" >Product/Article Code</th>
                                                <th  height="30" >Product/Article Name </th>
                                                <th  height="30" >Batch Code</th>
                                                <th  height="30" >No of Packages</th>
                                                <th  height="30" >UOM</th>
                                                <th  height="30" >Total Weight (in Kg)</th>
                                            </tr></thead>
                                            <% int i = 1;%>
                                            <c:if test="${consignmentArticles != null}">
                                            <tbody>
                                                <c:forEach items="${consignmentArticles}" var="article">
                                                    <tr>
                                                        <td  height="30" ><label><%=i++%></label></td>
                                                        <td  height="30" ><label><c:out value="${article.articleCode}"/></label></td>
                                                        <td  height="30" ><label><c:out value="${article.articleName}"/></label></td>
                                                        <td  height="30" ><label><c:out value="${article.batchName}"/></label></td>
                                                        <td  height="30" ><label><c:out value="${article.packageNos}"/></label></td>
                                                        <td  height="30" >
                                                            <c:if test="${article.unitOfMeasurement == 1}">
                                                                Box
                                                            </c:if>
                                                            <c:if test="${article.unitOfMeasurement == 2}">
                                                                Bag
                                                            </c:if>
                                                            <c:if test="${article.unitOfMeasurement == 3}">
                                                                Pallet
                                                            </c:if>
                                                            <c:if test="${article.unitOfMeasurement == 4}">
                                                                Each
                                                            </c:if>
                                                        </td>
                                                        <td  height="30" ><label><c:out value="${article.packageWeight}"/></label></td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </c:if>
                                    </table>
                                    <br>
                                    <br>

                                    <table class="table table-info mb30 table-hover" id="bg">

                                        <tr>
                                            <td>
                                                <label >Total No Packages</label>
                                                <label><c:out value="${cnote.totalPackages}"/></label>
                                            </td>
                                            <!--                                                        <td>
                                                                                                        <label >Total Weight (Kg)</label>
                                                                                                        <label><c:out value="${cnote.totalWeight}"/></label>
                                                                                                    </td>-->
                                        </tr>
                                    </table>

                                    <br>
                                    <table  class="table table-info mb30 table-hover" id="bg">

                                        <thead> <tr>
                                                <th  colspan="6" >Vehicle (Required) Details</th>
                                            </tr></thead>

                                        <tr>
                                            <td >Service Type</td>
                                            <td ><label>
                                                    <c:if test="${cnote.serviceType == 1}">
                                                        FTL
                                                    </c:if>
                                                    <c:if test="${cnote.serviceType == 2}">
                                                        LTL
                                                    </c:if>
                                                </label>
                                            </td>
                                            <td >Vehicle Type</td>
                                            <td > <c:if test="${vehicleTypeList != null}">
                                                    <c:forEach items="${vehicleTypeList}" var="vehType">
                                                        <c:if test="${cnote.vehicleTypeId == vehType.vehicleTypeId}">
                                                            <label><c:out value="${vehType.vehicleTypeName}"/></label>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if></label></td>
                                            <td ></td>
                                            <td ></td>
                                            <%--<c:if test="${cnote.reeferRequired == 'Yes'}">--%>
                                            <%--</c:if>--%>
                                            <%--<c:if test="${cnote.reeferRequired == 'No'}">--%>
                                            <%--</c:if>--%>
                                            <!--                                                            No
                                                                                                        Yes-->

                                        </tr>
                                        <tr>
                                            <td >Vehicle Required Date</td>
                                            <td ><label><c:out value="${cnote.vehicleRequiredDate}"/></label></td>
                                            <td >Vehicle Required Time</td>
                                            <td ><label><c:out value="${cnote.vehicleRequiredTime}"/></label></td>
                                            <td >Special Instruction</td>
                                            <td ><label><c:out value="${cnote.vehicleInstruction}"/></label></td>
                                        </tr>
                                    </table>
                                    <br>
                                    <br>
                                    <table  class="table table-info mb30 table-hover" id="bg">
                                        <thead> <tr>
                                                <th   height="30" colspan="6">Consignor Details</th>
                                            </tr></thead>
                                        <tr>
                                            <td >Consignor Name</td>
                                            <td ><label><c:out value="${cnote.consignorName}"/></label></td>
                                            <td >Mobile No</td>
                                            <td ><label><c:out value="${cnote.consignorMobile}"/></label></td>
                                            <td >Address</td>
                                            <td ><label><c:out value="${cnote.consignorAddress}"/></label></td>
                                        </tr>
                                    </table>
                                    <br>
                                    <br>
                                    <table  class="table table-info mb30 table-hover" id="bg">
                                        <thead><tr>
                                                <th   height="30" colspan="6">Consignee Details</th>
                                            </tr></thead>
                                        <tr>
                                            <td >Consignee Name</td>
                                            <td ><label><c:out value="${cnote.consigneeName}"/></label></td>
                                            <td >Mobile No</td>
                                            <td ><label><c:out value="${cnote.consigneeMobile}"/></label></td>
                                            <td >Address</td>
                                            <td ><label><c:out value="${cnote.consigneeAddress}"/></label></td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <br/>
                                    <center>
                                        <a  class="nexttab" ><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:100px;height:35px;"/></a>
                                        
                                    </center>
                                </div>



                                <div id="routeDetail">
                                    <table class="table table-info mb30 table-hover">
                                        <thead>
                                            <tr>
                                                <th  height="30" >Point Name</th>
                                                <th  height="30" >Type</th>
                                                <th  height="30" >Route Order</th>
                                                <th  height="30" >Address</th>
                                                <th  height="30" >Planned Date</th>
                                                <th  height="30" >Planned Time</th>
                                            </tr>
                                        </thead>
                                        <c:if test="${consignmentPoint != null}">
                                            <tbody>
                                                <c:forEach items="${consignmentPoint}" var="point">
                                                    <tr>
                                                        <td  height="30" ><label><c:out value="${point.pointNames}"/></label></td>
                                                        <td  height="30" ><label><c:out value="${point.consignmentPointType}"/></label></td>
                                                        <td  height="30" ><label><c:out value="${point.pointSequence}"/></label></td>
                                                        <td  height="30" ><label><c:out value="${point.pointAddress}"/></label></td>
                                                        <td  height="30" ><label><c:out value="${point.pointDate}"/></label></td>
                                                        <td  height="30" ><label><c:out value="${point.pointPlanTime}"/></label></td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </c:if>
                                    </table>
                                    <br>
                                    <center>

                                        <a  class="nexttab" ><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:100px;height:35px;"/></a>
                                        <a  class="nexttab" ><input type="button" class="btn btn-success btnPrevious" value="previous" name="previous" style="width:100px;height:35px;"/></a>
                                    </center>
                                    <br>
                                    <br>
                                </div>




                                <div id="paymentDetails" style="display:none;">
                                    <table  class="table table-info mb30 table-hover" id="contractFreightTable">
                                        <thead>  <tr>
                                                <th   height="30" colspan="6" >Contract Freight Details</th>
                                            </tr></thead>
                                        <tr>
                                            <td >Freight Charges</td>
                                            <td >INR. <label><c:out value="${cnote.freightCharges}"/></label></td>
                                        </tr>
<!--                                        <tr>
                                            <td >Sub Total</td>
                                            <td >INR. <label><c:out value="${cnote.totalStatndardCharges}"/></label></td>
                                        </tr>-->
                                    </table>
                                    <br/>
                                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg" style="display: none">
                                        <tr>
                                            <td   height="30" colspan="6" align="right">Total Charges</td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <td   height="30" align="right">INR.<label><c:out value="${cnote.freightCharges}"/></label></td>
                                        </tr>
                                    </table>
                                    <center>
                                        <a  class="nexttab" ><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:100px;height:35px;"/></a>
                                        <a  class="nexttab" ><input type="button" class="btn btn-success btnPrevious" value="previous" name="previous" style="width:100px;height:35px;"/></a>
                                    </center>
                                </div>
                                <script>
                                    $(".nexttab").click(function() {
                                        var selected = $("#tabs").tabs("option", "selected");
                                        $("#tabs").tabs("option", "selected", selected + 1);
                                    });
                                </script>

                                <c:if test="${orderStatus == 5 || orderStatus == 3 || orderStatus == 6}">
                                    <div id="action">
                                        <table  class="table table-info mb30 table-hover" id="contractFreightTable">
                                            <thead><tr>
                                                    <th   height="30" colspan="6" >Consignment Action</th>
                                                </tr></thead>
                                            <tr>
                                                <td >Consignment Status</td>
                                                <td ><select name="consignmentStatusId" id="consignmentStatusId" class="form-control" style="width:250px;height:40px" onchange="updateConsignment(this.value)">
                                                        <option value="0">--Select--</option>
                                                        <c:if test="${orderStatus == 6}">
                                                            <option value="4">Cancel Consignment</option>
                                                        </c:if>
                                                        <c:if test="${orderStatus == 5}">
                                                            <option value="4">Cancel Consignment</option>
                                                            <option value="3">Hold Consignment</option>
                                                        </c:if>
                                                        <c:if test="${orderStatus == 3}">
                                                            <option value="4">Cancel Consignment</option>
                                                            <option value="<c:out value="${cnote.nextStatusId}"/>">UnHold Consignment</option>
                                                        </c:if>
                                                    </select></td>
                                                <td >Remarks</td>
                                                <td ><textarea name="cancelRemarks" id="cancelRemarks" cols="100" rows="6" class="form-control" style="width:300px;height:50px"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                            <center>
                                                <a  class="nexttab" ><input type="button" class="btn btn-success btnPrevious" value="previous" name="previous" style="width:100px;height:35px;"/></a>
                                                <input type="button" class="btn btn-success" value="Save" name="Save" onclick="submitPage()" style="display:none;" id="saveConsignment">
                                            </center>
                                            </td>
                                            </tr>
                                        </table>
                                        <script>
                                            function updateConsignment(val) {
                                                if (val == '0') {
                                                    $("#saveConsignment").hide();
                                                    $("#cancelRemarks").readOnly = true;
                                                } else {
                                                    $("#saveConsignment").show();
                                                    $("#cancelRemarks").readOnly = false;
                                                }
                                            }
                                        </script>

                                    </div>
                                </c:if>
                            </c:forEach>
                        </c:if>
                        <c:if test="${cancelConsignment != null}">
                            <script>
                                window.close();
                            </script>
                        </c:if>        

                        <script>
                            $('.btnNext').click(function() {
                                $('.nav-tabs > .active').next('li').find('a').trigger('click');
                            });
                            $('.btnPrevious').click(function() {
                                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                            });
                        </script>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>