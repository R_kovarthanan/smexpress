<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>




<script type="text/javascript">
    function submitPage(value) {
        document.customerContract.action = "/throttle/saveCustomerContract.do";
        document.customerContract.submit();
    }

</script>

<!--    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>-->
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ViewContract" text="ViewContract"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.ViewContract" text="ViewContract"/></li>

		                </ol>
		            </div>
        </div>

        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">

    <body onload="">
        <form name="customerContract"  method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>



            <table class="table table-info mb30 table-hover" id="bg" >
                <thead>

                <tr>
                    <th  colspan="4" >Customer Contract Master</th>
                </tr>
                </thead>
                <tr>
                    <td >Customer Name</td>
                    <td ><input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>" class="form-control" style="width:250px;height:40px"><input type="hidden" name="customerName" id="customerName" value="<c:out value="${customerName}"/>" class="form-control" style="width:250px;height:40px" readonly=""><c:out value="${customerName}"/></td>
                    <td >Customer Code</td>
                    <td ><input type="hidden" name="customerCode" id="customerCode" value="<c:out value="${customerCode}"/>" class="form-control" style="width:250px;height:40px" readonly><c:out value="${customerCode}"/></td>
                </tr>
                <tr>
                    <td >Contract From</td>
                    <td ><input type="hidden" name="contractFrom" id="contractFrom" value="<c:out value="${contractFrom}"/>" class="datepicker"><c:out value="${contractFrom}"/></td>
                    <td >Contract To</td>
                    <td ><input type="hidden" name="contractTo" id="contractTo" value="<c:out value="${contractTo}"/>" class="datepicker"><c:out value="${contractTo}"/></td>
                </tr>
                <tr>
                    <td >Contract No</td>
                    <td ><input type="hidden" name="contractNo" id="contractNo" value="<c:out value="${contractNo}"/>" class="form-control" style="width:250px;height:40px" ><c:out value="${contractNo}"/></td>
                    <td >Billing Type</td>
                    <td ><input type="hidden" name="billingTypeId" id="billingTypeId" value="<c:out value="${billingTypeId}"/>"/>
                        <c:if test="${billingTypeList != null}">
                            <c:forEach items="${billingTypeList}" var="btl">
                                <c:if test="${billingTypeId == btl.billingTypeId}">
                                    <c:out value="${btl.billingTypeName}"/>
                                </c:if>
                            </c:forEach>
                        </c:if>
                    </td>
                </tr>
            </table>


            <div id="tabs" >
                <ul>
                        <c:if test="${billingTypeId == 1}">
                        <li id="pp" style="display: block"><a href="#ptp"><span>Point to Point - Based </span></a></li>
                        </c:if>
                         <c:if test="${billingTypeId == 1}">
                            <!--<li data-toggle="tab" id="pc" style="display: block"><a href="#pcm"><span> Penality charges </span></a></li>-->
                        </c:if>
                        <c:if test="${billingTypeId == 1}">
                            <!--<li data-toggle="tab" id="dc" style="display: block"><a href="#dcm"><span> Detention charges </span></a></li>-->
                        </c:if>  
                        <c:if test="${billingTypeId == 2}">
                        <li id="pw" style="display: block"><a href="#ptpw"><span>Point to Point Weight - Based </span></a></li>
                        </c:if>
                        <c:if test="${billingTypeId == 3}">
                        <li id="akm" style="display: block"><a href="#mfr"><span>Actual KM - Based </span></a></li>
                        </c:if>
                        <c:if test="${billingTypeId == 4}">
                        <li><a href="#fkmRate"><span>Fixed KM - Based - Rate Details </span></a></li>
                        <li><a href="#fkmRoute"><span>Fixed KM - Based - Route Details </span></a></li>
                        </c:if>
                </ul>





                <c:if test="${billingTypeId == 1 }">
                    <div id="ptp">
                        <div class="inpad">

                            <c:if test = "${ptpBillingList != null}">

                                    <table class="table table-info mb30 table-hover" id="itemsTable" >
                                    <thead>
                                        <% int sno1 = 1;%>
                                        <tr height="110">
                                            <th>S.No</th>
                                    <!--<th>Vehicle Route Contract Code</th>-->
                                    <th>Vehicle Type</th>
                                    <th>Movement Type</th>
                                    <th>First Pick UP</th>
                                    <th>Touch Point 1</th>
                                    <th>Touch Point 2</th>
                                    <th>Touch Point 3</th>
                                    <th>Touch Point 4</th>
                                    <th>Final Drop Point</th>
                                    <th>Travel Km</th>
                                    <th>Travel Time</th>
                                    <th>Agreed Fuel Price</th>
                                    <th>Rate</th>
                                    <th>TAT(Days)</th>
                                    <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${ptpBillingList}" var="ptpl">
                                            <tr>
                                                <td><%=sno1%><input type="hidden" name="ptpRouteContractCode" id="ptpRouteContractCode" value="<c:out value="${ptpl.routeCode}" />" class="tInput"  style="width: 60px;"/></td>
                                                <td>
                                                    <input type="hidden" name="ptpVehTypeId" id="ptpVehTypeId" value="<c:out value="${ptpl.vehicleTypeId}" />" class="tInput"  style="width: 60px;"/>
                                                    <input type="hidden" name="ptpRouteContractId" id="ptpRouteContractId" value="<c:out value="${ptpl.routeContractId}" />" class="tInput"  style="width: 60px;"/>
                                                    <c:out value="${ptpl.vehicleTypeName}" />
                                                </td>
                                              
                                                <td><c:out value="${ptpl.movementTypeId}" /></td>
                                                
                                                <td><input type="hidden" name="ptpFirstPickupId" value="<c:out value="${ptpl.firstPickupId}" />" class="tInput" id="ptpFirstPickupId"  style="width: 90px;"/><c:out value="${ptpl.firstPickupName}" /></td>
                                                <td><input type="hidden" name="ptpPoint1Id" value="<c:out value="${ptpl.point1Id}" />" class="tInput" id="ptpPoint1Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Km" value="<c:out value="${ptpl.point1Km}" />" class="tInput" id="ptpPoint1Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Hrs" value="<c:out value="${ptpl.point1Hrs}" />" class="tInput" id="ptpPoint1Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Minutes" value="<c:out value="${ptpl.point1Minutes}" />" class="tInput" id="ptpPoint1Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint1RouteId" value="<c:out value="${ptpl.point1RouteId}" />" class="tInput" id="ptpPoint1RouteId"  style="width: 90px;"/><c:out value="${ptpl.point1Name}" /></td>
                                                <td><input type="hidden" name="ptpPoint2Id" value="<c:out value="${ptpl.point2Id}" />" class="tInput" id="ptpPoint2Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Km" value="<c:out value="${ptpl.point2Km}" />" class="tInput" id="ptpPoint2Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Hrs" value="<c:out value="${ptpl.point2Hrs}" />" class="tInput" id="ptpPoint2Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Minutes" value="<c:out value="${ptpl.point2Minutes}" />" class="tInput" id="ptpPoint2Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint2RouteId" value="<c:out value="${ptpl.point2RouteId}" />" class="tInput" id="ptpPoint2RouteId"  style="width: 90px;"/><c:out value="${ptpl.point2Name}" /></td>
                                                <td><input type="hidden" name="ptpPoint3Id" value="<c:out value="${ptpl.point3Id}" />" class="tInput" id="ptpPoint3Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Km" value="<c:out value="${ptpl.point3Km}" />" class="tInput" id="ptpPoint3Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Hrs" value="<c:out value="${ptpl.point3Hrs}" />" class="tInput" id="ptpPoint3Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Minutes" value="<c:out value="${ptpl.point3Minutes}" />" class="tInput" id="ptpPoint3Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint3RouteId" value="<c:out value="${ptpl.point3RouteId}" />" class="tInput" id="ptpPoint3RouteId"  style="width: 90px;"/><c:out value="${ptpl.point3Name}" /></td>
                                                <td><input type="hidden" name="ptpPoint4Id" value="<c:out value="${ptpl.point4Id}" />" class="tInput" id="ptpPoint4Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Km" value="<c:out value="${ptpl.point4Km}" />" class="tInput" id="ptpPoint4Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Hrs" value="<c:out value="${ptpl.point4Hrs}" />" class="tInput" id="ptpPoint4Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Minutes" value="<c:out value="${ptpl.point4Minutes}" />" class="tInput" id="ptpPoint4Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint4RouteId" value="<c:out value="${ptpl.point4RouteId}" />" class="tInput" id="ptpPoint4RouteId"  style="width: 90px;"/><c:out value="${ptpl.point4Name}" /></td>
                                                <td><input type="hidden" name="ptpFinalPointId" value="<c:out value="${ptpl.finalPointId}" />" class="tInput" id="ptpFinalPointId"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointKm" value="<c:out value="${ptpl.finalPointKm}" />" class="tInput" id="ptpFinalPointKm"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointHrs" value="<c:out value="${ptpl.finalPointHrs}" />" class="tInput" id="ptpFinalPointHrs"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointMinutes" value="<c:out value="${ptpl.finalPointMinutes}" />" class="tInput" id="ptpFinalPointMinutes"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointRouteId" value="<c:out value="${ptpl.finalPointRouteId}" />" class="tInput" id="ptpFinalPointRouteId"  style="width: 90px;"/><c:out value="${ptpl.finalPointName}" /></td>

                                                <td><input type="hidden" name="ptpTotalKm" value="<c:out value="${ptpl.totalKm}" />" class="tInput" id="ptpTotalKm"  style="width: 90px;"/><c:out value="${ptpl.totalKm}" /></td>
                                                <td><input type="hidden" name="ptpTravelTime" value="<c:out value="${ptpl.totalHours}" />:<c:out value="${ptpl.totalMinutes}" />:00" class="tInput" id="<c:out value="${ptpl.totalHours}" />"  style="width: 90px;"/><c:out value="${ptpl.totalHours}" />:<c:out value="${ptpl.totalMinutes}" />:00</td>
                                                <td><input type="hidden" name="ptpRateWithReefer" value="<c:out value="${ptpl.rateWithReeferPerKm}" />" class="tInput" id="ptpRateWithReefer"  style="width: 60px;" /><c:out value="${ptpl.rateWithReefer}" /></td>
                                                <td><input type="hidden" name="ptpRateWithoutReefer" value="<c:out value="${ptpl.rateWithoutReeferPerKm}" />" class="tInput" id="ptpRateWithoutReefer" style="width: 60px;" /><c:out value="${ptpl.rateWithoutReefer}" /></td>
                                                <td><input type="hidden" name="tatTime" value="<c:out value="${ptpl.tatTime}" />" class="tInput" id="tatTime" style="width: 60px;" /><c:out value="${ptpl.tatTime}" /></td>
                                                <td><input type="hidden" name="status" value="<c:out value="${ptpl.activeInd}" />" class="tInput" id="status" style="width: 60px;" /><c:out value="${ptpl.activeInd}" /></td>
                                            </tr>
                                            <%sno1++;%>
                                            
                                        </c:forEach >
                                    </tbody>
                                </table>

                                    <table align="center">
                                            <!--<tr align="center"><td> <a href="/throttle/editCustomerContract.do?custId=<c:out value="${customerId}"/>"><input type="button" class="btn btn-success" value="Edit"  style="width: 120px"/></a> </tr></td>-->
                                            </table>
                            <script language="javascript" type="text/javascript">
                                setFilterGrid("itemsTable");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" selected="selected">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span>Entries Per Page</span>
                                </div>
                            </c:if>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text">Displaying Page 1 of 1</div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("itemsTable", 1);
                            </script>
                        </div>
                    </div>
                    
                    <div id="pcm" class="tab-pane" style="display:none">
                                        <div class="inpad">
                                            <table class="table table-info mb30 table-hover" width="90%" id="penalitytable">
                                                <thead>
                                                    <tr id="rowId0" >
                                                        <td    id="tableDesingTD">S No</td>
                                                        <td    id="tableDesingTD">Charges names</td>
                                                        <td    id="tableDesingTD">Unit</td>
                                                        <td    id="tableDesingTD">Amount</td>
                                                        <td    id="tableDesingTD">Remarks</td>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <% int sno = 0;%>
                                                    <c:forEach items="${viewpenalitycharge}" var="vpc">
                                                        <%
                                                                    sno++;
                                                        %>
                                                        <tr>
                                                            <td   align="left" > <%= sno%>  </td>
                                                            <td   align="left"> <c:out value="${vpc.penality}" /></td>
                                                            <td   align="left"> <c:out value="${vpc.pcmunit}" /></td>
                                                            <td align="left"   >
                                                                <input type="hidden" name="penalitycharge" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${vpc.chargeamount}" />"/><c:out value="${vpc.chargeamount}" />
                                                            </td>
                                                            <td   align="left"> <c:out value="${vpc.pcmremarks}" /></td>
                                                            <td align="left"   ><input type="hidden"  name="penid" id="penid" value="<c:out value="${vpc.id}"/>" /></td>
                                                        </tr>
                                                    </c:forEach>

                                                    <tr>
                                                <input type="hidden" name="selectedRowCount" id="sno" value="1"/>
                                                <input type="hidden" name="tripSheetId" id="tripSheetId" value="<%=request.getParameter("tripSheetId")%>" />
                                                </tbody>
                                            </table>
                                            <br>
                                            <center>
                                                <!--<INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRowPenality()" />-->
                                                <!--<INPUT type="button" class="button" value="Delete Row" onclick="deleteRowPenality()" />-->
                                                &nbsp;&nbsp;
                                                <!--<input type="button" class="btn btn-success" value="Save" onclick="Savepencharge(this.value)"/>-->
                                            </center>
                                            <SCRIPT language="javascript">
                                                var poItems = 1;
                                                var rowCount = 2;
                                                var sno = 1;
                                                var snumber = 1;

                                                function addRowPenality()
                                                {
                                                    //                                           alert(rowCount);
                                                    if (sno < 20) {
                                                        sno++;
                                                        var tab = document.getElementById("penalitytable");
                                                        rowCount = tab.rows.length;

                                                        snumber = (rowCount) - 1;
                                                        if (snumber == 1) {
                                                            snumber = parseInt(rowCount);
                                                        } else {
                                                            snumber++;
                                                        }
                                                        snumber = snumber - 1;
                                                        var newrow = tab.insertRow((rowCount));
                                                        newrow.height = "30px";
                                                        // var temp = sno1-1;
                                                        var cell = newrow.insertCell(0);
                                                        var cell0 = "<td class='text1' align='center'> " + snumber + "</td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(1);
                                                        cell0 = "<td class='text1'><input type='text' name='penality' id='penality" + snumber + "' class='form-control' /></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(2);
                                                        cell0 = "<td class='text1'><input type='text' name='pcmunit' id='pcmunit" + snumber + "' class='form-control' /></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(3);
                                                        cell0 = "<td class='text1'><input type='text' name='chargeamount' id='chargeamount" + snumber + "' class='form-control' onKeyPress='return onKeyPressBlockCharacters(event);' /></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(4);
                                                        cell0 = "<td class='text1'><input type='text' name='pcmremarks' id='pcmremarks" + snumber + "' class='form-control' /></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        rowCount++;
                                                    }
                                                }

                                            </SCRIPT>
                                        </div>
                                        &nbsp;&nbsp;
                                        <center>
                                            <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                            <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                        </center>
                                        <br>
                                    </div>
                                       <div id="dcm" class="tab-pane" style="display:none">
                                        <div class="inpad">
                                            <table class="table table-info mb30 table-hover" width="90%" id="detentiontable">
                                                <thead>
                                                    <tr id="rowId1" >
                                                        <td id="tableDesingTD" >S No</td>
                                                        <td id="tableDesingTD"  >Vehicle type</td>
                                                        <td id="tableDesingTD"  >Time Slot From(Days)</td>
                                                        <td id="tableDesingTD"  >Time Slot To(Days)</td>
                                                        <td id="tableDesingTD"  >Amount</td>
                                                        <td id="tableDesingTD"  >Remarks</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <% int Sno = 0;%>
                                                    <c:forEach items="${viewdetentioncharge}" var="vdc">
                                                        <%
                                                                    Sno++;
                                                        %>
                                                        <tr>
                                                            <td   align="left"> <%= Sno%>  </td>
                                                            <td   align="left"> <c:out value="${vdc.vehicleTypeName}" /></td>
                                                            <td   align="left"> <c:out value="${vdc.timeSlot}" /></td>
                                                            <td   align="left"> <c:out value="${vdc.timeSlotTo}" /></td>

                                                            <td align="left"   >
                                                                <input type="hidden" name="detentioncharge" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${vdc.chargeamt}" />"  style="width:150px;height:44px;"/><c:out value="${vdc.chargeamt}" />
                                                            </td>
                                                            <td   align="left"> <c:out value="${vdc.dcmremarks}" /></td>
                                                            <td align="left"   ><input type="hidden"  name="denid" id="denid" value="<c:out value="${vdc.id}"/>" /></td>

                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                            <br>
                                            <center>
                                                <!--<INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRowdetention()" />-->
                                                <!--<INPUT type="button" class="button" value="Delete Row" onclick="deleteRowdetention()" />-->
                                                <br>
                                                <!--<input type="button" class="btn btn-success" value="Save" onclick="Savedencharge(this.value)"/>-->
                                            </center>
                                            <SCRIPT language="javascript">
                                                var poItems1 = 1;
                                                var rowCount1 = 2;
                                                var sno1 = 1;
                                                var snumber1 = 1;

                                                function addRowdetention()
                                                {
                                                    //                                           alert(rowCount);
                                                    if (sno1 < 20) {
                                                        sno1++;
                                                        var tab = document.getElementById("detentiontable");
                                                        rowCount1 = tab.rows.length;

                                                        snumber1 = (rowCount1) - 1;
                                                        if (snumber1 == 1) {
                                                            snumber1 = parseInt(rowCount1);
                                                        } else {
                                                            snumber1++;
                                                        }
                                                        //                                            snumber1 = snumber1-1;
                                                        //alert( (rowCount)-1) ;
                                                        //alert(rowCount);
                                                        var newrow = tab.insertRow((rowCount1));
                                                        newrow.height = "30px";
                                                        // var temp = sno1-1;
                                                        var cell = newrow.insertCell(0);
                                                        var cell0 = "<td class='text1' align='center'> " + snumber1 + "</td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(1);
                                                        cell0 = "<td class='text1' ><select class='form-control' name='detention' id='detention" + snumber1 + "'  style='width:200px;height:44px;'><option value='0'>-Select-</option><c:if test = "${vehicleTypeList!= null}" ><c:forEach  items="${vehicleTypeList}" var="vehType"><option value='<c:out value="${vehType.vehicleTypeId}" />'><c:out value="${vehType.vehicleTypeName}" /></option></c:forEach ></c:if></select></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(2);
                                                        cell0 = "<td class='text1' ><select   name='dcmunit' id='dcmunit" + snumber1 + "' class='form-control' style='width:200px;height:44px;'/><option value='0'>-Select-</option><c:if test = "${detaintionTimeSlot!= null}" ><c:forEach  items="${detaintionTimeSlot}" var="detain"><option value='<c:out value="${detain.id}" />'><c:out value="${detain.timeSlot}" /></option></c:forEach ></c:if></select></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;
                                                        
                                                        cell = newrow.insertCell(2);
                                                        cell0 = "<td class='text1' ><select   name='dcmToUnit' id='dcmToUnit" + snumber1 + "' class='form-control' style='width:200px;height:44px;'/><option value='0'>-Select-</option><c:if test = "${detaintionTimeSlot!= null}" ><c:forEach  items="${detaintionTimeSlot}" var="detain"><option value='<c:out value="${detain.id}" />'><c:out value="${detain.timeSlot}" /></option></c:forEach ></c:if></select></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(3);
                                                        cell0 = "<td class='text1' ><input type='text' name='chargeamt' id='chargeamt" + snumber1 + "' class='form-control' style='width:200px;height:44px;' onKeyPress='return onKeyPressBlockCharacters(event);' /></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        cell = newrow.insertCell(4);
                                                        cell0 = "<td class='text1'><input type='text' name='dcmremarks' id='dcmremarks" + snumber1 + "' class='form-control' style='width:200px;height:44px;'/></td>";
                                                        //cell.setAttribute(cssAttributeName,"text1");
                                                        cell.innerHTML = cell0;

                                                        rowCount1++;
                                                    }
                                                }

                                                    </SCRIPT>

                                                </div>
                                                <br>
                                                <center>
                                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                                </center>
                                                <br>
                                            </div>
            
                    
                </c:if>
                <c:if test="${billingTypeId == 4}">
                    <div id="fkmRate">
                        <div class="inpad">

                            <c:if test = "${contractFixedRateList != null}">

                                     <table class="table table-info mb30 table-hover" id="itemsTable" >
                                    <thead>
                                        <% int sno1 = 1;%>
                                        <tr height="110">
                                            <th>S.No</th>
                                    <th>Vehicle Type Name</th>
                                    <th>Vehicle Count</th>
                                    <th>Total Km</th>
                                    <th>Total Reefer Hm</th>
                                    <th>Agreed Fuel Price</th>
                                    <th>Extra Km Rate/Km with Reefer</th>
                                    <th>Extra Reefer Hm Rate/Hm with Reefer</th>
                                    <th>Rate</th>
                                    <th>Extra Km Rate/Km without Reefer</th>
                                    <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${contractFixedRateList}" var="ptpl">
                                            <tr>
                                                <td><%=sno1%></td>
                                                <td>
                                                    <input type="hidden" name="fixedVehTypeId" id="fixedVehTypeId" value="<c:out value="${ptpl.vehicleTypeId}" />" class="tInput"  style="width: 60px;"/>
                                                    <c:out value="${ptpl.vehicleTypeName}" />
                                                </td>
                                                <td><input type="hidden" name="contractVehicleNos" value="<c:out value="${ptpl.contractVehicleNos}" />" class="tInput" id="contractVehicleNos"  style="width: 90px;"/><c:out value="${ptpl.contractVehicleNos}" /></td>
                                                <td><input type="hidden" name="fixedTotalKm" value="<c:out value="${ptpl.totalKm}" />" class="tInput" id="fixedTotalKm"  style="width: 90px;"/><c:out value="${ptpl.totalKm}" /></td>
                                                <td><input type="hidden" name="fixedTotalHm" value="<c:out value="${ptpl.totalHm}" />" class="tInput" id="fixedTotalHm"  style="width: 90px;"/><c:out value="${ptpl.totalHm}" /></td>
                                                <td><input type="hidden" name="fixedRateWithReefer" value="<c:out value="${ptpl.rateWithReefer}" />" class="tInput" id="fixedRateWithReefer"  style="width: 60px;" /><c:out value="${ptpl.rateWithReefer}" /></td>
                                                <td><input type="hidden" name="extraKmRatePerKmRateWithReefer" value="<c:out value="${ptpl.extraKmRatePerKmRateWithReefer}" />" class="tInput" id="extraKmRatePerKmRateWithReefer"  style="width: 60px;" /><c:out value="${ptpl.extraKmRatePerKmRateWithReefer}" /></td>
                                                <td><input type="hidden" name="extraHmRatePerHmRateWithReefer" value="<c:out value="${ptpl.extraHmRatePerHmRateWithReefer}" />" class="tInput" id="extraHmRatePerHmRateWithReefer"  style="width: 60px;" /><c:out value="${ptpl.extraHmRatePerHmRateWithReefer}" /></td>
                                                <td><input type="hidden" name="fixedRateWithoutReefer" value="<c:out value="${ptpl.rateWithoutReefer}" />" class="tInput" id="fixedRateWithoutReefer" style="width: 60px;" /><c:out value="${ptpl.rateWithoutReefer}" /></td>
                                                <td><input type="hidden" name="extraKmRatePerKmRateWithoutReefer" value="<c:out value="${ptpl.extraKmRatePerKmRateWithoutReefer}" />" class="tInput" id="extraKmRatePerKmRateWithoutReefer" style="width: 60px;" /><c:out value="${ptpl.extraKmRatePerKmRateWithoutReefer}" /></td>
                                                <td><input type="hidden" name="status" value="<c:out value="${ptpl.activeInd}" />" class="tInput" id="status" style="width: 60px;" /><c:out value="${ptpl.activeInd}" /></td>
                                            </tr>
                                            <%sno1++;%>
                                        </c:forEach >
                                    </tbody>
                                </table>
                            </c:if>

                            <script language="javascript" type="text/javascript">
                                setFilterGrid("itemsTable");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" selected="selected">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span>Entries Per Page</span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text">Displaying Page 1 of 1</div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("itemsTable", 1);
                            </script>
                        </div>
                    </div>
                    <div id="fkmRoute">
                        <div class="inpad">

                            <c:if test = "${fixedKmBillingList != null}">
                                <table id="itemsTable" class="sortable">
                                    <thead>
                                        <% int sno1 = 1;%>
                                        <tr height="110">
                                            <th>S.No</th>
                                    <!--<th>Vehicle Route Contract Code</th>-->
                                    <th>Vehicle Type</th>
                                    <th>First Pick UP</th>
                                    <th>Interim Point 1</th>
                                    <th>Interim Point 2</th>
                                    <th>Interim Point 3</th>
                                    <th>Interim Point 4</th>
                                    <th>Final Drop Point</th>
                                    <th>Travel Km</th>
                                    <th>Travel Time</th>
                                    <th>Agreed Fuel Price</th>
                                    <th>Rate</th>
                                    <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${fixedKmBillingList}" var="ptpl">
                                            <tr>
                                                <td><%=sno1%><input type="hidden" name="ptpRouteContractCode" id="ptpRouteContractCode" value="<c:out value="${ptpl.routeCode}" />" class="tInput"  style="width: 60px;"/></td>
                                                <td>
                                                    <input type="hidden" name="ptpVehTypeId" id="ptpVehTypeId" value="<c:out value="${ptpl.vehicleTypeId}" />" class="tInput"  style="width: 60px;"/>
                                                    <input type="hidden" name="ptpRouteContractId" id="ptpRouteContractId" value="<c:out value="${ptpl.routeContractId}" />" class="tInput"  style="width: 60px;"/>
                                                    <c:out value="${ptpl.vehicleTypeName}" />
                                                </td>
                                                <td><input type="hidden" name="ptpFirstPickupId" value="<c:out value="${ptpl.firstPickupId}" />" class="tInput" id="ptpFirstPickupId"  style="width: 90px;"/><c:out value="${ptpl.firstPickupName}" /></td>
                                                <td><input type="hidden" name="ptpPoint1Id" value="<c:out value="${ptpl.point1Id}" />" class="tInput" id="ptpPoint1Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Km" value="<c:out value="${ptpl.point1Km}" />" class="tInput" id="ptpPoint1Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Hrs" value="<c:out value="${ptpl.point1Hrs}" />" class="tInput" id="ptpPoint1Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint1Minutes" value="<c:out value="${ptpl.point1Minutes}" />" class="tInput" id="ptpPoint1Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint1RouteId" value="<c:out value="${ptpl.point1RouteId}" />" class="tInput" id="ptpPoint1RouteId"  style="width: 90px;"/><c:out value="${ptpl.point1Name}" /></td>
                                                <td><input type="hidden" name="ptpPoint2Id" value="<c:out value="${ptpl.point2Id}" />" class="tInput" id="ptpPoint2Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Km" value="<c:out value="${ptpl.point2Km}" />" class="tInput" id="ptpPoint2Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Hrs" value="<c:out value="${ptpl.point2Hrs}" />" class="tInput" id="ptpPoint2Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint2Minutes" value="<c:out value="${ptpl.point2Minutes}" />" class="tInput" id="ptpPoint2Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint2RouteId" value="<c:out value="${ptpl.point2RouteId}" />" class="tInput" id="ptpPoint2RouteId"  style="width: 90px;"/><c:out value="${ptpl.point2Name}" /></td>
                                                <td><input type="hidden" name="ptpPoint3Id" value="<c:out value="${ptpl.point3Id}" />" class="tInput" id="ptpPoint3Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Km" value="<c:out value="${ptpl.point3Km}" />" class="tInput" id="ptpPoint3Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Hrs" value="<c:out value="${ptpl.point3Hrs}" />" class="tInput" id="ptpPoint3Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint3Minutes" value="<c:out value="${ptpl.point3Minutes}" />" class="tInput" id="ptpPoint3Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint3RouteId" value="<c:out value="${ptpl.point3RouteId}" />" class="tInput" id="ptpPoint3RouteId"  style="width: 90px;"/><c:out value="${ptpl.point3Name}" /></td>
                                                <td><input type="hidden" name="ptpPoint4Id" value="<c:out value="${ptpl.point4Id}" />" class="tInput" id="ptpPoint4Id"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Km" value="<c:out value="${ptpl.point4Km}" />" class="tInput" id="ptpPoint4Km"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Hrs" value="<c:out value="${ptpl.point4Hrs}" />" class="tInput" id="ptpPoint4Hrs"  style="width: 90px;"/><input type="hidden" name="ptpPoint4Minutes" value="<c:out value="${ptpl.point4Minutes}" />" class="tInput" id="ptpPoint4Minutes"  style="width: 90px;"/><input type="hidden" name="ptpPoint4RouteId" value="<c:out value="${ptpl.point4RouteId}" />" class="tInput" id="ptpPoint4RouteId"  style="width: 90px;"/><c:out value="${ptpl.point4Name}" /></td>
                                                <td><input type="hidden" name="ptpFinalPointId" value="<c:out value="${ptpl.finalPointId}" />" class="tInput" id="ptpFinalPointId"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointKm" value="<c:out value="${ptpl.finalPointKm}" />" class="tInput" id="ptpFinalPointKm"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointHrs" value="<c:out value="${ptpl.finalPointHrs}" />" class="tInput" id="ptpFinalPointHrs"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointMinutes" value="<c:out value="${ptpl.finalPointMinutes}" />" class="tInput" id="ptpFinalPointMinutes"  style="width: 90px;"/><input type="hidden" name="ptpFinalPointRouteId" value="<c:out value="${ptpl.finalPointRouteId}" />" class="tInput" id="ptpFinalPointRouteId"  style="width: 90px;"/><c:out value="${ptpl.finalPointName}" /></td>

                                                <td><input type="hidden" name="ptpTotalKm" value="<c:out value="${ptpl.totalKm}" />" class="tInput" id="ptpTotalKm"  style="width: 90px;"/><c:out value="${ptpl.totalKm}" /></td>
                                                <td><input type="hidden" name="ptpTravelTime" value="<c:out value="${ptpl.totalHours}" />:<c:out value="${ptpl.totalMinutes}" />:00" class="tInput" id="<c:out value="${ptpl.totalHours}" />"  style="width: 90px;"/><c:out value="${ptpl.totalHours}" />:<c:out value="${ptpl.totalMinutes}" />:00</td>
                                                <td><input type="hidden" name="ptpRateWithReefer" value="<c:out value="${ptpl.rateWithReeferPerKm}" />" class="tInput" id="ptpRateWithReefer"  style="width: 60px;" /><c:out value="${ptpl.rateWithReefer}" /></td>
                                                <td><input type="hidden" name="ptpRateWithoutReefer" value="<c:out value="${ptpl.rateWithoutReeferPerKm}" />" class="tInput" id="ptpRateWithoutReefer" style="width: 60px;" /><c:out value="${ptpl.rateWithoutReefer}" /></td>
                                                <td><input type="hidden" name="status" value="<c:out value="${ptpl.activeInd}" />" class="tInput" id="status" style="width: 60px;" /><c:out value="${ptpl.activeInd}" /></td>
                                            </tr>
                                            <%sno1++;%>
                                        </c:forEach >
                                    </tbody>
                                </table>
                                    <table align="center">
                                            <tr align="center"><td> <a href="/throttle/editCustomerContract.do?custId=<c:out value="${customerId}"/>"><input type="button" class="btn btn-success" value="Edit"  style="width: 120px"/></a> </tr></td>
                                            </table>
                            </c:if>

                            <script language="javascript" type="text/javascript">
                                setFilterGrid("itemsTable");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" selected="selected">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span>Entries Per Page</span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text">Displaying Page 1 of 1</div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("itemsTable", 1);
                            </script>
                        </div>
                    </div>

                </c:if>

                <c:if test="${billingTypeId == 2 }">
                    <div id="ptpw">

                        <div class="inpad">
                            <c:if test = "${ptpwBillingList != null}">

                                     <table class="table table-info mb30 table-hover" id="itemsTable1" >
                                    <thead>
                                        <% int sno1 = 1;%>
                                        <tr height="60">
                                            <th>S.No</th>
                                    <!--<th>Vehicle Route Contract Code</th>-->
                                    <th>Vehicle Type</th>
                                    <th>First Pick UP</th>
                                    <th>Final Drop Point</th>
                                    <th>Minimum Base Weight</th>
                                    <th>Total Km</th>
                                    <th>Agreed Fuel Price / KG</th>
                                    <th>Rate / KG</th>
                                    <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${ptpwBillingList}" var="ptpwl">
                                            <tr>
                                                <td><%=sno1%>
                                                <input type="hidden" name="ptpwRouteContractCode" id="ptpwRouteContractCode" value="<c:out value="${ptpwl.routeCode}" />" class="tInput" style="width:120px"/></td>
                                                <td><input type="hidden" name="vehicleTypeId" id="vehicleTypeId" value="<c:out value="${ptpwl.vehicleTypeId}" />" class="tInput"  style="width: 60px;"/><c:out value="${ptpwl.vehicleTypeName}" /></td>
                                                <td><input type="hidden" name="ptpwFirstPickupId" id="ptpwFirstPickupId" value="<c:out value="${ptpwl.firstPickupId}" />" class="tInput"  style="width: 120px;"/><c:out value="${ptpwl.firstPickupName}" /></td>
                                                <td><input type="hidden" name="ptpwFinalPointId" id="ptpwFinalPointId" value="<c:out value="${ptpwl.finalPointId}" />" class="tInput"  style="width: 120px;"/> <c:out value="${ptpwl.finalPointName}" /></td>
                                                <td><input type="hidden" name="minBasWgt" id="minBasWgt" value="<c:out value="${ptpwl.minimumBaseWgt}" />" class="tInput"  style="width: 120px;"/><c:out value="${ptpwl.minimumBaseWgt}" /></td>
                                                <td><input type="hidden" name="ptpwTotalKm" id="ptpwTotalKm" value="<c:out value="${ptpwl.totalKm}" />" class="tInput"  style="width: 120px;"/><input type="hidden" name="ptpwPointId" id="ptpwPointId" value="<c:out value="${ptpwl.finalPointId}" />" class="tInput"  style="width: 120px;"/><c:out value="${ptpwl.finalPointKm}" /></td>
                                                <td><input type="hidden" name="ptpwRateWithReefer"  id="ptpwRateWithReefer" value="<c:out value="${ptpwl.rateWithReeferPerKg}" />" class="tInput" style="width: 120px;"/><c:out value="${ptpwl.rateWithReeferPerKg}" /></td>
                                                <td><input type="hidden" name="ptpwRateWithoutReefer" id="ptpwRateWithoutReefer"  value="<c:out value="${ptpwl.rateWithoutReeferPerKg}" />" class="tInput" style="width: 120px;"/><c:out value="${ptpwl.rateWithoutReeferPerKg}" /></td>
                                                <td><input type="hidden" name="status" id="status"  value="<c:out value="${ptpwl.status}" />" class="tInput" style="width: 120px;"/><c:out value="${ptpwl.activeInd}" /></td>
                                            </tr>
                                            <%sno1++;%>
                                        </c:forEach>
                                    </tbody>
                                </table >
                                        <table align="center">
                                            <tr align="center"><td> <a href="/throttle/editCustomerContract.do?custId=<c:out value="${customerId}"/>"><input type="button" class="btn btn-success" value="Edit"  style="width: 120px"/></a> </tr></td>
                                            </table>
                                            <!--                                <center>
                                    <a  class="nexttab" href="#" ><input type="button" class="btn btn-success" value="Next" name="Save" style="width: 120px"/></a>
                                </center>-->
                            </c:if>

                            <script language="javascript" type="text/javascript">
                                setFilterGrid("itemsTable1");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" selected="selected">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span>Entries Per Page</span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text">Displaying Page 1 of 1</div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("itemsTable1", 1);
                            </script>

                        </div>
<!--                        <center>
                            <input type="button" class="btn btn-success" value="Save" style="width: 120px" onclick="submitPage(this.value)"/>
                        </center>-->
                    </div>
                </c:if>



                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>
                <c:if test="${billingTypeId == 3}">
                    <div id="mfr">
                        <c:if test="${actualKmBillingList != null}">
                            <% int sno1 = 1;%>
                            <table width="815" align="center" border="0" id="table2" class="sortable">
                                <thead>
                                    <tr height="30">
                                        <th>S.No</th>
                                <th>Vehicle Type (Mfr/Model)</th>
                                <th>Vehicle Rs/Km</th>
                                <th>Reefer Rs/Hrs</th>
                                <th>Select</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${actualKmBillingList}" var="mfrList">
                                        <tr height="30">
                                            <td align="left"><%=sno1%></td>
                                            <td align="left">
                                                <input type="hidden" name="actualVehicleTypeId" value="<c:out value="${mfrList.vehicleTypeId}"/>" /><c:out value="${mfrList.vehicleTypeName}"/></td>
                                            <td align="left"><input type="hidden" name="vehicleRatePerKm" class="form-control" style="width:250px;height:40px" value="<c:out value="${mfrList.vehicleRatePerKm}"/>"><c:out value="${mfrList.vehicleRatePerKm}"/></td>
                                            <td align="left"><input type="hidden" name="reeferRatePerHour" class="form-control" style="width:250px;height:40px" value="<c:out value="${mfrList.reeferRatePerHour}"/>"><c:out value="${mfrList.reeferRatePerHour}"/></td>
                                            <td align="left"><input type="hidden" name="status" value="<c:out value="${mfrList.activeInd}"/>"/><c:out value="${mfrList.activeInd}"/></td>
                                        </tr>
                                        <%sno1++;%>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </c:if>

<!--                        <center>
                            <input type="button" class="btn btn-success" value="Save" style="width: 120px" onclick="submitPage(this.value)"/>
                        </center>-->

                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table2");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page 1 of 1</div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table2", 1);
                        </script>
                    </div>
                </c:if>

            </div>

        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>
