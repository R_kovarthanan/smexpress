<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="OrderUploadDetails"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.CityMaster" text="OrderUploadDetails"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <form name="consignment"  method="POST">
                    <%--<%@ include file="/content/common/message.jsp" %>--%>

                    <c:if test = "${contractExcelUploadSize > 0 }">
                        <table align="center">
                            <tr align="center">
                                <td align="center">
                                    <center> <input type="button" class="btn btn-success" name="Save" value="Create Contract" id="createOrder" onclick="submitPage(this.value);" ></center>
                                </td>
                            </tr>
                        </table>
                    </c:if>
                    <br>
                    <% int sno = 0;%>
                    <c:if test = "${contractExcelUploadSize > 0}">
                        <table class="table table-info mb30 table-hover" id="bg" >	
                            <thead>
                                <tr height="30">
                                    <th> S.No </th>
                                    <th> Customer Name  </th>
                                    <th> Origin </th>
                                    <th> Touch Point </th>
                                    <th> Destination </th>
                                    <th> Revenue </th>
                                    <th> Vehicle Type </th>
                                    <th> KM </th>
                                    <th> TAT </th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:if test = "${contractExcelUpload!=null}">
                                    <c:forEach items="${contractExcelUpload}" var="cng">
                                        <%
                                            sno++;
                                            String className = "text1";
                                            if ((sno % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                        %>
                                        <tr>
                                            <td align="left"> <%= sno + 0%> <input type="hidden" name="uploadId" id="uploadId" value="<c:out value="${cng.id}" />" class="form-control" style="width:250px;height:40px"  /></td>
                                            <td align="left"> <c:out value="${cng.customerName}" /></td>
                                            <td align="left"> <c:out value="${cng.origin}" /></td>
                                            <td align="left"> <c:out value="${cng.touchPoint1}" /></td>
                                            <td align="left"> <c:out value="${cng.destination}" /></td>
                                            <td align="left"> <c:out value="${cng.contractAmount}" /></td>
                                            <td align="left"> <c:out value="${cng.vehicleType}" /></td>                                            
                                            <td align="left"> <c:out value="${cng.totalKM}" /></td>
                                            <td align="left"> <c:out value="${cng.tat}" /></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                                <input type="hidden" name="count" id="count" value="<%=sno%>" />
                            </c:if>
                        </c:if>
                    </table>



                    <c:if test = "${contractUploadIncorectSize >0}">
                        <table class="table table-info mb30 table-hover" id="bg" >	
                            <thead>
                                <tr height="30" align="center">
                                    <th colspan="11" align="center"> <center><font>InValid Records</font></center></th>
                                </tr>

                                <tr height="30">
                                    <th> S.No </th>
                                    <th> Customer Name  </th>
                                    <th> Origin </th>
                                    <th> Touch Point </th>
                                    <th> Destination </th>
                                    <th> Revenue </th>
                                    <th> Vehicle Type </th>
                                    <th> KM </th>
                                    <th> TAT </th>
                                    <th>Issue</th>
                                </tr>
                            </thead>

                            <tbody>

                                <% int sno1 = 0;%>
                                <c:if test = "${contractUploadIncorect!=null}">
                                    <c:forEach items="${contractUploadIncorect}" var="cngInc">
                                        <%
                                                    sno1++;
                                                    String className = "text1";
                                                    if ((sno1 % 1) == 0) {
                                                        className = "text1";
                                                    } else {
                                                        className = "text2";
                                                    }
                                        %>
                                        <tr>
                                            <td align="left"> <%= sno1 + 0%> </td>
                                            <td align="left"> <c:out value="${cngInc.customerName}" /></td>
                                            <td align="left"> <c:out value="${cngInc.origin}" /></td>
                                            <td align="left"> <c:out value="${cngInc.touchPoint1}" /></td>
                                            <td align="left"> <c:out value="${cngInc.destination}" /></td>
                                            <td align="left"> <c:out value="${cngInc.contractAmount}" /></td>
                                            <td align="left"> <c:out value="${cngInc.vehicleType}" /></td>                                            
                                            <td align="left"> <c:out value="${cngInc.totalKM}" /></td>
                                            <td align="left"> <c:out value="${cngInc.tat}" /></td>
                                            <c:if test = "${cngInc.flag == 0}">
                                                <td>Valid</td>
                                            </c:if>
                                            <c:if test = "${cngInc.flag == 2}">
                                                <td align="left"><font color="red">Invalid Vehicle Type</font></td>
                                            </c:if>
                                            <c:if test = "${cngInc.flag == 3}">
                                                    <td><font color="red">InValid Route</font></td>
                                            </c:if>
                                            <c:if test = "${cngInc.flag == 1}">
                                                        <td align="left"><font color="red">Contract Already Exist</font></td>                                                
                                            </c:if>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                                <input type="hidden" name="count" id="count" value="<%=sno1%>" />
                            </c:if>
                        </c:if>
                    </table>
                    <script>
                        function submitPage() {
                            if (confirm("Are Sure Want to Upload")) {
                                document.consignment.action = "/throttle/insertContractExcelUpload.do";
                                document.consignment.submit();
                            }
                        }
                    </script>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>