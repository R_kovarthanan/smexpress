<%--
    Document   : standardChargesMaster
    Created on : Oct 29, 2013, 11:32:08 AM
    Author     : srinivasan
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
  
  $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });
  
  
    //Update Vehicle Planning
    function setValues(activeInd,statusId,deviceId,branchId,sno,effectiveDate) {
        var count = parseInt(document.getElementById("count").value);
        document.getElementById("activeInd").value = activeInd;
        document.getElementById("deviceId").value = deviceId;
        document.getElementById("fromDate").value = effectiveDate;
        document.getElementById("branchId").value = branchId;
        document.getElementById("statusId").value = statusId;

    }

    function submitPage() {
            document.device.action = '/throttle/saveMappingGpsDevice.do';
            document.device.submit();
        }
    function getDeviceList() {
            document.device.action = '/throttle/viewMappingGpsDevice.do';
            document.device.submit();
        }
    
    
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body >
        <form name="device"  method="post" >
            <br>
            <h2>Master >> Mapping GPS Device</h2>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>
            <br>

            <table width="980" align="center" class="table2" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="contenthead" colspan="4" align="center">Mapping GPS Device</td>
                </tr>
                 <tr class="text1">
                     <td >&nbsp;&nbsp;Search Date</td>
                    <td >
                        <input name="effectiveDate" id="effectiveDate" type="text" class="datepicker"  value="<c:out value="${effectiveDate}"/>">
                        <input type="button" class="button" value="Search" id="Search" onClick="getDeviceList();"   />
                    </td>
                    <td  colspan="2">&nbsp;</td>
                </tr>
                <tr>
                        <input type="hidden" id="statusId" name="statusId" value=""/>
                    <td class="text2">&nbsp;&nbsp;<font color="red">*</font>Device Name</td>
                    <td class="text2">
                        <select name="deviceId" id="deviceId" class="textbox" style="height:20px; width:122px;"" >
                     <c:if test="${deviceList != null}">
                            <option value="" selected>--Select--</option>
                            <c:forEach items="${deviceList}" var="device">
                                <option value='<c:out value="${device.deviceId}"/>'><c:out value="${device.deviceName}"/></option>
                            </c:forEach>
                        </c:if>
                        </select>
                    </td>
                     <td class="text2">&nbsp;&nbsp;<font color="red">*</font>Branch Id</td>
                    <td class="text2">
                        <select name="branchId" id="branchId" class="textbox" style="height:20px; width:122px;"" >
                     <c:if test="${branchList != null}">
                            <option value="" selected>--Select--</option>
                            <c:forEach items="${branchList}" var="branchList">
                                <option value='<c:out value="${branchList.branchId}"/>'><c:out value="${branchList.branchName}"/></option>
                            </c:forEach>
                        </c:if>
                        </select>
                    </td>
                </tr>
                <tr class="text1">
                    <td> Mapping Date </td>
                    <td><input name="fromDate" id="fromDate" type="text" class="datepicker"  value=""></td>
                    <td class="text1">&nbsp;&nbsp;Active Status</td>
                    <td class="text1">
                        <select  align="center" class="textbox" name="activeInd" id="activeInd" >
                            <option value='N' >Mapped</option>
                            <option value='Y'>Not Mapped</option>
                        </select>
                    </td>
                </tr>
                    
     
                <tr>
                    <td class="text1" colspan="4" align="center"><input type="button" class="button" value="Save" id="save" onClick="submitPage();"   /></td>
                </tr>
                
               
            </table>
            <br>
            <br/>
            <table width="815" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>Mapping Date</h3></th>
                        <th><h3>Device Name </h3></th>
                        <th><h3>Branch Name </h3></th>
                        <th><h3>Status </h3></th>
                       <th><h3>Select</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <% int sno = 0;%>
                    <c:if test = "${deviceListDetails != null}">
                        <c:forEach items="${deviceListDetails}" var="deviceList">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno %> </td>
                                <td class="<%=className%>"  align="left">
                                   <c:out value="${deviceList.effectiveDate}"/>
                                    </td>
                                <td class="<%=className%>"  align="left">
                                   <c:out value="${deviceList.deviceName}"/>
                                    </td>
                                <td class="<%=className%>"  align="left">
                                   <c:out value="${deviceList.branchName}"/>
                                    </td>
                                <td class="<%=className%>"  align="left">
                                    <c:if test = "${deviceList.activeInd == 'Y'}">
                                    Not Mapped
                                    </c:if>
                                    <c:if test = "${deviceList.activeInd == 'N'}">
                                    Mapped
                                    </c:if>
                                    </td>
                                <td class="<%=className%>"> 
                                    <input type="checkbox" id="edit<%=sno%>" onclick="setValues('<c:out value="${deviceList.activeInd}" />','<c:out value="${deviceList.statusId}" />','<c:out value="${deviceList.deviceId}" />', '<c:out value="${deviceList.branchId}" />','<%=sno%>','<c:out value="${deviceList.effectiveDate}" />');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>
            <input type="hidden" name="count" id="count" value="<%=sno%>" />
            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");</script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        </form>
    </body>
</html>