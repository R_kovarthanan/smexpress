<%@page import="java.text.SimpleDateFormat" %>
<%@ include file="../common/NewDesign/header.jsp" %>
<%@ include file="../common/NewDesign/sidemenu.jsp" %> 
<head>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ page import="java.util.* "%>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>
    <%@ page import="java.text.DecimalFormat" %>
    <%@ page import="java.text.NumberFormat" %>
    <script type="text/javascript" src="/throttle/js/validate.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/layout-styles.css";
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->
    <!--                <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>


    <script type="text/javascript">
        function checkSpcialChar(event) {
            if (!((event.keyCode >= 65) && (event.keyCode <= 90) || (event.keyCode >= 97) && (event.keyCode <= 122) || (event.keyCode >= 48) && (event.keyCode <= 57))) {
                event.returnValue = false;
                return;
            }
            event.returnValue = true;
        }
    </script>

    <script>
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#vehicleNo').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getLhcVehicleRegNo.do",
                        dataType: "json",
                        data: {
                            vehicleNo: request.term,
                            tripId: $("#tripSheetId").val(),
                            vehicleTypeId: $("#vehicleTypeId").val()
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var id = ui.item.Id;
                    $('#vehicleNo').val(value);
                    $('#vehicleId').val(id);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });

//        $(document).ready(function() {
//            // Use the .autocomplete() method to compile the list based on input from user
//            $('#lrNumber').autocomplete({
//                source: function(request, response) {
//                    $.ajax({
//                        url: "/throttle/checkDocketNoIsExist.do",
//                        dataType: "json",
//                        data: {
//                            lrNumber: request.term
//                        },
//                        success: function(data, textStatus, jqXHR) {
//                            var items = data;
//                            //alert(data);
//                            response(items);
//                        },
//                        error: function(data, type) {
//                            console.log(type);
//                        }
//                    });
//                },
//                minLength: 1,
//                select: function(event, ui) {
//                    var value = ui.item.Name;
//                    var id = ui.item.Id;
//                    alert("Docket No is Already Exist.");
//                    $('#lrNumber').val(value);
//                    $('#save').hide();
//                    return false;
//                }
//            })
//        });

        var httpRequest;
        function checkLrNumber() {
            var lrNumber = document.getElementById('lrNumber').value;
            var url = '/throttle/checkDocketNoIsExist.do?lrNumber=' + lrNumber;
            if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);
        }


        function processRequest() {
            if (httpRequest.readyState == 4) {
                if (httpRequest.status == 200) {
                    var val = httpRequest.responseText.valueOf();
                    if (val != "" && val != 'null') {
                        $("#nameStatus").show();
                        $("#lrNameStatus").text('DocketNo  :' + val + ' is Already Exists. Please Check.');
                        $("#update").hide();
                    } else {
                        $("#nameStatus").hide();
                        $("#lrNameStatus").text('');
                        $("#update").show();
                    }
                } else {
                    alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                }
            }
        }

    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                yearRange: '1900:' + new Date().getFullYear(),
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("#tabs").tabs();
        });
    </script>



    <script type="text/javascript">

        function calculateStartDateDifference() {
            var endDates1 = document.getElementById("tripScheduleDate").value;
            var tempDate11 = endDates1.split("-");

            var stDates1 = document.getElementById("startDate").value;
            var tempDate21 = stDates1.split("-");

            var vrDates1 = document.getElementById("vehicleactreportdate").value;
            var tempDate31 = vrDates1.split("-");

            var prevTime1 = new Date(tempDate21[2], tempDate21[1], tempDate21[0]);  // Feb 1, 2011
            var thisTime1 = new Date(tempDate11[2], tempDate11[1], tempDate11[0]);              // now
            var difference2 = thisTime1.getTime() - prevTime1.getTime();   // now - Feb 1

            var vrTime1 = new Date(tempDate31[2], tempDate31[1], tempDate31[0])
            var difference3 = (prevTime1.getTime() - vrTime1.getTime()) / (1000 * 60 * 60 * 24);
//            alert("difference3="+difference3);
            if (difference3 > 0) {
                document.getElementById("originDetention").value = (difference3 - 1) * 1500;
            } else {
                document.getElementById("originDetention").value = 0;
            }
            if (prevTime1.getTime() < thisTime1.getTime()) {
                alert(" Selected Date is less than Scheduled Date");
                document.getElementById("startDate").value = document.getElementById("todayDate").value;
            }
        }

        function callLoadedGrossWeight() {
            document.getElementById("origPendingPkgs2").value = document.getElementById("totalPackages").value;
        }


        function calculateDate() {
            var endDates = document.getElementById("vehicleactreportdate").value;
            var tempDate1 = endDates.split("-");
            var stDates = document.getElementById("vehicleloadreportdate").value;
            var tempDate2 = stDates.split("-");
            var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0]);  // Feb 1, 2011
            var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0]);              // now
            var difference1 = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
            var endDates1 = document.getElementById("vehicleloadreportdate").value;
            var tempDate11 = endDates1.split("-");
            var stDates1 = document.getElementById("startDate").value;
            var tempDate21 = stDates1.split("-");
            var prevTime1 = new Date(tempDate21[2], tempDate21[1], tempDate21[0]);  // Feb 1, 2011
            var thisTime1 = new Date(tempDate11[2], tempDate11[1], tempDate11[0]);              // now
            var difference2 = thisTime1.getTime() - prevTime1.getTime();   // now - Feb 1
            if (prevTime.getTime() < thisTime.getTime()) {
                alert(" Selected Date is greater than Reporting Time");
                document.getElementById("vehicleloadreportdate").value = document.getElementById("todayDate").value;
            }
            if (prevTime1.getTime() < thisTime1.getTime()) {
                alert(" Selected Date is less than Reporting Time");
                document.getElementById("vehicleloadreportdate").value = document.getElementById("todayDate").value;
            }
        }
        ///raju
        function calculateDate1() {
            var endDates = document.getElementById("DocumentRecivedDate").value;
            var tempDate1 = endDates.split("-");
            var stDates = document.getElementById("DocumentRecivedDate").value;
            var tempDate2 = stDates.split("-");
            var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0]);  // Feb 1, 2011
            var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0]);              // now
            var difference1 = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
            var endDates1 = document.getElementById("DocumentRecivedDate").value;
            var tempDate11 = endDates1.split("-");
            var stDates1 = document.getElementById("startDate").value;
            var tempDate21 = stDates1.split("-");
            var prevTime1 = new Date(tempDate21[2], tempDate21[1], tempDate21[0]);  // Feb 1, 2011
            var thisTime1 = new Date(tempDate11[2], tempDate11[1], tempDate11[0]);              // now
            var difference2 = thisTime1.getTime() - prevTime1.getTime();   // now - Feb 1
            if (prevTime.getTime() < thisTime.getTime()) {
                alert(" Selected Date is greater than Reporting Time");
                document.getElementById("vehicleloadreportdate").value = document.getElementById("todayDate").value;
            }
            if (prevTime1.getTime() < thisTime1.getTime()) {
                alert(" Selected Date is less than Reporting Time");
                document.getElementById("vehicleloadreportdate").value = document.getElementById("todayDate").value;
            }
        }
        function calculateTime() {
            var endDates = document.getElementById("vehicleactreportdate").value;
            var hour1 = document.getElementById("vehicleactreporthour").value;
            var minute1 = document.getElementById("vehicleactreportmin").value;
            var endTimeIds = hour1 + ":" + minute1 + ":" + "00";
            var tempDate1 = endDates.split("-");
            var tempTime1 = endTimeIds.split(":");
            var stDates = document.getElementById("vehicleloadreportdate").value;
            var hour = document.getElementById("vehicleloadreporthour").value;
            var minute = document.getElementById("vehicleloadreportmin").value;
            var stTimeIds = hour + ":" + minute + ":" + "00";
            var tempDate2 = stDates.split("-");
            var tempTime2 = stTimeIds.split(":");
            var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0], tempTime2[0], tempTime2[1]);  // Feb 1, 2011
            var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0], tempTime1[0], tempTime1[1]);              // now
            var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
            var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
            var endDates1 = document.getElementById("startDate").value;
            var hour11 = document.getElementById("tripStartHour").value;
            var minute11 = document.getElementById("tripStartMinute").value;
            var endTimeIds1 = hour11 + ":" + minute11 + ":" + "00";
            var tempDate11 = endDates1.split("-");
            var tempTime11 = endTimeIds1.split(":");
            var stDates1 = document.getElementById("vehicleloadreportdate").value;
            var hour2 = document.getElementById("vehicleloadreporthour").value;
            var minute2 = document.getElementById("vehicleloadreportmin").value;
            var stTimeIds1 = hour2 + ":" + minute2 + ":" + "00";
            var tempDate21 = stDates1.split("-");
            var tempTime21 = stTimeIds1.split(":");
            var prevTime2 = new Date(tempDate21[2], tempDate21[1], tempDate21[0], tempTime21[0], tempTime21[1]);  // Feb 1, 2011
            var thisTime2 = new Date(tempDate11[2], tempDate11[1], tempDate11[0], tempTime11[0], tempTime11[1]);              // now
            var difference2 = thisTime2.getTime() - prevTime2.getTime();   // now - Feb 1
            var hoursDifference2 = Math.floor(difference2 / 1000 / 60 / 60);
            if (prevTime.getTime() < thisTime.getTime()) {
                alert("Selected Time is greater than Reporting Time ");
                document.getElementById("vehicleloadreporthour").focus();
            }
            if (prevTime2.getTime() > thisTime2.getTime()) {
                alert("Selected Time is   Less than Start Time");
                document.getElementById("vehicleloadreporthour").focus();
            }

        }


        function validateLoadedPackages(sno) {

            var totalPackages = document.getElementById("totalPackages" + sno).value;
            var loadedPkgs = document.getElementById("loadedPkgs" + sno).value;
            var origPendingPkgs = parseFloat(document.getElementById("origPendingPkgs" + sno).value);

            if (loadedPkgs.trim() == '') {
                loadedPkgs = 0;
            }

            document.getElementById("pendingPkgs" + sno).value = parseFloat(totalPackages) - parseFloat(loadedPkgs);
            var pend = document.getElementById("pendingPkgs" + sno).value;

            if (parseFloat(pend) < 0) {
                alert("loaded packages cannot be less than zero. please check.")
                document.getElementById("loadedPkgs" + sno).value = origPendingPkgs;
                document.getElementById("loadedPkgs" + sno).focus();
            } else {
            }


        }


        function validateLoadedWeight(sno) {
            var loadedWeight = document.getElementById("loadedWeight" + sno).value;
            var pendingWeight = parseFloat(document.getElementById("pendingWeight" + sno).value);
            var origPendingWeight = parseFloat(document.getElementById("origPendingWeight" + sno).value);
            if (loadedWeight.trim() == '') {
                loadedWeight = 0;
            }
            if (parseFloat(pendingWeight) < 0) {
                alert("loaded weight cannot be less than zero. please check.")
                document.getElementById("loadedWeight" + sno).value = origPendingWeight;
                document.getElementById("loadedWeight" + sno).focus();
            } else {
                document.getElementById("pendingWeight" + sno).value = parseFloat(origPendingWeight) - parseFloat(loadedWeight);
            }
        }

    </script>

    <script type="text/javascript" language="javascript">

        function updatePage() {
             
            if (isEmpty(document.getElementById("lrNumber").value)) {
                alert('please enter Docketno');
                document.getElementById("lrNumber").focus();
                return;
            }
             else if (isEmpty(document.getElementById("sealNo").value)) {
                alert('please check the seal/tag number');
                document.getElementById("sealNo").focus();
                return;
            }           
             else if (isEmpty(document.getElementById("vehicleNo").value)) {
                alert('please check the vehicle No');
                document.getElementById("vehicleNo").focus();
                return;
            }
            
           else if (isEmpty(document.getElementById("driverMobile").value) || document.getElementById("driverMobile").value=="0") {
                alert('please enter valid driver Mobile number');
                document.getElementById("driverMobile").focus();
                return;
            }
           else if (isEmpty(document.getElementById("driverName").value) || document.getElementById("driverName").value=="0") {
                alert('please enter valid driver Name');
                document.getElementById("driverName").focus();
                return;
            }
           else if (isEmpty(document.getElementById("escortName").value) || document.getElementById("escortName").value=="0") {
                alert('please enter valid Escort Name');
                document.getElementById("escortName").focus();
                return;
            }
           else if (isEmpty(document.getElementById("escortMobile").value) || document.getElementById("escortMobile").value=="0") {
                alert('please enter valid Escort Mobile number');
                document.getElementById("escortMobile").focus();
                return;
            }
            else if (isEmpty(document.getElementById("invoiceValue").value)) {
                alert('please enter invoiceValue');
                document.getElementById("invoiceValue").focus();
                return;
            }
            else if (isEmpty(document.getElementById("invoiceDate").value)) {
                alert('please enter invoiceDate');
                document.getElementById("invoiceDate").focus();
                return;
            }
            else if (isEmpty(document.getElementById("packageWeight").value)) {
                alert('please enter packageWeight');
                document.getElementById("packageWeight").focus();
                return;
            }
           
            else if (isEmpty(document.getElementById("eWayBillNo").value)) {
                alert('please enter e-waybill number');
                document.getElementById("eWayBillNo").focus();
                return;
            }
            else {
                $("#update").hide();
                document.trip.action = '/throttle/updateStartTripSheet.do';
                document.trip.submit();
            }
        }

//        function submitPage() {
//            var errorMsg1 = "";
//            // Invoice Validation...
//            if (isEmpty(document.getElementById("sealNoType").value) || document.getElementById("sealNoType").value == 0) {
//                alert('please select seal/tag number type');
//                document.getElementById("sealNoType").focus();
//                return;
//            }
//            if (isEmpty(document.getElementById("sealNo").value)) {
//                alert('please check the seal/tag number');
//                document.getElementById("sealNo").focus();
//                return;
//            }
//            if (isEmpty(document.getElementById("eWayBillNo").value)) {
//                alert('please enter e-waybill number');
//                document.getElementById("eWayBillNo").focus();
//                return;
//            }
//            if (isEmpty(document.getElementById("invoiceNumber").value)) {
//                alert('please enter invoice number');
//                document.getElementById("invoiceNumber").focus();
//                return;
//            }
//            if (isEmpty(document.getElementById("invoiceValue").value)) {
//                alert('please enter invoice value');
//                document.getElementById("invoiceValue").focus();
//                return;
//            }
//            if (isEmpty(document.getElementById("packageWeight").value)) {
//                alert('please enter invoice weight');
//                document.getElementById("packageWeight").focus();
//                return;
//            }
//            if (isEmpty(document.getElementById("lrNumber").value)) {
//                alert('please enter Docketno');
//                document.getElementById("lrNumber").focus();
//                return;
//            }
//
//            if (isEmpty(document.getElementById("startDate").value)) {
//                alert('please enter plan start date');
//                document.getElementById("startDate").focus();
//                return;
//            }
//            if (isEmpty(document.getElementById("fuelLtr").value)) {
//                alert('please enter Fuel Details');
//                document.getElementById("fuelLtr").focus();
//                return;
//            }
////            if (isEmpty(document.getElementById("startOdometerReading").value)) {
////                alert('please enter start odometer reading');
////                document.getElementById("startOdometerReading").focus();
////                return;
////            }
//            var customer = document.getElementById("customerType").value;
//            if (customer == "2") {
////                if (isEmpty(document.getElementById("vehicleactreportdate").value)) {
////                    alert('please enter the vehicle reporting date');
////                    document.getElementById("vehicleactreportdate").focus();
////                }
////                if (isEmpty(document.getElementById("vehicleloadtemperature").value)) {
////                    alert('please enter the loading temperature');
////                    document.getElementById("vehicleloadtemperature").focus();
////                }
////                if (isEmpty(document.getElementById("startHM").value)) {
////                    alert('please enter start HM');
////                    document.getElementById("startHM").focus();
////                }
////                if (parseFloat(document.getElementById("vehicleloadtemperature").value) == 0) {
////                    errorMsg1 += "Are you sure to proceed...?";
////                }
////                if (parseFloat(document.getElementById("vehicleloadtemperature").value) == 0) {
////                    errorMsg1 += "The value of vehicle temperature is 0\n";
////                }
////                if (parseFloat(document.getElementById("startOdometerReading").value) == 0) {
////                    errorMsg1 += "The value of start odometer meter reading is 0\n";
////                }
////                if (parseFloat(document.getElementById("startHM").value) == 0) {
////                    errorMsg1 += "The value of start hour meter  reading is 0";
////
////                }
//
//                if (errorMsg1 != "") {
//                    var r = confirm(errorMsg1);
//                    if (r == true)
//                    {
//                        $("#save").hide();
//                        document.trip.action = '/throttle/updateStartTripSheet.do';
//                        document.trip.submit();
//                    }
//                } else {
//                    $("#save").hide();
//                    document.trip.action = '/throttle/updateStartTripSheet.do';
//                    document.trip.submit();
//
//                }
//            } else {
//                $("#save").hide();
//                document.trip.action = '/throttle/updateStartTripSheet.do';
//                document.trip.submit();
//            }
//        }

        function submitPage() {
            $("#save").hide();
            document.trip.action = '/throttle/updateStartTripSheetDetails.do';
            document.trip.submit();
        }
    </script>

</head>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.StartTrip" text="StartTrip"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.StartTrip" text="StartTrip"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">


            <body onload="addRow1();
                    calculateStartDateDifference();
                    cheeckHr();"  >

                <form name="trip" method="post">
                    <%
                    Date today = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    String startDate = sdf.format(today);
                    %>
                    <%--<%@ include file="/content/common/path.jsp" %>--%>


                    <%--<c:if test="${roleId == 1023 || roleId == 1013 || roleId == 1016}">--%>

                    <%
                    boolean isSingleConsignmentOrder = false;
                    %>

                    <c:if test="${roleId == 1023 || roleId == 1013 || roleId == 1016}">
                        <% isSingleConsignmentOrder = true;%>
                    </c:if>   
                    <%if (isSingleConsignmentOrder){%>

                    <table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                        <tr id="exp_table">
                            <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                                <div class="tabs" align="left" style="width:300;">

                                    <div id="first">
                                        <c:if test = "${tripDetails != null}" >
                                            <c:forEach items="${tripDetails}" var="trip">
                                                <%--<c:out value="${trip.orderRevenue}" />--%>
                                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                                        <td> <c:out value="${trip.orderRevenue}" /></td>
                                                        <!--<td> <c:out value="${trip.editFreightAmount}" /></td>-->

                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Projected Expense:</b></font></td>
                                                        <td> <c:out value="${trip.orderExpense}" /></td>
                                                    <input type="hidden" name="estimatedAdvancePerDay" value='<c:out value="${trip.estimatedAdvancePerDay}" />'>
                                                    <input type="hidden" name="estimatedTransitDays" value='<c:out value="${trip.estimatedTransitDays}" />'>
                                                    <input type="hidden" name="estimatedTransitHours" value='<c:out value="${trip.estimatedTransitHours}" />'>

                                                    </tr>


                                                    <c:set var="profitMargin" value="" />
                                                    <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                                    <c:set var="orderExpense" value="${trip.orderExpense}" />
                                                    <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                                                    <%
                                                    String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                                    String revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
        //                                            String revenueStr = "" + (String)pageContext.getAttribute("editFreightAmount");
                                                    float profitPercentage = 0.00F;
                                                    if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                                        profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                                    }


                                                    %>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                                        <td>  <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)
                                                            <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>                                                        
                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Credit Limit:</b></font></td>
                                                        <td>  Rs. 1,00,00,000.00
                                                        <td>
                                                    </tr>
                                                </table>
                                            </c:forEach>
                                        </c:if>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <%}else {%>
                    <c:if test = "${tripDetails != null}" >
                        <c:forEach items="${tripDetails}" var="trip">

                            <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">

                                <input type="hidden" name="estimatedAdvancePerDay" value='<c:out value="${trip.estimatedAdvancePerDay}" />'>
                                <input type="hidden" name="estimatedTransitDays" value='<c:out value="${trip.estimatedTransitDays}" />'>
                                <input type="hidden" name="estimatedTransitHours" value='<c:out value="${trip.estimatedTransitHours}" />'>

                                <c:set var="profitMargin" value="" />
                                <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                <c:set var="orderExpense" value="${trip.orderExpense}" />
                                <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                                <%
                                String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                String revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
//                                            String revenueStr = "" + (String)pageContext.getAttribute("editFreightAmount");
                                float profitPercentage = 0.00F;
                                if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                    profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                }

                                %>
                                <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                            </c:forEach>
                        </c:if>

                        <%}%>

                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <table class="table table-info mb30 table-hover"  >
                            <thead>
                                <% int loopCntr = 0;%>
                                <c:if test = "${tripDetails != null}" >
                                    <c:forEach items="${tripDetails}" var="trip">
                                        <% if(loopCntr == 0) {%>
                                        <tr>
                                            <th class="text1" >Vehicle: <c:out value="${trip.vehicleNo}" /></th>
                                            <th class="text1" >Trip Code: <c:out value="${trip.tripCode}"/></th>
                                            <th class="text1" >Customer Name:&nbsp;<c:out value="${trip.customerName}"/></th>
                                            <th class="text1" >Route: &nbsp;<c:out value="${trip.routeInfo}"/></th>
                                            <th class="text1" >Status: <c:out value="${trip.status}"/>
                                                <input type="hidden" name="tripCodeEmail" value='<c:out value="${trip.tripCode}"/>' />
                                                <input type="hidden" name="customerNameEmail" value='<c:out value="${trip.customerName}"/>' />
                                                <input type="hidden" name="routeInfoEmail" value='<c:out value="${trip.routeInfo}"/>' />
                                                <input type="hidden" name="tripType" value='<c:out value="${tripType}"/>' />
                                                <input type="hidden" name="statusId" value='<c:out value="${statusId}"/>' />
                                                <c:set var="customerId" value="${trip.customerId}"></c:set>
                                                <input type="hidden" id="customerId" name="customerId" value='<c:out value="${trip.customerId}"/>' >
                                                <c:set var="cbt" value="${trip.cbt}"></c:set>
                                                </th>
                                            </tr>
                                        </thead>
                                    <% }%>
                                    <% loopCntr++;%>
                                </c:forEach>
                            </c:if>
                        </table>
                        <div id="tabs">
                            <ul class="nav nav-tabs">
                                <li class="active" data-toggle="tab"><a href="#invDetail"><span>Invoice Details</span></a></li>
                                <li data-toggle="tab"><a href="#CBTDetail"><span>Load Details</span></a></li>
                                <li data-toggle="tab"><a href="#tripStart"><span>Trip Start </span></a></li>
                                <li data-toggle="tab"><a href="#tripDetail"><span>Trip Details</span></a></li>
                                <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                                <li data-toggle="tab"><a href="#statusDetail"><span>Status History</span></a></li>
                                <!--<li style="display:none" ><a href="#advance"><span>Advance</span></a></li>-->
                                <!--                    <li><a href="#preStart"><span>Trip Pre Start Details </span></a></li>-->
                                <!--
                                <li><a href="#cleanerDetail"><span>Cleaner</span></a></li>-->
                                <!--                    <li><a href="#advDetail"><span>Advance</span></a></li>-->
                                <!--<li><a href="#expDetail"><span>Expense Details</span></a></li>-->
                                <!--                    <li><a href="#summary"><span>Remarks</span></a></li>-->
                            </ul>

                            <div id="CBTDetail">
                                <table  class="table table-info mb30 table-hover" align="center" width="80%" id="bg">
                                    <thead><tr>
                                            <th  colspan="10" >Load Details</th>
                                        </tr></thead>
                                    <thead>
                                        <tr >
                                            <th  >S.No</th>
                                            <th  >Total Pallets</th>
                                            <th  >Pending Pallets</th>
                                            <th  >Loaded Pallets</th>
                                        </tr>
                                    </thead> 
                                    <tr>

                                        <c:if test = "${loadDetails != null}" >
                                        <tbody>
                                            <% int loadsno = 1;%>
                                            <c:forEach items="${loadDetails}" var="load">

                                                <tr>
                                                    <td><%=loadsno++%><input type="hidden" name="loadOrderId" id="loadOrderId<%=loadsno%>" value='<c:out value="${load.consignmentOrderId}"/>'/></td>

                                                    <td>
                                                        <c:out value="${load.totalPackages}"/>
                                                        <input type="hidden" name="totalPackages" id="totalPackages<%=loadsno%>" value="<c:out value="${load.totalPackages}"/>"  onblur="callLoadedGrossWeight();" onKeyPress="return onKeyPressBlockCharacters(event);"  class="form-control" style="width:180px;height:35px;" />
                                                    </td>

                                                    <td>
                                                        <input type="text" readonly name="pendingPkgs" id="pendingPkgs<%=loadsno%>" value='<c:out value="${load.pendingPackages}"/>' class='form-control' style='width: 200px;height: 40px'/>
                                                        <input type="hidden" readonly name="origPendingPkgs" id="origPendingPkgs<%=loadsno%>" value='<c:out value="${load.pendingPackages}"/>' class='form-control' style='width: 200px;height: 40px'/>
                                                    </td>
                                                    <td>
                                                        <input type="text" name="loadedPkgs" id="loadedPkgs<%=loadsno%>" onblur="validateLoadedPackages(<%=loadsno%>);" onKeyPress="return onKeyPressBlockCharacters(event);" value='<c:out value="${load.loadedpackagesNew}"/>' class='form-control' style='width: 200px;height: 40px'/>
                                                        <input type="hidden" name="loadedGrossWeight" id="loadedGrossWeight<%=loadsno%>" onblur="validateLoadedGrossWeight(<%=loadsno%>);"  onKeyPress="return onKeyPressBlockCharacters(event);" value='<c:out value="${load.pendingGrossWeight}"/>' class='form-control' style='width: 200px;height: 40px'/>
                                                        <input type="hidden" name="loadedWeight" id="loadedWeight<%=loadsno%>" onblur="validateLoadedWeight(<%=loadsno%>);"  onKeyPress="return onKeyPressBlockCharacters(event);" value='<c:out value="${load.pendingWeight}"/>' class='form-control' style='width: 200px;height: 40px'/>
                                                        <input type="hidden" readonly name="pendingGrossWeight" id="pendingGrossWeight<%=loadsno%>" value='0' class='form-control' style='width: 200px;height: 40px'/>
                                                        <input type="hidden" readonly name="origPendingGrossWeight" id="origPendingGrossWeight<%=loadsno%>" value='<c:out value="${load.pendingGrossWeight}"/>' class='form-control' style='width: 200px;height: 40px'/>
                                                        <input type="hidden" readonly name="pendingWeight" id="pendingWeight<%=loadsno%>" value='0' class='form-control' style='width: 200px;height: 40px'/>
                                                        <input type="hidden" readonly name="totalChargeableWeight" id="totalChargeableWeight<%=loadsno%>" value='<c:out value="${load.totalChargeableWeight}"/>' class='form-control' style='width: 200px;height: 40px'/>
                                                        <input type="hidden" readonly name="origPendingWeight" id="origPendingWeight<%=loadsno%>" value='<c:out value="${load.pendingWeight}"/>' class='form-control' style='width: 200px;height: 40px'/>

                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </c:if>
                                </table>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.NEXT" text="Next"/>" name="Next" style="width:100px;height:40px;"/></a>
                                </center>
                            </div>


                            <div id="invDetail"  >
                                <table  class="table table-info mb30 table-hover" align="center" width="80%" id="bg">
                                    <thead><tr>
                                            <th  colspan="4" >Vehicle Details</th>
                                        </tr></thead>



                                    <c:if test = "${lhcDetails != null}" >
                                        <c:forEach items="${lhcDetails}" var="lhc">
                                            <tr>
                                                <td >LHC No&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;</td>
                                                <td ><c:out value="${lhc.lhcNo}"/>
                                                    <input type="hidden" value="<c:out value="${lhc.lhcId}"/>" id="lhcId" name="lhcId">
                                                </td>
                                                <td>Device Id</td>
                                                <td><input type="text" autocomplete="off" value="<c:out value="${deviceId}"/>" id="deviceId" name="deviceId" class="form-control" style="width:175px;height:40px"></td>
                                            </tr>
                                            <tr>
                                                <td ><font color="red">*</font>Tag/Seal No&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;</td>
                                                <td >
                                                    <select name="sealNoType"  id="sealNoType" class="form-control" onchange="showLable(this.value);" style="width:175px;height:40px" >
                                                        <option value="sealNo">Seal No</option>
                                                        <option value="tagNo">Tag No</option>
                                                    </select>
                                                </td>

                                                <td>                                                
                                                    <div id="sealNoLab" >
                                                        <font color="red">*</font> Seal No
                                                    </div>
                                                    <div id="tagNoLab" style="display:none">
                                                        <font color="red">*</font> Tag No
                                                    </div>
                                                </td>                                            
                                                <td >
                                                    <input type="text" autocomplete="off" name="sealNo" id="sealNo"  value="<c:out value="${sealNo}"/>"  class="form-control" style="width:175px;height:40px" >
                                                    <input type="hidden" name="sealType" id="sealType"  value="<c:out value="${sealType}"/>"  >
                                                </td>

                                            <script>

                                                var val = document.getElementById("sealType").value;
                                                if (val == 'sealNo') {
                                                    $("#sealNoLab").show();
                                                    $("#tagNoLab").hide();
                                                    $("#sealNoType").val(val);
                                                } else if (val == 'tagNo') {
                                                    $("#sealNoLab").hide();
                                                    $("#tagNoLab").show();
                                                    $("#sealNoType").val(val);
                                                } else {
                                                    $("#sealNoLab").show();
                                                    $("#tagNoLab").hide();
                                                    $("#sealNoType").val(0);
                                                }

                                            </script>

                                            </tr>
                                            <tr>
                                                <td ><font color="red">*</font>Vehicle No&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;</td>
                                                <td >
                                                    <input type="hidden" name="vendorId" id="vendorId"  value="<c:out value="${lhc.vendorId}"/>"  class="form-control" style="width:175px;height:40px" >
                                                    <input type="hidden" name="vehicleTypeId" id="vehicleTypeId"  value="<c:out value="${lhc.vehicleTypeId}"/>"  class="form-control" style="width:175px;height:40px" >
                                                    <input type="hidden" name="vehicleId" id="vehicleId"  value=""  class="form-control" style="width:175px;height:40px" >
                                                    <input type="text" autocomplete="off" oncopy="return false" oncut="return false" onpaste="return false" name="vehicleNo" id="vehicleNo"  value="<c:out value="${lhc.vehicleNo}"/>"  class="form-control" style="width:175px;height:40px" onkeypress="return checkSpcialChar(event)">
                                                </td></tr>
                                            <tr>
                                                <td ><font color="red">*</font>Driver Name</td>
                                                <td >
                                                    <input type="text" name="driverName" id="driverName"  value=""   class="form-control" style="width:175px;height:40px" >
                                                </td>
                                                <td ><font color="red">*</font>Driver Mobile</td>
                                                <td >
                                                    <input type="text" autocomplete="off" name="driverMobile" id="driverMobile"  value="<c:out value="${lhc.driverMobile}"/>"  class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:175px;height:40px" >
                                                    <!--<input type="hidden" name="driverName" id="driverName"  value="<c:out value="${lhc.driverName}"/>"   class="form-control" style="width:175px;height:40px" >-->
                                                    <input type="hidden" name="dlNumber" id="dlNumber"  value="<c:out value="${lhc.dlNo}"/>"   class="form-control" style="width:175px;height:40px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td ><font color="red">*</font>Escort Name</td>
                                                <td >
                                                    <input type="text" name="escortName" id="escortName"  value=""   class="form-control" style="width:175px;height:40px" >
                                                </td>
                                                <td ><font color="red">*</font>Escort Mobile</td>
                                                <td >
                                                    <input type="text" autocomplete="off" name="escortMobile" id="escortMobile"  value=""  class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:175px;height:40px" >
                                                   
                                                </td>
                                            </tr>

                                        </c:forEach>
                                    </c:if>


                                    <table class="table table-info mb30 table-hover" id="bg" >     
                                        <thead>
                                            <tr>
                                                <th  colspan="4">Invoice Details</th>
                                            </tr>
                                        </thead>
                                        <tr>
                                            <td ><font color="red">*</font>E-Way Bill No&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;</td>
                                            <td >
                                                <input type="hidden" name="vehicleloadtemperature" id="vehicleloadtemperature"  value="0" onchange="checkEmpty();"  >
                                                <input type="text" autocomplete="off" name="eWayBillNo" id="eWayBillNo"  value="<c:out value="${ewaybillNo}"/>"  class="form-control" style="width:175px;height:40px" >
                                            </td>
                                            <td  ><font color="red">*</font>Invoice No</td>
                                            <td  >
                                                <input type="text" autocomplete="off" name="invoiceNumber" id="invoiceNumber"  value="<c:out value="${invoiceNo}"/>"   class="form-control" style="width:175px;height:40px" >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td ><font color="red">*</font>Invoice Value</td>
                                            <td >
                                                <input type="text" autocomplete="off" name="invoiceValue" id="invoiceValue"  value="<c:out value="${invoiceValue}"/>"  class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:175px;height:40px" >
                                            </td>
                                            <td ><font color="red">*</font>Invoice Date</td>
                                            <td >
                                                <input type="text" autocomplete="off" name="invoiceDate" id="invoiceDate" class="datepicker  form-control" style="width:175px;height:40px" value="<c:out value="${invoiceDate}"/>" >
                                                <input type="hidden" name="fuelLtr" id="fuelLtr"  value="0"   class="form-control" style="width:175px;height:40px" onKeyPress="return onKeyPressBlockCharacters(event);">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><font color="red">*</font>Invoice Weight (KG)</td>
                                            <td>                                            
                                                <input type="text" name="packageWeight" autocomplete="off" id="packageWeight" class="form-control" style="width:175px;height:40px" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${invoiceWeight}"/>" >
                                            </td>
                                            <td><font color="red">*</font>Docket No</td>

                                            <td>
                                                <c:if test = "${tripDetails != null}" >
                                                    <c:forEach items="${tripDetails}" var="trip">
                                                        <input type="text" autocomplete="off" name="lrNumber" readonly id="lrNumber" class="form-control" style="width:175px;height:40px"  value="<c:out value="${trip.tripCode}"/>" onchange="checkLrNumber();">
                                                    </c:forEach>
                                                </c:if>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td  colspan="4" align="center" style="display: none" id="nameStatus"><label id="lrNameStatus" style="color: red"></label></td>
                                        </tr>

                                    </table>
                                    <center>
                                        <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.NEXT" text="Next"/>" name="Next" style="width:100px;height:40px;"/></a>
                                            <c:if test = "${updateCheck==0}" >
                                            <a><input type="button" class="btn btn-success" name="Save" value="START" id="save" onclick="submitPage();" style="width:100px;height:40px;"/></a>
                                            </c:if>

                                        <c:if test = "${updateCheck>0}" >
                                            <a><input type="button" class="btn btn-success" name="Update" value="Update" id="update" onclick="updatePage();" style="width:100px;height:40px;"/></a>
                                            </c:if>

                                    </center>
                            </div>

                            <div id="tripDetail">
                                <table class="table table-info mb30 table-hover" id="bg" >
                                    <thead>
                                        <tr>
                                            <th  colspan="6" >Trip Details</th>
                                        </tr>
                                    </thead>

                                    <c:if test = "${tripDetails != null}" >
                                        <c:forEach items="${tripDetails}" var="trip">
                                            <tr>                                           
                                                <td >Consignment No(s)</td>
                                                <td >
                                                    <c:out value="${trip.cNotes}" />
                                                    <input type="hidden" name="cNotesEmail" value='<c:out value="${trip.cNotes}"/>' />
                                                </td>
                                                <td >Billing Type</td>
                                                <td >
                                                    <c:out value="${trip.billingType}" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >Customer Name</td>
                                                <td >
                                                    <c:out value="${trip.customerName}" />
                                                    <input type="hidden" name="customerName" Id="customerName" class="textbox" value='<c:out value="${customerName}" />'>
                                                </td>
                                                <td >Customer Type</td>
                                                <td  colspan="3" >
                                                    <c:out value="${trip.customerType}" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >Route Name</td>
                                                <td >
                                                    <c:out value="${trip.routeInfo}" />
                                                </td>
                                                <td >TAT Time</td>
                                                <td >
                                                    <input type="text" name="tatTime" Id="tatTime"  class="   form-control" style="width:130px;height:30px" readonly value='<c:out value="${trip.tatTime}" />'>
                                                </td>
                                                <script>
                                                    setTatTime('<c:out value="${trip.movementTypeId}" />');
                                                    function setTatTime(val){
//                                                        alert("val=="+val);
                                                        if(val == 15 || val == 16 || val == 7 || val == 8){
                                                            document.getElementById('tatTime').readonly=false;
                                                        }else{
                                                            document.getElementById('tatTime').readonly=true;
                                                        }
                                                    }
                                                </script>
                                                <td ></td>
                                                <td ></td>
                                                 <td ><input type="text" name="movementTypeId" id="movementTypeId"  class="   form-control" style="width:130px;height:30px" value='<c:out value="${trip.movementTypeId}" />' /></td>
                                      
                                                <td style="display:none">Reefer Required</td>
                                                <td style="display:none" >
                                                    <c:out value="${trip.reeferRequired}" />
                                                </td>
                                                <td style="display:none" >Order Est Weight (MT)</td>
                                                <td style="display:none" >
                                                    <c:out value="${trip.totalWeight}" />
                                                </td>
                                            </tr>
                                            <c:if test="${trip.movementTypeId == 3 ||trip.movementTypeId == 4 || trip.movementTypeId == 11 || trip.movementTypeId == 12 }" >
                                                 <tr>
                                                <td >Material Description</td>
                                                <td >
                                                    <input type="text" name="materialDesc" Id="materialDesc"  class="   form-control" style="width:130px;height:30px" value="">
                                                </td>
                                                <td >Insurance Contact Detail</td>
                                                <td  colspan="3" >
                                                   <input type="text" name="insuranceDetail" Id="insuranceDetail"  class="   form-control" style="width:130px;height:30px" value="">
                                              
                                                </td>
                                            </tr>
                                            </c:if>
                                            <c:if test="${trip.movementTypeId == 15 ||trip.movementTypeId == 16 || trip.movementTypeId == 7 || trip.movementTypeId == 8 }" >
                                                 <tr>
                                                <td >Appointment Date</td>
                                                <td >
                                                    <input type="text" name="appDate" Id="appDate"  class="datepicker   form-control" style="width:130px;height:30px" value="">
                                                </td>
                                                <td >Appointment time </td>
                                        <td  colspan="3" align="left" height="25" >HH:
                                            <select name="appHour" id="appHour" class="textbox" onchange="calculateTime();"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                            MI:
                                            <select name="appMin" id="appMin" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>

                                            </tr>
                                            </c:if>
                                            <tr style="display:none">
                                                <td >Vehicle Type</td>
                                                <td >
                                                    <c:out value="${trip.vehicleTypeName}" />
                                                </td>
                                                <td ><font color="red">*</font>Vehicle No</td>
                                                <td >
                                                    <c:out value="${trip.vehicleNo}" />
                                                    <input type="hidden" name="vehicleno" value="<c:out value="${trip.vehicleNo}" />"/>
                                                    <input type="hidden" name="vehicleNoEmail" value='<c:out value="${trip.vehicleNo}"/>' />

                                                </td>
                                                <td style="display:none" >Vehicle Capacity (MT)</td>
                                                <td style="display:none">
                                                    <c:out value="${trip.vehicleTonnage}" />
                                                </td>
                                            </tr>

                                            <tr style="display:none">
                                                <td >Veh. Cap [Util%]</td>
                                                <td >
                                                    <c:out value="${trip.vehicleCapUtil}" />
                                                </td>
                                                <td >Special Instruction</td>
                                                <td >-</td>
                                                <td >Trip Schedule</td>
                                                <td ><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" />
                                                    <input type="hidden" name="tripScheduleDate"  id="tripScheduleDate" value='<c:out value="${trip.tripScheduleDate}" />'>
                                                    <input type="hidden" name="tripScheduleTime" id="tripScheduleTime" value='<c:out value="${trip.tripScheduleTime}" />'></td>
                                            </tr>


                                            <tr style="display:none">
                                                <td ><font color="red">*</font>Driver </td>
                                                <td  colspan="5" >
                                                    <c:out value="${trip.driverName}" />
                                                </td>

                                            </tr>
                                            <tr style="display:none">
                                                <td >Product Info </td>
                                                <td  colspan="5" >
                                                    <c:out value="${trip.productInfo}" />
                                                </td>

                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                </table>



                                <c:if test = "${expiryDateDetails != null}" >
                                    <table class="table table-info mb30 table-hover" id="bg" style="display:none" >
                                        <thead>
                                            <tr>
                                                <th  colspan="4" >Vehicle Compliance Check</th>
                                            </tr>
                                        </thead>
                                        <c:forEach items="${expiryDateDetails}" var="expiryDate">
                                            <tr>
                                                <td >Vehicle FC Valid UpTo</td>
                                                <td ><label><font color="green"><c:out value="${expiryDate.fcExpiryDate}" /></font></label></td>
                                            </tr>
                                            <tr>
                                                <td >Vehicle Insurance Valid UpTo</td>
                                                <td ><label><font color="green"><c:out value="${expiryDate.insuranceExpiryDate}" /></font></label></td>
                                            </tr>
                                            <tr>
                                                <td >Vehicle Permit Valid UpTo</td>
                                                <td ><label><font color="green"><c:out value="${expiryDate.permitExpiryDate}" /></font></label></td>
                                            </tr>
                                            <tr>
                                                <td >Road Tax Valid UpTo</td>
                                                <td ><label><font color="green"><c:out value="${expiryDate.roadTaxExpiryDate}" /></font></label></td>
                                            </tr>
                                        </c:forEach>
                                    </table>


                                    <center>
                                        <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.NEXT" text="Next"/>" name="Next" style="width:100px;height:40px;"/></a>
                                    </center>
                                </c:if>

                            </div>
                            <div id="routeDetail">

                                <c:if test = "${tripPointDetails != null}" >
                                    <table class="table table-info mb30 table-hover" id="bg" >	
                                        <thead>
                                            <tr >
                                                <th  height="30" >S No</th>
                                                <th  height="30" >Point Name</th>
                                                <th  height="30" >Type</th>
                                                <th  height="30" >Route Order</th>
                                                <th  height="30" >Address</th>
                                                <th  height="30" >Planned Date</th>
                                                <th  height="30" >Planned Time</th>
                                            </tr>
                                        </thead>
                                        <% int index2 = 1; %>
                                        <c:forEach items="${tripPointDetails}" var="tripPoint">
                                            <%
                                                           String classText1 = "";
                                                           int oddEven = index2 % 2;
                                                           if (oddEven > 0) {
                                                               classText1 = "text1";
                                                           } else {
                                                               classText1 = "text2";
                                                           }
                                            %>
                                            <tr >
                                                <td  height="30" ><%=index2++%></td>
                                                <td  height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                                <td  height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                                <td  height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                                <td  height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                                <td  height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                                <td  height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                                                <td  height="30" >
                                                    <c:if test = "${tripPoint.pointSequence == '1'}" >
                                                        <input type="hidden" name="tripRouteCourseId" id="tripRouteCourseId" value="<c:out value="${tripPoint.tripRouteCourseId}"/>"/>
                                                    </c:if>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </table>


                                    <table class="table table-info mb30 table-hover" id="bg" style="display:none">	
                                        <thead>
                                            <c:if test = "${tripDetails != null}" >
                                                <c:forEach items="${tripDetails}" var="trip">
                                                    <tr>
                                                        <td  width="150"> Estimated KM</td>
                                                        <td  width="120" > <c:out value="${trip.estimatedKM}" />&nbsp;</td>
                                                        <td  colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td  width="150"> Estimated Reefer Hour</td>
                                                        <td  width="120"> <c:out value="${trip.estimatedTransitHours * 60 / 100}" />&nbsp;</td>
                                                        <td  colspan="4">&nbsp;</td>
                                                    </tr>
                                                </thead>
                                            </c:forEach>
                                        </c:if>
                                    </table>


                                    <center>
                                        <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.NEXT" text="Next"/>" name="Next" style="width:100px;height:40px;"/></a>
                                    </center>
                                </c:if>


                            </div>

                            <div id="statusDetail">
                                <% int index1 = 1; %>

                                <c:if test = "${statusDetails != null}" >
                                    <table class="table table-info mb30 table-hover" id="bg" >
                                        <thead>
                                            <tr >
                                                <th   height="30" >S No</th>
                                                <th  height="30" >Status Name</th>
                                                <th  height="30" >Remarks</th>
                                                <th  height="30" >Created User Name</th>
                                                <th  height="30" >Created Date</th>
                                            </tr>
                                        </thead>
                                        <c:forEach items="${statusDetails}" var="statusDetails">
                                            <%
                                                   String classText = "";
                                                   int oddEven1 = index1 % 2;
                                                   if (oddEven1 > 0) {
                                                       classText = "text1";
                                                   } else {
                                                       classText = "text2";
                                                   }
                                            %>
                                            <tr >
                                                <td  height="30" ><%=index1++%></td>
                                                <td  height="30" ><c:out value="${statusDetails.statusName}" /></td>
                                                <td  height="30" ><c:out value="${statusDetails.tripRemarks}" /></td>
                                                <td  height="30" ><c:out value="${statusDetails.userName}" /></td>
                                                <td  height="30" ><c:out value="${statusDetails.tripDate}" /></td>
                                            </tr>
                                        </c:forEach >
                                    </table>
                                </c:if>
                            </div>
                            <c:if test="${tripAdvanceDetails != null}">
                                <div id="advance" style="display:none">
                                    <c:if test="${tripAdvanceDetails != null}">
                                        <table class="table table-info mb30 table-hover" id="bg" >	
                                            <thead>
                                                <tr>
                                                    <th  width="30">Sno</th>
                                                    <th  width="90">Advance Date</th>
                                                    <th  width="90">Trip Day</th>
                                                    <th  width="120">Estimated Advance</th>
                                                    <th  width="120">Requested Advance</th>
                                                    <th  width="90"> Type</th>
                                                    <th  width="120">Requested By</th>
                                                    <th  width="120">Requested Remarks</th>
                                                    <th  width="120">Approved By</th>
                                                    <th  width="120">Approved Remarks</th>
                                                    <th  width="120">Paid Advance</th>
                                                </tr>
                                            </thead>
                                            <%int index7=1;%>
                                            <c:forEach items="${tripAdvanceDetails}" var="tripAdvance">
                                                <c:set var="totalAdvancePaid" value="${ totalAdvancePaid + tripAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                                <%
                                                       String classText7 = "";
                                                       int oddEven7 = index7 % 2;
                                                       if (oddEven7 > 0) {
                                                           classText7 = "text1";
                                                       } else {
                                                           classText7 = "text2";
                                                       }
                                                %>
                                                <tr>
                                                    <td ><%=index7++%></td>
                                                    <td ><c:out value="${tripAdvance.advanceDate}"/></td>
                                                    <td >DAY&nbsp;<c:out value="${tripAdvance.tripDay}"/></td>
                                                    <td ><c:out value="${tripAdvance.estimatedAdance}"/></td>
                                                    <td ><c:out value="${tripAdvance.requestedAdvance}"/></td>
                                                    <c:if test = "${tripAdvance.requestType == 'A'}" >
                                                        <td >Adhoc</td>
                                                    </c:if>
                                                    <c:if test = "${tripAdvance.requestType == 'B'}" >
                                                        <td >Batch</td>
                                                    </c:if>
                                                    <c:if test = "${tripAdvance.requestType == 'M'}" >
                                                        <td >Manual</td>
                                                    </c:if>
                                                    <td ><c:out value="${tripAdvance.approvalRequestBy}"/></td>
                                                    <td ><c:out value="${tripAdvance.approvalRequestRemarks}"/></td>
                                                    <td ><c:out value="${tripAdvance.approvedBy}"/></td>
                                                    <td ><c:out value="${tripAdvance.approvalRemarks}"/></td>
                                                    <td ><c:out value="${tripAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                                </tr>
                                            </c:forEach>

                                            <tr></tr>
                                            <tr>

                                                <td >&nbsp;</td>
                                                <td >&nbsp;</td>
                                                <td >&nbsp;</td>
                                                <td >&nbsp;</td>
                                                <td >&nbsp;</td>
                                                <td >&nbsp;</td>
                                                <td ></td>
                                                <td ></td>
                                                <td  colspan="2">Total Advance Paid</td>
                                                <td ><c:out value="${totalAdvancePaid}"/></td>
                                            </tr>
                                        </table>

                                        <c:if test="${tripAdvanceDetailsStatus != null}">
                                            <table class="table table-info mb30 table-hover" id="bg" >	
                                                <thead>
                                                    <tr>
                                                        <th  colspan="13" > Advance Approval Status Details</th>
                                                    </tr>
                                                </thead>
                                                <tr>
                                                    <th  width="30">Sno</th>
                                                    <th  width="90">Request Date</th>
                                                    <th  width="90">Trip Day</th>
                                                    <th  width="120">Estimated Advance</th>
                                                    <th  width="120">Requested Advance</th>
                                                    <th  width="90"> Type</th>
                                                    <th  width="120">Requested By</th>
                                                    <th  width="120">Requested Remarks</th>
                                                    <th  width="120">Approval Status</th>
                                                    <th  width="120">Paid Status</th>
                                                </tr>
                                                <%int index13=1;%>
                                                <c:forEach items="${tripAdvanceDetailsStatus}" var="tripAdvanceStatus">
                                                    <%
                                                           String classText13 = "";
                                                           int oddEven11 = index13 % 2;
                                                           if (oddEven11 > 0) {
                                                               classText13 = "text1";
                                                           } else {
                                                               classText13 = "text2";
                                                           }
                                                    %>
                                                    <tr>

                                                        <td ><%=index13++%></td>
                                                        <td ><c:out value="${tripAdvanceStatus.advanceDate}"/></td>
                                                        <td >DAY&nbsp;<c:out value="${tripAdvanceStatus.tripDay}"/></td>
                                                        <td ><c:out value="${tripAdvanceStatus.estimatedAdance}"/></td>
                                                        <td ><c:out value="${tripAdvanceStatus.requestedAdvance}"/></td>
                                                        <c:if test = "${tripAdvanceStatus.requestType == 'A'}" >
                                                            <td >Adhoc</td>
                                                        </c:if>
                                                        <c:if test = "${tripAdvanceStatus.requestType == 'B'}" >
                                                            <td >Batch</td>
                                                        </c:if>
                                                        <c:if test = "${tripAdvanceStatus.requestType == 'M'}" >
                                                            <td >Manual</td>
                                                        </c:if>

                                                        <td ><c:out value="${tripAdvanceStatus.approvalRequestBy}"/></td>
                                                        <td ><c:out value="${tripAdvanceStatus.approvalRequestRemarks}"/></td>
                                                        <td >
                                                            <c:if test = "${tripAdvanceStatus.approvalStatus== ''}" >
                                                                &nbsp
                                                            </c:if>
                                                            <c:if test = "${tripAdvanceStatus.approvalStatus== '1' }" >
                                                                Request Approved
                                                            </c:if>
                                                            <c:if test = "${tripAdvanceStatus.approvalStatus== '2' }" >
                                                                Request Rejected
                                                            </c:if>
                                                            <c:if test = "${tripAdvanceStatus.approvalStatus== '0'}" >
                                                                Approval in  Pending
                                                            </c:if>
                                                            &nbsp;</td>
                                                        <td >
                                                            <c:if test = "${ tripAdvanceStatus.approvalStatus== '1' || tripAdvanceStatus.approvalStatus== 'N'}" >
                                                                Yet To Pay
                                                            </c:if>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </table>



                                            <c:if test="${vehicleChangeAdvanceDetailsSize != '0'}">
                                                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                                    <tr>
                                                        <td  colspan="13" > Old Vehicle Advance Details</td>
                                                    </tr>
                                                    <tr>
                                                        <td  width="30">Sno</td>
                                                        <td  width="90">Advance Date</td>
                                                        <td  width="90">Trip Day</td>
                                                        <td  width="120">Estimated Advance</td>
                                                        <td  width="120">Requested Advance</td>
                                                        <td  width="90"> Type</td>
                                                        <td  width="120">Requested By</td>
                                                        <td  width="120">Requested Remarks</td>
                                                        <td  width="120">Approved By</td>
                                                        <td  width="120">Approved Remarks</td>
                                                        <td  width="120">Paid Advance</td>
                                                    </tr>
                                                    <%int index17=1;%>
                                                    <c:forEach items="${vehicleChangeAdvanceDetails}" var="vehicleChangeAdvance">
                                                        <c:set var="totalVehicleChangeAdvancePaid" value="${ totalVehicleChangeAdvancePaid + vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                                        <%
                                                               String classText17 = "";
                                                               int oddEven17 = index17 % 2;
                                                               if (oddEven17 > 0) {
                                                                   classText17 = "text1";
                                                               } else {
                                                                   classText17 = "text2";
                                                               }
                                                        %>
                                                        <tr>
                                                            <td class="<%=classText17%>"><%=index7++%></td>
                                                            <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.advanceDate}"/></td>
                                                            <td class="<%=classText17%>">DAY&nbsp;<c:out value="${vehicleChangeAdvance.tripDay}"/></td>
                                                            <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.estimatedAdance}"/></td>
                                                            <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.requestedAdvance}"/></td>
                                                            <c:if test = "${vehicleChangeAdvance.requestType == 'A'}" >
                                                                <td class="<%=classText17%>">Adhoc</td>
                                                            </c:if>
                                                            <c:if test = "${vehicleChangeAdvance.requestType == 'B'}" >
                                                                <td class="<%=classText17%>">Batch</td>
                                                            </c:if>
                                                            <c:if test = "${vehicleChangeAdvance.requestType == 'M'}" >
                                                                <td class="<%=classText17%>">Manual</td>
                                                            </c:if>
                                                            <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRequestBy}"/></td>
                                                            <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRequestRemarks}"/></td>
                                                            <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvedBy}"/></td>
                                                            <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRemarks}"/></td>
                                                            <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                                        </tr>
                                                    </c:forEach>

                                                    <tr></tr>
                                                    <tr>

                                                        <td >&nbsp;</td>
                                                        <td >&nbsp;</td>
                                                        <td >&nbsp;</td>
                                                        <td >&nbsp;</td>
                                                        <td >&nbsp;</td>
                                                        <td >&nbsp;</td>
                                                        <td ></td>
                                                        <td ></td>
                                                        <td  colspan="2">Total Advance Paid</td>
                                                        <td ><c:out value="${totalVehicleChangeAdvancePaid}"/></td>
                                                    </tr>
                                                </table>
                                            </c:if>
                                        </c:if>

                                        <center>
                                            <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="hrms.label.NEXT" text="Next"/>" name="Next" style="width:100px;height:40px;"/></a>
                                        </center>
                                    </c:if>
                                </div>
                            </c:if>
                            <div id="tripStart">

                                <table class="table table-info mb30 table-hover" id="bg"  >	
                                    <thead><tr>
                                            <th  colspan="4">Vehicle Loading Details</th>
                                        </tr></thead>
                                    <tr>
                                        <td >Document Received Date</td>
                                        <td ><input type="text" name="documentRecivedDate" id="documentRecivedDate" class="datepicker   form-control" style="width:175px;height:40px"  value="<c:out value="${documentRecivedDate}" />" onchange="calculateDate1();" ></td>
                                        <td >Document Received time </td>
                                        <td  colspan="3" align="left" height="25" >HH:
                                            <select name="documentRecivedHr" id="documentRecivedHr" class="textbox" onchange="calculateTime();"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                            MI:
                                            <select name="documentRecivedMi" id="documentRecivedMi" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>

                                    <input type="hidden" name="docHr" id="docHr" value="<c:out value="${documentRecivedHour}" />"/>
                                    <input type="hidden" name="docMin" id="docMin" value="<c:out value="${documentRecivedMin}" />"/>
                                    </tr>

                                    <script>
                                        document.getElementById("documentRecivedHr").value = document.getElementById("docHr").value;
                                        document.getElementById("documentRecivedMi").value = document.getElementById("docMin").value;
                                    </script>

                                    <tr>
                                        <td ><font color="red">*</font>Loading Completed Date</td>
                                        <td ><input type="text" name="vehicleloadreportdate" id="vehicleloadreportdate" class="datepicker   form-control" style="width:175px;height:40px"  value="<c:out value="${vehicleloadreportdate}" />" ></td>
                                        <td ><font color="red">*</font>Loading Completed Time </td>
                                        <td  colspan="3" align="left" height="25" >HH:<select name="vehicleloadreporthour" id="vehicleloadreporthour" class="textbox" onchange="calculateTime();"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                            MI:<select name="vehicleloadreportmin" id="vehicleloadreportmin" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>

                                    <input type="hidden" name="loadHr" id="loadHr" value="<c:out value="${vehicleloadreportHour}" />"/>
                                    <input type="hidden" name="loadMin" id="loadMin" value="<c:out value="${vehicleloadreportMin}" />"/>

                                    <script>
                                        document.getElementById("vehicleloadreporthour").value = document.getElementById("loadHr").value;
                                        document.getElementById("vehicleloadreportmin").value = document.getElementById("loadMin").value;
                                    </script>
                                    </tr>

                                    <thead>
                                        <tr>
                                            <th  colspan="4" >Vehicle Reporting Details</th>
                                        </tr>
                                    </thead>

                                    <tr>
                                        <c:if test = "${tripDetails != null}" >
                                            <c:forEach items="${tripDetails}" var="trip">
                                                <td >Trip Planned Start Date</td>
                                                <td > <c:out value="${trip.tripScheduleDate}" /></td>

                                                <td  height="25" >Trip Planned Start Time </td>
                                                <td > <c:out value="${trip.tripScheduleTime}" /></td>
                                            </c:forEach>
                                        </c:if>

                                    </tr>

                                    <tr>
                                        <td ><font color="red">*</font>Vehicle Actual Reporting Date</td>
                                        <td ><input type="text" name="vehicleactreportdate" id="vehicleactreportdate"  class="datepicker   form-control" style="width:175px;height:40px" onchange="calculateStartDateDifference();"  value="<c:out value="${vehicleactreportdate}" />"></td>
                                        <td  height="25" ><font color="red">*</font>Vehicle Actual Reporting Time </td>
                                        <td  colspan="3" align="left" height="25" >
                                            HH:<select name="vehicleactreporthour" id="vehicleactreporthour" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                            MI:<select name="vehicleactreportmin" id="vehicleactreportmin" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>

                                        </td>
                                    <input type="hidden" name="actualHr" id="actualHr" value="<c:out value="${vehicleactreportHour}" />"/>
                                    <input type="hidden" name="actualMin" id="actualMin" value="<c:out value="${vehicleactreportMin}" />"/>

                                    <script>
                                        document.getElementById("vehicleactreporthour").value = document.getElementById("actualHr").value;
                                        document.getElementById("vehicleactreportmin").value = document.getElementById("actualMin").value;
                                    </script>

                                    </tr>

                                    <thead>
                                        <tr>
                                            <th  colspan="4" >Trip Start Details</td>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <c:if test = "${tripDetails != null}" >
                                            <c:forEach items="${tripDetails}" var="trip">
                                                <td >Trip Planned Start Date&emsp;&emsp;</td>
                                                <td > <c:out value="${trip.tripScheduleDate}" /></td>
                                                <td  height="25" >Trip Planned Start Time </td>
                                                <td > <c:out value="${trip.tripScheduleTime}" /></td>
                                            </c:forEach>
                                        </c:if>
                                    </tr>
                                    <tr>
                                        <td ><font color="red">*</font>Trip Actual Start Date</td>
                                        <td ><input type="text" name="startDate" id="startDate" class="datepicker  form-control" style="width:175px;height:40px" value="<c:out value="${startDate}" />" onchange="calculateStartDateDifference();">
                                            <input type="hidden" name="todayDate" id="todayDate" value='<%=startDate%>'/></td>
                                        <td  height="25" ><font color="red">*</font>Trip Actual Start Time </td>
                                        <td  colspan="3" align="left" height="25" >HH:<select name="tripStartHour"  id="tripStartHour" class="textbox" onchange="cheeckHr();"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                            MI:<select name="tripStartMinute" id="tripStartMinute" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select></td>

                                    <input type="hidden" name="actualStartHr" id="actualStartHr" value="<c:out value="${startHours}" />"/>
                                    <input type="hidden" name="actualStartMin" id="actualStartMin" value="<c:out value="${startMin}" />"/>

                                    </tr>
                                    <script>
                                        document.getElementById("tripStartHour").value = document.getElementById("actualStartHr").value;
                                        document.getElementById("tripStartMinute").value = document.getElementById("actualStartMin").value;
                                    </script>

                                    <tr >
                                        <td ><font color="red">*</font>Trip Start Odometer Reading(KM)</td>
                                            <c:if test = "${tripDetails != null}" >
                                                <c:forEach items="${tripDetails}" var="tp">
                                                <td ><input type="text" name="startOdometerReading" value='<c:out value="${tp.startOdometerReading}" />' id="startOdometerReading" onKeyPress="return onKeyPressBlockCharacters(event);"   class="form-control" style="width:175px;height:40px" ></td>
                                                </c:forEach>
                                            </c:if>

                                        <td style="display:none" >Trip Start Reefer Reading(HM)</td>
                                        <td style="display:none" ><input type="text" name="startHM" id="startHM" onKeyPress="return onKeyPressBlockCharacters(event);" class ="form-control" style="width:175px;height:40px" value="<c:out value="${previousEndHm}" />"></td>
                                    </tr>

                                    <tr>
                                        <td >Detention Charge</td>
                                        <td ><input type="text" name="originDetention" id="originDetention" readonly class="form-control" style="width:175px;height:40px"  value="<c:out value="${detention}" />"></td>
                                        <td >Trip Remarks</td>
                                        <td >
                                            <textarea rows="3" cols="30" class="textbox" name="startTripRemarks" id="startTripRemarks"   style="width:175px;height:40px">
                                                <c:out value="${tripRemarks}" />
                                            </textarea>

                                            <input type="hidden" name="previousDate" id="previousDate" value="<c:out value="${previousTripEndDate}" />">
                                            <input name="previousHr" id="previousHr" type="hidden" value="<c:out value="${previousTripEndHr}"/>">
                                            <input name="previousMin" id="previousMin" type="hidden"  value="<c:out value="${previousTripEndMin}"/>">
                                            <input name="previousDestinationId" id="previousDestinationId" type="hidden" type="text"  value="<c:out value="${previousDestinationId}"/>">
                                        </td>
                                    </tr>


                                    <c:if test="${tripSheetId != null}">
                                        <input type="hidden" id="tripSheetId" name="tripSheetId" value="<c:out value="${tripSheetId}"/>" />
                                    </c:if>
                                    <c:if test="${tripSheetId == null}">
                                        <input type="hidden" id="tripSheetId" name="tripSheetId" value="<%=request.getParameter("tripSheetId")%>" />
                                    </c:if>
                                    <input type="hidden" name="customerType" id="customerType" value="2" />                                    

                                </table>

                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="NEXT" name="Next" style="width:100px;height:40px;"/></a>


                                </center>

                            </div>

                            <script type="text/javascript">

                                function cheeckHr()
                                {
                                    //alert("the value is" + diff);
                                    if (diff <= 0) {
                                        //                                        alert("Nilesh");
                                        var hr = document.getElementById("previousHr").value;
                                        var starthr = document.getElementById("tripStartHour").value;
                                        if (starthr <= hr) {
                                            alert("Invalid time hr for trip start.pls check the previous trip End hour");
                                            $("#save").show();
                                        } else {
                                            $("#save").show();
                                        }
                                    } else {
                                    }
                                }



                            </script>
                            <script>
                                $('.btnNext').click(function() {
                                    $('.nav-tabs > .active').next('li').find('a').trigger('click');
                                });
                                //                            $('.btnPrevious').click(function() {
                                //                                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                                //                            });
                            </script>        
                            <script>
                                function showLable(val) {

                                    if (val == 'sealNo') {
                                        $("#sealNoLab").show();
                                        $("#tagNoLab").hide();

                                        $("#sealNoType").val(val);
                                    } else if (val == 'tagNo') {
                                        $("#sealNoLab").hide();
                                        $("#tagNoLab").show();

                                        $("#sealNoType").val(val);
                                    } else {
                                        $("#sealNoLab").hide();
                                        $("#tagNoLab").hide();

                                        $("#sealNoType").val(0);
                                    }
                                }
                            </script>

                        </div>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>