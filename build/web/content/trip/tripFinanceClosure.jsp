<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%@page import="java.text.SimpleDateFormat" %>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script>
    function saveTripEndDetails() {
        if (isEmpty(document.getElementById("endOdometerReading").value)) {
            alert('please enter end odometer reading');
            document.getElementById("endOdometerReading").focus();
        }
        if (isEmpty(document.getElementById("endHM").value)) {
            alert('please enter end HM');
            document.getElementById("endHM").focus();
        }
        else {
            document.tripExpense.action = '/throttle/updateEndTripSheetDuringClosure.do';
            document.tripExpense.submit();
        }
    }
    function saveTripOverRideDetails() {
        if (isEmpty(document.getElementById("setteledKm").value)) {
            alert('please enter override end odometer reading');
            document.getElementById("setteledKm").focus();
        }
        if (isEmpty(document.getElementById("setteledHm").value)) {
            alert('please enter override end reefer reading');
            document.getElementById("setteledHm").focus();
        }
        if (isEmpty(document.getElementById("overrideRemarks").value)) {
            alert('please enter override remarks');
            document.getElementById("overrideRemarks").focus();
        }
        else {
            document.tripExpense.action = '/throttle/updateOverrideTripSheetDuringClosure.do';
            document.tripExpense.submit();
        }
    }
    function saveTripStartDetails() {
        if (isEmpty(document.getElementById("tripStartKm").value)) {
            alert('please enter tripStartKm');
            document.getElementById("tripStartKm").focus();
        }
        if (isEmpty(document.getElementById("tripStartHm").value)) {
            alert('please enter tripStartHm');
            document.getElementById("tripStartHm").focus();
        }
        else {
            document.tripExpense.action = '/throttle/updateStartTripSheetDuringClosure.do';
            document.tripExpense.submit();
        }
    }
    function calTotalKM() {
        var startKM = document.getElementById("startKM").value;
        var endKM = document.getElementById("endOdometerReading").value;
        document.getElementById("totalKM").value = endKM - startKM;
    }
    function calSettelTotalKM() {
        var startKM = document.getElementById("startKM").value;
        var endKM = document.getElementById("setteledKm").value;
        var TotalSettelKM = endKM - startKM;
        document.getElementById("setteltotalKM").value = TotalSettelKM;
    }
    function calTotalHM() {
        var startHM = document.getElementById("startHM").value;
        var endHM = document.getElementById("endHM").value;
        document.getElementById("totalHrs").value = endHM - startHM;
    }
    function calSettelTotalHM() {
        var startHM = document.getElementById("startHM").value;
        var endHM = document.getElementById("setteledHm").value;
        var TotalSettelHm = endHM - startHM;
        document.getElementById("setteltotalHrs").value = TotalSettelHm;
    }
    function saveOdoApprovalRequest() {
        var odoUsageRemarks = document.tripExpense.odoUsageRemarks.value;
        var requestType = 1; //odo request
        saveApprovalRequest(odoUsageRemarks, requestType, 1)

    }
    function saveOdoReApprovalRequest() {
        var odoUsageRemarks = document.tripExpense.odoUsageReApprovalRemarks.value;
        var requestType = 1; //odo request
        saveApprovalRequest(odoUsageRemarks, requestType, 2)
    }
    function saveExpDeviationApprovalRequest() {
        var remarks = document.tripExpense.expDeviationRemarks.value;
        var requestType = 2; //exp request
        saveApprovalRequest(remarks, requestType, 3)
    }
    function saveexpDeviationReApprovalRequest() {
        var remarks = document.tripExpense.expDeviationReApprovalRemarks.value;
        var requestType = 2; //exp request
        saveApprovalRequest(remarks, requestType, 4)
    }

    var httpReq;
    var temp = "";
    function saveApprovalRequest(remarks, requestType, val) {
        var tripId = document.tripExpense.tripSheetId.value;
        remarks = remarks.trim();
        var rcmExp = document.tripExpense.estimatedExpense.value;
        var nettExp = document.tripExpense.nettExpense.value;
        var oT = document.tripExpense.orderTypeNew.value;
        if (remarks != '') {
            var url = "/throttle/saveClosureApprovalRequest.do?tripId=" + tripId + "&requestType=" + requestType + "&remarks=" + remarks + "&rcmExp=" + rcmExp + "&nettExp=" + nettExp;
            //alert(url);
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processOdoApprovalRequest(requestType, val, oT);
            };
            httpReq.send(null);
        } else {
            alert('please enter request description');
        }

    }

    function processOdoApprovalRequest(requestType, val, oT) {
        if (httpReq.readyState == 4) {
            if (httpReq.status == 200) {
                temp = httpReq.responseText.valueOf();
                //alert(temp);
                if (temp != '0' && temp != '' && temp != null && temp != 'null') {
                    if (val == 1) {
                        $('#odoApprovalRequest').hide();
                        $('#odoPendingApproval').show();
                    }
                    if (val == 2) {
                        $('#odoReApprovalRequest').hide();
                        $('#odoPendingReApproval').show();
                    }
                    if (val == 3) {
                        $('#expDeviationApprovalRequest').hide();
                        $('#expDeviationPendingApproval').show();
                    }
                    if (val == 4) {
                        $('#expDeviationReApprovalRequest').hide();
                        $('#expDeviationPendingReApproval').show();
                    }
                }
            }
            else {
                //alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
        document.tripExpense.action = '/throttle/viewTripExpense.do?orderType=' + oT;
        document.tripExpense.submit();
    }
    function calculateDays() {


        var diff = 0;
        var one_day = 1000 * 60 * 60 * 24;
        var fromDate = document.getElementById("startDate").value;
        var toDate = document.getElementById("endDate").value;
        var endTimeIds = document.getElementById("startTime").value;
        var hour1 = document.getElementById("tripEndHour").value;
        var minute1 = document.getElementById("tripEndMinute").value;
        var stTimeIds = hour1 + ":" + minute1 + ":" + "00";
        var tempTime2 = stTimeIds.split(":");
        var tempTime1 = endTimeIds.split(":");
        var earDate = fromDate.split("-");
        var nexDate = toDate.split("-");
        var fD = parseFloat(earDate[0]).toFixed(0);
        var fM = parseFloat(earDate[1]).toFixed(0);
        var fY = parseFloat(earDate[2]).toFixed(0);
        var tD = parseFloat(nexDate[0]).toFixed(0);
        var tM = parseFloat(nexDate[1]).toFixed(0);
        var tY = parseFloat(nexDate[2]).toFixed(0);
        var date2 = tD + tM + tY;
        var d1 = new Date(fY, fM, fD);
        var d2 = new Date(tY, tM, tD);
        var difference = d2.getTime() - d1.getTime();
        diff = (d2.getTime() - d1.getTime()) / one_day;
        document.getElementById("totalDays1").value = diff + 1;
        var prevTime = new Date(earDate[2], earDate[1], earDate[0], tempTime2[0], tempTime2[1]); // Feb 1, 2011
        var thisTime = new Date(nexDate[2], nexDate[1], nexDate[0], tempTime1[0], tempTime1[1]); // now
        var difference = thisTime.getTime() - prevTime.getTime(); // now - Feb 1
        var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
        document.getElementById("tripTransitHour").value = hoursDifference;
    }


    function setEndTime() {
        var endtime = document.getElementById('endTime').value;
        var endtimes = endtime.split(":");
        document.tripExpense.tripEndHour.value = endtimes[0];
        document.tripExpense.tripEndMinute.value = endtimes[1];
    }
</script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String todayDate = sdf.format(today);
%>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });</script>

<script type="text/javascript" language="javascript">
    function submitPageExpense() {
        var editId = document.getElementsByName("editId");
        var expenseName = document.getElementsByName("expenseName");
        var employeeName = document.getElementsByName("employeeName");
        var expenseDate = document.getElementsByName("expenseDate");
        var expenseType = document.getElementsByName("expenseType");
        var expenses = document.getElementsByName("expenses");
        var orderType = 0;
        var custId = document.getElementsByName("custId");
        var orderId = document.getElementsByName("orderId");

        for (var i = 0; (i < editId.length && editId.length != 0); i++) {

            if (custId[i].value == '' || custId[i].value == 0) {
                alert("please select Customer");
                custId[i].focus();
                return;
            }

            if (orderId[i].value == '' || orderId[i].value == 0) {
                alert("please select Order");
                orderId[i].focus();
                return;
            }

            if (expenseName[i].value == '' || expenseName[i].value == 0) {
                alert("please select Expense Name");
                expenseName[i].focus();
                return;

            }

            if (employeeName[i].value == '') {
                alert("please select Driver Name");
                employeeName[i].focus();
                return;

            }
            if (expenseDate[i].value == '' || expenseDate[i].value == 0) {
                alert("please select Expense Date");
                expenseDate[i].focus();
                return;
            }
            if (expenseType[i].value == '' || expenseType[i].value == 0) {
                alert("please select Expense Type");
                expenseType[i].focus();
                return;

            }
            if (expenses[i].value == '') {
                alert("please enter Expense Value");
                expenses[i].focus();
                return;

            }

        }
        if (editId.length > 0) {
            $("#SaveExp").hide();
            document.tripExpense.action = "/throttle/saveTripOtherExpense.do?expenseStatus=1&orderType=" + orderType;
            document.tripExpense.submit();
        } else {
            alert("Please enter any one expense");
            return;
        }

    }




    function submitPageDone() {
        var txt;
        var r = confirm("Press a button!");
        if (r == true) {
            document.tripExpense.action = '/throttle/saveDoneTripOtherExpense.do';
            document.tripExpense.submit();
        } else {
            txt = "You pressed Cancel!";
        }
    }
    function closeTrip() {

        document.tripExpense.action = '/throttle/closeTrip.do';
        document.tripExpense.submit();
    }



    function setExpenseType(value, sno) {
        if (value == "1015" || value == "1028" || value == "1038" || value == "1045") {
            alert("Please select valid expense");
            document.getElementById('expenseName' + sno).value = '0';
        }

        if (value == "1040") {

            document.getElementById('expenseType' + sno).value = '6';
            setExpenseTypeDetails('6', sno);
        }
        if (value == "1041") {

            document.getElementById('expenseType' + sno).value = '7';
            setExpenseTypeDetails('7', sno);
        }

    }

    function setExpenseTypeDetails(value, sno) {
        document.getElementById('marginValue' + sno).readOnly = true;
//        if (value == '1') {//bill to expense
//            document.getElementById('taxPercentage' + sno).value = '';
//            document.getElementById('expenses' + sno).value = '';
//            //alert("fgd");
//            document.getElementById('billModeTemp' + sno).disabled = true;
//            document.getElementById('taxPercentage' + sno).readOnly = false;
//        }
        if (value == '3') {//bill with freight
            //  document.getElementById('taxPercentage' + sno).value = '';
            document.getElementById('expenses' + sno).value = '';
            //alert("fgd");
            document.getElementById('billModeTemp' + sno).disabled = true;
            document.getElementById('taxPercentage' + sno).readOnly = true;
        }
//        else if (value == '4') {//reimbursable charge
//            document.getElementById('taxPercentage' + sno).value = '';
//            document.getElementById('expenses' + sno).value = '';
//            //alert("fgd");
//            document.getElementById('billModeTemp' + sno).disabled = true;
//            document.getElementById('taxPercentage' + sno).readOnly = false;
//        }
//        else { // do not bill to customer
//            document.getElementById('billMode' + sno).value = 0;
//            document.getElementById('billModeTemp' + sno).value = 0;
//            document.getElementById('taxPercentage' + sno).value = 0;
//            document.getElementById('taxPercentage' + sno).readOnly = true;
//            document.getElementById('billModeTemp' + sno).disabled = true;
//            document.getElementById('marginValue' + sno).value = 0;
////            document.getElementById('expenses' + sno).value = '0';
////            document.getElementById('netExpense' + sno).value = '0';
//        }
    }
    function checkCustValidation(value, sno) {

        var expenseType = document.getElementById('expenseType' + sno).value;
        var orderId = document.getElementById('orderId' + sno).value;
//        alert("expenseType="+expenseType)
        if (expenseType == 3 && orderId == 0) {
            alert("Please select customer name and order no ");
            document.getElementById('custId' + sno).focus();
            document.getElementById('expenseType' + sno).value = 0;
            return false;
        }

    }
    function setBillMode(sno) {
        document.getElementById('billMode' + sno).value = document.getElementById('billModeTemp' + sno).value;
        var billModeValue = document.getElementById('billModeTemp' + sno).value;
        document.getElementById('marginValue' + sno).value = 0;
        if (billModeValue == '2') {//Not pass thru
            document.getElementById('marginValue' + sno).readOnly = false;
        } else {
            document.getElementById('marginValue' + sno).readOnly = true;
        }
    }

    function calTotalExpenses(sno) {
//        alert(sno)
        var marginValue = document.getElementById('marginValue' + sno).value;
//        alert("marginValue="+marginValue)
        var tax = document.getElementById('taxPercentage' + sno).value;
//        alert("tax=="+tax)
        if (tax == '') {
            tax = '0';
        }
        if (marginValue == '') {
            marginValue = '0';
        }

        var expenseAmount = document.getElementById('expenses' + sno).value;
        var totalAmount = parseFloat(expenseAmount) + parseFloat(marginValue);
        var totalAmount = totalAmount + (totalAmount * parseFloat(tax) / 100);
        //                var netAmount =  Math.round(parseFloat(totalAmount).toFixed(0))  + Math.round(parseFloat(expenseAmount).toFixed(0))   ;
        //                document.getElementById('netExpense'+sno).value = netAmount;
        document.getElementById('netExpense' + sno).value = totalAmount.toFixed(2);
    }

    function totalNetAmount() {
        var tax = document.getElementsByNames("").value;
        var expenseAmount = document.getElementById('expenses').value;
        var totalAmount = tax / 100 * expenseAmount;
        var netAmount = Math.round(parseFloat(totalAmount).toFixed(0)) + Math.round(parseFloat(expenseAmount).toFixed(0));
        document.getElementById('netExpense').value = netAmount;
    }

    function openPopup1(tripExpenseId) {
        var url = '/throttle/viewExpensePODDetails.do?tripExpenseId=' + tripExpenseId;
        window.open(url, 'PopupPage', 'height=500,width=700,scrollbars=yes,resizable=yes');
    }

    function popUp(url) {
        var http = new XMLHttpRequest();
        http.open('HEAD', url, false);
        http.send();
        if (http.status != 404) {
            popupWindow = window.open(
                    url, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
        }
        else {
            var url1 = "/throttle/content/trip/fileNotFound.jsp";
            popupWindow = window.open(
                    url1, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
        }
    }



</script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.TripClosure" text="TripClosure"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.TripClosure" text="TripClosure "/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body"> 

            <body onload="saveOdoApprovalRequest();
                    addRow();
                    setEndTime();">
                <form name="tripExpense" method="post">

                    <br>


                    <%
                        boolean isSingleConsignmentOrder = false;
                        String revenueStr = "";
                    %>

                    <c:if test="${roleId == 1023 || roleId == 1013 || roleId == 1016}">
                        <% isSingleConsignmentOrder = true;%>
                    </c:if>   
                    <%if (isSingleConsignmentOrder){%>

                    <table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                        <tr id="exp_table">
                            <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                                <div class="tabs" align="left" style="width:300;">

                                    <div id="first">
                                        <c:if test = "${tripDetails != null}" >
                                            <c:forEach items="${tripDetails}" var="trip">
                                                <%--<c:out value="${trip.orderRevenue}" />--%>
                                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                                        <td> <c:out value="${trip.orderRevenue}" /></td>
                                                        <!--<td> <c:out value="${trip.editFreightAmount}" /></td>-->

                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Projected Expense:</b></font></td>
                                                        <td> <c:out value="${trip.orderExpense}" /></td>
                                                    <input type="hidden" name="estimatedAdvancePerDay" value='<c:out value="${trip.estimatedAdvancePerDay}" />'>
                                                    <input type="hidden" name="estimatedTransitDays" value='<c:out value="${trip.estimatedTransitDays}" />'>
                                                    <input type="hidden" name="estimatedTransitHours" value='<c:out value="${trip.estimatedTransitHours}" />'>

                                                    </tr>

                                                    <c:set var="profitMargin" value="" />
                                                    <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                                    <c:set var="editFreightAmount" value="${trip.editFreightAmount}" />
                                                    <c:set var="orderExpense" value="${trip.orderExpense}" />
                                                    <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                                                    <c:set var="bookingType" value="${orderTypeNew}" />
                                                    <%
                                                    String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                                    revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
                    //                                            String revenueStr = "" + (String)pageContext.getAttribute("editFreightAmount");
                                                    float profitPercentage = 0.00F;
                                                    if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                                        profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                                    }

                                                    %>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                                        <td>  <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)
                                                            <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>                                                        
                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Credit Limit:</b></font></td>
                                                        <td>  Rs. 1,00,00,000.00
                                                        <td>
                                                    </tr>
                                                </table>
                                            </c:forEach>
                                        </c:if>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <%}else {%>
                    <c:if test = "${tripDetails != null}" >
                        <c:forEach items="${tripDetails}" var="trip">

                            <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">

                                <input type="hidden" name="estimatedAdvancePerDay" value='<c:out value="${trip.estimatedAdvancePerDay}" />'>
                                <input type="hidden" name="estimatedTransitDays" value='<c:out value="${trip.estimatedTransitDays}" />'>
                                <input type="hidden" name="estimatedTransitHours" value='<c:out value="${trip.estimatedTransitHours}" />'>

                                <c:set var="profitMargin" value="" />
                                <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                <c:set var="editFreightAmount" value="${trip.editFreightAmount}" />
                                <c:set var="orderExpense" value="${trip.orderExpense}" />
                                <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                                <c:set var="bookingType" value="${orderTypeNew}" />
                                <%
                                String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
//                                            String revenueStr = "" + (String)pageContext.getAttribute("editFreightAmount");
                                float profitPercentage = 0.00F;
                                if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                    profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                }

                                %>
                                <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                            </c:forEach>
                        </c:if>

                        <%}%>

                        <input type="hidden" name="orderTypeNew" value='<c:out value="${orderTypeNew}" />'>
                        <input type="hidden" name="orderTypes" id="orderTypes" value='<c:out value="${orderType}" />'>
                        <input type="hidden" name="expenseStatus" id="expenseStatus" value='1' />

                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br> 
                        <table class="table table-info mb30 table-hover">
                            <% int loopCntr = 0;%>
                            <c:if test = "${tripDetails != null}" >
                                <c:forEach items="${tripDetails}" var="trip">
                                    <% if (loopCntr == 0) {%>
                                    <thead><tr>
                                            <th  >Vehicle: <c:out value="${trip.vehicleNo}" /></th>
                                            <th  >Trip Code: <c:out value="${trip.tripCode}"/></th>
                                            <th  >Customer:&nbsp;<c:out value="${trip.customerName}"/></th>
                                            <th  >Route: &nbsp;<c:out value="${trip.routeInfo}"/></th>
                                            <th  >Status: <c:out value="${trip.status}"/>
                                                <input type="hidden" name="tripCodeEmail" value='<c:out value="${trip.tripCode}"/>' />
                                                <input type="hidden" name="customerNameEmail" value='<c:out value="${trip.customerName}"/>' />
                                                <input type="hidden" name="routeInfoEmail" value='<c:out value="${trip.routeInfo}"/>' /></td>
                                                <input type="hidden" name="tripType" id="tripType" value='<c:out value="${tripType}"/>' /></td>
                                                <input type="hidden" name="statusId" id="statusId" value='<c:out value="${statusId}"/>' />
                                                <c:set var="cbt" value="${trip.cbt}"></c:set></th>
                                            </tr></thead>
                                        <% }%>
                                        <% loopCntr++;%>
                                    </c:forEach>
                                </c:if>
                        </table>
                        <div id="tabs" >
                            <ul>
                                <li><a href="#otherExpDetail" style="width: auto"><span>Other Expenses</span></a></li>
                                <li  data-toggle="tab"><a href="#CBTDetail"><span>Load Details</span></a></li>                            
                                <li><a href="#tripDetail"><span>Trip Details</span></a></li>                                                        
                                <li><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                                <li><a href="#advance"><span>Advance</span></a></li>
                                    <c:if test="${tripType == 1}">
                                    <li><a href="#podDetail"><span>Trip POD Details</span></a></li>
                                    </c:if>
                                <li><a href="#statusDetail"><span>Status History</span></a></li>
                            </ul>


                            <c:if test="${cbt == 1}">
                                <div id="CBTDetail" >
                                </c:if>    
                                <c:if test="${cbt != 1}">
                                    <div id="CBTDetail" style="display:none;" >
                                    </c:if>    
                                    <table style="display:none">
                                        <thead><tr>
                                                <th  colspan="8" >Load Details</th>
                                            </tr></thead>
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Consignment Order No</th>
                                                <th>Total Packages</th>
                                                <th>Pending Packages</th>
                                            </tr>
                                        </thead> 
                                        <tr>

                                            <c:if test = "${loadDetails != null}" >

                                            <tbody>
                                                <% int loadsno = 1;%>
                                                <c:forEach items="${loadDetails}" var="load">
                                                    <%
                                                        String classText = "";
                                                        int oddEven = loadsno % 2;
                                                        if (oddEven > 0) {
                                                            classText = "text2";
                                                        } else {
                                                            classText = "text1";
                                                        }
                                                    %>
                                                    <tr>
                                                        <td><%=loadsno++%></td>
                                                        <td><input type="hidden" name="loadOrderId" id="loadOrderId<%=loadsno%>" value='<c:out value="${load.consignmentOrderId}"/>'/> 
                                                            <c:out value="${load.consignmentOrderNo}"/></td>
                                                        <td><c:out value="${load.totalPackages}"/></td>                                                    
                                                        <td><c:out value="${load.pendingPackages}"/>
                                                            <input type="hidden" readonly name="pendingPkgs" id="pendingPkgs<%=loadsno%>" value='<c:out value="${load.pendingPackages}"/>' class='form-control' style='width: 200px;height: 40px'/>
                                                            <input type="hidden" readonly name="origPendingPkgs" id="origPendingPkgs<%=loadsno%>" value='<c:out value="${load.pendingPackages}"/>' class='form-control' style='width: 200px;height: 40px'/>
                                                        </td>
                                                <input type="hidden" readonly name="pendingWeight" id="pendingWeight<%=loadsno%>" value='<c:out value="${load.pendingWeight}"/>' class='form-control' style='width: 200px;height: 40px'/>
                                                <input type="hidden" readonly name="origPendingWeight" id="origPendingWeight<%=loadsno%>" value='<c:out value="${load.pendingWeight}"/>' class='form-control' style='width: 200px;height: 40px'/>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </c:if>
                                    </table>

                                    <c:if test = "${tripUnPackDetails != null}" >
                                        <table class="table table-info mb30 table-hover" id="bg" >
                                            <thead><tr>
                                                    <th colspan="10"  align="center" height="30" >Consignment Unloading Details</th>
                                                </tr>
                                                <tr>
                                                    <th width="20"  align="center" height="30" >Sno</th>
                                                    <th  height="30" >No of Packages</th>
                                                    <th  height="30" >Uom</th>
                                                    <th  height="30" >Loaded Package Nos</th>
                                                    <th  height="30" >Shortage</th>
                                                    <th  height="30" >Damage</th>                                                
                                                </tr></thead>

                                            <%int i = 1;%>
                                            <c:forEach items="${tripUnPackDetails}" var="tripunpack">
                                                <tr>
                                                    <td><%=i%></td>
                                                    <td><input type="hidden" name="productNames" id="productNames" value="0" readonly/>
                                                        <input type="hidden" name="productbatch" id="productbatch" value="0" readonly/>
                                                        <input type="hidden" name="packagesNosNew" id="packagesNos" value="<c:out value="${tripunpack.packageNos}"/>" readonly class='form-control' style='width: 200px;height: 40px'/>
                                                        <c:out value="${tripunpack.packageNos}"/>
                                                    </td>
                                                    <td>
                                                        <input type="hidden" name="productuom" id="productuom" value="<c:out value="${tripunpack.uom}"/>" readonly class='form-control' style='width: 200px;height: 40px'/>
                                                        <input type="hidden" name="weights" id="weights" value="0" readonly/>
                                                        <c:out value="${tripunpack.uom}"/>
                                                    </td>
                                                    <td><c:out value="${tripunpack.loadpackageNos}"/>
                                                        <input type="hidden" name="loadedpackagesNew" id="loadedpackagesNew<%=i%>" value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly class='form-control' style='width: 200px;height: 40px'/>
                                                        <input type="hidden" name="consignmentId" value="<c:out value="${tripunpack.consignmentId}"/>" class='form-control' style='width: 200px;height: 40px' />
                                                        <input type="hidden" name="tripArticleId" value="<c:out value="${tripunpack.tripArticleid}"/>" class='form-control' style='width: 200px;height: 40px'/>
                                                    </td>
                                                    <td><c:out value="${tripunpack.shortageNew}"/><input type="hidden" name="shortageNew" id="shortageNew<%=i%>"  value="<c:out value="${tripunpack.shortageNew}"/>" readonly class='form-control' style='width: 200px;height: 40px' /></td>
                                                    <td><c:out value="${tripunpack.damageNew}"/><input type="hidden" name="damageNew" id="damageNew<%=i%>"  value="<c:out value="${tripunpack.damageNew}"/>" readonly class='form-control' style='width: 200px;height: 40px' /></td>
                                                <input type="hidden" name="unloadedpackagesNew" id="unloadedpackagesNew<%=i%>" readonly onblur="computeShortageNew(<%=i%>);"  value="<c:out value="${tripunpack.pendingPackages}"/>" class='form-control' style='width: 200px;height: 40px' onKeyPress="return onKeyPressBlockCharacters(event);"    />
                                                <input type="hidden" name="detentionHours" id="detentionHours<%=i%>"  value="0"  class='form-control' style='width: 200px;height: 40px' />
                                                <input type="hidden" name="detentionCharge" id="detentionCharge<%=i%>"  value="0"  class='form-control' style='width: 200px;height: 40px' />
                                                </tr>
                                                <%i++;%>
                                            </c:forEach>

                                        </table>
                                    </c:if>


                                </div>
                                <div id="otherExpDetail" style="width: auto">
                                    <div style="border: #ffffff solid" >
                                        <%int count = 0;%>
                                        <input type="hidden" name="count" id="count" value="<c:out value="${otherExpenseDetailsSize}"/>"/>
                                        <c:if test="${otherExpenseDetails != null}">                                        
                                            <table class="table table-info mb30 table-hover" id="bg" style="width:98%">
                                                <thead> <tr>
                                                        <th class="text1" width="50" >S No&nbsp;</th>
                                                        <th class="text1">Customer Name</th>
                                                        <th class="text1">Order List</th>
                                                        <th class="text1">Expense Name</th>
                                                        <th class="text1">Expense Date</th>
                                                        <th class="text1">Expense Remarks</th>
                                                        <th class="text1">Expense Type</th>
                                                        <th class="text1">Expense Amount</th>
                                                        <th class="text1">Total Expenses</th>
                                                    </tr></thead>

                                                <% int index5 = 1;%>
                                                <c:forEach items="${otherExpenseDetails}" var="expenseDetails">
                                                    <%
                                                                String classText5 = "";
                                                                int oddEven3 = index5 % 2;
                                                                if (oddEven3 > 0) {
                                                                    classText5 = "text1";
                                                                } else {
                                                                    classText5 = "text2";
                                                                }
                                                    %>
                                                    <%count++;%>
                                                    <tr>
                                                        <td  class="<%=classText5%>"><%=index5++%></td>
                                                        <td  class="<%=classText5%>">
                                                            <c:if test = "${expenseDetails.customerId == '0'}" >
                                                                Select
                                                            </c:if>
                                                            <c:if test = "${expenseDetails.customerId != '0'}" >
                                                                <c:out value="${expenseDetails.customerName}"/>&nbsp;
                                                            </c:if>

                                                        </td>
                                                        <td class="<%=classText5%>" >
                                                            <c:if test = "${expenseDetails.orderId == '0'}" >
                                                                Select
                                                            </c:if>
                                                            <c:if test = "${expenseDetails.orderId != '0'}" >
                                                                <c:out value="${expenseDetails.orderNo}"/>&nbsp;
                                                            </c:if>

                                                        </td>
                                                        <td class="<%=classText5%>"  ><c:out value="${expenseDetails.expenseName}"/>&nbsp;</td>

                                                        <td class="<%=classText5%>" ><c:out value="${expenseDetails.expenseDate}"/>&nbsp;<c:out value="${expenseDetails.expenseHour}"/>:<c:out value="${expenseDetails.expenseMinute}"/>&nbsp</td>
                                                        <td class="<%=classText5%>" ><c:out value="${expenseDetails.expenseRemarks}"/>&nbsp;</td>
                                                        <c:if test = "${expenseDetails.expenseType == '1'}" >
                                                            <td class="<%=classText5%>" >charge to customer </td>
                                                        </c:if>

                                                        <c:if test = "${expenseDetails.expenseType == '2'}" >
                                                            <td  class="<%=classText5%>">Do Not Bill To Customer </td>
                                                        </c:if>
                                                        <c:if test = "${expenseDetails.expenseType == '3'}" >
                                                            <td class="<%=classText5%>" >Bill with Freight </td>
                                                        </c:if>

                                                        <c:if test = "${expenseDetails.expenseType == '4'}" >
                                                            <td class="<%=classText5%>" >Reimbursable charge </td>
                                                        </c:if>
                                                        <c:if test = "${expenseDetails.expenseType == '6'}" >
                                                            <td class="<%=classText5%>" >TSP </td>
                                                        </c:if>
                                                        <c:if test = "${expenseDetails.expenseType == '7'}" >
                                                            <td  >Royalty </td>
                                                        </c:if>
                                                        <c:if test = "${expenseDetails.expenseType == '0'}" >
                                                            <td  class="<%=classText5%>">&nbsp; </td>
                                                        </c:if>

                                                        <td class="<%=classText5%>" >
                                                            <c:if test="${expenseDetails.expenseValue != null}">
                                                                <c:out value="${expenseDetails.expenseValue}"/>
                                                            </c:if>
                                                            <c:if test="${expenseDetails.expenseValue == null}">
                                                                0
                                                            </c:if>
                                                        </td>
                                                        <td class="<%=classText5%>" >
                                                            <c:if test="${expenseDetails.totalExpenseAmount != null}">
                                                                <c:out value="${expenseDetails.totalExpenseAmount}"/>
                                                            </c:if>
                                                            <c:if test="${expenseDetails.totalExpenseAmount == null}">
                                                                0
                                                            </c:if>
                                                        </td>

                                                    <!--<td  class="<%=classText5%>" height="30"> <c:if test="${expenseDetails.documentRequired=='1'}">Yes </c:if> <c:if test="${expenseDetails.documentRequired=='0'}">No</c:if> </td>-->

                                                            <input type="hidden" name="expenseId" id="expenseId<%=count%>" value="<c:out value="${expenseDetails.tripExpenseId}"/>"/>
                                                    <input type="hidden" name="customerNames" id="customerNames<%=count%>" value="<c:out value="${expenseDetails.customerId}"/>"/>
                                                    <input type="hidden" name="orderIds" id="orderIds<%=count%>" value="<c:out value="${expenseDetails.orderId}"/>"/>
                                                    <input type="hidden" name="employeeNames" id="employeeNames<%=count%>" value="<c:out value="${expenseDetails.employeeId}"/>"/>
                                                    <input type="hidden" name="expenseNames" id="expenseNames<%=count%>" value="<c:out value="${expenseDetails.expenseId}"/>"/>
                                                    <input type="hidden" name="expenseDates" id="expenseDates<%=count%>" value="<c:out value="${expenseDetails.expenseDate}"/>"/>
                                                    <input type="hidden" name="expenseHours" id="expenseHours<%=count%>" value="<c:out value="${expenseDetails.expenseHour}"/>"/>
                                                    <input type="hidden" name="expenseMinutes" id="expenseMinutes<%=count%>" value="<c:out value="${expenseDetails.expenseMinute}"/>"/>
                                                    <input type="hidden" name="expenseRemarkss" id="expenseRemarkss<%=count%>" value="<c:out value="${expenseDetails.expenseRemarks}"/>"/>
                                                    <input type="hidden" name="expenseTypes" id="expenseTypes<%=count%>" value="<c:out value="${expenseDetails.expenseType}"/>"/>
                                                    <input type="hidden" name="expenseLocations" id="expenseLocations<%=count%>" value="<c:out value="${expenseDetails.expenseLocation}"/>"/>
                                                    <input type="hidden" name="passThroughStatuss" id="passThroughStatuss<%=count%>" value="<c:out value="${expenseDetails.passThroughStatus}"/>"/>
                                                    <input type="hidden" name="taxPercentages" id="taxPercentages<%=count%>" value="<c:out value="${expenseDetails.taxPercentage}"/>"/>
                                                    <input type="hidden" name="marginValues" id="marginValues<%=count%>" value="<c:out value="${expenseDetails.marginValue}"/>"/>
                                                    <input type="hidden" name="expenseValues" id="expenseValues<%=count%>" value="<c:out value="${expenseDetails.expenseValue}"/>"/></td>
                                                    <input type="hidden" name="totalExpenseAmounts" id="totalExpenseAmounts<%=count%>" value="<c:out value="${expenseDetails.totalExpenseAmount}"/>"/></td>
                                                    </tr>
                                                </c:forEach>

                                            </table>
                                        </c:if>
                                        <br>
                                        <script>
                                            function modifyExpenseDetails(sno) {
                                                //                                        alert("hi:"+sno);
                                                var count = parseInt(document.getElementById("count").value);
                                                //                                        alert(count);
                                                var sno1 = 0;
                                                for (var i = 1; i <= count; i++) {
                                                    if (i != sno) {
                                                        document.getElementById("edit" + i).checked = false;
                                                    } else {
                                                        document.getElementById("edit" + i).checked = true;
                                                        getCustOrders(document.getElementById("customerNames" + i).value, sno1, document.getElementById("orderIds" + i).value);
                                                        document.getElementById('editId' + sno1).value = document.getElementById("expenseId" + i).value;

                                                        document.getElementById('custId' + sno1).value = document.getElementById("customerNames" + i).value;

                                                        document.getElementById('expenseName' + sno1).value = document.getElementById("expenseNames" + i).value;
                                                        document.getElementById('employeeName' + sno1).value = document.getElementById("employeeNames" + i).value;
                                                        document.getElementById('expenseDate' + sno1).value = document.getElementById("expenseDates" + i).value;
                                                        document.getElementById('expenseHour' + sno1).value = document.getElementById("expenseHours" + i).value;
                                                        document.getElementById('expenseMinute' + sno1).value = document.getElementById("expenseMinutes" + i).value;
                                                        document.getElementById('expenseRemarks' + sno1).value = document.getElementById("expenseRemarkss" + i).value;
                                                        document.getElementById('expenseType' + sno1).value = document.getElementById("expenseTypes" + i).value;
                                                        document.getElementById('expenseLocation' + sno1).value = document.getElementById("expenseLocations" + i).value;
                                                        document.getElementById('billModeTemp' + sno1).value = document.getElementById("passThroughStatuss" + i).value;
                                                        document.getElementById('taxPercentage' + sno1).value = document.getElementById("taxPercentages" + i).value;
                                                        document.getElementById('marginValue' + sno1).value = document.getElementById("marginValues" + i).value;
                                                        document.getElementById('expenses' + sno1).value = document.getElementById("expenseValues" + i).value;
                                                        document.getElementById('netExpense' + sno1).value = document.getElementById("totalExpenseAmounts" + i).value;
                                                        //                                                        alert("orderId"+document.getElementById("orderIds" + i).value)
                                                        //                                                        alert(sno1);
                                                        //                                                        var optionValues = [];
                                                        //
                                                        //                                                        $('#orderId0 option').each(function() {
                                                        //                                                            optionValues.push($(this).val());
                                                        //                                                        });
                                                        //
                                                        //                                                        alert(optionValues);
                                                        document.getElementById('orderId' + sno1).value = document.getElementById("orderIds" + i).value;
                                                    }
                                                }
                                            }
                                        </script>
                                        <table class="table table-info mb30 table-hover"  id="suppExpenseTBL" style="width:98%">
                                            <thead><tr >
                                                    <th class="text2" width="50" >S No&nbsp;</th>
                                                    <th class="text2"><font color='red'>*</font>Customer Name</th>
                                                    <th class="text2"><font color='red'>*</font>Order List</th>
                                                    <th class="text2"><font color='red'>*</font>Expense Name</th>                                                
                                                    <th class="text2"><font color='red'>*</font>Expense Date</th>
                                                    <th class="text2">Expense Remarks</th>
                                                    <th class="text2"><font color='red'>*</font>Expense Type</th>
                                                    <th class="text2"><font color='red'>*</font>Expense Value</th>
                                                    <th class="text2">Total Expenses</th>
                                                </tr></thead>
                                            <tr>
                                                <td align="center" colspan="11">
                                                    <input type="hidden" class="text" name="tripExpense" id="tripExpense" value="0" >
                                                    <input type="hidden" class="text" name="admin" value="<c:out value="${admin}"/>" >
                                                    &emsp; <input type="button" class="btn btn-success" name="add" value="Add Row" onclick="addRow();"/>
                                                    &emsp; <input type="button" class="btn btn-success" value="Save" id="SaveExp" name="Save expenses" onclick="submitPageExpense();"/>                                                
                                                </td>
                                            </tr>
                                        </table>

                                        <%
                                    Float estimatedExpense = 0.00F;
                                    if (request.getAttribute("estimatedExpense") != null) {
                                        estimatedExpense = Float.parseFloat((String) request.getAttribute("estimatedExpense"));
                                    }

                                        %>

                                        <%
                                                                        String tripType = (String) request.getAttribute("tripType");
                                                                        Float secTollCost = 0.00F;
                                                                        Float secAddlTollCost = 0.00F;
                                                                        Float secMiscCost = 0.00F;
                                                                        Float secParkingAmount = 0.00F;
                                                                        if (request.getAttribute("secTollAmount") != null && !"".equals(request.getAttribute("secTollAmount"))) {
                                                                            secTollCost = Float.parseFloat((String) request.getAttribute("secTollAmount"));
                                                                            secAddlTollCost = Float.parseFloat((String) request.getAttribute("secAddlTollAmount"));
                                                                            secMiscCost = Float.parseFloat((String) request.getAttribute("secMiscAmount"));
                                                                            secParkingAmount = Float.parseFloat((String) request.getAttribute("secParkingAmount"));
                                                                        }

                                                                        Float tollRate = 0.00F;
                                                                        Float driverBattaPerDay = 0.00F;
                                                                        Float driverIncentivePerKm = 0.00F;
                                                                        Float fuelPrice = 0.00F;
                                                                        Float milleage = 0.00F;
                                                                        Float reeferMileage = 0.00F;

                                                                        if (request.getAttribute("tollAmount") != null && !"".equals(request.getAttribute("tollAmount"))) {
                                                                            tollRate = Float.parseFloat((String) request.getAttribute("tollAmount"));
                                                                            driverBattaPerDay = Float.parseFloat((String) request.getAttribute("driverBatta"));
                                                                            driverIncentivePerKm = Float.parseFloat((String) request.getAttribute("driverIncentive"));
                                                                        }
                                                                        if (request.getAttribute("fuelPrice") != null && !"".equals(request.getAttribute("fuelPrice"))) {
                                                                            fuelPrice = Float.parseFloat((String) request.getAttribute("fuelPrice"));
                                                                            reeferMileage = Float.parseFloat((String) request.getAttribute("reeferConsumption"));
                                                                        }
                                                                            milleage = Float.parseFloat((String) request.getAttribute("milleage"));

                                                                        Float runKm = Float.parseFloat((String) request.getAttribute("runKm"));
                                                                        Float runHm = Float.parseFloat((String) request.getAttribute("runHm"));

                                                                        /*
                                                                         */
                                                                        String totalDaysValue = (String) request.getAttribute("totaldays");
                                                                        int gpsKm = Integer.parseInt((String) request.getAttribute("gpsKm"));
                                                                        int gpsHm = Integer.parseInt((String) request.getAttribute("gpsHm"));
                                                                        Float totalDays;
                                                                        if (totalDaysValue != null) {
                                                                            totalDays = Float.parseFloat((String) request.getAttribute("totaldays"));
                                                                        } else {
                                                                            totalDays = Float.parseFloat("0.00");
                                                                        }
                                                                        Float bookedExpense = Float.parseFloat((String) request.getAttribute("bookedExpense"));
                                                                        Float miscRate = Float.parseFloat((String) request.getAttribute("miscValue"));
                                                                        int driverCount = 0;
                                                                        if (request.getAttribute("driverCount") != null) {
                                                                            driverCount = Integer.parseInt((String) request.getAttribute("driverCount"));
                                                                        }
                                                                        //out.println("driverCount"+driverCount);
                                                                        Float extraExpenseValue = 0.00F;
                                                                        extraExpenseValue = Float.parseFloat((String) request.getAttribute("extraExpenseValue"));
                                                                        Float dieselUsed = (runKm / milleage) + (runHm * reeferMileage);
                                                                        System.out.println("reeferMileage" + reeferMileage);
                                                                        System.out.println("milleage" + milleage);
                                                                        System.out.println("runKm" + runKm);
                                                                        System.out.println("runHm" + runHm);
                                                                        Float driverBatta = totalDays * driverBattaPerDay * driverCount;
                                                                        Float miscValue = ((runKm * miscRate) + extraExpenseValue + driverBatta) * 5 / 100;
                                                                        Float dieselCost = dieselUsed * fuelPrice;
                                                                        Float tollCost = runKm * tollRate;
                                                                        Float driverIncentive = runKm * driverIncentivePerKm;
                                                                        //out.println("tollRate"+tollRate);

                                                                        Float systemExpense = dieselCost + tollCost + driverIncentive + driverBatta + miscValue;
                                                                        String fuelTypeId = (String) request.getAttribute("fuelTypeId");
                                                                        Float preColingAmount = 1.5f;
                                                                        Float fuelUsed = 0.00F;
                                                                        Float fuelCost = 0.00F;
                                                                        if ("2".equals(tripType)) {//secondary
                                                                            fuelUsed = (runKm / milleage);
                                                                            fuelCost = fuelUsed * fuelPrice;
                                                                            if (fuelTypeId.equals("1003")) {
                                                                                systemExpense = fuelCost + secTollCost + secAddlTollCost + secMiscCost + secParkingAmount + (fuelPrice * preColingAmount);
                                                                                preColingAmount = preColingAmount * fuelPrice;
                                                                            } else {
                                                                                systemExpense = fuelCost + secTollCost + secAddlTollCost + secMiscCost + secParkingAmount;
                                                                                preColingAmount = 0.0f;
                                                                            }
                                                                        }

                                                                        Float nettExpense = 0.00F;
                                                                        Float estActualExpDiff = estimatedExpense - nettExpense;
                                                                        int tripCount = (Integer) request.getAttribute("tripCnt");
                                        %>

                                        <br/>
                                        <table align="center" class="table table-info mb30 table-hover"  style="width:98%">

                                            <tr>
                                                <td align="right">Trip Expense </td>
                                                <td > <input type="text" id="estimatedExpense" name="estimatedExpense" class="text" style='width:120px;height:40px' value='<%=new DecimalFormat("#0.00").format(estimatedExpense)%>' /></td>
                                            </tr>

                                            <tr >
                                                <td colspan="2" align="center"><center><input type="button" value="Close Trip" class="btn btn-success" onclick="closeTrip();"></center> </td>
                                            </tr>
                                        </table>

                                        <br/>
                                    </div>
                                    <script>
                                        var httpRequest1;
                                        function getCustOrders(custId, sno, orderId) {
                                            //                                        alert("custId="+custId);
                                            //                                        alert("sno="+sno);
                                            if (custId != 'null') {
                                                var list1 = eval("document.tripExpense.orderId" + sno);
                                                while (list1.childNodes[0]) {
                                                    list1.removeChild(list1.childNodes[0])
                                                }
                                                var tripSheetId = document.tripExpense.tripSheetId.value;
                                                //                                            alert("tripSheetId=" + tripSheetId);
                                                var url = '/throttle/getOrderList.do?custId=' + custId + '&tripId=' + tripSheetId;

                                                if (window.ActiveXObject)
                                                {
                                                    httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
                                                }
                                                else if (window.XMLHttpRequest)
                                                {
                                                    httpRequest1 = new XMLHttpRequest();
                                                }
                                                httpRequest1.open("POST", url, true);
                                                httpRequest1.onreadystatechange = function() {
                                                    go(sno, orderId);
                                                };
                                                httpRequest1.send(null);
                                            }
                                        }

                                        function go(sno, orderIdToSet) {
                                            if (httpRequest1.readyState == 4) {
                                                if (httpRequest1.status == 200) {
                                                    var response = httpRequest1.responseText;
                                                    var list = eval("document.tripExpense.orderId" + sno);
                                                    var details = response.split(',');
                                                    var orderId = 0;
                                                    var orderName = '<spring:message code="head.label.--Select--" text="--Select--"/>';
                                                    var x = document.createElement('option');
                                                    var name = document.createTextNode(orderName);
                                                    x.appendChild(name);
                                                    x.setAttribute('value', orderId)
                                                    list.appendChild(x);
                                                    //                                                alert(details.length);
                                                    for (i = 1; i < details.length; i++) {
                                                        temp = details[i].split('*');
                                                        orderId = temp[0];
                                                        orderName = temp[1];
                                                        x = document.createElement('option');
                                                        name = document.createTextNode(orderName);
                                                        x.appendChild(name);
                                                        x.setAttribute('value', orderId);
                                                        //alert(orderIdToSet +"...."+ orderId);
                                                        if (orderIdToSet != 0 && orderIdToSet == orderId) {
                                                            x.setAttribute('selected', true);
                                                        }
                                                        list.appendChild(x);
                                                    }
                                                }
                                            }


                                        }

                                    </script>

                                    <script>


                                        var rowCount = 1;
                                        var sno = 0;
                                        var rowCount1 = 1;
                                        var sno1 = 0;
                                        var httpRequest;
                                        var httpReq;
                                        var styl = "";
                                        function addRow() {
                                            if (parseInt(rowCount1) % 2 == 0)
                                            {
                                                styl = "text2";
                                            } else {
                                                styl = "text1";
                                            }

                                            sno1++;
                                            var tab = document.getElementById("suppExpenseTBL");
                                            //find current no of rows
                                            var rowCountNew = document.getElementById('suppExpenseTBL').rows.length;
                                            rowCountNew--;
                                            var newrow = tab.insertRow(rowCountNew);
                                            cell = newrow.insertCell(0);
                                            var cell0 = "<td class='text1' height='25' style='width:10px;'> <input type='hidden' name='editId' id='editId" + sno + "' value=''/>" + sno1 + "</td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(1);
                                            cell0 = "<td class='text1' height='25'><select class='form-control' style='width:120px;height:40px' style='width:90px' id='custId" + sno + "'   name='custId' onchange='getCustOrders(this.value," + sno + ",0)'><option selected value='0'>--Select--</option> <c:if test="${customerLists != null}" ><c:forEach items="${customerLists}" var="customer"><option  value='<c:out value="${customer.customerId}" />'><c:out value="${customer.customerName}" /></c:forEach > </c:if> </select></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(2);
                                            cell0 = "<td class='text1' height='25'><select style='width:120px;height:40px;'  class='form-control' id='orderId" + sno + "'   name='orderId' ><option  selected value='0'>--Select--</option></select></td>";
                                            //                                        cell0 = "<td class='text1' height='25'><select style='width:180px;height:40px;'  class='form-control' id='orderId" + sno + "'   name='orderId'><option  selected value='0'>Common</option></select></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(3);
                                            cell0 = "<td class='text1' height='25'><input type='hidden'  id='employeeName" + sno + "'  name='employeeName' value='0' /><select class='form-control' style='width:120px;height:40px' style='width:90px' id='expenseName" + sno + "'   name='expenseName' onchange='setExpenseType(this.value," + sno + ")'><option selected value=0>---Select---</option> <c:if test="${expenseDetails != null}" ><c:forEach items="${expenseDetails}" var="expense"><option  value='<c:out value="${expense.expenseId}" />'><c:out value="${expense.expenseName}" /> </c:forEach > </c:if> </select></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(4);
                                            var cell0 = "<td class='text1' height='25' ><input type='text' value=''   name='expenseDate'  id='expenseDate" + sno + "'   class='datepicker'  style='width:120px;height:40px' ><input type='hidden' name='expenseHour'  id='expenseHour" + sno + "'   value='00'/><input type='hidden' name='expenseMinute'  id='expenseMinute" + sno + "'   value='00'/></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            $('.datepicker').datepicker();

                                            cell = newrow.insertCell(5);
                                            cell0 = "<td class='text1' height='25' >\n\
                                            <input type='hidden' class='form-control' style='width:120px;height:40px' id='expenseLocation" + sno + "'  value='0' name='expenseLocation' style='width:90px' >                                    \n\
                                            <textarea rows='3' cols='20' class='form-control' style='width:120px;height:40px' name='expenseRemarks' id='expenseRemarks" + sno + "'></textarea></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(6);
                                            var cell0 = "<td class='text1' height='25' ><select class='form-control' style='width:120px;height:40px' id='expenseType" + sno + "'   name='expenseType' style='width:90px' onchange='setExpenseTypeDetails(this.value," + sno + ");checkCustValidation(this.value," + sno + ");'><option value='0'>-Select-</option><option value='1'> Charge to customer</option><option value='2'>Do Not Bill Customer</option><c:if test="${orderTypeNew == '3'}" ><option value='6'>TSP</option><option value='7'>Royalty Charge</option></c:if>></select></td>";
                                            cell.setAttribute("className", "text1");
                                            cell.innerHTML = cell0;

                                            //                                        cell = newrow.insertCell(10);
                                            //                                        var cell0 = "<td style='display:none'><input type='hidden'  name='billMode' id='billMode" + sno + "' value = '0' ><select disabled class='form-control' style='width:120px;height:40px' id='billModeTemp" + sno + "'   name='billModeTemp' onchange='setBillMode(" + sno + ")' style='width:90px'><option value='0'>-Select-</option><option value='1'>Pass Through</option><option value='2'>No Pass Through</option></select>\n\
                                            //                                        <input type='hidden'  name='marginValue' id='marginValue" + sno + "'  value = '0'  onchange='calTotalExpenses(" + sno + ");' style='width:60px' onKeyPress='return onKeyPressBlockCharacters(event);' class='form-control' style='width:120px;height:40px'>\n\
                                            //                                        <input type='hidden'  name='taxPercentage' id='taxPercentage" + sno + "'  value = '0' onchange='calTotalExpenses(" + sno + ");' style='width:60px'  onKeyPress='return onKeyPressBlockCharacters(event);' class='form-control' style='width:120px;height:40px'  ></td>";
                                            //                                        cell.setAttribute("className", "text1");
                                            //                                        cell.innerHTML = cell0;
                                            //                            cell = newrow.insertCell(8);
                                            //                            var cell0 = "<td style='display:none'><input type='hidden'  name='marginValue' id='marginValue" + sno + "'  value = '0'  onchange='calTotalExpenses(" + sno + ");' style='width:60px' onKeyPress='return onKeyPressBlockCharacters(event);' class='form-control' style='width:120px;height:40px'  ></td>";
                                            //                            cell.setAttribute("className", styl);
                                            //                            cell.innerHTML = cell0;
                                            //                            cell = newrow.insertCell(9);
                                            //                            var cell0 = "<td style='display:none'><input type='hidden'  name='taxPercentage' id='taxPercentage" + sno + "'  value = '0' onchange='calTotalExpenses(" + sno + ");' style='width:60px'  onKeyPress='return onKeyPressBlockCharacters(event);' class='form-control' style='width:120px;height:40px'  ></td>";
                                            //                            cell.setAttribute("className", styl);
                                            //                            cell.innerHTML = cell0;
                                            cell = newrow.insertCell(7);
                                            var cell0 = "<td class='text1' height='10' >\n\
                                            <input type='hidden'  name='billMode' id='billMode" + sno + "' value = '0' >                                        \n\
                                            <input type='hidden'  name='taxPercentage' id='taxPercentage" + sno + "'  value = '0' onchange='calTotalExpenses(" + sno + ");' style='width:60px'  onKeyPress='return onKeyPressBlockCharacters(event);' class='form-control' style='width:120px;height:40px'  >\n\
                                            <input type='hidden'  name='marginValue' id='marginValue" + sno + "'  value = '0'  onchange='calTotalExpenses(" + sno + ");' style='width:60px' onKeyPress='return onKeyPressBlockCharacters(event);' class='form-control' style='width:120px;height:40px'>                                    \n\
                                            <input type='text'  name='expenses' id='expenses" + sno + "' class='form-control' style='width:120px;height:40px'  value=''  onKeyPress='return onKeyPressBlockCharacters(event);' onchange='calTotalExpenses(" + sno + ")'><input  type='hidden' name='activeValue' id='activeValue" + sno + "' value='0' ><input  type='hidden' name='documentRequired' id='documentRequired" + sno + "'  value='0' >";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            cell = newrow.insertCell(8);
                                            var cell0 = "<td class='text1' height='25' ><input readonly type='text' name='netExpense' id='netExpense" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' class='form-control' style='width:120px;height:40px'  value='' ></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;



                                            if (rowCountNew > 1) {
                                                cell = newrow.insertCell(9);
                                                var cell0 = "<td class='text1' height='25' align='left'><input type='button' class='btn btn-info'  style='width:90px;height:30px;font-weight: bold;padding: 1px;'  name='delete'  id='delete" + sno + "'   value='Delete Row' onclick='deleteRow(this);' ></td>";
                                                cell.setAttribute("className", styl);
                                                cell.innerHTML = cell0;
                                            }
                                            $(document).ready(function() {
                                                $("#datepicker").datepicker({
                                                    showOn: "button",
                                                    buttonImage: "calendar.gif",
                                                    buttonImageOnly: true

                                                });
                                            });
                                            $(function() {
                                                $(".datepicker").datepicker({
                                                    changeMonth: true, changeYear: true
                                                });
                                            });
                                            rowCount1++;
                                            sno++;
                                        }


                                        </script>

                                        <script>
                                            function deleteRow(src) {
                                                sno--;
                                                var oRow = src.parentNode.parentNode;
                                                var dRow = document.getElementById('suppExpenseTBL');
                                                dRow.deleteRow(oRow.rowIndex);
                                                document.getElementById("selectedRowCount").value--;
                                            }

                                        </script>
                                        <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="1"/>

                                        <script type="text/javascript">
                                            function setActiveValues(sno) {
                                                if (document.getElementById('documentRequired' + sno).checked) {

                                                    document.getElementById('activeValue' + sno).value = "1";
                                                } else {

                                                    document.getElementById('activeValue' + sno).value = "0";
                                                }
                                            }
                                        </script>

                                    </div>

                                    <div id="tripDetail">
                                        <table  class="table table-info mb30 table-hover" id="bg">
                                            <thead><tr>
                                                    <th colspan="6" >Trip Details</th>
                                                </tr></thead>

                                        <c:if test = "${tripDetails != null}" >
                                            <c:forEach items="${tripDetails}" var="trip">


                                                <tr>
                                                    <!--                            <td ><font color="red">*</font>Trip Sheet Date</td>
                                                                                <td ><input type="text" name="tripDate" class="datepicker , form-control" style="width:250px;height:40px" value=""></td>-->
                                                    <td class="classText1" >CNote No(s)</td>
                                                    <td class="classText1">
                                                        <c:out value="${trip.cNotes}" />
                                                        <input type="hidden" name="cNotesEmail" value='<c:out value="${trip.cNotes}"/>' />
                                                    </td>
                                                    <td class="classText1">Billing Type</td>
                                                    <td class="classText1">
                                                        <c:out value="${trip.billingType}" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <!--                            <td >Customer Code</td>
                                                                                <td >BF00001</td>-->
                                                    <td class="classText1">Customer Name</td>
                                                    <td class="classText1">
                                                        <c:out value="${trip.customerName}" />
                                                        <input type="hidden" name="customerName" Id="customerName" class="form-control" style="width:250px;height:40px" value='<c:out value="${trip.customerName}" />'>
                                                        <input type="hidden" name="tripSheetId" Id="tripSheetId" class="form-control" style="width:250px;height:40px" value='<c:out value="${trip.tripId}" />'>
                                                    </td>
                                                    <td class="classText1">Customer Type</td>
                                                    <td class="classText1" colspan="3" >
                                                        <c:out value="${trip.customerType}" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="classText1">Route Name</td>
                                                    <td class="classText1">
                                                        <c:out value="${trip.routeInfo}" />
                                                    </td>
                                                    <td class="classText1">Trip Schedule</td>
                                                    <td class="classText1"><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" /> </td>

                                                </tr>
                                                <tr>
                                                    <td class="classText1">Vehicle Type</td>
                                                    <td class="classText1">
                                                        <c:out value="${trip.vehicleTypeName}" />
                                                    </td>
                                                    <td class="classText1">Vehicle No</td>
                                                    <td class="classText1">
                                                        <c:out value="${trip.vehicleNo}" />
                                                        <input type="hidden" name="vehicleNoEmail" value='<c:out value="${trip.vehicleNo}"/>' />

                                                    </td>

                                                </tr>


                                            </c:forEach>
                                        </c:if>
                                    </table>

                                    <br>

                                    <c:if test="${vehicleChangedTripDetails != null}">
                                        <table class="table table-info mb30 table-hover" >
                                            <thead><tr>
                                                    <th  colspan="4" >Vehicle Change Details</th>
                                                </tr>
                                            </thead>
                                            <% int index2 = 1;%>
                                            <c:forEach items="${vehicleChangedTripDetails}" var="vehicleChange">
                                                <tr>
                                                    <td>Changed Vehicle No</td>
                                                    <td><c:out value="${vehicleChange.regNo}"/></td>
                                                    <td>Vehicle Changed Date</td>
                                                    <td><c:out value="${vehicleChange.vehicleChangeDate}"/></td>
                                                </tr>
                                                <tr>
                                                    <td>Changed Place</td>
                                                    <td><c:out value="${vehicleChange.cityName}"/></td>
                                                    <td>Eway Bill Changed Date</td>
                                                    <td><c:out value="${vehicleChange.ewayBillDate}"/></td>
                                                </tr>
                                                <tr>
                                                    <td>Insurance Survey</td>
                                                    <td><c:out value="${vehicleChange.insuranceSurvey}"/></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </c:forEach>

                                        </table>
                                    </c:if>

                                    <c:if test = "${expiryDateDetails != null}" >
                                        <table  class="table table-info mb30 table-hover" id="bg" style="display: none">
                                            <thead> <tr>
                                                    <th  colspan="4" >Vehicle Compliance Check</th>
                                                </tr></thead>
                                                <c:forEach items="${expiryDateDetails}" var="expiryDate">
                                                <tr>
                                                    <td >Vehicle FC Valid UpTo</td>
                                                    <td ><label><font color="green"><c:out value="${expiryDate.fcExpiryDate}" /></font></label></td>
                                                </tr>
                                                <tr>
                                                    <td >Vehicle Insurance Valid UpTo</td>
                                                    <td ><label><font color="green"><c:out value="${expiryDate.insuranceExpiryDate}" /></font></label></td>
                                                </tr>
                                                <tr>
                                                    <td >Vehicle Permit Valid UpTo</td>
                                                    <td ><label><font color="green"><c:out value="${expiryDate.permitExpiryDate}" /></font></label></td>
                                                </tr>
                                                <tr>
                                                    <td >Road Tax Valid UpTo</td>
                                                    <td ><label><font color="green"><c:out value="${expiryDate.roadTaxExpiryDate}" /></font></label></td>
                                                </tr>
                                            </c:forEach>
                                        </table>
                                    </c:if>


                                </div>

                                <div id="routeDetail">

                                    <c:if test = "${tripPointDetails != null}" >
                                        <table class="table table-info mb30 table-hover" >
                                            <thead><tr >
                                                    <th  height="30" >S No</th>
                                                    <th  height="30" >Point Name</th>
                                                    <th  height="30" >Type</th>
                                                    <th  height="30" >Route Order</th>
                                                    <th  height="30" >Address</th>
                                                    <th  height="30" >Planned Date</th>
                                                    <th  height="30" >Planned Time</th>
                                                </tr></thead>
                                                <% int index2 = 1;%>
                                                <c:forEach items="${tripPointDetails}" var="tripPoint">
                                                    <%
                                                                String classText1 = "";
                                                                int oddEven = index2 % 2;
                                                                if (oddEven > 0) {
                                                                    classText1 = "text1";
                                                                } else {
                                                                    classText1 = "text2";
                                                                }
                                                    %>
                                                <tr >
                                                    <td class="<%=classText1%>" height="30" ><%=index2++%></td>
                                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                                    <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                                                </tr>
                                            </c:forEach >
                                        </table>
                                        <br/>

                                        <table border="0" class="border" align="centver" width="100%" cellpadding="0" cellspacing="0" style="display: none">
                                            <c:if test = "${tripDetails != null}" >
                                                <c:forEach items="${tripDetails}" var="trip">
                                                    <tr>
                                                        <td  width="150"> Estimated KM</td>
                                                        <td  width="120" > <c:out value="${trip.estimatedKM}" />&nbsp;</td>
                                                        <td  colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td  width="150"> Estimated Reefer Hour</td>
                                                        <td  width="120"> <c:out value="${trip.estimatedTransitHours * 60 / 100}" />&nbsp;</td>
                                                        <td  colspan="4">&nbsp;</td>
                                                    </tr>

                                                </c:forEach>
                                            </c:if>
                                        </table>
                                        <!--                                    <br/>
                                                                            <br/>
                                                                            <center>
                                                                                <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                                                                            </center>-->
                                    </c:if>
                                    <!--                                <br>
                                                                    <br>-->
                                </div>            

                                <div id="advance" >
                                    <c:if test="${tripAdvanceDetails != null}">
                                        <c:if test="${tripAdvanceDetails != null}">
                                            <table  class="table table-info mb30 table-hover" id="bg">
                                                <thead> <tr>
                                                        <th  width="30">Sno</th>
                                                        <th  width="90">Advance Date</th>
                                                        <th  width="90">Trip Day</th>
                                                        <th  width="120">Estimated Advance</th>
                                                        <th  width="120">Requested Advance</th>
                                                        <th  width="90"> Type</th>
                                                        <th  width="120">Requested By</th>
                                                        <th  width="120">Requested Remarks</th>
                                                        <th  width="120">Approved By</th>
                                                        <th  width="120">Approved Remarks</th>
                                                        <th  width="120">Paid Advance</th>
                                                    </tr></thead>
                                                    <%int index7 = 1;%>
                                                    <c:forEach items="${tripAdvanceDetails}" var="tripAdvance">
                                                        <c:set var="totalAdvancePaid" value="${ totalAdvancePaid + tripAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                                        <%
                                                                    String classText7 = "";
                                                                    int oddEven7 = index7 % 2;
                                                                    if (oddEven7 > 0) {
                                                                        classText7 = "text1";
                                                                    } else {
                                                                        classText7 = "text2";
                                                                    }
                                                        %>
                                                    <tr>
                                                        <td class="<%=classText7%>"><%=index7++%></td>
                                                        <td class="<%=classText7%>"><c:out value="${tripAdvance.advanceDate}"/></td>
                                                        <td class="<%=classText7%>">DAY&nbsp;<c:out value="${tripAdvance.tripDay}"/></td>
                                                        <td class="<%=classText7%>"><c:out value="${tripAdvance.estimatedAdance}"/></td>
                                                        <td class="<%=classText7%>"><c:out value="${tripAdvance.requestedAdvance}"/></td>
                                                        <c:if test = "${tripAdvance.requestType == 'A'}" >
                                                            <td class="<%=classText7%>">Adhoc</td>
                                                        </c:if>
                                                        <c:if test = "${tripAdvance.requestType == 'B'}" >
                                                            <td class="<%=classText7%>">Batch</td>
                                                        </c:if>
                                                        <c:if test = "${tripAdvance.requestType == 'M'}" >
                                                            <td class="<%=classText7%>">Manual</td>
                                                        </c:if>
                                                        <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRequestBy}"/></td>
                                                        <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRequestRemarks}"/></td>
                                                        <td class="<%=classText7%>"><c:out value="${tripAdvance.approvedBy}"/></td>
                                                        <td class="<%=classText7%>"><c:out value="${tripAdvance.approvalRemarks}"/></td>
                                                        <td class="<%=classText7%>"><c:out value="${tripAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                                    </tr>
                                                </c:forEach>

                                                <tr></tr>
                                                <tr>

                                                    <td >&nbsp;</td>
                                                    <td >&nbsp;</td>
                                                    <td >&nbsp;</td>
                                                    <td >&nbsp;</td>
                                                    <td >&nbsp;</td>
                                                    <td >&nbsp;</td>
                                                    <td ></td>
                                                    <td ></td>
                                                    <td  colspan="2">Total Advance Paid</td>
                                                    <td ><c:out value="${totalAdvancePaid}"/></td>
                                                </tr>
                                            </table>
                                            <br/>
                                            <c:if test="${tripAdvanceDetailsStatus != null}">
                                                <table style="display:none">
                                                    <thead><tr>
                                                            <th  colspan="13" > Advance Approval Status Details</td>
                                                        </tr>
                                                        <tr>
                                                            <th  width="30">Sno</th>
                                                            <th  width="90">Request Date</th>
                                                            <th width="90">Trip Day</th>
                                                            <th  width="120">Estimated Advance</th>
                                                            <th  width="120">Requested Advance</th>
                                                            <th  width="90"> Type</th>
                                                            <th  width="120">Requested By</th>
                                                            <th  width="120">Requested Remarks</th>
                                                            <th  width="120">Approval Status</th>
                                                            <th  width="120">Paid Status</th>
                                                        </tr></thead>
                                                        <%int index13 = 1;%>
                                                        <c:forEach items="${tripAdvanceDetailsStatus}" var="tripAdvanceStatus">
                                                            <%
                                                                        String classText13 = "";
                                                                        int oddEven11 = index13 % 2;
                                                                        if (oddEven11 > 0) {
                                                                            classText13 = "text1";
                                                                        } else {
                                                                            classText13 = "text2";
                                                                        }
                                                            %>
                                                        <tr>

                                                            <td class="<%=classText13%>"><%=index13++%></td>
                                                            <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.advanceDate}"/></td>
                                                            <td class="<%=classText13%>">DAY&nbsp;<c:out value="${tripAdvanceStatus.tripDay}"/></td>
                                                            <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.estimatedAdance}"/></td>
                                                            <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.requestedAdvance}"/></td>
                                                            <c:if test = "${tripAdvanceStatus.requestType == 'A'}" >
                                                                <td class="<%=classText13%>">Adhoc</td>
                                                            </c:if>
                                                            <c:if test = "${tripAdvanceStatus.requestType == 'B'}" >
                                                                <td class="<%=classText13%>">Batch</td>
                                                            </c:if>
                                                            <c:if test = "${tripAdvanceStatus.requestType == 'M'}" >
                                                                <td class="<%=classText13%>">Manual</td>
                                                            </c:if>

                                                            <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.approvalRequestBy}"/></td>
                                                            <td class="<%=classText13%>"><c:out value="${tripAdvanceStatus.approvalRequestRemarks}"/></td>
                                                            <td class="<%=classText13%>">
                                                                <c:if test = "${tripAdvanceStatus.approvalStatus== ''}" >
                                                                    &nbsp
                                                                </c:if>
                                                                <c:if test = "${tripAdvanceStatus.approvalStatus== '1' }" >
                                                                    Request Approved
                                                                </c:if>
                                                                <c:if test = "${tripAdvanceStatus.approvalStatus== '2' }" >
                                                                    Request Rejected
                                                                </c:if>
                                                                <c:if test = "${tripAdvanceStatus.approvalStatus== '0'}" >
                                                                    Approval in  Pending
                                                                </c:if>
                                                                &nbsp;</td>
                                                            <td class="<%=classText13%>">
                                                                <c:if test = "${ tripAdvanceStatus.approvalStatus== '1' || tripAdvanceStatus.approvalStatus== 'N'}" >
                                                                    Yet To Pay
                                                                </c:if>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </table>
                                                <br/>
                                                <br/>

                                                <c:if test="${vehicleChangeAdvanceDetailsSize != '0'}">
                                                    <table style="display:none"  >
                                                        <tr>
                                                            <td  colspan="13" > Old Vehicle Advance Details</td>
                                                        </tr>
                                                        <tr>
                                                            <td  width="30">Sno</td>
                                                            <td  width="90">Advance Date</td>
                                                            <td  width="90">Trip Day</td>
                                                            <td  width="120">Estimated Advance</td>
                                                            <td  width="120">Requested Advance</td>
                                                            <td  width="90"> Type</td>
                                                            <td  width="120">Requested By</td>
                                                            <td  width="120">Requested Remarks</td>
                                                            <td  width="120">Approved By</td>
                                                            <td  width="120">Approved Remarks</td>
                                                            <td  width="120">Paid Advance</td>
                                                        </tr>
                                                        <%int index17 = 1;%>
                                                        <c:forEach items="${vehicleChangeAdvanceDetails}" var="vehicleChangeAdvance">
                                                            <c:set var="totalVehicleChangeAdvancePaid" value="${ totalVehicleChangeAdvancePaid + vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                                            <%
                                                                        String classText17 = "";
                                                                        int oddEven17 = index17 % 2;
                                                                        if (oddEven17 > 0) {
                                                                            classText17 = "text1";
                                                                        } else {
                                                                            classText17 = "text2";
                                                                        }
                                                            %>
                                                            <tr>
                                                                <td class="<%=classText17%>"><%=index7++%></td>
                                                                <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.advanceDate}"/></td>
                                                                <td class="<%=classText17%>">DAY&nbsp;<c:out value="${vehicleChangeAdvance.tripDay}"/></td>
                                                                <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.estimatedAdance}"/></td>
                                                                <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.requestedAdvance}"/></td>
                                                                <c:if test = "${vehicleChangeAdvance.requestType == 'A'}" >
                                                                    <td class="<%=classText17%>">Adhoc</td>
                                                                </c:if>
                                                                <c:if test = "${vehicleChangeAdvance.requestType == 'B'}" >
                                                                    <td class="<%=classText17%>">Batch</td>
                                                                </c:if>
                                                                <c:if test = "${vehicleChangeAdvance.requestType == 'M'}" >
                                                                    <td class="<%=classText17%>">Manual</td>
                                                                </c:if>
                                                                <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRequestBy}"/></td>
                                                                <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRequestRemarks}"/></td>
                                                                <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvedBy}"/></td>
                                                                <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.approvalRemarks}"/></td>
                                                                <td class="<%=classText17%>"><c:out value="${vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                                            </tr>
                                                        </c:forEach>

                                                        <tr></tr>
                                                        <tr>

                                                            <td >&nbsp;</td>
                                                            <td >&nbsp;</td>
                                                            <td >&nbsp;</td>
                                                            <td >&nbsp;</td>
                                                            <td >&nbsp;</td>
                                                            <td >&nbsp;</td>
                                                            <td ></td>
                                                            <td ></td>
                                                            <td  colspan="2">Total Advance Paid</td>
                                                            <td ><c:out value="${totalVehicleChangeAdvancePaid}"/></td>
                                                        </tr>
                                                    </table>
                                                </c:if>
                                            </c:if>
                                            <!--<br/>-->
                                            <!--                                        <center>
                                                                                        <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                                                                                    </center>-->
                                        </c:if>
                                    </c:if>
                                </div>             

                                <div id="podDetail">
                                    <c:if test="${tripType == 1}">
                                        <c:if test="${viewPODDetails != null}">
                                            <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                                <thead><tr>
                                                        <th class="text2" width="50" >S No&nbsp;</th>
                                                        <th class="text2">City Name</th>
                                                        <th class="text2">POD file Name</th>
                                                        <th class="text2">LR Number</th>
                                                        <th class="text2">POD Remarks</th>
                                                    </tr></thead>
                                                    <% int index2 = 1;%>
                                                    <c:forEach items="${viewPODDetails}" var="viewPODDetails">
                                                        <%
                                                                    String classText3 = "";
                                                                    int oddEven = index2 % 2;
                                                                    if (oddEven > 0) {
                                                                        classText3 = "text1";
                                                                    } else {
                                                                        classText3 = "text2";
                                                                    }
                                                        %>
                                                    <tr>
                                                        <td class="<%=classText3%>" ><%=index2++%></td>
                                                        <td class="<%=classText3%>" ><c:out value="${viewPODDetails.cityName}"/></td>
                                                        <td class="<%=classText3%>" ><a  href="JavaScript:popUp('/throttle/uploadFiles/Files/<c:out value="${viewPODDetails.podFile}"/>');"><c:out value="${viewPODDetails.podFile}"/></a></td>
                                                        <td class="<%=classText3%>" ><c:out value="${viewPODDetails.lrNumber}"/></td>
                                                        <td class="<%=classText3%>" ><c:out value="${viewPODDetails.podRemarks}"/></td>
                                                    </tr>
                                                </c:forEach>

                                            </table>
                                        </c:if>
                                    </c:if>
                                </div>             
                                <div id="statusDetail">
                                    <% int index1 = 1;%>

                                    <c:if test = "${statusDetails != null}" >
                                        <table class="table table-info mb30 table-hover" >
                                            <thead>  <tr >
                                                    <th  height="30" >S No</th>
                                                    <th  height="30" >Status Name</th>
                                                    <th  height="30" >Remarks</th>
                                                    <th  height="30" >Created User Name</th>
                                                    <th  height="30" >Created Date</th>
                                                </tr></thead>
                                                <c:forEach items="${statusDetails}" var="statusDetails">
                                                    <%
                                                                String classText = "";
                                                                int oddEven1 = index1 % 2;
                                                                if (oddEven1 > 0) {
                                                                    classText = "text1";
                                                                } else {
                                                                    classText = "text2";
                                                                }
                                                    %>
                                                <tr >
                                                    <td class="<%=classText%>" height="30" ><%=index1++%></td>
                                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.statusName}" /></td>
                                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripRemarks}" /></td>
                                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.userName}" /></td>
                                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripDate}" /></td>
                                                </tr>
                                            </c:forEach >
                                        </table>
                                        <br/>
                                        <br/>
                                        <br/>
                                    </c:if>
                                    <br>
                                    <br>
                                </div>            
                                <div id="systemExpDetail" style="dispaly:none">
                                    <c:if test = "${gpsKm == 0}" >
                                        <c:if test = "${odometerApprovalStatus == null}" >
                                            <div id="odoApprovalRequest" style="display:block;">

                                                <div  >Request Description</div>
                                                <br>
                                                <textarea name="odoUsageRemarks" style="width:600px;height:80px;">GPS data is un available. please approve trip closure based on odometer and reefer reading.</textarea>
                                                <br>
                                                <br>
                                                <input type="button" style="width:130px;" class="btn btn-success" onclick="saveOdoApprovalRequest();" value="request approval" />
                                            </div>
                                            <br>
                                            <div id="odoPendingApproval" style="display:none;"  >
                                                <br>
                                                request has been submitted.
                                                <br>
                                                <br>
                                                <b>odometer usage for expense calculation request is pending approval</b>
                                            </div>
                                        </c:if>
                                        <c:if test = "${odometerApprovalStatus == 0}" >
                                            <br>
                                            <br>
                                            <div  >Request Status: <b>request pending approval</b></div>
                                            <br>
                                        </c:if>
                                        <c:if test = "${odometerApprovalStatus == 2}" >
                                            <div id="odoReApprovalRequest" style="display:block;">
                                                <div  >Request Status : <b>request rejected</b></div>
                                                <br>
                                                <div  >Request Description</div>
                                                <br>
                                                <textarea name="odoUsageReApprovalRemarks"  style="width:600px;height:80px;"></textarea>
                                                <br>
                                                <br>
                                                <input type="button" style="width:150px;" class="btn btn-success" onclick="saveOdoReApprovalRequest();" value="request re approval" />
                                            </div>
                                            <br>
                                            <div id="odoPendingReApproval" style="display:none;" >
                                                <br>
                                                request has been submitted
                                                <br>
                                                <br>
                                                <b>odometer usage for expense calculation re-request is pending approval</b>
                                            </div>
                                        </c:if>
                                    </c:if>



                                    <input type="hidden" name="vehicleId" value='<c:out value="${vehicleId}"/>' />

                                    <input type="hidden" name="extraExpenseValue" value='<%=new DecimalFormat("#0.00").format(extraExpenseValue)%>' />
                                    <input type="hidden" name="tollRate" value='<%=new DecimalFormat("#0.00").format(tollRate)%>' />

                                    <input type="hidden" name="secTollCost" value='<%=new DecimalFormat("#0.00").format(secTollCost)%>' />
                                    <input type="hidden" name="secAddlTollCost" value='<%=new DecimalFormat("#0.00").format(secAddlTollCost)%>' />
                                    <input type="hidden" name="secMiscCost" value='<%=new DecimalFormat("#0.00").format(secMiscCost)%>' />
                                    <input type="hidden" name="secParkingAmount" value='<%=new DecimalFormat("#0.00").format(secParkingAmount)%>' />

                                    <input type="hidden" name="driverBattaPerDay" value='<%=new DecimalFormat("#0.00").format(driverBattaPerDay)%>' />
                                    <input type="hidden" name="driverIncentivePerKm" value='<%=new DecimalFormat("#0.00").format(driverIncentivePerKm)%>' />
                                    <input type="hidden" name="fuelPrice" value='<%=new DecimalFormat("#0.00").format(fuelPrice)%>' />
                                    <input type="hidden" name="mileage" value='<%=new DecimalFormat("#0.00").format(milleage)%>' />
                                    <input type="hidden" name="reeferMileage" value='<%=new DecimalFormat("#0.00").format(reeferMileage)%>' />
                                    <input type="hidden" name="runKm" value='<%=new DecimalFormat("#0.00").format(runKm)%>' />
                                    <input type="hidden" name="runHm" value='<%=new DecimalFormat("#0.00").format(runHm)%>' />
                                    <input type="hidden" name="totalDays" value='<%=new DecimalFormat("#0.00").format(totalDays)%>' />

                                    <input type="hidden" name="dieselUsed" value='<%=new DecimalFormat("#0.00").format(dieselUsed)%>' />
                                    <input type="hidden" name="dieselCost" value='<%=new DecimalFormat("#0.00").format(dieselCost)%>' />
                                    <input type="hidden" name="tollCost" value='<%=new DecimalFormat("#0.00").format(tollCost)%>' />
                                    <input type="hidden" name="driverIncentive" value='<%=new DecimalFormat("#0.00").format(driverIncentive)%>' />
                                    <input type="hidden" name="driverBatta" value='<%=new DecimalFormat("#0.00").format(driverBatta)%>' />
                                    <input type="hidden" name="miscRate" value='<%=new DecimalFormat("#0.00").format(miscRate)%>' />
                                    <input type="hidden" name="miscValue" value='<%=new DecimalFormat("#0.00").format(miscValue)%>' />
                                    <input type="hidden" name="bookedExpense" value='<%=new DecimalFormat("#0.00").format(bookedExpense)%>' />
                                    <input type="hidden" name="preColingAmount" value='<%=new DecimalFormat("#0.00").format(preColingAmount)%>' />
                                    <input type="hidden" name="nettExpense" value='<%=new DecimalFormat("#0.00").format(nettExpense)%>' />

                                    <input type="hidden" name="tripType" value='<c:out value="${tripType}" />' />
                                    <input type="hidden" name="gpsKm" value='<c:out value="${gpsKm}" />' />
                                    <input type="hidden" name="gpsHm" value='<c:out value="${gpsHm}" />' />


                                    <c:if test = "${tripType == 1 }" >
                                        <c:if test = "${(gpsKm != 0) || ((gpsKm == 0) && (odometerApprovalStatus == 1)) }" >
                                            <br>
                                            <table  border="1" class="border" align="left" width="30%" cellpadding="0" cellspacing="0" id="bg" style="display:none">
                                                <tr>
                                                    <td   >Diesel Used</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(dieselUsed)%></td>
                                                </tr>
                                                <tr>
                                                    <td  >Diesel Rate / Ltr (INR)</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(fuelPrice)%></td>
                                                </tr>
                                                <tr>
                                                    <td  >Diesel Cost (1)</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(dieselCost)%></td>
                                                </tr>
                                                <tr>
                                                    <td  >Toll Cost (2)</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(tollCost)%></td>
                                                </tr>
                                                <tr>
                                                    <td  >Driver Incentive (3)</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(driverIncentive)%></td>
                                                </tr>
                                                <tr>
                                                    <td  >Driver Bhatta (4)</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(driverBatta)%></td>
                                                </tr>
                                                <tr>
                                                    <td  >MIS (@kmrun*<c:out value="${miscValue}"/>*5%) (5)</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(miscValue)%></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" >&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td  >Total Expense (System Computed)</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(systemExpense)%></td>
                                                <input type="hidden" name="systemExpense"  id="systemExpense" value='<%=new DecimalFormat("#0.00").format(systemExpense)%>' />
                                                </tr>
                                            </table>
                                        </c:if>
                                    </c:if>
                                    <c:if test = "${tripType == 2 }" >
                                        <c:if test = "${(gpsKm != 0) || ((gpsKm == 0) && (odometerApprovalStatus == 1)) }" >
                                            <br>
                                            <br>
                                            <table  border="1" class="border" align="left" width="30%" cellpadding="0" cellspacing="0" id="bg">
                                                <tr>
                                                    <td   >Fuel Used</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(fuelUsed)%></td>
                                                </tr>
                                                <tr>
                                                    <td  >Fuel Rate / Kg (INR)</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(fuelPrice)%></td>
                                                </tr>
                                                <c:if test="${fuelTypeId == 1003}">
                                                    <tr>
                                                        <td  >Pre - Cooling CNG</td>
                                                        <%--  <c:if test="${tripCnt == 1}"> --%>
                                                        <td   align="right" ><%=new DecimalFormat("#0.00").format(preColingAmount)%></td>
                                                        <%-- </c:if> --%>
                                                        <%--  <c:if test="${tripCnt > 1 }">

                                        <td   align="right" ><%=new DecimalFormat("#0.00").format(0.00)%></td>
                                    </c:if> --%>
                                                    </tr>
                                                </c:if>
                                                <tr>
                                                    <td  >Fuel Cost (1)</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(fuelCost)%></td>
                                                </tr>
                                                <input type="hidden" name="fuelUsed" value='<%=new DecimalFormat("#0.00").format(fuelUsed)%>' />
                                                <input type="hidden" name="fuelPrice" value='<%=new DecimalFormat("#0.00").format(fuelPrice)%>' />
                                                <input type="hidden" name="fuelCost" value='<%=new DecimalFormat("#0.00").format(fuelCost)%>' />
                                                <tr>
                                                    <td  >Toll Cost (2)</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(secTollCost)%></td>
                                                </tr>
                                                <tr>
                                                    <td  >Addl Toll Cost (3)</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(secAddlTollCost)%></td>
                                                </tr>
                                                <tr>
                                                    <td  >Misc Cost (4)</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(secMiscCost)%></td>
                                                </tr>
                                                <tr>
                                                    <td  >Parking Cost (5)</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(secParkingAmount)%></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" >&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td  >Total Expense (System Computed)</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(systemExpense)%></td>
                                                <input type="hidden" name="systemExpense"  id="systemExpense" value='<%=new DecimalFormat("#0.00").format(systemExpense)%>' />
                                                </tr>
                                            </table>
                                        </c:if>
                                    </c:if>
                                    <br>
                                    <br>
                                    <br>

                                    <c:if test = "${approveProcessHistory != null}" >
                                        <table class="table table-info mb30 table-hover">
                                            <thead  style="display:none" ><tr>
                                                    <th  colspan="9" > Approve Process History</th>
                                                </tr>
                                                <tr >
                                                    <th  height="30" >Request By</th>
                                                    <th  height="30" >Request On</th>
                                                    <th  height="30" >Request Remarks</th>
                                                    <th  height="30" >Run KM</th>
                                                    <th  height="30" >Run HM</th>
                                                    <th  height="30" >Status</th>
                                                    <th  height="30" >Approver Remarks</th>
                                                    <th  height="30" >Approver</th>
                                                    <th  height="30" >Approver Processed On</th>
                                                </tr></thead>
                                                <c:forEach items="${approveProcessHistory}" var="approveHistory">
                                                    <c:if test = "${approveHistory.requestType == 1}" >
                                                    <tr  style="display:none" >
                                                        <td  height="30" ><c:out value="${approveHistory.approvalRequestBy}" />&nbsp;</td>
                                                        <td  height="30" ><c:out value="${approveHistory.approvalRequestOn}" />&nbsp;</td>
                                                        <td  height="30" ><c:out value="${approveHistory.approvalRequestRemarks}" />&nbsp;</td>
                                                        <td  height="30" ><c:out value="${approveHistory.runKm}" />&nbsp;</td>
                                                        <td  height="30" ><c:out value="${approveHistory.runHm}" />&nbsp;</td>
                                                        <td  height="30" ><c:out value="${approveHistory.approvalStatus}" />&nbsp;</td>
                                                        <td  height="30" ><c:out value="${approveHistory.approvalRemarks}" />&nbsp;</td>
                                                        <td  height="30" ><c:out value="${approveHistory.approvedBy}" />&nbsp;</td>
                                                        <td  height="30" ><c:out value="${approveHistory.approvedOn}" />&nbsp;</td>

                                                    </tr>
                                                </c:if>

                                            </c:forEach >
                                        </table>
                                    </c:if>
                                    <!--                                <center>
                                                                        <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Next" /></a>
                                                                    </center>
                                                                    <br>-->

                                </div>

                                <div id="closure" style="display:none">

                                    <!--<br>-->
                                    <c:if test = "${(gpsKm != 0) || ((gpsKm == 0) && (odometerApprovalStatus == 1)) }" >
                                        <table  style="display:none" border="1" class="border" align="left" width="30%" cellpadding="0" cellspacing="0" id="bg">
                                            <tr>
                                                <td  >RCM Cost</td>
                                                <td   align="right" ><%=new DecimalFormat("#0.00").format(estimatedExpense)%></td>
                                            </tr>
                                            <c:if test="${tripType == 1}">
                                                <tr>
                                                    <td  >System Expense + Booked Expense</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(nettExpense)%></td>
                                                </tr>
                                            </c:if>
                                            <c:if test="${tripType == 2 && fuelTypeId == 1002}">
                                                <tr>
                                                    <td  >System Expense + Booked Expense</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(nettExpense)%></td>
                                                </tr>
                                            </c:if>
                                            <c:if test="${tripType == 2 && fuelTypeId == 1003}">
                                                <tr>
                                                    <td  >Pre - Cooling CNG</td>
                                                    <%--<c:if test="${tripCnt == 1}"> --%>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(preColingAmount)%></td>

                                                    <%--  </c:if> --%>
                                                    <%--  <c:if test="${tripCnt > 1 }">

                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(0.00)%></td>
                                </c:if> --%>
                                                </tr>
                                                <tr>
                                                    <td  >System Expense + Booked Expense + Precooling Price</td>
                                                    <td   align="right" ><%=new DecimalFormat("#0.00").format(nettExpense)%></td>
                                                </tr>
                                            </c:if>

                                            <tr>
                                                <td  >Deviation</td>
                                                <td   align="right" >
                                                    <%
                                                                Float deviation = estimatedExpense - (nettExpense);
                                                                Float deviationPercentage = (deviation / estimatedExpense) * 100;
                                                                if (deviation > 0) {
                                                    %>
                                                    <font color="green">
                                                    <%=new DecimalFormat("#0.00").format(deviation)%>
                                                    <br>
                                                    <%=new DecimalFormat("#0.00").format(deviationPercentage)%> &nbsp;%
                                                    </font>
                                                    <%} else {%>
                                                    <font color="red">
                                                    <%=new DecimalFormat("#0.00").format(deviation)%>
                                                    <br>
                                                    <%=new DecimalFormat("#0.00").format(deviationPercentage)%> &nbsp;%
                                                    </font>
                                                    <%}%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td  >Profit Margin(%)</td>
                                                <td   align="right" >
                                                    <%
                                                                Float profitValue = Float.parseFloat(revenueStr) - (nettExpense);
                                                                Float profitMargin = (profitValue / Float.parseFloat(revenueStr)) * 100;
                                                                if (profitValue > 0) {
                                                    %>
                                                    <font color="green"><%=new DecimalFormat("#0.00").format(profitMargin)%> </font>
                                                    <%} else {%>
                                                    <font color="red"><%=new DecimalFormat("#0.00").format(profitMargin)%> </font>
                                                    <%}%>
                                                </td>
                                            </tr>
                                        </table>


                                        <%
                                                    if (estActualExpDiff > 0) {
                                        %>

                                        <table class="table table-info mb30 table-hover" style="display:none">
                                            <thead>
                                                <tr style="display: none">
                                                    <th  colspan="4">Trip Close </th>
                                                </tr>
                                            </thead>
                                            <tr style="display: none">
                                                <td ><font color="red">*</font>Vehicle Log Sheet Number</td>
                                                <td ><c:if test = "${tripStartDetails != null}" >
                                                        <c:forEach items="${tripStartDetails}" var="startDetails">
                                                            <%--<c:set var="lrNumber" value="${startDetails.lrNumber}" />--%>
                                                            <!--<input type="text" name="lrNumber" id="lrNumber"  value="0" class="form-control" style="width:250px;height:40px">-->
                                                        </c:forEach>
                                                    </c:if>
                                                </td>
                                                <c:if test = "${tripFuel != null}" >
                                                    <c:forEach items="${tripFuel}" var="tripFuel">
                                                        <c:set var="fuelLitres" value="${tripFuel.fuelLitres}" />

                                                    </c:forEach>
                                                </c:if>
                                                <td ><font color="red">*</font>Fuel Liters</td>
                                                <td ><input type="text" name="fuelLtr" id="fuelLtr"  value="<c:out value="${fuelLitres}" />" class="form-control" style="width:250px;height:40px"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center"><input type="button" value="Close Trip1" class="btn btn-success" onclick="closeTrip();" </td>
                                            </tr>
                                        </table>
                                        <!--<input type="button" class="btn btn-success" name="Save" style="width:200px;" value="Close Trip" onclick="closeTrip();" />-->

                                        <%} else {%>

                                        <c:if test = "${expenseDeviationApprovalStatus == null}" >
                                            <div id="expDeviationApprovalRequest" style="display:block;">
                                                <!--<br>-->
                                                <!--<br>-->
                                                <div align="left"  >Request Description</div>
                                                <br><textarea name="expDeviationRemarks" style="width:600px;height:80px;"></textarea>
                                                <!--<br>-->
                                                <!--<br>-->
                                                <input type="button" style="width:130px;" class="btn btn-success"  onclick="saveExpDeviationApprovalRequest();" value="request approval" />
                                            </div>
                                            <!--<br>-->
                                            <div id="expDeviationPendingApproval" style="display:none;" >
                                                <!--<br>-->
                                                request has been submitted
                                                <!--<br>-->
                                                <!--<br>-->
                                                <b>expense deviation request is pending approval</b>
                                            </div>
                                        </c:if>

                                        <c:if test = "${expenseDeviationApprovalStatus == 0}" >
                                            <!--<br>-->
                                            <!--<br>-->
                                            <div  >Request Status: <b>request pending approval</b></div>
                                            <!--<br>-->
                                        </c:if>
                                        <c:if test = "${expenseDeviationApprovalStatus == 1}" >
                                            <table>
                                                <c:if test="${viewPODDetails != null}">
                                                    <c:forEach items="${viewPODDetails}" var="viewPODDetails">

                                                    </c:forEach>
                                                </c:if>
                                            </table>
                                            <%--<c:if test = "${tripStartDetails != null}" >--%>
                                            <%--<c:forEach items="${tripStartDetails}" var="startDetails">--%>
                                            <%--<c:set var="lrNumber" value="${startDetails.lrNumber}" />--%>
                                            <!--<td ><font color="red">*</font>Vehicle Log Sheet Number</td>-->
                                            <!--<td ><input type="text" name="lrNumber" id="lrNumber"  value="<c:out value="${startDetails.lrNumber}"/>" class="form-control" style="width:250px;height:40px"></td>-->
                                            <!--<input type="button" value="Close Trip" class="btn btn-success" onclick="closeTrip();" />-->
                                            <%--</c:forEach>--%>
                                            <%--</c:if>--%>
                                            <table class="table table-info mb30 table-hover" >
                                                <thead>
                                                    <tr>
                                                        <th  colspan="4">Trip Close </th>
                                                    </tr>
                                                </thead>
                                                <tr>
                                                    <td ><font color="red">*</font>Vehicle Log Sheet Number</td>
                                                    <td ><c:if test = "${tripStartDetails != null}" >
                                                            <c:forEach items="${tripStartDetails}" var="startDetails">
                                                                <%--<c:set var="lrNumber" value="${startDetails.lrNumber}" />--%>
                                                                <input type="text" name="lrNumber" id="lrNumber"  value="<c:out value="${startDetails.lrNumber}"/>" class="form-control" style="width:250px;height:40px">
                                                            </c:forEach>
                                                        </c:if>
                                                    </td>
                                                    <c:if test = "${tripFuel != null}" >
                                                        <c:forEach items="${tripFuel}" var="tripFuel">
                                                            <c:set var="fuelLitres" value="${tripFuel.fuelLitres}" />

                                                        </c:forEach>
                                                    </c:if>
                                                    <td ><font color="red">*</font>Fuel Liters</td>
                                                    <td ><input type="text" name="fuelLtr" id="fuelLtr"  value="<c:out value="${fuelLitres}" />" class="form-control" style="width:250px;height:40px"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="center"><input type="button" value="Close Trip" class="btn btn-success" onclick="closeTrip();" </td>
                                                </tr>
                                            </table>
                                        </c:if>
                                        <c:if test = "${expenseDeviationApprovalStatus == 2}" >
                                            <div id="expDeviationReApprovalRequest" style="display:block;">
                                                <div  >Request Status : <b>request rejected</b></div>
                                                <!--<br>-->
                                                <div  >Request Description</div>
                                                <br><textarea name="expDeviationReApprovalRemarks"   style="width:600px;height:80px;"></textarea>
                                                <!--<br>-->
                                                <!--<br>-->
                                                <input type="button" style="width:150px;" class="btn btn-success"  onclick="saveexpDeviationReApprovalRequest();" value="request re approval" />
                                            </div>
                                            <br>
                                            <div id="expDeviationPendingReApproval" style="display:none;"  >
                                                <!--<br>-->
                                                request has been submitted
                                                <!--<br>-->
                                                <!--<br>-->
                                                <b>expense deviation re-request is pending approval</b>
                                            </div>
                                        </c:if>



                                        <%}%>


                                        <c:if test = "${approveProcessHistory != null}" >
                                            <table class="table table-info mb30 table-hover" style="display: none">
                                                <thead><tr>
                                                        <th  colspan="9" > Approve Process History</th>
                                                    </tr>
                                                    <tr >
                                                        <th  height="30" >Request By</th>
                                                        <th  height="30" >Request On</th>
                                                        <th  height="30" >Request Remarks</th>
                                                        <th  height="30" >RCM Expense</th>
                                                        <th  height="30" >Actual Expense</th>
                                                        <th  height="30" >Status</th>
                                                        <th  height="30" >Approver Remarks</th>
                                                        <th  height="30" >Approver</th>
                                                        <th  height="30" >Approver Processed On</th>
                                                    </tr></thead>
                                                    <c:forEach items="${approveProcessHistory}" var="approveHistory">
                                                        <c:if test = "${approveHistory.requestType == 2}" >
                                                        <tr >
                                                            <td  height="30" ><c:out value="${approveHistory.approvalRequestBy}" />&nbsp;</td>
                                                            <td  height="30" ><c:out value="${approveHistory.approvalRequestOn}" />&nbsp;</td>
                                                            <td  height="30" ><c:out value="${approveHistory.approvalRequestRemarks}" />&nbsp;</td>
                                                            <td  height="30" ><c:out value="${approveHistory.rcmExpense}" />&nbsp;</td>
                                                            <td  height="30" ><c:out value="${approveHistory.nettExpense}" />&nbsp;</td>
                                                            <td  height="30" ><c:out value="${approveHistory.approvalStatus}" />&nbsp;</td>
                                                            <td  height="30" ><c:out value="${approveHistory.approvalRemarks}" />&nbsp;</td>
                                                            <td  height="30" ><c:out value="${approveHistory.approvedBy}" />&nbsp;</td>
                                                            <td  height="30" ><c:out value="${approveHistory.approvedOn}" />&nbsp;</td>

                                                        </tr>
                                                    </c:if>

                                                </c:forEach >
                                            </table>
                                        </c:if>
                                    </c:if>
                                    <c:if test = "${((gpsKm == 0) && (odometerApprovalStatus != 1)) }" >
                                        <!--<br>-->
                                        <!--<b>please pass through km usage process</b>-->

                                    </c:if>


                                    <!--<center>-->
                                    <!--<a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Next" /></a>-->
                                    <!--</center>-->
                                </div>




                                <!--                <div id="preStart">
                                <%--<c:if test = "${tripPreStartDetails != null}" >--%>
                                    <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                <%--<c:forEach items="${tripPreStartDetails}" var="preStartDetails">--%>
                                    <tr>
                                        <td  colspan="4" >Pre Start Details</td>
                                    </tr>
                                    <tr >
                                        <td  height="30" >Pre Start Date</td>
                                        <td  height="30" ><c:out value="${preStartDetails.preStartDate}" /></td>
                                        <td  height="30" >Pre Start Time</td>
                                        <td  height="30" ><c:out value="${preStartDetails.preStartTime}" /></td>
                                    </tr>
                                    <tr>
                                        <td  height="30" >Pre Odometer Reading</td>
                                        <td  height="30" ><c:out value="${preStartDetails.preOdometerReading}" /></td>
                                <%--<c:if test = "${tripDetails != null}" >--%>
                                    <td >Pre Start Location / Distance</td>
                                    <td > 
                                <%--<c:out value="${trip.preStartLocation}" />--%> 
                         
                                <%--<c:out value="${trip.preStartLocationDistance}" />--%>
                        KM</td>
                                <%--</c:if>--%>
                            </tr>
                            <tr>
                                <td  height="30" >Pre Start Remarks</td>
                                <td  height="30" >
                                <%--<c:out value="${preStartDetails.preTripRemarks}" />--%>
                        </td>
                            </tr>
                                <%--</c:forEach >--%>
                            </table>
                            <br/>
                            <br/>
                            <br/>
                             <center>
                                <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                            </center>
                                <%--</c:if>--%>
                                <br>
                                <br>
                            </div>-->
                                <div style="display:none">
                                    <c:if test = "${tripStartDetails != null}" >
                                        <table  style="display:none" >
                                            <c:forEach items="${tripStartDetails}" var="startDetails">
                                                <thead>  <tr>
                                                        <th  colspan="6" > Trip Start Details</th>
                                                    </tr></thead>
                                                <tr>
                                                    <td  height="30" >Trip Planned Start Date</td>
                                                    <td  height="30" ><c:out value="${startDetails.planStartDate}" />&nbsp;</td>
                                                    <td  height="30" >Trip Planned Start Time</td>
                                                    <td  height="30" ><c:out value="${startDetails.planStartTime}" />&nbsp;</td>                                            

                                                </tr>
                                                <tr>                                                
                                                    <td  height="30" >Trip Start Reporting Date</td>
                                                    <td  height="30" ><c:out value="${startDetails.startReportingDate}" />&nbsp;</td>
                                                    <td></td>                                                
                                                    <td></td>                                                
                                                </tr>
                                                <tr>
                                                    <td  height="30" >Trip Start Reporting Time</td>
                                                    <td  height="30" ><c:out value="${startDetails.startReportingTime}" />&nbsp;</td>
                                                    <td  height="30" >Trip Loading date</td>
                                                    <td  height="30" ><c:out value="${startDetails.loadingDate}" />&nbsp;</td>
                                                    <td  height="30" >Trip Loading Time</td>
                                                    <td  height="30" ><c:out value="${startDetails.loadingTime}" />&nbsp;</td>
                                                </tr>
                                                <tr >
                                                    <td  height="30" >Trip Loading Temperature</td>
                                                    <td  height="30" ><c:out value="${startDetails.loadingTemperature}" />&nbsp;</td>
                                                    <td  height="30" >Trip Actual Start Date</td>
                                                    <td  height="30" ><c:out value="${startDetails.startDate}" />&nbsp;</td>
                                                    <td  height="30" >Trip Actual Start Time</td>
                                                    <td  height="30" ><c:out value="${startDetails.startTime}" />&nbsp;</td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td  height="30" >Trip Start Odometer Reading(KM)</td>
                                                    <td  height="30" ><c:out value="${startDetails.startOdometerReading}" />&nbsp;</td>
                                                    <td  height="30" >Trip Start Reefer Reading(HM)</td>
                                                    <td  height="30" ><c:out value="${startDetails.startHM}" />&nbsp;</td>
                                                    <td  height="30" colspan="2" ></td>
                                                </tr>
                                            </c:forEach >
                                        </table>

                                        <c:if test = "${tripUnPackDetails != null}" >
                                            <table class="table table-info mb30 table-hover" id="addTyres1">
                                                <thead><tr>
                                                        <th width="20"  align="center" height="30" >Sno</th>
                                                        <th  height="30" >Product/Article Code</th>
                                                        <th  height="30" >Product/Article Name </th>
                                                        <th  height="30" >Batch </th>
                                                        <th  height="30" ><font color='red'>*</font>No of Packages</th>
                                                        <th  height="30" ><font color='red'>*</font>Uom</th>
                                                        <th  height="30" ><font color='red'>*</font>Total Weight (in Kg)</th>
                                                        <th  height="30" ><font color='red'>*</font>Loaded Package Nos</th>
                                                        <th  height="30" ><font color='red'>*</font>UnLoaded Package Nos</th>
                                                        <th  height="30" ><font color='red'>*</font>Shortage</th>
                                                    </tr></thead>


                                                <%int i1 = 1;%>
                                                <c:forEach items="${tripUnPackDetails}" var="tripunpack">
                                                    <tr>
                                                        <td><%=i1%></td>
                                                        <td><input type="text"  name="productCodes" id="productCodes" value="<c:out value="${tripunpack.articleCode}"/>" readonly/></td>
                                                        <td><input type="text" name="productNames" id="productNames" value="<c:out value="${tripunpack.articleName}"/>" readonly/></td>
                                                        <td><input type="text" name="productbatch" id="productbatch" value="<c:out value="${tripunpack.batch}"/>" readonly/></td>
                                                        <td><input type="text" name="packagesNos" id="packagesNos" value="<c:out value="${tripunpack.packageNos}"/>" readonly/></td>
                                                        <td><input type="text" name="productuom" id="productuom" value="<c:out value="${tripunpack.uom}"/>" readonly/></td>
                                                        <td><input type="text" name="weights" id="weights" value="<c:out value="${tripunpack.packageWeight}"/> " readonly/>
                                                        <td><input type="text" name="loadedpackages" id="loadedpackages<%=i1%>" value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly/>
                                                            <input type="hidden" name="consignmentId" value="<c:out value="${tripunpack.consignmentId}"/>"/>
                                                            <input type="hidden" name="tripArticleId" value="<c:out value="${tripunpack.tripArticleid}"/>"/>
                                                        </td>
                                                        <td><input type="text" name="unloadedpackages" id="unloadedpackages<%=i1%>" onblur="computeShortage(<%=i1%>);"  value="0"  onKeyPress="return onKeyPressBlockCharacters(event);"    /></td>
                                                        <td><input type="text" name="shortage" id="shortage<%=i1%>"  value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly /></td>
                                                    </tr>
                                                    <%i1++;%>
                                                </c:forEach>


                                            </table>

                                            <!--<center>-->
                                            <!--<a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>-->
                                            <!--</center>-->
                                        </c:if>
                                        <!--<br>-->
                                        <!--<br>-->
                                    </c:if>
                                </div>
                                <c:if test="${fclogin == 'No' || fclogin == null }">
                                    <div id="editStartDetail" style="display:none">
                                        <c:if test = "${tripStartDetails != null}" >
                                            <table class="table table-info mb30 table-hover" >
                                                <c:forEach items="${tripStartDetails}" var="startDetails">
                                                    <thead><tr>
                                                            <th  colspan="6" > Trip Start Details</th>
                                                        </tr><thead>
                                                        <tr>
                                                            <td  height="30" >Trip Planned Start Date</td>
                                                            <td  height="30" ><input type="text" class="datepicker  form-control" style="width:250px;height:40px" name="tripPlannedDate" id="tripPlannedDate"  value='<c:out value="${startDetails.planStartDate}" />' />&nbsp;</td>
                                                            <td  height="30" >Trip Planned Start Time</td>

                                                            <td  height="30" >
                                                                <c:set var="startReportTime" value="${startDetails.planStartTime}" />
                                                                <%
                                                                             Object startReportTime = (Object) pageContext.getAttribute("startReportTime");
                                                                             String stReportTime = (String) startReportTime;
                                                                             String[] temps = null;
                                                                             temps = stReportTime.split(":");
                                                                %>
                                                                HH: <select name="tripPlannedTimeHr"  id="tripPlannedTimeHr">
                                                                    <option value='00'>00</option>
                                                                    <option value='01'>01</option>
                                                                    <option value='02'>02</option>
                                                                    <option value='03'>03</option>
                                                                    <option value='04'>04</option>
                                                                    <option value='05'>05</option>
                                                                    <option value='06'>06</option>
                                                                    <option value='07'>07</option>
                                                                    <option value='08'>08</option>
                                                                    <option value='09'>09</option>
                                                                    <option value='10'>10</option>
                                                                    <option value='11'>11</option>
                                                                    <option value='12'>12</option>
                                                                    <option value='13'>13</option>
                                                                    <option value='14'>14</option>
                                                                    <option value='15'>15</option>
                                                                    <option value='16'>16</option>
                                                                    <option value='17'>17</option>
                                                                    <option value='18'>18</option>
                                                                    <option value='19'>19</option>
                                                                    <option value='20'>20</option>
                                                                    <option value='21'>21</option>
                                                                    <option value='22'>22</option>
                                                                    <option value='23'>23</option>
                                                                </select>
                                                                MI: <select name="tripPlannedTimeMin"  id="tripPlannedTimeMin" >
                                                                    <option value='00'>00</option>
                                                                    <option value='01'>01</option>
                                                                    <option value='02'>02</option>
                                                                    <option value='03'>03</option>
                                                                    <option value='04'>04</option>
                                                                    <option value='05'>05</option>
                                                                    <option value='06'>06</option>
                                                                    <option value='07'>07</option>
                                                                    <option value='08'>08</option>
                                                                    <option value='09'>09</option>
                                                                    <option value='10'>10</option>
                                                                    <option value='11'>11</option>
                                                                    <option value='12'>12</option>
                                                                    <option value='13'>13</option>
                                                                    <option value='14'>14</option>
                                                                    <option value='15'>15</option>
                                                                    <option value='16'>16</option>
                                                                    <option value='17'>17</option>
                                                                    <option value='18'>18</option>
                                                                    <option value='19'>19</option>
                                                                    <option value='20'>20</option>
                                                                    <option value='21'>21</option>
                                                                    <option value='22'>22</option>
                                                                    <option value='23'>23</option>
                                                                    <option value='24'>24</option>
                                                                    <option value='25'>25</option>
                                                                    <option value='26'>26</option>
                                                                    <option value='27'>27</option>
                                                                    <option value='28'>28</option>
                                                                    <option value='29'>29</option>
                                                                    <option value='30'>30</option>
                                                                    <option value='31'>31</option>
                                                                    <option value='32'>32</option>
                                                                    <option value='33'>33</option>
                                                                    <option value='34'>34</option>
                                                                    <option value='35'>35</option>
                                                                    <option value='36'>36</option>
                                                                    <option value='37'>37</option>
                                                                    <option value='38'>38</option>
                                                                    <option value='39'>39</option>
                                                                    <option value='40'>40</option>
                                                                    <option value='41'>41</option>
                                                                    <option value='42'>42</option>
                                                                    <option value='43'>43</option>
                                                                    <option value='44'>44</option>
                                                                    <option value='45'>45</option>
                                                                    <option value='46'>46</option>
                                                                    <option value='47'>47</option>
                                                                    <option value='48'>48</option>
                                                                    <option value='49'>49</option>
                                                                    <option value='50'>50</option>
                                                                    <option value='51'>51</option>
                                                                    <option value='52'>52</option>
                                                                    <option value='53'>53</option>
                                                                    <option value='54'>54</option>
                                                                    <option value='55'>55</option>
                                                                    <option value='56'>56</option>
                                                                    <option value='57'>57</option>
                                                                    <option value='58'>58</option>
                                                                    <option value='59'>59</option>

                                                                </select>
                                                                <script>
                                                                    document.getElementById("tripPlannedTimeHr").value = '<%=temps[0]%>';
                                                                    document.getElementById("tripPlannedTimeMin").value = '<%=temps[1]%>';
                                                                </script>

                                                        </tr>

                                                        <tr>
                                                            <td  height="30" >Trip Start Reporting Date</td>
                                                            <td  height="30" ><input type="text" class="datepicker , form-control" style="width:250px;height:40px" name="tripStartReportingDate" id="tripStartReportingDate"  value='<c:out value="${startDetails.startReportingDate}" />' />&nbsp;</td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        <!--                                                        <td  height="30" >Trip Loading Temperature</td>
                                                                                                                <td  height="30" >
                                                                                                                    &nbsp;</td>-->
                                                    <input type="hidden" class="form-control" style="width:250px;height:40px" name="tripStartLoadTemp" id="tripStartLoadTemp"  value='<c:out value="${startDetails.loadingTemperature}" />' />
                                                    <tr>
                                                        <td  height="30" >Trip Actual Start Date</td>
                                                        <td  height="30" >
                                                            <input type="text" class="datepicker , form-control" style="width:250px;height:40px" name="tripStartDate" id="tripStartDate"  value='<c:out value="${startDetails.startDate}" />' />

                                                        </td>
                                                        <td  height="30" >Trip Actual Start Time</td>
                                                        <td  height="30" >
                                                            <c:set var="startTime" value="${startDetails.startTime}"/>
                                                            <%
                                                                        Object startTime = (Object) pageContext.getAttribute("startTime");
                                                                        String stTime = (String) startTime;
                                                                        String[] temp = null;
                                                                        temp = stTime.split(":");
                                                            %>
                                                            HH: <select name="tripStartTimeHr"  id="tripStartTimeHr">
                                                                <option value='00'>00</option>
                                                                <option value='01'>01</option>
                                                                <option value='02'>02</option>
                                                                <option value='03'>03</option>
                                                                <option value='04'>04</option>
                                                                <option value='05'>05</option>
                                                                <option value='06'>06</option>
                                                                <option value='07'>07</option>
                                                                <option value='08'>08</option>
                                                                <option value='09'>09</option>
                                                                <option value='10'>10</option>
                                                                <option value='11'>11</option>
                                                                <option value='12'>12</option>
                                                                <option value='13'>13</option>
                                                                <option value='14'>14</option>
                                                                <option value='15'>15</option>
                                                                <option value='16'>16</option>
                                                                <option value='17'>17</option>
                                                                <option value='18'>18</option>
                                                                <option value='19'>19</option>
                                                                <option value='20'>20</option>
                                                                <option value='21'>21</option>
                                                                <option value='22'>22</option>
                                                                <option value='23'>23</option>
                                                            </select>
                                                            MI: <select name="tripStartTimeMin"  id="tripStartTimeMin" >
                                                                <option value='00'>00</option>
                                                                <option value='01'>01</option>
                                                                <option value='02'>02</option>
                                                                <option value='03'>03</option>
                                                                <option value='04'>04</option>
                                                                <option value='05'>05</option>
                                                                <option value='06'>06</option>
                                                                <option value='07'>07</option>
                                                                <option value='08'>08</option>
                                                                <option value='09'>09</option>
                                                                <option value='10'>10</option>
                                                                <option value='11'>11</option>
                                                                <option value='12'>12</option>
                                                                <option value='13'>13</option>
                                                                <option value='14'>14</option>
                                                                <option value='15'>15</option>
                                                                <option value='16'>16</option>
                                                                <option value='17'>17</option>
                                                                <option value='18'>18</option>
                                                                <option value='19'>19</option>
                                                                <option value='20'>20</option>
                                                                <option value='21'>21</option>
                                                                <option value='22'>22</option>
                                                                <option value='23'>23</option>
                                                                <option value='24'>24</option>
                                                                <option value='25'>25</option>
                                                                <option value='26'>26</option>
                                                                <option value='27'>27</option>
                                                                <option value='28'>28</option>
                                                                <option value='29'>29</option>
                                                                <option value='30'>30</option>
                                                                <option value='31'>31</option>
                                                                <option value='32'>32</option>
                                                                <option value='33'>33</option>
                                                                <option value='34'>34</option>
                                                                <option value='35'>35</option>
                                                                <option value='36'>36</option>
                                                                <option value='37'>37</option>
                                                                <option value='38'>38</option>
                                                                <option value='39'>39</option>
                                                                <option value='40'>40</option>
                                                                <option value='41'>41</option>
                                                                <option value='42'>42</option>
                                                                <option value='43'>43</option>
                                                                <option value='44'>44</option>
                                                                <option value='45'>45</option>
                                                                <option value='46'>46</option>
                                                                <option value='47'>47</option>
                                                                <option value='48'>48</option>
                                                                <option value='49'>49</option>
                                                                <option value='50'>50</option>
                                                                <option value='51'>51</option>
                                                                <option value='52'>52</option>
                                                                <option value='53'>53</option>
                                                                <option value='54'>54</option>
                                                                <option value='55'>55</option>
                                                                <option value='56'>56</option>
                                                                <option value='57'>57</option>
                                                                <option value='58'>58</option>
                                                                <option value='59'>59</option>

                                                            </select>
                                                            <script>
                                                                document.getElementById("tripStartTimeHr").value = '<%=temp[0]%>';
                                                                document.getElementById("tripStartTimeMin").value = '<%=temp[1]%>';</script>
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr style="display: none">
                                                        <td  height="30" >Trip Start Odometer Reading(KM)</td>
                                                        <td  height="30" >
                                                            <input type="text" class="form-control" style="width:250px;height:40px" name="tripStartKm" id="tripStartKm"  value='<c:out value="${startDetails.startOdometerReading}"/>' />
                                                            &nbsp;</td>
                                                        <td  height="30" >Trip Start Reefer Reading(HM)</td>
                                                        <td  height="30" >
                                                            <input type="text" class="form-control" style="width:250px;height:40px" name="tripStartHm" id="tripStartHm" value='<c:out value="${startDetails.startHM}"/>' />
                                                        </td>
                                                        <td  height="30" colspan="2" ></td>
                                                    </tr>
                                                </c:forEach >
                                            </table>


                                            <br/>
                                            <center>
                                                <input type="button" class="btn btn-success" name="Save" style="width:200px;" value="Update Trip Start Details" onclick="saveTripStartDetails();" />
                                            </center>

                                            <br>
                                            <br>
                                        </c:if>
                                    </div>
                                </c:if>
                                <div id="endDetails" style="display:none">
                                    <c:if test = "${tripEndDetails != null}" >
                                        <table class="table table-info mb30 table-hover">
                                            <c:forEach items="${tripEndDetails}" var="endDetails">
                                                <thead><tr>
                                                        <th  colspan="6" >  Trip End Details</th>
                                                    </tr>
                                                    <tr>
                                                        <th  height="30" >Trip Planned End Date</th>
                                                        <th  height="30" ><c:out value="${endDetails.planEndDate}" /></th>
                                                        <th  height="30" >Trip Planned End Time</th>
                                                        <th  height="30" ><c:out value="${endDetails.planEndTime}" /></th>
                                                        <th  height="30" >Trip Actual Reporting Date</th>
                                                        <th  height="30" ><c:out value="${endDetails.endReportingDate}" /></th>
                                                    </tr></thead>
                                                <tr>
                                                    <td  height="30" >Trip Actual Reporting Time</td>
                                                    <td  height="30" ><c:out value="${endDetails.endReportingTime}" /></td>
                                                    <td  height="30" >Trip Unloading Completed Date</td>
                                                    <td  height="30" ><c:out value="${endDetails.unLoadingDate}" /></td>
                                                    <td  height="30" >Trip Unloading Completed Time</td>
                                                    <td  height="30" ><c:out value="${endDetails.unLoadingTime}" /></td>
                                                </tr>
                                                <tr>
                                                    <td  height="30" >Trip Unloading Temperature</td>
                                                    <td  height="30" ><c:out value="${endDetails.unLoadingTemperature}" /></td>
                                                    <td  height="30" >Trip Actual End Date</td>
                                                    <td  height="30" ><c:out value="${endDetails.endDate}" /></td>
                                                    <td  height="30" >Trip Actual End Time</td>
                                                    <td  height="30" ><input type="hidden" name="endTime" id="endTime" value="<c:out value="${endDetails.endTime}" />" />   <c:out value="${endDetails.endTime}" /></td>
                                                </tr>
                                                <tr>
                                                    <td  height="30" >Trip End Odometer Reading(KM)</td>
                                                    <td  height="30" ><c:out value="${endDetails.endOdometerReading}" /></td>
                                                    <td  height="30" >Trip End Reefer Reading(HM)</td>
                                                    <td  height="30" ><c:out value="${endDetails.endHM}" /></td>
                                                    <td  height="30" >Total Odometer Reading(KM)</td>
                                                    <td  height="30" ><c:out value="${endDetails.totalKM}" /></td>
                                                </tr>
                                                <tr>
                                                    <td  height="30" >Total Reefer Reading(HM)</td>
                                                    <td  height="30" ><c:out value="${endDetails.totalHrs}" /></td>
                                                    <td  height="30" >Total Duration Hours</td>
                                                    <td  height="30" ><c:out value="${endDetails.durationHours}" /></td>
                                                    <td  height="30" >Total Days</td>
                                                    <td  height="30" ><c:out value="${endDetails.totalDays}" />

                                                </tr>

                                            </c:forEach >
                                        </table>
                                        <!--<div id="endDetail">-->
                                        <c:if test = "${tripEndDetails != null}" >
                                            <table class="table table-info mb30 table-hover" style="display:none">
                                                <c:forEach items="${tripEndDetails}" var="endDetails">
                                                    <thead><tr>
                                                            <th  colspan="6" >  Edit Trip End Details11</th>
                                                        </tr></thead>
                                                    <tr>
                                                        <td  height="30" >Planned End Date</td>
                                                        <td  height="30" ><c:out value="${endDetails.planEndDate}" /></td>

                                                        <td  height="30" >Planned End Time</td>
                                                        <td  height="30" ><c:out value="${endDetails.planEndTime}" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td  height="30" >End Date</td>
                                                        <td  height="30" >
                                                            <input type="text" name="endDate" id="endDate"  class="datepicker , form-control" style="width:250px;height:40px" onchange="calculateDays();" value=" <c:out value="${endDetails.endDate}" />" >
                                                        <td  height="30" >End Time</td>

                                                        <td  height="25" >HH:<select name="tripEndHour" id="tripEndHour" class="textbox" style="width:60px;height:40px" onchange="calculateDays();" ><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                                            MI:<select name="tripEndMinute" id="tripEndMinute" class="textbox" style="width:60px;height:40px"onchange="calculateDays();"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
                                                    </tr>

                                                    <tr>
                                                        <td ><font color="red">*</font> Trip End Odometer Reading(KM)</td>
                                                        <td ><input type="text" name="endOdometerReading" id="endOdometerReading" class="form-control" style="width:250px;height:40px" value='<c:out value="${endDetails.endOdometerReading}" />' onKeyPress="return onKeyPressBlockCharacters(event);" onchange="calTotalKM();"></td>
                                                        <td ><font color="red">*</font>Trip End Reefer Reading(HM)</td>
                                                        <td ><input type="text" name="endHM" id="endHM" class="form-control" style="width:250px;height:40px" value='<c:out value="${endDetails.endHM}" />' onchange="calTotalHM();" onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                                                    </tr>


                                                    <c:if test="${startTripDetails != null}">
                                                        <c:forEach items="${startTripDetails}" var="tripDetails">
                                                            <input type="hidden" name="startKM" id="startKM" value="<c:out value="${tripDetails.startOdometerReading}"/>" />
                                                            <input type="hidden" name="startHM" id="startHM" value="<c:out value="${tripDetails.startHM}"/>" />
                                                            <input type="hidden" name="startDate" id="startDate" value="<c:out value="${tripDetails.startDate}"/>" />
                                                            <input type="hidden" name="startTime" id="startTime" value="<c:out value="${tripDetails.startTime}"/>" />
                                                        </c:forEach>
                                                    </c:if>


                                                    <tr>
                                                        <td > Total Odometer Reading(KM)</td>
                                                        <td ><input type="text" readonly name="totalKM" id="totalKM" class="form-control" style="width:250px;height:40px" value='<c:out value="${endDetails.totalKM}" />' onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                                                        <td > Total Reefer Reading(HM)</td>
                                                        <td ><input type="text" readonly name="totalHrs" id="totalHrs" class="form-control" style="width:250px;height:40px" value='<c:out value="${endDetails.totalHrs}" />' onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                                                    </tr>
                                                    <tr>

                                                        <td  height="30" >Total Duration Hours</td>
                                                        <td  height="30" ><input type="text" name="tripTransitHour" readonly id="tripTransitHour" class="form-control" style="width:250px;height:40px"  value="<c:out value="${endDetails.durationHours}" />"></td>

                                                        <td  height="30" >Total Days</td>

                                                        <td ><input type="text" name="totalDays1" readonly id="totalDays1" class="form-control" style="width:250px;height:40px"  value="<c:out value="${endDetails.totalDays}" />"></td>


                                                    </tr>

                                                </c:forEach >
                                            </table>
                                            <br/>
                                        </c:if>


                                        <center>
                                            <input type="button" class="btn btn-success" name="Save" style="width:200px;" value="Update Trip End Details" onclick="saveTripEndDetails();" />
                                        </center>
                                        <br>
                                        <!--</div>-->
                                        <c:if test = "${tripUnPackDetails != null}" >
                                            <table style="display:none" >
                                                <thead> <tr>
                                                        <th colspan="10"  align="center" height="30" >Consignment Unloading Details</th>
                                                    </tr>
                                                    <tr>
                                                        <th width="20"  align="center" height="30" >Sno</th>
                                                        <th  height="30" >Product/Article Code</th>
                                                        <th  height="30" >Product/Article Name </th>
                                                        <th  height="30" >Batch </th>
                                                        <th  height="30" ><font color='red'>*</font>No of Packages</th>
                                                        <th  height="30" ><font color='red'>*</font>Uom</th>
                                                        <th  height="30" ><font color='red'>*</font>Total Weight (in Kg)</th>
                                                        <th  height="30" ><font color='red'>*</font>Loaded Package Nos</th>
                                                        <th  height="30" ><font color='red'>*</font>UnLoaded Package Nos</th>
                                                        <th  height="30" ><font color='red'>*</font>Shortage</th>
                                                    </tr></thead>


                                                <%int i2 = 1;%>
                                                <c:forEach items="${tripUnPackDetails}" var="tripunpack">
                                                    <tr>
                                                        <td><%=i2%></td>
                                                        <td><input type="text"  name="productCodes" id="productCodes" value="<c:out value="${tripunpack.articleCode}"/>" readonly/></td>
                                                        <td><input type="text" name="productNames" id="productNames" value="<c:out value="${tripunpack.articleName}"/>" readonly/></td>
                                                        <td><input type="text" name="productbatch" id="productbatch" value="<c:out value="${tripunpack.batch}"/>" readonly/></td>
                                                        <td><input type="text" name="packagesNos" id="packagesNos" value="<c:out value="${tripunpack.packageNos}"/>" readonly/></td>
                                                        <td><input type="text" name="productuom" id="productuom" value="<c:out value="${tripunpack.uom}"/>" readonly/></td>
                                                        <td><input type="text" name="weights" id="weights" value="<c:out value="${tripunpack.packageWeight}"/> " readonly/>
                                                        <td><input type="text" name="loadedpackages" id="loadedpackages<%=i2%>" value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly/>
                                                            <input type="hidden" name="consignmentId" value="<c:out value="${tripunpack.consignmentId}"/>"/>
                                                            <input type="hidden" name="tripArticleId" value="<c:out value="${tripunpack.tripArticleid}"/>"/>
                                                        </td>
                                                        <td><input type="text" name="unloadedpackages" id="unloadedpackages<%=i2%>" onblur="computeShortage(<%=i2%>);"  value="0"  onKeyPress="return onKeyPressBlockCharacters(event);"    /></td>
                                                        <td><input type="text" name="shortage" id="shortage<%=i2%>"  value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly /></td>
                                                    </tr>
                                                    <%i2++;%>
                                                </c:forEach>

                                            </table>

                                        </c:if>

                                        <!--                                    <center>
                                                                                <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save11" /></a>
                                                                            </center>-->
                                    </c:if>

                                </div>

                                <div id="overrideDetail" style="display:none">
                                    <c:if test = "${tripEndDetails != null}" >
                                        <table class="table table-info mb30 table-hover" >
                                            <c:forEach items="${tripEndDetails}" var="endDetails">
                                                <thead> <tr>
                                                        <th  colspan="6" > Trip End Details</th>
                                                    </tr></thead>
                                                <tr>
                                                    <td  height="30" >Planned End Date</td>
                                                    <td  height="30" ><c:out value="${endDetails.planEndDate}" /></td>

                                                    <td  height="30" >Planned End Time</td>
                                                    <td  height="30" ><c:out value="${endDetails.planEndTime}" /></td>
                                                </tr>
                                                <tr>
                                                    <td  height="30" >End Date</td>
                                                    <td  height="30" >
                                                        <input type="text" name="endDate1" id="endDate1"  class="datepicker , form-control" style="width:250px;height:40px" onchange="calculateDays();" value=" <c:out value="${endDetails.endDate}" />" >
                                                    <td  height="30" >End Time</td>

                                                    <td  height="25" >HH:<select name="tripEndHour1" id="tripEndHour1" class="textbox" style="width:60px;height:40px" onchange="calculateDays();" ><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                                        MI:<select name="tripEndMinute1" id="tripEndMinute1" class="textbox" style="width:60px;height:40px"onchange="calculateDays();"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
                                                </tr>

                                                <tr>
                                                    <td ><font color="red">*</font> Trip End Odometer Reading(KM)</td>
                                                    <td ><input type="text" name="endOdometerReading1" id="endOdometerReading1" class="form-control" style="width:250px;height:40px" value='<c:out value="${endDetails.endOdometerReading}" />' onKeyPress="return onKeyPressBlockCharacters(event);" onchange="calTotalKM();"></td>
                                                    <td ><font color="red">*</font>Trip End Reefer Reading(HM)</td>
                                                    <td ><input type="text" name="endHM1" id="endHM1" class="form-control" style="width:250px;height:40px" value='<c:out value="${endDetails.endHM}" />' onchange="calTotalHM();" onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                                                </tr>
                                                <tr>
                                                    <td > Total Odometer Reading(KM)</td>
                                                    <td ><input type="text" readonly name="totalKM1" id="totalKM1" class="form-control" style="width:250px;height:40px" value='<c:out value="${endDetails.totalKM}" />' onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                                                    <td > Total Reefer Reading(HM)</td>
                                                    <td ><input type="text" readonly name="totalHrs1" id="totalHrs1" class="form-control" style="width:250px;height:40px" value='<c:out value="${endDetails.totalHrs}" />' onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                                                </tr>
                                                <tr>

                                                    <td  height="30" >Total Duration Hours</td>
                                                    <td  height="30" ><input type="text" name="tripTransitHour1" readonly id="tripTransitHour1" class="form-control" style="width:250px;height:40px"  value="<c:out value="${endDetails.durationHours}" />"></td>

                                                    <td  height="30" >Total Days</td>

                                                    <td ><input type="text" name="totalDays11" readonly id="totalDays11" class="form-control" style="width:250px;height:40px"  value="<c:out value="${endDetails.totalDays}" />"></td>
                                                </tr>

                                            </c:forEach >
                                        </table>
                                        <br/>
                                        <table>
                                        </table>
                                        <table class="table table-info mb30 table-hover"  >
                                            <thead><tr><th  colspan="6" height="10"> Override End Odometer Readings For Settelment </th></tr></thead>
                                            <tr>

                                                <td ><font color="red">*</font> Trip settelment Odometer Reading(KM)</td>
                                                <td ><input type="text" name="setteledKm" id="setteledKm" class="form-control" style="width:250px;height:40px" value='<c:out value="${setteledKm}" />'onchange="calSettelTotalKM();" onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                                                <td ><font color="red">*</font>Trip settelment Reefer Reading(HM)</td>
                                                <td ><input type="text" name="setteledHm" id="setteledHm" class="form-control" style="width:250px;height:40px" value='<c:out value="${setteledHm}" />'onchange="calSettelTotalHM();" onKeyPress="return onKeyPressBlockCharacters(event);" ></td>
                                            </tr>

                                            <tr>
                                                <td > Total Settelment Odometer Reading(KM)</td>
                                                <td ><input type="text" readonly name="setteltotalKM" id="setteltotalKM" class="form-control" style="width:250px;height:40px" value='<c:out value="${setteledTotalKm}"/>' onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                                                <td > Total Settelment Reefer Reading(HM)</td>
                                                <td ><input type="text" readonly name="setteltotalHrs" id="setteltotalHrs" class="form-control" style="width:250px;height:40px" value='<c:out value="${setteledTotalHm}" />' onKeyPress="return onKeyPressBlockCharacters(event);"></td>
                                            </tr>
                                            <c:if test="${overrideBy != null}">
                                                <tr>
                                                    <td >OverRide By</td>
                                                    <td ><c:out value="${overrideBy}"/></td>
                                                    <td >OverRide On</td>
                                                    <td ><c:out value="${overrideOn}"/></td>
                                                </tr>
                                            </c:if>
                                            <tr>
                                                <td >OverRide Remarks</td>
                                                <td  colspan="3"><textarea name="overrideRemarks" id="overrideRemarks" cols="40" rows="2"><c:out value="${overrideTripRemarks}"/></textarea></td>
                                            </tr>
                                        </table>
                                        <br>
                                    </c:if>

                                    <c:if test = "${tripDetails != null}" >
                                        <c:forEach items="${tripDetails}" var="trip">   
                                            <table class="table table-info mb30 table-hover" >
                                                <thead><tr>
                                                        <th  colspan="4" >Vehicle UnLoading Details</th>
                                                    </tr></thead>
                                                <tr>
                                                    <!--<td ><font color="red">*</font>Vehicle Actual UnLoading Completed Date</td>-->
                                                    <td ><font color="red">*</font>UnLoading Completed Date</td>
                                                    <td ><input type="text" name="vehicleloadreportdate" id="vehicleloadreportdate" class="datepicker , form-control" style="width:250px;height:40px" value="<c:out value="${trip.unLoadingDate}" />"></td>
                                                    <!--<td  height="25" ><font color="red">*</font>Vehicle Actual UnLoading Time </td>-->
                                                    <td  height="25" ><font color="red">*</font>UnLoading Completed Time </td>
                                                    <td  colspan="3" align="left" height="25" >HH:<select name="vehicleloadreporthour" id="vehicleloadreporthour" class="textbox" style="width:60px;height:40px"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                                        MI:<select name="vehicleloadreportmin" id="vehicleloadreportmin" class="textbox" style="width:60px;height:40px"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                                                </tr>
                                                <script>
                                                    document.getElementById("vehicleloadreporthour").value = '<c:out value="${trip.vehicleactreporthour}" />';
                                                    document.getElementById("vehicleloadreportmin").value = '<c:out value="${trip.vehicleactreportmin}" />';
                                                </script>
                                                <tr>
                                                    <td ><font color="red">*</font>Vehicle UnLoading Temperature</td>
                                                    <td colspan="3"><input type="text" name="vehicleloadtemperature" id="vehicleloadtemperature" class="form-control" style="width:250px;height:40px" value="<c:out value="${trip.unLoadingTemperature}" />"></td>
                                                    <!--                                        <td ><font color="red">*</font>Vehicle Log Sheet Number</td>
                                                                                            <td ><input type="text" name="lrNumber" id="lrNumber"  value="<c:out value="${lrNumber}" />" class="form-control" style="width:250px;height:40px"></td>-->
                                                </tr>
                                            </table>
                                        </c:forEach>
                                    </c:if>

                                    <center>
                                        <input type="button" class="btn btn-success" name="Save" style="width:200px;" value="OverRide Trip End Details" onclick="saveTripOverRideDetails();" />
                                    </center>
                                    <br>
                                </div>

                                <c:if test="${fuelTypeId == 1002}">
                                    <div id="bpclDetail" style="display:none">
                                        <% int index10 = 1;%>


                                        <c:if test = "${bpclTransactionHistory != null}" >
                                            <c:set var="totalAmount" value="0"/>
                                            <table class="table table-info mb30 table-hover">
                                                <thead>  <tr>
                                                        <th  height="30" >S No</th>
                                                        <th  height="30" >Trip Code</th>
                                                        <th  height="30" >Vehicle No</th>
                                                        <th  height="30" >Transaction History Id</th>
                                                        <th  height="30" >BPCL Transaction Id</th>
                                                        <th  height="30" >BPCL Account Id</th>
                                                        <th  height="30" >Dealer Name</th>
                                                        <th  height="30" >Dealer City</th>
                                                        <th  height="30" >Transaction Date</th>
                                                        <th  height="30" >Accounting Date</th>
                                                        <th  height="30" >Transaction Type</th>
                                                        <th  height="30" >Currency</th>
                                                        <th  height="30" >Transaction Amount</th>
                                                        <th  height="30" >Volume Document No</th>
                                                        <th  height="30" >Amount Balance</th>
                                                        <th  height="30" >Petromiles Earned</th>
                                                        <th  height="30" >Odometer Reading</th>
                                                    </tr></thead>
                                                    <c:forEach items="${bpclTransactionHistory}" var="bpclDetail">
                                                        <%
                                                                    String classText10 = "";
                                                                    int oddEven10 = index10 % 2;
                                                                    if (oddEven10 > 0) {
                                                                        classText10 = "text1";
                                                                    } else {
                                                                        classText10 = "text2";
                                                                    }
                                                        %>
                                                    <tr>
                                                        <td class="<%=classText10%>" height="30" ><%=index10++%></td>
                                                        <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.tripCode}" /></td>
                                                        <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.vehicleNo}" /></td>
                                                        <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.transactionHistoryId}" /></td>
                                                        <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.bpclTransactionId}" /></td>
                                                        <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.accountId}" /></td>
                                                        <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.dealerName}" /></td>
                                                        <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.dealerCity}" /></td>
                                                        <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.transactionDate}" /></td>
                                                        <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.accountingDate}" /></td>
                                                        <td class="<%=classText10%>" height="30" ><c:out value="${bpclDetail.transactionType}" /></td>
                                                        <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.currency}" /></td>
                                                        <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.amount}" /></td>
                                                        <c:set var="totalAmount" value="${bpclDetail.amount + totalAmount}"  />
                                                        <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.volumeDocNo}" /></td>
                                                        <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.amoutBalance}" /></td>
                                                        <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.petromilesEarned}" /></td>
                                                        <td class="<%=classText10%>" height="30" align="right"><c:out value="${bpclDetail.odometerReading}" /></td>
                                                    </tr>
                                                </c:forEach >

                                                <tr>
                                                    <td  height="30" style="text-align: right" colspan="12">Total Amount</td>
                                                    <td  height="30" style="text-align: right"  ><c:out value="${totalAmount}"/></td>
                                                    <td  height="30" style="text-align: right"  colspan="4">&nbsp;</td>
                                                </tr>
                                            </table>
                                            <br/>
                                            <br/>
                                            <br/>
                                        </c:if>
                                        <br>
                                        <br>
                                    </div>
                                </c:if>




                                <script>
                                    $(".nexttab").click(function() {
                                        var selected = $("#tabs").tabs("option", "selected");
                                        $("#tabs").tabs("option", "selected", selected + 1);
                                    });
                                </script>

                            </div>
                            </form>
                            </body>
                            <div>
                                <div>
                                    <div>
                                        <%@ include file="../common/NewDesign/settings.jsp" %>