<%@page import="ets.domain.trip.business.TripTO"%>
<html>
    <head>
        <%@page import="java.util.Iterator"%>
        <%@page import="java.util.ArrayList"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <style type='text/css'>
        body{
            color:#000000; background-color:#ffffff;
            font-family:arial, verdana, sans-serif; font-size:10pt;}

        fieldset {
            font-size:10pt;
            padding:5px;
            width:700px;
            line-height:5;}

        label:hover {cursor:hand;}

    </style>

    <style type="text/css">
        @media print {
            #printbtn {
                display :  none;
            }
        }
    </style>
    <style type="text/css" media="print">
        @media print
        {
            @page {
                margin-top: 0;
                margin-bottom: 0;
            }
        }
    </style>
    <script>
        function callPrint() {
            //document.getElementById("printbtn").style.display = "none";
            window.print();
        }
    </script>
</head>
<body>
    <form name="lhc"  method="post">                    
        <c:if test="${LHCDetails != null}">
            <c:forEach items="${LHCDetails}" var="lhc">
                <center>
                    <br/>        
                    <fieldset>
                        <table width="700" align="center" id="table" >
                            <tr  height="50">
                                <td colspan="4" align="center"> <u>Lorry Hire Challan</u></td>
                            </tr>
                            <tbody>
                                <tr height="50">
                                    <td>LHC No</td>
                                    <td>:&nbsp;<c:out value="${lhc.lhcNo}"/></td>
                                    <td>LHC Date</td>
                                    <td>:&nbsp;<c:out value="${lhc.lHCdate}"/></td>
                                </tr>
                                
                                  <tr height="50">                
                                    <td>LR Number</td>
                                    <td>:&nbsp;<c:out value="${lhc.lrNumber}"/></td>
                                    <td>LR Date</td>
                                    <td>:&nbsp;<c:out value="${lhc.lrdate}"/></td>
                                </tr>
                                <tr height="50">
                                    <td>Driver Mobile</td>
                                    <td>:&nbsp;<c:out value="${lhc.driverMobile}"/></td>
                                    <td>Vendor Name</td>
                                    <td>:&nbsp;<c:out value="${lhc.vendorName}"/></td>
                                </tr>
                                <tr height="50">
                                    <td>Vehicle No</td>
                                    <td>:&nbsp;<c:out value="${lhc.vehicleNo}"/></td>
                                    <td>Vehicle Type</td>
                                    <td>:&nbsp;<c:out value="${lhc.vehicleTypeName}"/></td>
                                </tr>
                              
                                <tr height="50">                
                                    <td>Agreed Rate</td>
                                    <td>:&nbsp;<c:out value="${lhc.rate}"/></td>
                                    <td>Agreed Advance</td>
                                    <td>:&nbsp;<c:out value="${lhc.requestedAdvance}"/></td>
                                </tr>
                                
                                <!--Driver Name-->
                                <%--<c:out value="${lhc.driverName}"/>--%>
                            </tbody>
                        </table>
                    </fieldset>
                </center>
                    </c:forEach>
        </c:if><br>
        <center><input type="button" id="printbtn" name="Save" value="Print" onclick="callPrint()"/></center>
    </form>
</body>
</html>