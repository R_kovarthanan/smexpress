<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>


<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">
        function printLhc(tripSheetId) {
            window.open('/throttle/handleLHCPrint.do?tripSheetId=' + tripSheetId + '&value=1', 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
        }

        function touchPoint(tripSheetId) {
            window.open('/throttle/handleTouchPoint.do?tripSheetId=' + tripSheetId + '&value=1', 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
        }

        function touchPointPrint(tripSheetId, docketNo) {

            window.open('/throttle/handleTripSheetPrint.do?tripSheetId=' + tripSheetId + '&printDocketNo=' + docketNo + '&value=1', 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
        }
        function mailTriggerPage(tripId) {
            window.open('/throttle/viewTripDetailsMail.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function viewCurrentLocation(vehicleNo) {
            //var url = 'http://ivts.noviretechnologies.com/IVTS/reportEngServ.do?username=serviceuser&password=7C53C003126C10E1091C73F4F945FEB4&companyId=759&reportRef=ref_currentStatusMapServ&param0=759&param1=ref_currentStatusMapServ&param2=&param3=&param4=&param5=&param6=&param7=&param8=&param9=&param10=&param11='+vehicleRegNo;
            //alert(url);
            window.open('/throttle/viewVehicleLocation.do?vehicleNo=' + vehicleNo, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }

        function viewTripDetails(tripId, vehicleId) {
            window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId + '&vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function viewVehicleDetails(vehicleId) {
            window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script type="text/javascript">
        function setValues() {
            if ('<%=request.getAttribute("vehicleTypeId")%>' != 'null') {
                document.getElementById('vehicleTypeId').value = '<%=request.getAttribute("vehicleTypeId")%>';
            }
            if ('<%=request.getAttribute("fromDate")%>' != 'null') {
                document.getElementById('fromDate').value = '<%=request.getAttribute("fromDate")%>';
            }
            if ('<%=request.getAttribute("toDate")%>' != 'null') {
                document.getElementById('toDate').value = '<%=request.getAttribute("toDate")%>';
            }
            if ('<%=request.getAttribute("fleetCenterId")%>' != 'null') {
                document.getElementById('fleetCenterId').value = '<%=request.getAttribute("fleetCenterId")%>';
            }
            if ('<%=request.getAttribute("cityFromId")%>' != 'null') {
                document.getElementById('cityFromId').value = '<%=request.getAttribute("cityFromId")%>';
            }
            if ('<%=request.getAttribute("cityFrom")%>' != 'null') {
                document.getElementById('cityFrom').value = '<%=request.getAttribute("cityFrom")%>';
            }
            if ('<%=request.getAttribute("vehicleId")%>' != 'null') {
                document.getElementById('vehicleId').value = '<%=request.getAttribute("vehicleId")%>';
            }
            if ('<%=request.getAttribute("zoneId")%>' != 'null') {
                document.getElementById('zoneId').value = '<%=request.getAttribute("zoneId")%>';
            }

            if ('<%=request.getAttribute("tripSheetId")%>' != 'null') {
                document.getElementById('tripSheetId').value = '<%=request.getAttribute("tripSheetId")%>';
            }
            if ('<%=request.getAttribute("customerId")%>' != 'null') {
                document.getElementById('customerId').value = '<%=request.getAttribute("customerId")%>';
            }
            if ('<%=request.getAttribute("tripStatusId")%>' != 'null') {
                document.getElementById('tripStatusId').value = '<%=request.getAttribute("tripStatusId")%>';
            }
            if ('<%=request.getAttribute("podStatus")%>' != 'null') {
                document.getElementById('podStatus').value = '<%=request.getAttribute("podStatus")%>';
            }
            if ('<%=request.getAttribute("statusId")%>' != 'null') {
                document.getElementById('stsId').value = '<%=request.getAttribute("statusId")%>';
            }
        }

        function submitPage() {
            document.tripSheet.action = '/throttle/viewTripSheets.do?statusId=' + document.getElementById('stsId').value;
            document.tripSheet.submit();
        }

        function submitPage1() {
            document.tripSheet.action = '/throttle/viewTripSheets.do?param=exportExcel';
            document.tripSheet.submit();
        }



    </script>

    <script type="text/javascript">
        var httpRequest;
        function getLocation() {
            var zoneid = document.getElementById("zoneId").value;
            if (zoneid != '') {

                // Use the .autocomplete() method to compile the list based on input from user
                $('#cityFrom').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getLocationName.do",
                            dataType: "json",
                            data: {
                                cityId: request.term,
                                zoneId: document.getElementById('zoneId').value
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#cityFromId').val(tmp[0]);
                        $('#cityFrom').val(tmp[1]);
                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

            }
        }

    </script>

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <c:out value="${menuPath}"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
                <li class=""><c:out value="${menuPath}"/></li>

            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="sorter.size(20);
                        setValues();">
                    <form name="tripSheet" method="post">
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <%@include file="/content/common/message.jsp" %>
                        <table class="table table-info mb30 table-hover" style="width:50%">
                            <thead><tr><th colspan="6"><c:out value="${menuPath}"/></th></tr></thead>
                            <tr>
                                <td style="display:none" width="80" >Billing Type </td>
                                <td style="display:none" width="80"> <select name="billingType" id="billingType" class="form-control" style="width:175px;height:40px" >
                                        <option value="" selected>--Select--</option>
                                        <option value="1" >Primary</option>
                                        <option value="2" >Secondary</option>
                                        <option value="3" >All</option>
                                    </select></td>
                                <td style="display:none" width="80" >Vehicle No</td>
                                <td style="display:none" width="80"> <select name="vehicleId" id="vehicleId" class="form-control" style="width:175px;height:40px" >
                                        <c:if test="${vehicleList != null}">
                                            <option value="" selected>--Select--</option>
                                            <c:forEach items="${vehicleList}" var="vehicleList">
                                                <option value='<c:out value="${vehicleList.vehicleId}"/>'><c:out value="${vehicleList.regNo}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                </td>
                                <td   >Trip Sheet No</td>
                                <td  height="25"><input name="tripSheetId" id="tripSheetId" type="text" class="form-control" style="width:175px;height:40px" value="<c:out value="${tripSheetId}"/>" size="20" style="width: 110px"></td>
                                <td   >Docket No</td>
                                <td  height="25"><input name="docketNo1" id="docketNo1" type="text" class="form-control" style="width:175px;height:40px" value="<c:out value="${docketNo1}"/>" size="20" style="width: 110px"></td>

                                <td style="display:none;" >Status </td>
                                <td style="display:none;" > <select name="tripStatusId" id="tripStatusId" class="form-control" style="width:175px;height:40px" >
                                        <c:if test="${statusDetails != null}">
                                            <option value="" selected>--Select--</option>
                                            <c:forEach items="${statusDetails}" var="statusDetails">
                                                <c:if test="${statusDetails.statusId == 8 || statusDetails.statusId == 10 || statusDetails.statusId == 12 || statusDetails.statusId == 13 || statusDetails.statusId == 14 || statusDetails.statusId == 16 }">
                                                    <option value='<c:out value="${statusDetails.statusId}"/>'><c:out value="${statusDetails.statusName}"/></option>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </select></td>
                            </tr>
                            <tr style="display:none" >
                                <td width="80" >Vehicle Type </td>
                                <td width="80" > <select name="vehicleTypeId" id="vehicleTypeId" class="form-control" style="width:175px;height:40px" >
                                        <c:if test="${vehicleTypeList != null}">
                                            <option value="" selected>--Select--</option>
                                            <c:forEach items="${vehicleTypeList}" var="vehicleTypeList">
                                                <option value='<c:out value="${vehicleTypeList.vehicleTypeName}"/>'><c:out value="${vehicleTypeList.vehicleTypeName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select></td>
                                <td width="80" >Fleet Center</td>
                                <td width="80"> <select name="fleetCenterId" id="fleetCenterId" class="form-control" style="width:175px;height:40px">
                                        <c:if test="${companyList != null}">
                                            <option value="" selected>--Select--</option>
                                            <c:forEach items="${companyList}" var="companyList">
                                                <option value='<c:out value="${companyList.cmpId}"/>'><c:out value="${companyList.name}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select></td>
                                <td width="80" >Zone </td>
                                <td width="80" > <select name="zoneId" id="zoneId" class="form-control" style="width:175px;height:40px" onchange="getLocation();" >
                                        <c:if test="${zoneList != null}">
                                            <option value="" selected>--Select--</option>
                                            <c:forEach items="${zoneList}" var="zoneList">
                                                <option value='<c:out value="${zoneList.zoneId}"/>'><c:out value="${zoneList.zoneName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select></td>

                            </tr>
                            <tr style="display:none" >
                                <td width="80">Location</td>
                                <td width="80">
                                    <input type="hidden" name="cityFromId" id="cityFromId" value="<c:out value="${cityName}"/>" class="textbox" >
                                    <input type="text" name="cityFrom" id="cityFrom" value="<c:out value="${cityId}"/>" class="form-control" style="width:175px;height:40px">
                                </td>
                                <td width="80">Customer</td>
                                <td  width="80"> <select name="customerId" id="customerId" class="form-control" style="width:175px;height:40px" >
                                        <c:if test="${customerList != null}">
                                            <option value="" selected>--Select--</option>
                                            <c:forEach items="${customerList}" var="customerList">
                                                <option value='<c:out value="${customerList.customerName}"/>'><c:out value="${customerList.customerName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td  ><font color="red">*</font>From Date</td>
                                <td ><input name="fromDate" id="fromDate" type="text" class="datepicker , form-control" style="width:175px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>

                                <td ><font color="red">*</font>To Date</td>
                                <td ><input name="toDate" id="toDate" type="text" class="datepicker , form-control" style="width:175px;height:40px" onClick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>

                                <td style="display:none" width="80" >POD Status </td>
                                <td style="display:none" width="80"> <select name="podStatus" id="podStatus" class="form-control" style="width:175px;height:40px" >
                                        <option value="" selected>--Select--</option>
                                        <option value="0" >POD Uploaded</option>
                                        <option value="1" >POD not Uploaded</option>
                                    </select></td>
                            </tr>

                        </table>
                        <center>
                            <input type="hidden"   value="<c:out value="${statusId}"/>" class="text" name="stsId" id="stsId">
                            <c:if test="${podStatus != ''}">                                
                                <input type="button"   value="POD Download" class="btn btn-success" name="pod" onclick="podDownload();" > &emsp;
                            </c:if>
                            <input type="hidden"   value="<c:out value="${tripType}"/>" class="text" name="tripType">
                            <input type="button"   value="Search" class="btn btn-success" name="Search" onclick="submitPage();" > &emsp;
                            <input type="button" class="btn btn-success" name="ExportExcel" id="ExportExcel" onclick="submitPage1(this.name);" value="ExportExcel">


                        </center>
                        <br>
                        <script>
                            function checkAll() {
                                var inputs = document.getElementsByName("podCopy");
                                for (var i = 0; i < inputs.length; i++) {
                                    if (inputs[i].type == "checkbox") {
                                        if (inputs[i].checked == true) {
                                            inputs[i].checked = false;
                                        } else if (inputs[i].checked == false) {
                                            inputs[i].checked = true;
                                        }
                                    }
                                }
                            }
                        </script>
                        <br>
                        <c:if test="${tripDetails != null}">
                            <table class="table table-info mb30 table-hover " id="table"  style="font-style:normal;;font-size:10pt;font-family:Arial;" size="2">
                                <thead>
                                    <tr >
                                        <th >Sno</th>                                                                                
                                        <th >
                                            <c:if test="${podStatus != ''}"> 
                                                <input type="checkbox" name="selectAll" id="selectAll" style="width:15px;height:12px;" onclick="checkAll();"/> </c:if>
                                                POD</th>
                                            <th >Vehicle No </th>
                                            <th >Vendor</th>
                                            <th >Trip Code</th>
                                            <th >Customer Name </th>
                                            <th >Customer Type </th>                        
                                            <th >Billing Type </th>
                                            <th >Route </th>                        
                                            <th >Vehicle Type </th>
                                            <th >Trip Schedule</th>                        
                                            <th >Trip Status</th>                        
                                            <th >select</th>                        

                                        </tr>
                                    </thead>
                                    <tbody>
                                    <% int index = 1;%>
                                    <c:forEach items="${tripDetails}" var="tripDetails">
                                        <%
                                                    String className = "text1";
                                                    if ((index % 2) == 0) {
                                                        className = "text1";
                                                    } else {
                                                        className = "text2";
                                                    }
                                        %>

                                    <script>


                                        function podDownload() {
                                            var selectedConsignment = document.getElementsByName('podCopy');
                                            var podcopy = [];
                                            var cntr = 0;
                                            for (var i = 0; i < selectedConsignment.length; i++) {
                                                if (selectedConsignment[i].checked == true) {
                                                    podcopy[i] = selectedConsignment[i].value;
                                                    cntr++;
                                                }
                                            }

                                            if (cntr > 0) {
                                             //   window.open('/throttle/podDownload.do?podCopy=' + podcopy, 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');

                                                document.tripSheet.action = '/throttle/viewTripSheets.do?param=podCopy&podCopys='+podcopy;
                                                document.tripSheet.submit();

                                            } else {
                                                alert("Please select any trip");
                                            }
                                        }
                                    </script>


                                    <input type="hidden" value="<c:out value="${tripDetails.customerOrderReferenceNo}"/>">
                                    <tr height="30">
                                        <td class="<%=className%>" width="40" align="left">
                                            <%=index%>
                                        </td>

                                        <c:if test="${tripType == 1}">
                                            <td class="<%=className%>" align="center">

                                                <c:if test="${podStatus != ''}">
                                                    <input type="checkbox" style="width:15px;height:12px;" name="podCopy" id="podCopy<%=index%>" value="<c:out value="${tripDetails.tripSheetId}"/>" />
                                                </c:if> &nbsp;&nbsp;
                                                <c:if test="${tripDetails.statusId == 10 || tripDetails.statusId == 12 || tripDetails.statusId == 13  || tripDetails.statusId == 14  || tripDetails.statusId == 16}">
                                                    <c:if test="${tripDetails.customerName != 'Empty Trip'}">
                                                        <c:if test="${tripDetails.podCount == '0' && tripDetails.customerOrderReferenceNo != 'Empty Trip'}">
                                                            <a href="viewTripPod.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">  <img src="images/Podinactive.png" alt="Y"   title="click to upload pod"/></a>
                                                            </c:if>
                                                            <c:if test="${tripDetails.podCount != '0' && tripDetails.customerOrderReferenceNo != 'Empty Trip'}">
                                                            <a href="viewTripPod.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">  <img src="images/Podactive.png" alt="Y"   title="click to upload pod"/></a>
                                                            </c:if>
                                                        </c:if>
                                                    </c:if>
                                            </td>
                                        </c:if>
                                        <td class="<%=className%>" width="120">
                                            <a href="#" onclick="viewVehicleDetails('<c:out value="${tripDetails.vehicleId}"/>')"><c:out value="${tripDetails.vehicleNo}"/></a><br><font color="red"><c:out value="${tripDetails.oldVehicleNo}"/></font></td>
                                        <td class="<%=className%>" width="120"><c:out value="${tripDetails.vendorName}"/></td>
                                        <td class="<%=className%>" width="120" >
                                            <c:if test="${tripDetails.oldVehicleNo != null}">
                                                <a href="#" onclick="viewTripDetails('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>', 1);"><c:out value="${tripDetails.tripCode}"/></a>
                                            </c:if>
                                            <c:if test="${tripDetails.oldVehicleNo == null}">
                                                <a href="#" onclick="viewTripDetails('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>', 0);"><c:out value="${tripDetails.tripCode}"/></a>
                                            </c:if>
                                        </td>


                                        <td  align="center" class="<%=className%>" width="150">
                                            <c:if test="${tripDetails.customerName != 'Empty Trip'}">
                                                <c:out value="${tripDetails.customerName}"/>
                                            </c:if>
                                            <c:if test="${tripDetails.customerName == 'Empty Trip'}">
                                                <img src="images/emptytrip.png" alt="EmptyTrip"   title="Empty Trip"/>
                                            </c:if></td>

                                        <td class="<%=className%>" align="center">
                                            <c:if test="${tripDetails.customerType == 'Contract'}">
                                                <img src="images/contract.png" alt="Y"   title="contract customer"/>
                                            </c:if>
                                            <c:if test="${tripDetails.customerType == 'Walk In'}">
                                                <img src="images/walkin.gif" alt="Y"  title="walkin customer"/>
                                            </c:if></td>

                                        <td class="<%=className%>" ><c:out value="${tripDetails.billingType}"/></td>
                                        <td class="<%=className%>" >                                
                                            <c:if test="${tripDetails.routeNameStatus > '2'}">
                                                <img src="images/Black_star1.png" alt="MultiPoint"  title=" MultiPoint"/><c:out value="${tripDetails.routeInfo}"/>
                                            </c:if>
                                            <c:if test="${tripDetails.routeNameStatus <= '2'}">
                                                <c:out value="${tripDetails.routeInfo}"/>
                                            </c:if>
                                        </td>

                                        <td class="<%=className%>" ><c:out value="${tripDetails.vehicleTypeName}"/></td>
                                        <td class="<%=className%>" ><c:out value="${tripDetails.tripScheduleDate}"/> &nbsp; <c:out value="${tripDetails.tripScheduleTime}"/></td>

                                        <td class="<%=className%>" >
                                            <c:if test="${tripDetails.extraExpenseStatus == 0}">
                                                <c:out value="${tripDetails.status}"/>
                                            </c:if>
                                            <c:if test="${tripDetails.extraExpenseStatus == 1}">
                                                <c:if test="${tripDetails.statusId == 14}">
                                                    <c:out value="${tripDetails.status}"/>
                                                </c:if>
                                                <c:if test="${tripDetails.statusId != 14}">
                                                    FC Closure
                                                </c:if>
                                            </c:if>
                                        </td>


                                        <c:if test="${roleId != '1030'}">

                                            <td class="<%=className%>" >

                                                <c:if test="${tripDetails.emptyTripApprovalStatus == 0}">
                                                    Empty Trip Waiting For Approval
                                                </c:if>

                                                <c:if test="${tripDetails.emptyTripApprovalStatus == 1}">


                                                    <c:if test="${tripDetails.statusId == 6}">
                                                        <a href="viewTripSheetForVehicleAllotment.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentOrderNos=<c:out value="${tripDetails.consignmentId}"/>&customerName=<c:out value="${tripDetails.customerName}"/>">
                                                            <!--<i class="fa fa-tasks" aria-hidden="true" title="Allot Vehicle"></i></a>-->

                                                        </c:if>
                                                        <c:if test="${tripDetails.statusId == 7}">
                                                            <c:if test="${RoleId != 1033}">
                                                                <!--<a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Freeze</a>-->
                                                            </c:if>
                                                        </c:if>
                                                        <c:if test="${tripDetails.statusId == 8}">
                                                            <c:if test="${RoleId != 1033}">
                                                                <c:if test="${tripDetails.nextTrip != 2}">
                                                                    <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>"  style="color:#272E39;"   >
                                                                        <!--<i class="fa fa-undo" aria-hidden="true" title="UnFreeze" ></i>-->
                                                                        <!--<span class="label label-warning">UnFreeze</span> </a>-->
                                                                    </c:if>
                                                                </c:if>
                                                                <c:if test="${tripDetails.nextTrip == 0 && tripDetails.nextTripCountStatus == 0}">
                                                                    <c:if test="${tripDetails.tripCountStatus == 0 || tripDetails.tripCountStatus == 1}">
                                                                        <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>"  style="color:#272E39;"   >
                                                                            &nbsp; <span class="label label-success">Start Trip</span> </a>
                                                                    </c:if><br/> 
                                                                </c:if>
                                                            </c:if>

                                                            <c:if test="${tripDetails.statusId > 8}">

                                                                <a href="#" style="color:#ff6699;"  onclick="printLhc('<c:out value="${tripDetails.tripSheetId}"/>')" >
                                                                    <span class="label label-default">LHC-Print</span> <br>
                                                                </a>     
                                                                <br/>
                                                            </c:if>
                                                            <c:if test="${tripDetails.nextTrip != 0 }">
                                                                <c:if test="${tripDetails.tripCountStatus == 0 || tripDetails.tripCountStatus == 1}">
                                                                    &nbsp; <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>" style="color:#272E39;" >
                                                                        &nbsp; <span class="label label-success">Start Trip</span> </a>
                                                                    </c:if>
                                                                </c:if>
                                                                

                                                                
                                                                        <a href="payManualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">customer Advance</a><br>
                                                                        <br>
                                                                
                                                              
                                                                 <a href="payVendorfinance.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">
                                                                 
                                                                <span class="label label-default">Vendor Advance</span> <br>
                                                            </a>     
                                                                 <br>
                                                                   
                                                                
                                                            

                                                            <c:if test="${tripDetails.statusId == 10}">
                                                                <c:if test="${tripDetails.routeNameStatus > '2'}">
                    <!--                                                <a href="viewTripRouteDetails.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">
                                                                    <span class="label label-info">Loading&Unloading</span> 
                                                                    
                                                                    </a>-->
                                                                </c:if>
                                                            </c:if>
                                                                        
                                                           <c:if test="${tripDetails.statusId > 8}">
                                                                    <a href="#" style="color:#1EE932;"  onclick="touchPointPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.lrNumber}"/>')" >
                                                                        <span class="label label-success">LR-Print </span> 
                                                                    </a>     
                                                            </c:if>
                                                            <c:if test="${tripDetails.statusId == 10}">
                                                                <c:if test="${tripDetails.emptyTrip == 0}">
                                                                    <!--<a href="viewWFUTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">-->
                                                                    <!--                                                 <span class="label label-default">WFU</span>  -->

                                                                    <a href="viewTripSheetForVehicleChange.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">
                                                                        <!--<span class="label label-warning">Change Vehicle</span>-->
                                                                    </a>
                                                                </c:if>

                                                                <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 0}">

                                                                    <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">
                                                                        <span class="label label-danger">End Trip</span> <br>
                                                                    </a>
                                                                </c:if>
                                                                <c:if test="${tripDetails.paymentType == 4 && tripDetails.advanceToPayStatus == 0}">

                                                                    <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">
                                                                        <span class="label label-danger">End Trip</span> <br>

                                                                    </a>
                                                                </c:if>
                                                                <c:if test="${tripDetails.paymentType == 1 || tripDetails.paymentType == 2 || tripDetails.paymentType == 0 }">
                                                                    <c:if test="${tripDetails.routeNameStatus > '2' && tripDetails.touchPointStatus==0 }">
                                                                        <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">
                                                                            <span class="label label-danger">End Trip</span> <br>
                                                                            <br/>
                                                                        </a>
                                                                    </c:if>
                                                                    <c:if test="${tripDetails.routeNameStatus <= '2'}">
                                                                        <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">
                                                                            <span class="label label-danger">End Trip</span> <br>
                                                                            <br/>
                                                                        </a>
                                                                    </c:if>
                                                                </c:if>
                                                                    
                                                                <c:if test="${tripDetails.statusId > 8}">
                                                                    <a href="#" style="color:#1EE932;"  onclick="touchPointPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.lrNumber}"/>')" >
                                                                        <span class="label label-success">LR-Print </span> 
                                                                    </a>     
                                                                    <br>
                                                                    <c:if test="${tripDetails.routeNameStatus > '2' && tripDetails.touchPointStatus==0}">
                                                                        <a href="#" style="color:#ff6699;"  onclick="touchPointPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.touchPointDocketno}"/>')" >
                                                                            <span class="label label-info">Touch Point Print</span> 
                                                                        </a>                                                       
                                                                        <br/>
                                                                    </c:if>                                                 
                                                                    <c:if test="${tripDetails.routeNameStatus > '2' && tripDetails.touchPointStatus>0}">                                                         
                                                                        <a href="#" style="color:#ff6699;"  onclick="touchPoint('<c:out value="${tripDetails.tripSheetId}"/>')" >
                                                                            <span class="label label-info">Touch Point Update</span> 
                                                                        </a>     
                                                                        <br/>
                                                                    </c:if>

                                                                </c:if>



                                                                <c:if test="${RoleId == 1023 || RoleId == 1033 || RoleId == 1041 || RoleId == 1032 || RoleId == 1016}">  

                                                                    <c:if test="${tripDetails.tripType == 2 && tripDetails.fuelTypeId == 1003}">
                                                                        <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                                            <a href="payManualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">pay</a><br>
                                                                        </c:if>
                                                                        <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) < 1}">
                                                                            <b>rcmpaid</b><a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">
                                                                                <!--<span class="label label-primary">Request Advance</span>-->
                                                                            </a>
                                                                        </c:if>
                                                                    </c:if>
                                                                    <c:if test="${tripDetails.tripType == 2 && tripDetails.fuelTypeId == 1002}">
                                                                        <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                                            <a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">
                                                                                <!--<span class="label label-primary">Request Advance</span>-->
                                                                            </a>
                                                                        </c:if>
                                                                        <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) < 1}">
                                                                            <b>rcmpaid</b><a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">
                                                                                <!--<span class="label label-primary">Request Advance</span>-->                                                        
                                                                            </a>
                                                                        </c:if>
                                                                    </c:if>

                                                                    <c:if test="${tripDetails.tripType != 2}">
                                                                        <a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">
                                                                            <!--<span class="label label-primary">Request Advance</span>-->
                                                                        </a>
                                                                    </c:if>
                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${tripDetails.statusId == 18}">
                                                                <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 0}">
                                                                    &nbsp;
                                                                    <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                                                </c:if>
                                                                <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 1}">
                                                                    &nbsp;
                                                                    <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                                                </c:if>
                                                                <c:if test="${tripDetails.paymentType == 4 && tripDetails.advanceToPayStatus == 0}">
                                                                    &nbsp;
                                                                    <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                                                </c:if>
                                                                <c:if test="${tripDetails.paymentType == 4 && tripDetails.advanceToPayStatus == 1}">
                                                                    &nbsp;
                                                                    <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">End</a>
                                                                </c:if>
                                                                <c:if test="${tripDetails.paymentType == 1 || tripDetails.paymentType == 2 || tripDetails.paymentType == 0 }">
                                                                    &nbsp;
                                                                    <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>"><span class="label label-danger">End Trip</span></a><br>
                                                                </c:if>
                                                                &nbsp;
                                                                <c:if test="${RoleId == 1023 || RoleId == 1033 || RoleId == 1032}">
                                                                </c:if>
                                                            </c:if>

                                                            <c:if test="${tripDetails.statusId == 12 && tripDetails.oldVehicleNo == null}">
                                                                <c:if test="${tripDetails.podCount == '0' && tripDetails.customerOrderReferenceNo != 'Empty Trip' }">
                                                                    <br><a href="viewTripPod.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>"><font color="red">Upload POD</font></a>
                                                                        </c:if>
                                                                        <c:if test="${RoleId != 1033 && tripDetails.podCount >= '1' || tripDetails.customerOrderReferenceNo == 'Empty Trip'}">
                                                                    <a href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />&orderType=<c:out value="${tripDetails.orderType}" />">
                                                                    <br>    <span class="label label-warning">Trip Closure</span>
                                                                    </a>
                                                                    &nbsp;
                                                                    <c:if test="${tripDetails.extraExpenseStatus == 1}">
                                                                        <img title="Extra Expense" alt="Extra Expense" src="images/thumbDone.jpg" style="width: 30px;height: 30px" >
                                                                    </c:if>
                                                                </c:if>
                                                                <c:if test="${RoleId == 1033 && tripDetails.extraExpenseStatus == 0}">
                                                                    <a href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />&orderType=<c:out value="${tripDetails.orderType}" />&fclogin=Yes">
                                                                        <br>   <span class="label label-warning">Trip Closure</span>
                                                                    </a>
                                                                    &nbsp;
                                                                </c:if>

                                                                <c:if test="${RoleId == 1033 && tripDetails.extraExpenseStatus == 1}">
                                                                    <img title="Extra Expense" alt="Extra Expense" src="images/thumbDone.jpg" style="width: 30px;height: 30px" >
                                                                </c:if>

                                                            </c:if>
                                                            <c:if test="${tripDetails.statusId == 12 && tripDetails.oldVehicleNo != null }">
                                                                <c:if test="${RoleId != 1033 &&  tripDetails.podCount != '0'}">
                                                                    <a href="viewVehicleTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />&orderType=<c:out value="${tripDetails.orderType}" />">
                                                                        <br><span class="label label-warning">Trip Closure</span>
                                                                    </a>
                                                                    &nbsp;
                                                                    <c:if test="${tripDetails.extraExpenseStatus == 1}">
                                                                        <img title="Extra Expense" alt="Extra Expense" src="images/thumbDone.jpg" style="width: 30px;height: 30px" >
                                                                    </c:if>
                                                                </c:if>
                                                                <c:if test="${tripDetails.podCount == '0' && RoleId != 1033 }">
                                                                    <a href="viewTripPod.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>"><font color="red">Upload POD</font></a>
                                                                        </c:if>

                                                                <c:if test="${RoleId == 1033 && tripDetails.extraExpenseStatus == 0}">
                                                                    <a href="viewVehicleTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />">
                                                                        <br><i class="fa fa-sign-out " aria-hidden="true" title="Trip Closure" ></i></a>
                                                                    &nbsp;
                                                                </c:if>
                                                                <c:if test="${RoleId == 1033 && tripDetails.extraExpenseStatus == 1}">
                                                                    <img title="Extra Expense" alt="Extra Expense" src="images/thumbDone.jpg" style="width: 30px;height: 30px" >
                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${tripDetails.statusId >= 13}">

                                                                &nbsp;
                                                            </c:if>
                                                            <c:if test="${tripDetails.statusId == 13 && tripDetails.oldVehicleNo != null}">
                                                                <a href="viewVehicleTripSettlement.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">
                                                                    <!--<span class="label label-success">Settlement</span>-->
                                                                </a>
                                                            </c:if>
                                                            </br>
                                                            <c:if test="${tripDetails.statusId == 13 && tripDetails.oldVehicleNo == null}">
                                                                <a href="viewTripSettlement.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">
                                                                    <!--<span class="label label-success">Settlement</span>-->
                                                                </a>
                                                                </br>
                                                            </c:if>
                                                            <c:if test="${tripDetails.statusId == 13 || tripDetails.statusId == 16 }">
                                                            <!--<a href="handleViewTripExpenses.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&mailStatus=<c:out value="${tripDetails.mailStatus}"/>"><span class="label label-primary">Finance Closure</span></a>	  &nbsp;-->                                                                                           
                                                            </c:if>



                                                        </c:if>
                                                        </td>
                                                    </c:if>

                                                    </tr>
                                                    <!--<a href="#" onClick="viewCurrentLocation('<c:out value="${tripDetails.vehicleNo}" />');"><c:out value="${tripDetails.location}" />.</a>-->
                                                    <%index++;%>
                                                </c:forEach>
                                                </tbody>
                                                </table>
                                            </c:if>
                                            <script language="javascript" type="text/javascript">
                                                setFilterGrid("table");
                                            </script>
                                            <div id="controls">
                                                <div id="perpage">
                                                    <select onchange="sorter.size(this.value)">
                                                        <option value="5" >5</option>
                                                        <option value="10">10</option>
                                                        <option value="20" selected="selected">20</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                    <span>Entries Per Page</span>
                                                </div>
                                                <div id="navigation">
                                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                                </div>
                                                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                                            </div>
                                            <script type="text/javascript">
                                                var sorter = new TINY.table.sorter("sorter");
                                                sorter.head = "head";
                                                sorter.asc = "asc";
                                                sorter.desc = "desc";
                                                sorter.even = "evenrow";
                                                sorter.odd = "oddrow";
                                                sorter.evensel = "evenselected";
                                                sorter.oddsel = "oddselected";
                                                sorter.paginate = true;
                                                sorter.currentid = "currentpage";
                                                sorter.limitid = "pagelimit";
                                                sorter.init("table", 0);
                                            </script>
                                            </form>
                                            </body>
                                            </div>
                                            </div>
                                            </div>
                                            <%@ include file="../common/NewDesign/settings.jsp" %>