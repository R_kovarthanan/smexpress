<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.trip.business.TripTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="BPCLTransaction" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "Invoice Freight-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

<c:if test="${invoiceHeader != null}">
    <table align="center" border="1" id="table" class="sortable" width="100%" >

        <thead>
            <tr height="50">
                <th width="150" align="center"><h3>S.No</h3></th>
                <th width="150" align="center"><h3>Trip code</h3></th>
                <th width="150" align="center"><h3>Cnote No</h3></th>
                <th width="150" align="center"><h3>Customer Name</h3></th>
                <th width="150" align="center"><h3>Consignor Name</h3></th>
                <th width="150" align="center"><h3>Consignee Name</h3></th>
                <th width="150" align="center"><h3>Customer Type</h3></th>
                <th width="150" align="center"><h3>vehicle Type</h3></th>
                <th width="150" align="center"><h3>Route Info</h3></th>
                <th width="150" align="center"><h3>Start Time</h3></th>
                <th width="150" align="center"><h3>End Time</h3></th>
                <th width="150" align="center"><h3>Trip Status</h3></th>
                <th width="150" align="center"><h3>Estimated Revenue</h3></th>
                <th width="150" align="center"><h3>Bill With Freight</h3></th>
                
            </tr>
        </thead>
        <tbody>
            <% int index = 1;%>
            <c:forEach items="${invoiceHeader}" var="BPCLTD">
                <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                %>
                <tr>
                    <td class="<%=classText%>"><%=index++%></td>
                    <td width="150" class="<%=classText%>"><c:out value="${BPCLTD.tripCode}"/></td>
                    <td width="150" class="<%=classText%>"><c:out value="${BPCLTD.cNotes}"/></td>
                    <td width="150" class="<%=classText%>"><c:out value="${BPCLTD.customerName}"/></td>
                    <td width="150" class="<%=classText%>"><c:out value="${BPCLTD.consigneeName}"/></td>
                    <td width="150" class="<%=classText%>"><c:out value="${BPCLTD.consignorName}"/></td>
                    <td width="150" class="<%=classText%>"><c:out value="${BPCLTD.customerType}"/></td>
                    <td width="150" class="<%=classText%>"><c:out value="${BPCLTD.vehicleType}"/></td>
                    <td width="150" class="<%=classText%>" ><c:out value="${BPCLTD.routeInfo}"/></td>
                    <td width="150" class="<%=classText%>"  ><c:out value="${BPCLTD.startDate}"/></td>
                    <td width="150" class="<%=classText%>"  ><c:out value="${BPCLTD.endDate}"/></td>
                    <td width="150" class="<%=classText%>"  >Billed</td>
                    <td width="150" class="<%=classText%>"  ><c:out value="${BPCLTD.estimatedRevenue}"/></td>
                    <td width="150" class="<%=classText%>"  ><c:out value="${BPCLTD.expenseToBeBilledToCustomer}"/></td>
                  
                </tr>

            </c:forEach>
        </tbody>
    </table>
   </c:if>
            <% int cntr = 1;%>
            <table class="table table-info mb30 table-hover" id="bg" border="1">>	
                <thead>
                        <tr>
                            <th  >Sno</th>
                            <th  >Trip Code</th>
                            <th  >Expense Description</th>
                            <th  >Expense Type</th>
                            <th  >Remarks</th>
                            <!--<th  >Expense Type</th>-->
                            <th  >Pass Through</th>
                            <th  >Margin</th>
                            <th  >Tax (%)</th>
                            <th  >Expense Value</th>
                        </tr>
                </thead>
                      <c:set var="rembType" value="0" />
                      <c:set var="billToCustomerType" value="0" />
                      <c:set var="billWithFreight" value="0" />
                      <c:set var="totalExpInvoiceAmt" value="0" />
                     
                        <c:set var="totalFreightInvoiceAmt" value="0" />
                        <c:set var="totalReimbursInvoiceAmt" value="0" />
                        <c:set var="totalOtherExpesneForFreightAmt" value="0" />
                        <c:if test = "${tripsOtherExpenseDetails != null}" >
                            
                            <c:forEach items="${tripsOtherExpenseDetails}" var="trip">
                                <tr>
                            <td  ><%=cntr%></td>
                            <td  ><c:out value="${trip.tripCode}"/></td>
                            <td  ><c:out value="${trip.expenseName}"/></td>
                            <c:if test = "${trip.expenseType == '1'}" >
                                            <td  >Bill To Customer(charge to customer) </td>
                                        </c:if>
                                        
                                        <c:if test = "${trip.expenseType == '2'}" >
                                            <td  >Do Not Bill To Customer </td>
                                        </c:if>
                                           <c:if test = "${trip.expenseType == '3'}" >
                                            <td  >Bill with Freight </td>
                                        </c:if> 
                                      
                                           <c:if test = "${trip.expenseType == '4'}" >
                                            <td  >Reimbursable charge </td>
                                        </c:if> 
                                       
                            <td  ><c:out value="${trip.expenseRemarks}"/></td>
                            <td  ><c:out value="${trip.passThroughStatus}"/></td>
                            <td   align="right"><c:out value="${trip.marginValue}"/></td>
                            <td   align="right"><c:out value="${trip.taxPercentage}"/></td>
                            <td   align="right"><c:out value="${trip.expenseValue}"/></td>
                            
                     
                      <c:set var="billWithFreight" value="1" />
                           <c:if test="${trip.expenseType == '3'}">
                                      <c:set var="totalFreightInvoiceAmt" value="${totalFreightInvoiceAmt + trip.expenseValue}" />
                                      <c:set var="totalOtherExpesneForFreightAmt" value="${totalOtherExpesneForFreightAmt + trip.expenseValue}" />
                                  </c:if>
                                  <c:if test="${trip.expenseType == '1'}"> 
                                      <c:set var="billToCustomerType" value="1" />
                                      
                                      <c:set var="totalExpInvoiceAmt" value="${totalExpInvoiceAmt + trip.expenseValue}" />
                                  </c:if>
                                  <c:if test="${trip.expenseType == '4'}">
                                       <c:set var="rembType" value="1" />
                                      <c:set var="totalReimbursInvoiceAmt" value="${totalReimbursInvoiceAmt + trip.expenseValue}" />
                                  </c:if>
                            <%cntr++;%>
                            </c:forEach>
                       </c:if>
                            <% if (cntr == 1){%>
                                <tr>
                                    <td colspan="8">no expenses to be billed</td>
                                </tr>
                            <% }%>
                    </table>
        </form>
    </body>
</html>