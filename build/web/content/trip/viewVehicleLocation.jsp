<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCVpQCXyHbal5NLsAdEUP5OY8myKMaXefg&signed_in=true&libraries=places"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh6LiXDw7IFksBn_vKKKaJTSdbcm8Au3g&libraries=places"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<body>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.LHC DETAILS" text="Vehicle Location"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Master" text="Master"/></a></li>
                <li class=""><spring:message code="hrms.label.LHC" text="Vehicle Location"/></li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <form name="veh" >
                    <%@ include file="/content/common/message.jsp" %>                    

                    <table class="table table-info mb30 table-hover" id="table" >                     
                        <thead>
                            <tr height="30">                                                             
                                <th>Vehicle No</th>                     
                                <th>Vehicle Type</th>
                                <th>Location</th>
                                <th>MobileNo</th>                                                                
                                <th>Current Date</th>
                                <th>Latitude</th>
                                <th>Longitude</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% int sno = 0;%>                        
                            <c:if test = "${viewVehicleLocation != null}">
                                <c:forEach items="${viewVehicleLocation}" var="loc">
                                    <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                                    %>
                                    <tr>
                                        <td class="<%=className%>"  align="left"> <c:out value="${loc.vehicleNo}" /></td>
                                        <td class="<%=className%>"  align="left"> <c:out value="${loc.vehicleType}" /></td>
                                        <td class="<%=className%>"  align="left"> <c:out value="${loc.location}" /></td>
                                        <td class="<%=className%>"  align="left"> <c:out value="${loc.mobileNo}" /></td>                                        
                                        <td class="<%=className%>"  align="left"> <c:out value="${loc.point1PlannedDate}" /></td>                                        
                                        <td class="<%=className%>"  align="left"> <c:out value="${loc.latitude}" /></td>                                        
                                        <td class="<%=className%>"  align="left"> <c:out value="${loc.longitude}" /></td>
                                <input type="hidden" name="lat" id="lat" value="<c:out value="${loc.latitude}" />"/>
                                <input type="hidden" name="lon" id="lon" value="<c:out value="${loc.longitude}" />"/>
                                <input type="hidden" name="location" id="location" value="<c:out value="${loc.location}" />"/>
                                <input id="latlng" name="latlng" type="hidden" value="<c:out value="${loc.latitude}" />,<c:out value="${loc.longitude}" />">
                                </tr>
                                <%sno++;%>
                            </c:forEach>
                            </tbody>
                        </c:if>
                    </table>
                    <br/>   
                    
                    <style>
                        /* Always set the map height explicitly to define the size of the div
                         * element that contains the map. */
                        #map {
                            height: 90%;
                        }
                        /* Optional: Makes the sample page fill the window. */
                        html, body {
                            height: 100%;
                            margin: 0;
                            padding: 0;
                        }
                    </style>
                    <div id="map"></div>
                    <script>
                        initMap();
                        var map, infoWindow;
                        function initMap() {
                            var lati = document.getElementById('lat').value;
                            var lang = document.getElementById('lon').value;
                            var location = document.getElementById('location').value;

                            var map = new google.maps.Map(document.getElementById('map'), {
                                zoom: 10,
                                center: {lat: parseFloat(lati), lng: parseFloat(lang)}
                            });
                            infoWindow = new google.maps.InfoWindow;

                            // Try HTML5 geolocation.
                            if (navigator.geolocation) {
                                navigator.geolocation.getCurrentPosition(function(position) {
                                    var pos = {
                                        lat: parseFloat(lati),
                                        lng: parseFloat(lang)
                                    };

                                    infoWindow.setPosition(pos);
                                    infoWindow.setContent(location);
                                    infoWindow.open(map);
                                    map.setCenter(pos);
                                }, function() {
                                    handleLocationError(true, infoWindow, map.getCenter());
                                });
                            } else {
                                // Browser doesn't support Geolocation
                                handleLocationError(false, infoWindow, map.getCenter());
                            }
                        }

                        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                            infoWindow.setPosition(pos);
                            infoWindow.setContent(browserHasGeolocation ?
                                    'Error: The Geolocation service failed.' :
                                    'Error: Your browser doesn\'t support geolocation.');
                            infoWindow.open(map);
                        }
                    </script>



                </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>