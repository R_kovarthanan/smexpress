<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>


<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });



</script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>


<script type="text/javascript" language="javascript">
    function submitPage() {
        var podFiles = document.getElementsByName("podFile");
        var statusCheck = true;
        for (var i = 0; i < podFiles.length; i++) {

            var cityId = document.getElementById("cityId" + (i + 1)).value;
            if (cityId == 0) {
                alert("Please select location");
                statusCheck = false;
                document.getElementById("cityId" + sno).focus();
            }

            if (podFiles[i].value == '' || podFiles[i].value == null) {
                alert('upload the pod attachment..')
                statusCheck = false;
            }
        }

        if (statusCheck) {
            document.tripPod.action = '/throttle/saveTripPodDetails.do';
            document.tripPod.submit();
        }
    }

    function popUp(url) {
        var http = new XMLHttpRequest();
        http.open('HEAD', url, false);
        http.send();
        if (http.status != 404) {
            popupWindow = window.open(
                    url, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
        }
        else {
            var url1 = "/throttle/content/trip/fileNotFound.jsp";
            popupWindow = window.open(
                    url1, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
        }
    }

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.TripPOD" text="TripPOD"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operation" text="Operation"/></a></li>
            <li class=""><spring:message code="hrms.label.TripPOD" text="TripPOD"/></li>

        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">    <body onload="addRow();">

                <form name="tripPod" method="post" enctype="multipart/form-data">
                    <%
                    boolean isSingleConsignmentOrder = false;
                    %>

                    <c:if test="${roleId == 1023 || roleId == 1013 || roleId == 1016}">
                        <% isSingleConsignmentOrder = true;%>
                    </c:if>   
                    <%if (isSingleConsignmentOrder){%>

                    <table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                        <tr id="exp_table">
                            <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                                <div class="tabs" align="left" style="width:300;">

                                    <div id="first">
                                        <c:if test = "${tripDetails != null}" >
                                            <c:forEach items="${tripDetails}" var="trip">
                                                <%--<c:out value="${trip.orderRevenue}" />--%>
                                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                                        <td> <c:out value="${trip.orderRevenue}" /></td>
                                                        <!--<td> <c:out value="${trip.editFreightAmount}" /></td>-->

                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Projected Expense:</b></font></td>
                                                        <td> <c:out value="${trip.orderExpense}" /></td>
                                                    <input type="hidden" name="estimatedAdvancePerDay" value='<c:out value="${trip.estimatedAdvancePerDay}" />'>
                                                    <input type="hidden" name="estimatedTransitDays" value='<c:out value="${trip.estimatedTransitDays}" />'>
                                                    <input type="hidden" name="estimatedTransitHours" value='<c:out value="${trip.estimatedTransitHours}" />'>

                                                    </tr>


                                                    <c:set var="profitMargin" value="" />
                                                    <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                                    <c:set var="orderExpense" value="${trip.orderExpense}" />
                                                    <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                                                    <%
                                                    String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                                    String revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
        //                                            String revenueStr = "" + (String)pageContext.getAttribute("editFreightAmount");
                                                    float profitPercentage = 0.00F;
                                                    if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                                        profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                                    }


                                                    %>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                                        <td>  <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)
                                                            <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>                                                        
                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Credit Limit:</b></font></td>
                                                        <td>  Rs. 1,00,00,000.00
                                                        <td>
                                                    </tr>
                                                </table>
                                            </c:forEach>
                                        </c:if>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <%}else {%>
                    <c:if test = "${tripDetails != null}" >
                        <c:forEach items="${tripDetails}" var="trip">

                            <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">

                                <input type="hidden" name="estimatedAdvancePerDay" value='<c:out value="${trip.estimatedAdvancePerDay}" />'>
                                <input type="hidden" name="estimatedTransitDays" value='<c:out value="${trip.estimatedTransitDays}" />'>
                                <input type="hidden" name="estimatedTransitHours" value='<c:out value="${trip.estimatedTransitHours}" />'>

                                <c:set var="profitMargin" value="" />
                                <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                <c:set var="orderExpense" value="${trip.orderExpense}" />
                                <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                                <%
                                String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                String revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
//                                            String revenueStr = "" + (String)pageContext.getAttribute("editFreightAmount");
                                float profitPercentage = 0.00F;
                                if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                    profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                }

                                %>
                                <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                            </c:forEach>
                        </c:if>

                        <%}%>
                    <br>


                    <br>
                    <br>
                    <br>
                    <br>
                    <table class="table table-info mb30 table-hover" style="width:100%">
                        <% int loopCntr = 0;%>
                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">
                                <% if(loopCntr == 0) {%>
                                <thead> <tr>
                                        <th  >Trip Code: <c:out value="${trip.tripCode}"/></th>
                                        <th  >Customer Name:&nbsp;<c:out value="${trip.customerName}"/></th>
                                        <th  >Route: &nbsp;<c:out value="${trip.routeInfo}"/></th>
                                        <th  >Status: <c:out value="${trip.status}"/>
                                            <input type="hidden" name="tripType" value="<c:out value="${tripType}"/>"/>
                                            <input type="hidden" name="statusId" value="<c:out value="${statusId}"/>"/>
                                        </th>
                                    </tr>
                                </thead>
                                <% }%>
                                <% loopCntr++;%>
                            </c:forEach>
                        </c:if>
                    </table>
                    <div id="tabs" >
                        <ul>
                            <li class="active" data-toggle="tab"><a href="#podDetail"><span>POD Details</span></a></li>
                            <li data-toggle="tab"><a href="#loadDetails"><span>Load Detail</span></a></li>                            
                            <li data-toggle="tab"><a href="#endDetail"><span>Trip End Detail</span></a></li>
                            <li data-toggle="tab"><a href="#touchPoint"><span>Invoice Detail</span></a></li>
                                <c:if test="${cbt == 1}">
                                <li  data-toggle="tab"><a href="#CBTDetail"><span>Load Detail</span></a></li>
                                </c:if>
                            <li data-toggle="tab"><a href="#tripDetail"><span>Trip Detail</span></a></li>
                            <li data-toggle="tab"><a href="#advance"><span>Advance</span></a></li>
                            <li data-toggle="tab"><a href="#startDetail"><span>Trip Start Detail</span></a></li>
                            <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Route Course </span></a></li>
                            <li data-toggle="tab"><a href="#statusDetail"><span>History</span></a></li>
                        </ul>

                        <div id="tripDetail">
                            <table  class="table table-info mb30 table-hover" id="bg" style="width:100%">
                                <thead><tr>
                                        <th  colspan="6" >Trip Details</th>
                                    </tr></thead>

                                <c:if test = "${tripDetails != null}" >
                                    <c:forEach items="${tripDetails}" var="trip">


                                        <tr>
                                            <!--                            <td ><font color="red">*</font>Trip Sheet Date</td>
                                                                        <td ><input type="text" name="tripDate" class="datepicker" value=""></td>-->
                                            <td >Consignment No(s)</td>
                                            <td >
                                                <c:out value="${trip.cNotes}" />
                                            </td>
                                            <td >Billing Type</td>
                                            <td >
                                                <c:out value="${trip.billingType}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <!--                            <td >Customer Code</td>
                                                                        <td >BF00001</td>-->
                                            <td >Customer Name</td>
                                            <td >
                                                <c:out value="${trip.customerName}" />
                                                <input type="hidden" name="customerName" Id="customerName" class="textbox" value='<c:out value="${customerName}" />'>
                                            </td>
                                            <td >Customer Type</td>
                                            <td  colspan="3" >
                                                <c:out value="${trip.customerType}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >Route Name</td>
                                            <td >
                                                <c:out value="${trip.routeInfo}" />
                                            </td>
                                            <td >Trip Schedule</td>
                                            <td ><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" /> </td>

                                        </tr>
                                        <tr>
                                            <td >Vehicle Type</td>
                                            <td >
                                                <c:out value="${trip.vehicleTypeName}" />
                                            </td>
                                            <td >Vehicle No</td>
                                            <td >
                                                <c:out value="${trip.vehicleNo}" />
                                            </td>                                           
                                        </tr>

                                        <tr style="display:none">
                                            <td ><font color="red">*</font>Driver </td>
                                            <td  colspan="5" >
                                                <c:out value="${trip.driverName}" />
                                            </td>

                                        </tr>
                                        <tr style="display:none">
                                            <td >Product Info </td>
                                            <td  colspan="5" >
                                                <c:out value="${trip.productInfo}" />
                                            </td>

                                        </tr>
                                    </c:forEach>
                                </c:if>
                            </table>
                            <br/>
                            <c:if test="${vehicleChangedTripDetails != null}">
                                <table class="table table-info mb30 table-hover" >
                                    <thead><tr>
                                            <th  colspan="4" >Vehicle Change Details</th>
                                        </tr>
                                    </thead>
                                    <% int index2 = 1;%>
                                    <c:forEach items="${vehicleChangedTripDetails}" var="vehicleChange">
                                        <tr>
                                            <td>Changed Vehicle No</td>
                                            <td><c:out value="${vehicleChange.regNo}"/></td>
                                            <td>Vehicle Changed Date</td>
                                            <td><c:out value="${vehicleChange.vehicleChangeDate}"/></td>
                                        </tr>
                                        <tr>
                                            <td>Changed Place</td>
                                            <td><c:out value="${vehicleChange.cityName}"/></td>
                                            <td>Eway Bill Changed Date</td>
                                            <td><c:out value="${vehicleChange.ewayBillDate}"/></td>
                                        </tr>
                                        <tr>
                                            <td>Insurance Survey</td>
                                            <td><c:out value="${vehicleChange.insuranceSurvey}"/></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </c:forEach>

                                </table>
                            </c:if>
                            <br/>

                            <c:if test = "${expiryDateDetails != null}" >
                                <table style="display:none" class="table table-info mb30 table-hover" id="bg" >
                                    <thead><tr>
                                            <th  colspan="4" >Vehicle Compliance Check</th>
                                        </tr></thead>
                                        <c:forEach items="${expiryDateDetails}" var="expiryDate">
                                        <tr>
                                            <td >Vehicle FC Valid UpTo</td>
                                            <td ><label><font color="green"><c:out value="${expiryDate.fcExpiryDate}" /></font></label></td>
                                        </tr>
                                        <tr>
                                            <td >Vehicle Insurance Valid UpTo</td>
                                            <td ><label><font color="green"><c:out value="${expiryDate.insuranceExpiryDate}" /></font></label></td>
                                        </tr>
                                        <tr>
                                            <td >Vehicle Permit Valid UpTo</td>
                                            <td ><label><font color="green"><c:out value="${expiryDate.permitExpiryDate}" /></font></label></td>
                                        </tr>
                                        <tr>
                                            <td >Road Tax Valid UpTo</td>
                                            <td ><label><font color="green"><c:out value="${expiryDate.roadTaxExpiryDate}" /></font></label></td>
                                        </tr>
                                    </c:forEach>
                                </table>
                                <br/>
                                <br/>
                                <center>
                                    <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Next" /></a>
                                </center>
                            </c:if>

                        </div>

                        <div id="touchPoint" >
                            <c:if test = "${invoiceDetails != null}" >
                                <table class="table table-info mb30 table-hover" id="bg" >     
                                    <thead>
                                        <tr>
                                            <th colspan="4">Invoice Details</th>
                                        </tr>
                                    </thead>
                                    <%int x=0;%>
                                    <c:set var="invoiceWeight" value="0" />
                                    <c:forEach items="${invoiceDetails}" var="inv">
                                        <tr>
                                            <td>Docket No</td>
                                            <td><c:out value="${inv.docketNo}"/></td>
                                            <td>Invoice No</td>
                                            <td><c:out value="${inv.invoiceNo}"/></td>
                                        </tr>
                                        <tr>
                                            <td>Invoice Value</td>                                            
                                            <td><c:out value="${inv.invoiceValue}"/></td>
                                            <td>Invoice Date</td>
                                            <td><c:out value="${inv.invoiceDate}"/><input type="hidden" name="invoiceDate" id="invoiceDate" value="<c:out value="${inv.invoiceDate}"/>"/></td>
                                        </tr>
                                        <tr>
                                            <td>Invoice Weight</td>                                            
                                            <td><c:out value="${inv.weight}"/></td>
                                            <td>EwayBillNo Date</td>
                                            <td><c:out value="${inv.eWayBillNo}"/></td>
                                        </tr>
                                        <tr>
                                            <td><c:out value="${inv.sealNoType}"/></td>
                                            <td><c:out value="${inv.sealNo}"/></td>
                                            <td></td>                                            
                                            <td></td>
                                        </tr>
                                        <%if(x==0){%>
                                        <!--                                        <thead><tr>
                                                                                        <th colspan="4" align="center">Touch Point Detail</th>
                                                                                    </tr></thead>-->
                                        <%}
                                         x++;   
                                        %>
                                        <c:set var="invoiceWeight" value="${inv.weight}" />

                                    </c:forEach>
                                </table>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="NEXT" name="Next" style="width:100px;height:40px;"/></a>
                                </center>
                            </c:if>	
                        </div>      

                        <div id="loadDetails">
                            <c:if test = "${loadDetails != null}" >
                                <table class="table table-info mb30 table-hover" id="addTyres1" style="width: 98%">
                                    <thead>
                                        <tr>
                                            <th width="10"  align="center" >Sno</th>
                                            <th>No of Pallets</th>
                                            <th>Uom</th>
                                            <th>Loaded </th>
                                            <th>UnLoaded </th>
                                        </tr></thead>
                                        <%int i = 1;%>
                                        <c:forEach items="${loadDetails}" var="tripunpack">
                                        <tr>
                                            <td><%=i%></td>
                                            <td><input type="hidden" name="productNames" id="productNames" value="0" readonly/>
                                                <input type="hidden" name="productbatch" id="productbatch" value="0" readonly/>
                                                <input type="hidden" name="packagesNosNew" id="packagesNos" value="<c:out value="${tripunpack.totalPackages}"/>" readonly class='form-control' style='width: 200px;height: 40px'/>
                                                <c:out value="${tripunpack.totalPackages}"/></td>
                                            <td><input type="hidden" name="productuom" id="productuom" value="Pallet" readonly class='form-control' style='width: 200px;height: 40px'/>
                                                <input type="hidden" name="weights" id="weights" value="0" readonly/>
                                                Pallet
                                            </td>
                                            <td><input type="hidden" name="loadedpackagesNew" id="loadedpackagesNew<%=i%>" value="<c:out value="${tripunpack.totalPackages}"/>" readonly class='form-control' style='width: 200px;height: 40px'/>
                                                <input type="hidden" name="consignmentId" value="<c:out value="${tripunpack.consignmentOrderId}"/>" class='form-control' style='width: 200px;height: 40px' />
                                                <input type="hidden" name="tripArticleId" value="0" class='form-control' style='width: 200px;height: 40px'/>
                                                <c:out value="${tripunpack.loadedpackagesNew}"/>
                                            </td>
                                        <input type="hidden" name="shortageNew" id="shortageNew<%=i%>"  value="0" onChange="computeShortageNew(<%=i%>);" onKeyPress="return onKeyPressBlockCharacters(event);" class='form-control' style='width: 200px;height: 40px'/>
                                        <td>
                                            <input type="text" readonly name="unloadedpackagesNew" id="unloadedpackagesNew<%=i%>" value="<c:out value="${tripunpack.totalPackages}"/>" class='form-control' style='width: 200px;height: 40px'/>
                                        </td>
                                        </tr>
                                        <%i++;%>
                                    </table>

                                </c:forEach>

                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="NEXT" name="Next" style="width:100px;height:40px;"/></a>                                    
                                </center>

                            </c:if>
                            <script>
                                function setValues(val) {

                                    if (document.getElementById("cofCheck").checked == true) {
                                        $("#cofDetails").show();
                                    } else {
                                        $("#cofDetails").hide();
                                    }
                                }
                            </script>
                        </div>

                        <div id="podDetail">
                            <c:if test="${viewPODDetails != null}">
                                <table  class="table table-info mb30 table-hover" id="bg">
                                    <thead>
                                        <tr><th colspan="5">Uploaded POD</th></tr>
                                        <tr>
                                            <th  >S No</th>
                                            <th >City Name</th>
                                            <th >POD file Name</th>
                                            <th >Ref. Number</th>
                                            <th >POD Remarks</th>
                                        </tr>
                                    </thead>
                                    <% int index2 = 1;%>
                                    <c:forEach items="${viewPODDetails}" var="viewPODDetails">
                                        <%
                                           String classText3 = "";
                                           int oddEven = index2 % 2;
                                           if (oddEven > 0) {
                                               classText3 = "text1";
                                           } else {
                                               classText3 = "text2";
                                           }
                                        %>
                                        <tr>
                                            <td  ><%=index2++%></td>
                                            <td  ><c:out value="${viewPODDetails.cityName}"/></td>
                                            <td  >
                                                <a onclick="viewPODFiles('<c:out value="${viewPODDetails.tripPodId}"/>')" href="#"><c:out value="${viewPODDetails.podFile}"/></a>
                                            </td>
                                            <td  ><c:out value="${viewPODDetails.lrNumber}"/></td>
                                            <td  ><c:out value="${viewPODDetails.podRemarks}"/></td>
                                        </tr>
                                    </c:forEach>

                                </table>
                            </c:if>
                            <br/>
                            <script>
                                function viewPODFiles(tripPodId) {
                                    window.open('/throttle/content/trip/displayBlobData.jsp?tripPodId=' + tripPodId, 'PopupPage', 'height = 500, width = 500, scrollbars = yes, resizable = yes');
                                }
                            </script>
                            <c:if test="${podDetails != null}">
                                <table class="table table-info mb30 table-hover" style="width:100%" >
                                    <thead><tr>
                                            <th  colspan="6" >Consignor and  Consignee Name</th>
                                        </tr></thead>
                                        <c:set var="count" value="1" scope="page" />
                                        <c:forEach items="${podDetails}" var="podDetails">
                                            <c:if test="${count == 1}">
                                            <tr height="30">
                                                <td  >Consignor Name</td>
                                                <td  ><input type='text' name='consignorName' class="form-control" style="width:250px;height:40px" id="consignorName"  value="<c:out value="${podDetails.consignorName}"/>" /></td>
                                                <td  >Consignor Mobile No</td>
                                                <td  ><input type='text' name='consignorMobileNo' class="form-control" style="width:250px;height:40px" id="consignorMobileNo"  maxlength="10" value="<c:out value="${podDetails.consignorMobileNo}"/>" /></td>
                                                <td  >Consignor Address</td>
                                                <td  ><input type='text' name='consignorAddress' class="form-control" style="width:250px;height:40px" id="consignorAddress"  value="<c:out value="${podDetails.consignorAddress}"/>" /></td>
                                            </tr>
                                            <tr height="30">
                                                <td  >Consignee Name</td>
                                                <td  ><input type='text' name='consigneeName' class="form-control" style="width:250px;height:40px"  id="consigneeName"  value="<c:out value="${podDetails.consigneeName}"/>" />
                                                <td  >Consignee Mobile No</td>
                                                <td  ><input type='text' name='consigneeMobileNo' class="form-control" style="width:250px;height:40px"  id="consigneeMobileNo"  maxlength="10" value="<c:out value="${podDetails.consigneeMobileNo}"/>" />
                                                <td  >Consignee Address</td>
                                                <td  ><input type='text' name='consigneeAddress' class="form-control" style="width:250px;height:40px"  id="consigneeAddress"  value="<c:out value="${podDetails.consigneeAddress}"/>" />
                                                    <input type="hidden" name="consignmentNote" id="consignmentNote" value="<c:out value="${podDetails.consignmentNote}"/>"/></td>
                                            </tr>
                                            <c:set var="count" value="${count + 1}" scope="page" />
                                        </c:if>
                                    </c:forEach>
                                </table>
                            </c:if>
                            <table class="table table-info mb30 table-hover" id="POD1" style="width:100%">
                                <thead> <tr id="rowId0">
                                        <th  align="center" height="30" >Sno</th>
                                        <th  height="30" >Location </th>
                                        <th  height="30" >Attachment</th>
                                        <th  height="30" >Ref. Number </th>
                                        <th  height="30" >POD Remarks</th>
                                        <th  height="30" ></th>
                                    </tr></thead>
                                    <% int index = 1;%>
                                <tr>
                                <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="1"/>
                                <td>      <input type="hidden" name="tripSheetId" id="tripSheetId" value="<%=request.getParameter("tripSheetId")%>" />

                                    </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <input type="button" class="btn btn-success" name="Save" value="Save" onclick="submitPage();"/>
                                        <input type="button" class="btn btn-success" name="add" value="add" onclick="addRow();"/>
                                    </td>
                                </tr>
                                <script>
                                    var podRowCount1 = 1;
                                    function addRow() {
                                        var podSno1 = document.getElementById('selectedRowCount').value;
                                        var tab = document.getElementById("POD1");
                                        var newrow = tab.insertRow(podSno1);

                                        var newrow = tab.insertRow(podSno1);
                                        //                alert(newrow.id);
                                        newrow.id = 'rowId' + podSno1;

                                        var cell = newrow.insertCell(0);
                                        var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(1);
                                        cell0 = "<td class='text1' height='25'><select class='form-control' id='cityId" + podSno1 + "' style='width:250px;height:40px'  name='cityId'><option selected value=0>---Select---</option> <c:if test="${podDetails != null}" ><c:forEach items="${podDetails}" var="podDetails1"><option  value='<c:out value="${podDetails1.podPointId}" />'><c:out value="${podDetails1.cityName}" /> </c:forEach > </c:if> </select></td>";
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(2);
                                        var cell0 = "<td class='text1' height='25' ><input type='file'   id='podFile" + podSno1 + "' name='podFile' class='form-control' value='' onchange='validateLocation(" + podSno1 + ");checkName(" + podSno1 + ");' ><br/><font size='2' color='blue'> Allowed file type:pdf & image</td>";
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(3);
                                        var cell0 = "<td class='text1' height='25' ><input type='text' value='' style='width:250px;height:40px'  name='lrNumber'  id='lrNumber" + podSno1 + "'   value='' ></td>";
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(4);
                                        cell0 = "<td class='text1' height='25' ><textarea rows='3' cols='30' class='form-control'  style='width:250px;height:60px' name='podRemarks' id='podRemarks" + podSno1 + "'></textarea></td>";
                                        cell.innerHTML = cell0;

                                        if (podSno1 == 1) {
                                            cell = newrow.insertCell(5);
                                            cell0 = "<td class='text1' height='25' ></td>";
                                            cell.innerHTML = cell0;
                                        } else {
                                            cell = newrow.insertCell(5);
                                            cell0 = "<td class='text1' height='25' align='left'><input type='button' class='button'  style='width:90px;'  name='delete'  id='delete" + podSno1 + "'   value='Delete Row' onclick='deleteRow(" + podSno1 + ");' ></td>";
                                            cell.innerHTML = cell0;
                                        }


                                        podSno1++;
                                        if (podSno1 > 0) {
                                            document.getElementById('selectedRowCount').value = podSno1;
                                        }

                                    }


                                        </script>

                                <%index++;%>
                            </table>
                        </div>


                        <script type="text/javascript">
                            function deleteRow(sno) {
                                var rowId = "rowId" + sno;
                                $("#" + rowId).remove();
                                document.getElementById('selectedRowCount').value--;
                            }
                            //                   function validateLocation(sno){
                            //                       var cityId = document.getElementById("cityId"+sno).value;
                            //                       if(cityId == "" || cityId == 0){
                            //                           alert("Please select location and then upload file");
                            //                           document.getElementById("cityId"+sno).focus();
                            //                       }
                            //                   }

                            var ar_ext = ['pdf', 'gif', 'jpeg', 'jpg', 'png', 'Gif', 'GIF', 'Png', 'PNG', 'JPG', 'Jpg'];        // array with allowed extensions

                            function checkName(sno) {
                                var name = document.getElementById("podFile" + sno).value;
                                var ar_name = name.split('.');

                                var ar_nm = ar_name[0].split('\\');
                                for (var i = 0; i < ar_nm.length; i++)
                                    var nm = ar_nm[i];

                                var re = 0;
                                for (var i = 0; i < ar_ext.length; i++) {
                                    if (ar_ext[i] == ar_name[1]) {
                                        re = 1;
                                        break;
                                    }
                                }

                                if (re == 1) {
                                }
                                else {
                                    alert('".' + ar_name[1] + '" is not an file type allowed for upload');
                                    document.getElementById("podFile" + sno).value = '';
                                }
                            }
                        </script>
                        <div id="routeDetail">

                            <c:if test = "${tripPointDetails != null}" >
                                <table class="table table-info mb30 table-hover" >
                                    <thead><tr >
                                            <th  height="30" >S. No</th>
                                            <th  height="30" >Point Name</th>
                                            <th  height="30" >Type</th>
                                            <th  height="30" >Route Order</th>
                                            <th  height="30" >Address</th>
                                            <th  height="30" >Planned Date</th>
                                            <th  height="30" >Planned Time</th>
                                        </tr></thead>
                                        <% int index3 = 1; %>
                                        <c:forEach items="${tripPointDetails}" var="tripPoint">
                                            <%
                                                           String classText1 = "";
                                                           int oddEven2 = index3 % 2;
                                                           if (oddEven2 > 0) {
                                                               classText1 = "text1";
                                                           } else {
                                                               classText1 = "text2";
                                                           }
                                            %>
                                        <tr >
                                            <td  height="30" ><%=index3++%></td>
                                            <td  height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                            <td  height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                            <td  height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                            <td  height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                            <td  height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                            <td  height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                                        </tr>
                                    </c:forEach >
                                </table>
                                <br/>

                                <table style="display:none" border="0" class="border" align="centver" width="100%" cellpadding="0" cellspacing="0" >
                                    <c:if test = "${tripDetails != null}" >
                                        <c:forEach items="${tripDetails}" var="trip">
                                            <tr>
                                                <td  width="150"> Estimated KM</td>
                                                <td  width="120" > <c:out value="${trip.estimatedKM}"  />&nbsp;</td>
                                                <td  colspan="4">&nbsp;</td>
                                            </tr>
                                            <!--                                    <tr>
                                                                                    <td  width="150"> Estimated Reefer Hour</td>
                                                                                    <td  width="120"> <c:out  value="${trip.estimatedTransitHours * 60 / 100}" />&nbsp;</td>
                                                                                    <td  colspan="4">&nbsp;</td>
                                                                                </tr>-->

                                        </c:forEach>
                                    </c:if>
                                </table>
                                <br/>
                                <center>
                                    <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                                </center>
                            </c:if>
                            <br>
                            <br>
                        </div>

                        <div id="endDetail">                            
                            <c:if test = "${tripEndDetails != null}" >
                                <table  style="width:100%"  class="table table-info mb30 table-hover" >
                                    <c:forEach items="${tripEndDetails}" var="endDetails">
                                        <thead><tr>
                                                <th  colspan="6" > Trip End Details</th>
                                            </tr></thead>
                                        <tr>
                                            <td  height="30" >Trip Planned End Date</td>
                                            <td  height="30" ><c:out value="${endDetails.planEndDate}" /></td>
                                            <td  height="30" >Trip Planned End Time</td>
                                            <td  height="30" ><c:out value="${endDetails.planEndTime}" /></td>
                                            <td  height="30" >Trip Actual Reporting Date</td>
                                            <td  height="30" ><c:out value="${endDetails.endReportingDate}" /></td>
                                        </tr>
                                        <tr>
                                            <td  height="30" >Trip Actual Reporting Time</td>
                                            <td  height="30" ><c:out value="${endDetails.endReportingTime}" /></td>
                                            <td  height="30" >Trip Unloading Date</td>
                                            <td  height="30" ><c:out value="${endDetails.unLoadingDate}" /></td>
                                            <td  height="30" >Trip Unloading Time</td>
                                            <td  height="30" ><c:out value="${endDetails.unLoadingTime}" /></td>
                                        </tr>
                                        <tr>
                                            <td  height="30" >Trip Unloading Temperature</td>
                                            <td  height="30" ><c:out value="${endDetails.unLoadingTemperature}" /></td>
                                            <td  height="30" >Trip Actual End Date</td>
                                            <td  height="30" ><c:out value="${endDetails.endDate}" /></td>
                                            <td  height="30" >Trip Actual End Time</td>
                                            <td  height="30" ><c:out value="${endDetails.endTime}" /></td>
                                        </tr>
                                        <tr style="display:none">
                                            <td  height="30" >Trip End Odometer Reading(KM)</td>
                                            <td  height="30" ><c:out value="${endDetails.endOdometerReading}" /></td>
                                            <!--                                <td  height="30" >Trip End Reefer Reading(HM)</td>
                                                                                <td  height="30" ><c:out value="${endDetails.endHM}" /></td>-->
                                            <td  height="30" >Total Odometer Reading(KM)</td>
                                            <td  height="30" ><c:out value="${endDetails.totalKM}" /></td>
                                        </tr>
                                        

                                    </c:forEach >
                                </table>                                
                            </c:if>
                            <br>
                            <br>
                        </div>
                        <div id="startDetail">
                            <c:if test = "${tripStartDetails != null}" >
                                <table  style="width:100%" class="table table-info mb30 table-hover" >
                                    <c:forEach items="${tripStartDetails}" var="startDetails">
                                        <thead> <tr>
                                                <th  colspan="6" > Trip Start Details</th>
                                            </tr></thead>
                                        <tr>
                                            <td  height="30" >Trip Planned Start Date</td>
                                            <td  height="30" ><c:out value="${startDetails.planStartDate}" />&nbsp;</td>
                                            <td  height="30" >Trip Planned Start Time</td>
                                            <td  height="30" ><c:out value="${startDetails.planStartTime}" />&nbsp;</td>
                                            <td  height="30" >Trip Start Reporting Date</td>
                                            <td  height="30" ><c:out value="${startDetails.startReportingDate}" />&nbsp;</td>

                                        </tr>
                                        <tr>
                                            <td  height="30" >Trip Start Reporting Time</td>
                                            <td  height="30" ><c:out value="${startDetails.startReportingTime}" />&nbsp;</td>
                                            <td  height="30" >Trip Loading date</td>
                                            <td  height="30" ><c:out value="${startDetails.loadingDate}" />&nbsp;</td>
                                            <td  height="30" >Trip Loading Time</td>
                                            <td  height="30" ><c:out value="${startDetails.loadingTime}" />&nbsp;</td>
                                        </tr>
                                        <tr >
                                            <td  height="30" >Trip Loading Temperature</td>
                                            <td  height="30" ><c:out value="${startDetails.loadingTemperature}" />&nbsp;</td>
                                            <td  height="30" >Trip Actual Start Date</td>
                                            <td  height="30" ><c:out value="${startDetails.startDate}" />&nbsp;</td>
                                            <td  height="30" >Trip Actual Start Time</td>
                                            <td  height="30" ><c:out value="${startDetails.startTime}" />&nbsp;</td>
                                        </tr>
                                        <tr style="display:none">
                                            <td  height="30" >Trip Start Odometer Reading(KM)</td>
                                            <td  height="30" ><c:out value="${startDetails.startOdometerReading}" />&nbsp;</td>
                                            <!--                                <td  height="30" >Trip Start Reefer Reading(HM)</td>
                                                                                <td  height="30" ><c:out value="${startDetails.startHM}" />&nbsp;</td>-->
                                            <td  height="30" colspan="2" ></td>
                                        </tr>
                                    </c:forEach >
                                </table>

                                <c:if test = "${tripUnPackDetails != null}" >
                                    <table style="display: none">
                                        <thead><tr>
                                                <th width="20"  align="center" height="30" >Sno</th>
                                                <th  height="30" >Product/Article Code</th>
                                                <th  height="30" >Product/Article Name </th>
                                                <th  height="30" >Batch </th>
                                                <th  height="30" ><font color='red'>*</font>No of Packages</th>
                                                <th  height="30" ><font color='red'>*</font>Uom</th>
                                                <th  height="30" ><font color='red'>*</font>Total Weight (in Kg)</th>
                                                <th  height="30" ><font color='red'>*</font>Loaded Package Nos</th>
                                                <th  height="30" ><font color='red'>*</font>UnLoaded Package Nos</th>
                                                <th  height="30" ><font color='red'>*</font>Shortage</th>
                                            </tr></thead>


                                        <%int i1=1;%>
                                        <c:forEach items="${tripUnPackDetails}" var="tripunpack">
                                            <tr>
                                                <td><%=i1%></td>
                                                <td><input type="text"  name="productCodes" id="productCodes" value="<c:out value="${tripunpack.articleCode}"/>" readonly/></td>
                                                <td><input type="text" name="productNames" id="productNames" value="<c:out value="${tripunpack.articleName}"/>" readonly/></td>
                                                <td><input type="text" name="productbatch" id="productbatch" value="<c:out value="${tripunpack.batch}"/>" readonly/></td>
                                                <td><input type="text" name="packagesNos" id="packagesNos" value="<c:out value="${tripunpack.packageNos}"/>" readonly/></td>
                                                <td><input type="text" name="productuom" id="productuom" value="<c:out value="${tripunpack.uom}"/>" readonly/></td>
                                                <td><input type="text" name="weights" id="weights" value="<c:out value="${tripunpack.packageWeight}"/> " readonly/>
                                                <td><input type="text" name="loadedpackages" id="loadedpackages<%=i1%>" value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly/>
                                                    <input type="hidden" name="consignmentId" value="<c:out value="${tripunpack.consignmentId}"/>"/>
                                                    <input type="hidden" name="tripArticleId" value="<c:out value="${tripunpack.tripArticleid}"/>"/>
                                                </td>
                                                <td><input type="text" name="unloadedpackages" id="unloadedpackages<%=i1%>" onblur="computeShortage(<%=i1%>);"  value="0"  onKeyPress="return onKeyPressBlockCharacters(event);"    /></td>
                                                <td><input type="text" name="shortage" id="shortage<%=i1%>"  value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly /></td>
                                            </tr>
                                            <%i1++;%>
                                        </c:forEach>

                                        <br/>
                                        <br/>
                                        <br/>
                                    </table>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <center>
                                        <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                                    </center>
                                </c:if>
                                <br>
                                <br>
                            </c:if>
                        </div>



                        <div id="statusDetail">
                            <% int index1 = 1; %>

                            <c:if test = "${statusDetails != null}" >
                                <table class="table table-info mb30 table-hover" style="width:100%">
                                    <thead>  <tr >
                                            <th  height="30" >S No</th>
                                            <th  height="30" >Status Name</th>
                                            <th  height="30" >Remarks</th>
                                            <th  height="30" >Created User Name</th>
                                            <th  height="30" >Created Date</th>
                                        </tr></thead>
                                        <c:forEach items="${statusDetails}" var="statusDetails">
                                            <%
                                                   String classText = "";
                                                   int oddEven1 = index1 % 2;
                                                   if (oddEven1 > 0) {
                                                       classText = "text1";
                                                   } else {
                                                       classText = "text2";
                                                   }
                                            %>
                                        <tr >
                                            <td  height="30" ><%=index1++%></td>
                                            <td  height="30" ><c:out value="${statusDetails.statusName}" /></td>
                                            <td  height="30" ><c:out value="${statusDetails.tripRemarks}" /></td>
                                            <td  height="30" ><c:out value="${statusDetails.userName}" /></td>
                                            <td  height="30" ><c:out value="${statusDetails.tripDate}" /></td>
                                        </tr>
                                    </c:forEach >
                                </table>
                                <br/>
                                <br/>
                            </c:if>
                            <br>
                            <br>
                        </div>
                        <c:if test="${tripAdvanceDetails != null}">
                            <div id="advance">
                                <c:if test="${tripAdvanceDetails != null}">
                                    <table  class="table table-info mb30 table-hover" style="width:100%" id="bg">
                                        <thead><tr>
                                                <th  width="30">Sno</th>
                                                <th  width="90">Advance Date</th>
                                                <th  width="90">Trip Day</th>
                                                <th  width="120">Estimated Advance</th>
                                                <th  width="120">Requested Advance</th>
                                                <th  width="90"> Type</th>
                                                <th  width="120">Requested By</th>
                                                <th  width="120">Requested Remarks</th>
                                                <th  width="120">Approved By</th>
                                                <th  width="120">Approved Remarks</th>
                                                <th  width="120">Paid Advance</th>
                                            </tr></thead>
                                            <%int index7=1;%>
                                            <c:forEach items="${tripAdvanceDetails}" var="tripAdvance">
                                                <c:set var="totalAdvancePaid" value="${ totalAdvancePaid + tripAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                                <%
                                                       String classText7 = "";
                                                       int oddEven7 = index7 % 2;
                                                       if (oddEven7 > 0) {
                                                           classText7 = "text1";
                                                       } else {
                                                           classText7 = "text2";
                                                       }
                                                %>
                                            <tr>
                                                <td ><%=index7++%></td>
                                                <td ><c:out value="${tripAdvance.advanceDate}"/></td>
                                                <td >DAY&nbsp;<c:out value="${tripAdvance.tripDay}"/></td>
                                                <td ><c:out value="${tripAdvance.estimatedAdance}"/></td>
                                                <td ><c:out value="${tripAdvance.requestedAdvance}"/></td>
                                                <c:if test = "${tripAdvance.requestType == 'A'}" >
                                                    <td >Adhoc</td>
                                                </c:if>
                                                <c:if test = "${tripAdvance.requestType == 'B'}" >
                                                    <td >Batch</td>
                                                </c:if>
                                                <c:if test = "${tripAdvance.requestType == 'M'}" >
                                                    <td >Manual</td>
                                                </c:if>
                                                <td ><c:out value="${tripAdvance.approvalRequestBy}"/></td>
                                                <td ><c:out value="${tripAdvance.approvalRequestRemarks}"/></td>
                                                <td ><c:out value="${tripAdvance.approvedBy}"/></td>
                                                <td ><c:out value="${tripAdvance.approvalRemarks}"/></td>
                                                <td ><c:out value="${tripAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                            </tr>
                                        </c:forEach>

                                        <tr></tr>
                                        <tr>

                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>
                                            <td ></td>
                                            <td ></td>
                                            <td  colspan="2">Total Advance Paid</td>
                                            <td ><c:out value="${totalAdvancePaid}"/></td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <c:if test="${tripAdvanceDetailsStatus != null}">
                                        <table style="display:none" class="table table-info mb30 table-hover" style="width:100%"  id="bg">
                                            <thead><tr>
                                                    <th  colspan="13" > Advance Approval Status Details</th>
                                                </tr>
                                                <tr>
                                                    <th  width="30">Sno</th>
                                                    <th  width="90">Request Date</th>
                                                    <th  width="90">Trip Day</th>
                                                    <th  width="120">Estimated Advance</th>
                                                    <th  width="120">Requested Advance</th>
                                                    <th  width="90"> Type</th>
                                                    <th  width="120">Requested By</th>
                                                    <th  width="120">Requested Remarks</th>
                                                    <th  width="120">Approval Status</th>
                                                    <th  width="120">Paid Status</th>
                                                </tr></thead>
                                                <%int index13=1;%>
                                                <c:forEach items="${tripAdvanceDetailsStatus}" var="tripAdvanceStatus">
                                                    <%
                                                           String classText13 = "";
                                                           int oddEven11 = index13 % 2;
                                                           if (oddEven11 > 0) {
                                                               classText13 = "text1";
                                                           } else {
                                                               classText13 = "text2";
                                                           }
                                                    %>
                                                <tr>

                                                    <td ><%=index13++%></td>
                                                    <td ><c:out value="${tripAdvanceStatus.advanceDate}"/></td>
                                                    <td >DAY&nbsp;<c:out value="${tripAdvanceStatus.tripDay}"/></td>
                                                    <td ><c:out value="${tripAdvanceStatus.estimatedAdance}"/></td>
                                                    <td ><c:out value="${tripAdvanceStatus.requestedAdvance}"/></td>
                                                    <c:if test = "${tripAdvanceStatus.requestType == 'A'}" >
                                                        <td >Adhoc</td>
                                                    </c:if>
                                                    <c:if test = "${tripAdvanceStatus.requestType == 'B'}" >
                                                        <td >Batch</td>
                                                    </c:if>
                                                    <c:if test = "${tripAdvanceStatus.requestType == 'M'}" >
                                                        <td >Manual</td>
                                                    </c:if>

                                                    <td ><c:out value="${tripAdvanceStatus.approvalRequestBy}"/></td>
                                                    <td ><c:out value="${tripAdvanceStatus.approvalRequestRemarks}"/></td>
                                                    <td >
                                                        <c:if test = "${tripAdvanceStatus.approvalStatus== ''}" >
                                                            &nbsp
                                                        </c:if>
                                                        <c:if test = "${tripAdvanceStatus.approvalStatus== '1' }" >
                                                            Request Approved
                                                        </c:if>
                                                        <c:if test = "${tripAdvanceStatus.approvalStatus== '2' }" >
                                                            Request Rejected
                                                        </c:if>
                                                        <c:if test = "${tripAdvanceStatus.approvalStatus== '0'}" >
                                                            Approval in  Pending
                                                        </c:if>
                                                        &nbsp;</td>
                                                    <td >
                                                        <c:if test = "${ tripAdvanceStatus.approvalStatus== '1' || tripAdvanceStatus.approvalStatus== 'N'}" >
                                                            Yet To Pay
                                                        </c:if>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </table>
                                        <br/>
                                        <br/>

                                        <c:if test="${vehicleChangeAdvanceDetailsSize != '0'}">
                                            <table style="display:none" border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                                <tr>
                                                    <td  colspan="13" > Old Vehicle Advance Details</td>
                                                </tr>
                                                <tr>
                                                    <td  width="30">Sno</td>
                                                    <td  width="90">Advance Date</td>
                                                    <td  width="90">Trip Day</td>
                                                    <td  width="120">Estimated Advance</td>
                                                    <td  width="120">Requested Advance</td>
                                                    <td  width="90"> Type</td>
                                                    <td  width="120">Requested By</td>
                                                    <td  width="120">Requested Remarks</td>
                                                    <td  width="120">Approved By</td>
                                                    <td  width="120">Approved Remarks</td>
                                                    <td  width="120">Paid Advance</td>
                                                </tr>
                                                <%int index17=1;%>
                                                <c:forEach items="${vehicleChangeAdvanceDetails}" var="vehicleChangeAdvance">
                                                    <c:set var="totalVehicleChangeAdvancePaid" value="${ totalVehicleChangeAdvancePaid + vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                                    <%
                                                           String classText17 = "";
                                                           int oddEven17 = index17 % 2;
                                                           if (oddEven17 > 0) {
                                                               classText17 = "text1";
                                                           } else {
                                                               classText17 = "text2";
                                                           }
                                                    %>
                                                    <tr>
                                                        <td ><%=index7++%></td>
                                                        <td ><c:out value="${vehicleChangeAdvance.advanceDate}"/></td>
                                                        <td >DAY&nbsp;<c:out value="${vehicleChangeAdvance.tripDay}"/></td>
                                                        <td ><c:out value="${vehicleChangeAdvance.estimatedAdance}"/></td>
                                                        <td ><c:out value="${vehicleChangeAdvance.requestedAdvance}"/></td>
                                                        <c:if test = "${vehicleChangeAdvance.requestType == 'A'}" >
                                                            <td >Adhoc</td>
                                                        </c:if>
                                                        <c:if test = "${vehicleChangeAdvance.requestType == 'B'}" >
                                                            <td >Batch</td>
                                                        </c:if>
                                                        <c:if test = "${vehicleChangeAdvance.requestType == 'M'}" >
                                                            <td >Manual</td>
                                                        </c:if>
                                                        <td ><c:out value="${vehicleChangeAdvance.approvalRequestBy}"/></td>
                                                        <td ><c:out value="${vehicleChangeAdvance.approvalRequestRemarks}"/></td>
                                                        <td ><c:out value="${vehicleChangeAdvance.approvedBy}"/></td>
                                                        <td ><c:out value="${vehicleChangeAdvance.approvalRemarks}"/></td>
                                                        <td ><c:out value="${vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                                    </tr>
                                                </c:forEach>

                                                <tr></tr>
                                                <tr>

                                                    <td >&nbsp;</td>
                                                    <td >&nbsp;</td>
                                                    <td >&nbsp;</td>
                                                    <td >&nbsp;</td>
                                                    <td >&nbsp;</td>
                                                    <td >&nbsp;</td>
                                                    <td ></td>
                                                    <td ></td>
                                                    <td  colspan="2">Total Advance Paid</td>
                                                    <td ><c:out value="${totalVehicleChangeAdvancePaid}"/></td>
                                                </tr>
                                            </table>
                                        </c:if>
                                    </c:if>
                                    <br/>
                                    <center>
                                        <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Save" /></a>
                                    </center>
                                </c:if>
                            </div>
                        </c:if>


                        <script>
                            $('.btnNext').click(function() {
                                $('.nav-tabs > .active').next('li').find('a').trigger('click');
                            });
                            //                            $('.btnPrevious').click(function() {
                            //                                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                            //                            });
                        </script> 

                    </div>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>