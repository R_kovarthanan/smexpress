<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%@page import="java.text.SimpleDateFormat"%>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ page import="java.util.* "%>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>
    <%@ page import="java.text.DecimalFormat" %>
    <%@ page import="java.text.NumberFormat" %>

    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

    <style type="text/css" title="currentStyle">
        @import "/throttle/css/layout-styles.css";
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->
    <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>

    <link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

    <link rel="stylesheet" type="text/css" href="/throttle/css/select2-bootstrap.css"/>

    <script type="text/javascript">

        function computeExpense() {

            var distance = document.trip.preStartLocationDistance.value;
            var vehicleMileage = document.trip.vehicleMileage.value;
            var tollRate = document.trip.tollRate.value;
            var fuelPrice = document.trip.fuelPrice.value;

            if (distance.trim() != '' && distance.trim() != '0') {
                var fuelLtrs = parseFloat(distance) / parseFloat(vehicleMileage);
                var fuelCost = (parseFloat(fuelPrice) * fuelLtrs);
                var tollCost = parseFloat(distance) * parseFloat(tollRate);
                var totalCost = (fuelCost + tollCost).toFixed(2);
                document.trip.preStartRouteExpense.value = totalCost;
                document.trip.preStartLocationDurationHrs.value = 0;
                document.trip.preStartLocationDurationMins.value = 0;
            }

        }

        var httpReq;
        var temp = "";

        function fetchRouteInfo() {
            var preStartLocationId = document.trip.preStartLocationId.value;
            var originId = document.trip.originId.value;
            var vehicleTypeId = document.trip.vehicleTypeId.value;
            if (preStartLocationId != '') {
                var url = "/throttle/checkPreStartRoute.do?preStartLocationId=" + preStartLocationId + "&originId=" + originId + "&vehicleTypeId=" + vehicleTypeId;
                //alert(url);
                if (window.ActiveXObject)
                {
                    httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest)
                {
                    httpReq = new XMLHttpRequest();
                }
                httpReq.open("GET", url, true);
                httpReq.onreadystatechange = function() {
                    processFetchRouteCheck();
                };
                httpReq.send(null);
            }

        }

        function processFetchRouteCheck()
        {
            if (httpReq.readyState == 4)
            {
                if (httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    //alert(temp);
                    if (temp != '' && temp != null && temp != 'null') {
                        var tempVal = temp.split('-');
                        document.trip.preStartLocationDistance.value = tempVal[0];
                        document.trip.preStartLocationDurationHrs.value = tempVal[1];
                        document.trip.preStartLocationDurationMins.value = tempVal[2];
                        document.trip.preStartRouteExpense.value = tempVal[3];
                        document.getElementById("preStartLocationDistance").readOnly = true;
                        document.getElementById("preStartLocationDurationHrs").readOnly = true;
                        document.getElementById("preStartLocationDurationMins").readOnly = true;
                        document.getElementById("preStartRouteExpense").readOnly = true;
                    } else {
                        alert('valid route does not exists between pre start location and trip start location');
                        document.getElementById("preStartLocationDistance").readOnly = false;
                        document.getElementById("preStartLocationDurationHrs").readOnly = false;
                        document.getElementById("preStartLocationDurationMins").readOnly = false;
                        document.getElementById("preStartRouteExpense").readOnly = false;
                    }
//                        if(tempVal[0] == 0){
//                            alert("no match found");
//                        }
                }
                else
                {
                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                }
            }
        }




        //end ajax for pre location
        function saveAction() {
            var statusCheck = false;
            var temp = document.trip.vehicleNo.value;
            
            if (temp != '') {
                statusCheck = true;
            } else {
                alert('please enter vehicle');
                $("#vehicleNo").focus();
            }



            if (statusCheck) {
                if ($("#vehicleChangeDate").val() == '') {
                    alert("please select vehicle change date");
                    $("#vehicleChangeDate").focus();
                } else if ($("#ewayBillDate").val() == '') {
                    alert("please enter eway Bill Date");
                    $("#ewayBillDate").focus();
                } else if ($("#sealNo").val() == '') {
                    alert("please enter seal No");
                    $("#sealNo").focus();
                } else if ($("#cityId").val() == '') {
                    alert("please enter Change City");
                    $("#cityId").focus();                 
                } else if ($("#driverMobile").val() == '') {
                    alert("please enter enter the driver mobile no");
                    $("#driverMobile").focus();                 
                } else {
                    $("#save").hide();
                    document.trip.action = "/throttle/saveTripSheetForVehicleChange.do";
                    document.trip.submit();
                }
            }
        }
        
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });



    </script>
    <script type="text/javascript">
        function calculateDate() {
            var endDates = document.getElementById("tripScheduleDate").value;
            var tempDate1 = endDates.split("-");
            var stDates = document.getElementById("preStartLocationPlanDate").value;
            var tempDate2 = stDates.split("-");
            var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0]);  // Feb 1, 2011
            var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0]);              // now
            var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
            if (prevTime.getTime() < thisTime.getTime()) {
                if (difference > 0) {
                    alert(" Selected Date is greater than scheduled date ");
                    document.getElementById("preStartLocationPlanDate").value = document.getElementById("todayDate").value;
                }
            }
        }
        function calculateTime() {
            var endDates = document.getElementById("tripScheduleDate").value;
            var endTimeIds = document.getElementById("tripScheduleTime").value;
            var tempDate1 = endDates.split("-");
            var tempTime3 = endTimeIds.split(" ");
            var tempTime1 = tempTime3[0].split(":");
            if (tempTime3[1] == "PM") {
                tempTime1[0] = 12 + parseInt(tempTime1[0]);
            }
            var stDates = document.getElementById("preStartLocationPlanDate").value;
            var hour = document.getElementById("preStartLocationPlanTimeHrs").value;
            var minute = document.getElementById("preStartLocationPlanTimeMins").value;
            var stTimeIds = hour + ":" + minute + ":" + "00";
            var tempDate2 = stDates.split("-");
            var tempTime2 = stTimeIds.split(":");
            var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0], tempTime2[0], tempTime2[1]);  // Feb 1, 2011
            var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0], parseInt(tempTime1[0]), tempTime1[1]);              // now
            var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
            var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
            if (prevTime.getTime() < thisTime.getTime()) {
                if (hoursDifference > 0) {
                    alert("Selected Time is greater than scheduled Time ");
                    document.getElementById("preStartLocationPlanTimeHrs").focus();
                }
            }
        }
    </script>
    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("#tabs").tabs();
        });
    </script>


    <script type="text/javascript" language="javascript">

        function getDriverName() {
            var oTextbox = new AutoSuggestControl(document.getElementById("driName"), new ListSuggestions("driName", "/throttle/handleDriverSettlement.do?"));

        }
    </script>
    <script language="">
        function print(val)
        {
            var DocumentContainer = document.getElementById(val);
            var WindowObject = window.open('', "TrackHistoryData",
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }



    </script>
    <script>
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#driver1Name').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getDriverNames.do",
                        dataType: "json",
                        data: {
                            driverName: (request.term).trim(),
                            textBox: 1
                        },
                        success: function(data, textStatus, jqXHR) {
//                                alert("data"+data); Muthu
//                        var items = data;
//                        response(items);
                            var items = data;
                            var primaryDriver = $('#primaryDriver').val();
//                        if (items == '' && primaryDriver != '') {
//                            alert("Invalid Primary Driver Name");
//                            $('#driver1Name').val('');
//                            $('#driver1Name').val('');
//                            $('#driver1Name').focus();
//                        } else {
//                        }
                            response(items);
                        },
                        error: function(data, type) {
                            //console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var id = ui.item.Id;
                    var name = ui.item.Name;
                    var mobile = ui.item.Mobile;
                    var licenseNo = ui.item.License;
//                alert("value" + id);

                    $('#driver1Id').val(id);
                    $('#driver1Name').val(name);
                    $('#mobileNo').val(mobile);
                    $('#licenseNo').val(licenseNo);
                    return false;
                }
                // Format the list menu output of the autocomplete
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
//            alert("item" + item);
                var dlNo = item.License;
                if (dlNo == '') {
                    dlNo = '-';
                }
                var itemVal = item.Name + '(DL:' + dlNo + ')';
//            var temp = itemVal.split('-');
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });
        //end ajax for vehicle Nos
    </script>



    <script type="text/javascript">

//        function computeVehicleCapUtil() {
//            setVehicleValues();
//            var orderWeight = document.trip.totalWeight.value;
//            var vehicleCapacity = document.trip.vehicleTonnage.value;
//            if (vehicleCapacity > 0) {
//                var utilPercent = (orderWeight / vehicleCapacity) * 100;
//                //document.trip.vehicleCapUtil.value = utilPercent.toFixed(2);
//
//
//            } else {
//                //document.trip.vehicleCapUtil.value = 0;
//            }
//        }
//
//        function setVehicleValues() {
//            var value = document.trip.vehicleNo.value;
//            //alert(value);
//            if (value != 0) {
//                var tmp = value.split('-');
//                $('#vehicleId').val(tmp[0]);
//                //alert("1");
//                $('#vehicleNoEmail').val(tmp[1]);
//                //alert("2");
//                $('#vehicleTonnage').val(tmp[2]);
//                //alert("3");
//                //                    $('#driver1Id').val(tmp[3]);
//                //alert("4");
//                $('#driver1Name').val(tmp[4]);
//                //alert("5");
//                $('#driver2Id').val(tmp[5]);
//                //alert("6");
//                $('#driver2Name').val(tmp[6]);
//                //alert("7");
//                $('#driver3Id').val(tmp[7]);
//                //alert("8");
//                $('#driver3Name').val(tmp[8]);
//                $('#lhcId').val(tmp[9]);
//                $('#actionName').empty();
//                var actionOpt = document.trip.actionName;
//                var optionVar = new Option("-select-", '0');
//                actionOpt.options[0] = optionVar;
//                optionVar = new Option("Freeze", '1');
//                actionOpt.options[1] = optionVar;
//                $("#actionDiv").hide();
//            } else {
//                $('#vehicleId').val('');
//                $('#vehicleNoEmail').val('');
//                $('#vehicleTonnage').val('');
//                $('#driver1Id').val('');
//                $('#driver1Name').val('');
//                $('#driver2Id').val('');
//                $('#driver2Name').val('');
//                $('#driver3Id').val('');
//                $('#driver3Name').val('');
//                $('#lhcId').val('');
//                $('#actionName').empty();
//                var actionOpt = document.trip.actionName;
//                var optionVar = new Option("-select-", '0');
//                actionOpt.options[0] = optionVar;
//                optionVar = new Option("Cancel Order", 'Cancel');
//                actionOpt.options[1] = optionVar;
//                optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
//                actionOpt.options[2] = optionVar;
//                optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
//                actionOpt.options[3] = optionVar;
//                $("#actionDiv").show();
//            }
//        }
        //start ajax for vehicle Nos
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#vehicleNo').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getLhcVehicleRegNo.do",
                        dataType: "json",
                        data: {
                            vehicleNo: request.term,
                            vehicleTypeId: $("#vehicleTypeId").val()
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                            $('#vehicleId').val(0);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var id = ui.item.Id;
                    //alert(value);
                    $('#vehicleNo').val(value);
                    $('#vehicleId').val(id);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });

    </script>



<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.TripSheet" text="Trip Sheet Details"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.SecondaryOperations" text="SecondaryOperations"/></a></li>
            <li class=""><spring:message code="hrms.label.TripSheet" text="Trip Sheet Details"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="trip" method="post" enctype="multipart/form-data" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <br/>
                    <%
                    Date today = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    String startDate = sdf.format(today);
                    %>
                    <%
                    String vehicleMileageAndTollRate = "";
                    String vehicleMileage = "0";
                    String tollRate = "0";
                    String fuelPrice = "0";
                    String[] temp = null;
                    if(request.getAttribute("vehicleMileageAndTollRate") != null){
                        vehicleMileageAndTollRate = (String)request.getAttribute("vehicleMileageAndTollRate");
                        temp = vehicleMileageAndTollRate.split("-");
                        vehicleMileage = temp[0];
                        tollRate = temp[1];
                        fuelPrice = temp[2];
                    }
                    %>
                    <input type="hidden" name="vehicleMileage" value="<%=vehicleMileage%>" />
                    <input type="hidden" name="tollRate" value="<%=tollRate%>" />
                    <input type="hidden" name="fuelPrice" value="<%=fuelPrice%>" />
                    <input type="hidden" name="tripType" value="<c:out value="${tripType}"/>" />
                    <input type="hidden" id="lhcId" name="lhcId" value="" />
                    <input type="hidden" name="statusId" value="<c:out value="${statusId}"/>" />
                    <table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">
                        <tr id="exp_table" style="display:none;">
                            <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                                <div class="tabs" align="left" style="width:300;">
                                    <div id="first">
                                        <c:if test = "${tripDetails != null}" >
                                            <c:forEach items="${tripDetails}" var="trip">
                                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                                        <td> <c:out value="${trip.orderRevenue}" /></td>

                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Projected Expense:</b></font></td>
                                                        <td> <c:out value="${trip.orderExpense}" /></td>

                                                    </tr>
                                                    <c:set var="profitMargin" value="" />
                                                    <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                                    <c:set var="orderExpense" value="${trip.orderExpense}" />

                                                    <input type="hidden" name="orderExpense" Id="orderExpense" class="form-control" style="width:150px;height:40px" value='<c:out value="${trip.orderExpense}" />' >
                                                    <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                                                    <%
                                                    String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                                    String revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
                                                    float profitPercentage = 0.00F;
                                                    if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                                        profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                                    }
                                                    %>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                                        <td>  <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)
                                                            <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                                                        </td>
                                                    </tr>

                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Credit Limit:</b></font></td>
                                                        <td>  Rs. 1,00,00,000.00
                                                        <td>
                                                    </tr>

                                                </table>
                                            </c:forEach>
                                        </c:if>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br> <br>
                    <br> <br>
                    <br>
                    <br> <br>
                    <br> <br>
                    <br>
                    <div id="tabs" >
                        <ul>
                            <li><a href="#tripDetail"><span>Trip Details</span></a></li>
                            <li><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                            <!--                    <li><a href="#action"><span>Action</span></a></li>-->
                            <!--                    <li><a href="#driverDetail"><span>Driver</span></a></li>
                                                <li><a href="#cleanerDetail"><span>Cleaner</span></a></li>-->
                            <!--                    <li><a href="#advDetail"><span>Advance</span></a></li>-->
                            <!--<li><a href="#expDetail"><span>Expense Details</span></a></li>-->
                            <!--                    <li><a href="#summary"><span>Remarks</span></a></li>-->
                        </ul>

                        <div id="tripDetail">
                            <table  class="table table-info mb30 table-hover" id="bg">
                                <thead> <tr>
                                        <th  colspan="4" >Trip Details</th>
                                    </tr></thead>

                                <c:if test = "${tripDetails != null}" >
                                    <c:forEach items="${tripDetails}" var="trip">
                                        <tr>
                                            <!--                        <td ><font color="red">*</font>Trip Sheet Date</td>
                                                                        <td ><input type="text" name="tripDate" class="datepicker" value=""></td>
                                            -->
                                            <td >CNote No(s)</td>
                                            <td >
                                                <c:out value="${trip.cNotes}" />
                                            </td>
                                            <td >Billing Type</td>
                                            <td >
                                                <c:out value="${trip.billingType}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <!--                            <td >Customer Code</td>
                                                                        <td >BF00001</td>-->
                                            <td >Customer Name</td>
                                            <td >
                                                <c:out value="${trip.customerName}" />
                                                <input type="hidden" name="customerName" Id="customerName" class="form-control" style="width:150px;height:40px" value='<c:out value="${trip.customerName}" />'>
                                                <input type="hidden" name="tripId" Id="tripId" class="form-control" style="width:150px;height:40px" value='<c:out value="${trip.tripId}" />'>
                                                <input type="hidden" name="originId" Id="originId" class="form-control" style="width:150px;height:40px" value='<c:out value="${trip.originId}" />'>
                                                <input type="hidden" name="vehicleTypeId" Id="vehicleTypeId" class="form-control" style="width:150px;height:40px" value='<c:out value="${trip.vehicleTypeId}" />'>
                                                <input type="hidden" name="vehicleNo1" Id="vehicleNo1" class="form-control" style="width:150px;height:40px" value='<c:out value="${trip.vehicleNo}" />'>
                                            </td>
                                            <td >Customer Type</td>
                                            <td  colspan="3" >
                                                <c:out value="${trip.customerType}" />
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td >Route Name</td>
                                            <td >
                                                <c:out value="${trip.routeInfo}" />
                                            </td>

                                            <td >Vehicle Type</td>
                                            <td >
                                                <c:out value="${trip.vehicleTypeName}" />
                                            </td>

                                        </tr>
                                        <tr>
                                            <td >Old Vehicle No</td>
                                            <td >
                                                <c:out value="${trip.vehicleNo}" />
                                                <input type="hidden" name="oldVehicleId" Id="oldVehicleId" class="form-control" style="width:150px;height:40px" value="<c:out value="${trip.vehicleId}" />"  >
                                                <input type="hidden" name="oldVehicleNo" Id="oldVehicleNo" class="form-control" style="width:150px;height:40px" value="<c:out value="${trip.vehicleNo}" />"  >
                                            </td>

                                            <td >Old <c:out value="${sealnotype}"/>
                                                <input type="hidden" name="sealnotype" id="sealnotype" value="<c:out value="${sealnotype}"/>"/></td>
                                            <td ><c:out value="${sealno}"/></td>
                                        </tr>
                                        <tr>

                                            <td >Trip Schedule</td>
                                            <td ><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" />
                                                <input type="hidden" name="tripScheduleDate"  id="tripScheduleDate" value='<c:out value="${trip.tripScheduleDate}" />'>
                                                <input type="hidden" name="tripScheduleTime" id="tripScheduleTime" value='<c:out value="${trip.tripScheduleTime}" />'>
                                                <input type="hidden" name="driver1Name" readonly   Id="driver1Name" class="form-control" style="width:150px;height:40px" value="-"  >
                                                <input type="hidden" name="driver1Id" Id="driver1Id" class="form-control" style="width:150px;height:40px" value="0"  >
                                            </td>

                                            <td >Vehicle Change City</td>
                                            <td  width="80"> <select name="cityId" id="cityId" class="form-control" style="width:150px;height:40px" >
                                                    <c:if test="${cityList != null}">
                                                        <option value="" selected>--Select--</option>
                                                        <c:forEach items="${cityList}" var="cityList">
                                                            <option value='<c:out value="${cityList.cityId}"/>'><c:out value="${cityList.cityName}"/></option>
                                                        </c:forEach>
                                                    </c:if>
                                                </select>
                                                <script>
                                                    $('#cityId').select2({placeholder: 'Fill vehicleNo'});
                                                </script>

                                        </tr>
                                        <tr style="display: none">
                                            <td >Reefer Required</td>
                                            <td  >
                                                <c:out value="${trip.reeferRequired}" />
                                            </td>
                                            <td >Order Est Weight (MT)</td>
                                            <td  >
                                                <c:out value="${trip.totalWeight}" />
                                                <input type="hidden" name="totalWeight" Id="totalWeight" class="form-control" style="width:150px;height:40px" value='<c:out value="${trip.totalWeight}" />' readonly >
                                            </td>
                                        </tr>

                                        <tr>
                                            <td ><font color="red">*</font>New Vehicle No</td>
                                            <td >
                                                <input type="text" name="vehicleNo" id="vehicleNo"  value=""  class="form-control" style="width:150px;height:40px" >
                                                <input type="hidden" name="vehicleId" id="vehicleId"  value=""  class="form-control" style="width:175px;height:40px" >
                                            </td>
                                        
                                        <c:if test ="${sealnotype!=''}">
                                            <td ><font color="red">*</font><c:out value="${sealnotype}"/></td>
                                            <td >
                                                <input type="text" name="sealNo" id="sealNo"  value=""  class="form-control" style="width:150px;height:40px" >
                                            </td>
                                        </c:if>

                                        </td>
                                        </tr>


                                        <tr >
                                            <td ><font color="red">*</font>Vehicle Change Date</td>
                                            <td ><input type="text" name="vehicleChangeDate" id="vehicleChangeDate"  class="datepicker , form-control" style="width:150px;height:40px" value="<%=startDate%>" >

                                            <td ><font color="red">*</font>Vehicle Change Time</td>
                                            <td  height="25" >
                                                HH:<select name="vehicleChangeHour" id="vehicleChangeHour" class="textbox" style="width:50px;height:30px" ><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                                MI:<select name="vehicleChangeMinute" id="vehicleChangeMinute" class="textbox" style="width:50px;height:30px"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>

                                        </tr>
                                        <tr style="display: none">
                                            <td >Last Vehicle Start Odometer Reading</td>
                                            <td >
                                                <input type="hidden" name="lastVehicleStartKm" Id="lastVehicleStartKm" class="form-control" style="width:150px;height:40px" value='<c:out value="${trip.startKm}"/>'  style="width: 80px" >
                                                <c:out value="${trip.startKm}"/>
                                            </td>
                                            <td >Last Vehicle End Odometer Reading</td>
                                            <td >
                                                <input type="text" name="lastVehicleEndKm" Id="lastVehicleEndKm" class="form-control" style="width:150px;height:40px" value='0'  style="width: 80px" onKeyPress='return onKeyPressBlockCharacters(event);' onkeyup="calculateKm()">
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td >Last Vehicle Total Odometer Reading</td>
                                            <td >
                                                <label id="totalKm">0</label>
                                            </td>
                                        <script>
                                            function calculateKm() {
                                                var lastVehicleStartKm = $("#lastVehicleStartKm").val();
                                                var lastVehicleEndKm = $("#lastVehicleEndKm").val();
                                                var res = parseFloat(lastVehicleEndKm) - parseFloat(lastVehicleStartKm);
                                                $("#totalKm").text(res);
                                            }
                                        </script>
                                        <td >Last Vehicle Start Reefer HM</td>
                                        <td >
                                            <input type="hidden" name="lastVehicleStartOdometer" Id="lastVehicleStartOdometer" class="form-control" style="width:150px;height:40px" value='<c:out value="${trip.startHm}"/>' style="width: 80px" >
                                            <c:out value="${trip.startHm}"/>
                                        </td>
                                        </tr>
                                        <tr style="display: none">

                                            <td >Last Vehicle End Reefer HM</td>
                                            <td >
                                                <input type="text" name="lastVehicleEndOdometer" Id="lastVehicleEndOdometer" class="form-control" style="width:150px;height:40px" value='0' style="width: 80px" onKeyPress='return onKeyPressBlockCharacters(event);' onkeyup="calculateHm()">
                                            </td>
                                        <script>
                                            function calculateHm() {
                                                var lastVehicleStartOdometer = $("#lastVehicleStartOdometer").val();
                                                var lastVehicleEndOdometer = $("#lastVehicleEndOdometer").val();
                                                var res = parseFloat(lastVehicleEndOdometer) - parseFloat(lastVehicleStartOdometer);
                                                $("#totalHm").text(res);
                                            }
                                        </script>
                                        <td >Last Vehicle Total Reefer HM</td>
                                        <td >
                                            <label id="totalHm">0</label>
                                        </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td >New Vehicle Start Odometer Reading</td>
                                            <td  >
                                                <input type="text" name="vehicleStartKm" Id="vehicleStartKm" class="form-control" style="width:150px;height:40px" value='0' style="width: 80px" >
                                            </td>
                                            <td >New Vehicle Start Reefer Hm</td>
                                            <td  >
                                                <input type="text" name="vehicleStartOdometer" Id="vehicleStartOdometer" class="form-control" style="width:150px;height:40px" value='0' style="width: 80px" >
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td >Vehicle Capacity (MT)</td>
                                            <td ><input type="text" name="vehicleTonnage" Id="vehicleTonnage" readonly class="form-control" style="width:150px;height:40px" value="0"></td>
                                            <td >Veh. Cap [Util%]</td>
                                            <td ><input type="text" name="vehicleCapUtil" Id="vehicleCapUtil" readonly class="form-control" style="width:150px;height:40px" value="0"></td>
                                        </tr>


                                        <input type="hidden" name="driver2Name"  readonly Id="driver2Name" class="form-control" style="width:150px;height:40px" value=""  />
                                        <input type="hidden" name="driver2Id" Id="driver2Id" class="form-control" style="width:150px;height:40px" value=""  />
                                        <input type="hidden" name="driver3Name"  readonly  Id="driver3Name" class="form-control" style="width:150px;height:40px" value=""  />
                                        <input type="hidden" name="driver3Id" Id="driver3Id" class="form-control" style="width:150px;height:40px" value=""  />
                                        <input type="hidden" name="consignmentId" Id="consignmentId" value="<c:out value="${consid}"/>"  />

                                        <tr >
                                            <td ><font color="red">*</font>EwayBill Swapped Date</td>
                                            <td ><input type="text" name="ewayBillDate" id="ewayBillDate"  class="datepicker , form-control" style="width:150px;height:40px" value="<%=startDate%>" >

                                            <td ><font color="red">*</font>EwayBill Swapped Time</td>
                                            <td  height="25" >
                                                HH:<select name="ewaybillHour" id="ewaybillHour" class="textbox" style="width:50px;height:30px" ><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                                MI:<select name="ewaybillMinute" id="ewaybillMinute" class="textbox" style="width:50px;height:30px"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>

                                        </tr>
                                        <tr>         
                                              <td><font color="red">*</font>Driver Mobile</td>
                                              <td><input type="text" name="driverMobile" id="driverMobile" maxlength="10" class="form-control" style="width:150px;height:40px" value="" ></td>
                                            <td>Insurance Survey Required</td>
                                            <td>
                                                <select name="isSurveyReq" id="isSurveyReq" class="form-control" style="width:150px;height:40px" style="height:20px; width:122px;" >
                                                    <option value="YES">YES</option>
                                                    <option value="NO">NO</option>
                                                </select>
                                            </td>



                                        </tr>
                                        <tr>
                                            <td>Remarks</td>
                                            <td>
                                                <textarea rows="3" cols="30" class="form-control" style="width:300px;height:50px" name="vehicleRemarks" id="vehicleRemarks"   style="width:142px"></textarea>
                                            </td>                                                
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                            </table>

                            </table>
                            <table class="table table-info mb30 table-hover" id="fileAddRow" >

                                <thead>
                                    <tr>
                                        <th  height="30">Sno</th>
                                        <th  height="30">Document</th>
                                        <th  height="30">Upload</th>
                                    </tr>
                                </thead>
                                <tr>
                                    <td  height="30">1</td>
                                    <td>Cross Docking Details</td>
                                    <td  height="30"><input class='textbox'  type='file' name='insurance' id='insurance' style="width:200px;height:25px;" /></td>
                                </tr>
                                <tr>
                                    <td  height="30">2</td>
                                    <td>FIR</td>
                                    <td  height="30"><input class='textbox'  type='file' name='roadTax' id='roadTax' style="width:200px;height:25px;" /></td>
                                </tr>
                                <tr>
                                    <td  height="30">3</td>
                                    <td>Survey Report</td>
                                    <td  height="30"><input class='textbox'  type='file' name='fc' id='fc' style="width:200px;height:25px;" /></td>
                                </tr>
                                <tr>
                                    <td  height="30">4</td>
                                    <td>Incident Report</td>
                                    <td  height="30"><input class='textbox'  type='file' name='permit' id='permit' style="width:200px;height:25px;" /></td>
                                </tr>

                            </table>                           

                            <br/>
                            <br/>
                            <center>
                                <input type="button" onclick="saveAction();" class="btn btn-success" value="save" name="Next" id="save"/>
                            </center>
                        </div>
                        <div id="routeDetail">

                            <c:if test = "${tripPointDetails != null}" >
                                <table class="table table-info mb30 table-hover" >
                                    <thead><tr >
                                            <th  height="30" >Point Name</th>
                                            <th  height="30" >Type</th>
                                            <th  height="30" >Route Order</th>
                                            <th  height="30" >Address</th>
                                            <th  height="30" >Planned Date</th>
                                            <th  height="30" >Planned Time</th>
                                        </tr></thead>
                                        <c:forEach items="${tripPointDetails}" var="tripPoint">

                                        <tr >
                                            <td  height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                            <td  height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                            <td  height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                            <td  height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                            <td  height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                            <td  height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                                        </tr>




                                    </c:forEach >
                                </table>
                            </c:if>

                            <!--
                                                <center>
                                                    <a  class="nexttab" href="#"><input type="button" class="btn btn-success" value="Next" name="Next" /></a>
                                                </center>
                            
                            
                                                <br>
                                                <br>
                                            </div>
                            
                                            <div id="action">-->
                            <br>
                            <br>

                            <br>

                        </div>




                        <script>
                            $(".nexttab").click(function() {
                                var selected = $("#tabs").tabs("option", "selected");
                                $("#tabs").tabs("option", "selected", selected + 1);
                            });
                        </script>

                    </div>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>