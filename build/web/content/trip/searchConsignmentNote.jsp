<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<!--<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>-->
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<!--<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>-->
<style>
    .form-control:focus{border-color: #5cb85c;  box-shadow: none; -webkit-box-shadow: none;} 
    .has-error .form-control:focus{box-shadow: none; -webkit-box-shadow: none;}
</style>
<meta http-equiv="ConConsignment Notent-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">


    function viewConsignmentDetails(orderId) {
        window.open('/throttle/getConsignmentDetails.do?consignmentOrderId=' + orderId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

</head>


<script type="text/javascript">
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('~');
                $('#customerId').val(tmp[0]);
                $('#customerName').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>

<script>


    function submitPageForTripGeneration() {
        var temp = "";
        var selectedConsignment = document.getElementsByName('selectedIndex');
        var consignmentOrder = document.getElementsByName('consignmentOrderId');

        var tripType = $("#tripType").val();
        //alert("1:"+selectedConsignment.length);
        var cntr = 0;
        for (var i = 0; i < selectedConsignment.length; i++) {
            //alert(selectedConsignment[i].checked);
            if (selectedConsignment[i].checked == true) {

                if (cntr == 0) {
                    temp = consignmentOrder[i].value;
                } else {
                    temp = temp + "," + consignmentOrder[i].value;
                }
                cntr++;
            }
        }
        //alert(temp);
        //alert(cntr);
        if (cntr > 0) {
            //alert("am here..");
            document.CNoteSearch.action = "/throttle/createTripSheet.do?consignmentOrderNos=" + temp + "&tripType=" + tripType;
            //alert("am here..1:"+document.CNoteSearch.action);
            document.CNoteSearch.submit();
            //alert("am here..2");
        } else {
            alert("Please select consignment order and proceed");
        }
    }



    function searchPage(val) {
        var temp = "";
        var selectedConsignment = document.getElementsByName('selectedIndex');
        var consignmentOrder = document.getElementsByName('consignmentOrderId');
        var tripType = $("#tripType").val();
        //alert("1:"+selectedConsignment.length);
        var cntr = 0;
        for (var i = 0; i < selectedConsignment.length; i++) {
            //alert(selectedConsignment[i].checked);
            if (selectedConsignment[i].checked == true) {

                if (cntr == 0) {
                    temp = consignmentOrder[i].value;
                } else {
                    temp = temp + "," + consignmentOrder[i].value;
                }
                cntr++;
            }
        }
        //        alert("temp"+temp);
        if (cntr > 0 && val == 'ExcelExport') {
            document.CNoteSearch.action = "/throttle/tripPlanning.do?param=" + val + "&consignmentOrderIds=" + temp;
            document.CNoteSearch.submit();
        } else if (cntr == 0 && val == 'ExcelExport') {
            alert("Please select consignment order and proceed");
        } else if (cntr == 0 && val == 'Search') {
            document.CNoteSearch.action = "/throttle/tripPlanning.do?param=" + val + "&consignmentOrderIds=" + temp;
            document.CNoteSearch.submit();
        }
    }


    function importExcel() {
        document.CNoteSearch.action = '/throttle/BrattleFoods/tripPlanningImport.jsp';
        document.CNoteSearch.submit();
    }


    function importExcel() {
        document.CNoteSearch.action = '/throttle/BrattleFoods/tripPlanningImport.jsp';
        document.CNoteSearch.submit();
    }
</script>

<body>
    <%
                String menuPath = "Consignment Note >> View / Edit";
                request.setAttribute("menuPath", menuPath);
    %>
    <form name="CNoteSearch" method="post" enctype="multipart">
        <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Trip Planning" text="Trip Planning"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
                    <li class=""><spring:message code="hrms.label.Trip Planning" text="Trip Planning"/></li>

                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <br>
                    <br>
                    <%--
                    <table width="900" height="50" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                        <tr id="exp_table" >
                            <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                                <div class="tabs" align="left" style="width:850;">
                                    <ul class="tabNavigation">
                                        <li style="background:#76b3f1;width:850;">CONSIGNMENT ORDERS</li>
                                    </ul>
                                    <div id="first">
                                        <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                            <tr>
                                                <td >Customer Name</td>
                                                <td>
                                                    <input type="hidden"  name="customerId" id="customerId" style="width: 110px" value="<c:out value="${customerId}"/>"/>
                                                    <input type="text"  name="customerName" id="customerName" style="width: 110px" value="<c:out value="${customerName}"/>"/>
                                                </td>

                                    <td >Customer Order Ref No</td>
                                    <td><input type="text"  name="customerOrderReferenceNo" id="customerOrderReferenceNo" style="width: 110px" value="<c:out value="${customerOrderReferenceNo}"/>"/></td>

                                    <td >Consignment Order No</td>
                                    <td><input type="text"  name="consignmentOrderReferenceNo" id="consignmentOrderReferenceNo" style="width: 110px" value="<c:out value="${consignmentOrderReferenceNo}"/>"/></td>


                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td><select name="status"  id="status"  style="width:120px;" >
                                            <option value="">--Select--</option>
                                            <option value="5">Order Created</option>
                                            <option value="4">Order Cancelled</option>
                                            <option value="3">Order On Hold</option>
                                        </select>
                                        <script>
                                            document.getElementById("status").value = '<c:out value="${status}"/>';
                                        </script>    
                                    </td>
                                    <td><font color="red">*</font>From Date</td>
                                    <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                                    <td><font color="red">*</font>To Date</td>
                                    <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>

                                </tr>
                                <tr align="center">
                                    <td colspan="2"><input type="button" class="btn btn-success"   value="Search" onclick="searchPage()"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
                    --%>
                    <table class="table table-info mb30 table-hover" style="width:90%" >
                        <thead><tr><th colspan="4">CONSIGNMENT ORDERS</th></tr></thead>
                        <tr>
                            <td >Customer Name</td>
                            <td>
                                <input type="hidden"  name="customerId" id="customerId" style="width:120px;" value="<c:out value="${customerId}"/>"/>
                                <input type="hidden"  name="tripType" id="tripType" style="width:120px;" value="<c:out value="${tripType}"/>"/>
                                <input type="hidden"  name="cbt" id="cbt" value="0"/>
                                <input type="text"  name="customerName" id="customerName" class="form-control" style="width:250px;height:40px" value="<c:out value="${customerName}"/>"/>
                            </td>

                            <td >Customer Order Ref No</td>
                            <td><input type="text"  name="customerOrderReferenceNo" id="customerOrderReferenceNo" class="form-control" style="width:250px;height:40px" value="<c:out value="${customerOrderReferenceNo}"/>"/></td>
                        </tr>
                        <tr>
                            <td >Consignment Order No</td>
                            <td><input type="text"  name="consignmentOrderReferenceNo" id="consignmentOrderReferenceNo" class="form-control" style="width:250px;height:40px" value="<c:out value="${consignmentOrderReferenceNo}"/>"/></td>


                            <td>Status</td>
                            <td><select name="status"  id="status1"  class="form-control" style="width:250px;height:40px" >
                                    <option value="">--Select--</option>
                                    <option value="5">Order Created</option>
                                    <option value="4">Order Cancelled</option>
                                    <option value="3">Order On Hold</option>
                                </select>
                                <script>
                                    document.getElementById("status").value = '<c:out value="${status}"/>';
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>From Date </td>
                            <td height="30"><input name="fromDate" id="fromDate" type="text"  class="datepicker , form-control" style="width:250px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                            <td><font color="red">*</font>To Date</td>
                            <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker , form-control" style="width:250px;height:40px" class="datepicker" onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>

                        </tr>
                        <tr align="center">
                            <!--                                    <td colspan="2"><input type="button"   value="Generate Excel" class="btn btn-success" name="search" onClick="exportExcel()" style="width:150px"></td>
                                                                <td colspan="2"><input type="button"   value="Upload Trip Planning" class="btn btn-success" name="search" onClick="importExcel()" style="width:200px"></td>-->
                            <td colspan="4"><input type="button" class="btn btn-success"   value="Search" onclick="searchPage('Search')"></td>
                        </tr>
                    </table>
                    <br>
                    <c:if test = "${consignmentList != null}" >
                        <center>
                            &nbsp;&nbsp;<input type="button" class="btn btn-success" name="trip"  value="Trip Planning" onclick="submitPageForTripGeneration()"/>
                        </center>
                        <table class="table table-info mb30 table-hover " id="table"  >
                            <thead>
                                <tr height="40">
                                    <th>S.No</th>
                                    <th>Select</th>
                                    <th>Consignment No</th>
                                    <th>Consignment Date</th>                                    
                                    <th>Customer Name </th>
                                    <th>Origin </th>
                                    <th>Destination </th>
                                    <th>Order Type </th>
                                    <th>Status </th>
                                    <th>Pallet</th>
                                    <th>Vehicle Required On </th>                                    
                                    <th>Consignment Status Change </th>
                                </tr>
                            </thead>
                            <% int index = 0;
                               int sno = 1;
                            %>
                            <tbody>
                                <c:forEach items="${consignmentList}" var="cnl">

                                    <tr height="30">
                                        <td align="left" ><%=sno%></td>

                                        <td align="left" >
                                            <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                                            <c:if test="${cnl.paymentType == 2}">
                                                <c:if test = "${cnl.advanceStatus == 0}" >
                                                    <a href='/throttle/viewPaymentDetails.do?consignmentOrderId=<c:out value="${cnl.consignmentOrderId}"/>&freightAmount=<c:out value="${cnl.freightAmount}"/>&paymentType=<c:out value="${cnl.paymentType}"/>&nextPage=1'>Proceed To Pay</a>
                                                </c:if>
                                                <c:if test = "${cnl.advanceStatus == 1}" >
                                                    <input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno - 1%>" value="<c:out value="${cnl.consignmentOrderId}"/>"  onclick="checkSelectedItem('<%=sno - 1%>', '<c:out value="${cnl.orderType}"/>');"/>
                                                </c:if>
                                            </c:if>
                                            <c:if test="${cnl.paymentType == 3}">
                                                <c:if test = "${cnl.advanceToPayStatus == 0}" >
                                                    <a href='/throttle/viewPaymentDetails.do?consignmentOrderId=<c:out value="${cnl.consignmentOrderId}"/>&freightAmount=<c:out value="${cnl.freightAmount}"/>&paymentType=<c:out value="${cnl.paymentType}"/>&nextPage=1'>Proceed To Pay</a>
                                                </c:if>
                                                <c:if test = "${cnl.advanceToPayStatus == 1}" >
                                                    <input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno - 1%>" value="<c:out value="${cnl.consignmentOrderId}"/>" onclick="checkSelectedItem('<%=sno - 1%>', '<c:out value="${cnl.orderType}"/>');"/>
                                                </c:if>
                                            </c:if>
                                            <c:if test="${cnl.paymentType == 1 || cnl.paymentType ==  4 || cnl.paymentType ==  0}">
                                                <input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno - 1%>" value="<c:out value="${cnl.consignmentOrderId}"/>" onclick="checkSelectedItem('<%=sno - 1%>', '<c:out value="${cnl.orderType}"/>');" />
                                            </c:if>
                                        </td>
                                        <td align="left" ><input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=index%>" value="<c:out value="${cnl.consignmentOrderId}"/>"/>
                                            <a href="#" onclick="viewConsignmentDetails('<c:out value="${cnl.consignmentOrderId}"/>');"><c:out value="${cnl.consignmentNoteNo}"/></a>
                                        </td>
                                        <td align="left" ><c:out value="${cnl.createdOn}"/></td>                                        
                                        <td align="left" ><c:out value="${cnl.customerName}"/></td>
                                        <td align="left" ><c:out value="${cnl.consigmentOrigin}"/></td>
                                        <td align="left" ><c:out value="${cnl.consigmentDestination}"/></td>
                                        <td align="left" ><input type="hidden" id="orderTypes<%=sno - 1%>" name="orderTypes" value="<c:out value="${cnl.orderType}"/>"/><c:out value="${cnl.orderType}"/></td>
                                        <td align="left" >Order Created</td>
                                        <td align="left" >
                                            <c:out value="${cnl.totalPackages}"/> 
                                        </td>
                                        <td align="left" ><c:out value="${cnl.tripScheduleDate}"/>&nbsp;<c:out value="${cnl.tripScheduleTime}"/></td>
                                        
                                        <td><a href='/throttle/makeConsignmentComplete.do?consignmentOrderId=<c:out value="${cnl.consignmentOrderId}"/>'>Mark as Complete</a></td>
                                    </tr>
                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <center>
                        &nbsp;&nbsp;<input type="button" class="btn btn-success" name="trip"  value="Trip Planning" onclick="submitPageForTripGeneration()"/>
                    </center>
                    <script language="javascript" type="text/javascript">
                        function checkSelectedItem(sno, orderType) {
                            if (document.getElementById("selectedIndex" + sno).checked) {
                                var selectedConsignment = document.getElementsByName('selectedIndex');
                                var orderTypes = document.getElementsByName('orderTypes');
//                                 if (orderType == "FTL") {
//                                    for (var i = 0; i < selectedConsignment.length; i++) {
//                                        if (sno != i) {
//                                            if (orderTypes[i].value == "FTL") {
//                                                selectedConsignment[i].checked = false;
//                                            } 
//                                            else if (orderTypes[i].value == "LTL") {
//                                                selectedConsignment[i].checked = false;
//                                            }
//
//                                        }
//    
//                                    }
//                                }
//
//                            }
//                        }

                                if (orderType == "CBT") {
                                    for (var i = 0; i < selectedConsignment.length; i++) {

                                        if (sno != i) {
                                            if (orderTypes[i].value == "CBT") {

                                            } else if (orderTypes[i].value == "FTL") {
                                                selectedConsignment[i].checked = false;
                                            } else if (orderTypes[i].value == "LTL") {
                                                selectedConsignment[i].checked = false;
                                            }
                                        }

                                    }
                                    document.getElementById("cbt").value = 1;
                                }
                                if (orderType == "LTL") {
                                    document.getElementById("cbt").value = 0;
                                    for (var i = 0; i < selectedConsignment.length; i++) {

                                        if (sno != i) {
                                            if (orderTypes[i].value == "LTL") {

                                            } else if (orderTypes[i].value == "FTL") {
                                                selectedConsignment[i].checked = false;
                                            } else if (orderTypes[i].value == "CBT") {
                                                selectedConsignment[i].checked = false;
                                            }
                                        }

                                    }
                                }
                                if (orderType == "FTL") {
                                    document.getElementById("cbt").value = 0;
                                    for (var i = 0; i < selectedConsignment.length; i++) {
                                        if (sno != i) {
                                            if (orderTypes[i].value == "FTL") {
                                                selectedConsignment[i].checked = false;
                                            } else if (orderTypes[i].value == "LTL") {
                                                selectedConsignment[i].checked = false;
                                            } else if (orderTypes[i].value == "CBT") {
                                                selectedConsignment[i].checked = false;
                                            }

                                        }

                                    }
                                }

                            }
                        }

                    </script>


                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5"  selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sort");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>

                    </form>
                    </body>
                </div>
            </div>
        </div>
        <%@ include file="../common/NewDesign/settings.jsp" %>