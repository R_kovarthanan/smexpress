<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
    </head>
      <%
                String menuPath = "Finance >> Daily Advance Advice";
                request.setAttribute("menuPath", menuPath);
                String dateval = request.getParameter("dateval");
                String active = request.getParameter("active");
                String type = request.getParameter("type");
    %>
     
    <body>

        <form name="tripSheet" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "Consignment Report-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>
        
      

            <br>
            <br>
            <br>

<br><br>
            
            <br>
            <br>
              <c:if test = "${consignmentList != null}" >
                 <table class="table table-info mb30 table-hover" id="table" >
                <thead>
                    <tr height="40">
                        <th>Sno</th>
                        <th>Company</th>
                        <th>Consignment No</th>
                        <th>Vehicle Required Date </th>
                        <th>Customer Name </th>
                        <th>Origin </th>
                        <th>Destination </th>
                        <th>Trip Code</th>
                        <th>Trip Actual Start Date</th>
                        <th>Trip Actual End Date</th>
                        <th>Status </th>
                        <th>Created By</th>
                        <th>Created Date</th>
                    </tr>
                </thead>
                <% int index = 0;
                            int sno = 1;
                %>
                <tbody>
                    <c:forEach items="${consignmentList}" var="cnl">

                        <tr height="30">
                            <td align="left" ><%=sno%></td>
                            <td align="left" >
                                <c:out value="${cnl.companyId}"/>
                            <td align="left" >
                                <c:out value="${cnl.consignmentNoteNo}"/>
                            </td>
                            <td align="left" ><c:out value="${cnl.tripScheduleDate}"/>&nbsp;<c:out value="${cnl.tripScheduleTime}"/></td>
                            <td align="left" ><c:out value="${cnl.customerName}"/></td>
                            <td align="left" ><c:out value="${cnl.consigmentOrigin}"/></td>
                            <td align="left" ><c:out value="${cnl.consigmentDestination}"/></td>
                               <td align="left" >
                            <c:if test = "${cnl.tripCode == null}" >
                            -
                            </c:if>
                            <c:if test = "${cnl.tripCode != null}" >
                                <c:out value="${cnl.tripCode}"/>
                            </c:if>
                             </td>
                            <td align="left" ><c:out value="${cnl.tripStartDate}"/></td>
                            <td align="left" ><c:out value="${cnl.tripEndDate}"/></td>
                         
                            <td align="left" ><c:out value="${cnl.statusName}"/></td>
                             <td>
                                 <c:out value="${cnl.userName}"/>
                             </td>
                             <td>
                                 <c:out value="${cnl.createdOn}"/>
                             </td>
                        </tr>
                        <%index++;%>
                        <%sno++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
        </form>
    </body>
</html>