<%-- 
    Document   : invoicePrint
    Created on : 11 Oct, 2018, 10:52:44 AM
    Author     : Ram
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
    <HEAD>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <META http-equiv="X-UA-Compatible" content="IE=8">
        <TITLE>Invoice</TITLE>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <META name="generator" content="BCL easyConverter SDK 5.0.140">
        <script>
            function callprint()
            {                
                document.getElementById("printbtn").style.display='none';
                window.print();
            }
        </script>
        <STYLE type="text/css">

            body {margin-top: 0px;margin-left: 0px;}

            #page_1 {position:relative; overflow: hidden;margin: 1px 0px 7px 5px;padding: 0px;border: none;width: 900px;}
            #page_1 #id1_1 {border:none;margin: 0px 0px 0px 193px;padding: 0px;border:none;width: 618px;overflow: hidden;}
            #page_1 #id1_2 {border:none;margin: 0px 0px 0px 0px;padding: 0px;border:none;width: 751px;overflow: hidden;}
            #page_1 #id1_2 #id1_2_1 {float:left;border:none;margin: 0px 0px 0px 0px;padding: 0px;border:none;width: 455px;overflow: hidden;}
            #page_1 #id1_2 #id1_2_2 {float:left;border:none;margin: 0px 0px 0px 34px;padding: 0px;border:none;width: 262px;overflow: hidden;}
            #page_1 #id1_3 {border:none;margin: 26px 0px 0px 0px;padding: 0px;border:none;width: 900px;overflow: hidden;}
            #page_1 #id1_4 {border:none;margin: 149px 0px 0px 756px;padding: 0px;border:none;width: 55px;overflow: hidden;}

            #page_1 #p1dimg1 {position:absolute;top:3px;left:0px;z-index:-1;width:801px;height:962px;}
            #page_1 #p1dimg1 #p1img1 {width:801px;height:962px;}



            #page_1 #tx1 {position:absolute;top:950px;left:15px;width:40px;height:11px;}
            #page_1 #tx2 {position:absolute;top:965px;left:14px;width:322px;height:10px;}
            #page_1 #tx3 {position:absolute;top:974px;left:14px;width:317px;height:10px;}
            #page_1 #tx4 {position:absolute;top:983px;left:14px;width:108px;height:10px;}
            #page_1 #tx5 {position:absolute;top:992px;left:14px;width:332px;height:10px;}
            #page_1 #tx6 {position:absolute;top:1112px;left:1px;width:273px;height:10px;}

            .ft0{font: 9px 'Arial';line-height: 11px;}
            .ft1{font: 8px 'Arial';line-height: 10px;}
            .ft2{font: 16px 'Arial';line-height: 18px;}
            .ft3{font: 11px 'Arial';line-height: 14px;}
            .ft4{font: 11px 'Arial';line-height: 13px;}
            .ft5{font: 15px 'Arial';line-height: 18px;}
            .ft6{font: 13px 'Arial';line-height: 16px;}
            .ft7{font: 12px 'Arial';line-height: 13px;}
            .ft8{font: 11px 'Arial';line-height: 13px;}
            .ft9{font: 1px 'Arial';line-height: 12px;}
            .ft10{font: 11px 'Arial';line-height: 12px;}
            .ft11{font: 1px 'Arial';line-height: 1px;}
            .ft12{font: 12px 'Arial';line-height: 14px;}
            .ft13{font: 1px 'Arial';line-height: 2px;}
            .ft14{font: 1px 'Arial';line-height: 4px;}
            .ft15{font: 1px 'Arial';line-height: 5px;}
            .ft16{font: 9px 'Times';line-height: 12px;}
            .ft17{font: 1px 'Arial';line-height: 8px;}
            .ft18{font: 8px 'Arial';line-height: 8px;}
            .ft19{font: 1px 'Arial';line-height: 9px;}
            .ft20{font: 8px 'Arial';line-height: 9px;}
            .ft21{font: 1px 'Arial';line-height: 10px;}

            .p0{text-align: left;margin-top: 0px;margin-bottom: 0px;}
            .p1{text-align: left;margin-top: 3px;margin-bottom: 0px;}
            .p2{text-align: left;padding-right: 201px;margin-top: 0px;margin-bottom: 0px;}
            .p3{text-align: left;padding-right: 165px;margin-top: 1px;margin-bottom: 0px;}
            .p4{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p5{text-align: left;padding-left: 7px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p6{text-align: left;padding-left: 9px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p7{text-align: left;padding-left: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p8{text-align: left;padding-left: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p9{text-align: left;padding-left: 8px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p10{text-align: right;padding-right: 29px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p11{text-align: right;padding-right: 106px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p12{text-align: left;padding-left: 15px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p13{text-align: right;padding-right: 46px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p14{text-align: left;padding-left: 16px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p15{text-align: center;padding-right: 25px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p16{text-align: left;padding-left: 22px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p17{text-align: left;padding-left: 5px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p18{text-align: left;padding-left: 11px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p19{text-align: left;padding-left: 14px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p20{text-align: left;padding-left: 10px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p21{text-align: left;padding-left: 52px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p22{text-align: right;padding-right: 14px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p23{text-align: right;padding-right: 20px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p24{text-align: left;padding-left: 21px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p25{text-align: left;padding-left: 6px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p26{text-align: right;padding-right: 12px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p27{text-align: right;padding-right: 48px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p28{text-align: left;padding-left: 46px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p29{text-align: right;padding-right: 3px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p30{text-align: right;padding-right: 13px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p31{text-align: right;padding-right: 102px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p32{text-align: right;padding-right: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p33{text-align: left;padding-left: 3px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p34{text-align: left;padding-left: 37px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p35{text-align: left;padding-left: 36px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p36{text-align: left;padding-left: 17px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p37{text-align: right;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p38{text-align: right;padding-right: 4px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p39{text-align: right;padding-right: 61px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
            .p40{text-align: left;padding-left: 4px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}

            .td0{padding: 0px;margin: 0px;width: 287px;vertical-align: bottom;}
            .td1{padding: 0px;margin: 0px;width: 158px;vertical-align: bottom;}
            .td2{padding: 0px;margin: 0px;width: 115px;vertical-align: bottom;}
            .td3{padding: 0px;margin: 0px;width: 242px;vertical-align: bottom;}
            .td4{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 98px;vertical-align: bottom;}
            .td5{padding: 0px;margin: 0px;width: 340px;vertical-align: bottom;}
            .td6{padding: 0px;margin: 0px;width: 147px;vertical-align: bottom;}
            .td7{padding: 0px;margin: 0px;width: 109px;vertical-align: bottom;}
            .td8{padding: 0px;margin: 0px;width: 39px;vertical-align: bottom;}
            .td9{padding: 0px;margin: 0px;width: 79px;vertical-align: bottom;}
            .td10{padding: 0px;margin: 0px;width: 31px;vertical-align: bottom;}
            .td11{padding: 0px;margin: 0px;width: 68px;vertical-align: bottom;}
            .td12{padding: 0px;margin: 0px;width: 35px;vertical-align: bottom;}
            .td13{padding: 0px;margin: 0px;width: 121px;vertical-align: bottom;}
            .td14{padding: 0px;margin: 0px;width: 96px;vertical-align: bottom;}
            .td15{padding: 0px;margin: 0px;width: 108px;vertical-align: bottom;}
            .td16{padding: 0px;margin: 0px;width: 93px;vertical-align: bottom;}
            .td17{padding: 0px;margin: 0px;width: 28px;vertical-align: bottom;}
            .td18{padding: 0px;margin: 0px;width: 34px;vertical-align: bottom;}
            .td19{padding: 0px;margin: 0px;width: 75px;vertical-align: bottom;}
            .td20{padding: 0px;margin: 0px;width: 218px;vertical-align: bottom;}
            .td21{padding: 0px;margin: 0px;width: 535px;vertical-align: bottom;}
            .td22{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 34px;vertical-align: bottom;}
            .td23{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 75px;vertical-align: bottom;}
            .td24{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 39px;vertical-align: bottom;}
            .td25{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 79px;vertical-align: bottom;}
            .td26{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 31px;vertical-align: bottom;}
            .td27{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 68px;vertical-align: bottom;}
            .td28{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 35px;vertical-align: bottom;}
            .td29{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 115px;vertical-align: bottom;}
            .td30{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 93px;vertical-align: bottom;}
            .td31{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 28px;vertical-align: bottom;}
            .td32{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 96px;vertical-align: bottom;}
            .td33{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 108px;vertical-align: bottom;}
            .td34{border-left: #000000 1px solid;padding: 0px;margin: 0px;width: 33px;vertical-align: bottom;}
            .td35{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 107px;vertical-align: bottom;}
            .td36{padding: 0px;margin: 0px;width: 114px;vertical-align: bottom;}
            .td37{padding: 0px;margin: 0px;width: 150px;vertical-align: bottom;}
            .td38{border-left: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 33px;vertical-align: bottom;}
            .td39{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 107px;vertical-align: bottom;}
            .td40{border-left: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 5px;vertical-align: bottom;}
            .td41{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 40px;vertical-align: bottom;}
            .td42{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 29px;vertical-align: bottom;}
            .td43{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 37px;vertical-align: bottom;}
            .td44{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 3px;vertical-align: bottom;}
            .td45{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 87px;vertical-align: bottom;}
            .td46{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 26px;vertical-align: bottom;}
            .td47{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 65px;vertical-align: bottom;}
            .td48{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 25px;vertical-align: bottom;}
            .td49{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 33px;vertical-align: bottom;}
            .td50{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 86px;vertical-align: bottom;}
            .td51{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 114px;vertical-align: bottom;}
            .td52{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 168px;vertical-align: bottom;}
            .td53{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 55px;vertical-align: bottom;}
            .td54{border-left: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 5px;vertical-align: bottom;}
            .td55{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 69px;vertical-align: bottom;}
            .td56{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 40px;vertical-align: bottom;}
            .td57{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 87px;vertical-align: bottom;}
            .td58{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 26px;vertical-align: bottom;}
            .td59{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 65px;vertical-align: bottom;}
            .td60{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 25px;vertical-align: bottom;}
            .td61{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 59px;vertical-align: bottom;}
            .td62{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 61px;vertical-align: bottom;}
            .td63{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 7px;vertical-align: bottom;}
            .td64{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 32px;vertical-align: bottom;}
            .td65{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 168px;vertical-align: bottom;}
            .td66{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 55px;vertical-align: bottom;}
            .td67{padding: 0px;margin: 0px;width: 6px;vertical-align: bottom;}
            .td68{padding: 0px;margin: 0px;width: 396px;vertical-align: bottom;}
            .td69{padding: 0px;margin: 0px;width: 61px;vertical-align: bottom;}
            .td70{padding: 0px;margin: 0px;width: 7px;vertical-align: bottom;}
            .td71{padding: 0px;margin: 0px;width: 32px;vertical-align: bottom;}
            .td72{padding: 0px;margin: 0px;width: 168px;vertical-align: bottom;}
            .td73{padding: 0px;margin: 0px;width: 56px;vertical-align: bottom;}
            .td74{padding: 0px;margin: 0px;width: 69px;vertical-align: bottom;}
            .td75{padding: 0px;margin: 0px;width: 37px;vertical-align: bottom;}
            .td76{padding: 0px;margin: 0px;width: 3px;vertical-align: bottom;}
            .td77{padding: 0px;margin: 0px;width: 87px;vertical-align: bottom;}
            .td78{padding: 0px;margin: 0px;width: 26px;vertical-align: bottom;}
            .td79{padding: 0px;margin: 0px;width: 65px;vertical-align: bottom;}
            .td80{padding: 0px;margin: 0px;width: 25px;vertical-align: bottom;}
            .td81{padding: 0px;margin: 0px;width: 33px;vertical-align: bottom;}
            .td82{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 5px;vertical-align: bottom;}
            .td83{border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 69px;vertical-align: bottom;}
            .td84{border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 37px;vertical-align: bottom;}
            .td85{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 2px;vertical-align: bottom;}
            .td86{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 86px;vertical-align: bottom;}
            .td87{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 25px;vertical-align: bottom;}
            .td88{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 64px;vertical-align: bottom;}
            .td89{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 24px;vertical-align: bottom;}
            .td90{border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 33px;vertical-align: bottom;}
            .td91{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 60px;vertical-align: bottom;}
            .td92{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 81px;vertical-align: bottom;}
            .td93{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 29px;vertical-align: bottom;}
            .td94{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 37px;vertical-align: bottom;}
            .td95{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 2px;vertical-align: bottom;}
            .td96{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 86px;vertical-align: bottom;}
            .td97{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 25px;vertical-align: bottom;}
            .td98{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 64px;vertical-align: bottom;}
            .td99{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 24px;vertical-align: bottom;}
            .td100{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 33px;vertical-align: bottom;}
            .td101{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 60px;vertical-align: bottom;}
            .td102{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 81px;vertical-align: bottom;}
            .td103{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 66px;vertical-align: bottom;}
            .td104{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 89px;vertical-align: bottom;}
            .td105{padding: 0px;margin: 0px;width: 46px;vertical-align: bottom;}
            .td106{padding: 0px;margin: 0px;width: 699px;vertical-align: bottom;}
            .td107{padding: 0px;margin: 0px;width: 40px;vertical-align: bottom;}
            .td108{padding: 0px;margin: 0px;width: 755px;vertical-align: bottom;}
            .td109{padding: 0px;margin: 0px;width: 356px;vertical-align: bottom;}
            .td110{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 68px;vertical-align: bottom;}
            .td111{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 153px;vertical-align: bottom;}
            .td112{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 61px;vertical-align: bottom;}
            .td113{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 75px;vertical-align: bottom;}
            .td114{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 28px;vertical-align: bottom;}
            .td115{padding: 0px;margin: 0px;width: 127px;vertical-align: bottom;}
            .td116{padding: 0px;margin: 0px;width: 207px;vertical-align: bottom;}
            .td117{padding: 0px;margin: 0px;width: 116px;vertical-align: bottom;}
            .td118{padding: 0px;margin: 0px;width: 502px;vertical-align: bottom;}
            .td119{padding: 0px;margin: 0px;width: 276px;vertical-align: bottom;}
            .td120{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 3px;vertical-align: bottom;}

            .tr0{height: 22px;}
            .tr1{height: 13px;}
            .tr2{height: 12px;}
            .tr3{height: 15px;}
            .tr4{height: 17px;}
            .tr5{height: 16px;}
            .tr6{height: 34px;}
            .tr7{height: 18px;}
            .tr8{height: 37px;}
            .tr9{height: 21px;}
            .tr10{height: 2px;}
            .tr11{height: 4px;}
            .tr12{height: 14px;}
            .tr13{height: 5px;}
            .tr14{height: 11px;}
            .tr15{height: 8px;}
            .tr16{height: 9px;}
            .tr17{height: 10px;}
            .tr18{height: 46px;}
            .tr19{height: 45px;}

            .t0{width: 445px;margin-left: 164px;margin-top: 6px;font: 13px 'Arial';}
            .t1{width: 455px;font: 11px 'Arial';}
            .t2{width: 262px;font: 12px 'Arial';}
            .t3{width: 801px;font: 12px 'Arial';}
            .t4{width: 801px;margin-top: 213px;font: 9px 'Arial';}

        </STYLE>
    </HEAD>

    <BODY onload="convertNumberToWords()">
        <DIV id="page_1">
            <DIV id="p1dimg1">
                <img src="/throttle/images/Throttle_Logo.png" alt="Throttle" id="p1img1" style="height:40px;width:150px;margin-left:-01px;"/></DIV>
            <DIV id="tx1"><SPAN class="ft0">E. & O.E</SPAN></DIV>
            <DIV id="tx2"><SPAN class="ft1">THIS IS A COMPUTER GENERATED DOCUMENT AND DOES NOT REQUIRE A SIGNATURE.</SPAN></DIV>
            <DIV id="tx3"><SPAN class="ft1">Service Tax Reg.No : AABCC1756DSD030 PAN No: AABCC1756D TAN No: CHEI06961D</SPAN></DIV>
            <DIV id="tx4"><SPAN class="ft1">GST No : 33AABCC1756D1Z7</SPAN></DIV>
            <DIV id="tx5"><SPAN class="ft1">SERVICE TAX/GST LIABILITY ON TRANSPORT CHARGES IS CONSIGNEE'S RESPONSIBILITY.</SPAN></DIV>
            <DIV id="tx6"><SPAN class="ft1">Ref: R00018 on </SPAN><NOBR><SPAN class="ft1">24-AUG-2018:07:38:AM</SPAN></NOBR><SPAN class="ft1"> by </SPAN><NOBR><SPAN class="ft1">RAJAN-4PL</SPAN></NOBR><SPAN class="ft1"> CHENNAI</SPAN></DIV>

            <DIV id="id1_1">
                <P class="p0 ft2">Kerry Indev Logistics Pvt. Ltd</P>
                <P class="p1 ft3">(Formerly known as Indev Logistics Pvt. Ltd.)</P>
                <P class="p2 ft4">A11 & A12 and B7 & B8, Sipcot Industrial Park, Irungattukottai, Sriperumbudur, 602105. CIN NO:U63012TN1997PTC037389,</P>
                <P class="p3 ft3">Regd Off: New NO. 81, Old No. 41, Swamy Complex , Thambu Chetty Street, Chennai - 600001 GST No:33AABCC1756D1Z7.</P>
                <TABLE cellpadding=0 cellspacing=0 class="t0">
                    <TR>
                        <TD class="tr0 td0"><P class="p4 ft5">TAX INVOICE</P></TD>
                        <TD class="tr0 td1"><P class="p4 ft6">ORIGINAL FOR RECIPIENT</P></TD>
                    </TR>
                </TABLE>
            </DIV>
            <DIV id="id1_2">
                <DIV id="id1_2_1">
                    <TABLE cellpadding=0 cellspacing=0 class="t1">
                        <TR>
                            <TD class="tr1 td2"><P class="p4 ft7">Customer</P></TD>
                            <TD class="tr1 td3"><P class="p4 ft8"><SPAN class="ft7">: </SPAN> <c:out value="${custName}" /></P></TD>
                            <TD class="tr2 td4"><P class="p4 ft9">&nbsp;</P></TD>
                        </TR>
                        <TR>
                            <TD class="tr2 td2"><P class="p4 ft9">&nbsp;</P></TD>
                            <TD colspan=2 class="tr2 td5"><P class="p5 ft10"> <c:out value="${customerAddress}" /></P></TD>
                        </TR>
                 
                        <TR>
                            <TD class="tr2 td2"><P class="p4 ft9">&nbsp;</P></TD>
                            <TD colspan=2 class="tr2 td5"><P class="p5 ft10"> <c:out value="${billingState}" /></P></TD>
                        </TR>
                        <TR>
                            <TD class="tr1 td2"><P class="p4 ft11">&nbsp;</P></TD>
                            <TD colspan=2 class="tr1 td5"><P class="p5 ft8">GST ID : <c:out value="${gstNo}" /> State Code : <c:out value="${custStateCode}" /></P></TD>
                        </TR>
                    </TABLE>
                </DIV>
                <DIV id="id1_2_2">
                    <TABLE cellpadding=0 cellspacing=0 class="t2">
                        <TR>
                            <TD class="tr3 td2"><P class="p4 ft12">Customer Code</P></TD>
                            <TD class="tr3 td6"><P class="p6 ft12">: <NOBR>C027559</NOBR></P></TD>
                        </TR>
                        <TR>
                            <TD class="tr4 td2"><P class="p7 ft12">Customer PAN No</P></TD>
                            <TD class="tr4 td6"><P class="p6 ft12">:  <c:out value="${panNo}" /></P></TD>
                        </TR>
                        <TR>
                            <TD class="tr5 td2"><P class="p8 ft12">Invoice Number</P></TD>
                            <TD class="tr5 td6"><P class="p6 ft12">: <NOBR> <c:out value="${invoiceNo}" /></NOBR></P></TD>
                        </TR>
                        <TR>
                            <TD class="tr3 td2"><P class="p7 ft12">Date</P></TD>
                            <TD class="tr3 td6"><P class="p6 ft12">: <NOBR> <c:out value="${billDate}" /></NOBR></P></TD>
                        </TR>
                        <TR>
                            <TD class="tr5 td2"><P class="p7 ft12">Payment Due Date</P></TD>
                            <TD class="tr5 td6"><P class="p6 ft12">: <NOBR><c:out value="${billDate}" /></NOBR></P></TD>
                        </TR>
                        <TR>
                            <TD class="tr5 td2"><P class="p7 ft12">Job Number</P></TD>
                            <TD class="tr5 td6"><P class="p9 ft8">: <NOBR>-</NOBR></P></TD>
                        </TR>
                        <TR>
                            <TD class="tr3 td2"><P class="p7 ft12">Job Date</P></TD>
                            <TD class="tr3 td6"><P class="p9 ft12">: <NOBR><c:out value="${billDate}" /></NOBR></P></TD>
                        </TR>
                        <TR>
                            <TD class="tr5 td2"><P class="p7 ft12">Master Number</P></TD>
                            <TD class="tr5 td6"><P class="p9 ft12">:</P></TD>
                        </TR>
                        <TR>
                            <TD class="tr3 td2"><P class="p7 ft12">House Number</P></TD>
                            <TD class="tr3 td6"><P class="p9 ft12">:</P></TD>
                        </TR>
                        <TR>
                            <TD class="tr4 td2"><P class="p8 ft12">Reverse Charge</P></TD>
                            <TD class="tr4 td6"><P class="p9 ft12">: No</P></TD>
                        </TR>
                        <TR>
                            <TD class="tr5 td2"><P class="p8 ft12">Advance Receipt No</P></TD>
                            <TD class="tr5 td6"><P class="p9 ft12">:</P></TD>
                        </TR>
                    </TABLE>
                </DIV>
            </DIV><br>
            <DIV id="id1_2_2">
                <TABLE cellpadding=0 cellspacing=0 class="t3">
                    <TR>
                        <TD colspan=2 class="tr4 td7"><P class="p4 ft12">Port of Origin</P></TD>
                        <TD class="tr4 td8"><P class="p10 ft12">:<c:out value="${startPoint}" /></P></TD>
                        <TD class="tr4 td9"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td10"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td11"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td12"><P class="p6 ft12">ETD</P></TD>
                        <TD class="tr4 td2"><P class="p11 ft12">:<c:out value="${tripStartDate}" /></P></TD>
                        <TD colspan=2 class="tr4 td13"><P class="p12 ft12">Number of Packs</P></TD>
                        <TD class="tr4 td14"><P class="p12 ft12">:</P></TD>
                        <TD class="tr4 td15"><P class="p4 ft11">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD colspan=2 class="tr4 td7"><P class="p4 ft12">Final Destination</P></TD>
                        <TD class="tr4 td8"><P class="p10 ft12">:<c:out value="${endPoint}" /></P></TD>
                        <TD class="tr4 td9"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td10"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td11"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td12"><P class="p6 ft12">ETA</P></TD>
                        <TD class="tr4 td2"><P class="p11 ft12">:<c:out value="${tripEndDate}" /></P></TD>
                        <TD class="tr4 td16"><P class="p12 ft12">Weight (Kgs.)</P></TD>
                        <TD class="tr4 td17"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td14"><P class="p12 ft12">:</P></TD>
                        <TD class="tr4 td15"><P class="p4 ft11">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr4 td18"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td19"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td8"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td9"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td10"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD colspan=3 rowspan=2 class="tr6 td20">Place of Supply : <SPAN class="ft8"><c:out value="${placeofSupply}" /></SPAN></TD>
                        <TD class="tr4 td16"><P class="p14 ft8">Volume (CBM)</P></TD>
                        <TD class="tr4 td17"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td14"><P class="p12 ft12">:</P></TD>
                        <TD class="tr4 td15"><P class="p4 ft11">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr4 td18"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td19"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td8"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td9"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td10"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td16"><P class="p12 ft12">IGM Number</P></TD>
                        <TD class="tr4 td17"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td14"><P class="p12 ft12">: /</P></TD>
                        <TD class="tr4 td15"><P class="p4 ft11">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD colspan=2 class="tr7 td7"><P class="p4 ft12">Shipper Ref. No</P></TD>
                        <TD class="tr7 td8"><P class="p10 ft12">:</P></TD>
                        <TD class="tr7 td9"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr7 td10"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr7 td11"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr7 td12"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr7 td2"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr7 td16"><P class="p12 ft12">Item Number</P></TD>
                        <TD class="tr7 td17"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr7 td14"><P class="p12 ft12">:</P></TD>
                        <TD class="tr7 td15"><P class="p4 ft11">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr3 td18"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td19"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td8"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td9"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td10"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td11"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td12"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td2"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD colspan=2 class="tr3 td13"><P class="p12 ft12">Sub Item Number</P></TD>
                        <TD class="tr3 td14"><P class="p12 ft12">:</P></TD>
                        <TD class="tr3 td15"><P class="p4 ft11">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr8 td18"><P class="p4 ft12">Note</P></TD>
                        <TD colspan=8 class="tr8 td21"><P class="p15 ft8">: INVOICE TOWARDS TRANSPORTATION CHARGES FOR THE MONTH OF <c:out value="${tripMonth}" /> <c:out value="${tripYear}" /> </P></TD>
                        <TD class="tr8 td17"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr8 td14"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr8 td15"><P class="p4 ft11">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr9 td22"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr9 td23"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr9 td24"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr9 td25"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr9 td26"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr9 td27"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr9 td28"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr9 td29"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr9 td30"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr9 td31"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr9 td32"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr9 td33"><P class="p4 ft11">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr5 td34"><P class="p5 ft8">No</P></TD>
                        <TD class="tr5 td19"><P class="p7 ft8">Charge Details</P></TD>
                        <TD class="tr5 td8"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr5 td9"><P class="p16 ft8">HSN/SAC</P></TD>
                        <TD class="tr5 td10"><P class="p4 ft8">Curr.</P></TD>
                        <TD class="tr5 td11"><P class="p17 ft8">Rate/Unit</P></TD>
                        <TD class="tr5 td12"><P class="p18 ft8">Unit</P></TD>
                        <TD class="tr5 td2"><P class="p19 ft8">Curr. Amt</P></TD>
                        <TD class="tr5 td16"><P class="p4 ft8">ROE Taxable Amt</P></TD>
                        <TD class="tr5 td17"><P class="p4 ft8">Rate(%)</P></TD>
                        <TD class="tr5 td14"><P class="p20 ft8">Tax </P></TD>
                        <TD class="tr5 td35"><P class="p21 ft8">Amt in INR</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr10 td34"><P class="p4 ft13">&nbsp;</P></TD>
                        <TD  class="tr10 td19"><P class="p4 ft13">&nbsp;</P></TD>
                        <TD  class="tr10 td8"><P class="p4 ft13">&nbsp;</P></TD>
                        <TD class="tr10 td9"><P class="p4 ft13">&nbsp;</P></TD>
                        <TD class="tr10 td10"><P class="p4 ft13">&nbsp;</P></TD>
                        <TD class="tr10 td11"><P class="p4 ft13">&nbsp;</P></TD>
                        <TD class="tr10 td11"><P class="p4 ft13">&nbsp;</P></TD>
                        <TD  class="tr10 td37"><P class="p4 ft13">&nbsp;</P></TD>
                        <TD class="tr10 td16"><P class="p4 ft13">&nbsp;</P></TD>
                        <TD class="tr10 td17"><P class="p4 ft13">&nbsp;</P></TD>
                        <TD class="tr10 td14"><P class="p4 ft13">&nbsp;</P></TD>
                        <TD class="tr10 td35"><P class="p4 ft13">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr11 td38"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td23"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td24"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td25"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td26"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td27"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td28"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td29"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td30"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td31"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td32"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td39"><P class="p4 ft14">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr1 td18"><P class="p22 ft8">1</P></TD>
                        <TD colspan=2 class="tr1 td36"><P class="p23 ft1">TRANSPORT CHARGES</P></TD>
                        <TD class="tr1 td9"><P class="p24 ft0">996511</P></TD>
                        <TD class="tr1 td10"><P class="p25 ft0">INR</P></TD>
                        <TD class="tr1 td11"><P class="p26 ft0"><c:out value="${grandTotal}" /></P></TD>
                        <TD class="tr1 td11"><P class="p26 ft0">1.000</P></TD>
                        <TD class="tr1 td37"><P class="p27 ft0"><c:out value="${grandTotal}" /></P></TD>
                        <TD class="tr1 td16"><P class="p28 ft0"><c:out value="${grandTotal}" /></P></TD>
                        <TD class="tr1 td17"><P class="p29 ft0"><c:out value="${cgstPercentage+sgstPercentage+igstPercentage}" /></P></TD>
                        <TD class="tr1 td14"><P class="p9 ft0"><c:out value="${cgstValue+sgstValue+igstValue}" /></P></TD>
                        <TD class="tr1 td15"><P class="p29 ft0"><c:out value="${grandTotal+cgstValue+sgstValue+igstValue}" /></P></TD>
                    </TR>
                </TABLE>
                <TABLE cellpadding=0 cellspacing=0 class="t4">
                    <TR>
                        <TD class="tr12 td40"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr12 td41"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr12 td42"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr12 td43"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr12 td44"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr12 td45"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr12 td46"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr12 td47"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr12 td48"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr12 td49"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr12 td46"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD colspan=2 class="tr12 td50"><P class="p30 ft8">Total in INR :</P></TD>
                        <TD colspan=3 class="tr12 td51"><P class="p26 ft0"><c:out value="${grandTotal}" /></P></TD>
                        <TD class="tr12 td52"><P class="p31 ft0"><c:out value="${cgstValue+sgstValue+igstValue}" /></P></TD>
                        <TD class="tr12 td53"><P class="p32 ft0"><c:out value="${grandTotal+cgstValue+sgstValue+igstValue}" /></P></TD>
                    </TR>
                    <TR>
                        <TD class="tr11 td54"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD colspan=2 class="tr11 td55"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD colspan=2 class="tr11 td56"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td57"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td58"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td59"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td60"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD colspan=2 class="tr11 td61"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td60"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td62"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td23"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td63"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td64"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td65"><P class="p4 ft14">&nbsp;</P></TD>
                        <TD class="tr11 td66"><P class="p4 ft14">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr1 td67"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD colspan=11 class="tr1 td68"><P class="p8 ft8"><span id="amountInWord"></span> Only</P></TD>
                    <TD class="tr1 td69"><P class="p4 ft11">&nbsp;</P></TD>
                    <TD class="tr1 td19"><P class="p4 ft11">&nbsp;</P></TD>
                    <TD class="tr1 td70"><P class="p4 ft11">&nbsp;</P></TD>
                    <TD class="tr1 td71"><P class="p4 ft11">&nbsp;</P></TD>
                    <TD class="tr1 td72"><P class="p4 ft11">&nbsp;</P></TD>
                    <TD class="tr1 td73"><P class="p4 ft11">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr13 td67"><P class="p4 ft15">&nbsp;</P></TD>
                        <TD colspan=2 class="tr13 td74"><P class="p4 ft15">&nbsp;</P></TD>
                        <TD class="tr13 td75"><P class="p4 ft15">&nbsp;</P></TD>
                        <TD class="tr13 td76"><P class="p4 ft15">&nbsp;</P></TD>
                        <TD class="tr13 td77"><P class="p4 ft15">&nbsp;</P></TD>
                        <TD class="tr13 td78"><P class="p4 ft15">&nbsp;</P></TD>
                        <TD class="tr13 td79"><P class="p4 ft15">&nbsp;</P></TD>
                        <TD class="tr13 td80"><P class="p4 ft15">&nbsp;</P></TD>
                        <TD class="tr13 td81"><P class="p4 ft15">&nbsp;</P></TD>
                        <TD class="tr13 td78"><P class="p4 ft15">&nbsp;</P></TD>
                        <TD class="tr13 td80"><P class="p4 ft15">&nbsp;</P></TD>
                        <TD class="tr13 td69"><P class="p4 ft15">&nbsp;</P></TD>
                        <TD class="tr13 td19"><P class="p4 ft15">&nbsp;</P></TD>
                        <TD class="tr13 td70"><P class="p4 ft15">&nbsp;</P></TD>
                        <TD class="tr13 td71"><P class="p4 ft15">&nbsp;</P></TD>
                        <TD class="tr13 td72"><P class="p4 ft15">&nbsp;</P></TD>
                        <TD class="tr13 td73"><P class="p4 ft15">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr4 td82"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD colspan=2 class="tr3 td83"><P class="p33 ft0">HSN/SAC</P></TD>
                        <TD class="tr3 td84"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td85"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td86"><P class="p8 ft0">Taxable Amount</P></TD>
                        <TD class="tr3 td87"><P class="p7 ft0">Rate</P></TD>
                        <TD class="tr3 td88"><P class="p34 ft0">CGST</P></TD>
                        <TD class="tr3 td89"><P class="p4 ft0">Rate</P></TD>
                        <TD class="tr3 td90"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td87"><P class="p4 ft0">SGST</P></TD>
                        <TD class="tr3 td89"><P class="p4 ft0">Rate</P></TD>
                        <TD class="tr3 td91"><P class="p35 ft0">IGST</P></TD>
                        <TD colspan=2 class="tr3 td92"><P class="p36 ft0">Total Amount</P></TD>
                        <TD class="tr4 td71"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td72"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr4 td73"><P class="p4 ft11">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr2 td82"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr14 td56"><P class="p7 ft0">996511</P></TD>
                        <TD class="tr14 td93"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr14 td94"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr14 td95"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr14 td96"><P class="p37 ft0"><c:out value="${grandTotal}" /></P></TD>
                        <TD class="tr14 td97"><P class="p4 ft0"><c:out value="${cgstPercentage}" />%</P></TD>
                        <TD class="tr14 td98"><P class="p4 ft0"><c:out value="${cgstValue}" /></P></TD>
                        <TD class="tr14 td99"><P class="p4 ft0"><c:out value="${sgstPercentage}" />%</P></TD>
                        <TD class="tr14 td100"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr14 td97"><P class="p4 ft0"><c:out value="${sgstValue}" /></P></TD>
                        <TD class="tr14 td99"><P class="p37 ft0"><c:out value="${igstPercentage}" />%</P></TD>
                        <TD class="tr14 td101"><P class="p37 ft0"><c:out value="${igstValue}" /></P></TD>
                        <TD colspan=2 class="tr14 td102"><P class="p38 ft0"><c:out value="${grandTotal+igstValue+sgstValue+cgstValue}" /></P></TD>
                        <TD class="tr2 td71"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr2 td72"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr2 td73"><P class="p4 ft11">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr3 td82"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr12 td56"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD colspan=2 class="tr12 td103"><P class="p33 ft0">Net Amount <SPAN class="ft8">:</SPAN></P></TD>
                        <TD colspan=2 class="tr12 td104"><P class="p37 ft0"><c:out value="${grandTotal}" /></P></TD>
                        <TD class="tr12 td58"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr12 td98"><P class="p4 ft0"><c:out value="${cgstValue}" /></P></TD>
                        <TD class="tr12 td60"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr12 td100"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr12 td97"><P class="p4 ft0"><c:out value="${sgstValue}" /></P></TD>
                        <TD class="tr12 td60"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr12 td101"><P class="p37 ft0"><c:out value="${igstValue}" /></P></TD>
                        <TD colspan=2 class="tr12 td102"><P class="p38 ft0"><c:out value="${grandTotal+igstValue+sgstValue+cgstValue}" /></P></TD>
                        <TD class="tr3 td71"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td72"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td73"><P class="p4 ft11">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD colspan=2 class="tr9 td105"><P class="p7 ft16">Terms:</P></TD>
                        <TD colspan=15 class="tr9 td106"><P class="p4 ft1">1. Cheques/DD should be made out to Kerry Indev Logistics Pvt Ltd. & crossed A/C payee. The company is not responsible for any cash settlement without an official receipt.</P></TD>
                        <TD class="tr9 td73"><P class="p4 ft11">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr15 td67"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td107"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD colspan=16 class="tr15 td108"><P class="p8 ft18">2. Any discrepancy should be notified to us in writing within 7 days from the invoice date, Otherwise it will be presumed that the amount reflected on the bill is correct and have been verified at your end.</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr16 td67"><P class="p4 ft19">&nbsp;</P></TD>
                        <TD class="tr16 td107"><P class="p4 ft19">&nbsp;</P></TD>
                        <TD colspan=15 class="tr16 td106"><P class="p4 ft20">3. Payment must be received within the agreed credit period, failing which interest @18% per annum will be charged on overdue invoices All Objections/Claims are subject to Chennai Juridiction.</P></TD>
                        <TD class="tr16 td73"><P class="p4 ft19">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr15 td67"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td107"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD colspan=10 class="tr15 td109"><P class="p4 ft18">4.Cargo will be delivered subject to receipt and realisation of cheque for Freight and other Charges.</P></TD>
                        <TD class="tr15 td69"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td19"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td70"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td71"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td72"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td73"><P class="p4 ft17">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr1 td67"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD colspan=2 class="tr1 td110"><P class="p4 ft0">Bank Details</P></TD>
                        <TD colspan=4 class="tr2 td111"><P class="p8 ft1">A/C Name : Kerry Indev Logistics Pvt. Ltd</P></TD>
                        <TD class="tr2 td47"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr2 td48"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr2 td49"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr2 td46"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr2 td48"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr2 td112"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr2 td113"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr1 td70"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr1 td71"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr1 td72"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr1 td73"><P class="p4 ft11">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr3 td67"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td107"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td114"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD colspan=3 class="tr3 td115"><P class="p8 ft1">Bank Name: AXIS BANK LTD</P></TD>
                        <TD class="tr3 td78"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td79"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td80"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td81"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td78"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td80"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td69"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr3 td19"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD colspan=3 class="tr3 td116"><P class="p39 ft0">For Kerry Indev Logistics Pvt. Ltd</P></TD>
                        <TD class="tr3 td73"><P class="p4 ft11">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr15 td67"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td107"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td114"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td75"><P class="p40 ft18">A/C No</P></TD>
                        <TD colspan=3 class="tr15 td117"><P class="p4 ft18">: 912030034817424 (INR)</P></TD>
                        <TD class="tr15 td79"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td80"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td81"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td78"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td80"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td69"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td19"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td70"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td71"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td72"><P class="p4 ft17">&nbsp;</P></TD>
                        <TD class="tr15 td73"><P class="p4 ft17">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr16 td67"><P class="p4 ft19">&nbsp;</P></TD>
                        <TD class="tr16 td107"><P class="p4 ft19">&nbsp;</P></TD>
                        <TD class="tr16 td114"><P class="p4 ft19">&nbsp;</P></TD>
                        <TD colspan=13 class="tr16 td118"><P class="p8 ft20">Address : CORPORATE BANKING, CHENNAI [TN], KARUMUTHU NILAYAM, GROUND FLOOR NO. 192, ANNA SALAI, CHENNAI 600002</P></TD>
                        <TD class="tr16 td72"><P class="p4 ft19">&nbsp;</P></TD>
                        <TD class="tr16 td73"><P class="p4 ft19">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr16 td67"><P class="p4 ft19">&nbsp;</P></TD>
                        <TD class="tr16 td107"><P class="p4 ft19">&nbsp;</P></TD>
                        <TD class="tr16 td114"><P class="p4 ft19">&nbsp;</P></TD>
                        <TD colspan=7 class="tr16 td119"><P class="p8 ft20">A/C No : 915020009574499 (USD) ; A/C No : 915020009572943 (EUR)</P></TD>
                        <TD class="tr16 td78"><P class="p4 ft19">&nbsp;</P></TD>
                        <TD class="tr16 td80"><P class="p4 ft19">&nbsp;</P></TD>
                        <TD class="tr16 td69"><P class="p4 ft19">&nbsp;</P></TD>
                        <TD class="tr16 td19"><P class="p4 ft19">&nbsp;</P></TD>
                        <TD class="tr16 td70"><P class="p4 ft19">&nbsp;</P></TD>
                        <TD class="tr16 td71"><P class="p4 ft19">&nbsp;</P></TD>
                        <TD class="tr16 td72"><P class="p4 ft19">&nbsp;</P></TD>
                        <TD class="tr16 td73"><P class="p4 ft19">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr17 td67"><P class="p4 ft21">&nbsp;</P></TD>
                        <TD class="tr17 td107"><P class="p4 ft21">&nbsp;</P></TD>
                        <TD class="tr17 td114"><P class="p4 ft21">&nbsp;</P></TD>
                        <TD colspan=7 class="tr17 td119"><P class="p8 ft1">SWIFT - AXISINBBA01; IFS - UTIB0001165; MICR - 600211036</P></TD>
                        <TD class="tr17 td78"><P class="p4 ft21">&nbsp;</P></TD>
                        <TD class="tr17 td80"><P class="p4 ft21">&nbsp;</P></TD>
                        <TD class="tr17 td69"><P class="p4 ft21">&nbsp;</P></TD>
                        <TD class="tr17 td19"><P class="p4 ft21">&nbsp;</P></TD>
                        <TD class="tr17 td70"><P class="p4 ft21">&nbsp;</P></TD>
                        <TD class="tr17 td71"><P class="p4 ft21">&nbsp;</P></TD>
                        <TD class="tr17 td72"><P class="p4 ft21">&nbsp;</P></TD>
                        <TD class="tr17 td73"><P class="p4 ft21">&nbsp;</P></TD>
                    </TR>
                    <TR>
                        <TD class="tr18 td67"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr18 td107"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr18 td114"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr19 td94"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr19 td120"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr19 td57"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr19 td58"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr19 td59"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr19 td60"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr19 td100"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr19 td58"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr19 td60"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr19 td62"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr19 td23"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr18 td70"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr18 td71"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr18 td72"><P class="p4 ft11">&nbsp;</P></TD>
                        <TD class="tr18 td73"><P class="p4 ft11">&nbsp;</P></TD>
                    </TR>
                </TABLE>
            </DIV>           
                    <center>                    
                    <input type="button" class="button" id="printbtn" value="Print" onClick="callprint();" />
                    <input type="hidden" id="grandTotalR" name="grandTotalR" value="<c:out value="${grandTotal+igstValue+sgstValue+cgstValue}" />" />
                    </center>
        </DIV>
<script>

                        function convertNumberToWords() {
                            var amount = document.getElementById("grandTotalR").value;
                            var words = new Array();
                            words[0] = '';
                            words[1] = 'One';
                            words[2] = 'Two';
                            words[3] = 'Three';
                            words[4] = 'Four';
                            words[5] = 'Five';
                            words[6] = 'Six';
                            words[7] = 'Seven';
                            words[8] = 'Eight';
                            words[9] = 'Nine';
                            words[10] = 'Ten';
                            words[11] = 'Eleven';
                            words[12] = 'Twelve';
                            words[13] = 'Thirteen';
                            words[14] = 'Fourteen';
                            words[15] = 'Fifteen';
                            words[16] = 'Sixteen';
                            words[17] = 'Seventeen';
                            words[18] = 'Eighteen';
                            words[19] = 'Nineteen';
                            words[20] = 'Twenty';
                            words[30] = 'Thirty';
                            words[40] = 'Forty';
                            words[50] = 'Fifty';
                            words[60] = 'Sixty';
                            words[70] = 'Seventy';
                            words[80] = 'Eighty';
                            words[90] = 'Ninety';
                            amount = amount.toString();
                            var atemp = amount.split(".");
                            var number = atemp[0].split(",").join("");
                            var n_length = number.length;
                            var words_string = "";
                            if (n_length <= 9) {
                                var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
                                var received_n_array = new Array();
                                for (var i = 0; i < n_length; i++) {
                                    received_n_array[i] = number.substr(i, 1);
                                }
                                for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
                                    n_array[i] = received_n_array[j];
                                }
                                for (var i = 0, j = 1; i < 9; i++, j++) {
                                    if (i == 0 || i == 2 || i == 4 || i == 7) {
                                        if (n_array[i] == 1) {
                                            n_array[j] = 10 + parseInt(n_array[j]);
                                            n_array[i] = 0;
                                        }
                                    }
                                }
                                value = "";
                                for (var i = 0; i < 9; i++) {
                                    if (i == 0 || i == 2 || i == 4 || i == 7) {
                                        value = n_array[i] * 10;
                                    } else {
                                        value = n_array[i];
                                    }
                                    if (value != 0) {
                                        words_string += words[value] + " ";
                                    }
                                    if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                                        words_string += "Crores ";
                                    }
                                    if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                                        words_string += "Lakhs ";
                                    }
                                    if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                                        words_string += "Thousand ";
                                    }
                                    if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                                        words_string += "Hundred and ";
                                    } else if (i == 6 && value != 0) {
                                        words_string += "Hundred ";
                                    }
                                }
                                words_string = words_string.split("  ").join(" ");
//                                        alert(words_string);
                                $("#amountInWord").text(words_string);                            
                            }
                            return words_string;
                        }
                    </script> 
    </BODY>
</HTML>

