<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">

    function viewTripDetails(tripId) {
            window.open('/throttle/viewTripSheetDetails.do?tripId='+tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
    function viewInvoiceDetails(invId) {
            window.open('/throttle/showinvoicedetail.do?invoiceId='+invId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
    function viewConsignmentDetails(orderId) {
            window.open('/throttle/getConsignmentDetails.do?consignmentOrderId='+orderId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

</head>


<script type="text/javascript">
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('~');
                $('#customerId').val(tmp[0]);
                $('#customerName').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#tripCode').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getTripCode.do",
                    dataType: "json",
                    data: {
                        tripCode: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#tripCode").val(ui.item.Name);
                var value = ui.item.Name;
                var tmp = value.split(',');
                $('#tripId').val(tmp[0]);
                $('#tripCode').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split(',');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>

<script>


    function submitPageForTripGeneration() {
        var temp = "";
        var selectedConsignment = document.getElementsByName('selectedIndex');
        var consignmentOrder = document.getElementsByName('consignmentOrderId');
        //alert("1:"+selectedConsignment.length);
        var cntr = 0;
        for (var i = 0; i < selectedConsignment.length; i++) {
            //alert(selectedConsignment[i].checked);
            if (selectedConsignment[i].checked == true) {

                if (cntr == 0) {
                    temp = consignmentOrder[i].value;
                } else {
                    temp = temp + "," + consignmentOrder[i].value;
                }
                cntr++;
            }
        }
        //alert(temp);
        //alert(cntr);
        if (cntr > 0) {
            //alert("am here..");
            document.CNoteSearch.action = "/throttle/createTripSheet.do?consignmentOrderNos=" + temp;
            //alert("am here..1:"+document.CNoteSearch.action);
            document.CNoteSearch.submit();
            //alert("am here..2");
        } else {
            alert("Please select consignment order and proceed");
        }
    }


    function searchPage(val) {
        document.CNoteSearch.action = "/throttle/viewconsignmentreport.do?param="+val;
        document.CNoteSearch.submit();
    }
</script>
<div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.ViewConsignmentReport" text="ViewConsignmentReport"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Report" text="Report"/></a></li>
		                    <li class=""><spring:message code="hrms.label.ViewConsignmentReport" text="ViewConsignmentReport"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
<body>
    <%
                String menuPath = "Consignment Note >> View / Edit";
                request.setAttribute("menuPath", menuPath);
    %>
    <form name="CNoteSearch" method="post" enctype="multipart">
         <table class="table table-info mb30 table-hover" id="report" >
		    <thead>
		<tr><th colspan="6" height="30" >CONSIGNMENT ORDERS</th></tr>
    </thead>
		
    <tr>
                                    <td >Customer Name</td>
                                    <td>
                                        <input type="hidden" class="form-control" style="width:250px;height:40px"  name="customerId" id="customerId"  value="<c:out value="${customerId}"/>"/>
                                        <input type="text" class="form-control" style="width:250px;height:40px"  name="customerName"   id="customerName"  value="<c:out value="${customerName}"/>"/>
                                    </td>

                                    <td >Trip Code</td>
                                    <td>
                                        <input type="text" class="form-control" style="width:250px;height:40px"  name="tripCode"  id="tripCode"  value="<c:out value="${tripCode}"/>"/>
                                        <input type="hidden" class="form-control" style="width:250px;height:40px"  name="tripId"  id="tripId"  value=""/>
                                    </td>
    </tr>
    <tr>
                                    <td >Consignment Order No</td>
                                    <td><input type="text" class="form-control" style="width:250px;height:40px"  name="consignmentOrderReferenceNo"  id="consignmentOrderReferenceNo"  value="<c:out value="${consignmentOrderReferenceNo}"/>"/></td>


                                
                                    <td>Status</td>
                                    <td > 
                                        <select name="status"  class="form-control" style="width:250px;height:40px"   >
                                           <c:if test="${statusDetails != null}">
                                                <option value="" selected>--Select--</option>
                                                <c:forEach items="${statusDetails}" var="statusDetails">
                                                    <option value='<c:out value="${statusDetails.status}"/>'><c:out value="${statusDetails.statusName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select> 

                                        <script>
                                            document.getElementById("status").value = '<c:out value="${status}"/>';
                                        </script>
                                   </td>
    </tr>
    <tr>
                                    <td><font color="red">*</font>From Date</td>
                                    <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:250px;height:40px"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                                    <td><font color="red">*</font>To Date</td>
                                    <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:250px;height:40px" onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>
                                 <tr>             
                                     <td></td>
                                    <td>
                                    <input  class="btn btn-success"   value="ExportExcel" onclick="searchPage(this.value)">
                                    </td>
                                    <td  colspan="2"><input  class="btn btn-success"   value="Search" onclick="searchPage(this.value)">
                                    </td>
                                
                                </tr>
        </table>
        </div></div>
        </td>
        </tr>
        </table>
        
        <c:if test = "${consignmentList != null}" >
                 <table class="table table-info mb30 table-hover" id="table" >
                <thead>
                    <tr height="40">
                        <th>Sno</th>
                        <th>Company</th>
                        <th>Consignment No</th>
                        <th>Vehicle Required Date </th>
                        <th>Customer Name </th>
                        <th>Origin </th>
                        <th>Destination </th>
                        <th>Trip Code</th>
                        <th>Trip Actual Start Date</th>
                        <th>Trip Actual End Date</th>
                        <th>Status </th>
                        <th>Created By</th>
                        <th>Created Date</th>
                    </tr>
                </thead>
                <% int index = 0;
                            int sno = 1;
                %>
                <tbody>
                    <c:forEach items="${consignmentList}" var="cnl">

                        <tr height="30">
                            <td align="left" ><%=sno%></td>
                            <td align="left" ><c:out value="${cnl.companyId}"/></td>
                            <td align="left" ><input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=index%>" value="<c:out value="${cnl.consignmentOrderId}"/>"/>

                                <a href="#" onclick="viewConsignmentDetails('<c:out value="${cnl.consignmentOrderId}"/>');"><c:out value="${cnl.consignmentNoteNo}"/></a>
                            </td>
                            <td align="left" ><c:out value="${cnl.tripScheduleDate}"/>&nbsp;<c:out value="${cnl.tripScheduleTime}"/></td>
                            <td align="left" ><c:out value="${cnl.customerName}"/></td>
                            <td align="left" ><c:out value="${cnl.consigmentOrigin}"/></td>
                            <td align="left" ><c:out value="${cnl.consigmentDestination}"/></td>
                               <td align="left" >
                            <c:if test = "${cnl.tripCode == null}" >
                            -
                            </c:if>
                            <c:if test = "${cnl.tripCode != null}" >
                                <c:out value="${cnl.tripCode}"/>
                            </c:if>
                             </td>
                            <td align="left" ><c:out value="${cnl.tripStartDate}"/></td>
                            <td align="left" ><c:out value="${cnl.tripEndDate}"/></td>
                         
                            <td align="left" ><c:out value="${cnl.statusName}"/></td>
                             <td>
                                 <c:out value="${cnl.userName}"/>
                             </td>
                             <td>
                                 <c:out value="${cnl.createdOn}"/>
                             </td>
                        </tr>
                        <%index++;%>
                        <%sno++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
<!--        <center>
            <input type="button" class="button" name="trip"  value="Trip Planning" onclick="submitPageForTripGeneration()"/>
        </center>-->
    </form>
</body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>
