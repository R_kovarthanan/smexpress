<html>
    <!DOCTYPE html>
    <head>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>POD Download</title>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@page import="java.io.FileNotFoundException"%>
        <%@page import="java.io.BufferedInputStream"%>
        <%@page import="java.io.BufferedOutputStream"%>
        <%@page import="java.util.ArrayList"%>
        <%@page import="java.util.List"%>
        <%@page import="java.util.zip.ZipEntry"%>
        <%@page import="java.util.zip.ZipOutputStream"%>
        <%@page import="java.io.FileInputStream"%>
        <%@page import="java.io.File"%>
        <%@page import="java.io.IOException"%>
        <%@page import="java.io.IOException"%>
        <%@page import="java.util.Map"%>

    </head>
    <body>

        <%

            // Set the content type based to zip
            response.setContentType("Content-type: text/zip");
            response.setHeader("Content-Disposition", "attachment; filename=POD.zip");
            List<File> tempList = new ArrayList<File>();

        %>

        <c:if test="${viewPODDetails != null}">
            <c:forEach items="${viewPODDetails}" var="viewPODDetails">
                <c:set var="podFile" value="${viewPODDetails.podFile}" />
                <%                String podStr = "" + (String) pageContext.getAttribute("podFile");
                    String actualServerFilePath = getServletContext().getRealPath("/uploadFiles/Files/");
                    String tempServerFilePath = actualServerFilePath.replace("\\", "//");
                    File dir1 = new File(tempServerFilePath + "/" + podStr);
                    //System.out.println("Server Path After Replace== " + tempServerFilePath + "//" + podStr);
                    tempList.add(dir1);

                %>    


            </c:forEach>
        </c:if>

        <%        try {

                ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(response.getOutputStream()));

                if (tempList != null) {
                    for (File file : tempList) {
                        //System.out.println("Adding " + file.getName());
                        zos.putNextEntry(new ZipEntry(file.getName()));

                        // Get the file
                        FileInputStream fis = null;
                        try {
                            fis = new FileInputStream(file);

                        } catch (FileNotFoundException fnfe) {
                            // If the file does not exists, write an error entry instead of
                            // file
                            // contents
                            zos.write(("ERRORld not find file " + file.getName()).getBytes());
                            zos.closeEntry();                            
                            continue;
                        }

                        BufferedInputStream fif = new BufferedInputStream(fis);

                        // Write the contents of the file
                        int data = 0;
                        while ((data = fif.read()) != -1) {
                            zos.write(data);
                        }

                        fif.close();
                        zos.closeEntry();
                    }
                }                
            zos.close();
            } catch (Exception fnfe) {

            }
        %>

    </body>
</html>
