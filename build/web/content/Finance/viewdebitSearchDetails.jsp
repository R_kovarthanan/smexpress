<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

         $(document).ready(function(){
             $("#showTable").hide();

             });
        </script>
        <script type="text/javascript">
            function submitPage(value) {
                
               $("#showTable").show();  
            }
            
           
        </script>


    </head>
    <body onload="setValue();sorter.size(10);">
        <form name="accountReceivable" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Payment Paid Report</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td><font color="red">*</font>Customer Name</td>
                                        <td height="30">
                                            <select class="textbox" name="customerId" id="customerId"  style="width:125px;">
                                                <option value="">---Select---</option>
                                                <option value="">---Select---</option>
                                                <option value="">---Select---</option>
                                                
                                            </select>
                                        </td>
                                        <td><font color="red">*</font>Customer Type</td>
                                        <td height="30">
                                            <select class="textbox" name="customerId" id="customerId"  style="width:125px;">
                                                <option value="">---Select---</option>
                                                <option value="">---Select---</option>
                                                <option value="">---Select---</option>
                                                
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value=""></td>
                                    </tr>
                                    <tr>
                                        <td><input type="button" class="button" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);"></td>
                                        <td><input type="button" class="button" name="Search"   value="Search" onclick="submitPage(this.name);"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>

<br>
            <br>
           
            <br>
            <br>
            <br>
            <div id="showTable">
                <table style="width: 1100px" align="center" border="0" id="table1" class="sortable">
                    <thead>
                        <tr height="45" >
                            <th><h3 align="center">S.No</h3></th>
                            <th><h3 align="center">Customer Name</h3></th>
                            <th><h3 align="center">Invoice No</h3></th>
                            <th><h3 align="center">Invoice Date</h3></th>
                            <th><h3 align="center">Payment Date</h3></th>
                            <th><h3 align="center">Amount</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                            
                            <tr height="30">

                                <td align="center">1</td>
                                <td align="left"></td>
                                <td align="left">INV0123</td>
                                <td align="left">01-02-2014</td>
                                <td align="left">01-02-2014</td>
                                <td align="left">30000</td>
                                
                               </tr>
                            <tr height="30">

                                <td align="center">1</td>
                                <td align="left"></td>
                                <td align="left">INV0123</td>
                                <td align="left">01-02-2014</td>
                                <td align="left">01-02-2014</td>
                                <td align="left">30000</td>
                               </tr>

                    </tbody>
                </table>
           
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
            
            </div>
        </form>
    </body>    
</html>