<%--  
    Document   : receiptEntry
    Created on : Mar 21, 2013, 10:50:00 PM
    Author     : Entitle
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <%@ page import="ets.domain.contract.business.ContractTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            function setValues(){
                var temp = document.getElementById("customerIdTemp").value;
                //alert(temp);
                var tempStr = temp.split("~");
                document.invoice.customerId.value = tempStr[0];
                document.invoice.ledgerId.value = tempStr[1];
                submitPage();
            }

            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>

    </head>
    <script language="javascript">
        
        function submitPage(){
                 if(document.getElementById("customerIdTemp").value == ""){
                    alert('please select customer');
                    document.getElementById("customerIdTemp").focus();
                }
                if(document.getElementById("customerIdTemp").value != ""){
                    
                    var customerName = document.getElementById('customerIdTemp').options[document.getElementById('customerIdTemp').selectedIndex].text;
                    document.invoice.customerName.value = customerName;
                    //alert(document.invoice.customerId.value);
                    document.invoice.action = "/throttle/creditNoteDetails.do";
                    document.invoice.submit();
                }
            }
            
        
    </script>
    <body>
        <form name="invoice" method="post" >
            <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                <tr>
                     <td align="left" width="80">
                        <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp"%>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="800" id="bg" class="border">
                <tr>
                        <td colspan="6" class="contenthead" height="30" align="center">
                        <div class="contenthead" align="center">INVOICE CREDIT</div></td>
                </tr>
                <tr class="text2">
                    <td width="80">&nbsp;</td>
                         <td width="80">Customer</td>
                         <input type="hidden" name="customerId" id="customerId" value="0" />
                         <input type="hidden" name="ledgerId" id="ledgerId" value="0" />
                         <td  width="80"> <select name="customerIdTemp" id="customerIdTemp" onchange="setValues();" class="textbox" style="height:20px; width:122px;" >
                               <c:if test="${customerList != null}">
                                <option value="" selected>--Select--</option>
                                <c:forEach items="${customerList}" var="customerList">
                                    <option value='<c:out value="${customerList.customerId}"/>~<c:out value="${customerList.ledgerId}"/>'><c:out value="${customerList.customerName}"/></option>
                                </c:forEach>
                            </c:if>
                                </select>
                               <input type="hidden" name="customerName" id="customerName" value="" > </td>
                </tr>
            </table>
            <br/>
            <br/>
            <br/>
             <table align="center" width="100%" border="0">
                <tr >
                   <td colspan="2" align="center"> <input type="button" id="Search" value="Search" class="button" onClick="submitPage();">
                </tr>
            </table>

          
    </body>
</html>