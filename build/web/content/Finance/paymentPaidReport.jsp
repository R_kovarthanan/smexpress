<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>


    <script type="text/javascript">
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            // alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script type="text/javascript">
        function setValues() {
            if ('<%=request.getAttribute("vendorId")%>' != 'null') {
                document.getElementById('vendorId').value = '<%=request.getAttribute("vendorId")%>';
            }
            if ('<%=request.getAttribute("fromDate")%>' != 'null') {
                document.getElementById('fromDate').value = '<%=request.getAttribute("fromDate")%>';
            }
            if ('<%=request.getAttribute("toDate")%>' != 'null') {
                document.getElementById('toDate').value = '<%=request.getAttribute("toDate")%>';
            }
        }
    </script>
    <script language="javascript">

        function submitPage() {
            document.invoice.action = '/throttle/handlePaymentPaidReport.do';
            document.invoice.submit();
        }

    </script>

</head>
<body >
    <form name="invoice" action=""  method="post">
        <%@ include file="/content/common/path.jsp" %>
        <%@ include file="/content/common/message.jsp" %>
        <br>
        <br>
        <br>
        <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
            <tr id="exp_table" >
                <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                    <div class="tabs" align="left" style="width:850;">
                        <ul class="tabNavigation">
                            <li style="background:#76b3f1">Payment Pending Report</li>
                        </ul>
                        <div id="first">
                            <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                <tr>
                                    <td width="80">Vendor</td>
                                    <td  width="80"> <select name="vendorId" id="vendorId"  class="textbox" style="height:20px; width:122px;" >
                                            <c:if test="${vendorList != null}">
                                                <option value="" selected>--Select--</option>
                                                <c:forEach items="${vendorList}" var="vendor">
                                                    <option value='<c:out value="${vendor.vendorId}"/>'><c:out value="${vendor.vendorName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                        <script>
                                            document.getElementById('vendorId').value = '<c:out value="${vendorId}"/>';
                                        </script>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="right"><input type="button" class="button" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);"></td>
                                    <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="button" name="Search"   value="Search" onclick="submitPage(this.name);"></td>
                                </tr>
                            </table>
                        </div></div>
                </td>
            </tr>
        </table>
        <br>
        <br>
        <br>
        <c:if test="${invoiceList != null}">
            <table align="center" border="0" id="table" class="sortable" style="width:800px;" >
                <thead>
                    <tr height="50">
                        <th><h3>S.No</h3></th>

                        <th><h3>PO Code</h3></th>
                        <th><h3>Grand Total </h3></th>
                        <th><h3>Paid Amount </h3></th>
                        <th><h3>Pending Amount </h3></th>
                    </tr>
                </thead>
                <tbody>
                    <% int index = 1;%>
                    <c:forEach items="${invoiceList}" var="invoice">
                        <%
                                String className = "text1";
                                if ((index % 2) == 0) {
                                    className = "text1";
                                } else {
                                    className = "text2";
                                }
                        %>
                        <c:if test="${invoice.payAmount > '0' }">
                            <tr height="30">
                                <td class="<%=className%>"   height="30"><%=index%></td>

                                <td class="<%=className%>" ><c:out value="${invoice.invoiceCode}"/></td>
                                <td class="<%=className%>" ><c:out value="${invoice.grandTotal}"/> </td>
                                <td class="<%=className%>" ><c:out value="${invoice.payAmount}"/> </td>
                                <td class="<%=className%>" ><c:out value="${invoice.pendingAmount}"/></td>

                            </tr>
                        </c:if>
                    </c:forEach>
                    <%index++;%>
                </tbody>
            </table>
        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" >5</option>
                    <option value="10">10</option>
                    <option value="20" selected="selected">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 1);
        </script>
        </c:if>
        <c:if test="${invoiceList == null}">
            <center>
                <font color="red">No Records Found</font>
            </center>
        </c:if>
    </form>
</body>    
</html>