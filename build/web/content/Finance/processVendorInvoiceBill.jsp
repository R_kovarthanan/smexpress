<%-- 
    Document   : processVendorInvoiceBill
    Created on : 7 Feb, 2017, 3:28:07 PM
    Author     : pavithra
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {

        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            dateFormat: 'dd-mm-yy',
            changeMonth: true, changeYear: true
        });

    });
</script>

<script type="text/javascript">
    var httpReq;
    function setVendor(vendorTypeid) { //alert(str);
        var temp = "";
        //                                alert(vendorTypeid);
        $.ajax({
            url: "/throttle/vendorPaymentsJson.do",
            dataType: "json",
            data: {
                vendorType: vendorTypeid
            },
            success: function(temp) {
                //                                         alert(temp);
                if (temp != '') {
                    $('#vendorName').empty();
                    $('#vendorName').append(
                            $('<option style="width:150px"></option>').val(0 + "~" + 0).html('---Select----')
                            )
                    $.each(temp, function(i, data) {
                        $('#vendorName').append(
                                $('<option value="' + data.vendorId + "~" + data.ledgerId + '" style="width:150px"></option>').val(data.vendorId + "~" + data.ledgerId).html(data.vendorName)
                                )
                    });
                } else {
                    $('#vendorName').empty();
                }
            }
        });

    }



    function submit(value) {
//            alert(value);
        document.clearance.action = '/throttle/handleCRJPayment.do';
        document.clearance.submit();
    }
    function submitPage(value) {
        document.clearance.action = '/throttle/addVendorInvoiceReceipt.do?param=value';
        document.clearance.submit();
    }
</script>
<script type="text/javascript">
    var httpReq;
    function setVendor(vendorTypeid) { //alert(str);
        var temp = "";
//                                alert(vendorTypeid);
        $.ajax({
            url: "/throttle/vendorPaymentsJson.do",
            dataType: "json",
            data: {
                vendorType: vendorTypeid
            },
            success: function(temp) {
//                                         alert(temp);
                if (temp != '') {
                    $('#vendorName').empty();
                    $('#vendorName').append(
                            $('<option style="width:150px"></option>').val(0 + "~" + 0).html('---Select----')
                            )
                    $.each(temp, function(i, data) {
                        $('#vendorName').append(
                                $('<option value="' + data.vendorId + "~" + data.ledgerId + '" style="width:150px"></option>').val(data.vendorId + "~" + data.ledgerId).html(data.vendorName)
                                )
                    });
                } else {
                    $('#vendorName').empty();
                }
            }
        });

    }
</script>
<style>
    #index td {
        color:white;
    }
</style>
<%
     DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
     Date date = new Date();
     String fDate = "";
     String tDate = "";
   
     if (request.getAttribute("fromDate") != null) {
         fDate = (String) request.getAttribute("fromDate");
     } else {
         fDate = dateFormat.format(date);
     }
     if (request.getAttribute("toDate") != null) {
         tDate = (String) request.getAttribute("toDate");
     } else {
         tDate = dateFormat.format(date);
     }
%>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Process VIR</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active">Process VIR</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="setFocus();" >
                <form name="clearance" action=""  method="post"  class="form-horizontal form-bordered">
                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover">
                        <thead>
                            <tr height="30" id="index">
                                <th colspan="6"  ><b>Process VIR</b></th>&nbsp;&nbsp;&nbsp;                    
                            </tr>
                            <tr>
                        </thead>
                        <tr>
                            <td><font color="red">*</font><spring:message code="finance.label.FromDate"  text="default text"/></td>
                            <td height="30"><input name="fromDate" id="fromDate" value="<c:out value="${fromDate}"/>" type="text" class="datepicker" autocomplete="off" style="width:200px;height:40px;"></td>
                            <td><font color="red">*</font><spring:message code="finance.label.ToDate"  text="default text"/></td>
                            <td height="30"><input name="toDate" id="toDate" value="<c:out value="${toDate}"/>" type="text" class="datepicker" autocomplete="off" style="width:200px;height:40px;"></td>
<!--                            <td><spring:message code="finance.label.VendorType"  text="default text"/></td>
                            <td>
                                <select class="form-control"  name="vendorType" id="vendorType" onchange="setVendor(this.value);" style="width:200px;height:40px;">
                                    <option value=''>--Select--</option>
                                    <c:forEach items="${VendorTypeList}" var="vendor">
                                        <option value="<c:out value="${vendor.vendorTypeId}"/>"><c:out value="${vendor.vendorTypeValue}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                        <script>
                            document.getElementById("vendorType").value = '<c:out value="${vendorType}"/>';
                            document.getElementById("vendorName").value = '<c:out value="${vendorName}"/>';
                        </script>-->
                        
                            <td><spring:message code="finance.label.VendorName"  text="default text"/> </td>
                            <td>
                                <select class="form-control" name="vendorName" id="vendorName" style="width:200px;height:40px;">
                                    <c:if test="${vendorList != null}">
                                            <option value=''>--Select--</option>
                                        <c:forEach items="${vendorList}" var="vendor">
                                            <option value='<c:out value="${vendor.vendorId}"/>~<c:out value="${vendor.ledgerId}"/>'><c:out value="${vendor.vendorName}"/></option>
                                            <script>
                                                document.getElementById("vendorName").value = '<c:out value="${vendorName}"/>';
                                            </script>
                                        </c:forEach>
                                    </c:if>

                                </select>
                            </td>  
                     <tr>
                            <td  height="30">VIR Code</td>
                            <td  height="30">
                                <input type="text" name="virCode" id="virCode" class="form-control" autocomplete="off" value="<c:out value="${virCode}"/>" style="width:200px;height:40px;">
                            </td>
                            <!--                            </tr>
                                                        <tr>-->
                            <td colspan="2"><input type="button"  value="<spring:message code="finance.label.FetchData"  text="default text"/>"  class="btn btn-success" name="Fetch Data" onClick="submit(this.value)" style="width:100px;height:35px;">
                            </td>  
                              <td colspan="2"></td>
                        </tr>
                    </table>
                    </td>
                    </tr>
                    </table>

                    <br/>
                    <c:if test = "${VIRdetailList != null}" >
                        <table class="table table-info mb30 table-hover" id="table">
                            <thead>
                            <th><spring:message code="finance.label.Sno"  text="default text"/></th>
                            <th>VIR Code</th>
                            <th><spring:message code="finance.label.VendorName"  text="default text"/></th>
                            <th><spring:message code="finance.label.VendorTypeName"  text="default text"/></th>
                            <th>VIR Date</th>
                            <th><spring:message code="finance.label.InvoiceNo"  text="default text"/></th>
                            <th><spring:message code="finance.label.InvoiceAmount"  text="default text"/></th>
                            <th><spring:message code="finance.label.InvoiceDate"  text="default text"/></th>
                            <th><spring:message code="finance.label.Narration"  text="default text"/></th>
                            <th width="130">&emsp;</th>
                            </thead>
                            <tbody>
                                <% int sno = 0;
                            int index = 1;%>
                                <c:forEach items="${VIRdetailList}" var="list"> 

                                    <tr  height="40" > 
                                        <td  height="20"><%=index%></td>
                                        <td  height="20"><c:out value="${list.code}"/></td>
                                        <td  height="20"><c:out value="${list.vendorName}"/></td>
                                        <td  height="20"><c:out value="${list.vendorTypeName}"/></td>
                                        <td  height="20"><c:out value="${list.virDate}"/></td>
                                        <td  height="20"><c:out value="${list.invoiceNo}"/></td>
                                        <td  height="20"><c:out value="${list.invoiceAmount}"/></td>
                                        <td  height="20"><c:out value="${list.invoiceDate}"/></td>
                                        <td  height="20"><c:out value="${list.headerNarration}"/></td>
                                        <td  height="20"  width="130" align="left">
                                            <c:if test = "${list.status == 0}" >
                                                <a href="/throttle/vendorPaymentView.do?vendorType=<c:out value='${list.vendorTypeId}' />&vendorName=<c:out value="${list.vendorId}"/>~<c:out value="${list.vendorName}"/>&invoiceNo=<c:out value="${list.invoiceNo}"/>&invoiceAmount=<c:out value="${list.invoiceAmount}"/>&invoiceDate=<c:out value="${list.invoiceDate}"/>&virCode=<c:out value="${list.code}" />&vendorLedgerId=<c:out value='${list.vendorLedgerId}' />&virCode=<c:out value='${list.code}' />&contractTypeId=<c:out value="${list.contractTypeId}" />&billingState=<c:out value="${list.billingState}" />&param=process" class="label label-warning">
                                                    Process 
                                                </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </c:if>
                                            <c:if test = "${list.status == 1}" >
                                                &nbsp;<label style="color:green">Processed </label>
                                               <a href="/throttle/vendorPaymentView.do?vendorType=<c:out value='${list.vendorTypeId}' />&vendorName=<c:out value="${list.vendorId}"/>~<c:out value="${list.vendorName}"/>&invoiceNo=<c:out value="${list.invoiceNo}"/>&invoiceAmount=<c:out value="${list.invoiceAmount}"/>&invoiceDate=<c:out value="${list.invoiceDate}"/>&virCode=<c:out value="${list.code}" />&vendorLedgerId=<c:out value='${list.vendorLedgerId}' />&virCode=<c:out value='${list.code}' />&contractTypeId=<c:out value="${list.contractTypeId}" />&billingState=<c:out value="${list.billingState}" />&param=view" class="label label-Primary">
                                                    View 
                                                </a>
                                            </c:if>
                                        </td>
                                        </td>
                                    </tr>
                                    <%
                                        index++;
                                        sno++;
                                    %>
                                </c:forEach>
                            </tbody>
                        </table>

                        <input type="hidden" name="count" id="count" value="<%=sno%>" />
                    </c:if>
                    <br>
                    <br>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                                <option value="5"  selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span><spring:message code="finance.label.EntriesPerPage"  text="default text"/></span>
                        </div>
                        <div id="navigation" >
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text"><spring:message code="finance.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="finance.label.of"  text="default text"/> <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
