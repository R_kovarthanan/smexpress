<%@page import="java.text.SimpleDateFormat"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
<!--        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />-->

<!--        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>-->

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>





        
        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
        function setDriverDetails(){
                if('<%=request.getAttribute("vehicleNo")%>' != null){
                    computeVehicleCapUtil();
                }
            }
        function computeExpense(){

            var distance = document.trip.preStartLocationDistance.value;
            var vehicleMileage = document.trip.vehicleMileage.value;
            var tollRate = document.trip.tollRate.value;
            var fuelPrice = document.trip.fuelPrice.value;

            if(distance.trim() != '' && distance.trim() != '0'){
                var fuelLtrs = parseFloat(distance) / parseFloat(vehicleMileage);
                var fuelCost = (parseFloat(fuelPrice) * fuelLtrs);
                var tollCost = parseFloat(distance) * parseFloat(tollRate);
                var totalCost = (fuelCost + tollCost).toFixed(2);
                document.trip.preStartRouteExpense.value = totalCost;
                document.trip.preStartLocationDurationHrs.value = 0;
                document.trip.preStartLocationDurationMins.value = 0;
            }

        }
         function validateRoute(val, pointName){
    //            alert(val);
                var pointIdValue = document.getElementById("pointId"+val).value;
                var pointOrderValue = document.getElementById("pointOrder"+val).value;
                var pointOrder = document.getElementsByName("pointOrder");
                var pointNames = document.getElementsByName("pointName");
                var pointIdPrev = 0;
                var pointIdNext = 0;
                var pointNamePrev = '';
                var pointNameNext = '';
    //            alert(pointOrderValue);
    //            alert(pointOrder.length);
                var prevPointOrderVal = parseInt(pointOrderValue)-1;
                var nextPointOrderVal = parseInt(pointOrderValue)+1;
                for(var m=1; m<= pointOrder.length; m++){
    //                    alert("loop:"+m+" :"+document.getElementById("pointOrder"+m).value +"pointOrderValue:"+pointOrderValue+"prevPointOrderVal:"+prevPointOrderVal+"nextPointOrderVal:"+nextPointOrderVal);
                        if(document.getElementById("pointOrder"+m).value == prevPointOrderVal){
                            pointIdPrev = document.getElementById("pointId"+m).value
                            pointNamePrev = document.getElementById("pointName"+m).value
    //                        alert("pointIdPrev:"+pointIdPrev);
                        }
                        if(document.getElementById("pointOrder"+m).value == nextPointOrderVal){
    //                        alert("am here..");
                            pointIdNext = document.getElementById("pointId"+m).value
                            pointNameNext = document.getElementById("pointName"+m).value
    //                        alert("pointIdNext:"+pointIdNext);
                        }
                }
    //            alert(pointIdValue);
    //            alert(pointIdPrev);
    //            alert(pointIdNext);

                ajaxRouteCheck(pointIdValue,pointIdPrev,pointIdNext,val, pointName,pointNamePrev,pointNameNext);

            }



            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

      function calculateTravelHours(sno){
        var endDates = document.getElementById("preStartLocationPlanDate").value;
        var endTimeIds = document.getElementById('endTimeIds'+sno).value;
        var tempDate1 = endDates.split("-");
        var tempTime1 = endTimeIds.split(":");
        var stDates = document.getElementById('stDates'+sno).value;
        var stTimeIds = document.getElementById('stTimeIds'+sno).value;
        var tempDate2 = stDates.split("-");
        var tempTime2 = stTimeIds.split(":");
        var prevTime = new Date(tempDate2[2],tempDate2[1],tempDate2[0],tempTime2[0],tempTime2[1]);  // Feb 1, 2011
        var thisTime = new Date(tempDate1[2],tempDate1[1],tempDate1[0],tempTime1[0],tempTime1[1]);              // now
        var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
        var hoursDifference = Math.floor(difference/1000/60/60);
        document.getElementById('timeDifferences'+sno).value = hoursDifference;
        }


    </script>
         <script type="text/javascript">
        function calculateDate(){
        var endDates = document.getElementById("tripScheduleDate").value;
        var tempDate1 = endDates.split("-");
        var stDates = document.getElementById("preStartLocationPlanDate").value;
        var tempDate2 = stDates.split("-");
        var prevTime = new Date(tempDate2[2],tempDate2[1],tempDate2[0]);  // Feb 1, 2011
        var thisTime = new Date(tempDate1[2],tempDate1[1],tempDate1[0]);              // now
        var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
        if(prevTime.getTime() < thisTime.getTime()){
            if(difference > 0){
            alert(" Selected Date is greater than scheduled date ");
            document.getElementById("preStartLocationPlanDate").value = document.getElementById("todayDate").value;
        }
        }
        }
         function calculateTime(){
        var endDates = document.getElementById("tripScheduleDate").value;
        var endTimeIds = document.getElementById("tripScheduleTime").value;
        var tempDate1 = endDates.split("-");
         var tempTime3 = endTimeIds.split(" ");
        var tempTime1 = tempTime3[0].split(":");
        if(tempTime3[1] =="PM"){
          tempTime1[0] = 12 + parseInt(tempTime1[0]);
        }
        var stDates = document.getElementById("preStartLocationPlanDate").value;
        var hour = document.getElementById("preStartLocationPlanTimeHrs").value;
        var minute = document.getElementById("preStartLocationPlanTimeMins").value;
        var stTimeIds = hour + ":" + minute + ":" + "00";
        var tempDate2 = stDates.split("-");
        var tempTime2 = stTimeIds.split(":");
        var prevTime = new Date(tempDate2[2],tempDate2[1],tempDate2[0],tempTime2[0],tempTime2[1]);  // Feb 1, 2011
        var thisTime = new Date(tempDate1[2],tempDate1[1],tempDate1[0],parseInt(tempTime1[0]),tempTime1[1]);              // now
        var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
        var hoursDifference = Math.floor(difference/1000/60/60);
         if(prevTime.getTime() < thisTime.getTime()){
         if(hoursDifference > 0){
            alert("Selected Time is greater than scheduled Time ");
            document.getElementById("preStartLocationPlanTimeHrs").focus();
                 }
     }
         }
        </script>
<script type="text/javascript">

            	var httpReqRouteCheck;
                var temp = "";
                function ajaxRouteCheck(pointIdValue,pointIdPrev,pointIdNext,val, pointName,pointNamePrev,pointNameNext)
                {

                    var url = "/throttle/checkForRoute.do?pointIdValue="+pointIdValue+"&pointIdPrev="+pointIdPrev+"&pointIdNext="+pointIdNext;

                    if (window.ActiveXObject)
                    {
                        httpReqRouteCheck = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                    {
                        httpReqRouteCheck = new XMLHttpRequest();
                    }
                    httpReqRouteCheck.open("GET", url, true);
                    httpReqRouteCheck.onreadystatechange = function() {
                        processAjaxRouteCheck(val, pointName,pointNamePrev,pointNameNext);
                    };
                    httpReqRouteCheck.send(null);
                }

                function processAjaxRouteCheck(val, pointName,pointNamePrev,pointNameNext)
                {
                    if (httpReqRouteCheck.readyState == 4)
                    {
                        if (httpReqRouteCheck.status == 200)
                        {
                            temp = httpReqRouteCheck.responseText.valueOf();
                            //alert(temp);
                            var tempVal = temp.split('~');
                            if(tempVal[0] == 0){
                                alert("valid route does not exists for the selected point: "+ pointName);
                                document.getElementById("pointName"+val).value='';
                                document.getElementById("pointOrder"+val).focus();
                            }
                            if(tempVal[0] == 1){
                                alert("valid route does not exists between "+ pointName + " and "+pointNameNext);
                                document.getElementById("pointName"+val).value='';
                                document.getElementById("pointOrder"+val).focus();
                            }
                            if(tempVal[0] == 2){
                                document.getElementById("pointOrder"+val).focus();
                                var tempValSplit = tempVal[1].split("-");
                                document.getElementById("pointRouteId"+(val-1)).value=tempValSplit[0];
                                document.getElementById("pointRouteKm"+(val-1)).value=tempValSplit[1];
                                document.getElementById("pointRouteReeferHr"+(val-1)).value=tempValSplit[2];

                                tempValSplit = tempVal[2].split("-");
                                document.getElementById("pointRouteId"+(val+1)).value=tempValSplit[0];
                                document.getElementById("pointRouteKm"+(val+1)).value=tempValSplit[1];
                                document.getElementById("pointRouteReeferHr"+(val+1)).value=tempValSplit[2];
                            }
                        }
                        else
                        {
                            alert("Error loading page\n" + httpReqRouteCheck.status + ":" + httpReqRouteCheck.statusText);
                        }
                    }
                }




            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });



            var httpReq;
            var temp = "";




            function fetchRouteInfo() {
                var preStartLocationId = document.trip.preStartLocationId.value;
                var originId = document.trip.originId.value;
                var vehicleTypeId = document.trip.vehicleTypeId.value;
                if(preStartLocationId != ''){
                     var url = "/throttle/checkPreStartRoute.do?preStartLocationId="+preStartLocationId+"&originId="+originId+"&vehicleTypeId="+vehicleTypeId;

                    if (window.ActiveXObject)
                    {
                        httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)
                    {
                        httpReq = new XMLHttpRequest();
                    }
                    httpReq.open("GET", url, true);
                    httpReq.onreadystatechange = function() {
                        processFetchRouteCheck();
                    };
                    httpReq.send(null);
                }
                
            }
            
            function processFetchRouteCheck()
            {
                if (httpReq.readyState == 4)
                {
                    if (httpReq.status == 200)
                    {
                        temp = httpReq.responseText.valueOf();
                        //alert(temp);
                        if(temp != '' && temp != null  && temp != 'null'){
                            var tempVal = temp.split('-');
                            document.trip.preStartLocationDistance.value = tempVal[0];
                            document.trip.preStartLocationDurationHrs.value = tempVal[1];
                            document.trip.preStartLocationDurationMins.value = tempVal[2];
                            document.trip.preStartRouteExpense.value = tempVal[3];
                            document.getElementById("preStartLocationDistance").readOnly = true;
                            document.getElementById("preStartLocationDurationHrs").readOnly = true;
                            document.getElementById("preStartLocationDurationMins").readOnly = true;
                            document.getElementById("preStartRouteExpense").readOnly = true;
                        }else{
                            alert('valid route does not exists between pre start location and trip start location');
                            document.getElementById("preStartLocationDistance").readOnly = false;
                            document.getElementById("preStartLocationDurationHrs").readOnly = false;
                            document.getElementById("preStartLocationDurationMins").readOnly = false;
                            document.getElementById("preStartRouteExpense").readOnly = false;
                        }
//                        if(tempVal[0] == 0){
//                            alert("no match found");
//                        }
                    }
                    else
                    {
                        alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                    }
                }
            }
            function processPreStartCheckBox(){
                
                var tempVal = document.getElementById("preStartLocationCheckbox").checked;
                if(tempVal){
                    document.trip.preStartLocationCheckbox.value=1;
                    $("#preStartDetailDiv").hide();
                }else{
                    document.trip.preStartLocationCheckbox.value=0;
                    $("#preStartDetailDiv").show();
                }
            }
            function setButtons(){
                var temp = document.trip.actionName.value;                
                if(temp != '0'){
                    if(temp == '1'){
                        $("#actionDiv").hide();
                        $("#tripDiv").show();
                        $("#preStartDiv").show();
                    }
                }else {
                    $("#preStartDiv").hide();
                    var vehicleId = document.trip.vehicleId.value;
                    if(vehicleId != '' && vehicleId != '0'){
                        $("#actionDiv").hide();
                        $("#tripDiv").show();
                    }else{
                        $("#actionDiv").show();
                        $("#tripDiv").show();
                    }


                }
            }
        </script>

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
        
       
        <script type="text/javascript" language="javascript">           

            


            function getDriverName() {
                var oTextbox = new AutoSuggestControl(document.getElementById("driName"), new ListSuggestions("driName", "/throttle/handleDriverSettlement.do?"));

            }
        </script>
        <script language="">
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }

            function estimateExpense() {
                var orderIds = document.trip.orderId;
                var pointIds = document.trip.pointId;
                var pointTypes = document.trip.pointType;
                var pointOrders = document.trip.pointOrder;
                var pointAddress = document.trip.pointAddress;
                var pointPlanDates = document.trip.pointPlanDate;
                var pointPlanTimes = document.trip.pointPlanTime;

                var orderIdStr = "";
                var pointIdStr = "";
                var pointTypeStr = "";
                var pointOrderStr = "";
                var pointAddressStr = "";
                var pointPlanDateStr = "";
                var pointPlanTimeStr = "";

                for (var i=0; i < orderIds.length; i++) {
                    if(i == 0){
                        orderIdStr = orderIds[i];
                        pointIdStr = pointIds[i];
                        pointTypeStr = pointTypes[i];
                        pointOrderStr = pointOrders[i];
                        pointAddressStr = pointAddress[i];
                        pointPlanDateStr = pointPlanDates[i];
                        pointPlanTimeStr = pointPlanTimes[i];
                    }else{
                        orderIdStr = orderIdStr + "," +orderIds[i];
                        pointIdStr = pointIdStr + "," +pointIds[i];
                        pointTypeStr = pointTypeStr + "," +pointTypes[i];
                        pointOrderStr = pointOrderStr + "," +pointOrders[i];
                        pointAddressStr = pointAddressStr + "," +pointAddress[i];
                        pointPlanDateStr = pointPlanDateStr + "," +pointPlanDates[i];
                        pointPlanTimeStr = pointPlanTimeStr + "," +pointPlanTimes[i];
                    }
                }
            }


                //start ajax for vehicle Nos
                $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#vehicleNo').autocomplete({ 
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getVehicleRegNos.do",
                            dataType: "json",
                            data: {
                                vehicleNo: (request.term).trim(),
                                textBox: 1
                            },
                            success: function(data, textStatus, jqXHR) {
//                                alert(data);
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                //console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        //alert(value);
                        var tmp = value.split('-');
                        $('#vehicleId').val(tmp[0]);
                        //alert("1");
                        $('#vehicleNo').val(tmp[1]);
                        //alert("2");
                        $('#vehicleTonnage').val(tmp[2]);
                        //alert("3");
                        $('#driver1Id').val(tmp[3]);
                        //alert("4");
                        $('#driver1Name').val(tmp[4]);
                        //alert("5");
                        $('#driver2Id').val(tmp[5]);
                        //alert("6");
                        $('#driver2Name').val(tmp[6]);
                        //alert("7");
                        $('#driver3Id').val(tmp[7]);
                        //alert("8");
                        $('#driver3Name').val(tmp[8]);
                        //<option value="Cancel">Cancel Order</option>
                        //<option value="Suggest Schedule Change">Suggest Schedule Change</option>
                        //<option value="Hold Order for further Processing">Hold Order for further Processing</option>
                        $('#actionName').empty();
                         var actionOpt = document.trip.actionName;
                         var optionVar = new Option("-select-", '0');
                         actionOpt.options[0] = optionVar;
                         optionVar = new Option("Freeze", '1');
                         actionOpt.options[1] = optionVar;
//                        $('#actionName').append(
//                            $('<option></option>').val(0).html('-select-')
//                            )
//                        $('#actionName').append(
//                            $('<option></option>').val(1).html('Freeze')
//                            )
                         $("#actionDiv").hide();

                        //
                        //alert("9");
                        //$itemrow.find('#vehicleId').val(tmp[0]);
                        //$itemrow.find('#vehicleNo').val(tmp[1]);
                        return false;
                    }
                    // Format the list menu output of the autocomplete
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    //alert(item);
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            //.append( "<a>"+ item.Name + "</a>" )
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

            });
            //end ajax for vehicle Nos
                
            //
                //start ajax for pre location Nos
                $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#preStartLocation').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getCityFromName.do",
                            dataType: "json",
                            data: {
                                cityFrom: request.term,
                                textBox: 1
                            },
                            success: function(data, textStatus, jqXHR) {
//                                alert(data);
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                //console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#preStartLocationId').val(tmp[0]);
                        $('#preStartLocation').val(tmp[1]);
                        return false;
                    }
                    // Format the list menu output of the autocomplete
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    //alert(item);
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

            });
            //end ajax for pre location

            function submitPage() {
                var statusCheck = true;
                var temp = document.trip.actionName.value;
                //alert(temp);
                if(temp != '0'){
                    if(temp == '1'){ //freeze selected
                        var tempVal = document.getElementById("preStartLocationCheckbox").checked;
                        //alert(tempVal);
                        if(!tempVal){//same checkbox is not selected
                            //check for distance and route expense;
                            var distance = document.trip.preStartLocationDistance.value;
                            //alert(distance);
                            if(distance == '' || distance == '0'){
                                alert('please enter pre start distance');
                                document.trip.preStartLocationDistance.focus();
                                statusCheck = false;
                            }
                        }
                    }
                }
                if(statusCheck && !tempVal && temp == '1'){
                    if(document.trip.preStartLocationPlanDate.value == ''){
                        alert('please enter pre start plan date');
                        document.trip.preStartLocationPlanDate.focus();
                        statusCheck = false;
                    }
                }
                if(statusCheck){
                    document.trip.action = "/throttle/saveTripSheet.do?";
                    document.trip.submit();
                }
            }
            function saveAction() {
                if(document.trip.actionName.value != 0 ){
                    document.trip.action = "/throttle/saveAction.do?";
                    document.trip.submit();
                }else {
                    alert("please select your action");
                    document.trip.actionName.focus();
                }
            }
            function setVehicleValues() {
                var value = document.trip.vehicleNo.value;

                if(value != 0){
                    var tmp = value.split('-');
                    $('#vehicleId').val(tmp[0]);
                    //alert("1");
                    $('#vehicleNoEmail').val(tmp[1]);
                    //alert("2");
                    $('#vehicleTonnage').val(tmp[2]);
                    //alert("3");
                    $('#driver1Id').val(tmp[3]);
                    //alert("4");
                    $('#driver1Name').val(tmp[4]);
                    //alert("5");
                    $('#driver2Id').val(tmp[5]);
                    //alert("6");
                    $('#driver2Name').val(tmp[6]);
                    //alert("7");
                    $('#driver3Id').val(tmp[7]);
                    //alert("8");
                    $('#driver3Name').val(tmp[8]);
                    $('#actionName').empty();
                     var actionOpt = document.trip.actionName;
                     var optionVar = new Option("-select-", '0');
                     actionOpt.options[0] = optionVar;
                     optionVar = new Option("Freeze", '1');
                     actionOpt.options[1] = optionVar;
                     $("#actionDiv").hide();
                }else{
                    $('#vehicleId').val('');
                    $('#vehicleNoEmail').val('');
                    $('#vehicleTonnage').val('');
                    $('#driver1Id').val('');
                    $('#driver1Name').val('');
                    $('#driver2Id').val('');
                    $('#driver2Name').val('');
                    $('#driver3Id').val('');
                    $('#driver3Name').val('');
                    $('#actionName').empty();
                    var actionOpt = document.trip.actionName;
                     var optionVar = new Option("-select-", '0');
                     actionOpt.options[0] = optionVar;
                     optionVar = new Option("Cancel Order", 'Cancel');
                     actionOpt.options[1] = optionVar;
                     optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                     actionOpt.options[2] = optionVar;
                     optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                     actionOpt.options[3] = optionVar;
                     $("#actionDiv").show();
                }
            }
            function computeVehicleCapUtil(){
                setVehicleValues();
                var orderWeight = document.trip.totalWeight.value;
                var vehicleCapacity = document.trip.vehicleTonnage.value;
                if(vehicleCapacity > 0) {
                    var utilPercent = (orderWeight/vehicleCapacity) * 100;
                    //document.trip.vehicleCapUtil.value = utilPercent.toFixed(2);


                    var e = document.getElementById("vehicleCheck");
                    e.style.display = 'block';
                }else{
                   //document.trip.vehicleCapUtil.value = 0;
                }
            }

        </script>





    </head>

    
    <body onload="setDriverDetails();" >
        
        <form name="trip" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>

            <%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String startDate = sdf.format(today);
            %>


            <%
            String vehicleMileageAndTollRate = "";
            String vehicleMileage = "0";
            String tollRate = "0";
            String fuelPrice = "0";
            String[] temp = null;
            if(request.getAttribute("vehicleMileageAndTollRate") != null){
                vehicleMileageAndTollRate = (String)request.getAttribute("vehicleMileageAndTollRate");
                temp = vehicleMileageAndTollRate.split("-");
                vehicleMileage = temp[0];
                tollRate = temp[1];
                fuelPrice = temp[2];
            }
            %>
            <input type="hidden" name="vehicleMileage" value="<%=vehicleMileage%>" />
            <input type="hidden" name="tollRate" value="<%=tollRate%>" />
            <input type="hidden" name="fuelPrice" value="<%=fuelPrice%>" />
            <input type="hidden" name="vehicleNoEmail" id="vehicleNoEmail" value="" />

            <c:set var="cNotes" value="" />
            <c:set var="routeInfo" value="" />
            <c:set var="customerName" value="" />
            <c:set var="customerType" value="" />
            <c:set var="vehicleType" value="" />
            <c:set var="billingType" value="" />
            <c:set var="consginmentRemarks" value="" />
            <c:set var="tripSchedule" value="" />
            <c:set var="routeInfo" value="" />
            <c:set var="totalPoints" value="" />
            <c:set var="totalHours" value="" />
            <c:set var="reeferRequired" value="" />
            <c:set var="orderRevenue" value="" />
            <c:set var="orderExpense" value="" />
            <c:set var="profitMargin" value="" />
            <c:set var="totalWeight" value="" />
            <c:set var="productInfo" value="" />
            <c:set var="vehicleCapUtilValue" value="" />
            
            <% int i=0; %>
            <c:if test = "${consignmentList != null}" >
                <c:forEach items="${consignmentList}" var="consignment">
                    <%if (i == 0){%>
                    <c:set var="customerName" value="${consignment.customerName}" />
                    <c:set var="customerType" value="${consignment.customerType}" />
                    <c:set var="billingType" value="${consignment.billingType}" />
                    <c:set var="consginmentRemarks" value="${consignment.consginmentRemarks}" />
                    <c:set var="reeferRequired" value="${consignment.reeferRequired}" />
                    <c:set var="orderRevenue" value="${consignment.orderRevenue}" />
                    <c:set var="orderExpense" value="${consignment.orderExpense}" />
                    <c:set var="totalWeight" value="${consignment.totalWeight}" />
                    <c:set var="vehicleCapUtilValue" value="${consignment.vehicleCapUtil}" />
                    
<!--                    //has to choose the earliest-->
                    <c:set var="tripSchedule" value="${consignment.tripScheduleDate} : ${consignment.tripScheduleTime}" />
                    <input type="hidden" name="tripScheduleDate"  id="tripScheduleDate" value='<c:out value="${consignment.tripScheduleDate}" />'>
                    <input type="hidden" name="consignmentOrderId"  value='<c:out value="${consignment.orderId}" />'>
                    <input type="hidden" name="tripScheduleTime" id="tripScheduleTime" value='<c:out value="${consignment.tripScheduleTimeDB}" />'>
                    <input type="hidden" name="vehicleTypeId" value='<c:out value="${consignment.vehicleTypeId}" />'>
                    <input type="hidden" name="originId" value='<c:out value="${consignment.originId}" />'>
                    <input type="hidden" name="destinationId" value='<c:out value="${consignment.destinationId}" />'>
                    <input type="hidden" name="customerId" value='<c:out value="${consignment.customerId}" />'>
                    <input type="hidden" name="productInfo" value='<c:out value="${consignment.productInfo}" />'>

                    <c:set var="vehicleType" value="${consignment.vehicleTypeName}" />                    
                    <c:set var="productInfo" value="${consignment.productInfo}" />
                    <c:set var="totalHours" value="${consignment.totalHrs}" />
                    <c:set var="cNotes" value="${consignment.orderNo}" />
                    <c:set var="routeInfo" value="${consignment.origin} - ${consignment.destination}" />
                    <%}else{%>
                    <input type="hidden" name="consignmentOrderId" value='<c:out value="${consignment.orderId}" />'>
                    <c:set var="totalWeight" value="${totalWeight + consignment.totalWeight}" />
                    <c:set var="orderRevenue" value="${orderRevenue + consignment.orderRevenue}" />
                    <c:set var="totalHours" value="${totalHours + consignment.totalHrs}" />
                    <c:set var="cNotes" value="${cNotes},${consignment.orderNo}" />
                    <c:set var="routeInfo" value="${routeInfo},${consignment.origin} - ${consignment.destination}" />
                    <%}
                      i++;
                    %>
                </c:forEach>
                    <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                    <c:set var="totalWeight" value="${totalWeight/1000}" />

            </c:if>

            <input type="hidden" name="totalHours" value='<c:out value="${totalHours}" />'>

            <%
            boolean isSingleConsignmentOrder = true;
            if(i > 1){
                isSingleConsignmentOrder = false;
            }
            String totalPointsStr = "2";
            System.out.println("val"+pageContext.getAttribute("totalPoints"));
            if(i == 1){
                totalPointsStr = "" + (String)pageContext.getAttribute("totalPoints");
            }else{
                totalPointsStr = "" + (String)pageContext.getAttribute("totalPoints");

            }
            int totalPointsValue = 0;
            if(!"".equals(totalPointsStr)){
                totalPointsValue = Integer.parseInt(totalPointsStr);
            }
            %>
<table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:300;">

                            <div id="first">
                               <%if (isSingleConsignmentOrder){%>
                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                        <td> Rs.<c:out value="${orderRevenue}" /></td>
                                            <input type="hidden" name="orderRevenue" value='<c:out value="${orderRevenue}" />'>
                                     </tr>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Projected Expense:</b></font></td>
                                        <td> Rs.<c:out value="${orderExpense}" /></td>
                                            <input type="hidden" name="orderExpense" value='<c:out value="${orderExpense}" />'>
                                     </tr>

                                     <%
                                     String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                     String revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
                                     float profitPercentage = 0.00F;
                                     if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                         profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                     }


                                     %>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                        <td>  <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)
                                            <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                                        <td>
                                     </tr>
                                </table>
                                <%}else{%>
                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                        <td> <c:out value="${orderRevenue}" /></td>
                                     </tr>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Projected Expense:</b></font></td>
                                        <td> fix route plan</td>
                                     </tr>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                        <td> fix route plans<td>
                                     </tr>
                                </table>
                                <%}%>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>



<!--            <table width="100%">
                <tr>
                    <td class="contenthead" >Create Trip Sheet</td>
                    <td class="contenthead" >&nbsp;</td>
                    <td class="contenthead" >Customer:Bakers Circle</td>
                    <td class="contenthead" >&nbsp;</td>
                    <td class="contenthead" >Route: Kashipur - Chandigarh</td>
                    <td class="contenthead" >&nbsp;</td>
                    <td class="contenthead" >Trip Schedule: 07/11/2013 : 10:00AM</td>
                </tr>
            </table>-->
<br>
<br>
<br>
<br>
            <div id="tabs" >
                <ul>
                    <li><a href="#tripDetail"><span>Trip Details</span></a></li>
                    <li><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
<!--                    <li><a href="#action"><span>Action</span></a></li>-->
<!--                    <li><a href="#driverDetail"><span>Driver</span></a></li>
                    <li><a href="#cleanerDetail"><span>Cleaner</span></a></li>-->
<!--                    <li><a href="#advDetail"><span>Advance</span></a></li>-->
                    <!--<li><a href="#expDetail"><span>Expense Details</span></a></li>-->
<!--                    <li><a href="#summary"><span>Remarks</span></a></li>-->
                </ul>

                <div id="tripDetail">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="6" >Trip Details</td>
                        </tr>
                        <tr>
<!--                            <td class="text1"><font color="red">*</font>Trip Sheet Date</td>
                            <td class="text1"><input type="text" name="tripDate" class="datepicker" value=""></td>-->
                            <td class="text1">CNote No(s)</td>
                            <td class="text1">
                                <c:out value="${cNotes}" />
                                <input type="hidden" name="cNotes" Id="cNotes" class="textbox" value='<c:out value="${cNotes}" />'>
                            </td>
                            <td class="text1">Billing Type</td>
                            <td class="text1">
                                <c:out value="${billingType}" />
                                <input type="hidden" name="billingType" Id="billingType" class="textbox" value='<c:out value="${billingType}" />'>
                            </td>
                        </tr>
                        <tr>
<!--                            <td class="text2">Customer Code</td>
                            <td class="text2">BF00001</td>-->
                            <td class="text2">Customer Name</td>
                            <td class="text2">
                                <c:out value="${customerName}" />
                                <input type="hidden" name="customerName" Id="customerName" class="textbox" value='<c:out value="${customerName}" />'>
                            </td>
                            <td class="text2">Customer Type</td>
                            <td class="text2" colspan="3" >
                                <c:out value="${customerType}" />
                                <input type="hidden" name="customerType" Id="customerType" class="textbox" value='<c:out value="${customerType}" />'>
                            </td>
                        </tr>
                        <tr>
                            <td class="text1">Route Name</td>
                            <td class="text1">
                                <c:out value="${routeInfo}" />
                                <input type="hidden" name="routeInfo" Id="routeInfo" class="textbox" value='<c:out value="${routeInfo}" />'>
                            </td>
<!--                            <td class="text1">Route Code</td>
                            <td class="text1" >DL001</td>-->
                            <td class="text1">Reefer Required</td>
                            <td class="text1" >
                                <c:out value="${reeferRequired}" />
                                <input type="hidden" name="reeferRequired" Id="reeferRequired" class="textbox" value='<c:out value="${reeferRequired}" />'>
                            </td>
                            <td class="text1">Order Est Weight (MT)</td>
                            <td class="text1" >
                                <input type="text" name="totalWeight" Id="totalWeight" class="textbox" value='<c:out value="${totalWeight}" />' readonly >

                            </td>
                        </tr>
                        <tr>
                            <td class="text2">Vehicle Type</td>
                            <td class="text2">
                                <c:out value="${vehicleType}" />
                                <input type="hidden" name="vehicleType" Id="vehicleType" class="textbox" value='<c:out value="${vehicleType}" />'>
                            </td>
                            <td class="text2">Vehicle No</td>
                            <td class="text2">
<!--                                <input type="text" name="vehicleNo" Id="vehicleNo" onblur="computeVehicleCapUtil();" class="textbox" value="<c:out value="${vehicleNo}"/>">-->

                                <%-- comment start here --%>
                                <c:if test="${vehicleNo != null && vehicleNo != ''}">
                                    <input type="text" name="vehicleNo" Id="vehicleNo" onblur="computeVehicleCapUtil();" class="textbox" value="<c:out value="${vehicleNo}"/>">
                                </c:if>
                                <c:if test="${vehicleNo == null || vehicleNo == ''}">
                                    <select name="vehicleNo" id="vehicleNo" onchange="computeVehicleCapUtil();" class="textbox" >
                                       <c:if test="${vehicleNos != null}">
                                            <option value="0" selected>--select--</option>
                                            <c:forEach items="${vehicleNos}" var="vehNo">
                                                <option value='<c:out value="${vehNo.vehicleId}"/>-<c:out value="${vehNo.vehicleNo}"/>-<c:out value="${vehNo.vehicleTonnage}"/>-<c:out value="${vehNo.primaryDriverId}"/>-<c:out value="${vehNo.primaryDriverName}"/>-<c:out value="${vehNo.secondaryDriver1Id}"/>-<c:out value="${vehNo.secondaryDriver1Name}"/>-<c:out value="${vehNo.secondaryDriver2Id}"/>-<c:out value="${vehNo.secondaryDriver2Name}"/>'>
                                                    <c:out value="${vehNo.vehicleNo}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                </c:if>
                                <%-- comment end here --%>

                            </td>
                            <input type="hidden" name="vehicleId" Id="vehicleId" class="textbox" value="<c:out value="${vehicleId}"/>">
                            <td class="text2">Vehicle Capacity (MT)</td>
                            <td class="text2"><input type="text" name="vehicleTonnage" Id="vehicleTonnage" readonly class="textbox" value="<c:out value="${vehicleTonnage}"/>"></td>
                        </tr>

                        <tr>
                            <td class="text1">Veh. Cap [Util%]</td>
                            <td class="text1"><input type="text" name="vehicleCapUtil" Id="vehicleCapUtil" readonly class="textbox" value='<c:out value="${vehicleCapUtilValue}" />'></td>
                            <td class="text1">Special Instruction</td>
                            <td class="text1"><textarea name="tripRemarks" cols="20" rows="2" > <c:out value="${consginmentRemarks}" /></textarea></td>
                            <td class="text1">Trip Schedule</td>
                            <td class="text1"><c:out value="${tripSchedule}" /></td>
                        </tr>

                      
                        <tr>
                            <td class="text2">Primary Driver </td>
                            <td class="text2">
                                <input type="text" name="driver1Name"  readonly  Id="driver1Name" class="textbox" value="<c:out value="${primaryDriverName}"/>"  >
                                <input type="hidden" name="driver1Id" Id="driver1Id" class="textbox" value="<c:out value="${primaryDriverId}"/>"  >
                            </td>
                            <td class="text2">Secondary Driver(s) </td>
                            <td class="text2" colspan="5" >
                                <input type="text" name="driver2Name"  readonly Id="driver2Name" class="textbox" value="<c:out value="${secondaryDriver1Name}"/>"  >
                                <input type="hidden" name="driver2Id" Id="driver2Id" class="textbox" value="<c:out value="${secondaryDriver1Id}"/>"  >
                                <input type="text" name="driver3Name"  readonly  Id="driver3Name" class="textbox" value="<c:out value="${secondaryDriver2Name}"/>"  >
                                <input type="hidden" name="driver3Id" Id="driver3Id" class="textbox" value="<c:out value="${secondaryDriver2Id}"/>"  >
                            </td>
                        </tr>
                        <tr>
                            <td class="text1">Product / Temp Info </td>
                            <td class="text1"><c:out value="${productInfo}" /></td>
                        </tr>
<!--                        <tr>
                            <td class="text1"><font color="red">*</font>Cleaner 1</td>
                            <td class="text1"><input type="text" name="ownership" class="textbox" value="" readonly ></td>
                            <td class="text1"><font color="red">*</font>Cleaner 2</td>
                            <td class="text1" colspan="3" ><input type="text" name="ownership" class="textbox" value="" readonly ></td>
                        </tr>-->
                    </table>
                    <br/>
                    <br/>
                    <div id="vehicleCheck" style="display:none;">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="4" >Vehicle Compliance Check</td>
                        </tr>
                        
                        <tr>
                            <td class="text2">Vehicle FC Valid UpTo</td>
                            <td class="text2"><label><font color="green">21-08-2014</font></label></td>
                        </tr>
                        <tr>
                            <td class="text1">Vehicle Insurance Valid UpTo</td>
                            <td class="text1"><label><font color="green">11-04-2014</font></label></td>
                        </tr>
                        <tr>
                            <td class="text2">Vehicle Permit Valid UpTo</td>
                            <td class="text2"><label><font color="green">30-12-2013</font></label></td>
                        </tr>
                        <tr>
                            <td class="text2">Road Tax Valid UpTo</td>
                            <td class="text2"><label><font color="green">30-12-2013</font></label></td>
                        </tr>
                    </table>
                    <br/>
                </div>
                    <br/>
                    <center>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>
                </div>
                <div id="routeDetail">
                    <% int cntr = 1; %>
                    <c:set var="prevConsignmentOrderId" value="0" />
                    <c:if test = "${orderPointDetails != null}" >
                        <table border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" >
                           <% int z = 1; %>

                        <c:forEach items="${orderPointDetails}" var="consignment">
                            <c:if test = "${prevConsignmentOrderId != consignment.consignmentOrderId}" >
                                <tr >
                                    <td class="text1" colspan="6" >Consignment Order No: <c:out value="${consignment.consignmentOrderNo}" /> </td>
                                </tr>
                                <tr >
                                <td class="contenthead" height="30" >Point Name</td>
                                <td class="contenthead" height="30" >Type</td>
                                <td class="contenthead" height="30" >Route Order</td>
                                <td class="contenthead" height="30" >Address</td>
                                <td class="contenthead" height="30" >Planned Date</td>
                                <td class="contenthead" height="30" >Planned Time</td>
                                </tr>
                            </c:if>
                            <tr>
                            <td class="text1" height="30" ><c:out value="${consignment.pointName}" /></td>
                            <input type="hidden" name="orderId" value='<c:out value="${consignment.consignmentOrderId}" />' />
                            <input type="hidden" name="pointId" id="pointId<%=cntr%>"  value='<c:out value="${consignment.pointId}" />' />
                            <td class="text1" height="30" ><input type="text" name="pointType" readonly value='<c:out value="${consignment.pointType}" />' /></td>
                            <%if (isSingleConsignmentOrder){%>
                                <td class="text1" height="30" ><input type="text" id="pointOrder<%=cntr%>"  name="pointOrder" readonly value='<c:out value="${consignment.pointSequence}" />' /></td>
                            <%}else{%>
                                <td class="text1" height="30" ><input type="text" id="pointOrder<%=cntr%>"  name="pointOrder" onchange="validateRoute();" value='<c:out value="${consignment.pointSequence}" />' /></td>
                            <%}%>
                            <td class="text1" height="30" ><textarea name="pointAddresss" rows="2" cols="15"><c:out value="${consignment.pointAddress}" /></textarea></td>
                            <td class="text1" height="30" ><input type="text" name="pointPlanDate"  class="datepicker"  value='<c:out value="${consignment.pointPlanDate}" />' /></td>
                            <td class="text1" height="30" >
                                HH:<input type="text" style="width:30px;" name="pointPlanTimeHrs" value='<c:out value="${consignment.pointPlanTimeHrs}" />' />
                            MI:<input type="text" style="width:30px;"  name="pointPlanTimeMins" value='<c:out value="${consignment.pointPlanTimeMins}" />' />
                            </td>
                            </tr>
                        <% cntr++; %>
                        <c:set var="prevConsignmentOrderId" value="${consignment.consignmentOrderId}" />
                       </c:forEach>
                            </table>
                        <br>
                   </c:if>
                    
<!--                    <br>
                    <center>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>
                    <br>
                </div>
                <div id="action">-->
                    <br>
                    <br>
                    <table border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td class="text1">Action: </td>
                            <td  class="text1">
                                <select name="actionName" id="actionName" onChange="setButtons();">
                                    <option value="0">-select-</option>
                                    <option value="Cancel">Cancel Order</option>
                                    <option value="Suggest Schedule Change">Suggest Schedule Change</option>
                                    <option value="Hold Order for further Processing">Hold Order for further Processing</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td  class="text2">Remarks</td>
                            <td  class="text2"><textarea name="actionRemarks" rows="3" cols="100"></textarea> </td>
                        </tr>
                        <tr>
                            <td  class="text1">Do you want to intimate?</td>
                            <td  class="text1">
                                Client <input type="checkbox" checked name="client" value="1" /> &nbsp;
                                A/C Mgr<input type="checkbox" checked name="manager" value="1" /> &nbsp;
                                Fleet Mgr <input type="checkbox" checked name="fleetManager" value="1" /> &nbsp;
                            </td>
                        </tr>

                    </table>
                    <br>
                    <center>
                        <table border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" >
                            <tr>
                                <td>
                        <div id="actionDiv" style="display:block;">
                        <input type="button" class="button" value="Save Action" name="Next" onclick="saveAction();" />                    
                        </div>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                    <div id="preStartDiv" style="display:none;">
                                        <table border="0" width="500" align="left" cellpadding="0" cellspacing="0" >
                                            <tr>
                                                <td class="text1">Pre Start Location </td>
                                                <td class="text1" align="left"  >
                                                    <input type="checkbox" onclick="processPreStartCheckBox();" style="width:10px;" name="preStartLocationCheckbox"  id="preStartLocationCheckbox" value="0" />same as trip start location
                                                </td>
                                            </tr>
                                        </table>
       
                                            
                                        <div id="preStartDetailDiv" style="display:block;">
                                        <table border="0" width="500" align="left" cellpadding="0" cellspacing="0" >
                                            <tr>
                                                <td class="text1">Pre Start Location  </td>
                                                <td class="text1" align="left"  >
                                                    <input  type="text" name="preStartLocation" id="preStartLocation" onBlur="fetchRouteInfo();" value="" class="textbox" />
                                                    <input  type="hidden" name="preStartLocationId" id="preStartLocationId" value="" class="textbox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text1">preStartLocationPlanDate:</td>
                                                <td class="text1"><input  type="text" name="preStartLocationPlanDate"  id="preStartLocationPlanDate"  value='<%=startDate%>' class="datepicker" onchange="calculateDate();" class="datepicker"/>
                                                    <input type="hidden" name="todayDate" id="todayDate" value='<%=startDate%>'/></td>
                                            </tr>
                                            <tr>

                                                    <td class="text1">preStartLocationPlanTime:</td>
                                                    <td class="text1">HH: <select name="preStartLocationPlanTimeHrs"  id="preStartLocationPlanTimeHrs" onchange="calculateTime();">
                                                        <option value='00'>00</option>
                                                        <option value='01'>01</option>
                                                        <option value='02'>02</option>
                                                        <option value='03'>03</option>
                                                        <option value='04'>04</option>
                                                        <option value='05'>05</option>
                                                        <option value='06'>06</option>
                                                        <option value='07'>07</option>
                                                        <option value='08'>08</option>
                                                        <option value='09'>09</option>
                                                        <option value='10'>10</option>
                                                        <option value='11'>11</option>
                                                        <option value='12'>12</option>
                                                        <option value='13'>13</option>
                                                        <option value='14'>14</option>
                                                        <option value='15'>15</option>
                                                        <option value='16'>16</option>
                                                        <option value='17'>17</option>
                                                        <option value='18'>18</option>
                                                        <option value='19'>19</option>
                                                        <option value='20'>20</option>
                                                        <option value='21'>21</option>
                                                        <option value='22'>22</option>
                                                        <option value='23'>23</option>
                                                        

                                                    </select>
                                                    MI: <select name="preStartLocationPlanTimeMins"  id="preStartLocationPlanTimeMins" >
                                                        <option value='00'>00</option>
                                                        <option value='01'>01</option>
                                                        <option value='02'>02</option>
                                                        <option value='03'>03</option>
                                                        <option value='04'>04</option>
                                                        <option value='05'>05</option>
                                                        <option value='06'>06</option>
                                                        <option value='07'>07</option>
                                                        <option value='08'>08</option>
                                                        <option value='09'>09</option>
                                                        <option value='10'>10</option>
                                                        <option value='11'>11</option>
                                                        <option value='12'>12</option>
                                                        <option value='13'>13</option>
                                                        <option value='14'>14</option>
                                                        <option value='15'>15</option>
                                                        <option value='16'>16</option>
                                                        <option value='17'>17</option>
                                                        <option value='18'>18</option>
                                                        <option value='19'>19</option>
                                                        <option value='20'>20</option>
                                                        <option value='21'>21</option>
                                                        <option value='22'>22</option>
                                                        <option value='23'>23</option>
                                                        <option value='24'>24</option>
                                                        <option value='25'>25</option>
                                                        <option value='26'>26</option>
                                                        <option value='27'>27</option>
                                                        <option value='28'>28</option>
                                                        <option value='29'>29</option>
                                                        <option value='30'>30</option>
                                                        <option value='31'>31</option>
                                                        <option value='32'>32</option>
                                                        <option value='33'>33</option>
                                                        <option value='34'>34</option>
                                                        <option value='35'>35</option>
                                                        <option value='36'>36</option>
                                                        <option value='37'>37</option>
                                                        <option value='38'>38</option>
                                                        <option value='39'>39</option>
                                                        <option value='40'>40</option>
                                                        <option value='41'>41</option>
                                                        <option value='42'>42</option>
                                                        <option value='43'>43</option>
                                                        <option value='44'>44</option>
                                                        <option value='45'>45</option>
                                                        <option value='46'>46</option>
                                                        <option value='47'>47</option>
                                                        <option value='48'>48</option>
                                                        <option value='49'>49</option>
                                                        <option value='50'>50</option>
                                                        <option value='51'>51</option>
                                                        <option value='52'>52</option>
                                                        <option value='53'>53</option>
                                                        <option value='54'>54</option>
                                                        <option value='55'>55</option>
                                                        <option value='56'>56</option>
                                                        <option value='57'>57</option>
                                                        <option value='58'>58</option>
                                                        <option value='59'>59</option>
                                                        <option value='60'>60</option>

                                                    </select>
                                                </td>

                                                </tr>

                                            <tr>
                                                <td class="text1">preStartLocationDistance</td>
                                                <td class="text1"><input  type="text" name="preStartLocationDistance"  id="preStartLocationDistance" onchange="computeExpense();" value="" class="textbox" /></td>
                                                </tr>
                                            <tr>
                                                <td class="text1">preStartLocationDurationHrs:</td>
                                                <td class="text1"><input  type="text" name="preStartLocationDurationHrs"  id="preStartLocationDurationHrs"  value="" class="textbox" /></td>
                                                </tr>
                                            <tr>
                                                <td class="text1">preStartLocationDurationMins:</td>
                                                <td class="text1"><input  type="text" name="preStartLocationDurationMins"  id="preStartLocationDurationMins"  value="" class="textbox" /></td>
                                                </tr>
                                            
                                            <tr>
                                                <td class="text1">preStartRouteExpense</td>
                                                <td class="text1"><input  type="text" name="preStartRouteExpense"  id="preStartRouteExpense"  value="" class="textbox" /></td>
                                                
                                            </tr>

                                        </table>
                                    </div>
                                    </div>

                                </td>
                                </tr>

                                
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                
                                <td>
                                    <div id="tripDiv" style="display:block;">
                                    <%if (!isSingleConsignmentOrder){%>
                                    <a  class="nexttab" href="#"><input type="button" value="Estimate Expense" name="Save" onclick="estimateExpense();" /></a> &nbsp;&nbsp;&nbsp;
                                    <%}%>
                                    <a  class="nexttab" href="#"><input type="button" class="button" value="Generate TripSheet" style="width:200px;" name="Save" onclick="submitPage();" /></a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </center>
                </div>
                



                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>
                
            </div>
        </form>
    </body>
</html>