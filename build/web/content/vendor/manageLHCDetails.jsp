<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">         
    function submitPage() {
        var errStr = "";
//        if (document.getElementById("vehicleNo").value == "") {
//            errStr = "Please enter vehicleNo.\n";
//            alert(errStr);
//            document.getElementById("vehicleNo").focus();
//        }
        if (errStr == "") {
            document.lhc.action = '/throttle/handleAddLHC.do';
            document.lhc.submit();
            $("#Edit").hide();            
        }
    }
</script>
<body onload="addRow();">
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.LHC DETAILS" text="LHC"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Master" text="Master"/></a></li>
                <li class=""><spring:message code="hrms.label.LHC" text="LHC"/></li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <form name="lhc"  method="post" enctype="multipart/form-data"  >
                    <%@ include file="/content/common/message.jsp" %>
                    
                    <center><b><font size="4">Lorry Hire Challan</font></b></center>
                    <br/>
                    <table class="table table-info mb30 table-hover" id="table" >                     
                        <thead>
                            <tr height="30">
                                <th>Docket No</th>                     
                                <th>Vehicle No</th>                     
                                <th>Vehicle Type</th>
<!--                                <th>Driver</th>
                                <th>Driver License</th>-->
                                <th>Mobile</th>
                                <th>Agreed Rate</th>                                
                                <th>Insurance</th>
                                <th>Insurance Exp.Date</th>
                                <th>FC</th>
                                <th>FC Exp.Date</th>
                                <th>Permit</th>
                                <th>Permit Exp.Date</th>
                                <th>RoadTax</th>                                
                                <th>RoadTax Exp.Date</th>                                
                                <!--<th>Active</th>-->                                
                            </tr>
                        </thead>
                        <tbody>
                            <% int sno = 0;%>
                            <% int sno1 = 1;%>
                            <c:if test = "${LHCDetails != null}">
                                <c:forEach items="${LHCDetails}" var="lhc">
                                    <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                                    %>
                                    <tr>
                                        <td class="<%=className%>"  align="left"> <c:out value="${lhc.lHCNo}" /></td>
                                        <td class="<%=className%>"  align="left"> <c:out value="${lhc.vehicleNo}" /></td>
                                        <td class="<%=className%>"  align="left"> <c:out value="${lhc.vehicleTypeName}" /></td>
                                        
                                        <%--<c:out value="${lhc.driverName}" />
                                        
                                        <c:if test = "${lhc.licenseNo != null}">                                        
                                        <c:out value="${lhc.licenseNo}" />                                        
                                        </c:if>
                                        <c:if test = "${lhc.license == null}">                                        
                                        </c:if>--%>
                                        
                                        <td class="<%=className%>"  align="left"> <c:out value="${lhc.driverMobile}" /></td>
                                        <td class="<%=className%>"  align="left"> <c:out value="${lhc.additionalCost}" /></td>
                                        
                                        <c:if test = "${lhc.insurance != null}">
                                        <td class="<%=className%>"  align="left"> 
                                                <a href="LHCFiles/Files/<c:out value="${lhc.insurance}" />" target="_blank" >Insurance</a>
                                        </td>
                                        
                                        </c:if>
                                        <c:if test = "${lhc.insurance == null}">
                                            <td> - </td>
                                        
                                        </c:if>
                                        
                                        <c:if test = "${lhc.insuranceDate != null}">
                                        
                                        <td class="<%=className%>"  align="left"> <c:out value="${lhc.insuranceDate}" /></td>
                                        </c:if>
                                        <c:if test = "${lhc.insuranceDate == null}">                                        
                                            <td> - </td>
                                        </c:if>
                                        
                                            <c:if test = "${lhc.fC != null}">
                                            <td class="<%=className%>"  align="left"> 
                                            <a href="LHCFiles/Files/<c:out value="${lhc.fC}" />" target="_blank">FC</a> 
                                            </td>
                                            
                                            </c:if>
                                            <c:if test = "${lhc.fC == null}">
                                            <td> - </td>
                                            
                                            </c:if>
                                            <c:if test = "${lhc.fcDate != null}">
                                            
                                            <td class="<%=className%>"  align="left"> <c:out value="${lhc.fcDate}" /></td>
                                            </c:if>
                                            <c:if test = "${lhc.fcDate == null}">
                                            
                                            <td> - </td>
                                            </c:if>
                                        
                                            <c:if test = "${lhc.permit != null}">
                                            <td class="<%=className%>"  align="left"> 
                                            <a href="LHCFiles/Files/<c:out value="${lhc.permit}" />" target="_blank">Permit</a>                                                 
                                            </td>
                                            
                                            </c:if>
                                            
                                            <c:if test = "${lhc.permit == null}">
                                            <td> - </td>
                                            
                                            </c:if>
                                            
                                            <c:if test = "${lhc.permitDate != null}">

                                            <td class="<%=className%>"  align="left"> <c:out value="${lhc.permitDate}" /></td>
                                            
                                            </c:if>
                                            
                                            <c:if test = "${lhc.permitDate == null}">
                                            <td> - </td>
                                            
                                            </c:if>
                                        
                                            <c:if test = "${lhc.roadTax != null}">
                                            <td class="<%=className%>"  align="left"> 
                                            <a href="LHCFiles/Files/<c:out value="${lhc.roadTax}" />" target="_blank">RoadTax</a>                                                 
                                            </td>
                                            </c:if>                                            
                                            <c:if test = "${lhc.roadTax == null}">
                                            <td> - </td>
                                            
                                            </c:if>
                                            
                                            <c:if test = "${lhc.roadTaxDate != null}">
                                            
                                            <td class="<%=className%>"  align="left"> <c:out value="${lhc.roadTaxDate}" /></td>
                                            </c:if>                                            
                                            <c:if test = "${lhc.roadTaxDate == null}">
                                            <td> - </td>
                                            </c:if>
                                        
<!--                                         <td class="<%=className%>">
                                            <select name="activeInd" id="activeInd" style="height:20px;width:90px;">
                                                <c:if test="${lhc.activeInd == 'Y'}" >
                                                    <option value="Y" selected>Yes</option>
                                                    <option value="N">No</option>
                                                </c:if>
                                                <c:if test="${lhc.activeInd == 'N'}" >
                                                    <option value="N" selected>No</option>
                                                    <option value="Y" >Yes</option>
                                                </c:if>
                                            </select>
                                        </td>-->
                                        
                                    </tr>
                                    <%sno1++;%>
                                </c:forEach>
                            </tbody>
                        </c:if>
                    </table>
                    <input type="hidden" name="count" id="count" value="<%=sno%>" />
                    <br>
                    <br>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>
                </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>