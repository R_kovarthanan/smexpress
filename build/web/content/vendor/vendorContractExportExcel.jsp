<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>  
        <style> table, td {border:1px solid black} table {border-collapse:collapse}</style>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
              String menuPath = "Vendor >> vendor list";
              request.setAttribute("menuPath", menuPath);
//              String dateval = request.getParameter("dateval");
//              String active = request.getParameter("active");
//              String type = request.getParameter("type");
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                       Date dNow = new Date();
                       SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mm");
                       //System.out.println("Current Date: " + ft.format(dNow));
                       String curDate = ft.format(dNow);
                       String expFile = "VendorContract-Hire-" + curDate + ".xls";

                       String fileName = "attachment;filename=" + expFile;
                       response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                       response.setHeader("Content-disposition", fileName);
            %>



            <br>
            <br>
            <br>

            <br><br>

            <br>
            <br>
            <c:if test="${hireList != null}">
                <table align="center" border="0" id="table" class="sortable" style="width:100%" >
                    <tr style="height: 40px;" id="tableDesingTH">
                        <th align="center">S.No</th>
                        <th align="center">Vendor Name</th>
                        <th align="center">From Date</th>
                        <th align="center">To Date</th>
                        <th align="center">Vehicle Type</th>
                        <th align="center">Route</th>
                        <th align="center">Load Type</th>
                        <th align="center">Rate</th>
                        <td align="center">Advance Type</td>
                        <td align="center">Advance Value</td>
                        <td align="center">Initial Advance</td>
                        <td align="center">Remaining Amount</td>
                        <td align="center">Current Status</td>                                            
                    </tr>
                    <tbody>
                        <% int index = 1;%>
                        <c:forEach items="${hireList}" var="route">
                            <%
                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                    classText = "text2";
                                } else {
                                    classText = "text1";
                                }
                            %>
                            <tr height="30">
                                <td class="<%=classText%>"><%=index++%></td>
                                <td class="<%=classText%>"><c:out value="${route.vendorName}"/></td>
                                <td class="<%=classText%>"><c:out value="${route.startDate}"/></td>
                                <td class="<%=classText%>"><c:out value="${route.endDate}"/></td>
                                <td class="<%=classText%>"><c:out value="${route.vehicleTypeName}"/></td>
                                <td class="<%=classText%>"><c:out value="${route.originNameFullTruck}"/>-<c:out value="${route.point1Name}"/>-<c:out value="${route.destinationNameFullTruck}"/></td>
                                <td class="<%=classText%>"   >
                                    <c:if test="${route.loadTypeId =='1'}">
                                        Empty Trip
                                    </c:if>
                                    <c:if test="${route.loadTypeId =='2'}">
                                        Load Trip
                                    </c:if>
                                </td>

                                <td class="<%=classText%>"   ><c:out value="${route.additionalCost}"/></td>
                                <td class="<%=classText%>"><c:out value="${route.advanceTripMode}"/></td>
                                <td class="<%=classText%>"><c:out value="${route.modeTripRate}"/></td>
                                <td class="<%=classText%>"><c:out value="${route.initialTripAdvance}"/></td>
                                <td class="<%=classText%>"><c:out value="${route.endTripAdvance}"/></td>
                                <c:if test="${route.approvalStatus == 1}">
                                    <td class="<%=classText%>"   ><span class="label label-success">approved</span></td>
                                </c:if>
                                <c:if test="${route.approvalStatus == 2}">
                                    <td class="<%=classText%>"   ><span class="label label-warning">pending</span></td>
                                </c:if>
                                <c:if test="${route.approvalStatus == 3}">
                                    <td class="<%=classText%>"   ><span class="label label-danger">rejected</span></td>
                                </c:if>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>