<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });</script>
<script>
    function savePage(val) {
//        alert("hgfh");

        document.customerContract.action = "/throttle/saveEditVehicleTrailerContract.do";
        document.customerContract.submit();
    }

</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });</script>
<script>
    function checkVehicleTrailerAvalible() {
        var vehicleUnits =<c:out value="${vehicleUnitsDedicate}"/>;
        var trailorUnits =<c:out value="${trailorUnitsDedicate}"/>;
//        alert("vehicleUnits ==== " + vehicleUnits);
//        alert("trailorUnits ==== " + trailorUnits);
        if (vehicleUnits == "" && vehicleUnits == "0") {
            $("#vehicles1").hide();
            $("#vehicles").hide();
            $("#tabs").tabs("select", 1);
            $("ul#myTab li:nth-child(2)").addClass("active");
            $("#trailers").addClass("active");
////            $("#trailers").attr('class').indexOf("active");
////            $("#trailers1").attr('class').indexOf("active");
////            $("#trailers").addClass("active");
////            $('#tabs').tabs({selected: '2'}); 
////            $( ".selector" ).tabs( "option", "active" );
//            $( ".selector" ).tabs( '2', "active" );
        }
        if (trailorUnits == "" && trailorUnits == "0") {
            $("#trailers1").hide();
            $("#trailers").hide();
            $("#tabs").tabs("select", 2);
            $("ul#myTab li:nth-child(1)").addClass("active");
            $("#vehicles").addClass("active");
        }
    }
    function checkTrailerNos(val) {
//        alert("Sno" + val);
        var sno = val - 1;
        var trailerNoOld = document.getElementById("trailerNo" + sno).value;
        var trailerNoNew = document.getElementById("trailerNo" + val).value;
//        alert("trailerNoOld" + trailerNoOld);
//        alert("trailerNoNew" + trailerNoNew);
        if (trailerNoNew == trailerNoOld) {
            document.getElementById("checkTrailerNo").value = "1";
            document.getElementById("trailerNo" + val).value = "";
            document.getElementById("trailerNo" + val).focus();
            alert("Trailer No's already Exit's");
        } else {
            document.getElementById("checkTrailerNo").value = "0";
        }
    }

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Vendor</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor</a></li>
            <li class="active">Edit Fleet Vendor Contract</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="checkVehicleTrailerAvalible();">

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>


                <form name="customerContract"  method="post" >

                    <br>
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <input type="hidden" name="contractDedicateId" id="contractDedicateId"  value="<c:out value="${contractDedicateId}"/>"/>
                    <input type="hidden" name="trailerStatus" id="trailerStatus"  value=""/>
                    <input type="hidden" name="Status" id="Status"  value=""/>
                    <input type="hidden" name="checkVehicleRegNo" id="checkVehicleRegNo" value='' >


                    <div id="tabs">
                        <ul class="" id="myTab">
                            <li id="vehicles1"><a href="#fullTruck"><span>Vehicles</span></a></li>
                            <!--<li id="trailers1"><a href="#weightBreak"><span>Trailers </span></a></li>-->
                        </ul>


                        <div id="fullTruck">


                            <div id="routeFullTruck">

                                <c:if test= "${vehicles != null}">
                                    <table width="300px;" align="left" border="1" >
                                        <thead>
                                            <tr id="tableDesingTD" height="30">
                                                <th>Vehicle Type Name's</th>
                                                <th>Vehicle Unit's</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><c:out value="${vehicleTypeDedicate}"/></td>
                                                <td><c:out value="${vehicleUnitsDedicate}"/></td>
                                        <input type="hidden" name="vehicleTypeId" id="vehicleTypeId" value="<c:out value="${vehicleTypeId}"/>" class="textbox">
                                        <input type="hidden" name="typeId" id="typeId" value="<c:out value="${vehicleTypeId}"/>"
                                               </tr>
                                        </tbody>
                                    </table>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <table width="990px;" align="center" border="1" id="table" >
                                        <thead>
                                            <tr id="tableDesingTD" height="30">
                                                <th align="center">S.No</th>
                                                <th align="center">Vehicle RegNo</th>
                                                <th align="center">Agreed Date</th>
                                                <th align="center">MFR</th>
                                                <th align="center">Model</th>
                                                <th align="center">Remarks</th>
                                                <th align="center">Active Status</th>
                                                <th align="center">Replacement</th>
                                            </tr> 
                                        </thead>
                                        <tbody>
                                            <% int index = 1;%>
                                            <c:forEach items="${vehicles}" var="vehi">
                                                <%
                                                    String classText = "";
                                                    int oddEven = index % 1;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                                
                                                %>
                                                <tr>
                                                    <td ><%=index%></td>


                                            <input type="hidden" name="contractId" id="contractId<%=index%>" value="<c:out value="${vehi.contractId}"/>"/>
                                            <input type="hidden" name="ownership" id="ownership" value="2"/>



                                            <input type="hidden" name="configureIds" id="configureIds<%=index%>" value="" />
                                            <td><input type="hidden" name="replaceVehicle" id="replaceVehicle<%=index%>"  value="<c:out value="${vehi.vehicleId}"/>" />
                                                <input type="text" name="vehicleRegNo" id="vehicleRegNo<%=index%>"  value="<c:out value="${vehi.vehicleRegNo}"/>" readOnly/></td>
                                            <td ><input type="text" name="agreedDate" id="agreedDate<%=index%>" class="datepicker" value="<c:out value="${vehi.agreedDate}"/>"/></td>
                                            <td>
                                                <select type="text" name="mfr" id="mfr<%=index%>" onChange="ajaxData('<%=index%>', this.value);">
                                                    <option value="0">--Select--</option>
                                                    <c:if test="${MfrList != null}">
                                                        <c:forEach items="${MfrList}" var="Dept">
                                                            <c:if test="${vehi.mfr == Dept.mfrId}">
                                                                <option value="<c:out value="${Dept.mfrId}"/>" selected><c:out value="${Dept.mfrName}"/></option>
                                                            </c:if>
                                                            <c:if test="${vehi.mfr != Dept.mfrId}">
                                                                <option value="<c:out value="${Dept.mfrId}"/>"><c:out value="${Dept.mfrName}"/></option>
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:if>
                                                </select>
                                            </td>

                                            <td >
                                                <select type="text" name="model" id="model<%=index%>">
                                                    <option value="0">--Select-<%=index%>-</option>
                                                    <c:if test="${modelList != null}">
                                                        <c:forEach items="${modelList}" var="model">
                                                            <c:if test="${vehi.model == model.modelId}">
                                                                <option value="<c:out value="${model.modelId}"/>" selected=""><c:out value="${model.modelName}"/></option>
                                                            </c:if>
                                                            <c:if test="${vehi.model != model.modelId}">
                                                                <option value="<c:out value="${model.modelId}"/>"><c:out value="${model.modelName}"/></option>
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:if>
                                                </select></td>
                                            <td   ><input type="text" name="remarks" id="remarks"  value="<c:out value="${vehi.remarks}"/>"/></td>

                                            <td>
                                                <!--<select name="activeIndVehicle" id="activeIndVehicle<%=index%>" onchange="activeReplace(this.value, '<c:out value="${vehi.vehicleId}"/>', '<c:out value="${vehi.vehicleRegNo}"/>', '<c:out value="${vehi.agreedDate}"/>',<%=index%>);">-->
                                                <select name="activeIndVehicle" id="activeIndVehicle<%=index%>" >

                                                    <c:if test="${vehi.activeInd == 'Y'}" >
                                                        <option value="Y" selected>Active</option>
                                                        <option value="N">In-Active</option>
                                                    </c:if>
                                                    <c:if test="${vehi.activeInd == 'N'}" >
                                                        <option value="N" selected>In-Active</option>
                                                        <option value="Y" >Active</option>
                                                    </c:if>
                                                </select>
                                                <input type="hidden" name="activeInd" id="activeInd<%=index%>" value="<c:out value="${vehi.activeInd}"/>" />
                                                <input type="hidden" name="configureId" id="configureId<%=index%>" value="<c:out value="${vehi.configureId}"/>" />
                                                <!--<input type="text" name="replacementVehicleId" id="replacementVehicleId<%=index%>" value="" />-->

                                                <script type="text/javascript">

                                                    function setActiveIndRate(sno) {
//                                                        alert(sno)
                                                        document.getElementById("activeInd" + sno).value = document.getElementById("activeIndVehicle" + sno).value;
                                                    }
                                                </script>
                                                <script type="text/javascript">

                                                    function setReplacementStatus(sno) {
                                                        if (document.getElementById("activeInd" + sno).checked == true) {
                                                            document.getElementById("newVehicleId" + sno).value = document.getElementById("replaceVehicle" + sno).value;
                                                        } else {
                                                            document.getElementById("replacementVehicleId" + sno).value = document.getElementById("replaceVehicle" + sno).value;
                                                        }
                                                    }
                                                </script>

                                            </td>
                                            <input type="hidden" name="rowCounted" id="rowCounted" value=""/>
                                            <td>
                                                <input type="hidden" name="existVehicle" id="existVehicle<%=index%>"  value="" />
                                                <input type="hidden" name="inDate" id="inDate<%=index%>" class="textbox" value=""/>
                                                <input type="hidden" name="outDate" id="outDate<%=index%>" class="textbox" value=""/>
                                                <input type="hidden" name="replacementStatus" id="replacementStatus<%=index%>" class="textbox" value=""/>
                                                <!--<input type="hidden" name="existVehicleId" id="existVehicleId<%=index%>" class="textbox" value=""/>-->
                                                <c:if test="${vehi.activeInd == 'Y'}">

                                                    <a href="#" style="display:block" id="replacement<%=index%>" name="replacement" value='1' onclick="addReplace(1, '<c:out value="${vehi.vehicleId}"/>', '<c:out value="${vehi.vehicleRegNo}"/>', '<c:out value="${vehi.agreedDate}"/>', '<c:out value="${vehi.configureId}"/>',<%=index%>);">Replace</a>
                                                </c:if>
                                                <c:if test="${vehi.outDate != null && vehi.activeInd == 'N'}">

                                                    <a href="#" style="display:block" id="replacement<%=index%>" name="replacement" value="2" onclick="activeReplace(this.value, '<c:out value="${vehi.vehicleId}"/>', '<c:out value="${vehi.vehicleRegNo}"/>', '<c:out value="${vehi.agreedDate}"/>', '<c:out value="${vehi.configureId}"/>',<%=index%>, 2);
                                                            ajaxExistingVehicle('<c:out value="${vehi.vehicleId}"/>',<%=index%>);"><font color="red">Replace Existing vehicle</font></a>
                                                    </c:if>

                                                </tr>
                                                <%index++;%>
                                            </c:forEach>

                                            </tbody>
                                    </table>
                                    <script>

                                        function ajaxExistingVehicle(vehicleId, sno) {
//                                            alert(sno)
//                                            alert(vehicleId)
                                            var rowCnts = <%=index%>;
//                                            alert(rowCnts)
                                            $.ajax({
                                                url: "/throttle/getReplaceVehicle.do",
                                                dataType: 'json',
                                                data: {
                                                    replaceVehicle: vehicleId
                                                },
                                                success: function(data) {
                                                    $.each(data, function(i, data) {
                                                        $('#existingVehicle' + rowCnts).val(data.Name)
                                                        checkExistVehicle1(data.Name, rowCnts);
//                                                        $('#existingVehicle'+rowCnts).append(
//                                                                $('<option style="width:150px"></option>').val(data.Name).html(data.Name)
//                                                                )
                                                    });
                                                }
                                            });

                                        }


                                        function activeReplace(val, vehicleId, regNo, agreedDate, configureId, sno, status) {
//                                            alert(status)
                                            var vehicleUnits =<c:out value="${vehicleUnitsDedicate}"/>;
                                            var count1 = 1;
                                            var count;
                                            var activeIndVehicle = document.getElementsByName("activeIndVehicle");
                                            for (var i = 0; i < activeIndVehicle.length; i++) {
                                                if (activeIndVehicle[i].value == 'Y') {
                                                    count = count1++;
                                                }
                                            }
//                                            alert(count)
//                                            alert(vehicleUnits)
                                            var table = document.getElementById("table");
                                            var rowCount = table.rows.length;
                                            var active = 1;
//                                            if (val == 'Y') {
                                            var row = table.insertRow(rowCount);
                                            var cell1 = row.insertCell(0);
                                            var cell2 = row.insertCell(1);
                                            var cell3 = row.insertCell(2);
                                            var cell4 = row.insertCell(3);
                                            var cell5 = row.insertCell(4);
                                            var cell6 = row.insertCell(5);
                                            var cell7 = row.insertCell(6);
                                            var cell8 = row.insertCell(7);

                                            cell1.innerHTML = cell1.innerHTML + '' + rowCount + '</td>';
                                            cell2.innerHTML = cell2.innerHTML + '<td><input type="hidden" name="replaceVehicle" id="replaceVehicle' + rowCount + '" value="' + vehicleId + '" ><input type="text" class="textbox" name="vehicleRegNo" id="vehicleRegNo' + rowCount + '" value="' + regNo + '" size="10" maxlength="11" style="width:180px;" readonly></td>';
                                            cell3.innerHTML = cell3.innerHTML + ' <td><input type="text" class="datepicker" name="agreedDate" id="agreedDate' + rowCount + '" value=""/></td>';
                                            cell4.innerHTML = cell4.innerHTML + '<td> <select type="text" name="mfr" id="mfr' + rowCount + '" onChange="ajaxData(' + rowCount + ',this.value);"><option value="0">--Select--</option><c:if test="${MfrList != null}"><c:forEach items="${MfrList}" var="Dept"><c:if test="${vehi.mfr == Dept.mfrId}"><option value="<c:out value="${Dept.mfrId}"/>" selected><c:out value="${Dept.mfrName}"/></option></c:if><c:if test="${vehi.mfr != Dept.mfrId}"><option value="<c:out value="${Dept.mfrId}"/>"><c:out value="${Dept.mfrName}"/></option></c:if></c:forEach></c:if></select></td>';
                                            cell5.innerHTML = cell5.innerHTML + '<td ><select type="text" name="model" id="model' + rowCount + '"><option value="0">--Select--</option><c:if test="${modelList != null}"><c:forEach items="${modelList}" var="model"><c:if test="${vehi.model == model.modelId}"><option value="<c:out value="${model.modelId}"/>" selected=""><c:out value="${model.modelName}"/></option></c:if><c:if test="${vehi.model != model.modelId}"><option value="<c:out value="${model.modelId}"/>"><c:out value="${model.modelName}"/></option></c:if></c:forEach></c:if></select></td>';
                                            cell6.innerHTML = cell6.innerHTML + '<td><input name="remarks" id="remarks' + rowCount + '"  type="text" ></td>';
                                            cell7.innerHTML = cell7.innerHTML + '<td><select name="activeIndVehicle" id="activeIndVehicle' + rowCount + '" onchange="setActiveIndRate(' + rowCount + ');"><option value="Y" >Active</option><option value="N">In-Active</option></select></td>';
                                            cell8.innerHTML = cell8.innerHTML + '<td><input type="text" placeholder="please enter vehicle No" name="existingVehicle" id="existingVehicle' + rowCount + '" value="" onchange="checkExistVehicle1(this.value,' + rowCount + ');" readonly><input type="hidden" name="existVehicle" id="existVehicle' + rowCount + '" value="" ><input type="hidden" name="inDate" id="inDate' + rowCount + '" value="" ></td><input type="hidden" name="configureId" id="configureId' + rowCount + '" value="" ><input type="hidden" name="configureIds" id="configureIds' + rowCount + '" value="" ><input type="hidden" name="replacementStatus" id="replacementStatus' + rowCount + '" value="' + status + '" ></td>';
                                            document.getElementById('rowCounted').value = rowCount;
                                            $(".datepicker").datepicker({dateFormat: "dd-mm-yy"});
                                            checkExistVehicle(val, sno);

//                                            } else {
//                                                alert("Vehicle Quantity exceeds the limit for replace");
//                                            }
                                        }

                                        function checkExistVehicle1(val, sno) {
//                                            alert(val)
                                            var count = 0;
                                            var replaceVehicle = document.getElementsByName("replaceVehicle");
                                            var regNo = document.getElementsByName("vehicleRegNo");
                                            var activeIndVehicle = document.getElementsByName("activeIndVehicle");
                                            var configureId = document.getElementsByName("configureId");
                                            var agreedDate = document.getElementsByName("agreedDate");
                                            var outDate = document.getElementsByName("outDate");
                                            var activeInd = document.getElementsByName("activeInd");
                                            for (var i = 0; i < regNo.length; i++) {
                                                if (val == regNo[i].value && activeIndVehicle[i].value == 'Y') {
                                                    document.getElementById("existingVehicle" + sno).value = val;
                                                    document.getElementById("existVehicle" + sno).value = replaceVehicle[i].value;
                                                    document.getElementById("inDate" + sno).value = agreedDate[i].value;
                                                    document.getElementById("configureId" + sno).value = configureId[i].value;
                                                    activeIndVehicle[i].value = 'N';
                                                } else {
                                                    activeIndVehicle[i].value = activeInd[i].value;
                                                }
                                                if (val == regNo[i].value) {
                                                    count++;
                                                }

                                                if (count == 0) {
                                                    document.getElementById("existingVehicle" + sno).value = "";
                                                    document.getElementById("existVehicle" + sno).value = "";
                                                    document.getElementById("inDate" + sno).value = "";
                                                    document.getElementById("configureId" + sno).value = "";
                                                }
                                            }

                                        }
                                        function checkExistVehicle(val, sno) {
                                            var regNo = document.getElementsByName("vehicleRegNo");
                                            var activeInd = document.getElementsByName("activeIndVehicle");
                                            var configureId = document.getElementsByName("configureId");
                                            var count = regNo.length;
                                            for (var i = 0; i < regNo.length; i++) {
                                                if (regNo[i].value == document.getElementById("vehicleRegNo" + sno).value) {
                                                    if (activeInd[i].value != document.getElementById("activeIndVehicle" + sno).value) {
                                                        document.getElementById("agreedDate" + count).value = document.getElementById("agreedDate" + sno).value;
                                                        document.getElementById("configureIds" + count).value = document.getElementById("configureId" + sno).value;
                                                        document.getElementById("mfr" + count).value = document.getElementById("mfr" + sno).value;
                                                        document.getElementById("model" + count).value = document.getElementById("model" + sno).value;
                                                    }
                                                }

                                            }



                                        }
                                        function addReplace(status, vehicleId, regNo, agreedDate, configureId, sno) {
//                                           alert(status)
                                            var vehicleUnits =<c:out value="${vehicleUnitsDedicate}"/>;
                                            var table = document.getElementById("table");
                                            var rowCount = table.rows.length;
                                            var activeIndVehicle = document.getElementsByName("activeIndVehicle");
                                            var count1 = 1;
                                            var count;
                                            var vehicleUnits1;
                                            var vehicleRegNo = document.getElementById("vehicleRegNo" + sno).value;
                                            document.getElementById("activeIndVehicle" + sno).value = 'N';
                                            document.getElementById("replacement" + sno).style.display = "none";
                                            for (var i = 0; i < activeIndVehicle.length; i++) {
                                                if (activeIndVehicle[i].value == 'N') {
                                                    count = count1++;
                                                    vehicleUnits1 = vehicleUnits++;
                                                }
                                            }
//                                            alert(count)
//                                                 alert(vehicleUnits1)
                                            if (count <= vehicleUnits1) {
                                                var row = table.insertRow(rowCount);
                                                var cell1 = row.insertCell(0);
                                                var cell2 = row.insertCell(1);
                                                var cell3 = row.insertCell(2);
                                                var cell4 = row.insertCell(3);
                                                var cell5 = row.insertCell(4);
                                                var cell6 = row.insertCell(5);
                                                var cell7 = row.insertCell(6);
                                                var cell8 = row.insertCell(7);

                                                cell1.innerHTML = cell1.innerHTML + '' + rowCount + '</td>';
                                                cell2.innerHTML = cell2.innerHTML + '<td><input type="hidden" name="replaceVehicle" id="replaceVehicle' + rowCount + '" value="" ><input type="text" class="textbox" name="vehicleRegNo" id="vehicleRegNo' + rowCount + '" maxlength="11" value="" onChange="getVehicleDetails(' + rowCount + ');checkVehicleRegNos(' + rowCount + ')" size="10" style="width:180px;"></td>';
                                                cell3.innerHTML = cell3.innerHTML + ' <td><input type="text" class="datepicker" name="agreedDate" id="agreedDate' + rowCount + '" value=""/></td>';
                                                cell4.innerHTML = cell4.innerHTML + '<td> <select type="text" name="mfr" id="mfr' + rowCount + '" onChange="ajaxData(' + rowCount + ',this.value);"><option value="0">--Select--</option><c:if test="${MfrList != null}"><c:forEach items="${MfrList}" var="Dept"><c:if test="${vehi.mfr == Dept.mfrId}"><option value="<c:out value="${Dept.mfrId}"/>" selected><c:out value="${Dept.mfrName}"/></option></c:if><c:if test="${vehi.mfr != Dept.mfrId}"><option value="<c:out value="${Dept.mfrId}"/>"><c:out value="${Dept.mfrName}"/></option></c:if></c:forEach></c:if></select></td>';
                                                cell5.innerHTML = cell5.innerHTML + '<td ><select type="text" name="model" id="model' + rowCount + '"><option value="0">--Select--</option><c:if test="${modelList != null}"><c:forEach items="${modelList}" var="model"><c:if test="${vehi.model == model.modelId}"><option value="<c:out value="${model.modelId}"/>" selected=""><c:out value="${model.modelName}"/></option></c:if><c:if test="${vehi.model != model.modelId}"><option value="<c:out value="${model.modelId}"/>"><c:out value="${model.modelName}"/></option></c:if></c:forEach></c:if></select></td>';
                                                cell6.innerHTML = cell6.innerHTML + '<td><input name="remarks" id="remarks' + rowCount + '"  type="text" ></td>';
                                                cell7.innerHTML = cell7.innerHTML + '<td><select name="activeIndVehicle" id="activeIndVehicle' + rowCount + '" onchange="setActiveIndRate(' + rowCount + ');"><option value="Y"  >Active</option><option value="N"  >In Active</option></select></td>';
                                                cell8.innerHTML = cell8.innerHTML + '<td><input type="hidden" name="existVehicle" id="existVehicle' + rowCount + '" value="' + vehicleId + '" ><input type="hidden" name="inDate" id="inDate' + rowCount + '" value="' + agreedDate + '" ><input type="hidden" name="configureId" id="configureId' + rowCount + '" value="' + configureId + '" ><input type="hidden" name="configureIds" id="configureIds"' + rowCount + '" value="" ><font color="red" size="2">' + regNo + '</font><input type="hidden" name="replacementStatus" id="replacementStatus' + rowCount + '" value="' + status + '" ></td>';
                                                document.getElementById('rowCounted').value = rowCount;
                                                $(".datepicker").datepicker({dateFormat: "dd-mm-yy"});


                                            } else {
                                                alert("Vehicle Quantity exceeds the limit for replace");
                                            }
                                        }



                                        var httpReq;
                                        var temp = "";
                                        function ajaxData(val, paramVal)
                                        {
                                            //                                    alert("paramVal = "+paramVal)
                                            //                                    var url = "/throttle/getModel.do?mfr=" + document.getElementById("mfr"+val).value;
                                            var url = "/throttle/getModel.do?mfr=" + paramVal;

                                            //                                    alert("check ="+document.vehicleVendorContract.mfr.value)
                                            //                                    var url = "/throttle/getModel.do?mfr=" + document.vehicleVendorContract.mfr.value;
                                            if (window.ActiveXObject)
                                            {
                                                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                                            }
                                            else if (window.XMLHttpRequest)
                                            {
                                                httpReq = new XMLHttpRequest();
                                            }
                                            httpReq.open("GET", url, true);
                                            httpReq.onreadystatechange = function() {
                                                processAjax(val);
                                            };
                                            httpReq.send(null);
                                        }


                                        function processAjax(val)
                                        {
                                            if (httpReq.readyState == 4)
                                            {
                                                if (httpReq.status == 200)
                                                {
                                                    temp = httpReq.responseText.valueOf();

                                                    //                                          setOptions(temp, document.vehicleVendorContract.model);
                                                    setOptions1(temp, document.getElementById("model" + val));

                                                }
                                                else
                                                {
                                                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                                                }
                                            }
                                        }
                                        function deleteRow(val, v1) {
                                            var loadCnt = val;
                                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                            if ($('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size() > 2) {
                                                $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().remove();
                                                loadCnt = loadCnt - 1;
                                            } else {
                                                alert('One row should be present in table');
                                            }
                                        }

                                        function setOptions1(text, variab) {
                                            //                                alert("setOptions on page")
                                            //                                alert("text = "+text+ "variab = "+variab)
                                            variab.options.length = 0;
                                            //                                alert("1")
                                            var option0 = new Option("--select--", '0');
                                            //                                alert("2")
                                            variab.options[0] = option0;
                                            //                                alert("3")


                                            if (text != "") {
                                                //                                    alert("inside the condition")
                                                var splt = text.split('~');
                                                var temp1;
                                                variab.options[0] = option0;
                                                for (var i = 0; i < splt.length; i++) {
                                                    //                                    alert("splt.length ="+splt.length)
                                                    //                                    alert("for loop ="+splt[i])
                                                    temp1 = splt[i].split('-');
                                                    option0 = new Option(temp1[1], temp1[0])
                                                    //alert("option0 ="+option0)
                                                    variab.options[i + 1] = option0;
                                                }
                                            }
                                        }

                                        function checkVehicleRegNos(val) {
//                                            alert("Sno" + val);
                                            var sno = val - 1;
                                            var vehicleRegNoOld = document.getElementById("vehicleRegNo" + sno).value;
                                            var vehicleRegNoNew = document.getElementById("vehicleRegNo" + val).value;
//        alert("vehicleRegNoOld" + vehicleRegNoOld);
//        alert("vehicleRegNoNew" + vehicleRegNoNew);
                                            if (vehicleRegNoNew == vehicleRegNoOld) {
                                                document.getElementById("checkVehicleRegNo").value = "1";
                                                document.getElementById("vehicleRegNo" + val).value = "";
                                                document.getElementById("vehicleRegNo" + val).focus();
                                                alert("vehicle RegNo's already Exit's");
                                            }
                                            else {
                                                document.getElementById("checkVehicleRegNo").value = "0";
                                            }
                                        }


                                        var httpRequest;
                                        function getVehicleDetails(val) {
//                                        alert("entered= " + val);
                                            //                alert(document.vehicleVendorContract.vehicleRegNo.value);
                                            var vehicleRegNo = document.getElementById("vehicleRegNo" + val).value;
                                            //alert("vehicleRegNo" + vehicleRegNo);
                                            if (vehicleRegNo != '') {
                                                var url = '/throttle/checkVehicleRegNoExists.do?vehicleRegNo=' + vehicleRegNo;
                                                //     alert("url----" + url);
                                                if (window.ActiveXObject)
                                                {
                                                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                                }
                                                else if (window.XMLHttpRequest)
                                                {
                                                    httpRequest = new XMLHttpRequest();
                                                }
                                                httpRequest.open("POST", url, true);
                                                httpRequest.onreadystatechange = function() {
                                                    //     alert("Hi I am Here");
                                                    go1(val);
                                                };
                                                httpRequest.send(null);
                                            }
                                        }


                                        function go1(val) {
                                            //alert("val" + val);
                                            // alert("hi");
                                            if (httpRequest.readyState == 4) {
                                                if (httpRequest.status == 200) {
                                                    var response = httpRequest.responseText;
                                                    var temp = response.split('-');
                                                    //     alert("hi");
                                                    if (response != "") {
                                                        alert('Vehicle Already Exists');
                                                        document.getElementById("Status").innerHTML = httpRequest.responseText.valueOf() + " Already Exists";
                                                        document.getElementById("vehicleRegNo" + val).value = "";
                                                        document.getElementById("vehicleRegNo" + val).focus();
                                                        document.getElementById("vehicleRegNo" + val).select();
                                                        document.vehicleVendorContract.regNoCheck.value = 'exists';
                                                    } else
                                                    {
                                                        document.vehicleVendorContract.regNoCheck.value = 'Notexists';
                                                        document.getElementById("Status").innerHTML = "";
                                                    }
                                                }
                                            }
                                        }

                                                </script>
                                                <br/>
                                                <center>
                                                    <a><input type="button" class="btn btn-info" value="AddNewRow" id="AddNewRowFullTruck" name="AddNewRow" onclick="addNewRowFullTruck1();"/></a>
                                                    <a  class="nexttab" href="#"><input type="button" class="btn btn-info" value="Next" name="Next" /></a></br>
                                                    <script>
                                                        var container = "";
                                                        function addNewRowFullTruck1() {
                                                            //                                   alert("HI I Am Here");
                                                            var vehicleCount = <c:out value="${vehicleUnitsDedicate}"/>;
                                                            //                                    alert("vehicleCount = " + vehicleCount);
//                                                            alert(<%=index%>)
                                                            var activeIndVehicle = document.getElementsByName("activeIndVehicle");
                                                            var count1 = 1;
                                                            var count;
                                                            for (var i = 0; i < activeIndVehicle.length; i++) {
                                                                if (activeIndVehicle[i].value == 'Y') {
                                                                    count = count1++;
                                                                }
                                                            }
//                                                            alert(count)
//                                                            alert(vehicleCount)
                                                            var rowCnts = <%=index%>;
//                                                            alert(rowCnts)
                                                            var cnt = 0;
                                                            var configureId = document.getElementsByName("configureIds");
                                                            for (var j = 0; j < configureId.length; j++) {
                                                                if (configureId[j].value != "") {
                                                                    cnt++;
                                                                }
                                                            }
//                                                             alert("count "+count)
                                                            if (cnt == 0) {
                                                                if (count < vehicleCount) {
                                                                    $("#AddNewRowFullTruck").hide();
                                                                    var iCnt = <%=index%>;
                                                                    var rowCnt = <%=index%> + 1;
                                                                    // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                                                    container = $($("#routeFullTruck")).css({
                                                                        padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                                                        borderTopColor: '#999', borderBottomColor: '#999',
                                                                        borderLeftColor: '#999', borderRightColor: '#999'
                                                                    });
                                                                    $(container).last().after('<table id="mainTableFullTruck" ><tr></td>\n\
                                                     <table class="contentsub" id="routeInfoDetailsFullTruck' + parseFloat(iCnt) + '" border="1"  width="73%">\n\
                                                     <tr><td>Sno</td><td>VehicleRegNo</td><td>Agreed Date</td><td>MFR</td><td>Model</td><td>Remarks</td><td>Select</td></tr>\n\
                                                     <tr>\n\
                                                     <td>' + iCnt + '</td>\n\
                                                     <input type="hidden" name="seatCapacity" id="seatCapacity" value="0.00" class="textbox">\n\
                                                     <td><input type="hidden" name="vehicleTypeIdcheck" id="vehicleTypeIdcheck' + iCnt + '" value="<c:out value="${vehicleTypeIdDedicate}"/>" class="textbox"><input type="hidden" name="contractId" id="contractId' + iCnt + '" value="" /><input type="hidden" name="replaceVehicle" id="replaceVehicle' + iCnt + '" value="" ><input type="text" name="vehicleRegNo" id="vehicleRegNo' + iCnt + '" value="" onChange="getVehicleDetails(' + iCnt + ');checkVehicleRegNos(' + iCnt + ')" /></td>\n\
                                                     <td><input type="text" name="agreedDate" id="agreedDate' + iCnt + '" class="datepicker" /></td>\n\
                                                      <td><select type="text" name="mfr" id="mfr' + iCnt + '" onChange="ajaxData(' + iCnt + ',this.value);"><option value="0">--Select--</option><c:if test="${MfrList != null}"><c:forEach items="${MfrList}" var="Dept"><option value="<c:out value="${Dept.mfrId}"/>"><c:out value="${Dept.mfrName}"/></option></c:forEach></c:if></select></td>\n\
                                                     <td><select type="text" name="model" id="model' + iCnt + '"><option value="0">---Select---</option></select></td>\n\
                                                     <td><input type="text" name="remarks" id="remarks' + iCnt + '" value="" /></td>\n\
                                                     <td><select name="activeIndVehicle" id="activeIndVehicle' + iCnt + '" onchange="setActiveIndRate(' + iCnt + ');"><option value="Y"  >Active</option><option value="N"  >In Active</option></select></td>\n\
                                                     <td><input type="hidden" name="existVehicle" id="existVehicle' + iCnt + '" value="" ><input type="hidden" name="inDate" id="inDate' + iCnt + '" value="" ><input type="hidden" name="configureId" id="configureId' + iCnt + '" value="" ><input type="hidden" name="configureIds" id="configureIds' + iCnt + '" value="" ><input type="hidden" name="replacementStatus" id="replacementStatus' + iCnt + '" value="" ></td>\n\
                                                     </tr></table>\n\
                                                     <table><tr>\n\
                                                     <td><input class="btn btn-info" type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + parseFloat(iCnt) + ')" />\n\
                                                     <input class="btn btn-info" type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRow(' + (parseFloat(iCnt)) + ')" /></td>\n\
                                                     </tr></table></td></tr></table><br><br>');
                                                                    $(".datepicker").datepicker({dateFormat: "dd-mm-yy"});

                                                                } else {
                                                                    alert("Vehicle Quantity exceeds the limit");
//                                                        }
                                                                }
                                                            } else if (count > vehicleCount) {
                                                                alert("Vehicle Quantity exceeds the limit");
                                                            } else {
                                                                alert("Please save Exist vehicle changes");
                                                                document.getElementById("existingVehicle" + rowCnts).focus();
                                                            }
                                                        }
                                                        //                                 PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                                        var divValue, values = '';
                                                        function GetTextValue() {
                                                            $(divValue).empty();
                                                            $(divValue).remove();
                                                            values = '';
                                                            $('.input').each(function() {
                                                                divValue = $(document.createElement('div')).css({
                                                                    padding: '5px', width: '200px'
                                                                });
                                                                values += this.value + '<br />'
                                                            });
                                                            $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                                            $('body').append(divValue);
                                                        }
                                                        var tempVar;
                                                        var loadCnt = 0;
                                                        var loadCnt1 = 0;
                                                        function addRow(val) {
                                                            //                                                alert(val)
                                                            var loadCnt = val;
                                                            var routeInfoSize = $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size();
                                                            var count = <c:out value="${vehicleUnitsDedicate}"/>;
                                                            routeInfoSize = routeInfoSize + val - 1;
                                                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                                            if (parseInt(count) >= parseInt(routeInfoSize)) {
                                                                $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSize + '</td>\n\
                                                         <td><input type="hidden" name="vehicleTypeIdcheck" id="vehicleTypeIdcheck' + routeInfoSize + '" value="<c:out value="${vehicleTypeIdDedicate}"/>" class="textbox"><input type="hidden" name="replaceVehicle" id="replaceVehicle' + routeInfoSize + '" value="" ><input type="text" name="vehicleRegNo" id="vehicleRegNo' + routeInfoSize + '" onChange="getVehicleDetails(' + routeInfoSize + ');checkVehicleRegNos(' + routeInfoSize + ')" ></td>\n\
                                                 <td><input type="text" name="agreedDate" id="agreedDate' + routeInfoSize + '" class="datepicker" /></td>\n\
                                                 <td><select type="text" name="mfr" id="mfr' + routeInfoSize + '" onChange="ajaxData(' + routeInfoSize + ',this.value);">\n\
                                                   <option value="0">--Select--</option><c:if test="${MfrList != null}"><c:forEach items="${MfrList}" var="Dept"><option value="<c:out value="${Dept.mfrId}"/>"><c:out value="${Dept.mfrName}"/></option></c:forEach></c:if></select></td>\n\
                                                  <td><select type="text" name="model" id="model' + routeInfoSize + '"><option value="0">---Select---</option></select></td>\n\
                                                <td><input type="text" name="remarks" id="remarks' + routeInfoSize + '" value="" /></td>\n\
                                                <td><select name="activeIndVehicle" id="activeIndVehicle' + routeInfoSize + '" onchange="setActiveIndRate(' + routeInfoSize + ');"><option value="Y"  >Active</option><option value="N"  >In Active</option></select></td>\n\
                                               <td><input type="hidden" name="existVehicle" id="existVehicle' + routeInfoSize + '" value="" ><input type="hidden" name="inDate" id="inDate' + routeInfoSize + '" value="" ><input type="hidden" name="configureId" id="configureId' + routeInfoSize + '" value="" ><input type="hidden" name="configureIds" id="configureIds' + routeInfoSize + '" value="" ><input type="hidden" name="replacementStatus" id="replacementStatus' + routeInfoSize + '" value="" ></td>\n\
                                                </tr>');
                                                            } else {
                                                                alert("Vehicle Quantity exceeds the limit");
                                                            }
                                                            $(".datepicker").datepicker({dateFormat: "dd-mm-yy"});
                                                        }
                                                        var httpReq;
                                                        var temp = "";
                                                        function getModelAndTonnage(val, str) {
                                                            $.ajax({
                                                                url: "/throttle/getModelForVendor.do",
                                                                dataType: "text",
                                                                data: {
                                                                    typeId: document.customerContract.typeId.value,
                                                                },
                                                                success: function(temp) {
                                                                    if (temp != '') {
                                                                        setOptions1(temp, document.getElementById("model" + val));
                                                                    } else {
                                                                        setOptions1(temp, document.getElementById("model" + val));
                                                                        alert('There is no model based on Vehicle Type Please add first');
                                                                    }
                                                                }
                                                            });
                                                            // for tonnage now
                                                            $.ajax({
                                                                url: "/throttle/getTonnage.do",
                                                                dataType: "text",
                                                                data: {
                                                                    typeId: document.customerContract.typeId.value
                                                                },
                                                                success: function(temp) {
                                                                    if (temp != '') {
                                                                        setOptions2(temp, document.getElementById("seatCapacity" + val));
                                                                    } else {
                                                                        alert('Tonnage is not there please set first in vehicle type');
                                                                    }
                                                                }
                                                            });
                                                        }
                                                        function setOptions2(text) {
                                                            t("text = " + text)
                                                            if (text != "") {
                                                                document.getElementById('seatCapacity').value = text;
                                                            }
                                                        }
                                                        // raja             
                                                        var httpReq;
                                                        var temp = "";
                                                        function ajaxData1(val, parmVal)
                                                        {
                                                            var url = "/throttle/getModelForVendor.do?typeId=" + document.customerContract.typeId.value;
                                                            if (window.ActiveXObject)
                                                            {
                                                                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                                                            }
                                                            else if (window.XMLHttpRequest)
                                                            {
                                                                httpReq = new XMLHttpRequest();
                                                            }
                                                            httpReq.open("GET", url, true);
                                                            httpReq.onreadystatechange = function() {
                                                                processAjax1(val);
                                                            };
                                                            httpReq.send(null);
                                                        }
                                                        function processAjax1(val)
                                                        {
                                                            if (httpReq.readyState == 4)
                                                            {
                                                                if (httpReq.status == 200)
                                                                {
                                                                    temp = httpReq.responseText.valueOf();
                                                                    if (temp == "") {
                                                                        alert("please Enter the MFR & Model in Master")
                                                                        setOptions1(temp, document.getElementById("model" + val));
                                                                    }
                                                                    else {
                                                                        setOptions1(temp, document.getElementById("model" + val));
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                                                                }
                                                            }
                                                        }

                                                        function setOptions1(text, variab) {
                                                            //                                     alert("setOptions on page")
                                                            variab.options.length = 0;
                                                            var option0 = new Option("--select--", '0');
                                                            variab.options[0] = option0;
                                                            if (text != "") {
                                                                var splt = text.split('~');
                                                                var temp1;
                                                                variab.options[0] = option0;
                                                                for (var i = 0; i < splt.length; i++) {
                                                                    temp1 = splt[i].split('-');
                                                                    option0 = new Option(temp1[1], temp1[0])
                                                                    //alert("option0 ="+option0)
                                                                    variab.options[i + 1] = option0;
                                                                }
                                                            }
                                                        }
                                                        // raja
                                                        function deleteRow(val) {
                                                            var loadCnt = val;
                                                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                                            if ($('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size() > 2) {
                                                                $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().remove();
                                                                loadCnt = loadCnt - 1;
                                                            } else {
                                                                alert('One row should be present in table');
                                                            }
                                                        }
                                                        var httpRequest;
                                                        function getVehicleDetails(val) {
                                                            var vehicleRegNo = document.getElementById("vehicleRegNo" + val).value;
                                                            if (vehicleRegNo != '') {
                                                                var url = '/throttle/checkVehicleRegNoExists.do?vehicleRegNo=' + vehicleRegNo;
                                                                if (window.ActiveXObject)
                                                                {
                                                                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                                                }
                                                                else if (window.XMLHttpRequest)
                                                                {
                                                                    httpRequest = new XMLHttpRequest();
                                                                }
                                                                httpRequest.open("POST", url, true);
                                                                httpRequest.onreadystatechange = function() {
                                                                    go1(val);
                                                                };
                                                                httpRequest.send(null);
                                                            }
                                                        }
                                                        function go1(val) {
                                                            if (httpRequest.readyState == 4) {
                                                                if (httpRequest.status == 200) {
                                                                    var response = httpRequest.responseText;
                                                                    var temp = response.split('-');
                                                                    if (response != "") {
                                                                        alert('Vehicle Already Exists');
                                                                        document.getElementById("vehicleRegNo" + val).value = "";
                                                                        //                                                document.vehicleVendorContract.regNoCheck.value = 'exists';
                                                                        document.getElementById("Status").innerHTML = httpRequest.responseText.valueOf() + " Already Exists";
                                                                        document.getElementById("vehicleRegNo" + val).focus();
                                                                        document.getElementById("vehicleRegNo" + val).select();
                                                                    } else
                                                                    {
                                                                        //                                                document.vehicleVendorContract.regNoCheck.value = 'Notexists';
                                                                        document.getElementById("Status").innerHTML = "";
                                                                    }
                                                                }
                                                            }
                                                        }
                                                </script>
                                            </center>
                                </c:if>

                            </div>
                        </div>

                        <div id="weightBreak" style="display:none;">

                            <div id="routeWeightBreak">

                                <c:if test="${trailerList != null}">
                                    <table border="1" id="table" >
                                        <thead>
                                            <tr class="contenthead">
                                                <th>Trailer Name's</th>
                                                <th>Trailer Unit's</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><c:out value="${trailorTypeDedicate}"/></td>
                                                <td><c:out value="${trailorUnitsDedicate}"/></td>
                                            </tr>
                                        </tbody>
                                        <input type="text" name="trailorTypeId" id="trailorTypeId" value="<c:out value="${trailorTypeIdDedicate}"/>" class="textbox">
                                    </table>
                                    <table align="center" border="0" id="table" class="sortable" style="width:1000px;" >
                                        <thead>
                                            <tr>
                                                <th align="center"><h3>S.No</h3></th>


                                        <th align="center"><h3>Trailer No</h3></th>
                                        <th align="center"><h3>Remarks</h3></th>
                                        <th align="center"><h3>Active Status</h3></th>

                                        </tr> 
                                        </thead>
                                        <tbody>
                                            <% int index1 = 1;%>
                                            <c:forEach items="${trailerList}" var="trailer">
                                                <%
                                                    String classText1 = "";
                                                    int oddEven1 = index1 % 2;
                                                    if (oddEven1 > 0) {
                                                        classText1 = "text2";
                                                    } else {
                                                        classText1 = "text1";
                                                    }
                                                %>
                                                <tr>
                                                    <td class="<%=classText1%>"  ><%=index1%></td>



                                                    <td class="<%=classText1%>"   ><input type="text" name="trailerNo" id="trailerNo<%=index1%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${trailer.trailerNo}"/>"/></td>
                                                    <td class="<%=classText1%>"   >
                                                        <input type="text" name="trailerRemarks" id="trailerRemarks<%=index1%>" onKeyPress="return;" value="<c:out value="${trailer.trailerRemarks}"/>"/></td>


                                                    <td class="<%=classText1%>"   >
                                                        <select name="activeIndTrailor" id="activeIndTrailor<%=index1%>" onchange="setActiveIndRate('<%=index1%>');">
                                                            <c:if test="${trailer.activeInd == 'Y'}" >
                                                                <option value="Y" selected>Active</option>
                                                                <option value="N">In-Active</option>
                                                            </c:if>
                                                            <c:if test="${trailer.activeInd == 'N'}" >
                                                                <option value="N" selected>In-Active</option>
                                                                <option value="Y" >Active</option>
                                                            </c:if>

                                                            //                                            <c:if test="${trailer.activeInd == 'Y'}">
                                                                //                                       
                                                                //                                            </c:if>
                                                            //                                            <c:if test="${trailer.activeInd == 'N'}">
                                                                //                                
                                                                //                                            </c:if>
                                                                //                                          </select>
                                                            <script type="text/javascript">
                                                                function setActiveIndRate(sno) {
                                                                    document.getElementById("activeIndTrailor1" + sno).value = document.getElementById("activeIndTrailor" + sno).value;
                                                                }
                                                            </script>

                                                        </td>
                                                    </tr>
                                                <%index1++;%>
                                            </c:forEach>
                                        </tbody>
                                    </table>

                                </div>
                                <center>
                                    <table>
                                        <tr>

                                            <td><a  href="#"><input type="button" class="btn btn-info" value="AddNewRow" id="AddNewRow"  name="AddNewRow" onclick="addNewRowtrailer('<%=index1%>');"/></a></td>    
                                            <td><a  class="pretab" href="#"><input type="button" class="btn btn-info" value="Previous" name="Previous" /></a></td>  
                                            <!--<a  class="pretab" href="#"><input type="button" class="button" value="Previous" name="Previous" /></a>-->
                                            <!--<td><a  href="#"><input type="button" class="button" value="Save" onclick="savePage(this.val);" /></a>  </td>-->    
                                        </tr>    
                                    </table>
                                </center>

                                <script>
                                    var container1 = "";
                                    function addNewRowtrailer(value) {
                                        //                                alert("Test" + value);
                                        var trailerCount = <c:out value="${trailorUnitsDedicate}"/>;
                                        //                                alert("trailerCount" + trailerCount);

                                        if (<%=index1%> <= trailerCount) {
                                            $("#AddNewRow").hide();
                                            var iCntWeightBreak = <%=index1%>;
                                            var rowCntWeightBreak = <%=index1%>;
                                            container1 = $($("#routeWeightBreak")).css({
                                                padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                                borderTopColor: '#999', borderBottomColor: '#999',
                                                borderLeftColor: '#999', borderRightColor: '#999'
                                            });
                                            $(container1).last().after('<table id="mainTableWeightBreak" ><tr></td>\n\
                                  </tr></tr>\n\
                                          <table  class="contenthead" id="routeDetailsWeightBreak' + iCntWeightBreak + '"  border="1">\n\
                                           <td>Sno</td>\n\
                                    <td>TrailerNo</td>\n\
                                    <td>Remarks</td>\n\
                                    </tr>\n\
                                    <tr><td>' + rowCntWeightBreak + '</td>\n\
                                    <td><input type="text" name="trailerNo" id="trailerNo' + iCntWeightBreak + '" value="" onchange="getTrailerDetails(' + iCntWeightBreak + ');checkTrailerNos(' + iCntWeightBreak + ')"/></td>\n\
                                    <td><input type="text" name="trailerRemarks" id="trailerRemarks' + iCntWeightBreak + '" value="" /></td>\n\
                                          <td><select name="activeIndTrailor" id="activeIndTrailor' + iCntWeightBreak + '"><option value="Y">Active</option><option value="N"  >In Active</option></select></td>\n\
                       <table><tr>\n\
                                            <td><input class="btn btn-info" type="button" name="addRouteDetailsWeightBreak" id="addRouteDetailsWeightBreak' + iCntWeightBreak + rowCntWeightBreak + '" value="Add" onclick="addRowWeightBreak(' + iCntWeightBreak + ')" />\n\
                                            <input class="btn btn-info" type="button" name="removeRouteDetailsWeightBreak" id="removeRouteDetailsWeightBreak' + iCntWeightBreak + rowCntWeightBreak + '" value="Remove"  onclick="deleteRowWeightBreak(' + iCntWeightBreak + ')" /></td>\n\
                                            </tr></table></td></tr></table><br><br>');
                                            callOriginAjaxWeightBreak(iCntWeightBreak);
                                            callDestinationAjaxWeightBreak(iCntWeightBreak);
                                        } else {
                                            alert("Trailer Quantity Exceeded");
                                        }

                                    }
                                </script>
                                <script>
                                    // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                    var divValue, values = '';
                                    function GetTextValue() {
                                        $(divValue).empty();
                                        $(divValue).remove();
                                        values = '';
                                        $('.input').each(function() {
                                            divValue = $(document.createElement('div')).css({
                                                padding: '5px', width: '200px'
                                            });
                                            values += this.value + '<br />'
                                        });
                                        $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                        $('body').append(divValue);
                                    }

                                    function addRowWeightBreak(val) {
                                        var trailerCount1 = <c:out value="${trailorUnitsDedicate}"/>;
                                        var loadCnt = val;
                                        var routeInfoSize = $('#routeDetailsWeightBreak' + loadCnt + ' tr').size();
                                        routeInfoSize = routeInfoSize + val - 1;
                                        //                                alert("routeInfoSize"+routeInfoSize);
                                        if (trailerCount1 >= routeInfoSize) {
                                            var addRouteDetails = "addRouteDetailsWeightBreak" + loadCnt;
                                            var routeInfoDetails = "routeDetailsWeightBreak" + loadCnt;
                                            $('#routeDetailsWeightBreak' + val + ' tr').last().after('<tr><td>' + routeInfoSize + '</td><td><input type="text" name="trailerNo" id="trailerNo' + routeInfoSize + '" value="" onchange="getTrailerDetails(' + routeInfoSize + ');"/></td><td><input type="text" name="trailerRemarks" id="trailerRemarks' + routeInfoSize + '" value="" /></td>\n\
                                    <td><select name="activeIndTrailor" id="activeIndTrailor' + routeInfoSize + '"><option value="Y">Active</option><option value="N"  >In Active</option></select></td>\n\
                                    </tr>');
                                            loadCnt++;
                                        } else {
                                            alert("Trailer Quantity Exceeded");
                                        }
                                    }
                                    function deleteRowWeightBreak(val) {
                                        // alert(val)
                                        var loadCnt = val;
                                        var addRouteDetails = "addRouteDetailsWeightBreak" + loadCnt;
                                        var routeInfoDetails = "routeDetailsWeightBreak" + loadCnt;
                                        if ($('#routeDetailsWeightBreak' + loadCnt + ' tr').size() > 2) {
                                            $('#routeDetailsWeightBreak' + loadCnt + ' tr').last().remove();
                                            loadCnt = loadCnt - 1;
                                        } else {
                                            alert('One row should be present in table');
                                        }
                                    }


                                    var httpRequest;
                                    function getTrailerDetails(val) {
                                        //                                alert("entered= " + val);
                                        //                alert(document.vehicleVendorContract.vehicleRegNo.value);
                                        var trailerNo = document.getElementById("trailerNo" + val).value;
                                        //                                alert("trailerNo" + trailerNo);
                                        if (trailerNo != '') {
                                            var url = '/throttle/checkTrailerExists.do?trailerNo=' + trailerNo;
                                            //alert("url----"+url);
                                            if (window.ActiveXObject)
                                            {
                                                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                            }
                                            else if (window.XMLHttpRequest)
                                            {
                                                httpRequest = new XMLHttpRequest();
                                            }
                                            httpRequest.open("POST", url, true);
                                            httpRequest.onreadystatechange = function() {
                                                go2(val);
                                                //                        alert("url----"+url);
                                            };
                                            httpRequest.send(null);
                                        }
                                    }


                                    function go2(val) {
                                        //                                alert("hi   =====  " + val);
                                        if (httpRequest.readyState == 4) {
                                            if (httpRequest.status == 200) {
                                                var response = httpRequest.responseText;
                                                var temp = response.split('-');
                                                //                         alert("hi111"+response);
                                                if (response != "") {
                                                    alert('Trailer Already Exists');
                                                    document.getElementById("trailerStatus").innerHTML = httpRequest.responseText.valueOf() + " Already Exists";
                                                    document.getElementById("trailerNo" + val).value = "";
                                                    document.getElementById("trailerNo" + val).focus();
                                                    //                                            document.getElementById("trailerNo" + val).select();
                                                    //                                            document.vehicleVendorContract.regNoCheck.value = 'exists';
                                                } else
                                                {
                                                    //                                            alert("Hi at 2");
                                                    //                                            document.vehicleVendorContract.regNoCheck.value = 'Notexists';
                                                    //                                            document.getElementById("Status").innerHTML = "";
                                                    document.getElementById("trailerStatus").innerHTML = "";
                                                }
                                            }
                                        }
                                    }
                                </script>



                            </c:if>

                            <br/>
                            <br/>

                        </div>


                        <!--<a  class="pretab" href="#"><input type="button" class="button" value="Update" name="Update" onclick="submit(this.val);"/></a>-->      
                    </div>  

                    <!--                     <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>-->
                    <script>


                        function submitPage(val) {
//                              alert("val"+val);
                            var regNo = document.getElementsByName("vehicleRegNo");
                            var agreedDate = document.getElementsByName("agreedDate");
                            var mfr = document.getElementsByName("mfr");
                            var model = document.getElementsByName("model");
                            var remarks = document.getElementsByName("remarks");
                            var activeInd = document.getElementsByName("activeIndVehicle");
                            var replaceVehicle = document.getElementsByName("replaceVehicle");
                            var existVehicle = document.getElementsByName("existVehicle");
//                           alert(regNo.length);
                            var vehicleUnits =<c:out value="${vehicleUnitsDedicate}"/>;
                            var counts = 1;
                            var count;
                            for (var i = 0; i < regNo.length; i++) {

                                if (regNo[i].value == "") {
                                    alert("please enter VehicleNo!")
                                    return;
                                }
                                if (agreedDate[i].value == "") {
                                    alert("please enter agreedDate!")
                                    return;
                                }
                                if (mfr[i].value == "0") {
                                    alert("please select mfr!")
                                    return;
                                }
                                if (model[i].value == "0") {
                                    alert("please select model!")
                                    return;
                                }
                                if (remarks[i].value == "") {
                                    alert("please enter remarks!")
                                    return;
                                }
                                if (activeInd[i].value == "") {
                                    alert("please select activeInd!")
                                    return;
                                }


                                if (activeInd[i].value == 'Y') {
                                    count = counts++;
//                                    if (replaceVehicle[i].value != "" && existVehicle[i].value != "") {
//                                        count = count - 1;
//                                    }
                                }
                            }
                            if (count > vehicleUnits || count < vehicleUnits) {
                                alert("please select atleast " + vehicleUnits + " active status")
                                return;
                            }
                            document.customerContract.action = "/throttle/saveEditVehicleTrailerContract.do?vendorId=<c:out value="${vendorId}"/>";
                            document.customerContract.method = "post";
                            document.customerContract.submit();


                        }
                        //                    

                        $(".nexttab").click(function() {
                            var selected = $("#tabs").tabs("option", "selected");
                            $("#tabs").tabs("option", "selected", selected + 1);
                        });
                        $(".pretab").click(function() {
                            var selected = $("#tabs").tabs("option", "selected");
                            $("#tabs").tabs("option", "selected", selected - 1);
                        });




                    </script>

                    <table align="center">
                        <tr>
                            <td><a href="#"><input type="button" class="btn btn-info" value="Save" name="update" onclick="submitPage(this.val);"/></a> </td>
                        </tr>
                    </table>

                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>