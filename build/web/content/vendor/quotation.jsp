<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });


</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script>
    function submitPage() {
        document.vehicleVendorContract.action = '/throttle/saveQuotation.do';
        document.vehicleVendorContract.submit();
    }

    $(document).ready(function() {
        $('#customerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getDailyContractedCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term,
                        customerCode: document.getElementById('customerCode').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('~');
                $itemrow.find('#customerId').val(tmp[0]);
                $itemrow.find('#customerName').val(tmp[1]);



                //$('#ahref').attr('href', '/throttle/viewCustomerContract.do?custId=' + tmp[0]);
                //   $("#contractDetails").show();


                return false;

            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>  <spring:message code="sales.label.CreateQuotation"  text="default text"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="sales.label.Sales/Ops"  text="default text"/></a></li>
            <li class="active"><spring:message code="sales.label.CreateQuotation"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <%--  <%try{%>--%>

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>



                <form name="vehicleVendorContract" method="post" >
                    <%@ include file="/content/common/message.jsp" %>

                    <td colspan="4"  style="background-color:#5BC0DE;">
                        <table class="table table-info mb30 table-hover"  >
                            <thead>  <tr >
                                    <th  colspan="4" ><spring:message code="sales.label.CustomerInfo"  text="default text"/></th>
                                </tr>
                                <tr> </thead>
                            <td height="30"><spring:message code="sales.label.BusinessType"  text="default text"/></td>
                            <td>
                                <select class="form-control" style="width:260px;height:40px;" id="quotationType" name="quotationType" >
                                    <option value="0">---<spring:message code="sales.label.select"  text="default text"/>---</option>
                                    <option value="1"><spring:message code="sales.label.Monthly"  text="default text"/></option>
                                    <option value="2"><spring:message code="sales.label.Daily"  text="default text"/></option>
                                </select>
                            </td>

                            <td height="30"><spring:message code="sales.label.QuotationDate"  text="default text"/></td>
                            <td>
                                <input name="quotationDate" type="text"   style="width:260px;height:40px;" class="datepicker form-control"  >
                            </td>
                            </tr>
                             <tr>
                    <td height="30">
                        contract tariff type
                    </td>
                     <td>
                        <select id="tariffType" name="tariffType" onchange="changeRateType();" >
                            <option value="0">---select---</option>
                            <option value="1">Flat</option>
                            <option value="2">Per Km</option>
                        </select>
                    </td>
                </tr>
                            <tr>
                                <td ><spring:message code="sales.label.CustomerName"  text="default text"/></td>
                                <td ><input type="text" name="customerName" id="customerName" value="" class="form-control" style="width:260px;height:40px;">
                                    <input type="hidden" name="customerCode" id="customerCode" value="C" class="form-control" style="width:260px;height:40px;">
                                    <input type="hidden" name="customerId" id="customerId" value="0" class="form-control" style="width:260px;height:40px;">
                                </td>
                                <td ><spring:message code="sales.label.CustomerAddress"  text="default text"/></td>
                                <td >   <textarea rows="3" cols="30" class="form-control" style="width:260px;height:40px;" name="customerAddress" id="customerAddress"   style="width:142px"></textarea></td>

                            </tr>
                            <tr>
                                <td ><spring:message code="sales.label.ContactPerson"  text="default text"/></td>
                                <td ><input type="text" name="contactPerson" id="contactPerson" value="" class="form-control" style="width:260px;height:40px;"></td>
                                <td ><spring:message code="sales.label.Dept"  text="default text"/></td>
                                <td >   <input type="text" name="dept" id="dept" class="form-control" style="width:260px;height:40px;"/></td>

                            </tr>
                            <tr>
                                <td ><spring:message code="sales.label.FuelResponsibility"  text="default text"/></td>
                                <td ><select class="form-control" style="width:260px;height:40px;" id="fuelResponsibility" name="fuelResponsibility">
                                        <option value="0">--<spring:message code="sales.label.select"  text="default text"/>--</option>
                                        <option value="1"><spring:message code="sales.label.own"  text="default text"/></option>
                                        <option value="2"><spring:message code="sales.label.customer"  text="default text"/></option>
                                    </select></td>
                                <td ><spring:message code="sales.label.DriverResponsibility"  text="default text"/></td>
                                <td > <select class="form-control" style="width:260px;height:40px;" id="driverResponsibility" name="driverResponsibility">
                                        <option value="0">--<spring:message code="sales.label.select"  text="default text"/>--</option>
                                        <option value="1"><spring:message code="sales.label.own"  text="default text"/></option>
                                        <option value="2"><spring:message code="sales.label.customer"  text="default text"/></option>
                                    </select></td>

                            </tr>
                            <tr>
                                <td height="30"><spring:message code="sales.label.SalesManager"  text="default text"/></td>
                                <td>
                                    <select class="form-control" style="width:260px;height:40px;" id="salesManager" name="salesManager" >
                                        <option value="0">---<spring:message code="sales.label.select"  text="default text"/>---</option>
                                        <c:forEach items="${salesPerson}" var="sales">
                                            <option value="<c:out value="${sales.empId}"/>"><c:out value="${sales.empName}"/></option>
                                        </c:forEach>

                                    </select>
                                </td>
                                <td height="30"><spring:message code="sales.label.QuotationStatus"  text="default text"/></td>
                                <td>
                                    <select class="form-control" style="width:260px;height:40px;" id="quotationStatus" name="quotationStatus" >
                                        <option value="32"><spring:message code="sales.label.Created"  text="default text"/></option>
                                        <option value="33"><spring:message code="sales.label.Approved"  text="default text"/></option>
                                        <option value="34"><spring:message code="sales.label.Rejected"  text="default text"/></option>
                                    </select>
                                </td>

                            </tr>
                            <tr>
                                <td height="30">
                                    <spring:message code="sales.label.Email"  text="default text"/>
                                </td>
                                <td><input type="text"  style="width:260px;height:40px;" id="email" name="email" value=""/></td>
                                <td height="30">
                                    <spring:message code="sales.label.validity"  text="default text"/>
                                </td>
                                <td> <input name="validity"  style="width:260px;height:40px;" type="text"  class="datepicker form-control"  ></td>
                            </tr>
                        </table>
            <br>

                    <!--            <div id="tabs">
                                    <ul class="">
                                        <li><a href="#veh"><span>Daily Contract Flat</span></a></li>
                                        <li><a href="#trailer"><span>Daily Contract (Fixed KM)</span></a></li>
                                       
                                        <li><a href="#weightBreak"><span>Weight Break </span></a></li>
                                    </ul>-->

                    <!--                <div id="trailer"></div>-->

                    <!--<script>
                        function saveVendorContract(){
                            document.customerContract.action = "/throttle/saveVehicleVendorContract.do";
                            document.customerContract.submit();
                        }
                     </script>-->
                    <div id="veh">
                        <script>
                            var contain = "";
                            $(document).ready(function() {
                                var iCnt = 1;
                                var rowCnt = 1;
                                // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                contain = $($("#routedeD")).css({
                                    padding: '5px', margin: '20px', width: '100%', border: '0px dashed',
                                    borderTopColor: '#999', borderBottomColor: '#999',
                                    borderLeftColor: '#999', borderRightColor: '#999'
                                });
                                $(contain).last().after('<table class="table table-info mb30 table-hover" id="mainTableFullTruck" width="100%"><tr></td>\n\
                                   <table  class="table table-info mb30 table-hover" id="routeDetails1' + iCnt + '"  border="1">\n\
                                <thead><tr >\n\
                                    <th>Sno</th>\n\
                                    <th><center>Vehicle Type</center></th>\n\
                                    <th><center>Trailer Type</center></th>\n\
                                    <th><center>Quantity</center></th>\n\
                                   <th><center>\n\
                                   <div id="flatBased" style="display:none;"> \n\
                                 <font color="red">*</font>Rate\n\
                               </div>\n\
                                <div id="kmBased" style="display:none;">\n\
                               <font color="red">*</font>Rate Per Km\n\
                                </div>\n\
                                    </center></th>\n\
                                   <th><center>\n\
                                         <div id="totalRate" style="display:none;">\n\
                                     <font color="red">*</font>Total Rate\n\
                                </div>\n\
                                                        </center>\n\
                                        </th>\n\
                                <tr></thead>\n\
                               <td> ' + iCnt + ' </td>\n\
                                   <td align="center"><select type="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '" onchange="onSelectVal(this.value,' + iCnt + ');"><option value="0">--Select--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td>\n\
                                   <td align="center"><select type="text" name="trailerTypeId" id="trailerTypeId' + iCnt + '"><option value="0">--Select--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="trailer"><option value="<c:out value="${trailer.trailerId}"/>"><c:out value="${trailer.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                                   <td align="center"><input type="text" name="qty" id="qty' + iCnt + '" value="1"  onchange="calculateTotalVehicleFixedCost(' + iCnt + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                   <td align="center"> <div id="perKmCostValue"  style="display:none;"><input type="text" name="perKmCost" id="perKmCost' + iCnt + '" value="0"  onchange="calculateTotalVehicleFixedCost(' + iCnt + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></div>\n\
                                    <div id="fixedCostPerVehicleValue"  style="display:none;"><input type="text" name="fixedCostPerVehicle" id="fixedCostPerVehicle' + iCnt + '" value="0"  onchange="calculateTotalVehicleFixedCost(' + iCnt + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></div></td>\n\
                                   <td align="center"> <div  id="totalRateValue"  style="display:none;"><input type="text" readonly name="totalFixedCost" id="totalFixedCost' + iCnt + '" value="0"  onchange="calculateTotalVehicleFixedCost(' + iCnt + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></div></td>\n\
                                </tr>\n\
                                </table>\n\
                                <table align="center" border="" width=""><tr>\n\
                                <td><input class="btn btn-info" type="button" name="addRouteDetailsFullTruck1" id="addRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="<spring:message code="sales.label.Add"  text="default text"/>" onclick="addRows(' + iCnt + ')" />\n\
                                <input class="btn btn-info" type="button" name="removeRouteDetailsFullTruck1" id="removeRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="<spring:message code="sales.label.Remove"  text="default text"/>"  onclick="deleteRows(' + iCnt + ')" />\n\
                                </tr></table></td></tr></table><br><br>');
                                callOriginAjaxdeD(iCnt);
                                callDestinationAjaxdeD(iCnt);
                                $('#btAdd').click(function() {
                                    iCnt = iCnt + 1;

                                    callOriginAjaxdeD(iCnt);
                                    callDestinationAjaxdeD(iCnt);
                                    $('#maindeD').after(contain);
                                });
                                $('#btRemove').click(function() {
//                                        alert($('#mainTabledeD tr').size());
                                    if ($(contain).size() > 2) {
                                        $(contain).last().remove();
                                        iCnt = iCnt - 1;
                                    }
                                });
                            });

                            // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                            var divValue, values = '';
                            function GetTextValue() {
                                $(divValue).empty();
                                $(divValue).remove();
                                values = '';
                                $('.input').each(function() {
                                    divValue = $(document.createElement('div')).css({
                                        padding: '5px', width: '200px'
                                    });
                                    values += this.value + '<br />'
                                });
                                $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                $('body').append(divValue);
                            }

                            $(document).ready(function() {
                                $("#mAllow").keyup(function() {
//                                        alert($(this).val());
                                });
                            })



                            function addRows(val) {
                                //alert(val);
                                var loadCnt = val;
                                //    alert(loadCnt)
                                //alert("loadCnt");
                                //var loadCnt1 = ve;
                                var routeInfoSize = $('#routeDetails1' + loadCnt + ' tr').size();
                                //  alert(routeInfoSize);
                                    var routeInfoSizeSub = routeInfoSize ;
                                var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                                var routeInfoDetails = "routeDetails1" + loadCnt;
                                $('#routeDetails1' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSizeSub + '</td>\n\
                            <%--   <td><input type="text" name="vehicleTypeIddeDTemp" id="vehicleTypeIddeDTemp' + loadCnt + '" /></td>--%>\n\
                                   <td align="center"><select ype="text" name="vehicleTypeId" id="vehicleTypeId' + routeInfoSizeSub + '" onchange="onSelectVal(this.value,' + routeInfoSizeSub + ');"><option value="0">--Select--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td>\n\
                                   <td align="center"> <select type="text" name="trailerTypeId" id="trailerTypeId' + routeInfoSizeSub + '"><option value="0">--Select--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="trailer"><option value="<c:out value="${trailer.trailerId}"/>"><c:out value="${trailer.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                                  <td align="center"><input type="text" name="qty" id="qty' + routeInfoSizeSub + '" value="1" onchange="calculateTotalVehicleFixedCost(' + routeInfoSizeSub + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                   <td align="center"> <div  id="perKmCostValueAdd' + routeInfoSizeSub + '"  style="display:none;"><input type="text" name="perKmCost" id="perKmCost' + routeInfoSizeSub + '" value="0"  onchange="calculateTotalVehicleFixedCost(' + routeInfoSizeSub + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></div>\n\
                                    <div id="fixedCostPerVehicleValueAdd' + routeInfoSizeSub + '"  style="display:none;"><input type="text" name="fixedCostPerVehicle" id="fixedCostPerVehicle' + routeInfoSizeSub + '" value="0"  onchange="calculateTotalVehicleFixedCost(' + routeInfoSizeSub + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></div></td>\n\
                                   <td align="center"> <div id="totalRateValueAdd' + routeInfoSizeSub + '"  style="display:none;"><input type="text" readonly name="totalFixedCost" id="totalFixedCost' + routeInfoSizeSub + '" value="0"  onchange="calculateTotalVehicleFixedCost(' + routeInfoSizeSub + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></div></td>\n\
                               </tr>');
                                // alert("loadCnt = "+loadCnt)
                                loadCnt++;

                                      var tariffType = document.getElementById("tariffType").value;
                                        if(tariffType == 1){

                                            document.getElementById("totalRateValueAdd"+routeInfoSizeSub).style.display = 'block';
                                            document.getElementById("perKmCostValueAdd"+routeInfoSizeSub).style.display = 'none';
                                            document.getElementById("fixedCostPerVehicleValueAdd"+routeInfoSizeSub).style.display = 'block';

                                        }else if(tariffType == 2){

                                             document.getElementById("totalRateValueAdd"+routeInfoSizeSub).style.display = 'none';
                                            document.getElementById("perKmCostValueAdd"+routeInfoSizeSub).style.display = 'block';
                                            document.getElementById("fixedCostPerVehicleValueAdd"+routeInfoSizeSub).style.display = 'none';
                                        }
                                //   alert("loadCnt = +"+loadCnt)
                            }
                            function onSelectVal(val, countVal) {
                                //                            alert("vehicleTypeIdDedicate"+val);
                                document.getElementById("vehicleTypeIddeDTemp" + countVal).value = val;
                            }


                            function deleteRows(val) {
                                var loadCnt = val;
                                //     alert(loadCnt);
                                var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                                var routeInfoDetails = "routeDetails1" + loadCnt;
                                // alert(routeInfoDetails);
                                if ($('#routeDetails1' + loadCnt + ' tr').size() > 2) {
                                    $('#routeDetails1' + loadCnt + ' tr').last().remove();
                                    loadCnt = loadCnt - 1;
                                } else {
                                    alert('One row should be present in table');
                                }
                            }

                            function calculateTotalTrailerFixedCost(sno) {
//                                    alert(sno);
                                var trailerUnits = document.getElementById("trailerTypeUnits" + sno).value;
                                var fixedCost = document.getElementById("fixedCostPerTrailer" + sno).value;
                                var totalCost = parseInt(trailerUnits) * parseInt(fixedCost);
                                document.getElementById("totalTrailerCost" + sno).value = totalCost;
                            }
                            function calculateTotalVehicleFixedCost(sno) {
                                
                                var vehicleUnits = document.getElementById("qty" + sno).value;
                                var fixedCost = document.getElementById("fixedCostPerVehicle" + sno).value;
                                var totalCost = parseInt(vehicleUnits) * parseInt(fixedCost);
                                document.getElementById("totalFixedCost" + sno).value = totalCost;
                            }
                            function setTextBoxEnable(val, count) {
                                //    alert("enterd----"+val);
                                //  alert(count);

                                if (val == 1) {
                                    document.getElementById("rateCost" + count).readOnly = true;
                                    document.getElementById("rateLimit" + count).readOnly = false;
                                    document.getElementById("maxAllowableKM" + count).readOnly = false;
                                }
                                if (val == 2) {

                                    document.getElementById("rateCost" + count).readOnly = false;
                                    document.getElementById("rateLimit" + count).readOnly = true;
                                    document.getElementById("maxAllowableKM" + count).readOnly = true;

                                }
                            }

                            function showVehicleExtraKmRun() {
                                var billingKmType = document.getElementById("billingKmCalculationId").value;

                                var tbl = document.getElementById("routeDetails11");
                                if (billingKmType == 1) {
                                    for (var i = 0; i < tbl.rows.length; i++) {

                                        tbl.rows[i].cells[6].style.display = "none";


                                    }

                                } else {
                                    for (var i = 0; i < tbl.rows.length; i++) {

                                        tbl.rows[i].cells[6].style.display = "block";


                                    }
                                }

                            }

                        function changeRateType(){
            //alert("am here...");
            var tariffType = document.getElementById("tariffType").value;
            if(tariffType == 1){
                document.getElementById("flatBased").style.display = 'block';
                document.getElementById("kmBased").style.display = 'none';
                document.getElementById("totalRate").style.display = 'block';
                document.getElementById("totalRateValue").style.display = 'block';
                document.getElementById("perKmCostValue").style.display = 'none';
                document.getElementById("fixedCostPerVehicleValue").style.display = 'block';
                document.getElementById("totalRateValueAdd").style.display = 'block';
                document.getElementById("perKmCostValueAdd").style.display = 'none';
                document.getElementById("fixedCostPerVehicleValueAdd").style.display = 'block';

            }else if(tariffType == 2){
                 document.getElementById("flatBased").style.display = 'none';
                document.getElementById("kmBased").style.display = 'block';
                 document.getElementById("totalRate").style.display = 'none';
                 document.getElementById("totalRateValue").style.display = 'none';
                document.getElementById("perKmCostValue").style.display = 'block';
                document.getElementById("fixedCostPerVehicleValue").style.display = 'none';
                 document.getElementById("totalRateValueAdd").style.display = 'none';
                document.getElementById("perKmCostValueAdd").style.display = 'block';
                document.getElementById("fixedCostPerVehicleValueAdd").style.display = 'none';
            }
        }
                                </script>

                                <script>
                                    function callOriginAjaxdeD(val) {
                                        // Use the .autocomplete() method to compile the list based on input from user
                                        //alert(val);
                                        var pointNameId = 'originNamedeD' + val;
                                        var pointIdId = 'originIddeD' + val;
                                        var desPointName = 'destinationNamedeD' + val;


                                        //alert(prevPointId);
                                        $('#' + pointNameId).autocomplete({
                                            source: function(request, response) {
                                                $.ajax({
                                                    url: "/throttle/getTruckCityList.do",
                                                    dataType: "json",
                                                    data: {
                                                        cityName: request.term,
                                                        textBox: 1
                                                    },
                                                    success: function(data, textStatus, jqXHR) {
                                                        var items = data;
                                                        response(items);
                                                    },
                                                    error: function(data, type) {

                                                        //console.log(type);
                                                    }
                                                });
                                            },
                                            minLength: 1,
                                            select: function(event, ui) {
                                                var value = ui.item.Name;
                                                var id = ui.item.Id;
                                                //alert(id+" : "+value);
                                                $('#' + pointIdId).val(id);
                                                $('#' + pointNameId).val(value);
                                                $('#' + desPointName).focus();
                                                //validateRoute(val,value);

                                                return false;
                                            }

                                            // Format the list menu output of the autocomplete
                                        }).data("autocomplete")._renderItem = function(ul, item) {
                                            //alert(item);
                                            var itemVal = item.Name;
                                            itemVal = '<font color="green">' + itemVal + '</font>';
                                            return $("<li></li>")
                                                    .data("item.autocomplete", item)
                                                    .append("<a>" + itemVal + "</a>")
                                                    .appendTo(ul);
                                        };


                                    }

                                    function callDestinationAjaxdeD(val) {
                                        // Use the .autocomplete() method to compile the list based on input from user
                                        //alert(val);
                                        var pointNameId = 'destinationNamedeD' + val;
                                        var pointIdId = 'destinationIddeD' + val;
                                        var originPointId = 'originIddeD' + val;
                                        var truckRouteId = 'routeIddeD' + val;
                                        var travelKm = 'travelKmdeD' + val;
                                        var travelHour = 'travelHourdeD' + val;
                                        var travelMinute = 'travelMinutedeD' + val;

                                        //alert(prevPointId);
                                        $('#' + pointNameId).autocomplete({
                                            source: function(request, response) {
                                                $.ajax({
                                                    url: "/throttle/getTruckCityList.do",
                                                    dataType: "json",
                                                    data: {
                                                        cityName: request.term,
                                                        originCityId: $("#" + originPointId).val(),
                                                        textBox: 1
                                                    },
                                                    success: function(data, textStatus, jqXHR) {
                                                        var items = data;
                                                        response(items);
                                                    },
                                                    error: function(data, type) {

                                                        //console.log(type);
                                                    }
                                                });
                                            },
                                            minLength: 1,
                                            select: function(event, ui) {
                                                var value = ui.item.Name;
                                                var id = ui.item.Id;
                                                //alert(id+" : "+value);
                                                $('#' + pointIdId).val(id);
                                                $('#' + pointNameId).val(value);
                                                $('#' + travelKm).val(ui.item.TravelKm);
                                                $('#' + travelHour).val(ui.item.TravelHour);
                                                $('#' + travelMinute).val(ui.item.TravelMinute);
                                                $('#' + truckRouteId).val(ui.item.RouteId);
                                                //validateRoute(val,value);

                                                return false;
                                            }

                                            // Format the list menu output of the autocomplete
                                        }).data("autocomplete")._renderItem = function(ul, item) {
                                            //alert(item);
                                            var itemVal = item.Name;
                                            itemVal = '<font color="green">' + itemVal + '</font>';
                                            return $("<li></li>")
                                                    .data("item.autocomplete", item)
                                                    .append("<a>" + itemVal + "</a>")
                                                    .appendTo(ul);
                                        };


                                    }

                                </script>

                                <div id="routedeD">

                                </div>



                            </div>
                                   <div id="conditions">
                                <table>
                                    <tr>
                                        <td>
                                            <textarea id="conditions" name="conditions" style="width: 1000px; height: 400px;">
                       Terms & conditions:
                               Above rates quoted on Daily Basis Inclusive of:-,
                             Drivers with H2S certification.,
                                PPE Equipped.,
                                Safety trained (Tahoos own safety department to train drivers and staff).,
                                GPS Equipped trucks enable to track truks 24/7.,
                                Link will be provided for tracking all trucks within your office.,
                                24/7 Road side assistance.,
                               Replacement of any equipment in case of major issues. ,
                                In-house safety officer will be available for monitoring and ensure all goes well. ,
                              A daily tracking report will be submitted upon request to have a clear picture of each trip. ,
                                Above the best we can offer and hope above service will suitable for your requirement. ,
                               All the rates are quoted based on Daily and exclude diesel and driver accommodation and food.,
                               All Equipment  are ready for inspection at given time .,
                                            </textarea>
                                        </td>
                                        </tr>
                                </table>
                            </div>

                            <script>

                                $(".nexttab").click(function() {
                                    var selected = $("#tabs").tabs("option", "selected");
                                    $("#tabs").tabs("option", "selected", selected + 1);
                                });
                                $(".pretab").click(function() {
                                    var selected = $("#tabs").tabs("option", "selected");
                                    $("#tabs").tabs("option", "selected", selected - 1);
                                });

                            </script>

                            <!--                    </div>-->

                            <center>
                                <input type="button"  style="height: 28px;" class="btn btn-info" value="<spring:message code="sales.label.Save"  text="default text"/>" onclick="submitPage();" />

                    </center>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                <%--   <%}catch(Exception e)
             {
                    out.println(e.toString());
                }
           %>--%>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
