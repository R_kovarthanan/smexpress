<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });

    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });

    function findAdvanceAmount(sno) {

        var additionalCost = $("#additionalCost" + sno).val();
        var advanceMode = $("#advanceMode" + sno).val();
        var modeRate = $("#modeRate" + sno).val();

        var endAdvance = "";
        var initialAdvance = "";

        if (advanceMode != "") {

            if (advanceMode == 1) {

                var percentValue = "";
                if (modeRate == 0) {
                    percentValue = 0;
                } else {
                    percentValue = parseFloat(additionalCost) * (parseFloat(modeRate) / 100)
                }

                initialAdvance = percentValue;
                endAdvance = parseFloat(additionalCost) - parseFloat(percentValue);

            } else if (advanceMode == 2) {
                var flatValue = parseFloat(additionalCost) - parseFloat(modeRate);
                initialAdvance = parseFloat(modeRate);
                endAdvance = parseFloat(flatValue);
            }
            else if (advanceMode == 3) {
                initialAdvance = 0;
                endAdvance = parseFloat(additionalCost);
                document.getElementById("modeRate" + sno).value = 0;
            }
            else {
                //alert("Please Select advance type");
                document.getElementById("initialAdvance" + sno).value = 0;
                document.getElementById("endAdvance" + sno).value = 0;
                document.getElementById("modeRate" + sno).value = 0;
                return false;
            }

            document.getElementById("initialAdvance" + sno).value = initialAdvance;
            document.getElementById("endAdvance" + sno).value = endAdvance;
        }
    }

</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script>
    function submitPage1() {
          if (isEmpty(document.vehicleVendorContract.startDate.value)) {
            alert('please enter Contract From Date');
            document.getElementById("startDate").focus();
            return false;
        }

        else if (isEmpty(document.vehicleVendorContract.endDate.value)) {
            alert('please enter Contract To Date');
            document.getElementById("endDate").focus();
            return false;
        }
        else{
        document.vehicleVendorContract.action = '/throttle/saveVehicleVendorContract.do';
        document.vehicleVendorContract.submit();
    }
    }

    function submitPage() {
        var contractTypeId = $("#contractTypeId").val();
        var count = 0;

        if (contractTypeId == 2 || contractTypeId == 3) {
            count = checkOnDemandContract();
        }

        if (isEmpty(document.vehicleVendorContract.startDate.value)) {
            alert('please enter Contract From Date');
            document.getElementById("startDate").focus();
            return false;
        }

        if (isEmpty(document.vehicleVendorContract.endDate.value)) {
            alert('please enter Contract To Date');
            document.getElementById("endDate").focus();
            return false;
        }


        var spot7 = $("[name=originNameFullTruck]");
        for (var i = 0; i < spot7.length; i++) {
            //alert(spot1[i].value);
            if (spot7[i].value == '0' || spot7[i].value == '0.00') {
                alert(" Origin cannot be empty")
                spot7[i].focus();
                return false;
            }
            if (spot7[i].value == '') {
                alert(" Origin cannot be empty")
                spot7[i].focus();
                return false;
            }
        }

        var spot8 = $("[name=destinationNameFullTruck]");
        for (var i = 0; i < spot8.length; i++) {
            //alert(spot1[i].value);
            if (spot8[i].value == '0' || spot8[i].value == '0.00') {
                alert(" Destination cannot be empty")
                spot8[i].focus();
                return false;
            }
            if (spot8[i].value == '') {
                alert(" Destination cannot be empty")
                spot8[i].focus();
                return false;
            }
        }

        var spot4 = $("[name=fromDate1]");
        for (var i = 0; i < spot4.length; i++) {
            //alert(spot1[i].value);
            if (spot4[i].value == '0' || spot4[i].value == '0.00') {
                alert(" From Date cannot be empty")
                spot4[i].focus();
                return false;
            }
            if (spot4[i].value == '') {
                alert(" From Date cannot be empty")
                spot4[i].focus();
                return false;
            }
        }

        var spot5 = $("[name=toDate1]");
        for (var i = 0; i < spot5.length; i++) {
            //alert(spot1[i].value);
            if (spot5[i].value == '0' || spot5[i].value == '0.00') {
                alert(" To Date cannot be empty")
                spot5[i].focus();
                return false;
            }
            if (spot5[i].value == '') {
                alert(" To Date cannot be empty")
                spot5[i].focus();
                return false;
            }
        }

        var spot6 = $("[name=vehicleTypeId]");
        for (var i = 0; i < spot6.length; i++) {
            //alert(spot1[i].value);
            if (spot6[i].value == '0' || spot6[i].value == '0.00') {
                alert(" Vehicle Type cannot be empty")
                spot6[i].focus();
                return false;
            }
            if (spot6[i].value == '') {
                alert(" Vehicle Type cannot be empty")
                spot6[i].focus();
                return false;
            }
        }


        var spot1 = $("[name=additionalCost]");
        for (var i = 0; i < spot1.length; i++) {
            //alert(spot1[i].value);
            if (spot1[i].value == '0' || spot1[i].value == '0.00') {
                alert("contract amount cannot be zero")
                spot1[i].focus();
                return false;
            }
            if (spot1[i].value == '') {
                alert("please enter the contract amount")
                spot[i].focus();
                return false;
            }
        }

        var spot3 = $("[name=advanceMode]");
        for (var i = 0; i < spot3.length; i++) {
            //alert(spot1[i].value);
            if (spot3[i].value == '0' || spot3[i].value == '0.00') {
                alert(" Advance Type cannot be empty")
                spot3[i].focus();
                return false;
            }
            if (spot3[i].value == '') {
                alert(" Advance Type cannot be empty")
                spot3[i].focus();
                return false;
            }
        }


        if (count == 0) {
            document.vehicleVendorContract.action = '/throttle/saveVehicleVendorContract.do';
            document.vehicleVendorContract.submit();
        }
    }

    function checkOnDemandContract() {
        var spot = $("[name=spotCost]");
        var count = 0;
        for (var i = 0; i < spot.length; i++) {
//        if(spot[i].value == '0' || spot[i].value == '0.00' ){
//          alert("contract amount cannot be 0")
//          spot[i].focus();
//          count = 1;
//          return count;
//        }
            if (spot[i].value == '') {
                alert("please enter the contract amount")
                spot[i].focus();
                count = 1;
                return count;
            }
        }
        return count;
    }
</script>

<script>
    function setVehicleTypeDetails(sel, sno) {
        var vehicleTypeName = sel.options[sel.selectedIndex].text;
        if (vehicleTypeName == "20'") {
            $("#containerTypeId" + sno + " option[value='2']").remove();
            document.getElementById("containerTypeId" + sno).value = 1;
            $('#containerQty' + sno).val('1');
        } else {
            var exist = 0;
            $('#containerTypeId' + sno + ' option').each(function() {
                if (this.value == '2') {
                    exist = 1;
                }
            });
            if (exist == 1) {
            } else {
                $('#containerTypeId' + sno).append(new Option("40'", '2'));
            }
            document.getElementById("containerTypeId" + sno).value = 0;
            $('#containerQty' + sno).val('');
        }
    }
    function setContainerValue(cnt, containerType) {
        var containers = containerType;
        var vehicleType = $('#vehicleTypeId' + cnt).val();
        if (vehicleType == "1059~40'" || vehicleType == "1059") {
            if (containers == "1") {
                $('#containerQty' + cnt).val('2');
            } else if (containers == "2") {
                $('#containerQty' + cnt).val('1');
            } else {
                $('#containerQty' + cnt).val('0');
            }
        }

    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Vendor Contract</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor Contract</a></li>
            <li class="active">Create Fleet Vendor Contract</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <%--  <%try{%>--%>

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>
                <form name="vehicleVendorContract" method="post" >
                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover" style="width:70%">
                        <thead>  <tr >
                                <th  colspan="4" >Vendor Contract Info</th>
                            </tr></thead>
                        <tr >
                            <td>Vendor Name</td>
                            <td><input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorId}"/>" class="form-control">
                                <input type="hidden" name="vendorName" id="vendorName" value="<c:out value="${vendorName}"/>" class="form-control">
                                <c:out value="${vendorName}"/></td>
                            <td>Contract Type</td>
                            <td>
                                <select name="contractTypeId" id="contractTypeId"  onchange="checkContractType(this.value);" style="height:22px;width:150px;">
                                    <!--<option value="0" selected>--Select--</option>-->
                                    <!--<option value="1" >Dedicated</option>-->
                                    <option value="2" >Market Hire</option>
                                    <!--<option value="3" selected>Both</option>-->
                                </select>
                            </td>
                        </tr>
                        <tr >
                            <td >Contract From</td>
                            <td ><input type="text" name="startDate" id="startDate" value="<c:out value="${startDate}" />" class="datepicker form-control"  style="height:22px;width:150px;color:black">
                            </td>

                            <td  >Contract To</td>
                            <td ><input type="text" name="endDate" id="endDate" value="<c:out value="${endDate}" />" class="datepicker form-control"  style="height:22px;width:150px;color:black"></td>
                        </tr>
                        <tr >
                            <td  >Payment Type</td>
                            <td>
                                <select name="paymentType" id="paymentType"   style="height:22px;width:150px;">
                                    <option value="1" >Monthly</option>
                                    <option value="2" >Trip Wise</option>
                                    <option value="3" >FortNight</option>
                                </select>
                            </td>
                            <td  colspan="2"></td>
                        </tr>
                    </table>
                    <br>

                    <div id="tabs" >
                        <ul class="nav nav-tabs">
                            <li class="active" data-toggle="tab"><a href="#fullTruck"><span>Market Hire</span></a></li>
                            <li  data-toggle="tab"><a href="#deD"><span>DEDICATED</span></a></li>
                            <!--                            <li data-toggle="tab" id="pc" style="display: block"><a href="#pcm"><span> Penality charges </span></a></li>
                                                        <li data-toggle="tab" id="dc" style="display: block"><a href="#dcm"><span> Detention charges </span></a></li>-->
                            <!--<li><a href="#weightBreak"><span>Weight Break </span></a></li>-->
                        </ul>
                        <div id="fullTruck" style="padding-left: 1px;">
                            <span id="statusSpan" style="color: red"></span>
                            <div id="mainFullTruck"  style="width: 72%;">
                                <input class="btn btn-info"  type="button" id="btAdd" value="Add Route" style="width:95px;height:30px;font-weight: bold;padding:1px;font-size: 12px;"/>
                                <input class="btn btn-info" type="button" id="btRemove" value="Remove Route" style="width:95px;height:30px;font-weight: bold;padding:1px;font-size: 12px;" />
                            </div>
                            <style>
                                .errorClass { 
                                    background-color: red;
                                }
                                .noErrorClass { 
                                    background-color: greenyellow;
                                }
                            </style>
                            <script>
                                function setContainerQtyList(val, sno) {
                                    if (val == '1') {
                                        // 20FT vehicle
                                        $('#containerQty' + sno).empty();
                                        $('#containerQty' + sno).append($('<option ></option>').val(2).html('2'))
                                    } else if (val == '2') {
                                        // 40FT vehicle
                                        $('#containerQty' + sno).empty();
                                        $('#containerQty' + sno).append($('<option ></option>').val(1).html('1'))

                                    }
                                }
                                function setContainerTypeList(val, sno) {
                                    if (val == '1058') {
                                        // 20FT vehicle
                                        $('#containerTypeId' + sno).empty();
                                        $('#containerTypeId' + sno).append($('<option ></option>').val(1).html('20'))
                                        $('#containerQty' + sno).empty();
                                        $('#containerQty' + sno).append($('<option ></option>').val(1).html('1'))
                                    } else if (val == '1059') {
                                        // 40FT vehicle
                                        $('#containerTypeId' + sno).empty();
                                        $('#containerTypeId' + sno).append($('<option ></option>').val(0).html('--Select--'))
                                        $('#containerTypeId' + sno).append($('<option ></option>').val(1).html('20'))
                                        $('#containerTypeId' + sno).append($('<option ></option>').val(2).html('40'))
                                        $('#containerQty' + sno).empty();

                                    }
                                }
                                function checkKey(obj, e, id, cnt) {
                                    var orgName = 'originNameFullTruck' + cnt;
                                    var point1Name = 'pointName1' + cnt;
                                    var point2Name = 'pointName2' + cnt;
                                    var point3Name = 'pointName3' + cnt;
                                    var point4Name = 'pointName4' + cnt;
                                    var destinationName = 'destinationNameFullTruck' + cnt;
                                    var pointId = "";
                                    if (id == orgName) {
                                        pointId = 'originIdFullTruck' + cnt;
                                    }
                                    if (id == point1Name) {
                                        pointId = 'pointId1' + cnt;
                                    }
                                    if (id == point2Name) {
                                        pointId = 'pointId2' + cnt;
                                    }
                                    if (id == point3Name) {
                                        pointId = 'pointId3' + cnt;
                                    }
                                    if (id == point4Name) {
                                        pointId = 'pointId4' + cnt;
                                    }
                                    if (id == destinationName) {
                                        pointId = 'originIdFullTruck' + cnt;
                                    }
                                    var idVal = $('#' + pointId).val();
                                    if (e.keyCode == 46 || e.keyCode == 8) {
                                        $('#' + id).val('');
                                        $('#' + pointId).val('');
                                    } else if (idVal != '' && e.keyCode != 13) {
//                                        $('#' + id).val('');
//                                        $('#' + pointId).val('');
                                    }
                                }

                                function checkPointNames(id, cnt) {
                                    var orgName = 'originNameFullTruck' + cnt;
                                    var point1Name = 'pointName1' + cnt;
                                    var point2Name = 'pointName2' + cnt;
                                    var point3Name = 'pointName3' + cnt;
                                    var point4Name = 'pointName4' + cnt;
                                    var destinationName = 'destinationNameFullTruck' + cnt;
                                    var pointId = "";
                                    if (id == orgName) {
                                        pointId = 'originIdFullTruck' + cnt;
                                    }
                                    if (id == point1Name) {
                                        pointId = 'pointId1' + cnt;
                                    }
                                    if (id == point2Name) {
                                        pointId = 'pointId2' + cnt;
                                    }
                                    if (id == point3Name) {
                                        pointId = 'pointId3' + cnt;
                                    }
                                    if (id == point4Name) {
                                        pointId = 'pointId4' + cnt;
                                    }
                                    if (id == destinationName) {
                                        pointId = 'destinationIdFullTruck' + cnt;
                                    }
                                    var idValue = $('#' + id).val();
                                    var idValue1 = $('#' + pointId).val();
                                    if (idValue != '' && idValue1 == '') {
                                        alert('Invalid Point Name');
                                        $('#' + id).focus();
//                                        $('#'+id).val('');
//                                        $('#'+pointId).val('');
                                    }
                                    getMarketHireRate(cnt, cnt);
                                }

                                function getMarketHireRate(sno, tableRow) {
                                    var vehicleTypeId = $("#vehicleTypeId" + sno).val();
                                    var loadTypeId = $("#loadTypeId" + sno).val();
//                                    var containerTypeId = $("#containerTypeId"+sno).val();
//                                    var containerQty = $("#containerQty"+sno).val();
                                    var originIdFullTruck = $("#originIdFullTruck" + tableRow).val();
                                    var pointId1 = $("#pointId1" + tableRow).val();
                                    var pointId2 = $("#pointId2" + tableRow).val();
                                    var pointId3 = $("#pointId3" + tableRow).val();
                                    var pointId4 = $("#pointId4" + tableRow).val();
                                    var destinationIdFullTruck = $("#destinationIdFullTruck" + tableRow).val();
                                    if (vehicleTypeId != '0' && originIdFullTruck != '' && destinationIdFullTruck != '' && loadTypeId != '0') {
                                        $.ajax({
                                            url: "/throttle/getMarketHireRate.do",
                                            dataType: "json",
                                            data: {
                                                vehicleTypeId: vehicleTypeId,
                                                loadTypeId: loadTypeId,
//                                            containerTypeId: containerTypeId,
//                                            containerQty: containerQty,
                                                originIdFullTruck: originIdFullTruck,
                                                pointId1: pointId1,
                                                pointId2: pointId2,
                                                pointId3: pointId3,
                                                pointId4: pointId4,
                                                destinationIdFullTruck: destinationIdFullTruck
                                            },
                                            success: function(data, textStatus, jqXHR) {
                                                if (data.MarketHireRate == '0') {
//                                                $("#statusSpan").text("Route not defined in organisation");
                                                    $("#marketRate" + sno).val(0);
                                                    $("#marketRateSpan" + sno).text(0);
//                                                $("#vehicleTypeId"+sno).val(0);
//                                                $("#containerTypeId"+sno).val(0);
//                                                $("#containerQty"+sno).val(0);
                                                } else if (data.MarketHireRate != '') {
                                                    $("#statusSpan").text("");
                                                    $("#marketRate" + sno).val(data.MarketHireRate);
                                                    $("#marketRateSpan" + sno).text(data.MarketHireRate);
                                                }
                                            },
                                            error: function(data, type) {
                                            }
                                        });
                                    }

                                }

                                function checkMarketRate(val, sno) {
                                    var spotCost = $("#spotCost" + sno).val();
                                    var additionalCost = $("#additionalCost" + sno).val();
                                    var marketRate = $("#marketRate" + sno).val();
                                    var sts = false;
                                    if (parseFloat(spotCost) > parseFloat(marketRate)) {

                                        sts = true;
                                    }
                                    if (parseFloat(additionalCost) > parseFloat(marketRate)) {
                                        sts = true;
                                    }
//                                    if(sts){
//                                    $("#approvalStatusSpan"+sno).text("Needs Approval");
//                                    $("#approvalStatusSpan"+sno).removeClass("noErrorClass");
//                                    $("#approvalStatusSpan"+sno).addClass("errorClass");
//                                    }else{
                                    $("#approvalStatusSpan" + sno).text("Auto Approval");
                                    $("#approvalStatusSpan" + sno).removeClass("errorClass");
                                    $("#approvalStatusSpan" + sno).addClass("noErrorClass");
//                                    }
                                }

                                var container = "";
                                $(document).ready(function() {
                                    var iCnt = 1;
                                    var rowCnt = 1;
                                    // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                    container = $($("#routeFullTruck")).css({
                                        padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                        borderTopColor: '#999', borderBottomColor: '#999',
                                        borderLeftColor: '#999', borderRightColor: '#999'
                                    });
                                    $(container).last().after('<table id="mainTableFullTruck" width="100%"><tr></td>\
                                    <table  id="routeDetails' + iCnt + '"  border="1">\n\
                                    <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                    <td>SNo</td>\n\
                                    <td>Origin</td>\n\
                                    <td>Touch Point 1</td>\n\
                                    <td>Touch Point 2</td>\n\
                                    <td>Touch Point 3</td>\n\
                                    <td>Touch Point 4</td>\n\
                                    <td>Destination</td>\n\
                                    <tr style="height:22px;"><td>Route&nbsp; ' + iCnt + '</td>\n\
                                    <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" />\n\
                                    <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/><input type="hidden" name="iCnt" id="iCnt' + iCnt + '" value="' + iCnt + '"/></td>\n\
                                    <td><input type="hidden" name="pointId1" id="pointId1' + iCnt + '" value="0" />\n\
                                    <input type="text" name="pointName1" id="pointName1' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);"  style="height:22px;width:160px;"/></td>\n\
                                    <input type="hidden" name="pointId2" id="pointId2' + iCnt + '" value="0" />\n\
                                    <td><input type="text" name="pointName2" id="pointName2' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/></td>\n\
                                    <input type="hidden" name="pointId3" id="pointId3' + iCnt + '" value="0" />\n\
                                    <td><input type="text" name="pointName3" id="pointName3' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/></td>\n\
                                    <input type="hidden" name="pointId4" id="pointId4' + iCnt + '" value="0" />\n\
                                    <td><input type="text" name="pointName4" id="pointName4' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" />\n\
                                    <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/></td>\n\
                                    <input type="hidden" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/><input type="hidden" name="travelHourFullTruck" id="travelHourFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/><input type="hidden" name="travelKmFullTruck" id="travelKmFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/><input type="hidden" name="routeIdFullTruck" id="routeIdFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/></td>\n\
                                    </tr>\n\
                                    </table>\n\
                                    <table  id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1">\n\
                                    <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                    <td>Status</td><td>SNo</td><td>FromDate</td><td>ToDate</td><td>Vehicle Type</td><td>Load Type</td><td>Rate</td><td>Advance Type</td><td>Advance Value</td><td>Initial Advance</td><td>Remaining Amount</td></tr>\n\
                                    <tr style="height:22px;">\n\
                                    <td><span id="approvalStatusSpan' + iCnt + '">Not Filled</span></td><td>' + rowCnt + '<input type="hidden" name="iCnt1" id="iCnt1' + iCnt + '" value="' + iCnt + '"/></td>\n\
                                    <td><input type="text" name="fromDate1" style="width:110px;"  id="fromDate1' + iCnt + '' + rowCnt + '" class="datepicker form-control"  value=""/></td>\n\
                                    <td><input type="text" name="toDate1"  style="width:110px;" id="toDate1' + iCnt + '' + rowCnt + '" class="datepicker form-control"  value=""/></td>\n\
                                    <td><input type="hidden" name="vehicleUnits" id="vehicleUnits' + iCnt + '" value="1" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/><select type="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '" style="height:22px;width:160px;" onchange="setVehicleTypeDetails(this,' + iCnt + ');setContainerTypeList(this.value,' + iCnt + ');getMarketHireRate(' + iCnt + ',' + iCnt + ')"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                    <td><select name="loadTypeId" id="loadTypeId' + iCnt + '"   onchange="getMarketHireRate(' + iCnt + ',' + iCnt + ');" style="height:22px;width:160px;"><option value="1">Empty Trip</option><option value="2" selected>Load Trip</option></select></td>\n\
                                    <input type="hidden" name="spotCost" id="spotCost' + iCnt + '" value="0"  onkeyup="checkMarketRate(this.value,' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/>\n\
                                    <input type="hidden" name="marketRate" id="marketRate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/>\n\
                                    <td><input type="text" name="additionalCost" id="additionalCost' + iCnt + '" value="0" onChange="findAdvanceAmount(' + iCnt + ')"  onkeyup="checkMarketRate(this.value,' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                    <td><select name="advanceMode" id="advanceMode' + iCnt + '"  onChange="findAdvanceAmount(' + iCnt + ')" style="height:22px;width:160px;" >\n\
                                        <option value=0>-Select-</option>\n\
                                        <option value=1>PERCENTAGE</option>\n\
                                        <option value=2>FLAT</option>\n\
                                        <option value=3>CREDIT</option>\n\
                                    </select>\n\
                                    </td>\n\
                                    <td><input type="text" name="modeRate" id="modeRate' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);" onChange="findAdvanceAmount(' + iCnt + ')" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="initialAdvance" id="initialAdvance' + iCnt + '" value="" readonly  style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="endAdvance" id="endAdvance' + iCnt + '" value=""  readonly style="height:22px;width:160px;"/></td>\n\
                                    <input type="hidden" name="lHCNO" id="lHCNO' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/>\n\
                                    </tr></table>\n\
                                    <br><table border="0"><tr>\n\
                                    <td><input class="btn btn-info" type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ',' + rowCnt + ')" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>\n\
                                    <input class="btn btn-info" type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRow(' + iCnt + rowCnt + ')" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></td>\n\
                                    </tr></table></td></tr></table><br><br>');
                                    callOriginAjaxFullTruck(iCnt);
                                    callPointId1AjaxFullTruck(iCnt);
                                    callPointId2AjaxFullTruck(iCnt);
                                    callPointId3AjaxFullTruck(iCnt);
                                    callPointId4AjaxFullTruck(iCnt);
                                    callDestinationAjaxFullTruck(iCnt);
                                    $('.datepicker').datepicker();
                                    $('#btAdd').click(function() {
                                        iCnt = iCnt + 1;
                                        $(container).last().after('<table id="mainTableFullTruck" width="100%"><tr><td>\
                                    <table  id="routeDetailsFullTruck' + iCnt + '" border="1" >\n\
                                    <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px">\n\
                                    <td>SNo</td>\n\
                                    <td>Origin</td>\n\
                                    <td>Touch Point 1</td>\n\
                                    <td>Touch Point 2</td>\n\
                                    <td>Touch Point 3</td>\n\
                                    <td>Touch Point 4</td>\n\
                                    <td>Destination</td>\n\
                                    <tr style="height:22px;">\n\
                                    <td>Route&nbsp; ' + iCnt + '</td>\n\
                                    <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/><input type="hidden" name="iCnt" id="iCnt' + iCnt + '" value="' + iCnt + '"/>\n\
                                    <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id, ' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="hidden" name="pointId1" id="pointId1' + iCnt + '" value="0" />\n\
                                    <input type="text" name="pointName1" id="pointName1' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id, ' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);"  style="height:22px;width:160px;"/></td>\n\
                                    <input type="hidden" name="pointId2" id="pointId2' + iCnt + '" value="0" />\n\
                                    <td><input type="text" name="pointName2" id="pointName2' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id, ' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/></td>\n\
                                    <input type="hidden" name="pointId3" id="pointId3' + iCnt + '" value="0" />\n\
                                    <td><input type="text" name="pointName3" id="pointName3' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id, ' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/></td>\n\
                                    <input type="hidden" name="pointId4" id="pointId4' + iCnt + '" value="0" />\n\
                                    <td><input type="text" name="pointName4" id="pointName4' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id, ' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id, ' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/>\n\
                                    <input type="hidden" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/><input type="hidden" name="travelHourFullTruck" id="travelHourFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/>\n\
                                    <input type="hidden" name="travelKmFullTruck" id="travelKmFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/><input type="hidden" name="routeIdFullTruck" id="routeIdFullTruck' + iCnt + '" value=""  style="height:22px;width:160px;"/>\n\
                                    <input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/></td>\n\
                                    </tr>\n\
                                    </table>\n\
                                    <table  id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1">\n\
                                    <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                    <td>Status</td><td>SNo</td><td>FromDate</td><td>ToDate</td><td>Vehicle Type</td><td>Load Type</td><td>Rate</td><td>Advance Type</td><td>Advance Value</td><td>Initial Advance</td><td>Remaining Amount</td></tr>\n\
                                    <tr style="height:22px;">\n\
                                    <td><span id="approvalStatusSpan' + iCnt + '">Not Filled</span></td><td>' + rowCnt + '<input type="hidden" name="iCnt1" id="iCnt1' + iCnt + '" value="' + iCnt + '"/></td>\n\
                                    <td><input type="text" name="fromDate1" style="width:110px;"  id="fromDate1' + iCnt + '' + rowCnt + '" class="datepicker form-control"  value=""/></td>\n\
                                    <td><input type="text" name="toDate1" style="width:110px;"  id="toDate1' + iCnt + '' + rowCnt + '" class="datepicker form-control"  value=""/></td>\n\
                                    <td><input type="hidden" name="vehicleUnits" id="vehicleUnits' + iCnt + '" value="1" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/><select type="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '" style="height:22px;width:160px;" onchange="setVehicleTypeDetails(this,' + iCnt + ');setContainerTypeList(this.value,' + iCnt + ');getMarketHireRate(' + iCnt + ',' + iCnt + ')"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                    <td><select name="loadTypeId" id="loadTypeId' + iCnt + '"   onchange="getMarketHireRate(' + iCnt + ',' + iCnt + ');" style="height:22px;width:160px;" ><option value="1">Empty Trip</option><option value="2" selected>Load Trip</option></select></td>\n\
                                    <input type="hidden" name="spotCost" id="spotCost' + iCnt + '" value="0"  onkeyup="checkMarketRate(this.value,' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/>\n\
                                    <input type="hidden" name="marketRate" id="marketRate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/>\n\
                                    <td><input type="text" name="additionalCost" id="additionalCost' + iCnt + '" value="0" onChange="findAdvanceAmount(' + iCnt + ')" onkeyup="checkMarketRate(this.value,' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;" /></td>\n\
                                    <td><select name="advanceMode" id="advanceMode' + iCnt + '"  onChange="findAdvanceAmount(' + iCnt + ')" style="height:22px;width:160px;" >\n\
                                        <option value=0>-Select-</option>\n\
                                        <option value=1>PERCENTAGE</option>\n\
                                        <option value=2>FLAT</option>\n\
                                        <option value=3>CREDIT</option>\n\
                                    </select>\n\
                                    </td>\n\
                                    <td><input type="text" name="modeRate" id="modeRate' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);" onChange="findAdvanceAmount(' + iCnt + ')" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="initialAdvance" id="initialAdvance' + iCnt + '" value="" readonly  style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="endAdvance" id="endAdvance' + iCnt + '" value=""  readonly style="height:22px;width:160px;"/></td>\n\
                                    <input type="hidden" name="lHCNO" id="lHCNO' + iCnt + '" value="0"  style="height:22px;width:160px;" onKeyPress="return onKeyPressBlockCharacters(event);"/>\n\
                                    </tr></table>\n\
                                    <br><table border="0"><tr>\n\
                                    <td><input class="btn btn-info"  type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ',' + iCnt + ')" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>\n\
                                    <input class="btn btn-info"  type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove" onclick="deleteRow(' + iCnt + rowCnt + ')"  style="width:70px;height:30px;font-weight: bold;padding:1px;"/></td>\n\
                                    </tr></table></td></tr></table><br><br>');
                                        callOriginAjaxFullTruck(iCnt);
                                        callPointId1AjaxFullTruck(iCnt);
                                        callPointId2AjaxFullTruck(iCnt);
                                        callPointId3AjaxFullTruck(iCnt);
                                        callPointId4AjaxFullTruck(iCnt);
                                        callDestinationAjaxFullTruck(iCnt);
                                        $('#mainFullTruck').after(container);
                                        $('.datepicker').datepicker();
                                    });
                                    $('#btRemove').click(function() {
                                        var rows = $('#mainTableFullTruck tr').length;
                                        if (rows > 2) {
                                            $('#mainTableFullTruck').last(container).remove();
                                            iCnt = iCnt - 1;
                                        }
                                    });
                                    $('.datepicker').datepicker();
                                });

                                // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                var divValue, values = '';
                                function GetTextValue() {
                                    $(divValue).empty();
                                    $(divValue).remove();
                                    values = '';
                                    $('.input').each(function() {
                                        divValue = $(document.createElement('div')).css({
                                            padding: '5px', width: '200px'
                                        });
                                        values += this.value + '<br />'
                                    });
                                    $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                    $('body').append(divValue);
                                }

                                var x = 0;
                                var loadCnt = 0;
                                var loadCnt1 = 0;
                                var loadCnt2 = 0;
                                function addRow(val, v1) {

                                    loadCnt1 = v1;
                                    loadCnt2 = val;
                                    if (x == 0) {
                                        loadCnt = val;
                                    }

                                    var routeInfoSize = $('#routeInfoDetailsFullTruck' + loadCnt2 + ' tr').size();
                                    //alert(routeInfoSize);
                                    $('#routeInfoDetailsFullTruck' + loadCnt2 + ' tr').last().after(
                                            '<tr>\n\
        <td><span id="approvalStatusSpan' + loadCnt + '">Not Filled</span><td>' + routeInfoSize + '<input type="hidden" name="iCnt1" id="iCnt1' + loadCnt + '" value="' + loadCnt1 + '"/></td>\n\
        <td><input type="text" name="fromDate1" style="width:110px;"  id="fromDate1' + loadCnt + '' + routeInfoSize + '" class="datepicker form-control"  value=""/></td>\n\
        <td><input type="text" name="toDate1" style="width:110px;"  id="toDate1' + loadCnt + '' + routeInfoSize + '" class="datepicker form-control"  value=""/></td>\n\
        <td><input type="hidden" name="vehicleUnits" id="vehicleUnits' + loadCnt + '" value="1" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/><select type="text" name="vehicleTypeId" id="vehicleTypeId' + loadCnt + '" style="height:22px;width:160px;" onchange="setVehicleTypeDetails(this,' + loadCnt + ');setContainerTypeList(this.value,' + loadCnt + ');getMarketHireRate(' + loadCnt + ',' + loadCnt1 + ')"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
        <td><select name="loadTypeId" id="loadTypeId' + loadCnt + '" onchange="getMarketHireRate(' + loadCnt + ',' + loadCnt1 + ');"  style="height:22px;width:160px;" ><option value="1">Empty Trip</option><option value="2" selected>Load Trip</option></select></td>\n\
        <input type="hidden" name="spotCost" id="spotCost' + loadCnt + '" value="0" onkeyup="checkMarketRate(this.value,' + loadCnt + ')" style="height:22px;width:160px;"/>\n\
        <input type="hidden" name="marketRate" id="marketRate' + loadCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/><span id="marketRateSpan' + loadCnt + '"></span>\n\
        <td><input type="text" name="additionalCost" id="additionalCost' + loadCnt + '" value="0" onChange="findAdvanceAmount(' + loadCnt + ')" onkeyup="checkMarketRate(this.value,' + loadCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;" /></td>\n\
        <td><select name="advanceMode" id="advanceMode' + loadCnt + '"  onChange="findAdvanceAmount(' + loadCnt + ')" style="height:22px;width:160px;" >\n\
            <option value=0>-Select-</option>\n\
            <option value=1>PERCENTAGE</option>\n\
            <option value=2>FLAT</option>\n\
            <option value=3>CREDIT</option>\n\
        </select>\n\
        </td>\n\
        <td><input type="text" name="modeRate" id="modeRate' + loadCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);" onChange="findAdvanceAmount(' + loadCnt + ')" style="height:22px;width:160px;"/></td>\n\
        <td><input type="text" name="initialAdvance" id="initialAdvance' + loadCnt + '" value="" readonly  style="height:22px;width:160px;"/></td>\n\
        <td><input type="text" name="endAdvance" id="endAdvance' + loadCnt + '" value=""  readonly style="height:22px;width:160px;"/></td>\n\\n\
        <input type="hidden" name="lHCNO" id="lHCNO' + loadCnt + '" value="0"  style="height:22px;width:160px;" onKeyPress="return onKeyPressBlockCharacters(event);"/>\n\
        </tr>');
                                    $('#mainFullTruck').after(container);
                                    $('.datepicker').datepicker();
                                    loadCnt++;
                                    x++;
                                }

                                function deleteRow(val) {
                                    var loadCnt = val;
                                    var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                    var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                    if ($('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size() > 2) {
                                        $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().remove();
                                        loadCnt = loadCnt - 1;
                                    } else {
                                        alert('One row should be present in table');
                                    }
                                }
                                    </script>

                                    <script>
                                        function callOriginAjaxFullTruck(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
                                            //alert(val);
                                            //       var pointNameId = 'originNameFullTruck' + val;
                                            var pointNameId = 'originNameFullTruck' + val;
                                            var pointId = 'originIdFullTruck' + val;
                                            var desPointName = 'destinationNameFullTruck' + val;


                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getCityFromList.do",
                                                        dataType: "json",
                                                        data: {
                                                            //                                                    cityName: request.term,
                                                            cityName: $("#" + pointNameId).val(),
                                                            //                                                    cityName: $("#" + pointNameId).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                            //alert("asdfasdf")
                                                        },
                                                        error: function(data, type) {
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Value;
                                                    //alert("value ="+value);
                                                    var temp = value.split("-");
                                                    var textId = temp[0];
                                                    var textName = temp[1];
                                                    var id = ui.item.Id;
                                                    //alert("id ="+id);
                                                    //alert(id+" : "+value);
                                                    $('#' + pointId).val(textId);
                                                    $('#' + pointNameId).val(textName);
                                                    $('#' + desPointName).focus();
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                //var temp [] = "";
                                                var itemVal = item.Value;
                                                var temp = itemVal.split("-");
                                                var textId = temp[0];
                                                var t2 = temp[1];
                                                //                                        alert("t1 = "+t1)
                                                //                                        alert("t2 = "+t2)
                                                //alert("itemVal = "+itemVal)
                                                t2 = '<font color="green">' + t2 + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + t2 + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }
                                        function callPointId1AjaxFullTruck(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
//                                            alert(val);
                                            //       var pointNameId = 'originNameFullTruck' + val;
                                            var pointNameId = 'pointName1' + val;
                                            var pointId = 'pointId1' + val;
                                            var point2Name = 'pointName2' + val;


                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getCityFromList.do",
                                                        dataType: "json",
                                                        data: {
                                                            //                                                    cityName: request.term,
                                                            cityName: $("#" + pointNameId).val(),
                                                            //                                                    cityName: $("#" + pointNameId).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                            //alert("asdfasdf")
                                                        },
                                                        error: function(data, type) {
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Value;
                                                    //alert("value ="+value);
                                                    var temp = value.split("-");
                                                    var textId = temp[0];
                                                    var textName = temp[1];
                                                    var id = ui.item.Id;
                                                    //alert("id ="+id);
                                                    //alert(id+" : "+value);
                                                    $('#' + pointId).val(textId);
                                                    $('#' + pointNameId).val(textName);
                                                    $('#' + point2Name).focus();
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                //var temp [] = "";
                                                var itemVal = item.Value;
                                                var temp = itemVal.split("-");
                                                var textId = temp[0];
                                                var t2 = temp[1];
                                                //                                        alert("t1 = "+t1)
                                                //                                        alert("t2 = "+t2)
                                                //alert("itemVal = "+itemVal)
                                                t2 = '<font color="green">' + t2 + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + t2 + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }
                                        function callPointId2AjaxFullTruck(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
//                                            alert(val);
                                            //       var pointNameId = 'originNameFullTruck' + val;
                                            var pointNameId = 'pointName2' + val;
                                            var pointId = 'pointId2' + val;
                                            var point3Name = 'pointName3' + val;


                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getCityFromList.do",
                                                        dataType: "json",
                                                        data: {
                                                            //                                                    cityName: request.term,
                                                            cityName: $("#" + pointNameId).val(),
                                                            //                                                    cityName: $("#" + pointNameId).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                            //alert("asdfasdf")
                                                        },
                                                        error: function(data, type) {
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Value;
                                                    //alert("value ="+value);
                                                    var temp = value.split("-");
                                                    var textId = temp[0];
                                                    var textName = temp[1];
                                                    var id = ui.item.Id;
                                                    //alert("id ="+id);
                                                    //alert(id+" : "+value);
                                                    $('#' + pointId).val(textId);
                                                    $('#' + pointNameId).val(textName);
                                                    $('#' + point3Name).focus();
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                //var temp [] = "";
                                                var itemVal = item.Value;
                                                var temp = itemVal.split("-");
                                                var textId = temp[0];
                                                var t2 = temp[1];
                                                //                                        alert("t1 = "+t1)
                                                //                                        alert("t2 = "+t2)
                                                //alert("itemVal = "+itemVal)
                                                t2 = '<font color="green">' + t2 + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + t2 + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }
                                        function callPointId3AjaxFullTruck(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
//                                            alert(val);
                                            //       var pointNameId = 'originNameFullTruck' + val;
                                            var pointNameId = 'pointName3' + val;
                                            var pointId = 'pointId3' + val;
                                            var point4Name = 'pointName4' + val;


                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getCityFromList.do",
                                                        dataType: "json",
                                                        data: {
                                                            //                                                    cityName: request.term,
                                                            cityName: $("#" + pointNameId).val(),
                                                            //                                                    cityName: $("#" + pointNameId).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                            //alert("asdfasdf")
                                                        },
                                                        error: function(data, type) {
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Value;
                                                    //alert("value ="+value);
                                                    var temp = value.split("-");
                                                    var textId = temp[0];
                                                    var textName = temp[1];
                                                    var id = ui.item.Id;
                                                    //alert("id ="+id);
                                                    //alert(id+" : "+value);
                                                    $('#' + pointId).val(textId);
                                                    $('#' + pointNameId).val(textName);
                                                    $('#' + point4Name).focus();
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                //var temp [] = "";
                                                var itemVal = item.Value;
                                                var temp = itemVal.split("-");
                                                var textId = temp[0];
                                                var t2 = temp[1];
                                                //                                        alert("t1 = "+t1)
                                                //                                        alert("t2 = "+t2)
                                                //alert("itemVal = "+itemVal)
                                                t2 = '<font color="green">' + t2 + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + t2 + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }
                                        function callPointId4AjaxFullTruck(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
                                            //       var pointNameId = 'originNameFullTruck' + val;
                                            var pointNameId = 'pointName4' + val;
                                            var pointId = 'pointId4' + val;
                                            var desPointName = 'destinationNameFullTruck' + val;


                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getCityFromList.do",
                                                        dataType: "json",
                                                        data: {
                                                            //                                                    cityName: request.term,
                                                            cityName: $("#" + pointNameId).val(),
                                                            //                                                    cityName: $("#" + pointNameId).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                            //alert("asdfasdf")
                                                        },
                                                        error: function(data, type) {
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Value;
                                                    //alert("value ="+value);
                                                    var temp = value.split("-");
                                                    var textId = temp[0];
                                                    var textName = temp[1];
                                                    var id = ui.item.Id;
                                                    //alert("id ="+id);
                                                    //alert(id+" : "+value);
                                                    $('#' + pointId).val(textId);
                                                    $('#' + pointNameId).val(textName);
                                                    $('#' + desPointName).focus();
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                //var temp [] = "";
                                                var itemVal = item.Value;
                                                var temp = itemVal.split("-");
                                                var textId = temp[0];
                                                var t2 = temp[1];
                                                //                                        alert("t1 = "+t1)
                                                //                                        alert("t2 = "+t2)
                                                //alert("itemVal = "+itemVal)
                                                t2 = '<font color="green">' + t2 + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + t2 + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }


                                        //
                                        //
                                        //                                }


                                        function callDestinationAjaxFullTruck(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
                                            //alert(val);
                                            var pointNameId = 'destinationNameFullTruck' + val;
                                            var pointIdId = 'destinationIdFullTruck' + val;
                                            var originPointId = 'originIdFullTruck' + val;
                                            var pointId1 = 'pointId1' + val;
                                            var pointId2 = 'pointId2' + val;
                                            var pointId3 = 'pointId3' + val;
                                            var pointId4 = 'pointId4' + val;
                                            var truckRouteId = 'routeIdFullTruck' + val;
                                            var travelKm = 'travelKmFullTruck' + val;
                                            var travelHour = 'travelHourFullTruck' + val;
                                            var travelMinute = 'travelMinuteFullTruck' + val;


                                            //                                    if($("#" + originPointId).val() == ""){
                                            //                                        alert("id inside condition ==")
                                            //                                    }else{
                                            //                                        alert("else inside condition ==")
                                            //                                    }
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getCityToList.do",
                                                        dataType: "json",
                                                        data: {
                                                            cityName: $("#" + pointNameId).val(),
                                                            originCityId: $("#" + originPointId).val(),
                                                            pointId1: $("#" + pointId1).val(),
                                                            pointId2: $("#" + pointId2).val(),
                                                            pointId3: $("#" + pointId3).val(),
                                                            pointId4: $("#" + pointId4).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                        },
                                                        error: function(data, type) {

                                                            //console.log(type);
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.cityName;
                                                    var id = ui.item.cityId;
                                                    //                                            alert(id+" : "+value);
                                                    $('#' + pointIdId).val(id);
                                                    $('#' + pointNameId).val(value);
                                                    $('#' + travelKm).val(ui.item.Distance);
                                                    $('#' + travelHour).val(ui.item.TravelHour);
                                                    $('#' + travelMinute).val(ui.item.TravelMinute);
                                                    $('#' + truckRouteId).val(ui.item.RouteId);
                                                    $('#vehicleTypeId' + val).val(0);
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                var itemVal = item.cityName;
                                                itemVal = '<font color="green">' + itemVal + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + itemVal + "</a>")
                                                        .appendTo(ul);
                                            };
                                        }
                                    </script>

                                    <div id="routeFullTruck">

                                    </div>
                                    <br><br>

                                    <center>
                                        <input type="button" class="btn btn-info" value="Save" onclick="submitPage();" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>
                                    </center>

                                </div>

                                <div id="pcm" class="tab-pane" style="display:none">
                                    <div class="inpad">
                                        <table class="table table-info mb30 table-hover" width="98%" id="penality">
                                            <thead> <tr id="rowId0">
                                                    <th  align="center" height="50">S no</th>
                                                    <th  height="50" >Penality Cause</th>
                                                    <th  height="50" >Unit</th>
                                                    <th  height="50" >Amount</th>
                                                    <th  height="50" >Remark</th>
                                                </tr></thead>
                                            <tbody>
                                        <% 
                                            int sno = 0;				                                                    
                                            sno++;
                                        %>
                                        <tr>
                                            <td  align="center"> <%= sno%>  </td>
                                            <td><input type="text" name="penality" id="penality" class="form-control" value=""/></td>
                                            <td><input type="text" name="pcmunit" id="pcmunit" class="form-control" value=""/></td>
                                            <td><input type="text" name="chargeamount" id="chargeamount" class="form-control" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                            <td><input type="text" name="pcmremarks" id="pcmremarks" class="form-control" value=""/></td>

                                        </tr>
                                    <input type="hidden" name="selectedRowCount" id="sno" value="1"/>
                                    <input type="hidden" name="tripSheetId" id="tripSheetId" value="<%=request.getParameter("tripSheetId")%>" />
                                    </tbody>
                                </table>
                                <center>
                                    <br>
                                    <br>
                                    <INPUT type="button" class="btn btn-info" value="Add Row" onclick="addRowPenality()" />
                                    <!--<a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>-->
                                    <!--<INPUT type="button" class="button" value="Delete Row" onclick="deleteRowPenality()" />-->
                                </center>

                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-info" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    <a><input type="button" class="btn btn-info" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                </center>
                                <br>
                                <br>
                                <SCRIPT language="javascript">
                                    var poItems = 1;
                                    var rowCount = 2;
                                    var sno = 1;
                                    var snumber = 1;

                                    function addRowPenality()
                                    {
                                        //   alert(rowCount);
                                        if (sno < 20) {
                                            sno++;
                                            var tab = document.getElementById("penality");
                                            rowCount = tab.rows.length;
                                            // alert(rowCount);
                                            snumber = (rowCount) - 1;
                                            if (snumber == 1) {
                                                snumber = parseInt(rowCount);
                                            } else {
                                                snumber++;
                                            }
                                            // snumber = snumber - 1;
                                            //alert( (rowCount)-1) ;
                                            // alert(snumber);
                                            var newrow = tab.insertRow((rowCount));
                                            newrow.height = "30px";
                                            // var temp = sno1-1;
                                            var cell = newrow.insertCell(0);
                                            var cell0 = "<td  align='center'> " + snumber + "</td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(1);
                                            cell0 = "<td ><input type='text' name='penality' id='penality" + snumber + "' class='form-control' /></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(2);
                                            cell0 = "<td ><input type='text' name='pcmunit' id='pcmunit" + snumber + "' class='form-control' /></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(3);
                                            cell0 = "<td ><input type='text' name='chargeamount' id='chargeamount" + snumber + "' class='form-control' onKeyPress='return onKeyPressBlockCharacters(event);' /></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(4);
                                            cell0 = "<td ><input type='text' name='pcmremarks' id='pcmremarks" + snumber + "' class='form-control' /></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            //                                            if (rowCount == 1) {
                                            //                                                cell = newrow.insertCell(5);
                                            //                                                cell0 = "<td </td>";
                                            //                                                //cell.setAttribute(cssAttributeName,"text1");
                                            //                                                cell.innerHTML = cell0;
                                            //                                            } else {
                                            //                                                cell = newrow.insertCell(5);
                                            //                                                cell0 = "<td ><input type='checkbox' name='delete' id='delete" + snumber + "'  /></td>";
                                            //                                                //cell.setAttribute(cssAttributeName,"text1");
                                            //                                                cell.innerHTML = cell0;
                                            //                                            }
                                            rowCount++;
                                        }
                                    }

                                    //                                    function deleteRowPenality() {
                                    //                                        try {
                                    //                                            var table = document.getElementById("penality");
                                    //                                            rowCount = table.rows.length;
                                    //                                            for (var i = 0; i < rowCount; i++) {
                                    //                                                var row = table.rows[i];
                                    //                                                var chkbox = row.cells[5].childNodes[0];
                                    //                                                if (null != chkbox && true == chkbox.checked) {
                                    //                                                    if (rowCount <= 1) {
                                    //                                                        alert("Cannot delete all the rows.");
                                    //                                                        break;
                                    //                                                    }
                                    //                                                    table.deleteRow(i);
                                    //                                                    rowCount--;
                                    //                                                    i--;
                                    //                                                    snumber = i;
                                    //                                                }
                                    ////                        alert(snumber);
                                    //                                            }
                                    //                                        } catch (e) {
                                    //                                            alert(e);
                                    //                                        }
                                    //                                    }
                                </SCRIPT>
                            </div>

                        </div>
                        <div id="dcm" class="tab-pane" style="display:none">
                            <div class="inpad">
                                <table class="table table-info mb30 table-hover" width="90%"  id="detention">
                                    <thead><tr id="rowId1">
                                            <th   align="center" height="30">S No</th>
                                            <th   height="30" >Vehicle type</th>
                                            <th   height="30" >Time Slot From(Days)</th>
                                            <th   height="30" >Time Slot To(Days)</th>
                                            <th   height="30" >Amount</th>
                                            <th    height="30" >Remark</th>
                                        </tr></thead>

                                    <tbody>
                                    <td  align="center"> <%= sno%>  </td>
                                    <td><c:if test="${TypeList != null}">
                                            <select name="detention" id="detention"  class="form-control" >
                                                <option value="0">-Select-</option>
                                                <c:forEach items="${TypeList}" var="vehType">
                                                    <option value='<c:out value="${vehType.typeId}"/>'><c:out value="${vehType.typeName}"/></option>
                                                </c:forEach>
                                            </select>
                                        </c:if>
                                    </td>
                                    <td><select name="dcmunit" id="dcmunit" class="form-control" >
                                            <option value="0">--Select--</option>
                                            <c:forEach items="${detaintionTimeSlot}" var="detain">
                                                <option value='<c:out value="${detain.id}"/>'><c:out value="${detain.timeSlot}"/></option>
                                            </c:forEach>
                                        </select></td>
                                    <td><select name="dcmToUnit" id="dcmToUnit" class="form-control" >
                                            <option value="0">--Select--</option>
                                            <c:forEach items="${detaintionTimeSlot}" var="detain">
                                                <option value='<c:out value="${detain.id}"/>'><c:out value="${detain.timeSlot}"/></option>
                                            </c:forEach>
                                        </select></td>
                                    <td><input type="text" name="chargeamt" id="chargeamt" class="form-control" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                    <td><input type="text" name="dcmremarks" id="dcmremarks" class="form-control" value=""/></td>
                                    </tbody>
                                </table>
                                <center>
                                    <br>
                                    <br>
                                    <INPUT type="button" class="btn btn-info" value="Add Row" onclick="addRowdetention()" />
                                    <!--<INPUT type="button" class="button" value="Delete Row" onclick="deleteRowdetention()" />-->
                                </center>
                                <SCRIPT language="javascript">
                                    var poItems1 = 1;
                                    var rowCount1 = 2;
                                    var sno1 = 1;
                                    var snumber1 = 1;

                                    function addRowdetention()
                                    {
                                        //   alert(rowCount);
                                        if (sno1 < 20) {
                                            sno1++;
                                            var tab = document.getElementById("detention");
                                            rowCount1 = tab.rows.length;

                                            snumber1 = (rowCount1) - 1;
                                            if (snumber1 == 1) {
                                                snumber1 = parseInt(rowCount1);
                                            } else {
                                                snumber1++;
                                            }
                                            //                                            snumber1 = snumber1-1;
                                            //alert( (rowCount)-1) ;
                                            //alert(rowCount);
                                            var newrow = tab.insertRow((rowCount1));
                                            newrow.height = "30px";
                                            // var temp = sno1-1;
                                            var cell = newrow.insertCell(0);
                                            var cell0 = "<td  align='center'> " + snumber1 + "</td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(1);
                                            cell0 = "<td  ><select class='form-control' name='detention' id='detention" + snumber1 + "'><option value='0'>-Select-</option><c:if test = "${TypeList!= null}" ><c:forEach  items="${TypeList}" var="vehType"><option value='<c:out value="${vehType.typeId}" />'><c:out value="${vehType.typeName}" /></option></c:forEach ></c:if></select></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(2);
                                            cell0 = "<td  ><select  name='dcmunit' id='dcmunit" + snumber1 + "' class='form-control' /><option value='0'>-Select-</option><c:if test = "${detaintionTimeSlot!= null}" ><c:forEach  items="${detaintionTimeSlot}" var="detain"><option value='<c:out value="${detain.id}" />'><c:out value="${detain.timeSlot}" /></option></c:forEach ></c:if></select></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(3);
                                            cell0 = "<td  ><select  name='dcmToUnit' id='dcmToUnit" + snumber1 + "' class='form-control' /><option value='0'>-Select-</option><c:if test = "${detaintionTimeSlot!= null}" ><c:forEach  items="${detaintionTimeSlot}" var="detain"><option value='<c:out value="${detain.id}" />'><c:out value="${detain.timeSlot}" /></option></c:forEach ></c:if></select></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(4);
                                            cell0 = "<td  ><input type='text' name='chargeamt' id='chargeamt" + snumber1 + "' class='form-control' /></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(5);
                                            cell0 = "<td ><input type='text' name='dcmremarks' id='dcmremarks" + snumber1 + "' class='form-control' /></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            //                                            if (rowCount == 1) {
                                            //                                                cell = newrow.insertCell(5);
                                            //                                                cell0 = "<td </td>";
                                            //                                                //cell.setAttribute(cssAttributeName,"text1");
                                            //                                                cell.innerHTML = cell0;
                                            //                                            } else {
                                            //                                                cell = newrow.insertCell(5);
                                            //                                                cell0 = "<td ><input type='checkbox' name='delete' id='delete" + snumber + "'  /></td>";
                                            //                                                //cell.setAttribute(cssAttributeName,"text1");
                                            //                                                cell.innerHTML = cell0;
                                            //                                            }
                                            rowCount1++;
                                        }
                                    }

                                    //                                    function deleteRowdetention() {
                                    //                                        try {
                                    //                                            var table = document.getElementById("penality");
                                    //                                            rowCount = table.rows.length;
                                    //                                            for (var i = 0; i < rowCount; i++) {
                                    //                                                var row = table.rows[i];
                                    //                                                var chkbox = row.cells[5].childNodes[0];
                                    //                                                if (null != chkbox && true == chkbox.checked) {
                                    //                                                    if (rowCount <= 1) {
                                    //                                                        alert("Cannot delete all the rows.");
                                    //                                                        break;
                                    //                                                    }
                                    //                                                    table.deleteRow(i);
                                    //                                                    rowCount--;
                                    //                                                    i--;
                                    //                                                    snumber = i;
                                    //                                                }
                                    ////                        alert(snumber);
                                    //                                            }
                                    //                                        } catch (e) {
                                    //                                            alert(e);
                                    //                                        }
                                    //                                    }
                                        </SCRIPT>

                                        <center>
                                            <a><input type="button" class="btn btn-info" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>

                                        </center>

                                        <br>
                                        <!--                                        <center>
                                                                                    <input type="button" class="btn btn-success" value="Save" style="width: 120px" onclick="submitPage(this.value)"/>
                                                                                </center>-->

                                        <br>
                                        <br>

                                        <br>
                                        <br>
                                    </div>
                                    </div>


                                    <!--<script>
                                        function saveVendorContract(){
                                            document.customerContract.action = "/throttle/saveVehicleVendorContract.do";
                                            document.customerContract.submit();
                                        }
                                     </script>-->
                                    <div id="deD"  class="tab-pane active" style="padding-left: 1px; display: none" >
                                        <script>
                                            var contain = "";
                                            $(document).ready(function() {
                                                var iCnt = 1;
                                                var rowCnt = 1;
                                                //alert(rowCnt)
                                                // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                                contain = $($("#routedeD")).css({
                                                    padding: '5px', margin: '20px', width: '100%', border: '0px dashed',
                                                    borderTopColor: '#999', borderBottomColor: '#999',
                                                    borderLeftColor: '#999', borderRightColor: '#999'
                                                });
                                                $(contain).last().after('<table align="center"  id="routeDetails1' + iCnt + '"  border="1" class="table table-info mb30 table-hover" style="width:90%;">\n\
                                            <thead >\n\
                                            <tr id="tableDesingTH" height="30">\n\
                                            <td>Sno</td>\n\
                                            <td>Vehicle Type</td>\n\
                                            <td>Vehicle Units</td>\n\
                                             <td>Contract Category</td>\n\
                                           <td>Fixed Cost Per Vehicle & Month</td>\n\
                                            <td colspan="2"><br>Fixed Duration <br>Per day<br>\n\
                                            <table  border="0">\n\
                                              <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;"><td width="235px;">Hours</td><td width="206px;">Minutes</td></tr></table></td>\n\
                                            <td>Rate Per KM</td>\n\
                                            <td>Rate Exceeds Limit KM</td>\n\
                                            <td>Max Allowable KM Per Month</td>\n\
                                            <td colspan="2"><br><br>Over Time Cost Per HR<br><br>\n\
                                            <table  border="0">\n\
                                              <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;"><td width="245px;">Work Days</td><td width="216px;height="20px;">Holidays</td></tr></table></td>\n\
                                            <td>Additional Cost</td></tr>\n\
                                             <tr>\n\
                                           <td> ' + iCnt + ' </td>\n\
                                           <td><select type="text" name="vehicleTypeIdDedicate" id="vehicleTypeIdDedicate' + iCnt + '" onchange="onSelectVal(this.value,' + iCnt + ');" ><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                           <td><input type="text" name="vehicleUnitsDedicate" id="vehicleUnitsDedicate' + iCnt + '" value="0" onchange="calculateTotalFixedCost(' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:80px;"/></td>\n\
                                            <td><select type="text" name="contractCategory" id="contractCategory' + iCnt + '" onchange="setTextBoxEnable(this.value,' + iCnt + ');" style="height:22px;width:80px;"><option value="0">--Select--</option>\n\
                                                <option value="1" >Fixed</option>\n\
                                                 <option value="2" >Actual</option></select></td>\n\
                                           <td><input type="text" name="fixedCost" id="fixedCost' + iCnt + '" value="0" onchange="calculateTotalFixedCost(' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                           <td><select name="fixedHrs" id="fixedHrs' + iCnt + '" class="textbox" style="height:22px;width:45px;">\n\
                                                      <option value="00">00</option>\n\
                                                    <option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option></select>\n\
                                           </td>\n\
                                            <td><select type="text" name="fixedMin" id="fixedMin' + iCnt + '" class="textbox" style="height:22px;width:45px;"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>\n\
                                           </td>\n\
                                            <input type="hidden" readonly name="totalFixedCost" id="totalFixedCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" />\n\
                                            <td><input type="text" name="rateCost" id="rateCost' + iCnt + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                            <td><input type="text" name="rateLimit" id="rateLimit' + iCnt + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                            <td><input type="text" name="maxAllowableKM" id="maxAllowableKM' + iCnt + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                            <td><input type="text" name="workingDays" id="workingDays' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:50px;"/></td>\n\
                                            <td><input type="text" name="holidays" id="holidays' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:50px;"/></td>\n\
                                            <td><input type="text" name="addCostDedicate" id="addCostDedicate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                             </tr>\n\
                                            </table>\n\
                                           <br><table border="0" id="buttonDetails1' + iCnt + '" ><tr>\n\
                                            <td>\n\
                                            <input class="btn btn-info" type="button" name="addRouteDetailsFullTruck1" id="addRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="Add" onclick="addRows(' + iCnt + ')" style="width:90px;height:30px;font-weight: bold;padding: 1px;">\n\
                                            <input class="btn btn-info" type="button" name="removeRouteDetailsFullTruck1" id="removeRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRows(' + iCnt + ')" style="width:90px;height:30px;font-weight: bold;padding: 1px;">\n\
                                       </tr></thead></table></td></tr></table>');
                                                callOriginAjaxdeD(iCnt);
                                                callDestinationAjaxdeD(iCnt);
                                                $('#btAdd').click(function() {
                                                    iCnt = iCnt + 1;
                                                    callOriginAjaxdeD(iCnt);
                                                    callDestinationAjaxdeD(iCnt);
                                                    $('#maindeD').after(contain);
                                                });
                                                $('#btRemove').click(function() {
                                                    var rows = $('#mainTableFullTruck tr').length;
                                                    if (rows > 2) {
                                                        $('#mainTableFullTruck').last(container).remove();
                                                        iCnt = iCnt - 1;
                                                    }
                                                });
                                            });

                                            // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                            var divValue, values = '';
                                            function GetTextValue() {
                                                $(divValue).empty();
                                                $(divValue).remove();
                                                values = '';
                                                $('.input').each(function() {
                                                    divValue = $(document.createElement('div')).css({
                                                        padding: '5px', width: '200px'
                                                    });
                                                    values += this.value + '<br />'
                                                });
                                                $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                                $('body').append(divValue);
                                            }

                                            $(document).ready(function() {
                                                $("#mAllow").keyup(function() {
                                                    //                                        alert($(this).val());
                                                });
                                            })

                                            function addRows(val) {
                                                //alert(val);
                                                var loadCnt = val;
                                                //    alert(loadCnt)
                                                //alert("loadCnt");
                                                //var loadCnt1 = ve;
                                                var routeInfoSize = $('#routeDetails1' + loadCnt + ' tr').size();
                                                // alert(routeInfoSize);
                                                var routeInfoSizeSub = routeInfoSize - 2;
                                                
                                                $('#routeDetails1' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSizeSub + '</td>\n\
                                                \n\
                                               <td><select type="text" name="vehicleTypeIdDedicate" id="vehicleTypeIdDedicate' + routeInfoSizeSub + '" onchange="onSelectVal(this.value,' + routeInfoSizeSub + ');" ><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                           <td><input type="text" name="vehicleUnitsDedicate" id="vehicleUnitsDedicate' + routeInfoSizeSub + '" value="0" onchange="calculateTotalFixedCost(' + routeInfoSizeSub + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:80px;"/></td>\n\
                                            <td><select type="text" name="contractCategory" id="contractCategory' + routeInfoSizeSub + '" onchange="setTextBoxEnable(this.value,' + routeInfoSizeSub + ');" style="height:22px;width:80px;"><option value="0">--Select--</option>\n\
                                                <option value="1" >Fixed</option>\n\
                                                 <option value="2" >Actual</option></select></td>\n\
                                           <td><input type="text" name="fixedCost" id="fixedCost' + routeInfoSizeSub + '" value="0" onchange="calculateTotalFixedCost(' + routeInfoSizeSub + ')" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                           <td><select name="fixedHrs" id="fixedHrs' + routeInfoSizeSub + '" class="textbox" style="height:22px;width:45px;">\n\
                                                      <option value="00">00</option>\n\
                                                    <option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option></select>\n\
                                           </td>\n\
                                            <td><select type="text" name="fixedMin" id="fixedMin' + routeInfoSizeSub + '" class="textbox" style="height:22px;width:45px;"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>\n\
                                           </td>\n\
                                            <input type="hidden" readonly name="totalFixedCost" id="totalFixedCost' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" />\n\
                                            <td><input type="text" name="rateCost" id="rateCost' + routeInfoSizeSub + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                            <td><input type="text" name="rateLimit" id="rateLimit' + routeInfoSizeSub + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                            <td><input type="text" name="maxAllowableKM" id="maxAllowableKM' + routeInfoSizeSub + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                            <td><input type="text" name="workingDays" id="workingDays' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:50px;"/></td>\n\
                                            <td><input type="text" name="holidays" id="holidays' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:50px;"/></td>\n\
                                            <td><input type="text" name="addCostDedicate" id="addCostDedicate' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                             </tr>\n\
                                                  </tr>');
                                                // alert("loadCnt = "+loadCnt)
                                                loadCnt++;
                                                //   alert("loadCnt = +"+loadCnt)
                                            }

                                            function onSelectVal(val, countVal) {
                                                //                            alert("vehicleTypeIdDedicate"+val);
                                                document.getElementById("vehicleTypeIddeDTemp" + countVal).value = val;
                                            }


                                            function deleteRows(val) {
                                                var loadCnt = val;
                                                //     alert(loadCnt);
                                                var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                                                var routeInfoDetails = "routeDetails1" + loadCnt;
                                                // alert(routeInfoDetails);
                                                if ($('#routeDetails1' + loadCnt + ' tr').size() > 4) {
                                                    $('#routeDetails1' + loadCnt + ' tr').last().remove();
                                                    loadCnt = loadCnt - 1;
                                                } else {
                                                    alert('One row should be present in table');
                                                }
                                            }

                                            function calculateTotalFixedCost(sno) {
                                                //                                    alert(sno);
                                                var vehicleUnits = document.getElementById("vehicleUnitsDedicate" + sno).value;
                                                var fixedCost = document.getElementById("fixedCost" + sno).value;
                                                var totalCost = parseInt(vehicleUnits) * parseInt(fixedCost);
                                                document.getElementById("totalFixedCost" + sno).value = totalCost;
                                            }

                                        </script>

                                        <script>
                                               function setTextBoxEnable(val, count) {

                                                    if (val == 1) {  //fixed part
                                                        document.getElementById("rateCost" + count).readOnly = true;
                                                        document.getElementById("rateLimit" + count).readOnly = false;
                                                        document.getElementById("maxAllowableKM" + count).readOnly = false;
                                                        document.getElementById("totalFixedCost" + count).readOnly = false;
                                                        document.getElementById("fixedCost" + count).readOnly = false;
                                                        document.getElementById("rateCost" + count).value = 0;
                                                    }
                                                    if (val == 2) {
                                                        document.getElementById("rateCost" + count).readOnly = false;
                                                        document.getElementById("rateLimit" + count).readOnly = true;
                                                        document.getElementById("maxAllowableKM" + count).readOnly = true;
                                                        document.getElementById("totalFixedCost" + count).readOnly = true;
                                                        document.getElementById("fixedCost" + count).readOnly = true;

                                                        document.getElementById("totalFixedCost" + count).value = 0;
                                                        document.getElementById("fixedCost" + count).value = 0;
                                                        document.getElementById("rateLimit" + count).value = 0;
                                                        document.getElementById("maxAllowableKM" + count).value = 0;

                                                    }

                                                }
                                            
                                            function callOriginAjaxdeD(val) {
                                                // Use the .autocomplete() method to compile the list based on input from user
                                                //alert(val);
                                                var pointNameId = 'originNamedeD' + val;
                                                var pointIdId = 'originIddeD' + val;
                                                var desPointName = 'destinationNamedeD' + val;


                                                //alert(prevPointId);
                                                $('#' + pointNameId).autocomplete({
                                                    source: function(request, response) {
                                                        $.ajax({
                                                            url: "/throttle/getTruckCityList.do",
                                                            dataType: "json",
                                                            data: {
                                                                cityName: request.term,
                                                                textBox: 1
                                                            },
                                                            success: function(data, textStatus, jqXHR) {
                                                                var items = data;
                                                                response(items);
                                                            },
                                                            error: function(data, type) {

                                                                //console.log(type);
                                                            }
                                                        });
                                                    },
                                                    minLength: 1,
                                                    select: function(event, ui) {
                                                        var value = ui.item.Name;
                                                        var id = ui.item.Id;
                                                        //alert(id+" : "+value);
                                                        $('#' + pointIdId).val(id);
                                                        $('#' + pointNameId).val(value);
                                                        $('#' + desPointName).focus();
                                                        //validateRoute(val,value);

                                                        return false;
                                                    }

                                                    // Format the list menu output of the autocomplete
                                                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                    //alert(item);
                                                    var itemVal = item.Name;
                                                    itemVal = '<font color="green">' + itemVal + '</font>';
                                                    return $("<li></li>")
                                                            .data("item.autocomplete", item)
                                                            .append("<a>" + itemVal + "</a>")
                                                            .appendTo(ul);
                                                };


                                            }

                                            function callDestinationAjaxdeD(val) {
                                                // Use the .autocomplete() method to compile the list based on input from user
                                                //alert(val);
                                                var pointNameId = 'destinationNamedeD' + val;
                                                var pointIdId = 'destinationIddeD' + val;
                                                var originPointId = 'originIddeD' + val;
                                                var truckRouteId = 'routeIddeD' + val;
                                                var travelKm = 'travelKmdeD' + val;
                                                var travelHour = 'travelHourdeD' + val;
                                                var travelMinute = 'travelMinutedeD' + val;

                                                //alert(prevPointId);
                                                $('#' + pointNameId).autocomplete({
                                                    source: function(request, response) {
                                                        $.ajax({
                                                            url: "/throttle/getTruckCityList.do",
                                                            dataType: "json",
                                                            data: {
                                                                cityName: request.term,
                                                                originCityId: $("#" + originPointId).val(),
                                                                textBox: 1
                                                            },
                                                            success: function(data, textStatus, jqXHR) {
                                                                var items = data;
                                                                response(items);
                                                            },
                                                            error: function(data, type) {

                                                                //console.log(type);
                                                            }
                                                        });
                                                    },
                                                    minLength: 1,
                                                    select: function(event, ui) {
                                                        var value = ui.item.Name;
                                                        var id = ui.item.Id;
                                                        //alert(id+" : "+value);
                                                        $('#' + pointIdId).val(id);
                                                        $('#' + pointNameId).val(value);
                                                        $('#' + travelKm).val(ui.item.TravelKm);
                                                        $('#' + travelHour).val(ui.item.TravelHour);
                                                        $('#' + travelMinute).val(ui.item.TravelMinute);
                                                        $('#' + truckRouteId).val(ui.item.RouteId);
                                                        //validateRoute(val,value);

                                                        return false;
                                                    }

                                                    // Format the list menu output of the autocomplete
                                                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                    //alert(item);
                                                    var itemVal = item.Name;
                                                    itemVal = '<font color="green">' + itemVal + '</font>';
                                                    return $("<li></li>")
                                                            .data("item.autocomplete", item)
                                                            .append("<a>" + itemVal + "</a>")
                                                            .appendTo(ul);
                                                };


                                            }

                                        </script>

                                        <div id="routedeD">

                                        </div>
                                        <br>
                                        <br>
                                        <center>
                                            <a><input type="button" class="btn btn-info btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                            &nbsp;&nbsp;&nbsp;
                                            <input type="button" class="btn btn-info" value="Save" onclick="submitPage1();" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>
                                        </center>
                                        <br>
                                        <br>
                                    </div>

                                    <script>
                                        $('.btnNext').click(function() {
                                            $('.nav-tabs > .active').next('li').find('a').trigger('click');
                                        });
                                        $('.btnPrevious').click(function() {
                                            $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                                        });
                                    </script>

                                

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                    <%--   <%}catch(Exception e)
                 {
                        out.println(e.toString());
                    }
               %>--%>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>