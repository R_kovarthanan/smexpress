<%--
    Document   : contractPointToPointWeight
    Created on : Jan 27, 2015, 8:35:48 PM
    Author     : Nivan
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script>
    function submitPage() {
        document.vehicleVendorContract.action = '/throttle/saveTransportCustomerDailyContract.do';
        document.vehicleVendorContract.submit();
    }
</script>
   <div class="pageheader">
      <h2><i class="fa fa-edit"></i>  Create Daily Contract </h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>
          <li><a href="general-forms.html">Leasing Operation</a></li>
          <li class="active">Create Daily Contract</li>
        </ol>
      </div>
      </div>
<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
    <body>
        <%--  <%try{%>--%>

        <style>
            body {
                font:13px verdana;
                font-weight:normal;
            }
        </style>



        <form name="vehicleVendorContract" method="post" >
            
            <br>


           <table class="table table-info mb30 table-hover" >
                <tr >
                    <td class="contenthead" colspan="4" >Customer Contract Info</td>
                </tr>
                <tr>
                    <td class="text1">Customer Name</td>
                    <td class="text1"><input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>" class="textbox"><c:out value="${customerName}"/></td>
                    <td class="text1">Customer Code</td>
                    <td class="text1"><c:out value="${customerCode}"/></td>
                    
                </tr>
                <tr>
                    <td height="30">Contract From</td>
                    <td><input type="text" name="startDate" id="startDate" value="<c:out value="${startDate}" />" class="datepicker">
                    </td>

                    <td height="30">Contract To</td>
                    <td><input type="text" name="endDate" id="endDate" value="<c:out value="${endDate}" />" class="datepicker"></td>
                </tr>
                
            </table>
            <br>

            <div id="tabs">
                <ul class="">
                    <li><a href="#veh"><span>Daily Contract Flat</span></a></li>
                    <li><a href="#trailer"><span>Daily Contract (Actual KM)</span></a></li>
                   
                    <!--<li><a href="#weightBreak"><span>Weight Break </span></a></li>-->
                </ul>
                
                <div id="trailer">
                    <div id="mainFullTruck" class="contenthead2" style="width: 72%">
                        
                    </div>
                    
                    <script>
                        var container = "";
                        $(document).ready(function() {
                            var iCnt = 1;
                            var rowCnt = 1;
                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                            container = $($("#routeFullTruck")).css({
                                padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                borderTopColor: '#999', borderBottomColor: '#999',
                                borderLeftColor: '#999', borderRightColor: '#999'
                            });
                            $(container).last().after('<table id="mainTableFullTruck" width="100%"><tr></td>\
                             <table  class="table table-info mb30 table-hover" id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1">\n\
                           <thead> <tr><th>Sno</th><th>Vehicle Type</th><th>Trailer Type</th><th>Rate(per Km)</th></tr>\n\
                            <tr></thead>\n\
                            <td>' + rowCnt + '<input type="hidden" name="iCnt1" id="iCnt1' + iCnt + '" value="' + iCnt + '"/></td>\n\
                           <td><select type="text" name="actualKmVehicleTypeId" id="actualKmVehicleTypeId' + iCnt + '" onchange="onSelectVal(this.value,' + iCnt + ');"><option value="0">--Select--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td>\n\
                            <td><select type="text" name="actualKmTrailerTypeId" id="actualKmTrailerTypeId' + iCnt + '"><option value="0">--Select--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="trailer"><option value="<c:out value="${trailer.trailerId}"/>"><c:out value="${trailer.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                           <td><input type="text" name="actualKmFixedCostPerVehicle" id="actualKmFixedCostPerVehicle' + iCnt + '" value="0"  onchange="calculateTotalVehicleFixedCost(' + iCnt + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></td> \n\
                              </tr></table>\n\
                            <table border="" width=><tr>\n\
                            <td><input class="button" type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ',' + rowCnt + ')" />\n\
                            <input class="button" type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRow(' + iCnt + rowCnt + ')" /></td>\n\
                           </tr></table></td></tr></table><br><br>');
                          //  callOriginAjaxFullTruck(iCnt);
                          //  callDestinationAjaxFullTruck(iCnt);
                            $('#btAdd').click(function() {
                                iCnt = iCnt + 1;
                                $(container).last().after('<tr><td>' + rowCnt + '<input type="hidden" name="iCnt1" id="iCnt1' + iCnt + '" value="' + iCnt + '"/></td>\n\
                            <td><select type="text" name="actualKmVehicleTypeId" id="actualKmVehicleTypeId' + iCnt + '" onchange="onSelectVal(this.value,' + iCnt + ');"><option value="0">--Select--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td>\n\
                            <td><select type="text" name="actualKmTrailerTypeId" id="actualKmTrailerTypeId' + iCnt + '"><option value="0">--Select--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="trailer"><option value="<c:out value="${trailer.trailerId}"/>"><c:out value="${trailer.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                           <td><input type="text" name="actualKmFixedCostPerVehicle" id="actualKmFixedCostPerVehicle' + iCnt + '" value="0"  onchange="calculateTotalVehicleFixedCost(' + iCnt + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                            </tr></table>\n\
                            <table border="" align="center" width=""><tr>\n\
                            <td><input class="button"  type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ','+ iCnt +')" />\n\
                            <input class="button"  type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove" onclick="deleteRow(' + iCnt + rowCnt + ')"  /></td>\n\
                           </tr></table></td></tr></table><br><br>');
                              //  callOriginAjaxFullTruck(iCnt);
                              //  callDestinationAjaxFullTruck(iCnt);
                                $('#mainFullTruck').after(container);
                            });
                            $('#btRemove').click(function() {
//                                alert($('#mainTableFullTruck tr').size());
                                if ($(container).size() > 1) {
                                    $(container).last().remove();
                                    iCnt = iCnt - 1;
                                }
                            });
                        });

                        // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                        var divValue, values = '';
                        function GetTextValue() {
                            $(divValue).empty();
                            $(divValue).remove();
                            values = '';
                            $('.input').each(function() {
                                divValue = $(document.createElement('div')).css({
                                    padding: '5px', width: '200px'
                                });
                                values += this.value + '<br />'
                            });
                            $(divValue).append('<p><b>Your selected values</b></p>' + values);
                            $('body').append(divValue);
                        }

                        function addRow(val,v1) {
//                            alert(val);
//                            alert(v1);
                            var loadCnt = val;
                            var loadCnt1 = v1;
                            var routeInfoSize = $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size()-1;
                            //alert(routeInfoSize);
                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                            $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSize + '<input type="hidden" name="iCnt1" id="iCnt1' + loadCnt + '" value="' + loadCnt1 + '"/></td><td><select type="text" name="vehicleTypeId" id="vehicleTypeId' + loadCnt + '" onchange="onSelectVal(this.value,' + loadCnt + ');"><option value="0">--Select--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td><td><select type="text" name="trailerTypeId" id="trailerTypeId' + loadCnt + '"><option value="0">--Select--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="vehType"><option value="<c:out value="${vehType.seatCapacity}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td><td><input type="text" name="fixedCostPerVehicle" id="fixedCostPerVehicle' + loadCnt + '" value="0"  onchange="calculateTotalVehicleFixedCost(' + loadCnt + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></td></tr>');
                            loadCnt++;
                        }
                        function deleteRow(val) {
                            var loadCnt = val;

                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                            if ($('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size() > 2) {
                                $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().remove();
                                loadCnt = loadCnt - 1;
                            } else {
                                alert('One row should be present in table');
                            }
                        }
                            </script>

                           

                            <div id="routeFullTruck">

                            </div>


                             <a  class="pretab" href=""><input type="button" class="button" value="Previous" name="Previous" /></a> &nbsp;
                            
                        </div>

                        <!--<script>
                            function saveVendorContract(){
                                document.customerContract.action = "/throttle/saveVehicleVendorContract.do";
                                document.customerContract.submit();
                            }
                         </script>-->
                        <div id="veh">
                            <script>
                                var contain = "";
                                $(document).ready(function() {
                                    var iCnt = 1;
                                    var rowCnt = 1;
                                    // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                    contain = $($("#routedeD")).css({
                                        padding: '5px', margin: '20px', width: '100%', border: '0px dashed',
                                        borderTopColor: '#999', borderBottomColor: '#999',
                                        borderLeftColor: '#999', borderRightColor: '#999'
                                    });
                                    $(contain).last().after('<table class="table table-info mb30 table-hover" id="mainTableFullTruck" width="100%"><tr></td>\n\
                                  <table  class="table table-info mb30 table-hover"  id="routeDetails1' + iCnt + '"  border="1">\n\
                                    <thead><tr >\n\
                                    <th>Sno</th>\n\
                                    <th><center>Vehicle Type</center></th>\n\
                                    <th><center>Trailer Type</center></th>\n\
                                    <th><center>Fixed Cost per vehicle</center></th>\n\
                                    <tr></thead>\n\
                                   <td> ' + iCnt + ' </td>\n\
                                   <td><select type="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '" onchange="onSelectVal(this.value,' + iCnt + ');"><option value="0">--Select--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td>\n\
                                   <td><select type="text" name="trailerTypeId" id="trailerTypeId' + iCnt + '"><option value="0">--Select--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="trailer"><option value="<c:out value="${trailer.trailerId}"/>"><c:out value="${trailer.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                                   <td><input type="text" name="fixedCostPerVehicle" id="fixedCostPerVehicle' + iCnt + '" value="0"  onchange="calculateTotalVehicleFixedCost(' + iCnt + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    </tr>\n\
                                    </table>\n\
                                    <table align ="center" border="" width=""><tr>\n\
                                    <td><input class="button" type="button" name="addRouteDetailsFullTruck1" id="addRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="Add" onclick="addRows(' + iCnt + ')" />\n\
                                    <input class="button" type="button" name="removeRouteDetailsFullTruck1" id="removeRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRows(' + iCnt + ')" />\n\
                                    </tr></table></td></tr></table><br><br>');
                                    callOriginAjaxdeD(iCnt);
                                    callDestinationAjaxdeD(iCnt);
                                    $('#btAdd').click(function() {
                                        iCnt = iCnt + 1;

                                        callOriginAjaxdeD(iCnt);
                                        callDestinationAjaxdeD(iCnt);
                                        $('#maindeD').after(contain);
                                    });
                                    $('#btRemove').click(function() {
//                                        alert($('#mainTabledeD tr').size());
                                        if ($(contain).size() > 2) {
                                            $(contain).last().remove();
                                            iCnt = iCnt - 1;
                                        }
                                    });
                                });

                                // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                var divValue, values = '';
                                function GetTextValue() {
                                    $(divValue).empty();
                                    $(divValue).remove();
                                    values = '';
                                    $('.input').each(function() {
                                        divValue = $(document.createElement('div')).css({
                                            padding: '5px', width: '200px'
                                        });
                                        values += this.value + '<br />'
                                    });
                                    $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                    $('body').append(divValue);
                                }

                                $(document).ready(function() {
                                    $("#mAllow").keyup(function() {
//                                        alert($(this).val());
                                    });
                                })



                                function addRows(val) {
                                    //alert(val);
                                    var loadCnt = val;
                                    //    alert(loadCnt)
                                    //alert("loadCnt");
                                    //var loadCnt1 = ve;
                                    var routeInfoSize = $('#routeDetails1' + loadCnt + ' tr').size();
                                   //  alert(routeInfoSize);
                                    var routeInfoSizeSub = routeInfoSize-1;
                                    var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                                    var routeInfoDetails = "routeDetails1" + loadCnt;
                                    $('#routeDetails1' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSizeSub + '</td>\n\
                        <%--   <td><input type="text" name="vehicleTypeIddeDTemp" id="vehicleTypeIddeDTemp' + loadCnt + '" /></td>--%>\n\
                                   <td><select ype="text" name="vehicleTypeId" id="vehicleTypeId' + routeInfoSizeSub + '" onchange="onSelectVal(this.value,' + routeInfoSizeSub + ');"><option value="0">--Select--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td>\n\
                                   <td><select type="text" name="trailerTypeId" id="trailerTypeId' + routeInfoSizeSub + '"><option value="0">--Select--</option><c:if test="${trailerTypeList != null}"><c:forEach items="${trailerTypeList}" var="trailer"><option value="<c:out value="${trailer.trailerId}"/>"><c:out value="${trailer.seatCapacity}"/></option></c:forEach></c:if></select></td>\n\
                                   <td><input type="text" name="fixedCostPerVehicle" id="fixedCostPerVehicle' + routeInfoSizeSub + '" value="0" onchange="calculateTotalVehicleFixedCost(' + routeInfoSizeSub + ');" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                   </tr>');
                                    // alert("loadCnt = "+loadCnt)
                                    loadCnt++;
                                    //   alert("loadCnt = +"+loadCnt)
                                }
                                function onSelectVal(val, countVal) {
                                    //                            alert("vehicleTypeIdDedicate"+val);
                                    document.getElementById("vehicleTypeIddeDTemp" + countVal).value = val;
                                }


                                function deleteRows(val) {
                                    var loadCnt = val;
                                    //     alert(loadCnt);
                                    var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                                    var routeInfoDetails = "routeDetails1" + loadCnt;
                                    // alert(routeInfoDetails);
                                    if ($('#routeDetails1' + loadCnt + ' tr').size() > 2) {
                                        $('#routeDetails1' + loadCnt + ' tr').last().remove();
                                        loadCnt = loadCnt - 1;
                                    } else {
                                        alert('One row should be present in table');
                                    }
                                }

                                function calculateTotalTrailerFixedCost(sno) {
//                                    alert(sno);
                                    var trailerUnits = document.getElementById("trailerTypeUnits" + sno).value;
                                    var fixedCost = document.getElementById("fixedCostPerTrailer" + sno).value;
                                    var totalCost = parseInt(trailerUnits) * parseInt(fixedCost);
                                    document.getElementById("totalTrailerCost" + sno).value = totalCost;
                                }
                                function calculateTotalVehicleFixedCost(sno) {
                                //alert(sno);
                                    var vehicleUnits = document.getElementById("vehicleUnits" + sno).value;
                                    var fixedCost = document.getElementById("fixedCostPerVehicle" + sno).value;
                                    var totalCost = parseInt(vehicleUnits) * parseInt(fixedCost);
                                    document.getElementById("totlaFixedCost" + sno).value = totalCost;
                                }
                               function setTextBoxEnable(val,count){
                                //    alert("enterd----"+val);
                              //  alert(count);
                                   
                                                 if(val==1){
                                                    document.getElementById("rateCost"+count).readOnly=true;
                                                    document.getElementById("rateLimit"+count).readOnly=false;
                                                    document.getElementById("maxAllowableKM"+count).readOnly=false;
                                                }
                                                  if(val==2){
                                                     
                                                document.getElementById("rateCost"+count).readOnly=false;
                                                document.getElementById("rateLimit"+count).readOnly=true;
                                                document.getElementById("maxAllowableKM"+count).readOnly=true;
                                                 
                                               }
                                                        }

                                                        function showVehicleExtraKmRun(){
                                                            var billingKmType=document.getElementById("billingKmCalculationId").value;
                                                          
                                                             var tbl = document.getElementById("routeDetails11");
                                                            if(billingKmType==1){
                                                               for (var i = 0; i < tbl.rows.length; i++) {
                                                          
                                                                        tbl.rows[i].cells[6].style.display ="none";

                                                           
                                                                 }

                                                            }else{
                                                              for (var i = 0; i < tbl.rows.length; i++) {

                                                                        tbl.rows[i].cells[6].style.display ="block";


                                                                 }
                                                            }

                                                        }
                            </script>

                            <script>
                                function callOriginAjaxdeD(val) {
                                    // Use the .autocomplete() method to compile the list based on input from user
                                    //alert(val);
                                    var pointNameId = 'originNamedeD' + val;
                                    var pointIdId = 'originIddeD' + val;
                                    var desPointName = 'destinationNamedeD' + val;


                                    //alert(prevPointId);
                                    $('#' + pointNameId).autocomplete({
                                        source: function(request, response) {
                                            $.ajax({
                                                url: "/throttle/getTruckCityList.do",
                                                dataType: "json",
                                                data: {
                                                    cityName: request.term,
                                                    textBox: 1
                                                },
                                                success: function(data, textStatus, jqXHR) {
                                                    var items = data;
                                                    response(items);
                                                },
                                                error: function(data, type) {

                                                    //console.log(type);
                                                }
                                            });
                                        },
                                        minLength: 1,
                                        select: function(event, ui) {
                                            var value = ui.item.Name;
                                            var id = ui.item.Id;
                                            //alert(id+" : "+value);
                                            $('#' + pointIdId).val(id);
                                            $('#' + pointNameId).val(value);
                                            $('#' + desPointName).focus();
                                            //validateRoute(val,value);

                                            return false;
                                        }

                                        // Format the list menu output of the autocomplete
                                    }).data("autocomplete")._renderItem = function(ul, item) {
                                        //alert(item);
                                        var itemVal = item.Name;
                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                        return $("<li></li>")
                                                .data("item.autocomplete", item)
                                                .append("<a>" + itemVal + "</a>")
                                                .appendTo(ul);
                                    };


                                }

                                function callDestinationAjaxdeD(val) {
                                    // Use the .autocomplete() method to compile the list based on input from user
                                    //alert(val);
                                    var pointNameId = 'destinationNamedeD' + val;
                                    var pointIdId = 'destinationIddeD' + val;
                                    var originPointId = 'originIddeD' + val;
                                    var truckRouteId = 'routeIddeD' + val;
                                    var travelKm = 'travelKmdeD' + val;
                                    var travelHour = 'travelHourdeD' + val;
                                    var travelMinute = 'travelMinutedeD' + val;

                                    //alert(prevPointId);
                                    $('#' + pointNameId).autocomplete({
                                        source: function(request, response) {
                                            $.ajax({
                                                url: "/throttle/getTruckCityList.do",
                                                dataType: "json",
                                                data: {
                                                    cityName: request.term,
                                                    originCityId: $("#" + originPointId).val(),
                                                    textBox: 1
                                                },
                                                success: function(data, textStatus, jqXHR) {
                                                    var items = data;
                                                    response(items);
                                                },
                                                error: function(data, type) {

                                                    //console.log(type);
                                                }
                                            });
                                        },
                                        minLength: 1,
                                        select: function(event, ui) {
                                            var value = ui.item.Name;
                                            var id = ui.item.Id;
                                            //alert(id+" : "+value);
                                            $('#' + pointIdId).val(id);
                                            $('#' + pointNameId).val(value);
                                            $('#' + travelKm).val(ui.item.TravelKm);
                                            $('#' + travelHour).val(ui.item.TravelHour);
                                            $('#' + travelMinute).val(ui.item.TravelMinute);
                                            $('#' + truckRouteId).val(ui.item.RouteId);
                                            //validateRoute(val,value);

                                            return false;
                                        }

                                        // Format the list menu output of the autocomplete
                                    }).data("autocomplete")._renderItem = function(ul, item) {
                                        //alert(item);
                                        var itemVal = item.Name;
                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                        return $("<li></li>")
                                                .data("item.autocomplete", item)
                                                .append("<a>" + itemVal + "</a>")
                                                .appendTo(ul);
                                    };


                                }

                            </script>

                            <div id="routedeD">

                            </div>

                            <a  class="nexttab" href=""><input type="button" class="button" value="Next" name="Next" /></a>

                        </div>
                        <script>

                            $(".nexttab").click(function() {
                                var selected = $("#tabs").tabs("option", "selected");
                                $("#tabs").tabs("option", "selected", selected + 1);
                            });
                            $(".pretab").click(function() {
                                var selected = $("#tabs").tabs("option", "selected");
                                $("#tabs").tabs("option", "selected", selected - 1);
                            });

                        </script>

                    </div>

                    <center>
                        <input type="button" class="button" value="Save" onclick="submitPage();" />

                    </center>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        <%--   <%}catch(Exception e)
     {
            out.println(e.toString());
        }
   %>--%>
    </body>
</div>


    </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
