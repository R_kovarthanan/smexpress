<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

    function createLHCDetails(hireId, vehtype, vehName, rate, vendor) {
        window.open('/throttle/createLHCDetails.do?contractHireId=' + hireId + "&vehicleTypeId=" + vehtype + "&vehicleTypeName=" + vehName + "&rate=" + rate + "&vendor=" + vendor, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    function viewLHCDetails(hireId, type, vendor) {
        window.open('/throttle/viewLHCDetails.do?contractHireId=' + hireId + "&Type=" + type + "&vendor=" + vendor, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }


</script>

<script>

    function contractExists(sno, tableRow) {
//        alert("sdasds");
        var vehicleTypeId = $("#vehicleTypeId" + sno).val();
        var loadTypeId = $("#loadTypeId" + sno).val();
        var originIdFullTruck = $("#originIdFullTruck" + tableRow).val();
        var pointId1 = $("#pointId1" + tableRow).val();
        var pointId2 = $("#pointId2" + tableRow).val();
        var pointId3 = $("#pointId3" + tableRow).val();
        var pointId4 = $("#pointId4" + tableRow).val();
        var destinationIdFullTruck = $("#destinationIdFullTruck" + tableRow).val();

        var vehicleTypeIdE = document.getElementsByName("vehicleTypeIdE");
        var loadTypeE = document.getElementsByName("loadTypeE");
        var originIdFullTruckE = document.getElementsByName("originIdsFullTruckE");
        var destinationIdFullTruckE = document.getElementsByName("destinationIdsFullTruckE");
        var pointId1E = document.getElementsByName("pointId1E");
        var pointId2E = document.getElementsByName("pointId2E");
        var pointId3E = document.getElementsByName("pointId3E");
        var pointId4E = document.getElementsByName("pointId4E");
        var activeInd = document.getElementsByName("activeIndD");


        for (var i = 0; i < vehicleTypeIdE.length; i++) {
//                     alert("sdasdsasdsd");
            if ((vehicleTypeIdE[i].value == vehicleTypeId) && (loadTypeE[i].value == loadTypeId) && (originIdFullTruckE[i].value == originIdFullTruck) && (destinationIdFullTruckE[i].value == destinationIdFullTruck) && (pointId1E[i].value == pointId1) && (pointId2E[i].value == pointId2) && (pointId3E[i].value == pointId3) && (pointId4E[i].value == pointId4)) {
//                    alert("This Contract Details Already Exists in row " + [i + 1] + " is InActivated ");
                activeInd[i].value = 'N';
            }
        }
    }

    function findAdvanceAmount(sno) {

        var additionalCost = $("#additionalCost" + sno).val();
        var advanceMode = $("#advanceMode" + sno).val();
        var modeRate = $("#modeRate" + sno).val();

        var endAdvance = "";
        var initialAdvance = "";

        if (advanceMode != "") {

            if (advanceMode == 1) {
                var percentValue = "";
                if (modeRate == 0) {
                    percentValue = 0;
                } else {
                    percentValue = parseFloat(additionalCost) * (parseFloat(modeRate) / 100)
                }

                initialAdvance = percentValue;
                endAdvance = parseFloat(additionalCost) - parseFloat(percentValue);
            } else if (advanceMode == 2) {
                var flatValue = parseFloat(additionalCost) - parseFloat(modeRate);
                initialAdvance = parseFloat(modeRate);
                endAdvance = parseFloat(flatValue);
            }
            else if (advanceMode == 3) {
                initialAdvance = 0;
                endAdvance = parseFloat(additionalCost);
                document.getElementById("modeRate" + sno).value = 0;
            }
            else {
                //alert("Please Select advance mode");

                document.getElementById("initialAdvance" + sno).value = 0;
                document.getElementById("endAdvance" + sno).value = 0;
                document.getElementById("modeRate" + sno).value = 0;
                return false;
            }

            document.getElementById("initialAdvance" + sno).value = initialAdvance;
            document.getElementById("endAdvance" + sno).value = endAdvance;
        }
    }
    function findAdvanceAmountE(sno) {

        var additionalCost = $("#additionalCostE" + sno).val();
        var advanceMode = $("#advanceModeE" + sno).val();
        var modeRate = $("#modeRateE" + sno).val();

        var endAdvance = "";
        var initialAdvance = "";

        if (advanceMode != "") {

            if (advanceMode == 1) {
                var percentValue = "";
                if (modeRate == 0) {
                    percentValue = 0;
                } else {
                    percentValue = parseFloat(additionalCost) * (parseFloat(modeRate) / 100)
                }
                initialAdvance = percentValue;
                endAdvance = parseFloat(additionalCost) - parseFloat(percentValue);
            } else if (advanceMode == 2) {
                var flatValue = parseFloat(additionalCost) - parseFloat(modeRate);
                initialAdvance = parseFloat(modeRate);
                endAdvance = parseFloat(flatValue);
            } else if (advanceMode == 3) {
                initialAdvance = 0;
                endAdvance = parseFloat(additionalCost);
                document.getElementById("modeRateE" + sno).value = 0;
            }
            else {
                //alert("Please Select advance mode");
                document.getElementById("initialAdvanceE" + sno).value = 0;
                document.getElementById("endAdvanceE" + sno).value = 0;
                document.getElementById("modeRateE" + sno).value = 0;
                return false;
            }

            document.getElementById("initialAdvanceE" + sno).value = initialAdvance;
            document.getElementById("endAdvanceE" + sno).value = endAdvance;
        }
    }


</script>


<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script>
    function submitPage() {


        var spot7 = $("[name=originNameFullTruck]");
        for (var i = 0; i < spot7.length; i++) {
            //alert(spot1[i].value);
            if (spot7[i].value == '0' || spot7[i].value == '0.00') {
                alert(" Origin cannot be empty")
                spot7[i].focus();
                return;
            }
            if (spot7[i].value == '') {
                alert(" Origin cannot be empty")
                spot7[i].focus();
                return;
            }
        }

        var spot8 = $("[name=destinationNameFullTruck]");
        for (var i = 0; i < spot8.length; i++) {
            //alert(spot1[i].value);
            if (spot8[i].value == '0' || spot8[i].value == '0.00') {
                alert(" Destination cannot be empty")
                spot8[i].focus();
                return;
            }
            if (spot8[i].value == '') {
                alert(" Destination cannot be empty")
                spot8[i].focus();
                return;
            }
        }

        var spot4 = $("[name=fromDate1]");
        for (var i = 0; i < spot4.length; i++) {
            //alert(spot1[i].value);
            if (spot4[i].value == '0' || spot4[i].value == '0.00') {
                alert(" From Date cannot be empty")
                spot4[i].focus();
                return;
            }
            if (spot4[i].value == '') {
                alert(" From Date cannot be empty")
                spot4[i].focus();
                return;
            }
        }

        var spot5 = $("[name=toDate1]");
        for (var i = 0; i < spot5.length; i++) {
            //alert(spot1[i].value);
            if (spot5[i].value == '0' || spot5[i].value == '0.00') {
                alert(" To Date cannot be empty")
                spot5[i].focus();
                return;
            }
            if (spot5[i].value == '') {
                alert(" To Date cannot be empty")
                spot5[i].focus();
                return;
            }
        }

        var spot6 = $("[name=vehicleTypeId]");
        for (var i = 0; i < spot6.length; i++) {
            //alert(spot1[i].value);
            if (spot6[i].value == '0' || spot6[i].value == '0.00') {
                alert(" Vehicle Type cannot be empty")
                spot6[i].focus();
                return;
            }
            if (spot6[i].value == '') {
                alert(" Vehicle Type cannot be empty")
                spot6[i].focus();
                return;
            }
        }

        var spot = $("[name=additionalCostE]");
        for (var i = 0; i < spot.length; i++) {
            if (spot[i].value == '0' || spot[i].value == '0.00') {
                alert("contract amount cannot be zero")
                spot[i].focus();
                return;
            }
            if (spot[i].value == '') {
                alert("please enter the contract amount")
                spot[i].focus();
                return;
            }
        }
        var spot1 = $("[name=additionalCost]");
        for (var i = 0; i < spot1.length; i++) {
            //alert(spot1[i].value);
            if (spot1[i].value == '0' || spot1[i].value == '0.00') {
                alert("contract amount cannot be zero")
                spot1[i].focus();
                return;
            }
            if (spot1[i].value == '') {
                alert("please enter the contract amount")
                spot[i].focus();
                return;
            }
        }
        var spot2 = $("[name=advanceModeE]");
        for (var i = 0; i < spot2.length; i++) {
            //alert(spot1[i].value);
            if (spot2[i].value == '0' || spot2[i].value == '0.00') {
                alert(" Advance Type cannot be empty")
                spot2[i].focus();
                return;
            }
            if (spot2[i].value == '') {
                alert("Advance Type cannot be empty")
                spot2[i].focus();
                return;
            }
        }
        var spot3 = $("[name=advanceMode]");
        for (var i = 0; i < spot3.length; i++) {
            //alert(spot1[i].value);
            if (spot3[i].value == '0' || spot3[i].value == '0.00') {
                alert(" Advance Type cannot be empty")
                spot3[i].focus();
                return;
            }
            if (spot3[i].value == '') {
                alert(" Advance Type cannot be empty")
                spot3[i].focus();
                return;
            }
        }


        document.vehicleVendorContract.action = '/throttle/saveEditVehicleVendorContract.do';
        document.vehicleVendorContract.submit();
    }

</script>
<script>
    function setVehicleTypeDetails(sel, sno) {
        var vehicleTypeName = sel.options[sel.selectedIndex].text;
        if (vehicleTypeName == "20'") {
            $("#containerTypeId" + sno + " option[value='2']").remove();
            document.getElementById("containerTypeId" + sno).value = 1;
            $('#containerQty' + sno).val('1');
        } else {
            var exist = 0;
            $('#containerTypeId' + sno + ' option').each(function() {
                if (this.value == '2') {
                    exist = 1;
                }
            });
            if (exist == 1) {
            } else {
                $('#containerTypeId' + sno).append(new Option("40'", '2'));
            }
            document.getElementById("containerTypeId" + sno).value = 0;
            $('#containerQty' + sno).val('');
        }
    }
    function setContainerValue(cnt, containerType) {
        var containers = containerType;
        var vehicleType = $('#vehicleTypeId' + cnt).val();
        if (vehicleType == "1059~40'") {
            if (containers == "1") {
                $('#containerQty' + cnt).val('2');
            } else if (containers == "2") {
                $('#containerQty' + cnt).val('1');
            } else {
                $('#containerQty' + cnt).val('0');
            }
        }
    }

    function pastContractDetails(venId, name) {
        window.open('/throttle/pastContractDetails.do?vendorId=' + venId + "&venName=" + name + "&type=search", 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Vendor Contract</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor</a></li>
            <li class="active">Edit Fleet Vendor Contract</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body >
                <%--  <%try{%>--%>

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>

                <style>
                    .errorClass { 
                        background-color: red;
                    }
                    .noErrorClass { 
                        background-color: greenyellow;
                    }
                </style>

                <form name="vehicleVendorContract" method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <br>

                    <table border="0"  class="table table-info mb30 table-hover" style="width:70%">
                        <thead ><tr  height="30">
                                <th  colspan="4" >Edit Vendor Contract Info</th>
                            </tr></thead>
                        <tr height="30">
                            <td >Vendor Name</td>

                            <td >
                                <input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorId}"/>" class="textbox">
                                <input type="hidden" name="vendorName" id="vendorName" value="<c:out value="${vendorName}"/>" class="textbox">
                                <c:out value="${vendorName}"/></td>
                            <td >Contract Type</td>
                            <td >
                                <select name="contractTypeId" id="contractTypeId"  onchange="checkContractType(this.value);"  style="height:22px;width:150px;">
                                    <!--<option value="0" selected>--Select--</option>-->
                                    <!--<option value="1" >Dedicated</option>-->
                                    <option value="2" >Hired</option>
                                    <!--<option value="3" selected>Both</option>-->
                                </select>
                                <script>
                                    document.getElementById("contractTypeId").value =<c:out value="${contractTypeId}" />;</script>
                            </td>
                        </tr>
                        <tr height="30">
                            <td >Contract From</td>
                            <td >
                                <input type="text" name="startDate" id="startDate" value="<c:out value="${startDate}" />"  class="datepicker form-control" style="height:22px;width:150px;color:black;">
                                <input type="hidden" name="contractId" id="contractId" value="<c:out value="${contractId}" />" >
                            </td>

                            <td >Contract To</td>
                            <td >

                                <input type="text" name="endDate" id="endDate" value="<c:out value="${endDate}" />" class="datepicker form-control"  style="height:22px;width:150px;color:black;">
                            </td>
                        </tr>
                        <tr height="30">
                            <td >Payment Type</td>
                            <td >
                                <select name="paymentType" id="paymentType"   style="height:22px;width:150px;" disabled>
                                    <option value="1" >Monthly</option>
                                    <option value="2" >Trip Wise</option>
                                    <option value="3" >FortNight</option>
                                </select>
                                <script>
                                    document.getElementById("paymentType").value =<c:out value="${paymentType}" />;</script>
                            </td>
                            <td class="text1" colspan="2">&emsp;
                                <!--<a href="#" onclick="pastContractDetails('<c:out value="${vendorId}"/>', '<c:out value="${vendorName}"/>');">Past Contracts</a>-->                                                            
                            </td>

                    </table>
                    <br>

                    <div id="tabs" >
                        <ul class="nav nav-tabs">
                            <li class="fullTruck1" data-toggle="tab"><a href="#fullTruck"><span>MARKET HIRE</span></a></li>
                            <li id="active" data-toggle="tab"><a href="#deD"><span>DEDICATED</span></a></li>
                            <!--                            <li data-toggle="tab" id="pc" style="display: block"><a href="#pcm"><span> Penality charges </span></a></li>
                                                        <li data-toggle="tab" id="dc" style="display: block"><a href="#dcm"><span> Detention charges </span></a></li>-->
                            <!--<li><a href="#weightBreak"><span>Weight Break </span></a></li>-->
                        </ul>
                                                <!--<th align="center">Touch Point 1</th>-->
                                                <!--<th align="center">Touch Point 2</th>-->
                                                <!--<th align="center">Touch Point 3</th>-->
                                                <!--<th align="center">Touch Point 4</th>-->
                                                <!--<th align="center">Destination</th>-->

                        <div id="fullTruck" class="tab-pane active">
                            <div id="routeFullTruck">
                                <c:if test="${hireList != null}">
                                    <table align="center" border="0" id="table" class="table table-info mb30 table-hover" style="width:100%" >
                                        <thead>
                                            <tr style="height: 40px;" id="tableDesingTH">
                                                <th align="center">S.No</th>
                                                <th align="center">From Date</th>
                                                <th align="center">To Date</th>
                                                <th align="center">Vehicle Type</th>
                                                <th align="center">Route</th>
                                                <th align="center">Load Type</th>
                                                <th align="center">Rate </th>                                                
                                                <td align="center">Advance Type</td>
                                                <td align="center">Advance Value</td>
                                                <td align="center">Initial Advance</td>
                                                <td align="center">Remaining Amount</td>
                                                <th align="center">Current Status</th>
                                                <th align="center">Active Status</th>
                                                <th align="center">select</th>
                                                <th align="center">LHC</th>                                                
                                                <th align="center">Used</th>                                                
                                                <th align="center">Not Used</th>                                                
                                            </tr></thead>
                                        <tbody>
                                            <% int index = 1;%>
                                            <c:forEach items="${hireList}" var="route">
                                                <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                                %>
                                                <%--<c:if test="${route.activeInd == 'Y'}" >--%>
                                                <tr height="30">
                                                    <td class="<%=classText%>">
                                                        <input type="hidden" id="originIdsFullTruckE" name="originIdsFullTruckE" value="<c:out value="${route.fromId}"/>" />
                                                        <input type="hidden" id="originNameFullTruckE" name="originNameFullTruckE" value="<c:out value="${route.originNameFullTruck}"/>" />
                                                        <input type="hidden" id="pointId1E" name="pointId1E" value="<c:out value="${route.pointId1}"/>" />
                                                        <input type="hidden" id="pointId2E" name="pointId2E" value="<c:out value="${route.pointId2}"/>" />
                                                        <input type="hidden" id="pointId3E" name="pointId3E" value="<c:out value="${route.pointId3}"/>" /> 
                                                        <input type="hidden" id="pointId4E" name="pointId4E" value="<c:out value="${route.pointId4}"/>" />
                                                        <input type="hidden" id="destinationIdsFullTruckE" name="destinationIdsFullTruckE" value="<c:out value="${route.toId}"/>" />
                                                        <input type="hidden" id="destinationNameFullTruckE" name="destinationNameFullTruckE" value="<c:out value="${route.destinationNameFullTruck}"/>" />
                                                        <input type="hidden" name="approvalStatusE" id="approvalStatusE<%=index%>"  value="<c:out value="${route.approvalStatus}"/>" />
                                                        <input type="hidden" name="vehicleTypeIdE" id="vehicleTypeIdE" value="<c:out value="${route.vehicleTypeId}"/>" /> 
                                                        <input type="hidden" name="loadTypeE" id="loadTypeE" value="<c:out value="${route.loadTypeId}"/>" /> 


                                                        <%=index++%></td>
                                                    <td class="<%=classText%>">
                                                        <input type="hidden" name="fromDateE" id="fromDateE" style="height:22px;width:130px;"  class="datepicker form-control" value="<c:out value="${route.startDate}"/>" />                                                         
                                                        <c:out value="${route.startDate}"/>
                                                    </td>
                                                    <td class="<%=classText%>">                                                            
                                                        <input type="text" name="toDateE" id="toDateE" style="height:22px;width:130px;"  class="datepicker form-control" value="<c:out value="${route.endDate}"/>" /> 
                                                    </td>

                                                    <td class="<%=classText%>"><c:out value="${route.vehicleTypeName}"/></td>
<td class="<%=classText%>"><c:out value="${route.originNameFullTruck}"/>-<c:out value="${route.point1Name}"/>-<c:out value="${route.point2Name}"/>-<c:out value="${route.point3Name}"/>-<c:out value="${route.point4Name}"/>-<c:out value="${route.destinationNameFullTruck}"/></td>                                                    <td class="<%=classText%>"> 
                                                        <c:if test="${route.loadTypeId == '1'}">
                                                            Empty Trip
                                                        </c:if>
                                                        <c:if test="${route.loadTypeId == '2'}"> 
                                                            Load Trip
                                                        </c:if>
                                                    </td>

                                            <input type="hidden" name="contractHireId" id="contractHireId<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${route.contractHireId}"/>" />
                                            <input type="hidden" name="spotCostE" id="spotCostE<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${route.spotCost}"/>" style="height:22px;width:130px;" />
                                            <td class="<%=classText%>"   ><input type="text" name="additionalCostE" id="additionalCostE<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${route.additionalCost}"/>" onChange="findAdvanceAmountE(<%=index%>)" style="height:22px;width:130px;" /></td>


                                            <td class="<%=classText%>"><select name="advanceModeE" id="advanceModeE<%=index%>"  onChange="findAdvanceAmountE(<%=index%>)" style="height:22px;width:130px;" >
                                                    
                                                    <c:if test="${route.advanceTripMode=='PERCENTAGE'  || route.advanceTripMode=='Percentage'}" >
                                                    <option value="1" selected>PERCENTAGE</option>
                                                    <option value="2">FLAT</option>
                                                    <option value="3">CREDIT</option>
                                                    </c:if>
                                                    <c:if test="${route.advanceTripMode=='FLAT'  || route.advanceTripMode=='Flat'}" >
                                                    <option value="1" >PERCENTAGE</option>
                                                    <option value="2" selected>FLAT</option>
                                                    <option value="3">CREDIT</option>
                                                    </c:if>
                                                    <c:if test="${route.advanceTripMode=='CREDIT' || route.advanceTripMode=='Credit'}" >
                                                    <option value="1" >PERCENTAGE</option>
                                                    <option value="2">FLAT</option>
                                                    <option value="3" selected>CREDIT</option>
                                                    </c:if>
                                                </select>
                                            </td>

                                          

                                            <td class="<%=classText%>"><input type="text" name="modeRateE" id="modeRateE<%=index%>" value="<c:out value="${route.modeTripRate}"/>"  onKeyPress="return onKeyPressBlockCharacters(event);" onChange="findAdvanceAmountE(<%=index%>)" style="height:22px;width:160px;"/></td>
                                            <td class="<%=classText%>"><input type="text" name="initialAdvanceE" id="initialAdvanceE<%=index%>" value="<c:out value="${route.initialTripAdvance}"/>" readonly  style="height:22px;width:160px;"/></td>
                                            <td class="<%=classText%>"><input type="text" name="endAdvanceE" id="endAdvanceE<%=index%>" value="<c:out value="${route.endTripAdvance}"/>"  readonly style="height:22px;width:160px;"/></td>


                                            <c:if test="${route.approvalStatus == 1}">
                                                <td class="<%=classText%>"   ><font color="green" size="2">Approved</font></td>
                                                </c:if>
                                                <c:if test="${route.approvalStatus == 2}">
                                                <td class="<%=classText%>"   ><font color="Violet" size="2">WFA</font></td>
                                                </c:if>
                                                <c:if test="${route.approvalStatus == 3}">
                                                <td class="<%=classText%>"><font color="red" size="2">Reject</font></td>
                                                </c:if>
                                            <td class="<%=classText%>">
                                                <select name="activeIndD" id="activeIndD<%=index%>" style="height:22px;width:130px;">
                                                    <c:if test="${route.activeInd == 'Y'}" >
                                                        <option value="Y" selected>Yes</option>
                                                        <option value="N">No</option>
                                                    </c:if>
                                                    <c:if test="${route.activeInd == 'N'}" >
                                                        <option value="N" selected>No</option>
                                                        <option value="Y" >Yes</option>
                                                    </c:if>
                                                </select>
                                            </td>
                                            <td class="<%=classText%>"><input type="checkbox" name="select" checked/></td>
                                            <td class="<%=classText%>">
                                                <!--<a href="#" >-->                                                                
                                                    <!--<input type="button" class="btn1c"  name="create" value="Create" onclick="createLHCDetails('<c:out value="${route.contractHireId}"/>','<c:out value="${route.vehicleTypeId}"/>','<c:out value="${route.vehicleTypeName}"/>','<c:out value="${route.additionalCost}"/>','<c:out value="${vendorName}"/>');"/>-->
                                                <!--</a>-->
                                                <a href="#" >
                                                    <input type="button" class="btn1v"   name="view" value="View" onclick="viewLHCDetails('<c:out value="${route.contractHireId}"/>', 'all', '<c:out value="${vendorName}"/>');"/>                                                            
                                                </a>
                                            </td>                                                        
                                            <td class="<%=classText%>">
                                                <a href="#" >
                                                    <input type="button" class="btn1u" name="used" value="<c:out value="${route.lhcUsed}"/>" onclick="viewLHCDetails('<c:out value="${route.contractHireId}"/>', 'used', '<c:out value="${vendorName}"/>');"/>
                                                </a>
                                            </td>
                                            <td class="<%=classText%>">
                                                <a href="#" ><input type="button" class="btn1n"  name="notused" value="<c:out value="${route.lhcNotUsed}"/>" onclick="viewLHCDetails('<c:out value="${route.contractHireId}"/>', 'notused', '<c:out value="${vendorName}"/>');"/></a>
                                            </td>
                                            </tr>
                                            <%--</c:if>--%>
                                        </c:forEach>
                                        <style>
                                            .btn1v {
                                                background-color: #83C9FD;
                                                border: none;
                                                color: white;    
                                                text-align: center;
                                                text-decoration: none;    
                                                font-size: 9px;
                                            }
                                            .btn1c {
                                                background-color: #45F13D;
                                                border: none;
                                                color: white;    
                                                text-align: center;
                                                text-decoration: none;    
                                                font-size: 9px;
                                            }
                                            .btn1n {
                                                background-color: #901A07;
                                                border: none;
                                                color: white;    
                                                text-align: center;
                                                text-decoration: none;    
                                                font-size: 9px;
                                            }
                                            .btn1u {
                                                background-color: #FA9A54;
                                                border: none;
                                                color: white;    
                                                text-align: center;
                                                text-decoration: none;    
                                                font-size: 9px;
                                            }
                                        </style>

                                        </tbody>
                                    </table>

                                    <script>
                                        function checkKey(obj, e, id, cnt) {
                                            var orgName = 'originNameFullTruck' + cnt;
                                            var point1Name = 'pointName1' + cnt;
                                            var point2Name = 'pointName2' + cnt;
                                            var point3Name = 'pointName3' + cnt;
                                            var point4Name = 'pointName4' + cnt;
                                            var destinationName = 'destinationNameFullTruck' + cnt;
                                            var pointId = "";
                                            if (id == orgName) {
                                                pointId = 'originIdFullTruck' + cnt;
                                            }
                                            if (id == point1Name) {
                                                pointId = 'pointId1' + cnt;
                                            }
                                            if (id == point2Name) {
                                                pointId = 'pointId2' + cnt;
                                            }
                                            if (id == point3Name) {
                                                pointId = 'pointId3' + cnt;
                                            }
                                            if (id == point4Name) {
                                                pointId = 'pointId4' + cnt;
                                            }
                                            if (id == destinationName) {
                                                pointId = 'originIdFullTruck' + cnt;
                                            }

                                            var idVal = $('#' + pointId).val();
                                            if (e.keyCode == 46 || e.keyCode == 8) {
                                                $('#' + id).val('');
                                                $('#' + pointId).val('');
                                            } else if (idVal != '' && e.keyCode != 13) {
//                                                $('#' + id).val('');
//                                                $('#' + pointId).val('');
                                            }
                                        }

                                        function checkPointNames(id, cnt) {
                                            var orgName = 'originNameFullTruck' + cnt;
                                            var point1Name = 'pointName1' + cnt;
                                            var point2Name = 'pointName2' + cnt;
                                            var point3Name = 'pointName3' + cnt;
                                            var point4Name = 'pointName4' + cnt;
                                            var destinationName = 'destinationNameFullTruck' + cnt;
                                            var pointId = "";
                                            if (id == orgName) {
                                                pointId = 'originIdFullTruck' + cnt;
                                            }
                                            if (id == point1Name) {
                                                pointId = 'pointId1' + cnt;
                                            }
                                            if (id == point2Name) {
                                                pointId = 'pointId2' + cnt;
                                            }
                                            if (id == point3Name) {
                                                pointId = 'pointId3' + cnt;
                                            }
                                            if (id == point4Name) {
                                                pointId = 'pointId4' + cnt;
                                            }
                                            if (id == destinationName) {
                                                pointId = 'destinationIdFullTruck' + cnt;
                                            }
                                            var idValue = $('#' + id).val();
                                            var idValue1 = $('#' + pointId).val();
                                            if (idValue != '' && idValue1 == '') {
                                                alert('Invalid Point Name');
                                                $('#' + id).focus();
//                                                $('#'+id).val('');
//                                                $('#'+pointId).val('');
                                            }
                                            getMarketHireRate(cnt, cnt);
                                        }

                                        var container = "";
                                        function addRouteFullTruck() {

                                            var iCnt = <%=index%>;
                                            var iCnt1 = 1;
                                            var rowCnt = <%=index%>;
                                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                            container = $($("#routeFullTruck")).css({
                                                padding: '5px', margin: '20px', width: '100%', border: '0px dashed',
                                                borderTopColor: '#999', borderBottomColor: '#999',
                                                borderLeftColor: '#999', borderRightColor: '#999'
                                            });
                                            var routeInfoSize = $('#routeDetails' + iCnt + ' tr').size();
                                            //alert(routeInfoSize);
                                            rowCnt = parseFloat(iCnt) + parseFloat(routeInfoSize / 2);

                                            iCnt = parseFloat(iCnt) + parseFloat(routeInfoSize / 2);
                                            //alert("routeInfoSize : "+routeInfoSize);
                                            //alert("iCnt : "+iCnt);
                                            //alert("rowCnt :"+rowCnt);
                                            $(container).last().after('<span id="statusSpan" style="color: red"></span>\n\
                                <table id="mainTableFullTruck" width="100%"><tr><td><table  id="routeDetails' + iCnt + '"  border="1" >\n\
                                <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                <td>Sno</td>\n\
                                <td>Origin</td>\n\
                                <td>Touch Point 1</td>\n\
                                <td>Touch Point 2</td>\n\
                                <td>Touch Point 3</td>\n\
                                <td>Touch Point 4</td>\n\
                                <td>Destination</td>\n\
                                <tr style="height:22px;"><td>Route&nbsp; ' + rowCnt + '</td>\n\
                                <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" />\n\
                                <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/><input type="hidden" name="iCnt" id="iCnt' + iCnt + '" value="' + iCnt + '"/></td>\n\
                                <td><input type="hidden" name="pointId1" id="pointId1' + iCnt + '" value="0" />\n\
                                <input type="text" name="pointName1" id="pointName1' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);"  style="height:22px;width:160px;"/></td>\n\
                                <input type="hidden" name="pointId2" id="pointId2' + iCnt + '" value="0" />\n\
                                <td><input type="text" name="pointName2" id="pointName2' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/></td>\n\
                                <input type="hidden" name="pointId3" id="pointId3' + iCnt + '" value="0" />\n\
                                <td><input type="text" name="pointName3" id="pointName3' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/>\n\
                                <input type="hidden" name="pointId4" id="pointId4' + iCnt + '" value="0" />\n\
                                <td><input type="text" name="pointName4" id="pointName4' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/></td>\n\
                                <td>\n\
                                <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/>\n\
                                <input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" />\n\
                                <input type="hidden" name="travelKmFullTruck" id="travelKmFullTruck' + rowCnt + '" value="" style="height:22px;width:160px;" onKeyPress="return onKeyPressBlockCharacters(event);"/>\n\
                                <input type="hidden" name="travelHourFullTruck" id="travelHourFullTruck' + rowCnt + '" value="" style="height:22px;width:160px;"  onKeyPress="return onKeyPressBlockCharacters(event);"/>\n\
                                <input type="hidden" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + rowCnt + '" value="" style="height:22px;width:160px;" onKeyPress="return onKeyPressBlockCharacters(event);"/>\n\
                                <input type="hidden" name="routeIdFullTruck" id="routeIdFullTruck' + rowCnt + '" value="" /></td>\n\
                                </tr>\n\
                                </table>\n\
                                <table  id="routeInfoDetailsFullTruck' + iCnt1 + rowCnt + '"  border="1">\n\
                                <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                <td>Status</td><td>SNo</td><td>FromDate</td><td>ToDate</td><td>Vehicle Type</td><td>Load Type</td><td>Rate </td>\n\
                                <td>Advance Type</td><td>Advance Value</td><td>Initial Advance</td><td>Remaining Amount</td>\n\
                                </tr>\n\
                                <tr style="height:22px;">\n\
                                <td><span id="approvalStatusSpan' + iCnt + '">Not Filled</span></td><td>' + iCnt1 + '\n\
                                <input type="hidden" name="iCnt1" id="iCnt1' + iCnt1 + '" value="' + rowCnt + '"/></td>\n\
                                <td><input type="text" name="fromDate1" style="width:160px;"  id="fromDate1' + iCnt + '' + rowCnt + '" class="datepicker form-control"  value=""/></td>\n\
                                <td><input type="text" name="toDate1"  style="width:160px;" id="toDate1' + iCnt + '' + rowCnt + '" class="datepicker form-control"  value=""/></td>\n\
                                <td><input type="hidden" name="vehicleUnits" id="vehicleUnits' + iCnt1 + '" value="1" /><select type="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '" style="height:22px;width:160px;" onchange="setVehicleTypeDetails(this,' + iCnt + ');setContainerTypeList(this.value,' + iCnt1 + ');getMarketHireRate(' + iCnt + ',' + iCnt + ')"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                <td><select name="loadTypeId" id="loadTypeId' + iCnt + '"   style="height:22px;width:160px;" onchange="getMarketHireRate(' + iCnt + ',' + iCnt + ');" ><option value="1">Empty Trip</option><option value="2" selected>Load Trip</option></select></td>\n\
                                <input type="hidden" name="spotCost" id="spotCost' + iCnt + '" value="0" onkeyup="checkMarketRate(this.value,' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/>\n\
                                <td><input type="text" name="additionalCost" id="additionalCost' + iCnt + '" value="0" onChange="findAdvanceAmount(' + iCnt + ')" onkeyup="checkMarketRate(this.value,' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;" /></td>\n\
                                <input type="hidden" name="marketRate" id="marketRate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/>\n\
                                <td><select name="advanceMode" id="advanceMode' + iCnt + '"  onChange="findAdvanceAmount(' + iCnt + ')" style="height:22px;width:160px;" >\n\
                                <option value=0>-Select-</option>\n\
                                <option value=1>PERCENTAGE</option>\n\
                                <option value=2>FLAT</option>\n\
                                <option value=3>CREDIT</option>\n\
                                </select>\n\
                                </td>\n\
                                <td><input type="text" name="modeRate" id="modeRate' + iCnt + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" onChange="findAdvanceAmount(' + iCnt + ')" style="height:22px;width:160px;"/></td>\n\
                                <td><input type="text" name="initialAdvance" id="initialAdvance' + iCnt + '" value="" readonly  style="height:22px;width:160px;"/></td>\n\
                                <td><input type="text" name="endAdvance" id="endAdvance' + iCnt + '" value=""  readonly style="height:22px;width:160px;"/></td>\n\
                                <input type="hidden" name="lHCNO" id="lHCNO' + iCnt + '" value="0"  style="height:22px;width:160px;" onKeyPress="return onKeyPressBlockCharacters(event);"/>\n\
                                </tr></table>\n\
                                <br><table border="0"  id="routebutton' + iCnt1 + rowCnt + '" ><tr>\n\
                                <td><input class="btn btn-info" type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt1 + rowCnt + '" value="Add" onclick="addRow(' + iCnt1 + rowCnt + ',' + rowCnt + ')" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>\n\
                                <input class="btn btn-info" type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt1 + rowCnt + '" value="Remove"  onclick="deleteRow(' + iCnt1 + rowCnt + ')" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></td>\n\
                                </tr></table></td></tr></table></td></tr></table>');
                                            callOriginAjaxFullTruck(iCnt);
                                            callPointId1AjaxFullTruck(iCnt);
                                            callPointId2AjaxFullTruck(iCnt);
                                            callPointId3AjaxFullTruck(iCnt);
                                            callPointId4AjaxFullTruck(iCnt);
                                            callDestinationAjaxFullTruck(iCnt);
                                            $('.datepicker').datepicker();
                                        }

                                        function deleteRouteFullTruck() {
                                            var rows = $('#mainTableFullTruck tr').length;

                                            if (rows > 2) {
                                                $('#mainTableFullTruck').last(container).remove();
                                                iCnt = iCnt - 1;
                                            }
                                        }

                                        // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                        var divValue, values = '';
                                        function GetTextValue() {
                                            $(divValue).empty();
                                            $(divValue).remove();
                                            values = '';
                                            $('.input').each(function() {
                                                divValue = $(document.createElement('div')).css({
                                                    padding: '5px', width: '200px'
                                                });
                                                values += this.value + '<br />'
                                            });
                                            $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                            $('body').append(divValue);
                                        }

                                        var x = 0;
                                        var loadCnt = 0;
                                        var loadCnt1 = 0;
                                        var loadCnt2 = 0;

                                        function addRow(val, v1) {

                                            loadCnt1 = v1;
                                            loadCnt2 = val;
                                            if (x == 0) {
                                                loadCnt = val;
                                            }
                                            //alert(x);
                                            //alert(loadCnt);
                                            //alert(loadCnt2);

                                            var routeInfoSize = $('#routeInfoDetailsFullTruck' + loadCnt2 + ' tr').size();

                                            $('#routeInfoDetailsFullTruck' + loadCnt2 + ' tr').last().after(
                                                    '<tr>\n\
                                    <td><span id="approvalStatusSpan' + loadCnt + '">Not Filled</span><td>' + routeInfoSize + '<input type="hidden" name="iCnt1" id="iCnt1' + loadCnt + '" value="' + loadCnt1 + '"/></td>\n\
                                    <td><input type="text" name="fromDate1" style="width:160px;"  id="fromDate1' + loadCnt + '' + routeInfoSize + '" class="datepicker form-control"  value=""/></td>\n\
                                    <td><input type="text" name="toDate1"  style="width:160px;" id="toDate1' + loadCnt + '' + routeInfoSize + '" class="datepicker form-control"  value=""/></td>\n\
                                    <td><input type="hidden" name="vehicleUnits" id="vehicleUnits' + loadCnt + '" value="1" style="height:22px;width:160px;"/><select type="text" name="vehicleTypeId" id="vehicleTypeId' + loadCnt + '" style="height:22px;width:160px;" onchange="setVehicleTypeDetails(this,' + loadCnt + ');setContainerTypeList(this.value,' + loadCnt + ');getMarketHireRate(' + loadCnt + ',' + loadCnt1 + ')"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                    <td><select name="loadTypeId" id="loadTypeId' + loadCnt + '"   style="height:22px;width:160px;" ><option value="1">Empty Trip</option><option value="2" selected>Load Trip</option></select></td>\n\
                                    <input type="hidden" name="spotCost" id="spotCost' + loadCnt + '" value="0" onkeyup="checkMarketRate(this.value,' + loadCnt + ')" style="height:22px;width:160px;"/>\n\
                                    <td><input type="text" name="additionalCost" id="additionalCost' + loadCnt + '" value="0"  onkeyup="checkMarketRate(this.value,' + loadCnt + ')" onChange="findAdvanceAmount(' + loadCnt + ')" style="height:22px;width:160px;"/></td>\n\
                                    <input type="hidden" name="marketRate" id="marketRate' + loadCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/><span id="marketRateSpan' + loadCnt + '"></span>\n\
                                    <td><select name="advanceMode" id="advanceMode' + loadCnt + '"  onChange="findAdvanceAmount(' + loadCnt + ')" style="height:22px;width:160px;" >\n\
                                    <option value=0>-Select-</option>\n\
                                    <option value=1>PERCENTAGE</option>\n\
                                    <option value=2>FLAT</option>\n\
                                    <option value=3>CREDIT</option>\n\
                                    </select>\n\
                                    </td>\n\
                                    <td><input type="text" name="modeRate" id="modeRate' + loadCnt + '" value="0" onChange="findAdvanceAmount(' + loadCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" onChange="findAdvanceAmount(' + loadCnt + ')" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="initialAdvance" id="initialAdvance' + loadCnt + '" value="" readonly  style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="endAdvance" id="endAdvance' + loadCnt + '" value=""  readonly style="height:22px;width:160px;"/></td>\n\
                                    <input type="hidden" name="lHCNO" id="lHCNO' + loadCnt + '" value="0"  style="height:22px;width:160px;" onKeyPress="return onKeyPressBlockCharacters(event);"/>\n\
                                    </tr>');
                                            loadCnt++;
                                            x++;
                                            $('.datepicker').datepicker();
                                        }

                                        function deleteRow(val) {
                                            var loadCnt = val;
                                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                            if ($('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size() > 2) {
                                                $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().remove();
                                                loadCnt = loadCnt - 1;
                                            } else {
                                                alert('One row should be present in table');
                                            }
                                        }
                                            </script>

                                            <script>
                                                function callOriginAjaxFullTruck(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
                                                    //alert(val);
                                                    //       var pointNameId = 'originNameFullTruck' + val;
                                                    var pointNameId = 'originNameFullTruck' + val;
                                                    var pointId = 'originIdFullTruck' + val;
                                                    var desPointName = 'destinationNameFullTruck' + val;
                                                    //alert(prevPointId);
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getCityFromList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    //                                                    cityName: request.term,
                                                                    cityName: $("#" + pointNameId).val(),
                                                                    //                                                    cityName: $("#" + pointNameId).val(),
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                    //alert("asdfasdf")
                                                                },
                                                                error: function(data, type) {
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.Value;
                                                            //alert("value ="+value);
                                                            var temp = value.split("-");
                                                            var textId = temp[0];
                                                            var textName = temp[1];
                                                            var id = ui.item.Id;
                                                            //alert("id ="+id);
                                                            //alert(id+" : "+value);
                                                            $('#' + pointId).val(textId);
                                                            $('#' + pointNameId).val(textName);
                                                            $('#' + desPointName).focus();
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        //var temp [] = "";
                                                        var itemVal = item.Value;
                                                        var temp = itemVal.split("-");
                                                        var textId = temp[0];
                                                        var t2 = temp[1];
                                                        //                                        alert("t1 = "+t1)
                                                        //                                        alert("t2 = "+t2)
                                                        //alert("itemVal = "+itemVal)
                                                        t2 = '<font color="green">' + t2 + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + t2 + "</a>")
                                                                .appendTo(ul);
                                                    };
                                                }

                                                function callPointId1AjaxFullTruck(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
//                                            alert(val);
                                                    //       var pointNameId = 'originNameFullTruck' + val;
                                                    var pointNameId = 'pointName1' + val;
                                                    var pointId = 'pointId1' + val;
                                                    var point2Name = 'pointName2' + val;
                                                    //alert(prevPointId);
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getCityFromList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    //                                                    cityName: request.term,
                                                                    cityName: $("#" + pointNameId).val(),
                                                                    //                                                    cityName: $("#" + pointNameId).val(),
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                    //alert("asdfasdf")
                                                                },
                                                                error: function(data, type) {
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.Value;
                                                            //alert("value ="+value);
                                                            var temp = value.split("-");
                                                            var textId = temp[0];
                                                            var textName = temp[1];
                                                            var id = ui.item.Id;
                                                            //alert("id ="+id);
                                                            //alert(id+" : "+value);
                                                            $('#' + pointId).val(textId);
                                                            $('#' + pointNameId).val(textName);
                                                            $('#' + point2Name).focus();
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        //var temp [] = "";
                                                        var itemVal = item.Value;
                                                        var temp = itemVal.split("-");
                                                        var textId = temp[0];
                                                        var t2 = temp[1];
                                                        //                                        alert("t1 = "+t1)
                                                        //                                        alert("t2 = "+t2)
                                                        //alert("itemVal = "+itemVal)
                                                        t2 = '<font color="green">' + t2 + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + t2 + "</a>")
                                                                .appendTo(ul);
                                                    };
                                                }
                                                function callPointId2AjaxFullTruck(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
//                                            alert(val);
                                                    //       var pointNameId = 'originNameFullTruck' + val;
                                                    var pointNameId = 'pointName2' + val;
                                                    var pointId = 'pointId2' + val;
                                                    var point3Name = 'pointName3' + val;
                                                    //alert(prevPointId);
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getCityFromList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    //                                                    cityName: request.term,
                                                                    cityName: $("#" + pointNameId).val(),
                                                                    //                                                    cityName: $("#" + pointNameId).val(),
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                    //alert("asdfasdf")
                                                                },
                                                                error: function(data, type) {
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.Value;
                                                            //alert("value ="+value);
                                                            var temp = value.split("-");
                                                            var textId = temp[0];
                                                            var textName = temp[1];
                                                            var id = ui.item.Id;
                                                            //alert("id ="+id);
                                                            //alert(id+" : "+value);
                                                            $('#' + pointId).val(textId);
                                                            $('#' + pointNameId).val(textName);
                                                            $('#' + point3Name).focus();
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        //var temp [] = "";
                                                        var itemVal = item.Value;
                                                        var temp = itemVal.split("-");
                                                        var textId = temp[0];
                                                        var t2 = temp[1];
                                                        //                                        alert("t1 = "+t1)
                                                        //                                        alert("t2 = "+t2)
                                                        //alert("itemVal = "+itemVal)
                                                        t2 = '<font color="green">' + t2 + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + t2 + "</a>")
                                                                .appendTo(ul);
                                                    };
                                                }
                                                function callPointId3AjaxFullTruck(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
//                                            alert(val);
                                                    //       var pointNameId = 'originNameFullTruck' + val;
                                                    var pointNameId = 'pointName3' + val;
                                                    var pointId = 'pointId3' + val;
                                                    var point4Name = 'pointName4' + val;
                                                    //alert(prevPointId);
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getCityFromList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    //                                                    cityName: request.term,
                                                                    cityName: $("#" + pointNameId).val(),
                                                                    //                                                    cityName: $("#" + pointNameId).val(),
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                    //alert("asdfasdf")
                                                                },
                                                                error: function(data, type) {
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.Value;
                                                            //alert("value ="+value);
                                                            var temp = value.split("-");
                                                            var textId = temp[0];
                                                            var textName = temp[1];
                                                            var id = ui.item.Id;
                                                            //alert("id ="+id);
                                                            //alert(id+" : "+value);
                                                            $('#' + pointId).val(textId);
                                                            $('#' + pointNameId).val(textName);
                                                            $('#' + point4Name).focus();
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        //var temp [] = "";
                                                        var itemVal = item.Value;
                                                        var temp = itemVal.split("-");
                                                        var textId = temp[0];
                                                        var t2 = temp[1];
                                                        //                                        alert("t1 = "+t1)
                                                        //                                        alert("t2 = "+t2)
                                                        //alert("itemVal = "+itemVal)
                                                        t2 = '<font color="green">' + t2 + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + t2 + "</a>")
                                                                .appendTo(ul);
                                                    };
                                                }
                                                function callPointId4AjaxFullTruck(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
                                                    //       var pointNameId = 'originNameFullTruck' + val;
                                                    var pointNameId = 'pointName4' + val;
                                                    var pointId = 'pointId4' + val;
                                                    var desPointName = 'destinationNameFullTruck' + val;
                                                    //alert(prevPointId);
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getCityFromList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    //                                                    cityName: request.term,
                                                                    cityName: $("#" + pointNameId).val(),
                                                                    //                                                    cityName: $("#" + pointNameId).val(),
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                    //alert("asdfasdf")
                                                                },
                                                                error: function(data, type) {
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.Value;
                                                            //alert("value ="+value);
                                                            var temp = value.split("-");
                                                            var textId = temp[0];
                                                            var textName = temp[1];
                                                            var id = ui.item.Id;
                                                            //alert("id ="+id);
                                                            //alert(id+" : "+value);
                                                            $('#' + pointId).val(textId);
                                                            $('#' + pointNameId).val(textName);
                                                            $('#' + desPointName).focus();
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        //var temp [] = "";
                                                        var itemVal = item.Value;
                                                        var temp = itemVal.split("-");
                                                        var textId = temp[0];
                                                        var t2 = temp[1];
                                                        //                                        alert("t1 = "+t1)
                                                        //                                        alert("t2 = "+t2)
                                                        //alert("itemVal = "+itemVal)
                                                        t2 = '<font color="green">' + t2 + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + t2 + "</a>")
                                                                .appendTo(ul);
                                                    };
                                                }
                                                //
                                                //
                                                //                                }
                                                function callDestinationAjaxFullTruck(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
                                                    //alert(val);
                                                    var pointNameId = 'destinationNameFullTruck' + val;
                                                    var pointIdId = 'destinationIdFullTruck' + val;
                                                    var originPointId = 'originIdFullTruck' + val;
                                                    var pointId1 = 'pointId1' + val;
                                                    var pointId2 = 'pointId2' + val;
                                                    var pointId3 = 'pointId3' + val;
                                                    var pointId4 = 'pointId4' + val;
                                                    var truckRouteId = 'routeIdFullTruck' + val;
                                                    var travelKm = 'travelKmFullTruck' + val;
                                                    var travelHour = 'travelHourFullTruck' + val;
                                                    var travelMinute = 'travelMinuteFullTruck' + val;
                                                    //                                    if($("#" + originPointId).val() == ""){
                                                    //                                        alert("id inside condition ==")
                                                    //                                    }else{
                                                    //                                        alert("else inside condition ==")
                                                    //                                    }
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getCityToList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    cityName: $("#" + pointNameId).val(),
                                                                    originCityId: $("#" + originPointId).val(),
                                                                    pointId1: $("#" + pointId1).val(),
                                                                    pointId2: $("#" + pointId2).val(),
                                                                    pointId3: $("#" + pointId3).val(),
                                                                    pointId4: $("#" + pointId4).val(),
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                },
                                                                error: function(data, type) {

                                                                    //console.log(type);
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.cityName;
                                                            var id = ui.item.cityId;
                                                            //                                            alert(id+" : "+value);
                                                            $('#' + pointIdId).val(id);
                                                            $('#' + pointNameId).val(value);
                                                            $('#' + travelKm).val(ui.item.Distance);
                                                            $('#' + travelHour).val(ui.item.TravelHour);
                                                            $('#' + travelMinute).val(ui.item.TravelMinute);
                                                            $('#' + truckRouteId).val(ui.item.RouteId);
                                                            $('#vehicleTypeId' + val).val(0);
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        var itemVal = item.cityName;
                                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + itemVal + "</a>")
                                                                .appendTo(ul);
                                                    };
                                                }
                                            </script>

                                        </div>

                                        <br>
                                        <br>
                                        <center>
                                            <a><input type="button" class="btn btn-info" value="Add Route" id="AddRouteFullTruck" name="AddRouteFullTruck" onclick="addRouteFullTruck()" style="width:80px;height:30px;font-weight: bold;padding:1px;"/></a>
                                            <a><input type="button" class="btn btn-info " value="Remove Route" name="Remove" onclick="deleteRouteFullTruck()" style="width:80px;height:30px;font-weight: bold;padding:1px;"/></a>
                                        </center>

                            </c:if>
                        </div>

                        <div id="pcm" class="tab-pane" style="display:none">
                            <div class="inpad">
                                <table class="table table-info mb30 table-hover" width="90%" id="penalitytable">
                                    <thead>
                                        <tr id="rowId0" >
                                            <td    id="tableDesingTD">S No</td>
                                            <td    id="tableDesingTD">Charges names</td>
                                            <td    id="tableDesingTD">Unit</td>
                                            <td    id="tableDesingTD">Amount</td>
                                            <td    id="tableDesingTD">Remarks</td>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <% int sno = 0;%>
                                        <c:forEach items="${viewpenalitycharge}" var="vpc">
                                            <%
                                                        sno++;
                                            %>
                                            <tr>
                                                <td   align="left" > <%= sno%>  </td>
                                                <td   align="left"> <c:out value="${vpc.penality}" /></td>
                                                <td   align="left"> <c:out value="${vpc.pcmunit}" /></td>
                                                <td align="left"   >
                                                    <input type="text" name="penalitycharge" class="form-control" style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${vpc.chargeamount}" />"/>
                                                </td>
                                                <td   align="left"> <c:out value="${vpc.pcmremarks}" /></td>
                                                <td align="left"   ><input type="hidden"  name="penid" id="penid" value="<c:out value="${vpc.id}"/>" /></td>
                                            </tr>
                                        </c:forEach>

                                        <tr>
                                    <input type="hidden" name="selectedRowCount" id="sno" value="1"/>
                                    <input type="hidden" name="tripSheetId" id="tripSheetId" value="<%=request.getParameter("tripSheetId")%>" />
                                    </tbody>
                                </table>
                                <br>
                                <center>
                                    <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRowPenality()" />
                                    <!--<INPUT type="button" class="button" value="Delete Row" onclick="deleteRowPenality()" />-->
                                    <!--&nbsp;&nbsp;-->
                                    <!--<input type="button" class="btn btn-success" value="Save" onclick="Savepencharge(this.value)"/>-->
                                </center>
                                <SCRIPT language="javascript">
                                    var poItems = 1;
                                    var rowCount = 2;
                                    var sno = 1;
                                    var snumber = 1;

                                    function addRowPenality()
                                    {
                                        //                                           alert(rowCount);
                                        if (sno < 20) {
                                            sno++;
                                            var tab = document.getElementById("penalitytable");
                                            rowCount = tab.rows.length;

                                            snumber = (rowCount) - 1;
                                            if (snumber == 1) {
                                                snumber = parseInt(rowCount);
                                            } else {
                                                snumber++;
                                            }
                                            snumber = snumber - 1;
                                            var newrow = tab.insertRow((rowCount));
                                            newrow.height = "30px";
                                            // var temp = sno1-1;
                                            var cell = newrow.insertCell(0);
                                            var cell0 = "<td class='text1' align='center'> " + snumber + "</td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(1);
                                            cell0 = "<td class='text1'><input type='text' name='penality' id='penality" + snumber + "' class='form-control' /></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(2);
                                            cell0 = "<td class='text1'><input type='text' name='pcmunit' id='pcmunit" + snumber + "' class='form-control' /></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(3);
                                            cell0 = "<td class='text1'><input type='text' name='chargeamount' id='chargeamount" + snumber + "' class='form-control' onKeyPress='return onKeyPressBlockCharacters(event);' /></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(4);
                                            cell0 = "<td class='text1'><input type='text' name='pcmremarks' id='pcmremarks" + snumber + "' class='form-control' /></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            rowCount++;
                                        }
                                    }

                                </SCRIPT>
                            </div>
                            &nbsp;&nbsp;
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>
                            <br>
                        </div>

                        <div id="dcm" class="tab-pane" style="display:none">
                            <div class="inpad">
                                <table class="table table-info mb30 table-hover" width="90%" id="detentiontable">
                                    <thead>
                                        <tr id="rowId1" >
                                            <td id="tableDesingTD" >S No</td>
                                            <td id="tableDesingTD"  >Vehicle type</td>
                                            <td id="tableDesingTD"  >Time Slot From(Days)</td>
                                            <td id="tableDesingTD"  >Time Slot To(Days)</td>
                                            <td id="tableDesingTD"  >Amount</td>
                                            <td id="tableDesingTD"  >Remarks</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <% int Sno = 0;%>
                                        <c:forEach items="${viewdetentioncharge}" var="vdc">
                                            <%
                                                        Sno++;
                                            %>
                                            <tr>
                                                <td   align="left"> <%= Sno%>  </td>
                                                <td   align="left"> <c:out value="${vdc.vehicleTypeName}" /></td>
                                                <td   align="left"> <c:out value="${vdc.timeSlot}" /></td>
                                                <td   align="left"> <c:out value="${vdc.timeSlotTo}" /></td>

                                                <td align="left"   >
                                                    <input type="text" readonly name="detentioncharge" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${vdc.chargeamt}" />"  style="width:150px;height:44px;"/>
                                                </td>
                                                <td   align="left"> <c:out value="${vdc.dcmremarks}" /></td>
                                                <td align="left"   ><input type="hidden"  name="denid" id="denid" value="<c:out value="${vdc.id}"/>" /></td>

                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <br>
                                <center>
                                    <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRowdetention()" />
                                    <!--<INPUT type="button" class="button" value="Delete Row" onclick="deleteRowdetention()" />-->
                                    <br>
                                    <!--<input type="button" class="btn btn-success" value="Save" onclick="Savedencharge(this.value)"/>-->
                                </center>
                                <SCRIPT language="javascript">
                                    var poItems1 = 1;
                                    var rowCount1 = 2;
                                    var sno1 = 1;
                                    var snumber1 = 1;

                                    function addRowdetention()
                                    {
                                        //                                           alert(rowCount);
                                        if (sno1 < 20) {
                                            sno1++;
                                            var tab = document.getElementById("detentiontable");
                                            rowCount1 = tab.rows.length;

                                            snumber1 = (rowCount1) - 1;
                                            if (snumber1 == 1) {
                                                snumber1 = parseInt(rowCount1);
                                            } else {
                                                snumber1++;
                                            }
                                            //                                            snumber1 = snumber1-1;
                                            //alert( (rowCount)-1) ;
                                            //alert(rowCount);
                                            var newrow = tab.insertRow((rowCount1));
                                            newrow.height = "30px";
                                            // var temp = sno1-1;
                                            var cell = newrow.insertCell(0);
                                            var cell0 = "<td class='text1' align='center'> " + snumber1 + "</td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(1);
                                            cell0 = "<td class='text1' ><select class='form-control' name='detention' id='detention" + snumber1 + "'  style='width:200px;height:44px;'><option value='0'>-Select-</option><c:if test = "${TypeList!= null}" ><c:forEach  items="${TypeList}" var="vehType"><option value='<c:out value="${vehType.typeId}" />'><c:out value="${vehType.typeName}" /></option></c:forEach ></c:if></select></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(2);
                                            cell0 = "<td class='text1' ><select   name='dcmunit' id='dcmunit" + snumber1 + "' class='form-control' style='width:200px;height:44px;'/><option value='0'>-Select-</option><c:if test = "${detaintionTimeSlot!= null}" ><c:forEach  items="${detaintionTimeSlot}" var="detain"><option value='<c:out value="${detain.id}" />'><c:out value="${detain.timeSlot}" /></option></c:forEach ></c:if></select></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(3);
                                            cell0 = "<td class='text1' ><select   name='dcmToUnit' id='dcmToUnit" + snumber1 + "' class='form-control' style='width:200px;height:44px;'/><option value='0'>-Select-</option><c:if test = "${detaintionTimeSlot!= null}" ><c:forEach  items="${detaintionTimeSlot}" var="detain"><option value='<c:out value="${detain.id}" />'><c:out value="${detain.timeSlot}" /></option></c:forEach ></c:if></select></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(4);
                                            cell0 = "<td class='text1' ><input type='text' name='chargeamt' id='chargeamt" + snumber1 + "' class='form-control' style='width:200px;height:44px;' onKeyPress='return onKeyPressBlockCharacters(event);' /></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(5);
                                            cell0 = "<td class='text1'><input type='text' name='dcmremarks' id='dcmremarks" + snumber1 + "' class='form-control' style='width:200px;height:44px;'/></td>";
                                            //cell.setAttribute(cssAttributeName,"text1");
                                            cell.innerHTML = cell0;

                                            rowCount1++;
                                        }
                                    }

                                        </SCRIPT>

                                    </div>
                                    <br>
                                    <center>
                                        <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    </center>
                                    <br>
                                </div>   



                                <div id="deD"  style="padding-left: 1px;" class="tab-pane">
                                    <div id="addDedicate"  style="padding-left: 1px;">
                                        <table  style="border-spacing: 40px 10px;height:25px; " cellspacing="10" id="dedicateDetails" border="1">
                                    <c:if test="${fuelHike != null}">
                                        <c:forEach items="${fuelHike}" var="fuelHike">
                                            <tr style="display:none"><td style="padding-left:10px;padding-right:10px;color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:25px;">Agreed Fuel Price</td>
                                                <td style="margin-left:-200px;">
                                                    <input type="text"  style="width:90px;height:25px;" class="textbox" name="agreedFuelPrice" id="agreedFuelPrice" value="68" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                                <td style="width:3px;">
                                                    <select type="text" name="uom" id="uom" style="width:120px;height:25px;" class="textbox">                                                        
                                                        <option value="1" selected>Litre</option>
                                                        <option value="2">Gallon</option>
                                                        <option value="3">Kilogram</option>
                                                    </select>
                                                    <script>
                                                        document.getElementById("uom").value =<c:out value="${fuelHike.uom}"/>;</script>
                                                </td>
                                                <td style="padding-left:10px;padding-right:10px;color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:25px;">Fuel Hike % for Revising Cost</td>
                                                <td><input type="text"  name="hikeFuelPrice" class="textbox" id="hikeFuelPrice" value="<c:out value="${fuelHike.fuelHikePercentage}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:120px;height:25px;"/></td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                </table>
                                <br>

                                <c:if test="${dedicateList != null}">
                                    <table align="center" border="1" class="table table-info mb30 table-hover" id="table">
                                        <thead>
                                            <tr >
                                                <th >S.No</th>
                                                <th >Vehicle Type</th>
                                                <th >Vehicle Units</th>
                                                <!--
                                                <th >Trailer Type</th>
                                                <th >Trailer Units</th>
                                                -->
                                                <th >Contract Category</th>
                                                <th >Fixed Cost Per Vehicle & Month</th>
                                                <th colspan="2" >Fixed Duration Per day<br>&emsp14;
                                        <table border="0" class="table table-info mb30" >
                                            <tr  >
                                                <td width="250px;"><center>Hours</center></td>
                                            <td width="250px;"><center>Minutes</center></td>
                                            </tr>
                                        </table>
                                        </th>

                                        <th >Rate Per KM</th>
                                        <th >Rate Exceeds Limit KM</th>
                                        <th >Max Allowable KM Per Month</th>
                                        <th  colspan="2">Over Time Cost Per HR<br>&emsp14;<table  border="0"><tr  id="tableDesingTD"><td width="250px;"><center>Work Days</center></td><td width="250px;"><center>Holidays</center></td></tr></table></th>
                                        <th >Additional Cost</th>
                                        <th >Active Status</th>
                                        <th >Vehicle & Trailer Details</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                            <% int index1 = 1;%>
                                            <c:forEach items="${dedicateList}" var="weight">
                                                <%
                                                    String classText1 = "";
                                                    int oddEven1 = index1 % 2;
                                                    if (oddEven1 > 0) {
                                                        classText1 = "text2";
                                                    } else {
                                                        classText1 = "text1";
                                                    }
                                                %>
                                                <tr height="25">
                                                    <td class="<%=classText1%>"  ><%=index1++%></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.vehicleTypeIdDedicate}"/></td>
                                                    <td class="<%=classText1%>"   ><input type="text" name="total" id="total<%=index1%>" value="<c:out value="${weight.vehicleUnitsDedicate}"/>" onchange="calculateTotalFixedCost1(<%=index1%>, this.value);" /></td>

                                                    <td class="<%=classText1%>"   >
                                                        <c:if test="${weight.contractCategory == '1'}" >
                                                            Fixed
                                                        </c:if>
                                                        <c:if test="${weight.contractCategory == '2'}" >
                                                            Actual
                                                        </c:if>
                                                        <input type="hidden" name="category" id="category<%=index1%>" value="<c:out value="${weight.contractCategory}"/>"/>
                                                        </select></td>
                                                    <td class="<%=classText1%>"   >

                                                        <input type="hidden" name="contractDedicateId" id="contractDedicateId<%=index1%>"  value="<c:out value="${weight.contractDedicateId}"/>"/>
                                                        <input type="text" name="fixedCostE" id="fixedCostE<%=index1%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${weight.fixedCost}"/>" onchange="calculateTotalFixedCost1(<%=index1%>, this.value);" /></td>
                                                    <td class="<%=classText1%>"   ><input type="text" name="fixedHrsE" id="fixedHrsE<%=index1%>" value="<c:out value="${weight.fixedHrs}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                                    <td class="<%=classText1%>"   ><input type="text" name="fixedMinE" id="fixedMinE<%=index1%>" value="<c:out value="${weight.fixedMin}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                            <input type="hidden" readonly name="totalFixedCostE" id="totalFixedCostE<%=index1%>" value="<c:out value="${weight.totalFixedCost}"/>"  onKeyPress="return onKeyPressBlockCharacters(event);" />
                                            <td class="<%=classText1%>"   ><input type="text" name="rateCostE" id="rateCostE<%=index1%>" value="<c:out value="${weight.rateCost}"/>"  onclick="setTextBoxEdit('<%=index1%>');" onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>
                                            <td class="<%=classText1%>"   ><input type="text" name="rateLimitE" id="rateLimitE<%=index1%>" value="<c:out value="${weight.rateLimit}"/>"  onclick="setTextBoxEdit('<%=index1%>')" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                            <td class="<%=classText1%>"   ><input type="text" name="maxAllowableKME" id="maxAllowableKME<%=index1%>" value="<c:out value="${weight.maxAllowableKM}"/>" onclick="setTextBoxEdit('<%=index1%>')" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                            <td class="<%=classText1%>"   ><input type="text" name="workingDaysE" id="workingDaysE<%=index1%>" value="<c:out value="${weight.workingDays}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                            <td class="<%=classText1%>"   ><input type="text" name="holidaysE" id="holidaysE<%=index1%>" value="<c:out value="${weight.holidays}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                            <td class="<%=classText1%>"   ><input type="text" name="addCostDedicateE" id="addCostDedicateE<%=index1%>" value="<c:out value="${weight.addCostDedicate}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                            <td class="<%=classText1%>"   >
                                                <select name="activeIndE" id="activeIndE<%=index1%>" >
                                                    <c:if test="${weight.activeInd == 'Y'}" >
                                                        <option value="Y" selected>Active</option>
                                                        <option value="N">In-Active</option>
                                                    </c:if>
                                                    <c:if test="${weight.activeInd == 'N'}" >
                                                        <option value="N" selected>In-Active</option>
                                                        <option value="Y" >Active</option>
                                                    </c:if>
                                                </select></td>
                                            <td class="<%=classText1%>"   >

                                                <c:if test="${weight.existingVehicleUnit == 0 }" >
                                                    <a href="/throttle/configureVehicleTrailerPage.do?vendorId=<c:out value="${vendorId}"/>&vehicleTypeId=<c:out value="${weight.vehicleTypeId}"/>&vehicleTypeDedicate=<c:out value="${weight.vehicleTypeIdDedicate}"/>&vehicleUnitsDedicate=<c:out value="${weight.vehicleUnitsDedicate}"/>&trailorTypeDedicate=<c:out value="${weight.trailorTypeDedicate}"/>&trailorUnitsDedicate=<c:out value="${weight.trailorUnitsDedicate}"/>&contractDedicateId=<c:out value="${weight.contractDedicateId}"/>&trailorTypeIdDedicate=<c:out value="${weight.trailorTypeIdDedicate}"/>" >Configure</a>

                                                </c:if>
                                                <c:if test="${weight.existingVehicleUnit > 0 }" >
                                                    <a href="/throttle/viewVehicleTrailerContract.do?vendorId=<c:out value="${vendorId}"/>&vehicleTypeId=<c:out value="${weight.vehicleTypeId}"/>&vehicleTypeIdDedicate=<c:out value="${weight.vehicleTypeIdDedicate}"/>&vehicleUnitsDedicate=<c:out value="${weight.vehicleUnitsDedicate}"/>&trailorTypeDedicate=<c:out value="${weight.trailorTypeDedicate}"/>&trailorUnitsDedicate=<c:out value="${weight.trailorUnitsDedicate}"/>&contractDedicateId=<c:out value="${weight.contractDedicateId}"/>&trailorTypeIdDedicate=<c:out value="${weight.trailorTypeIdDedicate}"/>" >View</a>
                                                </c:if>
                                                <c:if test="${weight.existingVehicleUnit > 0}" >
                                                    <a href="/throttle/editVehicleTrailerContract.do?vendorId=<c:out value="${vendorId}"/>&vehicleTypeId=<c:out value="${weight.vehicleTypeId}"/>&vehicleTypeIdDedicate=<c:out value="${weight.vehicleTypeIdDedicate}"/>&vehicleUnitsDedicate=<c:out value="${weight.vehicleUnitsDedicate}"/>&trailorTypeDedicate=<c:out value="${weight.trailorTypeDedicate}"/>&trailorUnitsDedicate=<c:out value="${weight.trailorUnitsDedicate}"/>&contractDedicateId=<c:out value="${weight.contractDedicateId}"/>&trailorTypeIdDedicate=<c:out value="${weight.trailorTypeIdDedicate}"/>" >Edit</a>
                                                </c:if>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                    </table>
                                    <br/>

                                    <script>
                                        //                              function addNewRowDedicate1() {
                                        //                                alert("Hi Iam Here");
                                        //                            }
                                        var containerDedicate = "";
                                        function addNewRowDedicate() {
                                            // alert("Hi Iam Here");
                                            $('#AddNewRowFullTruck').hide();
                                            var iCnt = <%=index1%>;
                                            var rowCnt = <%=index1%>;
                                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                            containerDedicate = $($("#addDedicate")).css({
                                                padding: '5px', margin: '20px', width: '100%', border: '0px dashed',
                                                borderTopColor: '#999', borderBottomColor: '#999',
                                                borderLeftColor: '#999', borderRightColor: '#999'
                                            });
                                            //                                    alert(iCnt);
                                            $(containerDedicate).last().after('\
                                           <table align="center"  id="routeDetails1' + iCnt + '"  border="1" class="table table-info mb30 table-hover" style="width:90%;">\n\
                                            <thead >\n\
                                            <tr id="tableDesingTH" height="30">\n\
                                            <td>Sno</td>\n\
                                            <td>Vehicle Type</td>\n\
                                            <td>Vehicle Units</td>\n\
                                             <td>Contract Category</td>\n\
                                           <td>Fixed Cost Per Vehicle & Month</td>\n\
                                            <td colspan="2"><br>Fixed Duration <br>Per day<br>\n\
                                            <table  border="0">\n\
                                              <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;"><td width="235px;">Hours</td><td width="206px;">Minutes</td></tr></table></td>\n\
                                            <td>Rate Per KM</td>\n\
                                            <td>Rate Exceeds Limit KM</td>\n\
                                            <td>Max Allowable KM Per Month</td>\n\
                                            <td colspan="2"><br><br>Over Time Cost Per HR<br><br>\n\
                                            <table  border="0">\n\
                                              <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;"><td width="245px;">Work Days</td><td width="216px;height="20px;">Holidays</td></tr></table></td>\n\
                                            <td>Additional Cost</td></tr>\n\
                                             <tr  >\n\
                                           <td> ' + iCnt + ' </td>\n\
                                           <td><select type="text" name="vehicleTypeIdDedicate" id="vehicleTypeIdDedicate' + iCnt + '" onchange="onSelectVal(this.value,' + iCnt + ');" ><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                           <td><input type="text" name="vehicleUnitsDedicate" id="vehicleUnitsDedicate' + iCnt + '" value="0" onchange="calculateTotalFixedCost(' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:80px;"/></td>\n\
                                            <td><select type="text" name="contractCategory" id="contractCategory' + iCnt + '" onchange="setTextBoxEnable(this.value,' + iCnt + ');" style="height:22px;width:80px;"><option value="0">--Select--</option>\n\
                                                <option value="1" >Fixed</option>\n\
                                                 <option value="2" >Actual</option></select></td>\n\
                                           <td><input type="text" name="fixedCost" id="fixedCost' + iCnt + '" value="0" onchange="calculateTotalFixedCost(' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                           <td><select name="fixedHrs" id="fixedHrs' + iCnt + '" class="textbox" style="height:22px;width:45px;">\n\
                                                      <option value="00">00</option>\n\
                                                    <option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option></select>\n\
                                           </td>\n\
                                            <td><select type="text" name="fixedMin" id="fixedMin' + iCnt + '" class="textbox" style="height:22px;width:45px;"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>\n\
                                           </td>\n\
                                            <input type="hidden" readonly name="totalFixedCost" id="totalFixedCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" />\n\
                                            <td><input type="text" name="rateCost" id="rateCost' + iCnt + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                            <td><input type="text" name="rateLimit" id="rateLimit' + iCnt + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                            <td><input type="text" name="maxAllowableKM" id="maxAllowableKM' + iCnt + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                            <td><input type="text" name="workingDays" id="workingDays' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:50px;"/></td>\n\
                                            <td><input type="text" name="holidays" id="holidays' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:50px;"/></td>\n\
                                            <td><input type="text" name="addCostDedicate" id="addCostDedicate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                             </tr>\n\
                                            </table>\n\
                                           <br><table border="0" id="buttonDetails1' + iCnt + '" ><tr>\n\
                                            <td>\n\
                                            <input class="btn btn-info" type="button" name="addRouteDetailsFullTruck1" id="addRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="Add" onclick="addRows(' + iCnt + ')" style="width:90px;height:30px;font-weight: bold;padding: 1px;">\n\
                                            <input class="btn btn-info" type="button" name="removeRouteDetailsFullTruck1" id="removeRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRows(' + iCnt + ')" style="width:90px;height:30px;font-weight: bold;padding: 1px;">\n\
                                       </tr></thead></table></td></tr></table>');
                                        }
                                            </script>
                                            <script>

                                                function addRows(val) {
                                                    var iCnt = <%=index1%>;
                                                    var loadCnt = val;
                                                    var routeInfoSize = $('#routeDetails1' + loadCnt + ' tr').size();
                                                    var routeInfoSizeSub = iCnt + routeInfoSize - 3;


                                                    $('#routeDetails1' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSizeSub + '</td>\n\
                                                   <td><select type="text" name="vehicleTypeIdDedicate" id="vehicleTypeIdDedicate' + routeInfoSizeSub + '" onchange="onSelectVal(this.value,' + routeInfoSizeSub + ');" ><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                           <td><input type="text" name="vehicleUnitsDedicate" id="vehicleUnitsDedicate' + routeInfoSizeSub + '" value="0" onchange="calculateTotalFixedCost(' + routeInfoSizeSub + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:80px;"/></td>\n\
                                            <td><select type="text" name="contractCategory" id="contractCategory' + routeInfoSizeSub + '" onchange="setTextBoxEnable(this.value,' + routeInfoSizeSub + ');" style="height:22px;width:80px;"><option value="0">--Select--</option>\n\
                                                <option value="1" >Fixed</option>\n\
                                                 <option value="2" >Actual</option></select></td>\n\
                                           <td><input type="text" name="fixedCost" id="fixedCost' + routeInfoSizeSub + '" value="0" onchange="calculateTotalFixedCost(' + routeInfoSizeSub + ')" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                           <td><select name="fixedHrs" id="fixedHrs' + routeInfoSizeSub + '" class="textbox" style="height:22px;width:45px;">\n\
                                                      <option value="00">00</option>\n\
                                                    <option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option></select>\n\
                                           </td>\n\
                                            <td><select type="text" name="fixedMin" id="fixedMin' + routeInfoSizeSub + '" class="textbox" style="height:22px;width:45px;"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>\n\
                                           </td>\n\
                                            <input type="hidden" readonly name="totalFixedCost" id="totalFixedCost' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" />\n\
                                            <td><input type="text" name="rateCost" id="rateCost' + routeInfoSizeSub + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                            <td><input type="text" name="rateLimit" id="rateLimit' + routeInfoSizeSub + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                            <td><input type="text" name="maxAllowableKM" id="maxAllowableKM' + routeInfoSizeSub + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                            <td><input type="text" name="workingDays" id="workingDays' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:50px;"/></td>\n\
                                            <td><input type="text" name="holidays" id="holidays' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:50px;"/></td>\n\
                                            <td><input type="text" name="addCostDedicate" id="addCostDedicate' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                             </tr>\n\
                                                  </tr>');
                                                    // alert("loadCnt = "+loadCnt)
                                                    loadCnt++;
                                                    //   alert("loadCnt = +"+loadCnt)
                                                }
                                                function onSelectVal(val, countVal) {
                                                    //                            alert("vehicleTypeIdDedicate"+val);
                                                    document.getElementById("vehicleTypeIddeDTemp" + countVal).value = val;
                                                }


                                                function deleteRows(val) {
                                                    var loadCnt = val;
                                                    //     alert(loadCnt);
                                                    var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                                                    var routeInfoDetails = "routeDetails1" + loadCnt;
                                                    // alert(routeInfoDetails);
                                                    if ($('#routeDetails1' + loadCnt + ' tr').size() > 4) {
                                                        $('#routeDetails1' + loadCnt + ' tr').last().remove();
                                                        loadCnt = loadCnt - 1;
                                                    } else {
                                                        //                                               alert('One row should be present in table');
                                                        $('#dedicateDetails' + loadCnt).remove();
                                                        $('#routeDetails1' + loadCnt).remove();
                                                        $('#buttonDetails1' + loadCnt).remove();
                                                        $('#AddNewRowFullTruck').show();
                                                    }
                                                }
                                                function calculateTotalFixedCost(sno) {
                                                    //        alert(sno);
                                                    var vehicleUnits = document.getElementById("vehicleUnitsDedicate" + sno).value;
                                                    var fixedCost = document.getElementById("fixedCost" + sno).value;
                                                    var totalCost = parseInt(vehicleUnits) * parseInt(fixedCost);
                                                    document.getElementById("totalFixedCost" + sno).value = totalCost;
                                                }


                                                function calculateTotalFixedCost1(sno1) {
                                                    //     alert("dfgfgdfg"+sno1);
                                                    var vehicleUnits1 = document.getElementById("total" + sno1).value;
                                                    var fixedCostE = document.getElementById("fixedCostE" + sno1).value;
                                                    // alert("fixedCostE"+fixedCostE);
                                                    var total = parseInt(vehicleUnits1) * parseInt(fixedCostE);
                                                    // alert(total)
                                                    // document.getElementByName("total" ).value = total;
                                                    document.getElementById("totalFixedCostE" + sno1).value = total;
                                                }



                                                function setTextBoxEnable(val, count) {

                                                    if (val == 1) {  //fixed part
                                                        document.getElementById("rateCost" + count).readOnly = true;
                                                        document.getElementById("rateLimit" + count).readOnly = false;
                                                        document.getElementById("maxAllowableKM" + count).readOnly = false;
                                                        document.getElementById("totalFixedCost" + count).readOnly = false;
                                                        document.getElementById("fixedCost" + count).readOnly = false;
                                                        document.getElementById("rateCost" + count).value = 0;
                                                    }
                                                    if (val == 2) {
                                                        document.getElementById("rateCost" + count).readOnly = false;
                                                        document.getElementById("rateLimit" + count).readOnly = true;
                                                        document.getElementById("maxAllowableKM" + count).readOnly = true;
                                                        document.getElementById("totalFixedCost" + count).readOnly = true;
                                                        document.getElementById("fixedCost" + count).readOnly = true;

                                                        document.getElementById("totalFixedCost" + count).value = 0;
                                                        document.getElementById("fixedCost" + count).value = 0;
                                                        document.getElementById("rateLimit" + count).value = 0;
                                                        document.getElementById("maxAllowableKM" + count).value = 0;

                                                    }

                                                }

                                                function setTextBoxEdit(v2) {
                                                    var val = document.getElementById("category" + v2).value
                                                    //  alert(val)
                                                    if (val == 1) {
                                                        // alert("fin0")
                                                        document.getElementById("rateCostE " + v2).readOnly = true;
                                                        // alert("fin1")

                                                        document.getElementById("rateLimitE" + v2).readOnly = false;
                                                        //                                                      alert("fin2")
                                                        document.getElementById("maxAllowableKME" + v2).readOnly = false;
                                                        //                                                    alert("fin3")
                                                    }
                                                    if (val == 2) {

                                                        document.getElementById("rateCostE" + v2).readOnly = false;
                                                        document.getElementById("rateLimitE" + v2).readOnly = true;
                                                        document.getElementById("maxAllowableKME" + v2).readOnly = true;
                                                    }
                                                }
                                            </script>

                                            <script>
                                                function callOriginAjaxdeD(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
                                                    //alert(val);
                                                    var pointNameId = 'originNamedeD' + val;
                                                    var pointIdId = 'originIddeD' + val;
                                                    var desPointName = 'destinationNamedeD' + val;
                                                    //alert(prevPointId);
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getTruckCityList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    cityName: request.term,
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                },
                                                                error: function(data, type) {

                                                                    //console.log(type);
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.Name;
                                                            var id = ui.item.Id;
                                                            //alert(id+" : "+value);
                                                            $('#' + pointIdId).val(id);
                                                            $('#' + pointNameId).val(value);
                                                            $('#' + desPointName).focus();
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        var itemVal = item.Name;
                                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + itemVal + "</a>")
                                                                .appendTo(ul);
                                                    };
                                                }

                                                function callDestinationAjaxdeD(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
                                                    //alert(val);
                                                    var pointNameId = 'destinationNamedeD' + val;
                                                    var pointIdId = 'destinationIddeD' + val;
                                                    var originPointId = 'originIddeD' + val;
                                                    var truckRouteId = 'routeIddeD' + val;
                                                    var travelKm = 'travelKmdeD' + val;
                                                    var travelHour = 'travelHourdeD' + val;
                                                    var travelMinute = 'travelMinutedeD' + val;
                                                    //alert(prevPointId);
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getTruckCityList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    cityName: request.term,
                                                                    originCityId: $("#" + originPointId).val(),
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                },
                                                                error: function(data, type) {

                                                                    //console.log(type);
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.Name;
                                                            var id = ui.item.Id;
                                                            //alert(id+" : "+value);
                                                            $('#' + pointIdId).val(id);
                                                            $('#' + pointNameId).val(value);
                                                            $('#' + travelKm).val(ui.item.TravelKm);
                                                            $('#' + travelHour).val(ui.item.TravelHour);
                                                            $('#' + travelMinute).val(ui.item.TravelMinute);
                                                            $('#' + truckRouteId).val(ui.item.RouteId);
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        var itemVal = item.Name;
                                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + itemVal + "</a>")
                                                                .appendTo(ul);
                                                    };
                                                }

                                                function setContainerQtyList(val, sno) {
                                                    if (val == '1') {
                                                        // 20FT vehicle
                                                        $('#containerQty' + sno).empty();
                                                        $('#containerQty' + sno).append($('<option ></option>').val(2).html('2'))
                                                    } else if (val == '2') {
                                                        // 40FT vehicle
                                                        $('#containerQty' + sno).empty();
                                                        $('#containerQty' + sno).append($('<option ></option>').val(1).html('1'))

                                                    }
                                                }
                                                function setContainerTypeList(val, sno) {
//                                    alert(sno)
                                                    if (val == '1058') {
                                                        // 20FT vehicle
                                                        $('#containerTypeId' + sno).empty();
                                                        $('#containerTypeId' + sno).append($('<option ></option>').val(1).html('20'))
                                                        $('#containerQty' + sno).empty();
                                                        $('#containerQty' + sno).append($('<option ></option>').val(1).html('1'))
                                                    } else if (val == '1059') {
                                                        // 40FT vehicle
                                                        $('#containerTypeId' + sno).empty();
                                                        $('#containerTypeId' + sno).append($('<option ></option>').val(0).html('--Select--'))
                                                        $('#containerTypeId' + sno).append($('<option ></option>').val(1).html('20'))
                                                        $('#containerTypeId' + sno).append($('<option ></option>').val(2).html('40'))
                                                        $('#containerQty' + sno).empty();
                                                    }
                                                }

                                                function getMarketHireRate(sno, tableRow) {
//                                    var containerTypeId = $("#containerTypeId"+sno).val();
//                                    var containerQty = $("#containerQty"+sno).val();
                                                    var vehicleTypeId = $("#vehicleTypeId" + sno).val();
                                                    var loadTypeId = $("#loadTypeId" + sno).val();
                                                    var originIdFullTruck = $("#originIdFullTruck" + tableRow).val();
                                                    var pointId1 = $("#pointId1" + tableRow).val();
                                                    var pointId2 = $("#pointId2" + tableRow).val();
                                                    var pointId3 = $("#pointId3" + tableRow).val();
                                                    var pointId4 = $("#pointId4" + tableRow).val();
                                                    var destinationIdFullTruck = $("#destinationIdFullTruck" + tableRow).val();
//                                    alert("vehicleTypeId"+vehicleTypeId);
//                                    alert("loadTypeId"+loadTypeId);
//                                    alert("originIdFullTruck"+originIdFullTruck);
//                                    alert("pointId1"+pointId1);
//                                    alert("destinationIdFullTruck"+destinationIdFullTruck);
                                                    contractExists(sno, tableRow);
                                                    if (loadTypeId != '0' && loadTypeId != '' && vehicleTypeId != '0' && vehicleTypeId != ''
                                                            && originIdFullTruck != '' && destinationIdFullTruck != '') {
                                                        $.ajax({
                                                            url: "/throttle/getMarketHireRate.do",
                                                            dataType: "json",
                                                            data: {
                                                                vehicleTypeId: vehicleTypeId,
//                                            containerTypeId: containerTypeId,
//                                            containerQty: containerQty,
                                                                originIdFullTruck: originIdFullTruck,
                                                                pointId1: pointId1,
                                                                pointId2: pointId2,
                                                                pointId3: pointId3,
                                                                pointId4: pointId4,
                                                                destinationIdFullTruck: destinationIdFullTruck,
//                                            loadTypeId:loadTypeId
                                                            },
                                                            success: function(data, textStatus, jqXHR) {
                                                                if (data.MarketHireRate == '0') {
//                                                                    $("#statusSpan").text("Route not defined in organisation");
                                                                    $("#marketRate" + sno).val(0);
                                                                    $("#marketRateSpan" + sno).text(0);
//                                                                    $("#vehicleTypeId" + sno).val(0);
//                                                $("#containerTypeId"+sno).val(0);
//                                                $("#containerQty"+sno).val(0);
                                                                } else if (data.MarketHireRate != '') {
                                                                    $("#statusSpan").text("");
                                                                    $("#marketRate" + sno).val(data.MarketHireRate);
                                                                    $("#marketRateSpan" + sno).text(data.MarketHireRate);
                                                                }
                                                            },
                                                            error: function(data, type) {
                                                            }
                                                        });
                                                    }

                                                }

                                                function checkMarketRate(val, sno) {
                                                    var spotCost = $("#spotCost" + sno).val();
                                                    var additionalCost = $("#additionalCost" + sno).val();
                                                    var marketRate = $("#marketRate" + sno).val();

                                                    var sts = false;
                                                    if (parseFloat(spotCost) > parseFloat(marketRate)) {
                                                        sts = true;
                                                    }
                                                    if (parseFloat(additionalCost) > parseFloat(marketRate)) {
                                                        sts = true;
                                                    }
//                                                    if (sts) {
//                                                        $("#approvalStatusSpan" + sno).text("Needs Approval");
//                                                        $("#approvalStatusSpan" + sno).removeClass("noErrorClass");
//                                                        $("#approvalStatusSpan" + sno).addClass("errorClass");
//                                                    } else {
                                                    $("#approvalStatusSpan" + sno).text("Auto Approval");
                                                    $("#approvalStatusSpan" + sno).removeClass("errorClass");
                                                    $("#approvalStatusSpan" + sno).addClass("noErrorClass");
//                                                    }
                                                }
                                            </script>



                                </c:if>
                                <!--</div>-->

                            </div>

                            <br>
                            <center>
                                <a href="#"><input type="button" class="btn btn-info" value="AddNewRow" id="AddNewRowFullTruck" name="addRowDedicate" onclick="addNewRowDedicate();" style="width:90px;height:30px;font-weight: bold;padding:1px;"/></a>
                                <a><input type="button" class="btn btn-info btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>

                        </div>
                        <br>
                        <center>
                            <input type="button" class="btn btn-info" value="Submit" onclick="submitPage();" style="width:150px;height:30px;font-weight: bold;padding:1px;"/>

                        </center>
                        <br>
                        <br>
                        <br>
                    </div>

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5"  selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>   

                    <script>
                        $('.btnNext').click(function() {
                            $('.nav-tabs > .active').next('li').find('a').trigger('click');
                        });
                        $('.btnPrevious').click(function() {
                            $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                        });
                    </script>
                    <%--<%@ include file="/content/common/NewDesign/commonParameters.jsp" %>--%>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>