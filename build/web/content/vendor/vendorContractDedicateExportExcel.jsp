<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>  
        <style> table, td {border:1px solid black} table {border-collapse:collapse}</style>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
              String menuPath = "Vendor >> vendor list";
              request.setAttribute("menuPath", menuPath);
//              String dateval = request.getParameter("dateval");
//              String active = request.getParameter("active");
//              String type = request.getParameter("type");
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                       Date dNow = new Date();
                       SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mm");
                       //System.out.println("Current Date: " + ft.format(dNow));
                       String curDate = ft.format(dNow);
                       String expFile = "VendorContract-Dedicate" + curDate + ".xls";

                       String fileName = "attachment;filename=" + expFile;
                       response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                       response.setHeader("Content-disposition", fileName);
            %>



            <br>
            <br>
            <br>

            <br><br>

            <br>
            <br>
            <c:if test="${dedicateList != null}">
                <table align="center" border="1" class="table table-info mb30 table-hover" id="table">
                    <thead>
                        <tr >
                            <th >S.No</th>
                            <th >Vendor Name</th>
                            <th >Vehicle Type</th>
                            <th >Vehicle Units</th>
                            <th >Contract Category</th>
                            <th >Fixed Cost Per Vehicle & Month</th>
                            <th colspan="2" >Fixed Duration Per day<br>
                                <table border="0" class="table table-info mb30" >
                                    <tr  >
                                        <td width="250px;"><center>Hours</center></td>
                                        <td width="250px;"><center>Minutes</center></td>
                                    </tr>
                                </table>
                            </th>

                            <th >Rate Per KM</th>
                            <th >Rate Exceeds Limit KM</th>
                            <th >Max Allowable KM Per Month</th>
                            <th  colspan="2">Over Time Cost Per HR<br>
                                <table  border="0"><tr  id="tableDesingTD"><td width="250px;"><center>Work Days</center></td><td width="250px;"><center>Holidays</center></td></tr></table></th>
                            <th >Additional Cost</th>

                        </tr>
                    </thead>
                    <tbody>
                        <% int index1 = 1;%>
                        <c:forEach items="${dedicateList}" var="weight">
                            <%
                                String classText1 = "";
                                int oddEven1 = index1 % 2;
                                if (oddEven1 > 0) {
                                    classText1 = "text2";
                                } else {
                                    classText1 = "text1";
                                }
                            %>
                            <tr height="25">
                                <td class="<%=classText1%>"  ><%=index1++%></td>
                                <td class="<%=classText1%>"   ><c:out value="${weight.vendorName}"/></td>
                                <td class="<%=classText1%>"   ><c:out value="${weight.vehicleTypeIdDedicate}"/></td>
                                <td class="<%=classText1%>"   ><c:out value="${weight.vehicleUnitsDedicate}"/></td>

                                <td class="<%=classText1%>"   >
                                    <c:if test="${weight.contractCategory == '1'}" >
                                        Fixed
                                    </c:if>
                                    <c:if test="${weight.contractCategory == '2'}" >
                                        Actual
                                    </c:if>
                                    </select></td>
                                <td class="<%=classText1%>"   >

                                    <c:out value="${weight.fixedCost}"/></td>
                                <td class="<%=classText1%>"   ><c:out value="${weight.fixedHrs}"/></td>
                                <td class="<%=classText1%>"   ><c:out value="${weight.fixedMin}"/></td>
                                <td class="<%=classText1%>"   ><c:out value="${weight.rateCost}"/></td>
                                <td class="<%=classText1%>"   ><c:out value="${weight.rateLimit}"/></td>
                                <td class="<%=classText1%>"   ><c:out value="${weight.maxAllowableKM}"/></td>
                                <td class="<%=classText1%>"   ><c:out value="${weight.workingDays}"/></td>
                                <td class="<%=classText1%>"   ><c:out value="${weight.holidays}"/></td>
                                <td class="<%=classText1%>"   ><c:out value="${weight.addCostDedicate}"/></td>

                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>