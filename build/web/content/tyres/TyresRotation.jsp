<%-- 
    Document   : TyresRotation
    Created on : 15 Mar, 2012, 8:26:55 PM
    Author     : kannan
--%>

<%@page contentType="text/html" import="java.sql.*,java.util.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <title>ViewTyreRotationDetails</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript">

        function show_src() {
            document.getElementById('exp_table').style.display='none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display='block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display='none';
        }

        </script>

    </head>

    <body>

        <form name="tyresRotation" action="/throttle/saveRotation.do">

            <c:set var="vehicleId" value=""></c:set>
            <c:set var="vehicleRegNo" value=""></c:set>
            <c:set var="mfrName" value=""></c:set>
            <c:set var="modelName" value=""></c:set>

            <c:set var="currentRotationNo" value=""></c:set>
            <c:set var="currentRotationDate" value=""></c:set>
            <c:set var="currentRotationKm" value=""></c:set>
             <c:set var="currentRemarks" value=""></c:set>
            <c:set var="lastRotationDate" value=""></c:set>

            <c:set var="frontLeftTyreId" value=""></c:set>
            <c:set var="frontRightTyreId" value=""></c:set>
            <c:set var="rearLeftInnerTyreId" value=""></c:set>
            <c:set var="rearLeftOuterTyreId" value=""></c:set>
            <c:set var="rearRightInnerTyreId" value=""></c:set>
            <c:set var="rearRightOuterTyreId" value=""></c:set>
            <c:set var="stephneyTyreId" value=""></c:set>

            <c:set var="frontLeftPositionId" value=""></c:set>
            <c:set var="frontRightPositionId" value=""></c:set>
            <c:set var="rearLeftInnerPositionId" value=""></c:set>
            <c:set var="rearLeftOuterPositionId" value=""></c:set>
            <c:set var="rearRightInnerPositionId" value=""></c:set>
            <c:set var="rearRightOuterPositionId" value=""></c:set>
            <c:set var="stephneyPositionId" value=""></c:set>

            <c:set var="frontLeft" value=""></c:set>
            <c:set var="frontRight" value=""></c:set>
            <c:set var="rearLeftInner" value=""></c:set>
            <c:set var="rearLeftOuter" value=""></c:set>
            <c:set var="rearRightInner" value=""></c:set>
            <c:set var="rearRightOuter" value=""></c:set>
            <c:set var="stephney" value=""></c:set>


    <c:if test = "${vehicleList != null}" >
        <c:forEach items="${vehicleList}" var="vd">
            <c:set var="vehicleId" value="${vd.vehicleId}"></c:set>
            <c:set var="vehicleRegNo" value="${vd.vehicleRegNo}"></c:set>
            <c:set var="mfrName" value="${vd.mfrName}"></c:set>
            <c:set var="modelName" value="${vd.modelName}"></c:set>
        </c:forEach >
    </c:if>

    <c:if test = "${tyresRotationList != null}" >
        <c:forEach items="${tyresRotationList}" var="tr">
            <c:set var="currentRotationNo" value="${tr.tyreRotationNo}"></c:set>
            <c:set var="currentRotationDate" value="${tr.tyreRotationDate}"></c:set>
            <c:set var="currentRotationKm" value="${tr.tyreRotationKm}"></c:set>
            <c:set var="currentRemarks" value="${tr.tyreRotationRemarks}"></c:set>
            <c:set var="lastRotationDate" value="${tr.tyreRotationDate}"></c:set>
        </c:forEach>
    </c:if>



    <c:if test = "${currentTyrePosition != null}" >
    <c:forEach items="${currentTyrePosition}" var="curr">        
        <c:if test = "${curr.tyrePosition eq 'FL'}" >             
            <c:set var="frontLeftTyreId" value="${curr.tyreId}"></c:set>
            <c:set var="frontLeftTyrePositionId" value="${curr.tyrePositionId}"></c:set>
            <c:set var="frontLeft" value="${curr.tyreNumber}"></c:set>
           
        </c:if>
        
        <c:if test = "${curr.tyrePosition eq 'FR'}" >
            <c:set var="frontRightTyreId" value="${curr.tyreId}"></c:set>
            <c:set var="frontRightTyrePositionId" value="${curr.tyrePositionId}"></c:set>
            <c:set var="frontRight" value="${curr.tyreNumber}"></c:set>
        </c:if>
        <c:if test = "${curr.tyrePosition eq 'RLI'}" >
            <c:set var="rearLeftTyreId" value="${curr.tyreId}"></c:set>
            <c:set var="rearLeftTyrePositionId" value="${curr.tyrePositionId}"></c:set>
            <c:set var="rearLeftInner" value="${curr.tyreNumber}"></c:set>
        </c:if>
        <c:if test = "${curr.tyrePosition eq 'RLO'}" >
            <c:set var="rearLeftOuterTyreId" value="${curr.tyreId}"></c:set>
            <c:set var="rearLeftOuterTyrePositionId" value="${curr.tyrePositionId}"></c:set>
            <c:set var="rearLeftOuter" value="${curr.tyreNumber}"></c:set>
        </c:if>
        <c:if test = "${curr.tyrePosition eq 'RRI'}" >
            <c:set var="rearRightInnerTyreId" value="${curr.tyreId}"></c:set>
            <c:set var="rearRightInnerTyrePositionId" value="${curr.tyrePositionId}"></c:set>
            <c:set var="rearRightInner" value="${curr.tyreNumber}"></c:set>
        </c:if>
        <c:if test = "${curr.tyrePosition eq 'RRO'}" >
            <c:set var="rearRightOuterTyreId" value="${curr.tyreId}"></c:set>
            <c:set var="rearRightOuterTyrePositionId" value="${curr.tyrePositionId}"></c:set>
            <c:set var="rearRightOuter" value="${curr.tyreNumber}"></c:set>
        </c:if>
        <c:if test = "${curr.tyrePosition eq 'Stephney'}" >
            <c:set var="stephneyTyreId" value="${curr.tyreId}"></c:set>
            <c:set var="stephneyTyrePositionId" value="${curr.tyrePositionId}"></c:set>
            <c:set var="stephney" value="${curr.tyreNumber}"></c:set>
        </c:if>

    </c:forEach >
    </c:if>


        <center><h2>Tyres Rotation</h2></center>
        <table width="800" align="center" class="table2" cellpadding="0" cellspacing="0">
            <tr><td class="contenthead" colspan="4">Vehicle Details</td></tr>
            <tr><td class="texttitle1">Vehicle No</td><td class="text1"><c:out value="${vehicleRegNo}" />
                    <input type="hidden" name="vehicleId" value="<c:out value="${vehicleId}" />" />
                </td>
                <td class="texttitle1">MFR Name</td><td class="text1"><c:out value="${mfrName}" /></td>
            </tr>
            <tr><td class="texttitle2">Model Name</td><td class="text2"><c:out value="${modelName}" /></td>
                <td class="texttitle2">Last Rotation Date</td><td class="text2"></td>
            </tr>
       
            <tr><td class="contenthead" colspan="4">Tyre Rotations(Current)</td></tr>
            <tr align="left">
                <td class="texttitle1">Rotation No</td><td class="text1"><c:out value="${currentRotationNo}" /></td>
                <td class="texttitle1">Rotation Date</td><td class="text1"><c:out value="${lastRotationDate}" /></td>
            </tr>
            <tr align="left">
                <td class="texttitle2">Run KM</td><td class="text2"><c:out value="${currentRotationKm}" /></td>
                <td class="texttitle2">Remarks</td><td class="text2"><c:out value="${currentRemarks}" /></td>
            </tr>
        </table>        
        <table width="800" align="center" class="table2" cellpadding="0" cellspacing="0">
            <tr><td class="contenthead" colspan="4">Tyre Rotations(Current)</td></tr>

            <!--<tr><td class="titleHead">Position</td><td class="titleHead">Tyre No</td><td class="titleHead">Position</td><td class="titleHead">Tyre No</td></tr>-->

            <tr align="center">
                <td class="texttitle1">Front (Left):&nbsp;<c:out value="${frontLeft}" /> </td>
                
                <td class="texttitle1">Front(Right):&nbsp;<c:out value="${frontRight}" /> </td>
                
            </tr>
            <tr align="center">
                <td class="texttitle2">Rear (Left-Out):&nbsp;<c:out value="${rearLeftOuter}" /> &emsp;&emsp; Rear (Left-In):&nbsp;<c:out value="${rearLeftInner}" /> </td>
                <td class="texttitle2">Rear(Right-In):&nbsp;<c:out value="${rearRightInner}" />  &emsp;&emsp; Rear(Right-Out):&nbsp;<c:out value="${rearRightOuter}" /> </td>
            </tr>            
            <tr align="center">
                <td  colspan="2" class="texttitle1">Stepny:&nbsp;<c:out value="${stephney}" />  </td>
            </tr>
        </table>
        
        <table width="800" align="center" class="table2" cellpadding="0" cellspacing="0">
            <tr><td class="contenthead" colspan="4">Tyre Rotations(New)</td></tr>
            <tr align="left">
                <td class="texttitle1">Rotation No</td><td class="texttitle1"><input type="text" name="rotationNo" id="rotationNo" class="textbox" style="width:80px;" /> </td>
                <td class="texttitle1">Rotation Date</td><td class="texttitle1"><input type="text" name="rotationDate" id="rotationDate" class="textbox" style="width:80px;" /><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.tyresRotation.rotationDate,'dd-mm-yyyy',this)"/></td>
            </tr>
            <tr align="left">
                <td class="texttitle2">Run KM</td><td class="texttitle2"><input type="text" name="runKm" id="runKm" class="textbox" style="width:80px;" /> </td>
                <td class="texttitle2">Remarks</td><td class="texttitle2"><input type="text" name="remarks" id="remarks" class="textbox" style="width:80px;" /></td>
            </tr>
        </table>
        <table width="800" align="center" class="table2" cellpadding="0" cellspacing="0">
            <tr><td class="contenthead" colspan="4">Tyres(New)</td></tr>
            
            <!--<tr><td class="titleHead">Position</td><td class="titleHead">Tyre No</td><td class="titleHead">Position</td><td class="titleHead">Tyre No</td></tr>-->
<!--
            <tr>
                <td class="texttitle1">Rotation: &nbsp;<input type="text" class="textbox" style="width:80px;" /></td>
                <td class="texttitle1">Date: &nbsp;<input type="text" class="textbox" style="width:80px;" /></td>
            </tr>
-->
            <tr align="center">
                <td class="texttitle1">Front(Left): &nbsp;
                    <select name="tyreId" class="textbox" style="width:80px;">
                        <option value="">-select-</option>
                        <c:if test = "${tyresList != null}" >
                        <c:forEach items="${tyresList}" var="tr1">
                        <option  value='<c:out value="${tr1.tyreId}" />'>
                            <c:out value="${tr1.tyreNumber}" /></option>
                        </c:forEach >
                        </c:if>
                    </select>
                    <input type="hidden" name="positionId" value="4" />
                </td>
                <td class="texttitle1">Front(Right): &nbsp;<select name="tyreId" class="textbox" style="width:80px;">
                        <option value="">-select-</option>
                       <c:if test = "${tyresList != null}" >
                        <c:forEach items="${tyresList}" var="tr1">
                        <option  value='<c:out value="${tr1.tyreId}" />'>
                            <c:out value="${tr1.tyreNumber}" /></option>
                        </c:forEach >
                        </c:if>
                    </select>
                    <input type="hidden" name="positionId" value="2" />
                </td>
            </tr>
            <tr align="center">
                <td class="texttitle2">Rear(Left-Out): &nbsp;<select name="tyreId" class="textbox" style="width:80px;">
                        <option value="">-select-</option>
                      <c:if test = "${tyresList != null}" >
                        <c:forEach items="${tyresList}" var="tr1">
                        <option  value='<c:out value="${tr1.tyreId}" />'>
                            <c:out value="${tr1.tyreNumber}" /></option>
                        </c:forEach >
                        </c:if>
                    </select>  <input type="hidden" name="positionId" value="1" />
                    &emsp;&emsp; Rear(Left-In): &nbsp;<select name="tyreId" class="textbox" style="width:80px;">
                        <option value="">-select-</option>
                       <c:if test = "${tyresList != null}" >
                        <c:forEach items="${tyresList}" var="tr1">
                        <option  value='<c:out value="${tr1.tyreId}" />'>
                            <c:out value="${tr1.tyreNumber}" /></option>
                        </c:forEach >
                        </c:if>
                    </select>
                    <input type="hidden" name="positionId" value="3" />
                </td>
                
                <td class="texttitle2">Rear(Right-In): &nbsp;<select name="tyreId" class="textbox" style="width:80px;">
                        <option value="">-select-</option>
                        <c:if test = "${tyresList != null}" >
                        <c:forEach items="${tyresList}" var="tr1">
                        <option  value='<c:out value="${tr1.tyreId}" />'>
                            <c:out value="${tr1.tyreNumber}" /></option>
                        </c:forEach >
                        </c:if>
                    </select>
                    <input type="hidden" name="positionId" value="3" />
                    &emsp;&emsp;Rear(Right-Out): &nbsp;<select name="tyreId" class="textbox" style="width:80px;">
                        <option value="">-select-</option>
                       <c:if test = "${tyresList != null}" >
                        <c:forEach items="${tyresList}" var="tr1">
                        <option  value='<c:out value="${tr1.tyreId}" />'>
                            <c:out value="${tr1.tyreNumber}" /></option>
                        </c:forEach >
                        </c:if>
                    </select>
                    <input type="hidden" name="positionId" value="5" /> </td>
                
            </tr>            
            <tr align="center">
                <td colspan="2" class="texttitle2">Stepny: <select name="tyreId" class="textbox" style="width:80px;">
                        <option value="">-select-</option>
                       <c:if test = "${tyresList != null}" >
                        <c:forEach items="${tyresList}" var="tr1">
                        <option  value='<c:out value="${tr1.tyreId}" />'>
                            <c:out value="${tr1.tyreNumber}" /></option>
                        </c:forEach >
                        </c:if>
                    </select><input type="hidden" name="positionId" value="7" />
                </td>
            </tr>
            <tr><td colspan="4" align="center">&nbsp;</td></tr>
            <tr><td colspan="4" align="center"><input type="submit" value="  Save  " class="button" /></td></tr>
        </table>
</form>
    </body>
</html>
