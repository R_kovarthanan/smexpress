<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/content/common/NewDesign/header.jsp" %>
    <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
   
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->

        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
  
    <script language="javascript">
        function submitPage() {
            if (document.getElementById('vehicleNo').value == '') {
                alert("please enter the vehicle No")
                $("#vehicleNo").focus();
            }
            else if (document.getElementById('contToDate').value == '') {
                alert("please enter the Date")
                $("#contToDate").focus();
            }
            else if (document.getElementById('odometer').value == '') {
                alert("please enter the Odometer")
                $("#odometer").focus();
            }
            else if (document.getElementById('lastodometer').value == '' || document.getElementById('lastodometer').value == 0) {
                alert("Please enter the Valid Last Odometer reading")
                $("#lastodometer").focus();
            }
            else if (document.getElementById('lastRunKM').value == '') {
                alert("please enter the Run KM")
                $("#lastRunKM").focus();
            }
            else if (document.getElementById('totalKM').value == '' || document.getElementById('totalKM').value <= 0) {
                alert("please enter the Valid Total KM")
                $("#totalKM").focus();
            }
            else if (document.getElementById('remarks').value == '') {
                alert("please enter the Remarks")
                $("#remarks").focus();
            }
            else {
                document.editcustomer.action = '/throttle/addCustomerVehicleLogger.do';
                document.editcustomer.submit();
            }
        }
    </script>

    <script>
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            // alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

        window.onload = function()
        {
            var currentDate = new Date();
            var day = currentDate.getDate();
            var month = currentDate.getMonth() + 1;
            var year = currentDate.getFullYear();
            var myDate = day + "-" + month + "-" + year;
            //document.getElementById("contFromDate").value = myDate;
            document.getElementById("contToDate").value = myDate;
        }


        $(document).ready(function() {
            $('#vehicleNo').autocomplete({
                source: function(request, response) {
                    var customerId = $("#customerId").val();
                    var vehicle = $("#vehicleNo").val();
                    $.ajax({
                        url: "/throttle/getCustomerVehicleDetails.do",
                        dataType: "json",
                        data: {customerId: customerId, vehicleNo: vehicle},
                        success: function(data, textStatus, jqXHR) {
                            if (data == '') {
                                alert('please enter valid Data');
                                $('#vehicleNo').val('');
                                $('#vehicleId').val('');
                                $('#lastodometer').val('');
                                $('#totalKM').val('');
                                $('#contFromDate').val('');
                                $('#vehicleNo').focus();
                            }
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {

                            //console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var vehicle = ui.item.vehicleNo;
                    var vehid = ui.item.vehicleId;
                    var customer = $("#customerId").val();                  
                    
                    $('#vehicleNo').val(vehicle);
                    $('#vehicleId').val(vehid);
                    setLogInfo(vehid, customer);
                    return false;
                }

            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.vehicleNo;
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };
        });

        function setLogInfo(vehicle, customer){
        
        $.ajax({
                url: "/throttle/getCustomerVehicleLogDetails.do",
                dataType: "json",
                data: {
                    vehicleId: vehicle, customerId: customer
                },
                success: function (data) {                    
                    
                    $('#lastodometer').val(data.odometer);
                    $('#tempodometer').val(data.odometer);
                    $('#totalKM').val(data.totalKM);
                    $('#temptotal').val(data.totalKM);
                    $('#contFromDate').val(data.contToDate);                                        
                    }                                                  
            });
        }

    function flushVal(){
        var tempodometer = parseInt(document.getElementById("tempodometer").value);        
        var curodometer = parseInt(document.getElementById("lastodometer").value);        
        if(tempodometer > curodometer){
        alert("Please enter the Valid odometer reading");
        document.getElementById("lastodometer").value = tempodometer;
        document.getElementById("odometer").value = "";
        document.getElementById("lastRunKM").value="";
        document.getElementById("totalKM").value=document.getElementById("temptotal").value;
       }
    }
    
        function calKM(val) {
            var lastodoval = parseInt(document.getElementById("lastodometer").value);
            var tempodometer = parseInt(document.getElementById("tempodometer").value);
            
            var curodoval = parseInt(val);

            if (lastodoval >= curodoval) {
                alert("Please enter the Valid Odometer reading");
                document.getElementById("odometer").value = "";
                document.getElementById("odometer").focus();
            }             
            else if(tempodometer>lastodoval){
              alert("Please enter the Valid Odometer reading");
                document.getElementById("odometer").value = "";
                document.getElementById("odometer").focus();  
            }
            else if (lastodoval<=0){
               alert("Please enter the Valid Last Odometer reading");
                document.getElementById("lastodometer").value = "";
                document.getElementById("odometer").value = "";
                document.getElementById("lastRunKM").value="";
                document.getElementById("totalKM").value=document.getElementById("temptotal").value;
                document.getElementById("lastodometer").focus();  
            }
            else {
                var odoval = parseInt(val) - parseInt(document.getElementById("lastodometer").value);
                document.getElementById("lastRunKM").value = odoval;
                document.getElementById("totalKM").value = parseInt(document.getElementById("temptotal").value) + odoval;
            }
        }

    </script>
<div class="pageheader">
      <h2><i class="fa fa-edit"></i> Fuel Logger </h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>
          <li><a href="general-forms.html">Leasing Operation</a></li>
          <li class=""> View TripSheet</li>
          <li class=""> Fuel Logger</li>

        </ol>
      </div>
      </div>
   <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">
    <body>
        <form name="editcustomer"  method="post" >
           
            <%@ include file="/content/common/message.jsp" %>
            <br>

            <table class="table table-bordered">
                <tr align="center">
                    <td colspan="4" align="center" class="contenthead" height="30"><div class="contenthead">Vehicle Fuel Logger</div></td>
                </tr>
                <tr class="text2">                      
                    <td  height="30"><font color="red">*</font>Customer Name</td>
                    <td  height="30">
                        <input name="customerId" id="customerId" type="hidden" class="textbox" value="<c:out value="${customerId}"/>">
                        <c:out value="${customerName}"/></td>
                    <td  height="30"><font color="red">*</font>Vehicle No</td>
                    <td  height="30">
                        <input name="vehicleNo" id="vehicleNo" type="hidden" class="textbox" value="<c:out value="${vehicleNo}"/>" /><c:out value="${vehicleNo}"/>
                        <input name="vehicleId" id="vehicleId" type="hidden" class="textbox" value="<c:out value="${vehicleId}"/>" >
                    </td>
                </tr>

                <tr class="text1">
                    <td height="30"><font color="red">*</font>Last Fuel Date</td>
                    <td height="30"><input class="textbox" name="contFromDate" id="contFromDate" value="" readonly></td>                    
                    <td height="30"><font color="red">*</font>Last FuelFill Odometer Reading</td>
                    <td height="30"><input class="textbox" name="lastodometer" id="lastodometer" value="0" onchange="flushVal();"></td> 
                </tr>
                
                <tr class="text2">
                    <td height="30"><font color="red">*</font>Fuel Filled So Far</td>
                    <td height="30"><input name="lastRunKM" id="lastRunKM" type="text" class="textbox"  value="" readonly /></td>
                    <td height="30"><font color="red">*</font>Total KM Run So For</td>
                    <td height="30"><input class="textbox" name="totalKM" id="totalKM" value="0" readonly></td>                    
                    <input type="hidden" name="temptotal" id="temptotal" value="0"/>
                    <input type="hidden" name="tempodometer" id="tempodometer" value="0"/>
                </tr>
                <tr class="text1">
                    <td height="30"><font color="red">*</font>KM Run(From Last Fill)</td>
                    <td height="30"><input name="lastRunKM" id="lastRunKM" type="text" class="textbox"  value="" readonly /></td>
                    <td  height="30"><font color="red">*</font>Fuel Filling Date</td>
                    <td  height="30"><input name="contToDate" id="contToDate" type="text" class="datepicker"  value=""></td>
                </tr>
                <tr class="text2">                    
                    <td height="30"><font color="red">*</font>Fuel(Litters)</td>
                    <td height="30"><input class="textbox" name="totalKM" id="totalKM" value="0" readonly></td>                    
                    <input type="hidden" name="temptotal" id="temptotal" value="0"/>
                    <input type="hidden" name="tempodometer" id="tempodometer" value="0"/>
                    <td  height="30"><font color="red">*</font>Odometer Reading(At Fuel Fill)</td>
                    <td  height="30"><input class="textbox" name="odometer" id="odometer" value="" onchange="calKM(this.value);"></td>
                </tr>
                <tr class="text1">
                    <td height="30"><font color="red">*</font>Location</td>
                    <td height="30"><input name="lastRunKM" id="lastRunKM" type="text" class="textbox"  value="" readonly /></td>
                    <td height="30"><font color="red">*</font>Filling Station Name</td>
                    <td height="30"><input class="textbox" name="totalKM" id="totalKM" value="0" readonly></td>                    
                    <input type="hidden" name="temptotal" id="temptotal" value="0"/>
                    <input type="hidden" name="tempodometer" id="tempodometer" value="0"/>
                </tr>
                <tr class="text2">
                    <td  height="30"><font color="red">*</font>Remarks</td>
                    <td  height="30"><textarea name="remarks" id="remarks"></textarea></td>
                    <td  height="30"></td>
                    <td  height="30"></td>                    
                </tr>
            </table>
            <br>
            <center>
                <input type="button" value="SAVE" class="button" onClick="submitPage();">
                &emsp;<input type="reset" class="button" value="Clear">
            </center>

            <br> <br>


            <c:if test = "${getCustomerDetails != null}" >
                 <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                        <tr height="30">
                            <th><h3>S.No</h3></th>
                            <th><h3>Customer Name</h3></th>
                            <th><h3>Vehicle No</h3></th>
                            <th><h3>Odometer</h3></th>
                            <th><h3>Last Run KM</h3></th>
                            <th><h3>Total KM</h3></th>
                            <th><h3>From Date</h3></th>
                            <th><h3>To Date</h3></th>
                        </tr>
                    </thead><tbody>
                        <% int index = 0;%>
                        <c:forEach items="${getCustomerDetails}" var="BL">
                            <%
                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                    classText = "text2";
                                } else {
                                    classText = "text1";
                                }
                            %>
                            <tr height="30">
                                <td   align="left"> <%= index + 1 %> </td>
                                <td  align="left"> <c:out value="${BL.customerName}" /></td>
                                <td   align="left"> <c:out value="${BL.vehicleNo}"/> </td>
                                <td   align="left"> <c:out value="${BL.odometer}"/> </td>
                                <td   align="left"> <c:out value="${BL.lastRunKM}"/> </td>
                                <td   align="left"> <c:out value="${BL.totalKM}"/> </td>
                                <td   align="left"> <c:out value="${BL.contFromDate}"/> </td>
                                <td   align="left"> <c:out value="${BL.contToDate}"/> </td>
                            </tr>
                            <% index++;%>
                        </c:forEach>
                    </c:if>
                </tbody>
            </table>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
                $('#contFromDate').datepicker({ dateFormat: 'dd-mm-yy' }).val();
                $('#contToDate').datepicker({ dateFormat: 'dd-mm-yy' }).val();
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
