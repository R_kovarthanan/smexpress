<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>  
        <style> table, td {border:1px solid black} table {border-collapse:collapse}</style>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
              String menuPath = "Customer >> Customer list";
              request.setAttribute("menuPath", menuPath);
//              String dateval = request.getParameter("dateval");
//              String active = request.getParameter("active");
//              String type = request.getParameter("type");
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                       Date dNow = new Date();
                       SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                       //System.out.println("Current Date: " + ft.format(dNow));
                       String curDate = ft.format(dNow);
                       String expFile = "CustomerList-" + curDate + ".xls";

                       String fileName = "attachment;filename=" + expFile;
                       response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                       response.setHeader("Content-disposition", fileName);
            %>



            <br>
            <br>
            <br>

            <br><br>

            <br>
            <br>
            <c:if test="${customerExportExcelList != null}">
                <table align="center" border="0" id="table" class="sortable" style="width:1700px;" >
                    <thead>
                        <tr height="50">
                            <th>SNo</th>
                            <th>Customer Code</th>
                            <th>Customer Name</th>
                            <th>Location</th>
                            <th>Enrolled Date</th>
                            <th>Status</th>
                            <th>VehicleType</th>
                            <th>Origin</th>
                            <th>Touch Point1</th>
                            <th>Touch Point2</th>
                            <th>Touch Point3</th>
                            <th>Touch Point4</th>
                            <th>Destination</th>
                            <th>Agreed Fuel Price</th>
                            <th>Rate</th>
                            <th>TAT</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                        <c:forEach items="${customerExportExcelList}" var="cus">
                            <%
                                        String className = "text1";
                                        if ((index % 2) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>
                            <tr >
                                <td class="form-control"  >
                                    <%=index%>
                                </td>
                                <td class="form-control" align="left" >
                                    <c:out value="${cus.customerCode}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${cus.custName}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${cus.custCity}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${cus.enrollDate}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:if test="${(cus.custStatus=='n') || (cus.custStatus=='N')}" >
                                        InActive
                                    </c:if>
                                    <c:if test="${(cus.custStatus=='y') || (cus.custStatus=='Y')}" >
                                        Active
                                    </c:if>
                                </td>
                                <td class="form-control" align="left" >
                                    <c:out value="${cus.vehicleTypeName}"/>
                                </td>
                                <td align="left"   ><c:out value="${cus.firstPickupName}"/></td>
                                <td align="left"   ><c:out value="${cus.point1Name}"/></td>
                                <td align="left"   ><c:out value="${cus.point2Name}"/></td>
                                <td align="left"   ><c:out value="${cus.point3Name}"/></td>
                                <td align="left"   ><c:out value="${cus.point4Name}"/></td>
                                <td align="left"   ><c:out value="${cus.finalPointName}"/></td>
                                <td align="left"   >
                                    <c:out value="${cus.rateWithReefer}"/>
                                </td>
                                <td align="left" >
                                    <c:out value="${cus.rateWithoutReefer}"/>
                                </td>
                                <td align="left" >
                                    <c:out value="${cus.tatTime}"/>
                                </td>
                                <td>
                                    <c:if test="${cus.activeInd == 'Y'}" >
                                        Active
                                    </c:if>
                                    <c:if test="${cus.activeInd == 'N'}" >
                                        In-Active
                                    </c:if>
                                </td>

                            </tr>

                            <%index++;%>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>