<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });


    function setValuesConsignee(sno, consigneeId, customerId, consigneeName, contactPerson, phoneNo, address1, address2, email, cityId, stateId, stateName, remarks, activeInd, pinCode, customerName, gstNo, eFSCode) {

        var count = parseInt(document.getElementById("count").value);
        for (var i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById('consigneeId').value = consigneeId;
        document.getElementById('customerId').value = customerId;
        document.getElementById('custName').value = customerName;
        document.getElementById('consigneeName').value = consigneeName;
        document.getElementById('contactPerson').value = contactPerson;
        document.getElementById('phoneNo').value = phoneNo;
        document.getElementById('address1').value = address1;
        document.getElementById('address2').value = address2;
        document.getElementById('email').value = email;
        document.getElementById('cityId').value = cityId;
        document.getElementById('stateId').value = stateId;
        document.getElementById('remarks').value = remarks;
        document.getElementById('activeInd').value = activeInd;
        document.getElementById('pinCode').value = pinCode;
        document.getElementById('gstNo').value = gstNo;
        document.getElementById('consigneeCode').value = eFSCode;

    }

    function submitPageConsignee()
    {
        //        alert("im in test");
        var custId = document.getElementById("customerId").value;
        var custName = document.getElementById("custName").value;
        var gstNoSize = $("#gstNo").val().length;
        var errStr = "";
        if (document.getElementById("custName").value == "") {
            errStr = "Please enter Customer Name.\n";
            alert(errStr);
            document.getElementById("custName").focus();
        }
        else if (document.getElementById("consigneeCode").value == "") {
            errStr = "Please enter eFS Code.\n";
            alert(errStr);
            document.getElementById("consigneeCode").focus();
        }
        else if (document.getElementById("consigneeName").value == "") {
            errStr = "Please select valid Consignee Name.\n";
            alert(errStr);
            document.getElementById("consigneeName").focus();
        } else if (document.getElementById("contactPerson").value == "") {
            errStr = "Please select valid Contact Person.\n";
            alert(errStr);
            document.getElementById("contactPerson").focus();
        } else if (document.getElementById("phoneNo").value == "") {
            errStr = "Please enter Contact No.\n";
            alert(errStr);
            document.getElementById("phoneNo").focus();
        } else if (document.getElementById("address1").value == "") {
            errStr = "Please enter Address.\n";
            alert(errStr);
            document.getElementById("address1").focus();

        } else if (document.getElementById("cityId").value == 0) {
            errStr = "Please select valid City.\n";
            alert(errStr);
            document.getElementById("cityId").focus();
        } else if (document.getElementById("stateId").value == 0) {
            errStr = "Please select valid State.\n";
            alert(errStr);
            document.getElementById("stateId").focus();
        } else if (document.getElementById("pinCode").value == "") {
            errStr = "Please enter PinCode.\n";
            alert(errStr);
            document.getElementById("pinCode").focus();
        }
//        else if (document.getElementById("gstNo").value == "") {
//            errStr = "Please enter GST No.\n";
//            alert(errStr);
//            document.getElementById("gstNo").focus();
//        }
//       else if (gstNoSize < 15) {
//            errStr = "Please enter Valid GST No.\n";
//            alert(errStr);
//            document.getElementById("gstNo").focus();
//        }

        if (errStr == "") {
            document.consignee.action = "/throttle/handleConsigneeInsert.do?custId=" + custId + "&custName=" + custName;
            document.consignee.method = "post";
            document.consignee.submit();
        }
    }

</script>

<script type="text/javascript">

    function consigneeNameAllowAlphabet() {
        var field = document.consignee.consigneeName.value;
        if (!field.match(/^[a-zA-Z-\s]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignee.consigneeName.value = "";
            document.consignee.consigneeName.focus();
        }
    }
    function contactPersonAllowAlphabet() {
        var field = document.consignee.contactPerson.value;
        if (!field.match(/^[a-zA-Z-\s]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignee.contactPerson.value = "";
            document.consignee.contactPerson.focus();
        }
    }
    function phoneNoAllowAlphabet() {
        var field = document.consignee.phoneNo.value;
        if (!field.match(/^[0-9]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignee.phoneNo.value = "";
            document.consignee.phoneNo.focus();
        }
    }
    function consigneeNameAltAllowAlphabet() {
        var field = document.consignee.consigneeNameAlt.value;
        if (!field.match(/^[a-zA-Z. -\s]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignee.consigneeNameAlt.value = "";
            document.consignee.consigneeNameAlt.focus();
        }
    }
    function contactPersonAltAllowAlphabet() {
        var field = document.consignee.contactPersonAlt.value;
        if (!field.match(/^[a-zA-Z. -\s]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignee.contactPersonAlt.value = "";
            document.consignee.contactPersonAlt.focus();
        }
    }
    function phoneNoAltAllowAlphabet() {
        var field = document.consignee.phoneNoAlt.value;
        if (!field.match(/^[0-9]+$/) && field != "")
        {
            alert("Please Enter numbers in text");
            document.consignee.phoneNoAlt.value = "";
            document.consignee.phoneNoAlt.focus();
        }
    }



    var httpRequest;
    function getStateName(cityId) {
        var url = '/throttle/handleStateName.do?cityId=' + cityId;
        if (window.ActiveXObject)
        {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest)
        {
            httpRequest = new XMLHttpRequest();
        }

        httpRequest.open("GET", url, true);

        httpRequest.onreadystatechange = function() {
            processRequest();
        };

        httpRequest.send(null);
    }

    function processRequest() {
        if (httpRequest.readyState == 4)
        {
            if (httpRequest.status == 200)
            {
                if (httpRequest.responseText.valueOf() != "") {
                    var val = httpRequest.responseText.valueOf();
                    var state = val.split('~');
                    //                            alert(state[0]);
                    //                            alert(state[1]);

                    //                    document.getElementById("stateId").value = state[0];
                    //                    document.getElementById("stateName").value = state[1];
                } else
                {
                    alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                }
            }
        }
    }

    function checkConsigneeName(value) {
        var customerId = document.getElementById('custId').value;
        var url = '/throttle/checkConsigneeName.do';
        $.ajax({
            url: url,
            data: {customerId: customerId, consignName: value},
            type: "POST",
            success: function(data)
            {
                if (data > 0) {
                    //                                alert("already Exits");
                    var consigneeName = $('#consigneeName').val();
                    $("#StatusNew").text("ConsingeeName " + consigneeName + " Already Exits");
                    $("#StatusNew").css('color', 'red');
                    $('#consigneeName').val('');
                    $('#consigneeName').focus();
                } else {
                    $("#StatusNew").text("");
                }
                //                            $('#contractSet').html(data);
            }
        });
    }


</script>
<body onload="document.consignee.consigneeName.focus();">
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Consignee" text="Consignee"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Master" text="Master"/></a></li>
                <li class=""><spring:message code="hrms.label.Consignee" text="Consignee"/></li>

            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">


                <form name="consignee"  method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    <%@ include file="/content/common/message.jsp" %>


                    <table  border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" id="bg">
                        <table class="table table-info mb30 table-hover" id="bg" >
                            <thead><tr>
                                    <th colspan="5" > Consignee Details</th>
                                </tr>
                            </thead>

                            <tr>
                            <input type="hidden" name="custIds" id="custIds" class="form-control"  value="<c:out value="${custId}"/>"/>
                            <td  height="30"> <font color="red"> *</font>Customer Name</td>
                            <td  height="30">
                                <input type="hidden" name="customerId" id="customerId" class="form-control" value="<c:out value="${custId}"/>" />
                                <input type="text" readonly name="custName" id="custName"  class="form-control"  style="width:250px;height:40px"  value="<c:out value="${custName}" />"  ></td>

                            <td><font color="red"> *</font>eFS Code</td>                            
                            <td><input type="text" class="form-control" style="width:240px;height:40px" name="consigneeCode" id="consigneeCode" maxlength="25" value="" /></td>
                            </tr>

                            <tr >
                                <td ><font color="red"> *</font>Consignee Name</td>
                                <td ><input type="hidden" name="consigneeId" id="consigneeId" value=""/>
                                    <input type="text"  class="form-control" style="width:240px;height:40px"   name="consigneeName" id="consigneeName"  value="" maxlength="200"  onchange="checkConsigneeName(this.value)" /></td>

                                <td ><font color="red"> *</font>Contact Person </td>
                                <td ><input type="text"  class="form-control" style="width:240px;height:40px"   name="contactPerson" id="contactPerson" maxlength="25" value="" onKeyPress="return onKeyPressBlockNumbers(event);" /></td>
                            </tr>
                            <tr >
                                <td ><font color="red"> *</font>Contact No</td>
                                <td ><input type="text"  class="form-control" style="width:240px;height:40px"   name="phoneNo" id="phoneNo" maxlength="10" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                <td >&nbsp;Email</td>
                                <td ><input type="text"  class="form-control" style="width:240px;height:40px"   name="email" id="email" value="" maxlength="50" /></td>
                            </tr>
                            <tr >
                                <td ><font color="red"> *</font>Address 1</td>
                                <td ><textarea cols="15" rows="1" name="address1" id="address1" style="width:240px;height:40px"  > </textarea></td>
                                <td >&nbsp;Address 2</td>
                                <td ><textarea cols="15" rows="1" name="address2" id="address2" style="width:240px;height:40px"  > </textarea></td>
                            </tr>
                            <tr >
                                <td><font color="red"> *</font>City</td>
                                <td ><input type="text"  class="form-control" style="width:240px;height:40px"   name="cityId" id="cityId" value=""/>
                                    <!--                                    <select name="cityId"  class="form-control" style="width:240px;height:40px"   id="cityId" onchange="getStateName(this.value)">
                                                                            <option value="0">-select-</option>
                                    <c:if test = "${CityList!= null}" >
                                        <c:forEach items="${CityList}" var="cList">
                                            <option  value="<c:out value='${cList.cityId}'/>"><c:out value="${cList.cityName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>-->
                                </td>
                                <td ><font color="red"> *</font>State</td>
                                <td ><select name="stateId" class="form-control" style="width:240px;height:40px" id="stateId" onchange="getStateName(this.value)">
                                        <option value="0">-select-</option>
                                        <c:if test = "${stateList!= null}" >
                                            <c:forEach items="${stateList}" var="sList">
                                                <option  value="<c:out value='${sList.stateId}'/>"><c:out value="${sList.stateName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                    <!--                                    <input type="hidden"  class="form-control" style="width:240px;height:40px"   name="stateId" id="stateId" value=""/>
                                                                        <input type="text"  class="form-control" style="width:240px;height:40px"   name="stateName" id="stateName" value="" readonly/></td>-->
                            </tr>
                            <tr >
                                <td ><font color="red"> *</font>PinCode</td>
                                <td ><input type="text"  class="form-control" style="width:240px;height:40px"   name="pinCode" id="pinCode" value="" maxlength="6" onkeypress="return onKeyPressBlockCharacters(event);" /></td>
                                <td >Status</td>
                                <td >
                                    <select  class="form-control" style="width:240px;height:40px"   name="activeInd" id="activeInd">
                                        <option value="0">Active</option>
                                        <option value="1">In-Active</option>
                                    </select>
                            </tr>

                            <tr >                            
                                <td>GST No</td>
                                <td><input type="text"  class="form-control" style="width:240px;height:40px;text-transform:uppercase"  id="gstNo" name="gstNo" maxlength="15" id="gstNo" value="<c:out value='${gstNo}'/>" /></td>

                                <td >Remarks</td>
                                <td ><textarea cols="15" rows="1" name="remarks" id="remarks" style="width:240px;height:40px"  > </textarea> </td>
                            </tr>
                            <br>
                            <br>
                            <tr >
                                <td colspan="4" ><center><input type="button" name="Save" value="Save" class="btn btn-success" onclick="submitPageConsignee()"/></center></td>
                            </tr>

                        </table>
                        <br>
                        <br>
                        <table class="table table-info mb30 table-hover" id="table" >	
                            <!--<table align="center" border="0" id="tableNew" class="sortable" style="width:650px">-->
                            <thead>
                                <tr height="30">
                                    <th >Customer Name</th>
                                    <th>Consignee Name</th>
                                    <th >Customer Site Location</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>View</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int sno = 0;%>
                                <% int sno1 = 1;%>
                                <c:if test = "${ConsigneeList != null}">
                                    <c:forEach items="${ConsigneeList}" var="consignee">
                                        <%
                                                    sno++;
                                                    String className = "text1";
                                                    if ((sno % 1) == 0) {
                                                        className = "text1";
                                                    } else {
                                                        className = "text2";
                                                    }
                                        %>

                                        <tr>
                                            <td class="<%=className%>"  align="left"> <c:out value="${consignee.customerName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${consignee.consigneeName}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${consignee.customerSiteLocation}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${consignee.cityId}" /></td>
                                            <td class="<%=className%>"  align="left"> <c:out value="${consignee.stateName}" /></td>
                                            <td  >
                                                <a href="handleViewConsignee.do?customerId=<c:out value="${consignee.customerId}"/>">View</a>
                                            </td>
                                            <c:set var="customerId" value="${consignee.customerId}" scope="page" />
                                            <td class="<%=className%>" align="center"> <input type="checkbox" align="center" id="edit<%=sno%>" onclick="setValuesConsignee('<%= sno%>', '<c:out value="${consignee.consigneeId}"/>', '<c:out value="${consignee.customerId}"/>', '<c:out value="${consignee.consigneeName}"/>', '<c:out value="${consignee.contactPerson}"/>', '<c:out value="${consignee.phoneNo}"/>', '<c:out value="${consignee.address1}"/>', '<c:out value="${consignee.address2}"/>', '<c:out value="${consignee.email}"/>', '<c:out value="${consignee.cityId}"/>', '<c:out value="${consignee.stateId}"/>', '<c:out value="${consignee.stateName}"/>', '<c:out value="${consignee.remarks}"/>', '<c:out value="${consignee.activeInd}"/>', '<c:out value="${consignee.pinCode}"/>', '<c:out value="${consignee.customerName}"/>', '<c:out value="${consignee.gstNo}"/>', '<c:out value="${consignee.consigneeCode}"/>');" /></td>
                                        </tr>
                                        <%sno1++;%>
                                    </c:forEach>
                                </tbody>
                            </c:if>
                        </table>

                        <input type="hidden" name="count" id="count" value="<%=sno%>" />
                        <br>
                        <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 1);
                        </script>
                </form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="../common/NewDesign/settings.jsp" %>