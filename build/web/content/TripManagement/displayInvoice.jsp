<%-- 
    Document   : displayInvoice
    Created on : Dec 23, 2012, 4:41:24 PM
    Author     : Entitle
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />

        <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
        <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %>
        <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>


        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>

        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <title>INVOICE</title>
    </head>

    <script>



        function print()
        {
            var DocumentContainer = document.getElementById("printDiv");
            var WindowObject = window.open('', "TrackHistoryData",
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }

        function back()
        {
            window.history.back()
        }

    </script>


    <body>
        <form name="suggestedPS" method="post">
            <div id="printDiv">
                <br>
                <br>
                <br>

                <table align="center" cellspacing="0" cellpadding="0" width="950" border="5" style="border:0px; border-color:#E8E8E8; border-style:solid;">
                    <tr>
                        <td>
                            <table align="center" cellspacing="0" cellpadding="0" width="950" border="2" style="border:0px; border-color:#E8E8E8; border-style:solid;">
                                <tr colspan="2">
<!--                                    <td height="27" colspan="1" align="center"><font size="4"><b> CHETTINAD LOGISTICS PRAIVATE LIMITED - KARIKKALI</b></font></td>-->
                                    <td align="center" colspan="1"  width="35%" scope="col"  style=" border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid;" ><font size="4"><b> CHETTINAD LOGISTICS PRAIVATE LIMITED - KARIKKALI</b></font></td>
                                </tr>
                                <tr><td align="left" colspan="1"  width="35%" scope="col"  style=" border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid;" >&nbsp;</td></tr>
                                <tr><td align="left" colspan="1"  width="35%" scope="col"  style=" border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid;" >&nbsp;</td></tr>
                                <tr><td align="left" colspan="1"  width="35%" scope="col"  style=" border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid;" >&nbsp;</td></tr>
                                <tr><td align="left" colspan="1"  width="35%" scope="col"  style=" border:0px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid;" >&nbsp;</td></tr>
                            </table>
                            <table align="center" cellspacing="0" cellpadding="0" width="950" border="0" style="border:0px; border-color:#E8E8E8; border-style:solid;">
                                <c:if test="${invoiceDetailList != null}">

                                    <c:forEach items="${invoiceDetailList}" var="idl">
                                        <c:set var="invoiceCode" value="${idl.invoiceCode}" />
                                        <c:set var="invRefCode" value="${idl.invRefCode}" />
                                        <c:set var="invoiceDate" value="${idl.invoiceDate}" />
                                        <c:set var="grandTotal" value="${idl.grandTotal}" />
                                        <c:set var="invoiceStatus" value="${idl.invoiceStatus}" />
                                        <c:set var="invoiceStatus" value="${idl.invoiceStatus}" />
                                        <c:set var="numberOfTri" value="${idl.numberOfTri}" />
                                        <c:set var="fromDate" value="${idl.fromDate}" />
                                        <c:set var="toDate" value="${idl.toDate}" />
                                        <c:set var="toDate" value="${idl.toDate}" />
                                        <c:set var="remarks" value="${idl.remarks}" />
                                    </c:forEach>
                                </c:if>
                                <tr  valign="top">
                                    <td align="left" colspan="1"  width="50%" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                                        <font size="3"><b>To</b></font><br>
                                        <font size="3"><b>&emsp;CHETTINAD CEMENT CORPORATION</b></font><br>
                                        <font size="3"><b>&emsp;KARIKKALI - 624703,</b></font><br>
                                        <font size="3"><b>&emsp;DINDIGUL DIST.</b></font><br>
                                    </td>
                                    <td align="left" colspan="7" width="148" scope="col" valign="bottom"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   " >
                                        <font size="3"><b>&emsp;&emsp;&emsp;&emsp;&emsp; Invoice NO     &emsp;&emsp;&emsp;: <c:out value="${invoiceCode}" /></b></font><br>
                                        <%java.text.DateFormat df = new java.text.SimpleDateFormat("dd-MM-yyyy");%>
                                        <font size="3"><b>&emsp;&emsp;&emsp;&emsp;&emsp; Invoice Date &emsp;&emsp;&nbsp;  : <%= df.format(new java.util.Date())%></b></font><br>
<!--                                        <font size="3"><b>&nbsp; No.Of Trip &emsp;&emsp;&emsp;&nbsp;  : <c:out value="${numberOfTri}" /></b></font><br>-->
                                    </td>
                                </tr>
                                <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                                <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>

                                <tr>
                                    <td colspan="7" style=" border:1px; border-top-color:#CCCCCC; border-top-style:solid;   " >
                                        <table align="center" cellspacing="0" cellpadding="0" width="950" border="0" style="border:1px; border-color:#E8E8E8; border-style:solid;">
                                            <%int index = 0;%>
                                          <%--  <c:if test="${invoiceTripDetailList != null}">  --%>
                                                <tr style="background:#CCCCCC; color:#000000;">
                                                    <td><b>S.No</b></td>
                                                    <td><b>Trip No</b></td>
                                                    <td><b>Trip Date</b></td>
                                                    <td><b>Customer Name</b></td>
                                                    <td><b>Destination</b></td>
                                                    <td><b>Vehicle</b></td>
                                                    <td><b>Tonnage</b></td>
                                                    <td><b>Freight</b></td>
                                                </tr>
                                                <c:forEach items="${invoiceTripDetailList}" var="itdl">

                                                    <c:set var="totalAmt" value="${totalAmt + itdl.totalamount}"/>

                                                    <%
                                                                String classText1 = "";
                                                                int oddEven1 = index % 2;
                                                                if (oddEven1 > 0) {
                                                                    classText1 = "text2";
                                                                } else {
                                                                    classText1 = "text1";
                                                                }
                                                    %>
                                                    <tr>
                                                        <td align="left"  width="148" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   "><%=index + 1%></td>
                                                        <td align="left"  width="148" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   "><c:out value="${itdl.tripNo}"/></td>
                                                        <td align="left"  width="148" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   "><c:out value="${itdl.tripDate}"/></td>
                                                        <td align="left"  width="148" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   "><c:out value="${itdl.customerName}"/></td>
                                                        <td align="left"  width="148" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   "><c:out value="${itdl.routeName}"/></td>
                                                        <td align="left"  width="148" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   "><c:out value="${itdl.regno}"/></td>
                                                        <td align="left"  width="148" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   "><c:out value="${itdl.totalTonnage}"/></td>
                                                        <td align="left"  width="148" scope="col"  style=" border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;   "><c:out value="${itdl.totalamount}"/></td>
                                                    </tr>
                                                    <%index++;%>
                                                </c:forEach>
                                            <%--</c:if> --%>
                                            <tr style="height: 40px"><td colspan="4">&nbsp;</td></tr>
                                            <tr>
                                                <td align=right" colspan="3">
                                                    <font size="3"><b>TOTAL</b></font>
                                                </td>
                                                <td class="text2" align="right">
                                                    <b> <c:out value="${totalAmt}" /></b>
                                                </td>

                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
            </div>

            <table align="center" width="725" border="0" cellspacing="0" cellpadding="0"  style="border:0px; border-color:#E8E8E8; border-style:solid;" >
               
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr align="left">
                    <td>
                        <b>  &emsp;&emsp;&emsp;&emsp;Prepared by&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Checked by</b>
                  </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr align="center">
                    <td >
                        <input align="center" type="button" onclick="print();" value = " Print "   />
                    </td>
                </tr>

            </table>




        </form>
    </body>
</html>
