

<%--
    Document   : jobCardPaymentDetails
    Created on : Oct 26, 2016, 4:57:05 PM
    Author     : hp
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">

        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.*" %>
    </head>
    <body>
      
        <form id="paymentDetails" action="post" method="multipart">
            
            <div><ul id="StatusMsg" align="center"></ul></div>
            
                                <%int img = 0;%>
                            <table class="table table-info"   style="width: 700px;" align="center" id="fileAddRow">
                                <thead>
                                <th><spring:message code="trucks.label.SNo"  text="default text"/>SNo</th>
                                <th>&emsp;&emsp;&emsp;<spring:message code="trucks.label.File"  text="default text" />File</th>
                                <th>&emsp;&emsp;&emsp;&emsp;<spring:message code="trucks.label.FileName"  text="default text"/>FileName</th>
                                <th>&nbsp;<spring:message code="trucks.label.Remarks"  text="default text"/>Remarks</th>
                                </thead>
                        <c:if test="${customerUploadsDetails != null}">
                                    <c:forEach items="${customerUploadsDetails}" var="cust">
                                    <tr>
                                        <td>&nbsp;<%=img+1%></td>
                                        <td style="width: 100px;">
                                            <a href="#" onclick="showPdf(<c:out value ="${cust.uploadId}"/>);">
                                             <img src="/throttle/displayCustomerLogoBlobData.do?uploadId=<c:out value ="${cust.uploadId}"/>"  style="width: 90px;height: 40px" data-toggle="modal" data-target="#myModal<%=img%>" title="<c:out value ="${cust.remarks}"/>"/>
                                            </a>
                                           
                                        </td>
                                        <td>&emsp;&emsp;<c:out value ="${cust.fileName}"/></td>
                                        <td> <c:out value ="${cust.remarks}"/></td>
                                    </tr>
                                    <input  type="hidden" id="uploadId" name="uploadId" value="<c:out value ="${cust.uploadId}"/>"/>
                                        <%img++;%>
                                    </c:forEach>
                            </table>
                                
                            </c:if>
                            
                                <input type="hidden" id="invoiceId" name="invoiceId" value="<c:out value="${invoiceId}"/>"/>

        </form>
        
           <table align="center">

                <tr >
                     <td>&nbsp;</td>
<!--                    <input  type="hidden" id="custId" name="custId" value="<c:out value ="${custId}"/>"/>-->
                    <td align="right"><input type="button" id="AddRow" value="AddRow" class="btn btn-success" onClick="addRow();" style="width:100px;height:35px;"></td>
                    <td align="left"> &emsp;&emsp;<input type="button" id="DeleteRow1" value="DeleteRow"  text="default text" class="btn btn-success" onClick="DeleteRow();" style="width:100px;height:35px;">
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                 </tr>
                
              
            </table>
                    
          <center>
                    <div><input type="button" id="uploadBtn" value="UploadFiles" class="btn btn-success"  style="width:100px;height:35px;">
                        </div>
                </center>
         <script>

//                       $(document).ready(function ()
//                       {
                           $("#uploadBtn").click(function ()
                           {

//                             function saveUploadFiles(){
//                                 alert("jhscjdh")
//                                   document.paymentDetails.action='/throttle/uploadCustomerDocuments.do';
//                                document.paymentDetails.submit();
//                            }
//                                alert("jhscjdh")
                               var uploadRemarks = "";
                               var count = 0;
                                
                               $('input[name="file"]').each(function (index, value)
                               {
                               
                                   var file = value.files[0];
                                   if (file)
                                   {
                                       document.getElementById("invoiceId").value ='<c:out value="${invoiceId}"/>';
                                       var invoiceId = document.getElementById("invoiceId").value;
                                     
                                      
                                    
                                       uploadRemarks = $("#narration" + count).val();
                                   
                                       var formData = new FormData();
                                       formData.append('file', file);
                                       $.ajax({
                                           url: './uploadCustomerDocuments.do?narrationVal=' + uploadRemarks + '&invoiceId=' + invoiceId,
                                           type: 'POST',
                                           data: formData,
                                           cache: false,
                                           contentType: false,
                                           processData: false,
                                           success: function (data, textStatus, jqXHR) {
                                               var message = jqXHR.responseText;
////                                               alert(message);
////                                                $("#messages").append("<li>" + message + "</li>");
                                               $("#StatusMsg").text("Files Uploaded sucessfully ").css("color", "green");
                                               alert("Files Uploaded sucessfully");
                                           },
                                           error: function (jqXHR, textStatus, errorThrown) {
//                                                $("#messages").append("<li style='color: red;'>" + textStatus + "</li>");
                                           }
                                       });
                                   }
                                   count++;
                               });
//                           }
                           });
//                       });
                       </script>  
                        
            
                 
             <script>
                 
                    var rowCount = 1;
                    var sno = 0;
                    var style = "text2";
                    var align = "center";
                    function addRow()
                    {
                        //var sno=document.getElementById("serialNo").value;
                        // alert("swdbgf");
                        if (rowCount % 2 == 0) {
                            style = "text2";
                        } else {
                            style = "text1";
                        }

                        // var serialNo=parseInt(sno)+1;
                        var tab = document.getElementById("fileAddRow");
                        var rowCount = tab.rows.length;
                        snumber = parseInt(rowCount);
                        var newrow = tab.insertRow(rowCount);

                        var cell = newrow.insertCell(0);
                        var cell1 = "<td  height='25' >" + snumber + "</td>";
                        cell.setAttribute("align", align);
                        cell.innerHTML = cell1;

                        cell = newrow.insertCell(1);
                        var cell2 = "<td  height='30'><input type='file' name='file' size='110'   multiple  /></td>";
                        cell.setAttribute("align", align);
                        cell.innerHTML = cell2;
                        cell = newrow.insertCell(2);
                        var cell3 = "<td  height='30'><textarea name='narration' class='form-control'  id='narration" + sno + "'  style='width:300px;'/></textarea></td>";
                        cell.setAttribute("align", align);
                        cell.innerHTML = cell3;
                        cell = newrow.insertCell(3);
                        var cell4 = "<td align='left' ><input type='checkbox' class='checkbox form-control' id='selectedIndex" + sno + "' name='selectedIndex' value='" + sno + "'style='width:15px;'/></td>";
                        cell.setAttribute("align", align);
                        cell.innerHTML = cell4;

                        var temp = sno - 1;

                        rowCount++;
                        sno++;

                    }
                    function DeleteRow() {
                        document.getElementById('AddRow').style.display = 'block';
                        try {
                            var table = document.getElementById("fileAddRow");
                            rowCount = table.rows.length;
                            // alert(rowCount);
                            for (var i = 1; i < rowCount; i++) {
                                var row = table.rows[i];
                                var checkbox = row.cells[3].childNodes[0];
                                if (null != checkbox && true == checkbox.checked) {
                                    if (rowCount <= 1) {
                                        alert("Cannot delete all the rows");
                                        break;
                                    }
                                    table.deleteRow(i);
                                    rowCount--;
                                    i--;
                                    sno--;
                                    // snumber--;
                                }
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                      </script>
                      
                      
                      <script>
                           function showPdf(val) {
                window.open('/throttle/displayCustomerLogoBlobData.do?uploadId=' + val, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
       }
                      </script>
                     
                                
                            
          
    </body>
</html>