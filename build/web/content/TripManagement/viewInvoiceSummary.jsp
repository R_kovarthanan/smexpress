<%-- 
    Document   : viewInvoiceSummary
    Created on : Jan 8, 2013, 4:24:12 AM
    Author     : ASHOK
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->

        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <%@ page import="ets.domain.security.business.SecurityTO" %>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script src="/throttle/js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script src="/throttle/js/TableSort.js" language="javascript" type="text/javascript"></script>
        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />

        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true
                });
            });
            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });
        </script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    </head>
    <script type="text/javascript">

        function submitPage(value)
        {
            if(document.billG.customer.value==0){
                alert("Customer should not be Empty");
                document.billG.bunkId.focus();
                return;
            }

            if(textValidation(document.billG.fromDate,'From Date')){
                return;
            }else if(textValidation(document.billG.toDate,'To Date')){
                return;
            }
            document.billG.action="/throttle/displyInvoiceSummary.do";
            document.billG.submit();
        }

    </script>
    <body>
        <form name="billG" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <table cellpadding="0" cellspacing="2" align="center" border="0" width="700" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <tr>
                    <td><b>Search Invoice Summary</b></td>
                    <td align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;">Close</span>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding:15px;" align="left">
                        <div class="tabs" align="center" style="width:900px">
                            <table width="700" cellpadding="0" cellspacing="1" border="0" align="left" class="table4">
                                <tr>
                                    <td height="30"><font color="red">*</font> <b>Customer</b></td>
                                    <td height="30">
                                        <select  style="width:125px;" name="customer" id="customer">
                                            <option selected   value=0>-select-</option>
                                            <c:if test = "${customerList != null}" >
                                                <c:forEach items="${customerList}" var="cust">
                                                    <option  value='<c:out value="${cust.customerId}" />'><c:out value="${cust.customerName}" /> </option>
                                                </c:forEach >
                                            </c:if>
                                        </select>  </td>
                                </tr>
                                <tr>
                                    <td  height="30"><font color="red">*</font>From Date</td>
                                    <td  height="30"><input type="text" name="fromDate" class="datepicker" ></td>
                                    <td ><font color="red">*</font>To Date</td>
                                    <td ><input type="text" name="toDate" class="datepicker" ></td>
                                    <td  height="30" colspan="2">
                                        <center> <input type="button"  value="Fetch Data"  class="button" name="Fetch Data" onClick="submitPage(this.value)"></center>
                                    </td>
                                </tr>
                                <tr>
                                    <c:if test="${lastInvoiceDetail != null}">
                                        <c:forEach items="${lastInvoiceDetail}" var="lid">
                                            <td colspan="2">
                                                <b>Last Invoice Date : <c:out value="${lid.invoiceDate}"/> </b><br>
                                            </td>
                                            <td colspan="2">
                                                <b>From : <c:out value="${lid.fromDate}"/> To : <c:out value="${lid.toDate}"/>  </b><br>
                                            </td>

                                        </c:forEach>
                                    </c:if>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                
                       <div class="tabs" align="center" style="width:900px">
                            <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" id="lpsOrderList" class="border">
                                <%
                                DecimalFormat df = new DecimalFormat("0.00##");
                                int index = 0;
                                String invoiceCode = "",invoiceDate = "",invoiceFor = "",grandTotal = "",numberOfTri = "";
                                String classText2 = "";
                                double totalGrand = 0,totalTrip = 0;
                                ArrayList invoiceHeaderList = (ArrayList) request.getAttribute("invoiceHeaderList");
                                Iterator invHeader = invoiceHeaderList.iterator();
                                OperationTO optTO = null;
                                if (invoiceHeaderList.size() != 0) {
    %>
                                    <tr>
                                        <td class="contenthead">S.No</td>
                                        <td class="contenthead">Invoice No</td>
                                        <td class="contenthead">Invoice Date</td>
                                        <td class="contenthead">Customer</td>
                                        <td class="contenthead">Grand Total</td>
                                        <td class="contenthead">Number Of Trip</td>
                                        <td class="contenthead">Print</td>
                                    </tr>
                                    <%

                                    while (invHeader.hasNext()) {
                                            index++;
                                            
                                            int oddEven2 = index % 2;
                                            if (oddEven2 > 0) {
                                                classText2 = "text2";
                                            } else {
                                                classText2 = "text1";
                                            }
                                            optTO = new OperationTO();
                                            optTO = (OperationTO) invHeader.next();

                                            invoiceCode = optTO.getInvoiceCode();
                                            if(invoiceCode == ""){
                                                invoiceCode = "";
                                                }
                                            invoiceDate = optTO.getInvoiceDate();
                                            if(invoiceDate == ""){
                                                invoiceDate = "";
                                                }
                                            invoiceFor = optTO.getInvoiceFor();
                                            if(invoiceFor == ""){
                                                invoiceFor = "";
                                                }
                                            grandTotal = optTO.getGrandTotal();
                                            if(grandTotal == ""){
                                                grandTotal = "";
                                                }
                                            totalGrand += Double.parseDouble(grandTotal);
                                            numberOfTri = optTO.getNumberOfTri();
                                            if(numberOfTri == ""){
                                                numberOfTri = "";
                                                }
                                            totalTrip += Double.parseDouble(numberOfTri);

                                            %>
                                            <tr>
                                                <td class="<%=classText2%>"><%=index%></td>
                                                <td class="<%=classText2%>"><%=invoiceCode%></td>
                                                <td class="<%=classText2%>"><%=invoiceDate%></td>
                                                <td class="<%=classText2%>"><%=invoiceFor%></td>
                                                <td class="<%=classText2%>"><%=grandTotal%></td>
                                                <td class="<%=classText2%>"><%=numberOfTri%></td>
                                                <td class="<%=classText2%>" align="left"> <a href="/throttle/viewInvoiceDetail.do?invoiceCode=<%=invoiceCode%>"  > Print </a> </td>
                                        </tr>


                                    <%
                                        }
                                   %>
                                     <tr>
                                         <td class="<%=classText2%>" colspan="4">&nbsp;</td>
                                                <td class="<%=classText2%>"><%=df.format(totalGrand)%></td>
                                                <td class="<%=classText2%>"><%=df.format(totalTrip)%></td>
                                         <td class="<%=classText2%>" >&nbsp;</td>
                                                
                                        </tr>

                                        <%
                                }
%>
                                    
                                
                            </table>
                        </div>
            </table>
        </form>

    </body>

</html>
