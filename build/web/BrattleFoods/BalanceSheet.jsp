<%-- 
    Document   : BalanceSheet
    Created on : Nov 7, 2013, 2:52:50 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <title>JSP Page</title>
    </head>
     <%
        String menuPath = "Finance >> Balance Sheet";
        request.setAttribute("menuPath", menuPath);
    %>
    <body>
        <form name="balanceSheet" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <table width="95%"  align="center" cellpadding="0" cellspacing="0" class="border">
                <thead>
                    <tr>
                        <h3 style="text-align: center; font: normal; font-size: 30px; color: skyblue">Brattle Foods Balance Sheet</h3>
                    </tr>
                </thead>
                <br>
                <br>
                <tbody>
                    <tr class="contenthead">
                    <td>Balance Sheet</td>
                    <td>Jan</td>
                    <td>Feb</td>
                    <td>Mar</td>
                    <td>Apr</td>
                    <td>May</td>
                    <td>Jun</td>
                    <td>Jul</td>
                    <td>Aug</td>
                    <td>Sep</td>
                    <td>Oct</td>
                    <td>Nov</td>
                    <td>Dec</td>
<!--                    <td>EOY1</td>
                    <td>EOY2</td>
                    <td>EOY3</td>-->
                </tr>
                <tr><td colspan="16">&nbsp;</td></tr>
                <tr>
                    <td>Cash</td>
                    <td>10,000</td><td>10,050</td><td>12,000</td><td>10,000</td><td>13,000</td><td>20,000</td><td>21,000</td><td>22,000</td><td>18,250</td><td>26,000</td><td>22,000</td><td>25,450</td>
                    <!--<td>25,450</td><td>27,000</td><td>35,000</td>-->
                    
                </tr>
                <tr>
                    <td>Inventory</td>
                    <td>20,000</td><td>18,000</td><td>20,000</td><td>17,000</td><td>16,000</td><td>17,900</td><td>17,850</td><td>19,000</td><td>25,000</td><td>26,000</td><td>21,000</td><td>18,000</td>
                    <!--<td>18,000</td><td>25,000</td><td>35,000</td>-->
                </tr>
                <tr>
                    <td>Accounts Recievable</td>
                    <td>12,000</td><td>15,000</td><td>13,900</td><td>17,000</td><td>21,000</td><td>13,000</td><td>15,000</td><td>18,000</td><td>20,000</td><td>15,700</td><td>14,000</td><td>16,000</td>
                    <!--<td>16,000</td><td>20,000</td><td>22,000</td>-->
                </tr>
                <tr>
                    <td>Prepaid Expenses</td>
                    <td>1,000</td><td>1,000</td><td>1,000</td><td>1,000</td><td>1,000</td><td>1,000</td><td>1,000</td><td>1,000</td><td>1,000</td><td>1,000</td><td>1,000</td><td>1,000</td>
                    <!--<td>1,000</td><td>1,000</td><td>1,000</td>-->
                </tr>
                <tr>
                    <td>Other Current Assets</td>
                    <td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
                    <!--<td>0</td><td>0</td><td>0</td>-->
                </tr>
                <tr>
                    <td><b>TOTAL CURRENT ASSETS</b></td>
                    <td><b>43,000</b></td><td><b>44,050</b></td><td><b>46,900</b></td><td><b>45,000</b></td><td><b>51,000</b></td><td><b>51,900</b></td><td><b>54,850</b></td><td><b>60,000</b></td><td><b>64,250</b></td><td><b>68,700</b></td><td><b>58,000</b></td><td><b>60,450</b></td>
                    <!--<td><b>60,450</b></td><td><b>73,000</b></td><td><b>93,000</b></td>-->
                </tr>
                <tr><td colspan="16">&nbsp;</td></tr>
                <tr>
                    <td>Equipment (less depreciation)</td>
                    <td>30,000</td><td>29,500</td><td>29,000</td><td>35,000</td><td>35,500</td><td>35,000</td><td>34,500</td><td>34,000</td><td>34,000</td><td>33,500</td><td>37,000</td><td>36,000</td>
                    <!--<td>36,000</td><td>50,000</td><td>62,000</td>-->
                </tr>
                <tr>
                    <td>Leasehold Improvements (less depr)</td>
                    <td>20,000</td><td>19,750</td><td>19,500</td><td>19,250</td><td>19,000</td><td>18,750</td><td>18,500</td><td>18,250</td><td>18,000</td><td>17,750</td><td>17,500</td><td>17,250</td>
                    <!--<td>17,250</td><td>13,650</td><td>10,050</td>-->
                </tr>
                <tr>
                    <td>Other Fixed Assets</td>
                    <td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
                    <!--<td>0</td><td>0</td><td>0</td>-->
                </tr>
                <tr>
                    <td><b>TOTAL FIXED ASSETS</b></td>
                    <td><b>50,000</b></td><td><b>49,250</b></td><td><b>48,500</b></td><td><b>54,250</b></td><td><b>54,500</b></td><td><b>53,750</b></td><td><b>53,000</b></td><td><b>52,250</b></td><td><b>52,000</b></td><td><b>51,250</b></td><td><b>54,500</b></td><td><b>53,250</b></td>
                    <!--<td><b>53,250</b></td><td><b>63,650</b></td><td><b>72,050</b></td>-->
                </tr>
                <tr><td colspan="16">&nbsp;</td></tr>
                <tr style="background-color: springgreen">
                    <td>TOTAL ASSETS</td>
                    <td>93,000</td><td>93,300</td><td>95,400</td><td>99,250</td><td>105,500</td><td>105,650</td><td>107,850</td><td>112,250</td><td>116,250</td><td>119,950</td><td>112,500</td><td>113,700</td>
                    <!--<td>113,700</td><td>136,650</td><td>165,050</td>-->
                </tr>
                <tr><td colspan="16">&nbsp;</td></tr>
                <tr>
                    <td>Accounts Payable</td>
                    <td>8,000</td><td>6,550</td><td>6,000</td><td>5,000</td><td>11,050</td><td>9,000</td><td>8,000</td><td>10,700</td><td>15,000</td><td>15,000</td><td>12,000</td><td>10,000</td>
                    <!--<td>10,000</td><td>14,650</td><td>20,000</td>-->
                </tr>
                <tr>
                    <td>Notes Payable</td>
                    <td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
                    <!--<td>0</td><td>0</td><td>0</td>-->
                </tr>
                <tr>
                    <td>Line of Credit Payable</td>
                    <td>5,000</td><td>5,000</td><td>5,000</td><td>5,000</td><td>5,000</td><td>5,000</td><td>5,000</td><td>4,500</td><td>4,000</td><td>3,500</td><td>3,000</td><td>1,500</td>
                    <!--<td>1,500</td><td>1,000</td><td>0</td>-->
                </tr>
                <tr>
                    <td>Taxes Payable</td>
                    <td>2,000</td><td>2,000</td><td>2,000</td><td>2,000</td><td>2,000</td><td>2,000</td><td>2,000</td><td>2,000</td><td>2,000</td><td>2,000</td><td>2,000</td><td>2,000</td>
                    <!--<td>2,000</td><td>2,000</td><td>2,000</td>-->
                </tr>
                <tr>
                    <td>Other Current Liabiliites</td>
                    <td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
                    <!--<td>0</td><td>0</td><td>0</td>-->
                </tr>
                <tr>
                    <td><b>TOTAL CURRENT LIABILITIES</b></td>
                    <td><b>15,000</b></td><td><b>13,550</b></td><td><b>13,000</b></td><td><b>12,000</b></td><td><b>18,050</b></td><td><b>16,000</b></td><td><b>15,000</b></td><td><b>17,200</b></td><td><b>21,000</b></td><td><b>20,500</b></td><td><b>17,000</b></td><td><b>13,500</b></td>
                    <!--<td><b>13,500</b></td><td><b>17,650</b></td><td><b>22,000</b></td>-->
                </tr>
                <tr><td colspan="16">&nbsp;</td></tr>
                <tr>
                    <td>Long Term Debt</td>
                    <td>20,000</td><td>19,700</td><td>19,400</td><td>25,750</td><td>25,450</td><td>25,150</td><td>24,850</td><td>24,550</td><td>24,250</td><td>23,950</td><td>23,650</td><td>23,350</td>
                    <!--<td>23,350</td><td>35,000</td><td>23,050</td>-->
                </tr>
                <tr>
                    <td>Other Long Term Liabilities</td>
                    <td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
                    <!--<td>0</td><td>0</td><td>0</td>-->
                </tr>
                <tr>
                    <td><b>TOTAL LONG TERM LIABILITIES</b></td>
                    <td><b>20,000</b></td><td><b>19,700</b></td><td><b>19,400</b></td><td><b>25,750</b></td><td><b>25,450</b></td><td><b>25,150</b></td><td><b>24,850</b></td><td><b>24,550</b></td><td><b>24,250</b></td><td><b>23,950</b></td><td><b>23,650</b></td><td><b>23,350</b></td>
                    <!--<td><b>23,350</b></td><td><b>35,000</b></td><td><b>23,050</b></td>-->
                </tr>
                <tr><td colspan="16">&nbsp;</td></tr>
                <tr>
                    <td>Owner's Capital Contribution</td>
                    <td>50,000</td><td>50,000</td><td>50,000</td><td>50,000</td><td>50,000</td><td>50,000</td><td>50,000</td><td>50,000</td><td>50,000</td><td>50,000</td><td>50,000</td><td>50,000</td>
                    <!--<td>50,000</td><td>50,000</td><td>50,000</td>-->
                </tr>
                <tr>
                    <td>Less Owners Draw</td>
                    <td>2,000</td><td>2,500</td><td>3,000</td><td>3,500</td><td>5,000</td><td>5,500</td><td>6,000</td><td>6,500</td><td>9,000</td><td>9,500</td><td>20,000</td><td>20,500</td>
                    <!--<td>20,500</td><td>43,000</td><td>90,000</td>-->
                </tr>
                <tr>
                    <td>Retained Earnings</td>
                    <td>10,000</td><td>12,550</td><td>16,000</td><td>15,000</td><td>17,000</td><td>20,000</td><td>24,000</td><td>27,000</td><td>30,000</td><td>35,000</td><td>41,850</td><td>47,350</td>
                    <!--<td>47,350</td><td>77,000</td><td>160,000</td>-->
                </tr>
                <tr>
                    <td><b>TOTAL OWNER'S EQUITY</b></td>
                    <td><b>58,000</b></td><td><b>60,050</b></td><td><b>63,000</b></td><td><b>61,500</b></td><td><b>62,000</b></td><td><b>64,500</b></td><td><b>68,000</b></td><td><b>70,500</b></td><td><b>71,000</b></td><td><b>75,500</b></td><td><b>71,850</b></td><td><b>76,850</b></td>
                    <!--<td><b>76,850</b></td><td><b>84,000</b></td><td><b>120,000</b></td>-->
                </tr>
                <tr><td colspan="16">&nbsp;</td></tr>
                <tr style="background-color: springgreen">
                    <td>TOTAL LIABILITIES & EQUITY</td>
                    <td>93,000</td><td>93,300</td><td>95,400</td><td>99,250</td><td>105,500</td><td>105,650</td><td>107,850</td><td>112,250</td><td>116,250</td><td>119,950</td><td>112,500</td><td>113,700</td>
                    <!--<td>113,700</td><td>136,650</td><td>165,050<td>-->
                </tr>
                <tr><td colspan="16">&nbsp;</td></tr>
                <tr><td colspan="16">&nbsp;</td></tr>
                <tr>
                    <td>BALANCE</td>
                    <td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
                    <!--<td>0</td><td>0</td><td>0</td>-->
                </tr>
                </tbody>
        </form>
    </body>
</html>
