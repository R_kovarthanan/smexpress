<%-- 
    Document   : tripSheetClose
    Created on : Nov 4, 2013, 7:56:59 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });
        </script>
        <script type="text/javascript" language="javascript">
            var poItems = 0;
            var rowCount = 1;
            var rowCount1 = 1;
            var sno = 0;
            var sno1 = 0;
            var snumber = 1;
            var snumber1 = 1;

            function addExpenseRow() {
                if (sno1 < 19) {
                    sno1++;
                    var tab = document.getElementById("FuelExpenseTBL");
                    var rowCount = tab.rows.length;

                    snumber1 = parseInt(rowCount1) - 1;

                    var newrow = tab.insertRow(parseInt(rowCount1) - 1);
                    newrow.height = "30px";
                    // var temp = sno1-1;
                    var cell = newrow.insertCell(0);
                    var cell0 = "<td><input type='hidden'  name='itemId' /> " + snumber1 + "</td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(1);
                    cell0 = "<td class='text1'><input name='driverName' type='text' class='textbox' id='driverName'  /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(2);
                    cell0 = "<td class='text1'><input name='expenseDetails' type='text' class='textbox' id='expenseDetails' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(3);
                    cell0 = "<td class='text1'><input name='date' type='text' class='textbox' id='date' class='datepicker' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(4);
                    cell0 = "<td class='text1'><input name='amount' type='text' class='textbox' id='amount'  /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(4);
                    cell0 = "<td class='text1'><input name='amount' type='text' class='textbox' id='amount'  /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    rowCount1++;

                    $(".datepicker").datepicker({
                        /*altField: "#alternate",
                         altFormat: "DD, d MM, yy"*/
                        changeMonth: true, changeYear: true
                    });
                }
                document.settle.expenseId.value = sno1;
            }

            function delAllowanceRow() {
                try {
                    var table = document.getElementById("expenseTBL");
                    rowCount = table.rows.length - 1;
                    for (var i = 2; i < rowCount; i++) {
                        var row = table.rows[i];
                        var checkbox = row.cells[6].childNodes[0];
                        if (null != checkbox && true == checkbox.checked) {
                            if (rowCount <= 1) {
                                alert("Cannot delete all the rows");
                                break;
                            }
                            table.deleteRow(i);
                            rowCount--;
                            i--;
                            sno--;
                        }
                    }
                    document.settle.expenseId.value = sno;
                } catch (e) {
                    alert(e);
                }
            }
            function parseDouble(value) {
                if (typeof value == "string") {
                    value = value.match(/^-?\d*/)[0];
                }
                return !isNaN(parseInt(value)) ? value * 1 : NaN;
            }


            function getDriverName() {
                var oTextbox = new AutoSuggestControl(document.getElementById("driName"), new ListSuggestions("driName", "/throttle/handleDriverSettlement.do?"));

            }
        </script>
        <script language="">
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
        </script>
    </head>

    <script type="text/javascript">
        var rowCount = 1;
        var sno = 0;
        var rowCount1 = 1;
        var sno1 = 0;
        var httpRequest;
        var httpReq;
        var styl = "";
        function addRow() {
            if (parseInt(rowCount) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            sno++;
            var tab = document.getElementById("addTyres");
            var newrow = tab.insertRow(rowCount);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' >" + sno + "</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            // Positions
            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='25' >" +
                    "<select name='positionIds'   class='textbox' > <option value='0'>-Select-</option>" +
                    "<option value=Salem > Salem  </option>" +
                    "<option value=Madurai >Madurai</option>" +
                    "<option value=Madurai >Delhi</option>" +
                    "<option value=Madurai >Chennai</option>" +
                    "<option value=Madurai >Covai</option>" +
                    +"</select> </td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            // TyreIds
            var cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='25' >" +
                    "<select name='itemIds' class='textbox'    > <option value='0'>-Select-</option>" +
                    "<option value=pk > Pickup </option>" +
                    "<option value=dp > Drop </option>" +
                    "<option value=pkdp > Pickup &amp; Drop </option>" +
                    +"</select> </td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(3);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='estDate' class='datepicker' >";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;


            cell = newrow.insertCell(4);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='estDate' class='datepicker' >";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(5);
            var cell0 = "<td class='text1' height='25' >" +
                    "<select name='positionIds'   class='textbox' > <option value='0'>-Select-</option>" +
                    "<option value=Salem > Salem  </option>" +
                    "<option value=Madurai >Madurai</option>" +
                    "<option value=Madurai >Delhi</option>" +
                    "<option value=Madurai >Chennai</option>" +
                    "<option value=Madurai >Covai</option>" +
                    +"</select> </td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            var cell = newrow.insertCell(6);
            var cell0 = "<td class='text1' height='25' >" +
                    "<select name='itemIds' class='textbox'    > <option value='0'>-Select-</option>" +
                    "<option value=pk > Pickup </option>" +
                    "<option value=dp > Drop </option>" +
                    "<option value=pkdp > Pickup &amp; Drop </option>" +
                    +"</select> </td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(7);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='estDate' class='textbox' >";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;


            cell = newrow.insertCell(8);
            var cell2 = "<td class='text1' height='30' ><input type='text' name='tyreDate' class='datepicker' >";
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell2;

            cell = newrow.insertCell(9);
            var cell2 = "<td class='text1' height='30' ><input type='text' name='tyreDate' class='distance' value='100'>";
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell2;


            rowCount++;

            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });

        }

        var cNoteSno = 0;
        var cNoteRowCount = 1;
        function addCNoteRow() {
            if (parseInt(cNoteRowCount) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            cNoteSno++;
            var tab = document.getElementById("CNoteAdd");
            var newrow = tab.insertRow(cNoteRowCount);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' >" + cNoteSno + "</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            // Positions
            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='25' >" +
                    "<select name='positionIds'   class='textbox' > <option value='0'>-Select-</option>" +
                    "<option value=Salem > Salem  </option>" +
                    "<option value=Madurai >Madurai</option>" +
                    "<option value=Madurai >Delhi</option>" +
                    "<option value=Madurai >Chennai</option>" +
                    "<option value=Madurai >Covai</option>" +
                    +"</select> </td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            // TyreIds
            var cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='25' >" +
                    "<select name='itemIds' class='textbox'    > <option value='0'>-Select-</option>" +
                    "<option value=pk > Pickup </option>" +
                    "<option value=dp > Drop </option>" +
                    "<option value=pkdp > Pickup &amp; Drop </option>" +
                    +"</select> </td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(3);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='estDate' class='datepicker' >";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;


            cell = newrow.insertCell(4);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='estDate' class='datepicker' >";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(5);
            var cell0 = "<td class='text1' height='25' >" +
                    "<select name='positionIds'   class='textbox' > <option value='0'>-Select-</option>" +
                    "<option value=Salem > Salem  </option>" +
                    "<option value=Madurai >Madurai</option>" +
                    "<option value=Madurai >Delhi</option>" +
                    "<option value=Madurai >Chennai</option>" +
                    "<option value=Madurai >Covai</option>" +
                    +"</select> </td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            var cell = newrow.insertCell(6);
            var cell0 = "<td class='text1' height='25' >" +
                    "<select name='itemIds' class='textbox'    > <option value='0'>-Select-</option>" +
                    "<option value=pk > Pickup </option>" +
                    "<option value=dp > Drop </option>" +
                    "<option value=pkdp > Pickup &amp; Drop </option>" +
                    +"</select> </td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(7);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='estDate' class='textbox' >";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;


            cell = newrow.insertCell(8);
            var cell2 = "<td class='text1' height='30' ><input type='text' name='tyreDate' class='datepicker' >";
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell2;

            cell = newrow.insertCell(9);
            var cell2 = "<td class='text1' height='30' ><input type='text' name='tyreDate' class='distance' value='100'>";
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell2;
            cNoteRowCount++;

            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });

        }



        var adRowCount = 1;
        var adSno = 0;
        function addAdvanceRow() {
            if (parseInt(adRowCount) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            adSno++;
            var tab = document.getElementById("AdvanceTBL");
            var newrow = tab.insertRow(adRowCount);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' >" + adSno + "</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            // Positions
            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='25' >" +
                    " <input type='text' name='estDate' class='datepicker' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            // TyreIds
            var cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='25' ><input type='text' name='estDate' class='datepicker' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(3);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='estDate' class='datepicker' >";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;


            cell = newrow.insertCell(4);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='estDate' class='datepicker' >";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;


            adRowCount++;

            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });

        }

        var fuelRowCount = 1;
        var fuelSno = 0;
        function addFuelExpenseRow() {
            if (parseInt(fuelRowCount) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            fuelSno++;
            var tab = document.getElementById("FuelExpenseTBL");
            var newrow = tab.insertRow(fuelRowCount);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' >" + fuelSno + "</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            // Positions
            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='25' >" +
                    "<input type='text' name='estDate' class='textbox' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            // TyreIds
            var cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='25' ><input type='text' name='estDate' class='textbox' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(3);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='estDate' class='datepicker' >";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;


            cell = newrow.insertCell(4);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='estDate' class='textbox' >";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(4);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='estDate' class='textbox' >";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;


            fuelRowCount++;

            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });

        }


        var suppRowCount = 1;
        var suppSno = 0;
        function addSuppExpenseRow() {
            if (parseInt(fuelRowCount) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            suppSno++;
            var tab = document.getElementById("suppExpenseTBL");
            var newrow = tab.insertRow(suppRowCount);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' >" + suppSno + "</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            // Positions
            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='25' >" +
                    "<input type='text' name='estDate' class='datepicker' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            // TyreIds
            var cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='25' ><input type='text' name='estDate' class='textbox' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(3);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='estDate' class='textbox' >";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;

            suppRowCount++;

            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });

        }

        var driRowCount = 1;
        var driSno = 0;
        function addDriverDetails() {
            if (parseInt(driRowCount) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            driSno++;
            var tab = document.getElementById("DriverDetails");
            var newrow = tab.insertRow(driRowCount);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' >" + driSno + "</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;


            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='25' >" +
                    "<input type='text' name='estDate' class='textbox' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            var cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='25' ><input type='text' name='estDate' class='textbox' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;


            cell = newrow.insertCell(3);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='estDate' class='textbox' >";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(4);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='estDate' class='textbox' readonly>";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;

            driRowCount++;


        }

        var clRowCount = 1;
        var clSno = 0;
        function addCleanerDetails() {
            if (parseInt(clRowCount) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            clSno++;
            var tab = document.getElementById("CleanerDetails");
            var newrow = tab.insertRow(clRowCount);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' >" + clSno + "</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;


            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='25' >" +
                    "<input type='text' name='estDate' class='textbox' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;


            clRowCount++;


        }
        
        var podRowCount1 = 1;
        var podSno1 = 0;
        function addPod1() {
            if (parseInt(podRowCount1) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            podSno1++;
            var tab = document.getElementById("POD1");
            var newrow = tab.insertRow(podRowCount1);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;


            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='25' ><input type='text' name='estDate' class='textbox' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            
            cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='25' ><input type='text' name='estDate' class='datepicker' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            
            cell = newrow.insertCell(3);
            var cell0 = "<td class='text1' height='25' ><input type='file' name='estDate' class='textbox' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            podRowCount1++;
            
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });
        }
        

        function callAddRow() {
            for (var i = 0; i < 3; i++) {
                addRow();
            }
        }
    </script>
    <body onload="callAddRow();
            addCNoteRow();
            addFuelExpenseRow();
            addSuppExpenseRow();addPod1();">
        <% String menuPath = "Operation >>  Close Trip Sheet";
            request.setAttribute("menuPath", menuPath);
        %>
        <form name="settle" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <table width="100%">
                <tr>
                    <td class="contenthead" colspan="8">Trip Id : 1000</td>
                </tr>
            </table>
            <div id="tabs" >
                <ul>
                    <li><a href="#tripDetail"><span>Trip Details</span></a></li>
                    <li><a href="#routeDetail"><span>Route Course</span></a></li>
                    <li><a href="#driverDetail"><span>Driver Details</span></a></li>
                    <li><a href="#cleanerDetail"><span>Cleaner Details</span></a></li>
                    <li><a href="#advDetail"><span>Advance Details</span></a></li>
                    <li><a href="#fuelExpDetail"><span>Fuel Expense Details</span></a></li>
                    <li><a href="#otherExpDetail"><span>Other Expense Details</span></a></li>
                    <li><a href="#tripSettlement"><span>Trip Settlement</span></a></li>
                    <li><a href="#podUpdate"><span>POD Updated</span></a></li>
                    <li><a href="#summary"><span>Remarks</span></a></li>
                </ul>

                <div id="tripDetail">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="6" >Trip Sheet</td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Trip Sheet Date</td>
                            <td class="text1"><input type="text" name="date" class="datepicker" value="date"></td>
                            <td class="text1"><font color="red">*</font>CNote No</td>
                            <td class="text1"><input type="text" name="customerName" class="textbox" value="101,102"></td>
                            <td class="text1"><font color="red">*</font>Customer Freight Rate/Km (INR)</td>
                            <td class="text1"><input type="text" name="customerName" class="textbox" valu="12"></td>
                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>Customer Code</td>
                            <td class="text2"><input type="text" name="customerCode" class="textbox" value="BF00001"></td>
                            <td class="text2"><font color="red">*</font>Customer Name</td>
                            <td class="text2"><input type="text" name="customerName" class="textbox" valu="Parveen Travells"></td>
                            <td class="text2"><font color="red">*</font>Customer ContactNo</td>
                            <td class="text2"><input type="text" name="Phone" class="textbox" valu="993456789"></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Route Name</td>
                            <td class="text1"><input type="text" name="routeName" class="textbox" value="Chennai-Delhi"></td>
                            <td class="text1"><font color="red">*</font>Route Code</td>
                            <td class="text1"><input type="text" name="routeName" class="textbox" valu="DL001"></td>
                            <td class="text1"><font color="red">*</font>Vehicle No</td>
                            <td class="text1"><input type="text" name="vehicleNo" class="textbox" value="DL 01 G 2025"></td>
                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>Actual Pickup Date</td>
                            <td class="text2"><input type="text" name="tripDate" class="textbox"></td>
                            <td class="text2"><font color="red">*</font>Estimated Delivery Date</td>
                            <td class="text2"><input type="text" name="departureDate" class="textbox"></td>
                            <td class="text2"><font color="red">*</font>Carrying Weight</td>
                            <td class="text2"><input type="text" name="arrivalDate" class="textbox"></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Trip Start Date</td>
                            <td class="text1"><input type="text" name="kmOut" class="datepicker" value=""></td>
                            <td class="text1"><font color="red">*</font>Trip Start Time</td>
                            <td class="text1"><select>
                                    <option value=\"06:00\">6:00 AM</option>
                                    <option value=\"06:30\">6:30 AM</option>
                                    <option value=\"07:00\">7:00 AM</option>
                                    <option value=\"07:30\">7:30 AM</option>
                                    <option value=\"08:00\">8:00 AM</option>
                                    <option value=\"08:30\">8:30 AM</option>
                                    <option value=\"09:00\"  selected>9:00 AM</option>
                                    <option value=\"09:30\">9:30 AM</option>
                                    <option value=\"10:00\">10:00 AM</option>
                                    <option value=\"10:30\">10:30 AM</option>
                                    <option value=\"11:00\">11:00 AM</option>
                                    <option value=\"11:30\">11:30 AM</option>
                                    <option value=\"12:00\">12:00 PM</option>
                                    <option value=\"12:30\">12:30 PM</option>
                                    <option value=\"13:00\">1:00 PM</option>
                                    <option value=\"13:30\">1:30 PM</option>
                                    <option value=\"14:00\">2:00 PM</option>
                                    <option value=\"14:30\">2:30 PM</option>
                                    <option value=\"15:00\">3:00 PM</option>
                                    <option value=\"15:30\">3:30 PM</option>
                                    <option value=\"16:00\">4:00 PM</option>
                                    <option value=\"16:30\">4:30 PM</option>
                                    <option value=\"17:00\">5:00 PM</option>
                                    <option value=\"17:30\">5:30 PM</option>
                                    <option value=\"18:00\">6:00 PM</option>
                                    <option value=\"18:30\">6:30 PM</option>
                                    <option value=\"19:00\">7:00 PM</option>
                                    <option value=\"19:30\">7:30 PM</option>
                                    <option value=\"20:00\">8:00 PM</option>
                                    <option value=\"20:30\">8:30 PM</option>
                                    <option value=\"21:00\">9:00 PM</option>
                                    <option value=\"21:30\">9:30 PM</option>
                                    <option value=\"22:00\">10:00 PM</option>
                                </select></td>
                            <td class="text1"><font color="red">*</font>Km Out Start Point</td>
                            <td class="text1"><input type="text" name="hmOut" class="textbox"></td>

                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>Trip End Date</td>
                            <td class="text2"><input type="text" name="kmOut" class="datepicker" value=""></td>
                            <td class="text2"><font color="red">*</font>Trip End Time</td>
                            <td class="text2"><select>
                                    <option value=\"06:00\">6:00 AM</option>
                                    <option value=\"06:30\">6:30 AM</option>
                                    <option value=\"07:00\">7:00 AM</option>
                                    <option value=\"07:30\">7:30 AM</option>
                                    <option value=\"08:00\">8:00 AM</option>
                                    <option value=\"08:30\">8:30 AM</option>
                                    <option value=\"09:00\"  selected>9:00 AM</option>
                                    <option value=\"09:30\">9:30 AM</option>
                                    <option value=\"10:00\">10:00 AM</option>
                                    <option value=\"10:30\">10:30 AM</option>
                                    <option value=\"11:00\">11:00 AM</option>
                                    <option value=\"11:30\">11:30 AM</option>
                                    <option value=\"12:00\">12:00 PM</option>
                                    <option value=\"12:30\">12:30 PM</option>
                                    <option value=\"13:00\">1:00 PM</option>
                                    <option value=\"13:30\">1:30 PM</option>
                                    <option value=\"14:00\">2:00 PM</option>
                                    <option value=\"14:30\">2:30 PM</option>
                                    <option value=\"15:00\">3:00 PM</option>
                                    <option value=\"15:30\">3:30 PM</option>
                                    <option value=\"16:00\">4:00 PM</option>
                                    <option value=\"16:30\">4:30 PM</option>
                                    <option value=\"17:00\">5:00 PM</option>
                                    <option value=\"17:30\">5:30 PM</option>
                                    <option value=\"18:00\">6:00 PM</option>
                                    <option value=\"18:30\">6:30 PM</option>
                                    <option value=\"19:00\">7:00 PM</option>
                                    <option value=\"19:30\">7:30 PM</option>
                                    <option value=\"20:00\">8:00 PM</option>
                                    <option value=\"20:30\">8:30 PM</option>
                                    <option value=\"21:00\">9:00 PM</option>
                                    <option value=\"21:30\">9:30 PM</option>
                                    <option value=\"22:00\">10:00 PM</option>
                                </select></td>
                            <td class="text2"><font color="red">*</font>Km In End Point</td>
                            <td class="text2"><input type="text" name="reeferHourMeter" class="textbox"></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Reefer Start Date</td>
                            <td class="text1"><input type="text" name="kmOut" class="datepicker" value=""></td>
                            <td class="text1"><font color="red">*</font>Reefer Start Time</td>
                            <td class="text1"><select>
                                    <option value=\"06:00\">6:00 AM</option>
                                    <option value=\"06:30\">6:30 AM</option>
                                    <option value=\"07:00\">7:00 AM</option>
                                    <option value=\"07:30\">7:30 AM</option>
                                    <option value=\"08:00\">8:00 AM</option>
                                    <option value=\"08:30\">8:30 AM</option>
                                    <option value=\"09:00\"  selected>9:00 AM</option>
                                    <option value=\"09:30\">9:30 AM</option>
                                    <option value=\"10:00\">10:00 AM</option>
                                    <option value=\"10:30\">10:30 AM</option>
                                    <option value=\"11:00\">11:00 AM</option>
                                    <option value=\"11:30\">11:30 AM</option>
                                    <option value=\"12:00\">12:00 PM</option>
                                    <option value=\"12:30\">12:30 PM</option>
                                    <option value=\"13:00\">1:00 PM</option>
                                    <option value=\"13:30\">1:30 PM</option>
                                    <option value=\"14:00\">2:00 PM</option>
                                    <option value=\"14:30\">2:30 PM</option>
                                    <option value=\"15:00\">3:00 PM</option>
                                    <option value=\"15:30\">3:30 PM</option>
                                    <option value=\"16:00\">4:00 PM</option>
                                    <option value=\"16:30\">4:30 PM</option>
                                    <option value=\"17:00\">5:00 PM</option>
                                    <option value=\"17:30\">5:30 PM</option>
                                    <option value=\"18:00\">6:00 PM</option>
                                    <option value=\"18:30\">6:30 PM</option>
                                    <option value=\"19:00\">7:00 PM</option>
                                    <option value=\"19:30\">7:30 PM</option>
                                    <option value=\"20:00\">8:00 PM</option>
                                    <option value=\"20:30\">8:30 PM</option>
                                    <option value=\"21:00\">9:00 PM</option>
                                    <option value=\"21:30\">9:30 PM</option>
                                    <option value=\"22:00\">10:00 PM</option>
                                </select></td>
                            <td class="text1"><font color="red">*</font>Reefer Hm Out Start Point</td>
                            <td class="text1"><input type="text" name="reeferHourMeter" class="textbox"></td>
                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>Reefer End Date</td>
                            <td class="text2"><input type="text" name="kmOut" class="datepicker" value=""></td>
                            <td class="text2"><font color="red">*</font>Reefer End Time</td>
                            <td class="text2"><select>
                                    <option value=\"06:00\">6:00 AM</option>
                                    <option value=\"06:30\">6:30 AM</option>
                                    <option value=\"07:00\">7:00 AM</option>
                                    <option value=\"07:30\">7:30 AM</option>
                                    <option value=\"08:00\">8:00 AM</option>
                                    <option value=\"08:30\">8:30 AM</option>
                                    <option value=\"09:00\" >9:00 AM</option>
                                    <option value=\"09:30\">9:30 AM</option>
                                    <option value=\"10:00\">10:00 AM</option>
                                    <option value=\"10:30\">10:30 AM</option>
                                    <option value=\"11:00\">11:00 AM</option>
                                    <option value=\"11:30\">11:30 AM</option>
                                    <option value=\"12:00\">12:00 PM</option>
                                    <option value=\"12:30\">12:30 PM</option>
                                    <option value=\"13:00\">1:00 PM</option>
                                    <option value=\"13:30\">1:30 PM</option>
                                    <option value=\"14:00\">2:00 PM</option>
                                    <option value=\"14:30\">2:30 PM</option>
                                    <option value=\"15:00\">3:00 PM</option>
                                    <option value=\"15:30\">3:30 PM</option>
                                    <option value=\"16:00\">4:00 PM</option>
                                    <option value=\"16:30\">4:30 PM</option>
                                    <option value=\"17:00\">5:00 PM</option>
                                    <option value=\"17:30\">5:30 PM</option>
                                    <option value=\"18:00\">6:00 PM</option>
                                    <option value=\"18:30\">6:30 PM</option>
                                    <option value=\"19:00\">7:00 PM</option>
                                    <option value=\"19:30\">7:30 PM</option>
                                    <option value=\"20:00\">8:00 PM</option>
                                    <option value=\"20:30\">8:30 PM</option>
                                    <option value=\"21:00\">9:00 PM</option>
                                    <option value=\"21:30\">9:30 PM</option>
                                    <option value=\"22:00\">10:00 PM</option>
                                </select></td>
                            <td class="text2"><font color="red">*</font>Reefer Hm In End Point</td>
                            <td class="text2"><input type="text" name="reeferHourMeter" class="textbox"></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Total Km Operated</td>
                            <td class="text1"><input type="text" name="ownership" class="textbox" value=""></td>
                            <td class="text1"><font color="red">*</font>Reefer Total Hr Operated</td>
                            <td class="text1"><input type="text" name="ownership" class="textbox" value=""></td>
                            <td class="text1"><font color="red">*</font>Trip Status</td>
                            <td class="text1"><select>
                                    <option value="">Open</option>
                                    <option value="" selected>Close</option>
                                </select></td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="4" >Check List</td>
                        </tr>
                        <tr>
                            <td class="text1">Vehicle RC Copy</td>
                            <td class="text1"><input type="checkbox" name="date"  value="date" checked></td>
                            <td class="text1" colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="text2">Vehicle FC Copy</td>
                            <td class="text2"><input type="checkbox" name="customerName" class="textbox" valu="R Srinivasan" checked></td>
                            <td class="text2">Vehicle FC Valid Date</td>
                            <td class="text2"><label><font color="green">21-10-2013</font></label></td>
                        </tr>
                        <tr>
                            <td class="text1">Vehicle Insurance Copy</td>
                            <td class="text1"><input type="checkbox" name="customerName" class="textbox" valu="R Srinivasan" checked></td>
                            <td class="text1">Vehicle Insurance Valid Date</td>
                            <td class="text1"><label><font color="green">21-10-2013</font></label></td>
                        </tr>
                        <tr>
                            <td class="text2">Vehicle Permit</td>
                            <td class="text2"><input type="checkbox" name="customerCode" class="textbox" value="BF00001" checked></td>
                            <td class="text2">Vehicle Permit Valid Date</td>
                            <td class="text2"><label><font color="green">30-12-2013</font></label></td>
                        </tr>
                        <tr>
                            <td class="text2">Road Tax</td>
                            <td class="text2"><input type="checkbox" name="customerCode" class="textbox" value="BF00001" checked></td>
                            <td class="text2">Road Tax Valid Date</td>
                            <td class="text2"><label><font color="green">30-12-2013</font></label></td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    <center>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Save &amp; Next" name="Save" /></a>
                    </center>
                </div>
                <div id="routeDetail">
                    <table border="0" align="center" width="980" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td>CNote : 101</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <label class="contentsub">Origin :</label>
                                <label>Chennai</label>
                            </td>
                            <td align="right">
                                <label class="contentsub">Destination:</label>
                                <label>Delhi</label>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <table border="1" class="border" align="center" width="500" cellpadding="0" cellspacing="0" id="addTyres">
                        <tr >
                            <td class="contenthead" align="center" height="30" >Sno</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Starting Point</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Type</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>St Address</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Est Date&amp;Time</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Ending Point</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Type</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>End Address</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Est Date&amp;Time</td>
                            <td class="contenthead" height="30" >Total Distance</td>
                        </tr>
                    </table>
                    <center>
                        &emsp;<input type="reset" class="button" value="Clear">
                        <input type="button" class="button" value="Add Row" name="save" onClick="addRow()">
                    </center>
                    <br>
                    <br>
                    <table border="0" align="center" width="980" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td>CNote : 102</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <label class="contentsub">Origin :</label>
                                <label>Chennai</label>
                            </td>
                            <td align="right">
                                <label class="contentsub">Destination:</label>
                                <label>Delhi</label>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <table border="1" class="border" align="center" width="500" cellpadding="0" cellspacing="0" id="CNoteAdd">
                        <tr >
                            <td class="contenthead" align="center" height="30" >Sno</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Starting Point</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Type</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>St Address</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Est Date&amp;Time</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Ending Point</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Type</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>End Address</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Est Date&amp;Time</td>
                            <td class="contenthead" height="30" >Total Distance</td>
                        </tr>
                    </table>
                    <center>
                        &emsp;<input type="reset" class="button" value="Clear">
                        <input type="button" class="button" value="Add Row" name="save" onClick="addCNoteRow(CNoteAdd)">
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Save &amp; Next" name="Save" /></a>
                    </center>
                    <br>
                    <br>
                </div>

                <div id="driverDetail">
                    <table border="0" align="center" width="980" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td align="left">
                                <label class="contentsub">Origin :</label>
                                <label>Chennai</label>
                            </td>
                            <td align="right">
                                <label class="contentsub">Destination:</label>
                                <label>Delhi</label>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>  
                    <table class="border" style="width: 100%; left: 30px;" border="0" cellpadding="0" cellspacing="4" rules="all" >
                        <tr style="width:50px;">
                            <td width="50" class="contenthead">S No&nbsp;</td>
                            <td class="contenthead">Driver Name</td>
                            <td class="contenthead">Log Start Km</td>
                            <td class="contenthead">Log End Km</td>
                            <td class="contenthead">Km Operate</td>
                        </tr>
                        <tr>
                            <td class="text1">1.</td>
                            <td class="text1">Babu</td>
                            <td class="text1">1100 </td>
                            <td class="text1">1300</td>
                            <td class="text1">200</td>
                        </tr>
                        <tr>
                            <td class="text2">2.</td>
                            <td class="text2">Arun</td>
                            <td class="text2">1300</td>
                            <td class="text2">1570</td>
                            <td class="text2">270</td>
                        </tr>
                        <tr>
                            <td class="text1">3.</td>
                            <td class="text1">Madhan</td>
                            <td class="text1">1570</td>
                            <td class="text1">1810</td>
                            <td class="text1">240</td>
                        </tr>

                    </table>
                    <br>
                    <br>
                    <table border="1" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="DriverDetails">
                        <tr >
                            <td class="contenthead" align="center" height="30" >Sno</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Driver Name</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Log Start Km</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Log End Km</td>
                            <td class="contenthead" height="30" >Km Operate</td>
                        </tr>
                    </table>
                    <center>
                        &emsp;<input type="reset" class="button" value="Clear">
                        <input type="button" class="button" value="Add Row" name="save" onClick="addDriverDetails()">
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Save &amp; Next" name="Save" /></a>
                    </center>
                    <br>
                    <br>
                </div>
                <div id="cleanerDetail">
                    <table border="0" align="center" width="980" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td align="left">
                                <label class="contentsub">Origin :</label>
                                <label>Chennai</label>
                            </td>
                            <td align="right">
                                <label class="contentsub">Destination:</label>
                                <label>Delhi</label>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>  
                    <table class="border" style="width: 100%; left: 30px;" border="0" cellpadding="0" cellspacing="4" rules="all" >
                        <tr style="width:50px;">
                            <td width="50" class="contenthead">S No&nbsp;</td>
                            <td class="contenthead">Cleaner Name</td>
                        </tr>
                        <tr>
                            <td class="text1">1.</td>
                            <td class="text1">Surya</td>
                        </tr>
                        <tr>
                            <td class="text2">2.</td>
                            <td class="text2">Sangaya</td>
                        </tr>
                        <tr>
                            <td class="text1">3.</td>
                            <td class="text1">Raja</td>
                        </tr>

                    </table>
                    <br>
                    <br>
                    <table border="1" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="CleanerDetails">
                        <tr >
                            <td class="contenthead" align="center" height="30" >Sno</td>
                            <td class="contenthead" height="30" >Cleaner Name</td>
                        </tr>
                    </table>
                    <center>
                        &emsp;<input type="reset" class="button" value="Clear">
                        <input type="button" class="button" value="Add Row" name="save" onClick="addCleanerDetails()">
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Save &amp; Next" name="Save" /></a>
                    </center>
                    <br>
                    <br>
                </div>

                <div id="advDetail">
                    <div style="border-color: #ffffff solid">
                        <h3>Trip Advance Details</h3>
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                                <td class="contenthead">S No</td>
                                <td class="contenthead">Driver Name</td>
                                <td class="contenthead">Account No</td>
                                <td class="contenthead">Transaction Date and Time</td>
                                <td class="contenthead">Amount</td>
                            </tr>
                            <tr>
                                <td class="text1">1</td>
                                <td class="text1">Babu</td>
                                <td class="text1">IB11236576A001</td>
                                <td class="text1">20-10-2013</td>
                                <td class="text1">2000</td>
                            </tr>
                            <tr>
                                <td class="text2">2</td>
                                <td class="text2">Arun</td>
                                <td class="text2">IB11236576A002</td>
                                <td class="text2">21-10-2013</td>
                                <td class="text2">2000</td>
                            </tr>
                            <tr>
                                <td class="text1">3</td>
                                <td class="text1">Madhan</td>
                                <td class="text1">IB11236576A003</td>
                                <td class="text1">22-10-2013</td>
                                <td class="text1">2100</td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <br>
                        <table class="border" style="width: 100%; left: 30px;" border="0" cellpadding="0" cellspacing="4" rules="all" id="AdvanceTBL" >
                            <tr style="width:50px;">
                                <th width="50" class="contenthead">S No&nbsp;</th>
                                <th class="contenthead"><font color='red'>*</font>Driver Name</th>
                                <th class="contenthead"><font color='red'>*</font>Account No</th>
                                <th class="contenthead"><font color='red'>*</font>Transaction Date &amp; Time</th>
                                <th class="contenthead"><font color='red'>*</font>Amount</th>
                            </tr>
                        </table>
                        <center>
                            &emsp;<input type="reset" class="button" value="Clear">
                            <input type="button" class="button" value="Add Row" name="Save" onClick="addAdvanceRow()">
                            <a  class="nexttab" href="#"><input type="button" class="button" value="Save &amp; Next" name="Save" /></a>
                        </center>
                        <br/>
                        <br/>
                    </div>
                </div>

                <div id="fuelExpDetail">
                    <div style="border: #ffffff solid">
                        <h4>Fuel Expense Details</h4>
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                                <td class="contenthead">S No</td>
                                <td class="contenthead">Bunk Name</td>
                                <td class="contenthead">Address</td>
                                <td class="contenthead">Date and Time</td>
                                <td class="contenthead">Liter</td>
                                <td class="contenthead">Amount</td>
                            </tr>
                            <tr>
                                <td class="text1">1</td>
                                <td class="text1">Madhava Agencies</td>
                                <td class="text1">Madhavaram Chennai</td>
                                <td class="text1">20-10-2013</td>
                                <td class="text1">30</td>
                                <td class="text1">1500</td>
                            </tr>
                            <tr>
                                <td class="text2">2</td>
                                <td class="text2">Guntur Enterprises</td>
                                <td class="text2">Gunter AP</td>
                                <td class="text2">21-10-2013</td>
                                <td class="text2">30</td>
                                <td class="text2">1500</td>
                            </tr>
                        </table>
                        <br>
                        <table class="border" style="width: 100%; left: 30px;" border="0" cellpadding="0" cellspacing="4" rules="all" id="FuelExpenseTBL" >
                            <tr style="width:50px;">
                                <th width="50" class="contenthead">S No&nbsp;</th>
                                <th class="contenthead"><font color='red'>*</font>Bunk Name</th>
                                <th class="contenthead">Address</th>
                                <th class="contenthead"><font color='red'>*</font>Expense Date</th>
                                <th class="contenthead"><font color='red'>*</font>Liter</th>
                                <th class="contenthead"><font color='red'>*</font>Amount</th>
                            </tr>
                        </table> 
                        <center>
                            &emsp;<input type="reset" class="button" value="Clear">
                            <input type="button" class="button" value="Add Row" name="save" onClick="addFuelExpenseRow()">
                            <a  class="nexttab" href="#"><input type="button" class="button" value="Save &amp; Next" name="Save" /></a>
                        </center>
                    </div>

                </div>
                <div id="otherExpDetail">
                    <div style="border: #ffffff solid">
                        <h4>Supplementary Expense Details</h4>
                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                            <tr>
                                <td class="contenthead">S No</td>
                                <td class="contenthead">Date</td>
                                <td class="contenthead">Description</td>
                                <td class="contenthead">Amount</td>
                            </tr>
                            <tr>
                                <td class="text1">1</td>
                                <td class="text1">20-10-2013</td>
                                <td class="text1">Food</td>
                                <td class="text1">200</td>
                            </tr>
                            <tr>
                                <td class="text2">2</td>
                                <td class="text2">21-10-2013</td>
                                <td class="text2">Food</td>
                                <td class="text2">200</td>
                            </tr>
                            <tr>
                                <td class="text2">3</td>
                                <td class="text2">21-10-2013</td>
                                <td class="text2">Air Check</td>
                                <td class="text2">50</td>
                            </tr>
                        </table>
                        <br>
                        <table class="border" style="width: 100%; left: 30px;" border="0" cellpadding="0" cellspacing="4" rules="all" id="suppExpenseTBL" >
                            <tr style="width:50px;">
                                <th width="50" class="contenthead">S No&nbsp;</th>
                                <th class="contenthead"><font color='red'>*</font>Expense Date</th>
                                <th class="contenthead">Expenses Details</th>
                                <th class="contenthead"><font color='red'>*</font>Amount</th>
                            </tr>
                        </table> 
                        <center>
                            &emsp;<input type="reset" class="button" value="Clear">
                            <input type="button" class="button" value="Add Row" name="save" onClick="addSuppExpenseRow()">
                            <a  class="nexttab" href="#"><input type="button" class="button" value="Save &amp; Next" name="Save" /></a>
                        </center>
                        <br/>
                        <br/>
                    </div>
                </div>

                <div id="tripSettlement">
                    <h2><center>TRIP SETTLEMENT</center></h2>
                    <br>
                    <br>
                    <h2>Advance Details</h2>
                    <br>
                    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                        <thead>

                            <tr >
                                <th width="34" class="contentsub">S.No</th>
                                <th width="100" class="contentsub">Trip Date</th>
                                <th width="150" class="contentsub">Vehicle No</th>
                                <th width="130" class="contentsub">Route Name</th>
                                <th width="130" class="contentsub">Created Date&Time</th>
                                <th width="130" class="contentsub">Driver Name</th>
                                <th width="170" class="contentsub">Account No</th>
                                <th width="170" class="contentsub">Amount</th>
                                <th width="130" class="contentsub">Remarks</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>12-03-2013</td>
                                <td>TN 12 AD 124</td>
                                <td>Chennai</td>
                                <td>12/10</td>
                                <td>Tamil</td>
                                <td>AD121213132</td>
                                <td>1000</td>
                                <td>NO</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>12-03-2013</td>
                                <td>TN 12 AD 124</td>
                                <td>Maduri</td>
                                <td>12/10</td>
                                <td>madhan</td>
                                <td>AD121213132</td>
                                <td>1000</td>
                                <td>NO</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>12-03-2013</td>
                                <td>TN 78 AD 124</td>
                                <td>Bangalore</td>
                                <td>12/10</td>
                                <td>Natarajan</td>
                                <td>AD12123232</td>
                                <td>1000</td>
                                <td>YES</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>31-07-2013</td>
                                <td>TN 23 ED 565</td>
                                <td>Kerala</td>
                                <td>12/10</td>
                                <td>Selvam</td>
                                <td>AD121213132</td>
                                <td>1000</td>
                                <td>YES</td>
                            </tr>
                            <tr>
                                <td colspan="8" >Total</td>
                                <td>4000</td>
                            </tr>

                        </tbody>
                    </table>
                    <br>
                    <br>
                    <br>

                    <h2>Expense Details</h2>

                    <br>
                    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                        <thead>

                            <tr >
                                <th width="34" class="contentsub">S.No</th>
                                <th width="100" class="contentsub">Trip Date</th>
                                <th width="150" class="contentsub">Vehicle No</th>
                                <th width="130" class="contentsub">Route Name</th>
                                <th width="130" class="contentsub">Created Date&Time</th>
                                <th width="130" class="contentsub">Driver Name</th>
                                <th width="170" class="contentsub">Expense Type</th>
                                <th width="170" class="contentsub">Amount</th>
                                <th width="130" class="contentsub">Remarks</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>12-03-2013</td>
                                <td>TN 12 AD 124</td>
                                <td>Chennai</td>
                                <td>12/10</td>
                                <td>Tamil</td>
                                <td>Cash</td>
                                <td>1000</td>
                                <td>NO</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>12-03-2013</td>
                                <td>TN 12 AD 124</td>
                                <td>Maduri</td>
                                <td>12/10</td>
                                <td>Madhan</td>
                                <td>Cash</td>
                                <td>1000</td>
                                <td>NO</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>12-03-2013</td>
                                <td>TN 78 AD 124</td>
                                <td>Bangalore</td>
                                <td>12/10</td>
                                <td>Natarajan</td>
                                <td>Cash</td>
                                <td>1000</td>
                                <td>YES</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>31-07-2013</td>
                                <td>TN 23 ED 565</td>
                                <td>Kerala</td>
                                <td>12/10</td>
                                <td>Selvam</td>
                                <td>Cash</td>
                                <td>1000</td>
                                <td>YES</td>
                            </tr>
                            <tr>
                                <td colspan="8" >Total</td>
                                <td>4000</td>
                            </tr>

                        </tbody>
                    </table>
                    <br>
                    <br>
                    <br>
                    <h2>Settlement Details</h2>
                    <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                        <thead>

                            <tr >
                                <th width="34" class="contentsub">S.No</th>
                                <th width="100" class="contentsub">Driver Name</th>
                                <th width="150" class="contentsub">Vehicle No</th>
                                <th width="130" class="contentsub">Total Advance Amount</th>
                                <th width="130" class="contentsub">Total Expense Amount</th>
                                <th width="130" class="contentsub">Balance Amount</th>

                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>1</td>
                                <td>Tamil</td>
                                <td>5000</td>
                                <td>2000</td>
                                <td>200</td>
                                <td>-20</td>

                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Selvam</td>
                                <td>6000</td>
                                <td>1000</td>
                                <td>1000</td>
                                <td>-10</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Natarajan</td>
                                <td>2000</td>
                                <td>1500</td>
                                <td>100</td>
                                <td>+300</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>madhan</td>
                                <td>8000</td>
                                <td>3000</td>
                                <td>2500</td>
                                <td>+750</td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <br>
                    <table align="center" width="200" border="0" id="table" class="sortable">
                        <tr>
                            <td><font color="red">*</font>Settlement Summary</td>
                            <td height="30"><input name="tadvance" id="tadvance" type="text" class="textbox" size="20" value="" autocomplete="off">
                            </td>

                        </tr>
                        <tr>
                            <td><font color="red">*</font>Total Expense</td>
                            <td height="30"><input name="tadvance" id="tadvance" type="text" class="textbox" size="20" value="" autocomplete="off">
                            </td>

                        </tr>
                        <tr>
                            <td><font color="red">*</font>Balance Amount</td>
                            <td height="30"><input name="tadvance" id="tadvance" type="text" class="textbox" size="20" value="(+) or (-)" autocomplete="off">
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <center>
                            <a  class="nexttab" href="#"><input type="button" class="button" value="Proceed Settlement &amp; Next" name="Save" /></a>
                        </center>
                        <br/>
                        <br/>
                </div>
                
                <div id="podUpdate" align="center">
                    <table border="1" class="border" align="center" width="500" cellpadding="0" cellspacing="0" id="POD1">
                        <tr>
                            <td class="contenthead" align="center" height="30" >Sno</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>POD No</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>POD Date</td>
                            <td class="contenthead" height="30" ><font color="red">*</font>Attachment</td>
                        </tr>
                    </table>
                    <center>
                        &emsp;<input type="reset" class="button" value="Clear">
                        <input type="button" class="button" value="Add Row" name="save" onClick="addPod1()">
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Save &amp; Next" name="Save" /></a>
                    </center>
                    <br>
                    <br>
                </div>
                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>
                
                <div id="summary" align="center">
                    <div id="print" >
                        <table cellpadding="0" cellspacing="4" border="0" width="80%" class="border">
                            <tr style="width:50px;">
                                <th colspan="8" class="contenthead">Remarks</th>
                            </tr>
                            <tr>
                                <td class="text2">Remarks</td>
                                <td class="text2"><textarea name="remarks"></textarea></td>
                            </tr>

                        </table>
                        <br/>

                        <center>
                            <input type="button" class="button" name="Save" value="Save" />
                            <input type="button" class="button" name="Print" value="Print" onClick="print('print');" > &nbsp;
                        </center>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>