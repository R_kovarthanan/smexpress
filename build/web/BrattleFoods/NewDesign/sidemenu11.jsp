
<%@page import="ets.domain.users.business.LoginTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<!--      <h5 class="sidebartitle">Navigation</h5>-->

<% 
    Object rolId = session.getAttribute("RoleId");
    String lastAccessTime = (String) request.getAttribute("LastAccessTime");
    String sessionPageParam = (String) request.getAttribute("sessionPageParam");
    String sessionPageParam1 = (String) session.getAttribute("sessionPageParam");
    System.out.println("exp = " + request.getAttribute("startTime"));
%>
<form>
<input type="hidden" name="lastAccessTime" value="<%=lastAccessTime%>" />
<input type="hidden" name="sessionPageParam" value="<%=sessionPageParam%>" />
<input type="hidden" name="sessionPageParam1" value="<%=sessionPageParam1%>" />
<ul class="nav nav-pills nav-stacked nav-bracket">
<!--    <li class="nav-parent"><a href="#"><i class="fa fa-dashboard"></i> <span><spring:message code="subMenus.label.Dashboard" text="default text"/></span></a>
        <ul class="children" >
            <li><a href="/throttle/dashboardOperation.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="dashboard.subMenus.label.Operations" text="default text"/></a></li>
            <li><a href="/throttle/dashboardWorkshop.do?menuClick=1"><i class="fa fa-cog"></i> <spring:message code="dashboard.subMenus.label.Workshop" text="default text"/></a></li>
        </ul>
    </li>-->
<!--    <li class="nav-parent"><a href=""><i class="fa fa-gears"></i> <span><spring:message code="subMenus.label.Settings" text="default text"/></span></a>
        <ul class="children">
            <li><a href="/throttle/alterPassword.do?menuClick=1" ><spring:message code="settings.subMenus.label.ChangePassword" text="default text"/></a></li>
            <li><a href="/throttle/recoverPassword.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManagePassword" text="default text"/></a></li>
            <li><a href="/throttle/viewuser.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageUsers" text="default text"/></a></li>
            <li><a href="/throttle/allroles.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageRoles" text="default text"/></a></li>
            <li><a href="/throttle/userroles.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageUserRoles" text="default text"/></a></li>
            <li><a href="/throttle/roleMenu.do?menuClick=1" ><spring:message code="settings.subMenus.label.ManageRoleFunctions" text="default text"/></a></li>

        </ul>
    </li>-->
    <% ArrayList menuList = (ArrayList) session.getAttribute("menuList");
        if (menuList != null) {
            Iterator itr = menuList.iterator();
            LoginTO loginTO = new LoginTO();
            while (itr.hasNext()) {
                loginTO = (LoginTO) itr.next();

    %>
    <li class="nav-parent"><a href=""><i class="<%=loginTO.getIconName()%>"></i> <span><spring:message code="<%=loginTO.getLabelName()%>" text="<%=loginTO.getDefaultLabelName()%>"/></span></a>
        <% ArrayList subMenuList = (ArrayList) loginTO.getUserSubMenuList();
            if (subMenuList.size() > 0) {
                Iterator itr1 = subMenuList.iterator();
                LoginTO logTO = new LoginTO();
                %>
                <ul class="children">
               <%     
                while (itr1.hasNext()) {
                    logTO = (LoginTO) itr1.next();
        %>
            <li><a href="/throttle/<%=logTO.getMethodName()%>?menuClick=1"><i class="<%=logTO.getSubMenuIconName()%>"></i><spring:message code="<%=logTO.getSubMenuLabelName()%>" text="<%=logTO.getSubMenuDefaultLabelName()%>"/> </a></li>
        <%
                }
            }
        %>
        </ul>
    </li>
    <%                }
        }

    %>

    <!-- =============================HR Section=============================================== -->
</ul>
<%@ include file="../common/NewDesign/commonParameters.jsp" %></form>


</div><!-- leftpanelinner -->
</div><!-- leftpanel -->

<div class="mainpanel">

    <div class="headerbar">

        <a class="menutoggle"><i class="fa fa-bars"></i></a>

        <!--      <form class="searchform" action="/throttle/dashboardOperation.do?menuClick=1" method="post">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
              <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>-->


        <div class="header-right">
            <ul class="headermenu">

                <li>
                    <div class="btn-group">
                        <span >
                            <a href="/throttle/languageChangeLoginRequest.do?paramName=en">english</a> &nbsp;|&nbsp;
                            <a href="/throttle/languageChangeLoginRequest.do?paramName=ar">العربية</a>
                        </span>
                    </div>
                </li>
                <li>
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-globe"></i>
                            <span class="badge">1</span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-head pull-right">
                            <h5 class="title"><spring:message code="sidemenu.label.YouHave5NewNotifications" text="default text"/></h5>
                            <ul class="dropdown-list gen-list">
                                <li class="new">
                                    <a href="">
                                        <span class="thumb"><img src="images/photos/user4.png" alt="" /></span>
                                        <span class="desc">
                                            <span class="name"><spring:message code="sidemenu.label.NewJobcard1960created" text="default text"/> <span class="badge badge-success"><spring:message code="sidemenu.label.new" text="default text"/></span></span>
                                        </span>
                                    </a>
                                </li>

                                <li class="new"><a href=""><spring:message code="sidemenu.label.SeeAllNotifications" text="default text"/></a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <!--                <img src="/throttle/content/NewDesign/images/photos/loggeduser.png" alt="" />-->
                            <spring:message code="sidemenu.label.WelcomeMr" text="default text"/>.<%= session.getAttribute("userName")%>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                            <!--                <li><a href="profile.html"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>-->
                            <li><a href="#"><i class="glyphicon glyphicon-cog"></i> <spring:message code="sidemenu.label.AccountSettings" text="default text"/></a></li>
                            <li><a href="#"><i class="glyphicon glyphicon-question-sign"></i> <spring:message code="sidemenu.label.Help" text="default text"/></a></li>
                            <li><a href="/throttle/logout.do?menuClick=1"><i class="glyphicon glyphicon-log-out"></i><spring:message code="sidemenu.label.LogOut" text="default text"/></a></li>
                        </ul>
                    </div>
                </li>

            </ul>
        </div><!-- header-right -->

    </div><!-- headerbar -->

    <!--sidemenu.label.LogOut-->