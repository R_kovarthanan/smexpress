<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="java.text.SimpleDateFormat"%>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });

            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>

    </head>
    <script language="javascript">
        function submitPage(){
            if(textValidation(document.approve.paidamt,'Paid Amount')){
                return;
            }
            
            document.approve.action = '/throttle/saveTripAmount.do';
            document.approve.submit();
            $("#saveButton").hide();
        }
        function setFocus(){
            document.approve.approveamt.focus();
        }
    </script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Finance Advice" text="Finance Advice"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
            <li class=""><spring:message code="hrms.label.Finance Advice" text="Finance Advice"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body onload="setFocus();">
        <form name="approve"  method="post" >
            <%
                        //request.setAttribute("menuPath", "Trip >> Payment");
                        String status = request.getParameter("status");
                        String tripid = request.getParameter("tripid");
                        String tripCode = request.getParameter("tripCode");
                        String customerName = request.getParameter("customerName");
                        String tripday = request.getParameter("tripday");
                        String advicedate = request.getParameter("advicedate");
                        String estimatedadvance = request.getParameter("estimatedadvance");
                        String requestedadvance = request.getParameter("requestedadvance");
                        String cnoteName = request.getParameter("cnoteName");
                        String vehicleTypeName = request.getParameter("vehicleTypeName");
                        String routeName = request.getParameter("routeName");
                        String driverName = request.getParameter("driverName");
                        String planneddate = request.getParameter("planneddate");
                        String estimatedexpense = request.getParameter("estimatedexpense");
                        String actualadvancepaid = request.getParameter("actualadvancepaid");
                        String vegno = request.getParameter("vegno");
                        String tripAdvaceId = request.getParameter("tripAdvaceId");

            %>
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table class="table table-info mb30 table-hover" id="table" >
                <thead>
                    <tr align="center">
                        <th colspan="2">Trip Payment</th>
                    </tr>                    
                </thead>

                <tr>
                    <td  height="30">Customer Name</td>
                    <td  height="30"><%=customerName%></td>
                </tr>
                <tr>
                    <td  height="30">Cnote Name</td>
                    <td  height="30"><%=cnoteName%></td>
                </tr>
                <tr>
                    <td  height="30">Vehicle Type</td>
                    <td  height="30"><%=vehicleTypeName%></td>
                </tr>
                <tr>
                    <td  height="30">Vehicle No</td>
                    <td  height="30"><%=vegno%></td>
                    <input type="hidden" name="tripCodeEmail" value='<%=tripCode%>'/>
                    <input type="hidden" name="vehicleNoEmail" value='<%=vegno%>'/>
                    <input type="hidden" name="routeInfoEmail" value='<%=routeName%>'/>
                    <input type="hidden" name="customerNameEmail" value='<%=customerName%>'/>
                    <input type="hidden" name="cNotesEmail" value='<%=cnoteName%>'/>
                    <input type="hidden" name="dateval" value='<c:out value="${dateval}"/>'/>
                    <input type="hidden" name="type" value='<c:out value="${type}"/>'/>
                    <input type="hidden" name="active" value='<c:out value="${active}"/>'/>
                    <input type="hidden" name="tripType" value='<c:out value="${tripType}"/>'/>
                </tr>
                <tr>
                    <td  height="30">Route Name</td>
                    <td  height="30"><%=routeName%></td>
                </tr>
                <tr style="display:none">
                    <td  height="30">Driver Name</td>
                    <td  height="30"><%=driverName%></td>
                </tr>
                <tr>
                    <td  height="30">Planned Date</td>
                    <td  height="30"><%=planneddate%></td>
                </tr>
                <tr>
                    <td  height="30">Actual Advance Paid</td>
                    <td  height="30"><%=actualadvancepaid%></td>
                </tr>

                <tr>
                    <td  height="30">Estimated Expense</td>
                    <td  height="30"><%=estimatedexpense%></td>
                </tr>

                <tr>
                    <td  height="30">Estimated Amount</td>
                    <%if (estimatedadvance.equals("")) {%>
                    <td  height="30">0</td>
                    <%} else {%>
                    <td  height="30"><%=estimatedadvance%></td>
                    <%}%>
                </tr>
                <%
                            String appramt = "";
                            if (status.equals("0")) {
                                appramt = estimatedadvance;
                %>
                <tr>
                    <td  height="30">To Be Paid </td>
                    <td  height="30"><%=estimatedadvance%></td>
                </tr>
                <%} else {
                                    appramt = requestedadvance;
                %>
                <tr>
                    <td  height="30">Approved Amount</td>
                    <td  height="30"><%=requestedadvance%></td>
                </tr>
                <%}%>
                <tr>
                    <td  height="30">Trip Day</td>
                    <td  height="30"><%=tripday%></td>
                </tr>
                <tr>
                    <td  height="30"><font color="red">*</font>Paid Amt.</td>
                    <td  height="30"><input name="paidamt" id="paidamt" type="text" onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" value="" onchange="checkamt()"></td>
                </tr>

                <input type="hidden" name="paidstatus" value="Y"/>
                <input name="tripday" type="hidden" class="textbox" value="<%=tripday%>" readonly>
                <input type="hidden" name="tripid" value="<%=tripid%>"/>
                <input type="hidden" name="tripAdvaceId" value="<%=tripAdvaceId%>"/>
                <input type="hidden" name="advicedate" value="<%=advicedate%>"/>
                <input name="tobepaidtoday" id="tobepaidtoday" type="hidden" class="textbox" value="<%=appramt%>" readonly>


            </table>
            <br>   
            <center>
                <input type="button" value="save" class="btn btn-success" id="saveButton" onClick="submitPage();">
                &emsp;
                <!--                <input type="reset" class="button" value="Clear">-->
            </center>


            <script>
                function checkamt(){
                    var reqvalue=document.getElementById("tobepaidtoday").value;                    
                    var paidamt=document.getElementById("paidamt").value;                    
                    var reqpnt = parseInt(reqvalue) * (10/100);
                    var allowvalue = parseInt(reqvalue)+parseInt(reqpnt);
                    
                    if( parseInt(paidamt) > parseInt(allowvalue)){
                        alert("You can pay only 10% max on the request amount.");
                        document.approve.paidamt.value=0;
                        document.getElementById("mySubmit").style.display = "none";
                        document.approve.advancerequestamt.focus();
                    }else{
                        document.getElementById("mySubmit").style.display = "block";
                    }

                }
            </script>
        </form>
    </body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
