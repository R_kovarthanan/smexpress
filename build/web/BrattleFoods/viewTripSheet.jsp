<%-- 
    Document   : viewTripSheet
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Throttle
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
            function submitWindow(val){
            document.tripSheet.action = '/throttle/BrattleFoods/viewTripSheet.jsp?reqFor='+val;
            document.tripSheet.submit();
            }
            <%
            String reqFor = "";
            if(request.getParameter("reqFor") != null && !"".equals(request.getParameter("reqFor"))){
            reqFor = request.getParameter("reqFor");
            if(reqFor != null){
            request.setAttribute("reqFor", reqFor);    
            }
            }
            %>
        </script>
    </head>
    <%
        String menuPath = "Trip Sheet >> View Trip Sheet";
        request.setAttribute("menuPath", menuPath);
    %>
    <body>
        <form name="tripSheet" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>
             <%@include file="/content/common/message.jsp" %>

            <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:900;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">View Trip Sheet</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">

                                    <tr>
                                        <td  height="25" >Vehicle Reg No</td>
                                        <td  height="25"><input name="regNo" type="text" class="textbox" value="" size="20" style="width: 110px"></td>
                                        <td  height="25" >Trip Sheet No</td>
                                        <td  height="25"><input name="regNo" type="text" class="textbox" value="" size="20" style="width: 110px"></td>
                                        <td  height="25" >Consignment No</td>
                                        <td  height="25"><input name="regNo" type="text" class="textbox" value="" size="20" style="width: 110px"></td>
                                    </tr>
                                    <tr>
                                        <td  height="25" >Customer</td>
                                        <td  height="25"><input name="regNo" type="text" class="textbox" value="" size="20" style="width: 110px"></td>
                                        <td width="80" height="30"><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td width="80" height="30"><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onClick="ressetDate(this);"></td>

                                        
                                    </tr>
                                    <tr>
                                        <td><input type="button"   value="Search" class="button" name="searchc" onClick="submitWindow('reqFor')">
                                            <input type="hidden" value="" name="reqfor"> </td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <% if(request.getAttribute("reqFor") != null && !"".equals(request.getAttribute("reqFor")) ){%>
            <br>
            <br>
            <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" >
                <tr>
                    <td class="table">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="table5">
                            <tr>
                                <td class="bottom" align="left"><img src="/throttle/images/left_status.jpg" alt=""  /></td>                                            
                                <td class="bottom" height="35" align="right"><img src="/throttle/images/icon_active.png" alt="" /></td>
                                <td class="bottom">&nbsp;<span style="font-size:16px; color:#3C0;" align="left">2</span></td>
                                <td class="bottom" align="right"><img src="/throttle/images/icon_closed.png" alt="" /></td>
                                <td class="bottom">&nbsp;<span style="font-size:16px; color:#F30;">1</span></td>
                                <td class="bottom" align="center"><h2>Total  3</h2></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                <thead>
                    <tr height="30">

                        <td class="contenthead">S.No</td>
                        <td class="contenthead">Customer</td>
                        <td class="contenthead">CNote No</td>
                        <td class="contenthead">Trip Id</td>
                        <td class="contenthead">Vehicle No</td>
                        <td class="contenthead">Vehicle Type</td>
                        <td class="contenthead">Route Name</td>
                        <td class="contenthead">Driver Name</td>
                        <td class="contenthead">Trip Start Date</td>
                        <td class="contenthead">Trip End Date</td>
                        <td class="contenthead">Out Km</td>
                        <td class="contenthead">In Km</td>
                        <td class="contenthead">Load Tonnage</td>
                        <td class="contenthead">Status</td>
                        <td class="contenthead">Actions</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text1" >1<img width="15px" height="15px" src="/throttle/images/flag2.gif" align="middle" border="0"></td>
                        <td class="text1" >M/s Doshi Foods</td>
                        <td class="text1" >CN/13-14/10023</td>
                        <td class="text1" ><a href="/throttle/BrattleFoods/tripSheet.jsp">TS/13-14/10045</a></td>
                        <td class="text1" >TN 02 AA 2025</td>
                        <td class="text1" >AL / 2516 / 12 PLT</td>
                        <td class="text1" >Delhi-Chennai</td>
                        <td class="text1" >Mr.Vinayagam</td>
                        <td class="text1" >07-11-2013</td>
                        <td class="text1" >10-11-2013</td>
                        <td class="text1" >125345</td>
                        <td class="text1" >&nbsp;</td>
                        <td class="text1" >25.00</td>
                        <td class="text1" ><img src="/throttle/images/icon_active.png" alt="" /></td>
                        <td class="text1" >
                            <a href="/throttle/BrattleFoods/tripSheetStart.jsp">Start</a>
                            &nbsp;
                            <a href="/throttle/BrattleFoods/tripSheetPOD.jsp">POD</a>
                            &nbsp;
                            <a href="/throttle/BrattleFoods/closeTripSheet.jsp">End</a>
                            &nbsp;
                            <a href="/throttle/BrattleFoods/tripSheetFinanceClose.jsp">FinanceClosure</a>
                            &nbsp;
                            <a href="/throttle/BrattleFoods/tripSheetSettlement.jsp">Settlement</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="text1" >2<img width="15px" height="15px" src="/throttle/images/flag2.gif" align="middle" border="0"></td>
                        <td class="text1" >M/s Bakers Circle</td>
                        <td class="text1" >CN/13-14/10321</td>
                        <td class="text1" ><a href="/throttle/BrattleFoods/tripSheet.jsp">TS/13-14/10925</a></td>
                        <td class="text1" >DL 22 SR 4578</td>
                        <td class="text1" >AL / 2516 / 12 PLT</td>
                        <td class="text1" >Kashipur - Delhi</td>
                        <td class="text1" >Mr.Gurmeet</td>
                        <td class="text1" >06-11-2013</td>
                        <td class="text1" >10-11-2013</td>
                        <td class="text1" >278549</td>
                        <td class="text1" >&nbsp;</td>
                        <td class="text1" >35.00</td>
                        <td class="text1" ><img src="/throttle/images/icon_active.png" alt="" /></td>
                        <td class="text1" >
                            <a href="/throttle/BrattleFoods/tripSheetStart.jsp">Start</a>
                            &nbsp;
                            <a href="/throttle/BrattleFoods/tripSheetPOD.jsp">POD</a>
                            &nbsp;
                            <a href="/throttle/BrattleFoods/closeTripSheet.jsp">End</a>
                            &nbsp;
                            <a href="/throttle/BrattleFoods/tripSheetFinanceClose.jsp">FinanceClosure</a>
                            &nbsp;
                            <a href="/throttle/BrattleFoods/tripSheetSettlement.jsp">Settlement</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="text1" >3<img width="15px" height="15px" src="/throttle/images/flag2.gif" align="middle" border="0"></td>
                        <td class="text1" >M/s Balaji & Co</td>
                        <td class="text1" >CN/13-14/10549</td>
                        <td class="text1" ><a href="/throttle/BrattleFoods/tripSheet.jsp">TS/13-14/10741</a></td>
                        <td class="text1" >DL 02 WE 7658</td>
                        <td class="text1" >TATA / 3118 / 16 PLT</td>
                        <td class="text1" >Delhi - Chandigarh</td>
                        <td class="text1" >Mr.Rajat</td>
                        <td class="text1" >03-11-2013</td>
                        <td class="text1" >04-11-2013</td>
                        <td class="text1" >225375</td>
                        <td class="text1" >225847</td>
                        <td class="text1" >29.00</td>
                        <td class="text1" ><img src="/throttle/images/icon_closed.png" alt="" /></td>
                        <td class="text1" >
                            <a href="/throttle/BrattleFoods/tripSheetStart.jsp">Start</a>
                            &nbsp;
                            <a href="/throttle/BrattleFoods/tripSheetPOD.jsp">POD</a>
                            &nbsp;
                            <a href="/throttle/BrattleFoods/closeTripSheet.jsp">End</a>
                            &nbsp;
                            <a href="/throttle/BrattleFoods/tripSheetFinanceClose.jsp">FinanceClosure</a>
                            &nbsp;
                            <a href="/throttle/BrattleFoods/tripSheetSettlement.jsp">Settlement</a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <%}%>
        </form>
    </body>
</html>
