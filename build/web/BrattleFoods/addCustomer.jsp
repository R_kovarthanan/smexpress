<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
    </head>
    <script language="javascript">
        function submitPage(){        
            if(textValidation(document.manufacturer.custName,'Customer name')){
                return;
            }
            if(textValidation(document.manufacturer.custContactPerson,'Contact Person')) {
  
                return;
            }
            if(textValidation(document.manufacturer.custAddress,'Customer Address')){
   
                return;
            }
            if(textValidation(document.manufacturer.custCity,'Customer City')) {
  
                return;
            }
            if(textValidation(document.manufacturer.custState,'Customer State')){
   
                return;
            }
            if(numberValidation(document.manufacturer.custPhone,'Customer Phone')){
   
                return;
            }
            if(document.manufacturer.custMobile.value!=""){
                if(numberValidation(document.manufacturer.custMobile,'Customer Mobile')){
   
                    return;
                }
            }else{
                document.manufacturer.custMobile.value=parseInt(0);
            }
            if(document.manufacturer.custEmail.value!="")
                if(isEmail(document.manufacturer.custEmail.value)){
                    document.manufacturer.custEmail.focus();
                }
            if(isSpecialCharacter(document.manufacturer.custEmail.value)){
   
                return;
            }  else{
                document.manufacturer.custEmail.value="-";
            }
            document.manufacturer.action = '/throttle/BrattleFoods/customerMaster.jsp?reqFor=showVal';
            document.manufacturer.submit();
        }
        function setFocus(){
            document.manufacturer.custName.focus();
        }
    </script>


    <body onload="setFocus();">
        <form name="manufacturer"  method="post" >
            <%
            request.setAttribute("menuPath","Customer >> Add Customer");
            %>
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="400" id="bg" class="border">
                <tr align="center">
                    <td colspan="2" align="center" class="contenthead" height="30"><div class="contenthead">Add Customer</div></td>
                </tr>
                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Customer Name</td>
                    <td class="text2" height="30"><input name="custName" type="text" class="textbox" value=""></td>
                </tr>
                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Billing Type</td>
                    <td class="text2" height="30">
                        <select class="textbox" name="customerType" style="width:125px;">
                            <option value="1" selected>Actual KMs</option>
                            <option value="2">Point to Point</option>
                            <option value="2">Point to Point, based on Weight</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Contactperson</td>
                    <td class="text1" height="30"><input name="custContactPerson" type="text" class="textbox" value=""></td>
                </tr>
                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Customer Address</td>
                    <td class="text2" height="30"><textarea class="textbox" name="custAddress"></textarea></td>
                </tr>
                <tr>
                    <td class="text1" height="30"><font color="red">*</font> City</td>
                    <td class="text1" height="30"><input name="custCity" type="text" class="textbox" value=""></td>
                </tr>

                <tr>
                    <td class="text2" height="30"><font color="red">*</font> State</td>
                    <td class="text2" height="30"><input name="custState" type="text" class="textbox" value=""></td>
                </tr>

                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Phone No</td>
                    <td class="text1" height="30"><input name="custPhone" type="text" class="textbox" value=""></td>
                </tr>
                <tr>
                    <td class="text2" height="30">&nbsp;&nbsp;Mobile No</td>
                    <td class="text2" height="30"><input name="custMobile" type="text" class="textbox" value=""></td>
                </tr>
                <tr>
                    <td class="text1" height="30">&nbsp;&nbsp;Email</td>
                    <td class="text1" height="30"><input name="custEmail" type="text" class="textbox" value=""></td>
                </tr>
            </table>
            <br>
            <center>
                <input type="button" value="Add" class="button" onClick="submitPage();">
                &emsp;<input type="reset" class="button" value="Clear">
            </center>
        </form>
    </body>
</html>
