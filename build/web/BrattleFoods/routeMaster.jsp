<%-- 
    Document   : routecreate
    Created on : Oct 28, 2013, 3:48:50 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% String menuPath = "Route  >>  New Route Creation";
        request.setAttribute("menuPath", menuPath);
        %>
        <form name="fuleprice"  method="post">
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="700" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                            </td></tr></table>
                    <!-- pointer table -->

                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <table width="980" align="center" class="table2" cellpadding="0" cellspacing="0">

                <tr>
                    <td class="contenthead" colspan="4" >Route  Creation</td>
                </tr>
                <tr>
                    <td class="text1">Route Code</td>
                    <td class="text1"><input type="text" name="routeCode" class="textbox" value="DL001"></td>
                    <td class="text1">&nbsp;</td>
                    <td class="text1">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text2">From Location</td>
                    <td class="text2"><input type="text" name="fromLocation" class="textbox"></td>
                    <td class="text2">To Location</td>
                    <td class="text2"><input type="text" name="toLocation" class="textbox"></td>
                </tr>
                <tr>
                    <td class="text1">Travel Time(Hrs)</td>
                    <td class="text1"><input type="text" name="travelTime" class="textbox"></td>
                    <td class="text1">KM</td>
                    <td class="text1"><input type="text" name="distance" class="textbox" onkeyup="calcVehExp(this.value)"></td>
                </tr>
                <tr>
                    <td class="text2">Reefer Running Hours</td>
                    <td class="text2"><input type="text" name="reeferRunning" class="textbox" onkeyup="calcReeferExp(this.value)"></td>
                    <td class="text2">Road Type</td>
                    <td class="text2"><select name="roadType">
                            <option value="0">--Select--</option>
                            <option value="NH">National Highway</option>
                            <option value="SH">State Highway</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="text1">Toll Amount (Rs)</td>
                    <td class="text1"><input type="text" id="tollAmount" name="tollAmount" value="0" class="textbox" onkeyup="calcTotExp(this.value)"></td>
                    <td class="text1">Current Fuel Cost</td>
                    <td class="text1"><input type="hidden" name="fuelCost" id="fuelCost" class="textbox" value="53" /><label>53 &nbsp;(Effective Date From 10-09-2013)</label></td>
                </tr>
                <tr>
                    <td colspan="4" class="text2">

                    <center>
                        <input type="submit" class="button" value=" Save " />
                    </center>
                    </td>
                </tr>
            </table>
            <script type="text/javascript">
                function calcVehExp(val){
                    var fuelCost = document.getElementById('fuelCost').value;
                    var vehMileage = document.getElementsByName('vehMileage');
                    var vehExpense = document.getElementsByName('vehExpense');
                    for(var i=0; i<vehMileage.length; i++){
                       var totLtr = parseInt(val)/parseInt(vehMileage[i].value);
                       var exp = parseInt(totLtr) * parseInt(fuelCost);
                       vehExpense[i].value = exp;
                    }
                }
                function calcReeferExp(val){
                     var fuelCost = document.getElementById('fuelCost').value;
                    var reeferConsump = document.getElementsByName('reeferConsump');
                    var reeferExpense = document.getElementsByName('reeferExpense');
                    for(var i=0; i<reeferConsump.length; i++){
                       var totLtr = parseInt(val)/parseInt(reeferConsump[i].value);
                       var exp = parseInt(totLtr) * parseInt(fuelCost);
                       reeferExpense[i].value = exp;
                    }
                }
                function calcTotExp(val){
                    if(val != null && val !=''){
                    var tollAmount = document.getElementById('tollAmount').value;
                    var vehExpense = document.getElementsByName('vehExpense');
                    var reeferExpense = document.getElementsByName('reeferExpense');
                    var totExpense = document.getElementsByName('totExpense');
                    var varExpense = document.getElementsByName('varCost');
                    for(var i=0; i<vehExpense.length; i++){
                       var tot = parseInt(reeferExpense[i].value)+parseInt(vehExpense[i].value) +parseInt(varExpense[i].value);
                       var totExp = parseInt(tot) + parseInt(tollAmount);
                       totExpense[i].value = totExp;
                    }

                    }
                }

               function calculateVarCost(val){
                   var km = document.fuleprice.distance.value;
                   if(val == 1){
                       var varRate = document.getElementById('varRate0').value;
                       var varCostVal = parseInt(varRate) * parseInt(km);
                       document.getElementById('varCost0').value = varCostVal;

                   }else{
                       var varRate = document.getElementById('varRate').value;
                       var varCostVal = parseInt(varRate) * parseInt(km);
                       document.getElementById('varCost').value = varCostVal;
                   }
                   calcTotExp(km);
               }
            </script>
            <br>
            <br>
            <br>
            <br>
            <table width="96%" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="40">
                        <th><h3>S.No</h3></th>
                        <th><h3>Vehicle Type </h3></th>
                        <th><h3>Vehicle Mileage Km/Ltr</h3></th>
                        <th><h3>Vehicle Expense Cost</h3></th>
                        <th><h3>Reefer Consumption Hrs/Ltr</h3></th>
                        <th><h3>Reefer Expense Cost</h3></th>
                        <th><h3>Variable Cost Rate(Rs./KM)</h3></th>
                        <th><h3>Variable Cost (Rs.)</h3></th>
                        <th><h3>Total Expense Cost</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <tr height="30">
                        <td align="left" class="text2">1</td>
                        <td align="left" class="text2">Ashok Leyland /2516 </td>
                        <td align="left" class="text2"><input type="hidden" name="vehMileage" id="vehMileage0" value="5"/>5</td>
                        <td align="left" class="textboxlabeltext2"><input type="text" name="vehExpense" id="vehExpense0" readonly/></td>
                        <td align="left" class="text2"><input type="hidden" name="reeferConsump" id="reeferConsump0" value="5"/>5</td>
                        <td align="left" class="textboxlabeltext2"><input type="text" name="reeferExpense" id="reeferExpense0" readonly/></td>
                        <td align="left" class="text2"><input type="text" name="varRate" id="varRate0" value="0" onKeyUp="calculateVarCost(1);"/></td>
                        <td align="left" class="textboxlabeltext2"><input type="text" name="varCost" id="varCost0" value="0" readonly/></td>
                        <td align="left" class="textboxlabeltext2"><input type="text" name="totExpense" id="totExpense0" readonly/></td>
                    </tr>
                    <tr height="30">
                        <td align="left" class="text2">2</td>
                        <td align="left" class="text2">Tata / 3118</td>
                        <td align="left" class="text2"><input type="hidden" name="vehMileage" id="vehMileage" value="3"/>3</td>
                        <td align="left" class="text2"><input type="text" name="vehExpense" id="vehExpense" readonly/></td>
                        <td align="left" class="text2"><input type="hidden" name="reeferConsump" id="reeferConsump" value="4"/>4</td>
                        <td align="left" class="text2"><input type="text" name="reeferExpense" id="reeferExpense" readonly/></td>
                        <td align="left" class="text2"><input type="text" name="varRate" id="varRate" value="0"  onKeyUp="calculateVarCost(2);" /></td>
                        <td align="left" class="text2"><input type="text" name="varCost" id="varCost"  value="0" readonly/></td>
                        <td align="left" class="text2"><input type="text" name="totExpense" id="totExpense" readonly/></td>
                    </tr>
                </tbody>
            </table>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table",1);
            </script>

            <br>
        </form>
    </body>
</html>
