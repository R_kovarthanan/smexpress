<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });
        </script>
        <script type="text/javascript" language="javascript">
            var poItems = 0;
            var rowCount = '';
            var snumber = '';
            function addAllowanceRow()
            {
                if (snumber < 19) {
                    var tab = document.getElementById("expenseTBL");
                    var rowCount = tab.rows.length;

                    snumber = parseInt(rowCount) - 1;

                    var newrow = tab.insertRow(parseInt(rowCount) - 1);
                    newrow.height = "30px";
                    // var temp = sno1-1;
                    var cell = newrow.insertCell(0);
                    var cell0 = "<td><input type='hidden'  name='itemId' /> " + snumber + "</td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(1);
                    cell0 = "<td class='text1'><input name='driName' type='text' class='textbox' id='driName' onkeyup='getDriverName(" + snumber + ")' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(2);
                    cell0 = "<td class='text1'><input name='cleanerName' type='text' class='textbox' id='cleanerName' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(3);
                    cell0 = "<td class='text1'><input name='date' type='text' id='date' class='datepicker' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    // rowCount++;

                    $(".datepicker").datepicker({
                        /*altField: "#alternate",
                         altFormat: "DD, d MM, yy"*/
                        changeMonth: true, changeYear: true
                    });
                }
                document.settle.expenseId.value = snumber;
            }

            function delAllowanceRow() {
                try {
                    var table = document.getElementById("expenseTBL");
                    rowCount = table.rows.length - 1;
                    for (var i = 2; i < rowCount; i++) {
                        var row = table.rows[i];
                        var checkbox = row.cells[6].childNodes[0];
                        if (null != checkbox && true == checkbox.checked) {
                            if (rowCount <= 1) {
                                alert("Cannot delete all the rows");
                                break;
                            }
                            table.deleteRow(i);
                            rowCount--;
                            i--;
                            sno--;
                        }
                    }
                    document.settle.expenseId.value = sno;
                } catch (e) {
                    alert(e);
                }
            }
            function parseDouble(value) {
                if (typeof value == "string") {
                    value = value.match(/^-?\d*/)[0];
                }
                return !isNaN(parseInt(value)) ? value * 1 : NaN;
            }
            function submitPage(obj) {
                if (obj.name == "search") {
                    var fromDate = document.settle.fromDate.value;
                    var toDate = document.settle.toDate.value;
                    //var regno=document.settle.regno.value;
                    var driName = document.settle.driName.value;
                    if (driName == "") {
                        alert("please enter the Driver Name");
                        document.settle.driName.focus();
                    } else if (fromDate == "") {
                        alert("please enter the From Date");
                        document.settle.fromDate.focus();
                    } else if (toDate == "") {
                        alert("please enter the To Date");
                        document.settle.toDate.focus();
                    } else {
                        document.settle.action = "/throttle/searchProDriverSettlement.do";
                        document.settle.submit();
                    }
                }
                /*alert("hi main: "+obj.name);
                 if(obj.name=="save"){
                 alert("hi");
                 alert(document.settle.expenseId.value);
                 document.settle.buttonName.value = "save";
                 obj.name="none";
                 if(document.settle.expenseId.value != ""){
                 document.settle.action='/throttle/saveDriverExpenses.do';
                 document.settle.submit();
                 }
                 }*/
                if (obj.name == "proceed") {
                    //alert("proceed");
                    document.settle.buttonName.value = "proceed";
                    obj.name = "none";
                    document.settle.action = '/throttle/saveDriverExpenses.do';
                    document.settle.submit();
                }
            }
            function saveExp(obj) {
                document.settle.buttonName.value = "save";
                obj.name = "none";
                if (document.settle.expenseId.value != "") {
                    document.settle.action = '/throttle/saveDriverExpenses.do';
                    document.settle.submit();
                }
            }

            function getDriverName(sno) {
                var oTextbox = new AutoSuggestControl(document.getElementById("driName"), new ListSuggestions("driName", "/throttle/handleDriverSettlement.do?"));

            }
        </script>
        <script language="">
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
            function calcTotalPacks(val) {
                var totVal = 0;
                var packagesNos = document.getElementsByName('packagesNos');
                for (var i = 0; i < packagesNos.length; i++) {
                    if (packagesNos[i].value != '') {
                        totVal += parseInt(packagesNos[i].value);
                    }
                }
                document.getElementById('totalPackages').innerHTML = totVal;
            }


            function calcTotalWeights(val) {
                var totVal = 0;
                var weights = document.getElementsByName('weights');
                for (var i = 0; i < weights.length; i++) {
                    if (weights[i].value != '') {
                        totVal += parseInt(weights[i].value);
                    }
                }
                document.getElementById('totalWeight').innerHTML = totVal;
            }
        </script>
    </head>

    <script type="text/javascript">
        var rowCount = 1;
        var sno = 0;
        var rowCount1 = 1;
        var sno1 = 0;
        var httpRequest;
        var httpReq;
        var styl = "";

        function addRow1() {
            if (parseInt(rowCount1) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            sno1++;
            var tab = document.getElementById("addTyres1");
            var newrow = tab.insertRow(rowCount1);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' > " + sno1 + "</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            // Positions
            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='estDate' class='textbox' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            // TyreIds
            var cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='estDate' class='textbox' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(3);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='packagesNos' id='packagesNos' class='textbox' value='' onkeyup='calcTotalPacks(this.value)'>";

            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(4);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='weights' id='weights' class='textbox' value='' onkeyup='calcTotalWeights(this.value)' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            rowCount1++;
        }


        //        totalPackages totalWeights
        function addRow() {
            if (parseInt(rowCount) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            sno++;
            var tab = document.getElementById("addTyres");
            var newrow = tab.insertRow(rowCount);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' > "+sno+"</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            // Positions
            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='25' >" +
                    "<select name='sartingPointIds' id='sartingPointIds" +sno+"'  class='textbox' > <option value='0'>-Select-</option>" +
                    "<option value='Salem' > Balwal  </option>" +
                    "<option value='Madurai' >Chandigarh</option>" +
                    "<option value='Delhi' >Delhi</option>" +
                    "<option value='Chennai' selected >Kashipur</option>" +
                    +"</select> </td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            // TyreIds
            var cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='25' >" +
                    "<select name='startingTypeIds' class='textbox'> <option value='0'>-Select-</option>" +
                    "<option value=pk  selected > Pickup </option>" +
                    "<option value=dp > Drop </option>" +
                    "<option value=pkdp > Pickup &amp; Drop </option>" +
                    +"</select> </td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(3);
            var cell1 = "<td class='text1' height='30' ><textarea id='stAddress"+sno+"' name='stAddress' row='3' cols='20' style='height: 50px; width:100px' ></textarea>";
            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;


            cell = newrow.insertCell(4);
            var cell1 = "<td class='text1' height='30' ><input type='text' id='stDates"+sno+"' name='stDates' class='datepicker' value='06-11-2013'>";
            cell1 = cell1 + "</td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(5);
            var cell0 = "<td class='text1' height='25' >" +
                    "<select id='stTimeIds"+sno+"' name='stTimeIds' class='textbox' >" +
                    "<option value='06:00'>6:00 AM</option>" +
                    "<option value='06:30'>6:30 AM</option>" +
                    "<option value='07:00'>7:00 AM</option>" +
                    "<option value='07:30'>7:30 AM</option>" +
                    "<option value='08:00'>8:00 AM</option>" +
                    "<option value='08:30'>8:30 AM</option>" +
                    "<option value='09:00'  selected>9:00 AM</option>" +
                    "<option value='09:30'>9:30 AM</option>" +
                    "<option value='10:00'>10:00 AM</option>" +
                    "<option value='10:30'>10:30 AM</option>" +
                    "<option value='11:00'>11:00 AM</option>" +
                    "<option value='11:30'>11:30 AM</option>" +
                    "<option value='12:00'>12:00 PM</option>" +
                    "<option value='12:30'>12:30 PM</option>" +
                    "<option value='13:00'>1:00 PM</option>" +
                    "<option value='13:30'>1:30 PM</option>" +
                    "<option value='14:00'>2:00 PM</option>" +
                    "<option value='14:30'>2:30 PM</option>" +
                    "<option value='15:00'>3:00 PM</option>" +
                    "<option value='15:30'>3:30 PM</option>" +
                    "<option value='16:00'>4:00 PM</option>" +
                    "<option value='16:30'>4:30 PM</option>" +
                    "<option value='17:00'>5:00 PM</option>" +
                    "<option value='17:30'>5:30 PM</option>" +
                    "<option value='18:00'>6:00 PM</option>" +
                    "<option value='18:30'>6:30 PM</option>" +
                    "<option value='19:00'>7:00 PM</option>" +
                    "<option value='19:30'>7:30 PM</option>" +
                    "<option value='20:00'>8:00 PM</option>" +
                    "<option value='20:30'>8:30 PM</option>" +
                    "<option value='21:00'>9:00 PM</option>" +
                    "<option value='21:30'>9:30 PM</option>" +
                    "<option value='22:00'>10:00 PM</option>" +
                    +" </select> </td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(6);
            var cell0 = "<td class='text1' height='25' >" +
                    "<select name='endingPointIds' id='endingPointIds"+sno+"'   class='textbox' onchange='resetAddRow("+sno+")'> <option value='0'>-Select-</option>" +
                    "<option value='Salem' > Balwal  </option>" +
                    "<option value='Madurai'  selected >Chandigarh</option>" +
                    "<option value='Delhi' >Delhi</option>" +
                    "<option value='Chennai' >Kashipur</option>" +
                    +"</select> </td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;


            cell = newrow.insertCell(7);
            var cell1 = "<td class='text1' height='30' ><textarea id='endAddress' name='endAddress' row='3' cols='20' style='height: 50px; width:100px' ></textarea>";
            cell1 = cell1 + "</td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;


            cell = newrow.insertCell(8);
            var cell2 = "<td class='text1' height='30' ><input type='text' id='endDates"+sno+"' name='endDates' value='07-11-2013' class='datepicker' onchange='calculateTravelHours("+sno+")'>";
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell2;

            cell = newrow.insertCell(9);
            var cell0 = "<td class='text1' height='25' >" +
                    "<select id='endTimeIds"+sno+"' name='endTimeIds' class='textbox' >" +
                    "<option value='06:00'>6:00 AM</option>" +
                    "<option value='06:30'>6:30 AM</option>" +
                    "<option value='07:00'>7:00 AM</option>" +
                    "<option value='07:30'>7:30 AM</option>" +
                    "<option value='08:00'>8:00 AM</option>" +
                    "<option value='08:30'>8:30 AM</option>" +
                    "<option value='09:00'  selected>9:00 AM</option>" +
                    "<option value='09:30'>9:30 AM</option>" +
                    "<option value='10:00'>10:00 AM</option>" +
                    "<option value='10:30'>10:30 AM</option>" +
                    "<option value='11:00'>11:00 AM</option>" +
                    "<option value='11:30'>11:30 AM</option>" +
                    "<option value='12:00'>12:00 PM</option>" +
                    "<option value='12:30'>12:30 PM</option>" +
                    "<option value='13:00'>1:00 PM</option>" +
                    "<option value='13:30'>1:30 PM</option>" +
                    "<option value='14:00'>2:00 PM</option>" +
                    "<option value='14:30'>2:30 PM</option>" +
                    "<option value='15:00'>3:00 PM</option>" +
                    "<option value='15:30'>3:30 PM</option>" +
                    "<option value='16:00'>4:00 PM</option>" +
                    "<option value='16:30'>4:30 PM</option>" +
                    "<option value='17:00'>5:00 PM</option>" +
                    "<option value='17:30'>5:30 PM</option>" +
                    "<option value='18:00'>6:00 PM</option>" +
                    "<option value='18:30'>6:30 PM</option>" +
                    "<option value='19:00'>7:00 PM</option>" +
                    "<option value='19:30'>7:30 PM</option>" +
                    "<option value='20:00'>8:00 PM</option>" +
                    "<option value='20:30'>8:30 PM</option>" +
                    "<option value='21:00'>9:00 PM</option>" +
                    "<option value='21:30'>9:30 PM</option>" +
                    "<option value='22:00'>10:00 PM</option>" +
                    +" </select> </td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(10);
            var cell2 = "<td class='text1' height='30' ><input type='text' id='distances"+sno+"' name='distances' class='textbox' value='450' readonly>";
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell2;
            cell = newrow.insertCell(11);
            var cell2 = "<td class='text1' height='30' ><input type='text' id='timeDifferences"+sno+"' name='timeDifferences' class='textbox' value='9:00' readonly>";
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell2;
            rowCount++;

            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });
        }
        
        
        function calculateTravelHours(sno){
        var endDates = document.getElementById('endDates'+sno).value;    
        var endTimeIds = document.getElementById('endTimeIds'+sno).value;  
        var tempDate1 = endDates.split("-");
        var tempTime1 = endTimeIds.split(":");
        var stDates = document.getElementById('stDates'+sno).value; 
        var stTimeIds = document.getElementById('stTimeIds'+sno).value; 
        var tempDate2 = stDates.split("-");
        var tempTime2 = stTimeIds.split(":");
        var prevTime = new Date(tempDate2[2],tempDate2[1],tempDate2[0],tempTime2[0],tempTime2[1]);  // Feb 1, 2011
        var thisTime = new Date(tempDate1[2],tempDate1[1],tempDate1[0],tempTime1[0],tempTime1[1]);              // now
        var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
        var hoursDifference = Math.floor(difference/1000/60/60);
        document.getElementById('timeDifferences'+sno).value = hoursDifference;
        }
        
        function goToImportPage() {
            document.settle.action = '/throttle/BrattleFoods/importCnoteDetails.jsp';
            document.settle.submit();
        }
    </script>
    <script>
    //endingPointIds
    function resetAddRow(sno){
        if(document.getElementById('endingPointIds'+sno).value == document.getElementById('destination').value){
//           addRow 
        document.getElementById('addRowDetails').style.display = 'none';
        }
    }
    </script>    
    <body onload="addRow();
            addRow1();
            addAllowanceRow()">
        <% String menuPath = "Operations >>  Create New Consignment Note";
            request.setAttribute("menuPath", menuPath);
        %>
        <form name="settle" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <table width="100%">
                <tr></tr>
            </table>
            <br>
            <br>
            <br>
            <table cellpadding="0" cellspacing="0" width="800" border="0px" class="border" align="center">
                <tr>
                    <td class="contenthead" colspan="4">Consignment Note </td>
                </tr>
                <tr>
                    <td class="text2">Entry Option</td>
                    <td class="text2"><input type="radio" name="entry" value="1" >Manual</td>
                    <td class="text2"><input type="radio" name="entry" value="2" onclick="goToImportPage()">Import</td>
                    <td class="text2" >&nbsp;</td>
                </tr>
                <tr>
                    <td class="text1">Consignment Note </td>
                    <td class="text1"><input type="text" name="cNoteNo" id="cNoteNo" value="CN001" readonly /></td>
                    <td class="text1">Consignment Note Date</td>
                    <td class="text1"><input name="tripDate" type="text" class="datepicker"  readonly="readonly" id="tripDate"  /></td>
                </tr>

                <tr>
                    <td class="text2">Customer Type</td>
                    <td class="text2">
                        <select name="customerType" id="customerType" class="textbox" style="width:120px;" onchange="showContract(this.value)">
                            <option value="1"> Contract </option>
                            <option value="2"> Third Party </option>
                            <option value="2"> Walk In </option>
                        </select>
                    </td>
                    <td class="text2">Product Category</td>
                    <td class="text2">
                        <select name="vehicleId" onchange="fillData()" id="vehicleId" class="textbox" style="width:120px;">
                            <option value="0"> -Select- </option>
                            <option value="2"> Beverages </option>
                            <option value="1"> Food Items </option>
                            <option value="5"> Perishables</option>
                        </select>
                    </td>
            </table>
            <br>
            <br>

            <div id="tabs">
                <ul>
                    <li><a href="#customerDetail"><span>Basic Details</span></a></li>
<!--                    <li><a href="#routeDetail"><span>Route Course</span></a></li>-->
                    <li><a href="#paymentDetails"><span>Invoice Info</span></a></li>
                </ul>

                <div id="customerDetail">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="4" >Customer Details</td>
                        </tr>
                        <tr>
                            <td class="text1">Name</td>
                            <td class="text1"><input name="customerName" type="text" class="textbox"  id="customerName" onchange="showContract(this.value)" /></td>
                            <td class="text1">Code</td>
                            <td class="text1"><input name="customerCode" type="text" class="textbox"  id="customerCode" /></td>
                        </tr>
                        <tr>
                            <td class="text2">Address</td>
                            <td class="text2"><textarea rows="1" cols="16" ></textarea></td>
                            <td class="text2">Pincode</td>
                            <td class="text2"><input name="pincode" type="text"  class="textbox"  id="pincode" /></td>
                        </tr>
                        <tr>
                            <td class="text1">Mobile No</td>
                            <td class="text1"><input name="mobileNo" type="text"  class="textbox"  id="mobileNo" /></td>
                            <td class="text1">E-Mail ID</td>
                            <td class="text1"><input name="mailId" type="text" class="textbox"  id="mailId" /></td>
                        </tr>
                        <tr>
                            <td class="text2">Phone No</td>
                            <td class="text2"><input name="phoneNo" type="text"  class="textbox" maxlength="10"   id="phoneNo" /></td>
                            
                            <td class="text2">Billing Type</td>
                            <td class="text2"> Actual KMs</td>

                        </tr>

                    </table>
                    <script>
                        function showContract(val) {
                            if (val != '' && document.getElementById('customerType').value == '1') {
                                document.getElementById('contractDetails').style.display = 'block';
                            } else {
                                document.getElementById('contractDetails').style.display = 'none';
                            }
                        }
                    </script>
                    <div id="contractDetails" style="display: none">
                        <table>
                            <tr>
                                <td class="text1">Contract No :</td>
                                <td class="text1"><b>BF4567</b></td>
                                <td class="text1">Contract Expiry Date :</td>
                                <td class="text1"><font color="green"><b>10-10-2014</b></font></td>
                                <td class="text1">View Contract :</td>
                                <td class="text1"><a href="/throttle/BrattleFoods/customerContractMaster.jsp"><b>View Contract</b></a></td>
                            </tr>
                        </table>
                    </div>
                    <br>
                    <br>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">

                        <tr>
                            <td class="contenthead" colspan="6" >Consignment Details</td>
                        </tr>
                        <tr>
                            <td class="text1">Origin</td>
                            <td class="text1"><select id="origin" name="origin" onchange='originDetais()'> <option value="0">-Select-</option>
                                    
                                    <option value='Balwal' >Balwal</option>
                                    <option value='Chandigarh' >Chandigarh</option>
                                    <option value='Delhi' >Delhi</option>
                                    <option value='Kashipur' > Kashipur  </option>
                                </select></td>
                            <td class="text1">Destination</td>
                            <td class="text1"><select class="textbox" id="destination" name="destination" onchange='destinationDetais()'> <option value="0">-Select-</option>
                                    <option value='Balwal' >Balwal</option>
                                    <option value='Chandigarh' >Chandigarh</option>
                                    <option value='Delhi' >Delhi</option>
                                    
                                </select></td>
<!--                            <td class="text1">Business Type</td>
                            <td class="text1">
                                <select name="businessType" class="textbox"  id="businessType" style="width:120px;">
                                    <option value="0"> --Select-- </option>
                                    <option value="1"> Primary  </option>
                                    <option value="2"> Secondary  </option>
                                </select>
                            </td>-->
                        </tr>
                        <script type="text/javascript">
                            function originDetais() {
                                document.getElementById('originDetails').innerHTML = document.getElementById('origin').value;
                                var sartingPointIds = document.getElementsByName('sartingPointIds');
                                for (var i = 0; i < sartingPointIds.length; i++) {
//                                    if (sartingPointIds[i].value != '') {
//                                        totVal += parseInt(sartingPointIds[i].value);
//                                    }
                                }
                                sartingPointIds[0].value = document.getElementById('origin').value;
                            }
                            function destinationDetais() {
                                document.getElementById('destinationDetails').innerHTML = document.getElementById('destination').value;
                                var endingPointIds = document.getElementsByName('endingPointIds');
                                for (var i = 0; i < endingPointIds.length; i++) {
//                                    if (sartingPointIds[i].value != '') {
//                                        totVal += parseInt(sartingPointIds[i].value);
//                                    }
                                }
                                //endingPointIds[0].value = document.getElementById('destination').value;
                            }
                            
                        </script>
                        <tr>
                            <td class="text2">Multi Pickup</td>
                            <td class="text2"><input type="checkbox" class="textbox" name="multiPickup" id="multiPickup" onclick="multipickupShow()"></td>
                            <td class="text2">Multi Delivery</td>
                            <td class="text2"><input type="checkbox" class="textbox" name="multiDelivery" id="multiDelivery" ></td>
                            <td class="text2">Special Instruction</td>
                            <td class="text2"><textarea rows="1" cols="16"></textarea></td>
                        </tr>
                        <tr>
                            <td colspan="4" >

                                <table border="0" class="border" align="left" width="700" cellpadding="0" cellspacing="0" id="addTyres1">
                                    <tr>
                                        <td width="20" class="contenthead" align="center" height="30" >Sno</td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>Product/Article Code</td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>Product/Article Name </td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>No of Packages</td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>Total Weight (in Kg)</td>
                                    </tr>
                                    <br>

                                    <tr>
                                        <td colspan="5" align="center">
                                            &nbsp;&nbsp;&nbsp;<input type="reset" class="button" value="Clear">
                                            &emsp;<input type="button" class="button" value="Add Row" name="save" onClick="addRow1()">

                                        </td>
                                    </tr>
                                </table>
                                <br>
                                <br>
                            </td>
                            <td colspan="2">
                                <table border="0"  align="right" width="300" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td colspan="2" height="100">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="contentsub">Total No Packages</label>
                                            <label id="totalPackages">0</label>
                                        </td>
                                        <td>
                                            <label class="contentsub">Total Weight (Kg)</label>
                                            <label id="totalWeight">0</label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">

                        <tr>
                            <td class="contenthead" colspan="6" >Vehicle (Required) Details</td>
                        </tr>

                        <tr>
<!--                            <td class="text1">Service Type</td>
                            <td class="text1">
                                <select name="serviceType" class="textbox"  id="paymentType" style="width:120px;">
                                    <option value="0"> -Select- </option>
                                    <option value="1"> FTL </option>
                                    <option value="2"> LTL </option>
                                </select>
                            </td>-->
                            <td class="text1">Vehicle Type</td>
                            <td class="text1"> <select name="paymentType" class="textbox"  id="paymentType" style="width:120px;">
                                    <option value="0"> -Select- </option>
                                    <option value="1"> Ashok Leyland/2516 </option>
                                    <option value="2"> Tata/3118 </option>
                                </select></td>
                            <td class="text1">Reefer Required</td>
                            <td class="text1">
                                <select name="paymentType" class="textbox"  id="paymentType" style="width:120px;">
                                    <option value="0"> Yes </option>
                                    <option value="1"> No </option>
                                </select>
                            </td>
                        </tr>
                        <tr>

                            <td class="text2">Vehicle Required Date</td>
                            <td class="text2"><input type="textbox" class="datepicker" name="vehcleRequiredDate" id="vehcleRequiredDate" onchange='selectVehicleStDate(this.value)'></td>
                        <script type="text/javascript">
                            function selectVehicleStDate(val) {
                                var stDates = document.getElementsByName('stDates');
                                for (var i = 0; i < stDates.length; i++) {
                                    //                                    if (sartingPointIds[i].value != '') {
                                    //                                        totVal += parseInt(sartingPointIds[i].value);
                                    //                                    }
                                }
                                stDates[0].value = document.getElementById('vehcleRequiredDate').value;
                            }
                            function selectVehicleStTime(val) {
                                var stTimeIds = document.getElementsByName('stTimeIds');
                                for (var i = 0; i < stTimeIds.length; i++) {
                                    //                                    if (sartingPointIds[i].value != '') {
                                    //                                        totVal += parseInt(sartingPointIds[i].value);
                                    //                                    }
                                }
                                stTimeIds[0].value = document.getElementById('vehicleRequiredTime').value;
                            }
                        </script>
                        <td class="text2">Vehicle Required Time</td>
                        <td class="text2"><select id='vehicleRequiredTime' onchange='selectVehicleStTime(this.value)'>
                                <option value="06:00">6:00 AM</option>
                                <option value="06:30">6:30 AM</option>
                                <option value="07:00">7:00 AM</option>
                                <option value="07:30">7:30 AM</option>
                                <option value="08:00">8:00 AM</option>
                                <option value="08:30">8:30 AM</option>
                                <option value="09:00"  selected>9:00 AM</option>
                                <option value="09:30">9:30 AM</option>
                                <option value="10:00">10:00 AM</option>
                                <option value="10:30">10:30 AM</option>
                                <option value="11:00">11:00 AM</option>
                                <option value="11:30">11:30 AM</option>
                                <option value="12:00">12:00 PM</option>
                                <option value="12:30">12:30 PM</option>
                                <option value="13:00">1:00 PM</option>
                                <option value="13:30">1:30 PM</option>
                                <option value="14:00">2:00 PM</option>
                                <option value="14:30">2:30 PM</option>
                                <option value="15:00">3:00 PM</option>
                                <option value="15:30">3:30 PM</option>
                                <option value="16:00">4:00 PM</option>
                                <option value="16:30">4:30 PM</option>
                                <option value="17:00">5:00 PM</option>
                                <option value="17:30">5:30 PM</option>
                                <option value="18:00">6:00 PM</option>
                                <option value="18:30">6:30 PM</option>
                                <option value="19:00">7:00 PM</option>
                                <option value="19:30">7:30 PM</option>
                                <option value="20:00">8:00 PM</option>
                                <option value="20:30">8:30 PM</option>
                                <option value="21:00">9:00 PM</option>
                                <option value="21:30">9:30 PM</option>
                                <option value="22:00">10:00 PM</option>
                            </select></td>
                        <td class="text2">Special Instruction</td>
                        <td class="text2"><textarea rows="1" cols="16"></textarea></td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6">Consignor Details</td>
                        </tr>
                        <tr>
                            <td class="text2">Consignor Name</td>
                            <td class="text2"><input type="text" class="textbox" id="consignorName" name="consignorName" ></td>
                            <td class="text2">Mobile No</td>
                            <td class="text2"><input type="text" class="textbox" id="phoneNo" name="phoneNo" ></td>
                            <td class="text2">Address</td>
                            <td class="text2"><textarea rows="1" cols="16"></textarea> </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6">Consignee Details</td>
                        </tr>
                        <tr>
                            <td class="text2">Consignee Name</td>
                            <td class="text2"><input type="text" class="textbox" id="consignorName" name="consignorName" ></td>
                            <td class="text2">Mobile No</td>
                            <td class="text2"><input type="text" class="textbox" id="phoneNo" name="phoneNo" ></td>
                            <td class="text2">Address</td>
                            <td class="text2"><textarea rows="1" cols="16"></textarea> </td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    <center>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Save &amp; Next" name="Save" ></a>
                    </center>
                </div>

<!--                <div id="routeDetail">
                    <table border="0" align="center" width="980" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td>
                                <label class="contentsub">Origin :</label>
                                <label id='originDetails'></label> 
                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <label class="contentsub">Destination:</label>
                                <label id='destinationDetails'></label> 
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <table border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" id="addTyres">
                        <tr >
                            <td width="20" class="contenthead" align="center" height="30" >Sno</td>
                            <td class="contenthead" height="40" ><font color="red">*</font>Start Point</td>
                            <td class="contenthead" height="40" ><font color="red">*</font>Type</td>
                            <td class="contenthead" height="40" ><font color="red">*</font>St.Address</td>
                            <td class="contenthead" height="40" ><font color="red">*</font>St.Date</td>
                            <td class="contenthead" height="40" ><font color="red">*</font>St.Time</td>
                            <td class="contenthead" height="40" ><font color="red">*</font>End Point</td>
                            <td class="contenthead" height="40" ><font color="red">*</font>Type</td>
                            <td class="contenthead" height="40" ><font color="red">*</font>E.Address</td>
                            <td class="contenthead" height="40" ><font color="red">*</font>E.Date</td>
                            <td class="contenthead" height="40" ><font color="red">*</font>E.Time</td>
                            <td class="contenthead" height="40" ><font color="red">*</font>Total Distance (KM)</td>
                            <td class="contenthead" height="40" ><font color="red">*</font>Travel Hours</td>
                        </tr>
                    </table>
                    <center>
                        &emsp;<input type="reset" class="button" value="Clear">
                        &emsp;<input type="button" class="button" value="Add Row" name="save" id="addRowDetails" onClick="addRow()">
                        &emsp;<a  class="nexttab" href="#"><input type="button" class="button" value="Save &amp; Next" name="Save" /></a>
                    </center>
                </div>-->
                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>
                <div id="paymentDetails">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6">Freight Details</td>
                        </tr>
                        <tr>
                            <td class="text1">Billing Type</td>
                            <td class="text1">Actual KMs</td>
                            <td class="text1">Freight Charges</td>
                            <td class="text1">INR. 1,23,450.00</td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6">Standard / Additional Charges</td>
                        </tr>
                        <tr>
                            <td class="text1">Document Charges</td>
                            <td class="text1"><input type="text" class="textbox" id="docCharges" name="docCharges" ></td>
                            <td class="text1">ODA Charges</td>
                            <td class="text1"><input type="text" class="textbox" id="odaCharges" name="odaCharges" ></td>
                            <td class="text1">Multi Pickup Charges</td>
                            <td class="text1"><input type="text" class="textbox" id="multiPickup" name="multiPickup" ></td>
                        </tr>
                        <tr>
                            <td class="text2">Multi Delivery Charges</td>
                            <td class="text2"><input type="text" class="textbox" id="multiDelivery" name="multiDelivery" ></td>
                            <td class="text2">Handling Charges</td>
                            <td class="text2"><input type="text" class="textbox" id="handleCharges" name="handleCharges" ></td>
                            <td class="text2">Other Charges</td>
                            <td class="text2"><input type="text" class="textbox" id="otherCharges" name="otherCharges" ></td>
                        </tr>
                        <tr>                            
                            <td class="text1">Unloading Charges</td>
                            <td class="text1"><input type="text" class="textbox" id="unloadingCharges" name="unloadingCharges" ></td>
                            <td class="text1">Loading Charges</td>
                            <td class="text1"><input type="text" class="textbox" id="loadingCharges" name="loadingCharges" ></td>
                            <td class="text1">Remarks</td>
                            <td class="text1"><textarea rows="3" cols="20"></textarea></td>
                        </tr>
                        <tr>
                            <td class="text2" colspan="4"></td>
                            <td class="text2">Sub Total</td>
                            <td class="text2"><input type="text" class="textbox" id="fuelCharges" name="fuelCharges" ></td>
                        </tr>
                    </table>
                    <br/>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="60">&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td class="contentsub"  height="30" colspan="6" align="right">Total Charges</td>
                            <td class="contentsub"  height="30" align="right">INR.<input align="right" value="1,23,450.00" type="text" readonly class="textbox" id="fuelCharges" name="fuelCharges" ></td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    
                    <center>
                        <input type="button" class="button" name="Save" value="Save" >
                    </center>
                </div>

            </div>
        </form>
    </body>
</html>