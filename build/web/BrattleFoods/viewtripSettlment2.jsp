<%-- 
    Document   : viewtripSettlment2
    Created on : Oct 14, 2013, 1:50:43 PM
    Author     : srinivasan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>


 <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
        </head>
        <body>
        <form name="customerWise" method="post">
             <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>


            <h2><center>TRIP SETTLEMENT</center></h2>
            <br>
            <br>
            <h2>Advance Details</h2>
            <br>
               <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
    <thead>

                    <tr >
                        <th width="34" class="contentsub">S.No</th>
                        <th width="100" class="contentsub">Trip Date</th>
                        <th width="150" class="contentsub">Vehicle No</th>
                        <th width="130" class="contentsub">Route Name</th>
                        <th width="130" class="contentsub">Created Date&Time</th>
                        <th width="130" class="contentsub">Driver Name</th>
                        <th width="170" class="contentsub">Account No</th>
                        <th width="170" class="contentsub">Amount</th>
                        <th width="130" class="contentsub">Remarks</th>
                        
                    </tr>
             </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>12-03-2013</td>
                        <td>TN 12 AD 124</td>
                        <td>Chennai</td>
                        <td>12/10</td>
                        <td>Tamil</td>
                        <td>AD121213132</td>
                        <td>1000</td>
                        <td>NO</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>12-03-2013</td>
                        <td>TN 12 AD 124</td>
                        <td>Maduri</td>
                        <td>12/10</td>
                        <td>Tamil</td>
                        <td>AD121213132</td>
                        <td>1000</td>
                        <td>NO</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>12-03-2013</td>
                        <td>TN 78 AD 124</td>
                        <td>Bangalore</td>
                        <td>12/10</td>
                        <td>Natarajan</td>
                        <td>AD12123232</td>
                        <td>1000</td>
                        <td>YES</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>31-07-2013</td>
                        <td>TN 23 ED 565</td>
                        <td>Kerala</td>
                        <td>12/10</td>
                        <td>Selvam</td>
                        <td>AD121213132</td>
                        <td>1000</td>
                        <td>YES</td>
                    </tr>
                    
                </tbody>
            </table>
            <br>
            <br>
            <br>

<h2>Expense Details</h2>
            
            <br>
            <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
    <thead>

                    <tr >
                        <th width="34" class="contentsub">S.No</th>
                        <th width="100" class="contentsub">Trip Date</th>
                        <th width="150" class="contentsub">Vehicle No</th>
                        <th width="130" class="contentsub">Route Name</th>
                        <th width="130" class="contentsub">Created Date&Time</th>
                        <th width="130" class="contentsub">Driver Name</th>
                        <th width="170" class="contentsub">Expense Type</th>
                        <th width="170" class="contentsub">Amount</th>
                        <th width="130" class="contentsub">Remarks</th>

                    </tr>
             </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>12-03-2013</td>
                        <td>TN 12 AD 124</td>
                        <td>Chennai</td>
                        <td>12/10</td>
                        <td>Tamil</td>
                        <td>Cash</td>
                        <td>1000</td>
                        <td>NO</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>12-03-2013</td>
                        <td>TN 12 AD 124</td>
                        <td>Maduri</td>
                        <td>12/10</td>
                        <td>Madhan</td>
                        <td>Cash</td>
                        <td>1000</td>
                        <td>NO</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>12-03-2013</td>
                        <td>TN 78 AD 124</td>
                        <td>Bangalore</td>
                        <td>12/10</td>
                        <td>Natarajan</td>
                        <td>Cash</td>
                        <td>1000</td>
                        <td>YES</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>31-07-2013</td>
                        <td>TN 23 ED 565</td>
                        <td>Kerala</td>
                        <td>12/10</td>
                        <td>Selvam</td>
                        <td>Cash</td>
                        <td>1000</td>
                        <td>YES</td>
                    </tr>

                </tbody>
            </table>
			<br>
			<br>
			<table align="center" width="200" border="0" id="table" class="sortable">
                                      <tr>
                                        <td><font color="red">*</font>Settlement Summary</td>
                                        <td height="30">
                                            <input name="tadvance" id="tadvance" type="text" class="textbox" size="20" value="" autocomplete="off">
                                        </td>

                                    </tr>
									<tr>
                                        <td><font color="red">*</font>Total Expense</td>
                                        <td height="30">
                                            <input name="tadvance" id="tadvance" type="text" class="textbox" size="20" value="" autocomplete="off">
                                        </td>

                                    </tr>
									<tr>
                                        <td><font color="red">*</font>Balance Amount</td>
                                        <td height="30">
                                            <input name="tadvance" id="tadvance" type="text" class="textbox" size="20" value="(+) or (-)" autocomplete="off">
                                        </td>

                                    </tr>
		   </table>
		   <br>
		   <br>


                   <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
    <thead>

                    <tr >
                        <th width="34" class="contentsub">S.No</th>
                        <th width="100" class="contentsub">Driver Name</th>
                        <th width="150" class="contentsub">Vehicle No</th>
                        <th width="130" class="contentsub">Total Advance Amount</th>
                        <th width="130" class="contentsub">Total Expense Amount</th>
                        <th width="130" class="contentsub">Balance Amount</th>

                    </tr>
             </thead>
			 <tbody>
                   
                    <tr>
                        <td>1</td>
                        <td>Tamil</td>
                        <td>5000</td>
                        <td>2000</td>
                        <td>200</td>
                        <td>20</td>

                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Selvam</td>
                        <td>6000</td>
                        <td>1000</td>
                        <td>1000</td>
                        <td>10</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Natarajan</td>
                        <td>2000</td>
                        <td>1500</td>
                        <td>100</td>
                        <td>300</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>madhan</td>
                        <td>8000</td>
                        <td>3000</td>
                        <td>2500</td>
                        <td>750</td>
                    </tr>

                  </tbody>
				</table>
                   <br>
                   <br>
        </form>
            </body>
</html>
