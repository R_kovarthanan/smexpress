<%@ include file="/content/common/NewDesign/header.jsp" %>
	<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
        
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script type="text/javascript">


    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#zoneName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getZoneName.do",
                    dataType: "json",
                    data: {
                       zoneName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                            $('#zoneId').val('');
                            $('#zoneName').val('');
                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split();
                $('#zoneId').val(tmp[0]);
                $('#zoneName').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
        });

    function citysubmit()
    {
        var errStr = "";
        var nameCheckStatus = $("#cityNameStatus").text();
        if(document.getElementById("cityName").value == ""){
            errStr = "Please enter cityName.\n";
            alert(errStr);
            document.getElementById("cityName").focus();
        }else if(nameCheckStatus != "") {
            errStr ="City Name Already Exists.\n";
            alert(errStr);
            document.getElementById("cityName").focus();
        }else if(document.getElementById("zoneName").value == ""){
            errStr ="Please select valid zoneName.\n";
            alert(errStr);
            document.getElementById("zoneName").focus();
        }else if(document.getElementById("zoneName").value != "" && document.getElementById("zoneId").value == ""){
            errStr ="Please select valid zoneName.\n";
            alert(errStr);
            document.getElementById("zoneName").focus();
        }
        if(errStr == "") {
        document.cityMaster.action="/throttle/saveCityMaster.do";
        document.cityMaster.method="post";
        document.cityMaster.submit();
        }
    }

    function setValues(sno,cityId,cityName,zoneName,zoneId,status,googleCityName,latitude,longitude){
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if(i != sno) {
                document.getElementById("edit"+i).checked = false;
            } else {
                document.getElementById("edit"+i).checked = true;
            }
        }
        document.getElementById("cityId").value = cityId;
        document.getElementById("cityName").value = cityName;
        document.getElementById("zoneName").value = zoneName;
        document.getElementById("zoneId").value = zoneId;
        document.getElementById("status").value = status;
        document.getElementById("googleCityName").value = googleCityName;
        document.getElementById("latitude").value = latitude;
        document.getElementById("longitude").value = longitude;
    }
//
//
//
//
//
//
    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }

var httpRequest;
function checkCityName() {

        var cityName = document.getElementById('cityName').value;

        
            var url = '/throttle/checkCityName.do?cityName=' + cityName ;
           if (window.ActiveXObject) {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest();
            };
            httpRequest.send(null);
      
    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#cityNameStatus").text('Please Check City Name  :' + val+' is Already Exists');
                } else {
                     $("#nameStatus").hide();
                    $("#cityNameStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }



 
</script>
<script type="text/javascript">
    var source, destination;
    var directionsDisplay;
    var options = {
        types: ['(cities)'],
        componentRestrictions: {country: "india"}
    };
    var directionsService = new google.maps.DirectionsService();
    google.maps.event.addDomListener(window, 'load', function() {
        new google.maps.places.Autocomplete(document.getElementById('googleCityName', options));
        directionsDisplay = new google.maps.DirectionsRenderer({'draggable': true});
//        setCityName();
        getLatLng();
    });
    function setCityName() {
        // alert("hau");
        var tempdestination = document.getElementById("googleCityName").value;

        var tempdestination1 = tempdestination.split(",");
        //  var tempdestination2=tempdestination1.split("-");
        // alert(tempdestination2[0]);
        //  var tempdestination2 = tempdestination1.split("-");
        document.getElementById("googleCityName").value = tempdestination1[0];
//        document.getElementById("cityId1").value = tempdestination1[0];
        getLatLng();
    }

    function initialize(lat, lng, address) {
        var mapOptions = {
            center: new google.maps.LatLng(lat, lng),
            zoom: 4,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("myMap"),
                mapOptions);

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            title: address
        });

        marker.setMap(map);

        var infotext = address + '<hr>' +
                'Latitude: ' + lat + '<br>Longitude: ' + lng;
        var infowindow = new google.maps.InfoWindow();
        infowindow.setContent(infotext);
        infowindow.setPosition(new google.maps.LatLng(lat, lng));
        infowindow.open(map);
    }

    function getLatLng() {

        var address = document.getElementById("googleCityName").value;
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var longaddress = results[0].address_components[0].long_name;
                var latitude = results[0].geometry.location.lat();
                //                         var latitude = results[0].geometry.location.lat();
                //alert(latitute);
                var longitude = results[0].geometry.location.lng();
                // alert(longitude);
                //                         latitude.value = latitute;
                //                         longitute.value = longitude;
                document.getElementById("latitude").value = latitude.toFixed(5);
                document.getElementById("longitude").value = longitude.toFixed(5);
                initialize(results[0].geometry.location.lat(), results[0].geometry.location.lng(), longaddress);
            } else {
//                        alert('Geocode error: ' + status);
            }
        });
    }



</script>
<!--<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>City Master Brattle Food</title>
    </head>-->
    
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.CityMaster" text="CityMaster"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.CityMaster" text="CityMaster"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
    
    <body onload="document.cityMaster.cityName.focus();">
        <form name="cityMaster"  method="POST">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            
            <%@ include file="/content/common/message.jsp" %>
            
            
           <table class="table table-info mb30 table-hover" id="bg" >	
			<thead>
                 <tr>
                    <th  colspan="4" >City Master</th>
                        </thead>
                </tr>
                <tr>
                    <td  colspan="4" align="center" style="display: none" id="nameStatus"><label id="cityNameStatus" style="color: red"></label></td>
                </tr>
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font>City Name</td>
                    <td ><input type="text" name="cityName" id="cityName" class="form-control" style="width:250px;height:40px"  onkeypress="return onKeyPressBlockNumbers(event);" onchange="checkCityName();" autocomplete="off" maxlength="50"></td>
                    <td >&nbsp;&nbsp;<font color="red">*</font>City Zone Name</td>
                    <td ><input type="hidden" name="zoneId" id="zoneId" value="<c:out value="${zoneId}"/>" class="form-control" style="width:250px;height:40px" ><input type="text" name="zoneName" id="zoneName" value="<c:out value="${zoneName}"/>" class="form-control" style="width:250px;height:40px" ></td>
                </tr>
                <tr>

                    <td >&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                    <td >
                        <select  align="center" class="form-control" style="width:250px;height:40px"  name="status" >
                            <option value='Y'>Active</option>
                            <option value='N' id="inActive" style="display: none">In-Active</option>
                        </select>
                    </td>
                    <td>&nbsp;Google City Name</td>
                    <td>
                        <input type="text" name="googleCityName" id="googleCityName" class="form-control" style="width:250px;height:40px"  value="<c:out value="${googleCityName}"/>" onkeypress="return onKeyPressBlockNumbers(event);"   autocomplete="off" maxlength="50" placeholder="Enter a location" style="width:300px;height: 30px;" onchange="getLatLng();">
                                
                    </td>
                  
                </tr>
                <tr >
                    <td >&nbsp;Latitude</td>
                    <td ><input type="text" style="width: 184px;" name="latitude" id="latitude" class="form-control" style="width:250px;height:40px"   value="<c:out value="${latitude}"/>" onfocus="setCityName();" readOnly maxlength="50"></td>
                    <td >&nbsp;Longitude</td>
                    <td ><input type="text" style="width: 184px;" name="longitude" id="longitude" onfocus="setCityName()"  value="<c:out value="${longitude}"/>" readOnly class="form-control" style="width:250px;height:40px" ></td>
                    <td ><input type="hidden" style="width: 184px;" name="cityId" id="cityId" onfocus="setCityName()"  value="<c:out value="${cml.cityId}" />" readOnly class="form-control" style="width:250px;height:40px" ></td>
                </tr>


            </table>
                    </tr>
                <tr>
                    <td>
                        
                        <center>
                            <input type="button" class="btn btn-success"  value="Save" name="Submit" onClick="citysubmit()">


                        </center>
                    </td>
                </tr>
            </table>
            
            

            <h2 align="center">City List</h2>


           <table class="table table-info mb30 table-hover" id="bg" >	
			

                <thead>

                    <tr height="30">
                        <th> S.No </th>
                        <th> City Name  </th>
                        <th> City Zone </th>
                        <th> status </th>
                        <th> Select </th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
            <c:if test = "${cityMasterList != null}">
                        <c:forEach items="${cityMasterList}" var="cml">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>
 
                            <tr>
                                <td    align="left"> <%= sno + 1%> </td>
                                <td    align="left"> <c:out value="${cml.cityName}" /></td>
                                <td    align="left"><c:out value="${cml.zoneName}"/></td>
                                <td    align="left"><c:out value="${cml.status}"/></td>
                                <td  > <input type="checkbox" id="edit<%=sno%>" onclick="setValues( <%= sno%>,'<c:out value="${cml.cityId}" />','<c:out value="${cml.cityName}" />','<c:out value="${cml.zoneName}" />','<c:out value="${cml.zoneId}" />','<c:out value="${cml.status}" />','<c:out value="${cml.googleCityName}" />','<c:out value="${cml.latitude}" />','<c:out value="${cml.longitude}" />');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
            <input type="hidden" name="count" id="count" value="<%=sno%>" />
                </c:if>
            </table>



            
            
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        </form>
    </body>
</div>
            </div>
        </div>
<%@ include file="../common/NewDesign/settings.jsp" %>