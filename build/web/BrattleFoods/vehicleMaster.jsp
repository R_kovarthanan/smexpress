<%-- 
    Document   : vehicleMaster
    Created on : Oct 27, 2013, 5:05:21 PM
    Author     : Throttle
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="ets.domain.mrs.business.MrsTO"%>
<%@page import="ets.domain.vehicle.business.VehicleTO"%>
<%@page import="java.util.Iterator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<script language="javascript" src="/throttle/js/ajaxFunction.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <script type="text/javascript" >

        function CalWarrantyDate(selDate) {

            var seleDate = selDate.split('-');
            var dd = seleDate[0];
            var mm = seleDate[1];
            var yyyy = seleDate[2];

            var today = new Date();
            var dd1 = today.getDate();
            var mm1 = today.getMonth() + 1; //January is 0!
            var yyyy1 = today.getFullYear();

            //                if(dd<10){dd='0'+dd}if(mm<10){mm='0'+mm}today = mm+'-'+dd+'-'+yyyy;alert("today"+today);alert("value"+value);

            var selecedDate = new Date(yyyy, mm, dd);
            var currentDate = new Date(yyyy1, mm1, dd1);
            var Days = Math.floor((selecedDate.getTime() - currentDate.getTime()) / (1000 * 60 * 60 * 24));
            //alert("DAYS--"+Days);
            document.addVehicle.war_period.value = Days;

        }

        function submitPage(value) {

            var owner = document.getElementById("asset").value;
            document.getElementById("asset").value = owner;

            if (validate() == 'fail') {
                return;
            } else if (document.addVehicle.regNo.value == '') {
                alert('Please Enter Vehicle Registration Number');
                document.addVehicle.regNo.focus();
                return;
            } else if (document.addVehicle.regNoCheck.value == 'exists') {
                alert('Vehicle RegNo already Exists');
                return;
            } else if (isSelect(document.addVehicle.usageId, 'Usage Type')) {
                return;
            } else if (numberValidation(document.addVehicle.war_period, 'Warranty Period')) {
                return;
            } else if (isSelect(document.addVehicle.mfrId, 'Manufacturer')) {
                return;
            } else if (isSelect(document.addVehicle.classId, 'Vehicle Class')) {
                return;
            } else if (isSelect(document.addVehicle.modelId, 'Vehicle Model')) {
                return;
            } else if (document.addVehicle.dateOfSale.value == '') {
                alert("Please select Date");
                return;
            } else if (isSelect(document.addVehicle.opId, 'Operation Point')) {
                return;
            } else if (numberValidation(document.addVehicle.kmReading, 'Km Reading')) {
                return;
            } else if (numberValidation(document.addVehicle.hmReading, 'Hm reading')) {
                return;
            } else if (numberValidation(document.addVehicle.dailyKm, 'Daily Km Reading')) {
                return;
            } else if (numberValidation(document.addVehicle.dailyHm, 'Daily Hm reading')) {
                return;
            } else if (textValidation(document.addVehicle.regNo, 'Registration Number')) {
                return;
            } else if (textValidation(document.addVehicle.engineNo, 'Engine Number')) {
                return;
            }
            else if (textValidation(document.addVehicle.chassisNo, 'Chassis Number')) {
                return;
            }
            else if (textValidation(document.addVehicle.nextFCDate, 'Next FC Date')) {
                return;
            } else if (textValidation(document.addVehicle.seatCapacity, 'seatCapacity')) {
                return;

            } else if (textValidation(document.addVehicle.warrantyDate, 'WarrantyDate')) {
                return;
            }
            if (value == "save") {
                document.addVehicle.nextFCDate.value = dateFormat(document.addVehicle.nextFCDate);
                document.addVehicle.dateOfSale.value = dateFormat(document.addVehicle.dateOfSale);
                document.addVehicle.action = '/throttle/addVehicle.do';
                document.addVehicle.submit();
            }
        }

        var httpRequest;
        function getVehicleDetails() {
            if (document.addVehicle.regNo.value != '') {
                var url = '/throttle/checkVehicleExists.do?regno=' + document.addVehicle.regNo.value;

                if (window.ActiveXObject)
                {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest)
                {
                    httpRequest = new XMLHttpRequest();
                }
                httpRequest.open("POST", url, true);
                httpRequest.onreadystatechange = function() {
                    go1();
                };
                httpRequest.send(null);
            }
        }


        function go1() {
            if (httpRequest.readyState == 4) {
                if (httpRequest.status == 200) {
                    var response = httpRequest.responseText;
                    var temp = response.split('-');
                    if (response != "") {
                        alert('Vehicle Already Exists');
                        document.getElementById("Status").innerHTML = httpRequest.responseText.valueOf() + " Already Exists";
                        document.addVehicle.regNo.focus();
                        document.addVehicle.regNo.select();
                        document.addVehicle.regNoCheck.value = 'exists';
                    } else
                    {
                        document.addVehicle.regNoCheck.value = 'Notexists';
                        document.getElementById("Status").innerHTML = "";
                    }
                }
            }
        }


        var rowCount = 1;
        var sno = 0;
        var httpRequest;
        var httpReq;
        var styl = "";
        <%   ArrayList tyreItems = (ArrayList) request.getAttribute("tyreItemList");

                    ArrayList positionList = (ArrayList) request.getAttribute("positionList");
                    VehicleTO veh = new VehicleTO();
                    MrsTO mrs = new MrsTO();
        %>

        function addRow() {
            if (parseInt(rowCount) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            sno++;
            var tab = document.getElementById("addTyres");
            var newrow = tab.insertRow(rowCount);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' > " + sno + "</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;


            // TyreIds
            var cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='25' ><font color='red'>*</font>" +
                    "<select name='itemIds' class='textbox'    > <option value='0'>-Select-</option>" +
        <%

                    Iterator itr = tyreItems.iterator();

                    while (itr.hasNext()) {
                        veh = new VehicleTO();
                        veh = (VehicleTO) itr.next();
        %>
            "<option value=<%= veh.getItemId()%> >  <%= veh.getItemName()%> </option>" +
        <%  }%>

            + "</select> </td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;


            // Positions
            cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='25' >" +
                    "<font color='red'>*</font><select name='positionIds'   class='textbox' > <option value='0'>-Select-</option>" +
        <% ;
                    itr = positionList.iterator();
                    while (itr.hasNext()) {
                        mrs = new MrsTO();
                        mrs = (MrsTO) itr.next();
        %>
            "<option value=<%= mrs.getPosId()%> >  <%= mrs.getPosName()%> </option>" +
        <% }

        %>

            + "</select> </td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;



            cell = newrow.insertCell(3);
            var cell1 = "<td class='text1' height='30' ><font color='red'>*</font><input type='text' name='tyreIds' onChange='checkTyreId(" + rowCount + ")' class='textbox' >";

            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(4);
            var cell2 = "<td class='text1' height='30' ><font color='red'>*</font><input type='text' name='tyreDate' class='datepicker' >";

            cell.setAttribute("className", "text1");
            cell.innerHTML = cell2;

            rowCount++;

            $(document).ready(function() {

                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });

            });

        }



        var httpReq;
        var temp = "";
        function ajaxData()
        {
            //alert(document.addVehicle.mfrId.value);
            var url = "/throttle/getModels1.do?mfrId=" + document.addVehicle.mfrId.value;
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processAjax();
            };
            httpReq.send(null);
        }


        function processAjax()
        {
            if (httpReq.readyState == 4)
            {
                if (httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    setOptions(temp, document.addVehicle.modelId);
                }
                else
                {
                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                }
            }
        }

        function validate()
        {
            var tyreIds = document.getElementsByName("tyreIds");
            var tyreDate = document.getElementsByName("tyreDate");
            var positionIds = document.getElementsByName("positionIds");
            var itemIds = document.getElementsByName("itemIds");
            var tyreExists = document.getElementsByName("tyreExists");

            var cntr = 0;
            for (var i = 0; i < tyreIds.length; i++) {
                for (var j = 0; j < tyreIds.length; j++) {
                    if ((tyreIds[i].value == tyreIds[j].value) && (tyreIds[i].value == tyreIds[j].value) && (i != j) && (tyreIds[i].value != '')) {
                        cntr++;
                    }
                }
                if (parseInt(cntr) > 0) {
                    alert("Same Tyre Number should not exists twice");
                    return "fail";
                    break;
                }
            }

            for (var i = 0; i < positionIds.length; i++) {
                for (var j = 0; j < positionIds.length; j++) {
                    if ((positionIds[i].value == positionIds[j].value) && (positionIds[i].value == positionIds[j].value) && (i != j) && (positionIds[i].value != '0')) {
                        cntr++;
                    }
                }
                if (parseInt(cntr) > 0) {
                    alert("Tyre Positions should not be repeated");
                    return "fail";
                    break;
                }
            }

            for (var i = 0; i < tyreIds.length; i++) {

                if (itemIds[i].value != '0') {
                    if (positionIds[i].value == '0') {
                        alert('Please Select Position');
                        positionIds[i].focus();
                        return "fail";
                    } else if (tyreIds[i].value == '') {
                        alert('Please Enter Tyre No');
                        tyreIds[i].focus();
                        return "fail";
                    } else if (tyreExists[i].value == 'exists') {
                        alert('Tyre number ' + tyreIds[i].value + ' is already fitted to another vehicle');
                        tyreIds[i].focus();
                        tyreIds[i].select();
                        return "fail";
                    }
                }
            }

            return "pass";
        }




        var httpRequest;
        function checkTyreId(val)
        {
            val = val - 1;
            var tyreNo = document.getElementsByName("tyreIds");
            var tyreExists = document.getElementsByName("tyreExists");
            if (tyreNo[val].value != '') {
                var url = '/throttle/checkVehicleTyreNo.do?tyreNo=' + tyreNo[val].value;
                if (window.ActiveXObject)
                {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }
                else if (window.XMLHttpRequest)
                {
                    httpRequest = new XMLHttpRequest();
                }
                httpRequest.open("GET", url, true);
                httpRequest.onreadystatechange = function() {
                    processRequest(tyreNo[val].value, val);
                };
                httpRequest.send(null);
            }
        }


        function processRequest(tyreNo, index)
        {
            if (httpRequest.readyState == 4)
            {
                if (httpRequest.status == 200)
                {
                    var tyreExists = document.getElementsByName("tyreExists");
                    if (httpRequest.responseText.valueOf() != "") {
                        document.getElementById("userNameStatus").innerHTML = "Tyre No " + tyreNo + " Already Exists in " + httpRequest.responseText.valueOf();
                        tyreExists[index].value = 'exists';
                    } else {
                        document.getElementById("userNameStatus").innerHTML = "";
                        tyreExists[index].value = 'notExists';
                    }
                }
                else
                {
                    alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                }
            }
        }

        function openAttachedInfo() {
            document.getElementById("asset").value = "2";
            document.getElementById("attachInfo").style.display = "block";
        }
        function closeAttachedInfo() {
            document.getElementById("asset").value = "1";
            document.getElementById("attachInfo").style.display = "none";
        }
    </script>
    <body onload="addRow()" >
        <form name="addVehicle"  method="post" >
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="700" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                            </td></tr></table>
                    <!-- pointer table -->

                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" id="bg" class="border">
                <tr align="center">
                    <td colspan="4" align="center" class="contenthead" height="30"><div class="contenthead">Add New Vehicle</div></td>
                </tr>
                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Vehicle Number</td>
                    <td class="text1" height="30"><input maxlength='13'  name="regNo" type="text" class="textbox" value="" ></td>
                    <td class="text1" height="30"><font color="red">*</font>Registration Date</td>
                    <td class="text1" height="30"><input name="dateOfSale" type="text" class="datepicker" value="" size="20"><!--<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.addVehicle.dateOfSale,'dd-mm-yyyy',this)"/> -->
                    </td>
                </tr>
                <tr>
                    <td class="text2" height="30"><font color="red">*</font>MFRs</td>
                    <td class="text2"  ><select class="textbox" name="mfrId" onChange="ajaxData();"  style="width:125px;">
                            <option value="0">---Select---</option>
                            <c:if test = "${MfrList != null}" >
                                <c:forEach items="${MfrList}" var="Dept">
                                    <option value='<c:out value="${Dept.mfrId}" />'><c:out value="${Dept.mfrName}" /></option>
                                </c:forEach >
                            </c:if>
                        </select></td>

                    <td class="text2" height="30"><font color="red">*</font>Model </td>
                    <td class="text2" height="30">
                        <select class="textbox" name="modelId"  style="width:125px;">
                            <option value="0">---Select---</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Engine No</td>
                    <td class="text1" height="30"><input maxlength='20' name="engineNo" type="text" class="textbox" value="" size="20"></td>
                    <td class="text1" height="30"><font color="red">*</font>Chassis No</td>
                    <td class="text1" height="30"><input maxlength='20'  type="text" name="chassisNo" size="20" class="textbox" value=""> </td>
                </tr>
                <tr>
                    <td class="text2" height="30"><font color="red">*</font>No Of Axles</td>
                    <td class="text2" height="30"><input maxlength='20' name="axles" type="text" class="textbox" value="" size="20"></td>
                    <td class="text2" height="30">Tonnage (MT)</td>
                    <td class="text2" height="30"><input name="seatCapacity" type="text" class="textbox" value="0" size="20"></td>
                </tr>
                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Warranty Expiry Date</td>
                    <td class="text1" height="30"><input name="warrantyDate" type="text" class="datepicker" value="" size="20" >
                    <td class="text1" height="30">Warranty (days)</td>
                    <td class="text1" height="30"><input name="warPeriod" type="text"  class="textbox" value="" size="20" readonly></td>
                </tr>
                <tr>
                    <td class="text2" height="30">Vehicle Cost As On Date</td>
                    <td class="text2"  ><input type="text" name="vehicleCost" class="textbox" /></td>
                    <td class="text2" height="30">Depreciation(%)</td>
                    <td class="text2"  ><input type="text" class="textbox" /></td>
                </tr>
                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Cold Storage</td>
                    <td class="text1"  ><select class="textbox" name="classId" style="width:125px;">
                            <option value="0">---Select---</option>
                            <option value="Y">Yes</option>
                            <option value="N">No</option>
                        </select></td>
                    <td class="text1" height="30"> <font color="red">*</font>KM Reading</td>
                    <td class="text1"  ><input maxlength='20'  name="kmReading" type="text" class="textbox" value="" size="20">
                </tr>

                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Daily Run KM</td>
                    <td class="text2" height="30"> <input name="dailyKm" type="text" class="textbox" value="" size="20"> </td>
                    <td class="text2" height="30"> <font color="red">*</font>Daily Hour Meter</td>
                    <td class="text2"  ><input maxlength='20'  name="hourMeter" type="text" class="textbox" value="" size="20">
                </tr>
                <tr>
                    <td class="text1" height="30"> <font color="red">*</font>Vehicle Color</td>
                    <td class="text1"  ><input type="text"  name="vehicleColor" id="vehicleColor" class="textbox" value=""></td>
                    <td class="text1" height="30"><font color="red">*</font>Vehicle Usage</td>
                    <td class="text1"  ><select class="textbox" name="usageId"  style="width:125px;">
                            <option value="0">---Select---</option>
                            <option value="1">Short Trip</option>
                            <option value="2">Long Trip</option>
                        </select></td>
                </tr>
                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Fleet Center</td>
                    <td class="text2"  >
                        <select class="textbox" name="opId"  style="width:125px;">
                            <option value="0">---Select---</option>
                            <c:if test = "${OperationPointList != null}" >
                                <c:forEach items="${OperationPointList}" var="Dept">
                                    <option value='<c:out value="${Dept.opId}" />'> <c:out value="${Dept.opName}" /> </option>
                                </c:forEach >
                            </c:if>
                        </select></td>
                    <td class="text2" colspan="2">&nbsp;</td>
                </tr>        

                <tr>
                    <td class="text1" height="30"> <font color="red">*</font>GPS Tracking System</td>
                    <td class="text1"  >
                        <select class="textbox" name="gpsSystem" id="gpsSystem" onchange="openGpsInfo(this.value)" style="width:125px;">
                            <option value="0">---Select---</option>
                            <option value='YES'>YES</option>
                            <option value='NO'>NO</option>
                        </select>
                        <script type="text/javascript">
                            function openGpsInfo(val) {
                                if (val == 'YES') {
                                    document.getElementById("trckingId").style.display = "block";
                                } else {
                                    document.getElementById("trckingId").style.display = "none";
                                }
                            }
                        </script>
                    </td>
                    <td class="text1" height="30"> Vehicle Ownership</td>
                    <td class="text1" height="30">
                        <input type="radio" name="vAsset" id="own" value="Own" checked onclick="closeAttachedInfo();"> Own
                        <input type="radio" name="vAsset" id="attach" value="Attach" onclick="openAttachedInfo();"> Attach
                        <input type="hidden" name="asset" id="asset" class="textbox" value="1">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="trckingId" style="display: none;">
                            <table width="100%" cellpadding="0" cellspacing="2">
                                <tr>
                                    <td class="text2" height="30"><font color="red">*</font>GPS System Id</td>
                                    <td class="text2" align="center">
                                        <select class="textbox" name="gpsSystemId"  style="width:125px;">
                                            <option value="0" checked>--Select--</option>
                                            <option value="1" >0001</option>
                                            <option value="2" >0002</option>
                                            <option value="3" >0003</option>
                                        </select>
                                    </td>
                                    <td class="text2" height="30">&nbsp;</td>
                                    <td class="text2">&nbsp;</td>

                                </tr>
                            </table>
                        </div>
                    </td>
                    <td colspan="2">
                        <div id="attachInfo" style="display: none;">
                            <table width="100%" cellpadding="0" cellspacing="2">
                                <tr>
                                    <td class="text2" height="30">Vendor Name</td>
                                    <td class="text2" align="center">
                                        <select class="textbox" name="leasingCustId"  style="width:125px;">
                                            <option value="0" checked>--Select--</option>
                                            <option value="1" >Vendor 1</option>
                                            <option value="2" >Vendor 2</option>
                                            <option value="3" >Vendor 3</option>
                                        </select>
                                    </td>
                                    <td class="text2" height="30">&nbsp;</td>
                                    <td class="text2">&nbsp;</td>

                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <script type="text/javascript">
                    function openAttachedInfo() {
                        document.getElementById("asset").value = "2";
                        document.getElementById("attachInfo").style.display = "block";
                    }
                    function closeAttachedInfo() {
                        document.getElementById("asset").value = "1";
                        document.getElementById("attachInfo").style.display = "none";
                    }
                </script>
            </table>

            <table border="0" class="border" align="center" width="700" cellpadding="0" cellspacing="0" id="addTyres">
                <tr >
                    <td width="20" class="contenthead" align="center" height="30" ><div class="contenthead">Sno</div></td>
                    <td class="contenthead" height="30" ><div class="contenthead">Item</div></td>
                    <td class="contenthead" height="30" ><div class="contenthead">Position</div> </td>
                    <td class="contenthead" height="30" ><div class="contenthead">Tyre Number</div></td>
                    <td class="contenthead" height="30" ><div class="contenthead">Date</div></td>
                </tr>
            </table>
            <br>
            <table border="0" class="border" align="center" width="700" cellpadding="0" cellspacing="0" id="bg">
                <tr>
                    <td>
                    <td class="text2" height="30">Remarks</td>
                    <td class="text2" height="30"><textarea class="textbox" name="description"></textarea></td>
                </tr>
                <tr>
                    <td>
                    <td class="text2" height="30">Is Free Services Done</td>
                    <td class="text2" height="30"><input type="checkbox" value="Y" name="selectedindex"></td>
                </tr>
            </table>
            <br>
            <center>
                <input type="button" class="button" value="Add" name="save" onClick="submitPage(this.name)">
                &emsp;<input type="reset" class="button" value="Clear">
                <input type="button" class="button" value="Add Row" name="save" onClick="addRow()">
            </center>

        </form>
    </body>
</html>

