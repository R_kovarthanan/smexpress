<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

        <%@ include file="/content/common/NewDesign/header.jsp" %>
	<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $( "#datepicker" ).datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $( ".datepicker" ).datepicker({

            /*altField: "#alternate",
                        altFormat: "DD, d MM, yy"*/
            changeMonth: true,changeYear: true
        });

    });
</script>

    </head>
    <script language="javascript">
//        function submitPage(){        
//            document.approve.action = '/throttle/saveAdvanceApproval.do';
//            document.approve.submit();
//        }
        function submitManualPage(){
            document.approve.action = '/throttle/saveManualAdvanceApproval.do';
            document.approve.submit();
        }
        function setFocus(){
            document.approve.approveamt.focus();
        }
    </script>


                       <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Advance Approval" text="Advance Approval"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Operations" text="Operations"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Advance Approval" text="Advance Approval"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">
    <body onload="setFocus();">
        <form name="approve"  method="post" >
            <%
            request.setAttribute("menuPath","Advance >> Approval");
            String tripid = request.getParameter("tripId");
            String type = request.getParameter("batchType");
            String tripAdvaceId = request.getParameter("tripAdvaceId");
            %>
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <c:if test = "${viewAdviceDetails != null}" >
           <table class="table table-info mb30 table-hover" id="bg" >

              
                <thead>
                <tr align="center">
                    <th colspan="2" class="contenthead" >
                        Advance Approval</th>
                </tr>
                 <thead>
                <c:forEach items="${viewAdviceDetails}" var="fd">
                <tr>
                    <td  height="30" >Customer Name</td>
                    <td  height="30"><c:out value="${fd.customerName}"/></td>
                </tr>
                <tr>
                    <td  height="30" >Cnote Name</td>
                    <td  height="30"><c:out value="${fd.cnoteName}"/></td>
                </tr>
                <tr>
                    <td  height="30">Vehicle Type</td>
                    <td  height="30"><c:out value="${fd.vehicleTypeName}"/></td>
                </tr>
                <tr>
                    <td  height="30">Vehicle No</td>
                    <td  height="30"><c:out value="${fd.regNo}"/></td>
                </tr>
                
                <tr>
                    <td  height="30">Vendor Name</td>
                    <td  height="30"><c:out value="${fd.vendorName}"/></td>
                </tr>
                <tr>
                    <td  height="30">Route Name</td>
                    <td  height="30"><c:out value="${fd.routeName}"/></td>
                </tr>
       
                <tr>
                    <td  height="30">Planned Date</td>
                    <td  height="30"><c:out value="${fd.planneddate}"/></td>
                </tr>
                <tr>
                    <td  height="30">Actual Advance Paid</td>
                    <td  height="30"><c:out value="${fd.actualadvancepaid}"/></td>
                </tr>

                <tr>
                    <td  height="30">Estimated Expense</td>
                    <td  height="30"><c:out value="${fd.estimatedexpense}"/></td>
                </tr>
                
                <input type="hidden" name="eFSId" value="<c:out value="${fd.vendoreFSId}"/>"/>
                <input type="hidden" name="tripid" value="<%=tripid%>"/>
                <input type="hidden" name="batchType" value="<%=type%>"/>
                <input type="hidden" name="advid" value="<%=tripAdvaceId%>"/>
                <input type="hidden" name="advicedate" value="<c:out value="${fd.advicedate}"/>"/>
                <input type="hidden" name="cnote" value="<c:out value="${fd.cnoteName}"/>"/>
                <input type="hidden" name="vegno" value="<c:out value="${fd.regNo}"/>"/>
                <input type="hidden" name="routename" value="<c:out value="${fd.routeName}"/>"/>
                <input type="hidden" name="tripCode" value="<c:out value="${fd.tripcode}"/>"/>
                <input type="hidden" name="driver" value="<c:out value="${fd.driverName}"/>"/>
                <input type="hidden" name="planneddate" value="<c:out value="${fd.planneddate}"/>"/>
                <input type="hidden" name="actualadvancepaid" value="<c:out value="${fd.actualadvancepaid}"/>"/>
                <input type="hidden" name="estiexpense" value="<c:out value="${fd.estimatedexpense}"/>"/>
                <input type="hidden" name="requeston" value="<c:out value="${fd.requeston}"/>"/>
                <input type="hidden" name="advancerequestamt" value="<c:out value="${fd.requestedadvance}"/>"/>
                <input type="hidden" name="tripStatusId" value="<c:out value="${fd.tripStatusId}"/>"/>
                <input type="hidden" name="wfuDay" value="<c:out value="${fd.wfuDay}"/>"/>
                <input type="hidden" name="rcmDeviationStr" value="<c:out value="${fd.deviateAmount}"/>"/>
                <input type="hidden" name="consignmentOrderId" value="<c:out value="${fd.consignmentOrderId}"/>"/>
                
                <tr>
                    <td  height="30">Request On</td>
                    <td  height="30"><c:out value="${fd.requeston}"/></td>
                </tr>

                <tr>
                    <td  height="30"><font color="red">*</font>Req.Advance Amt.</td>
                    <td  height="30"><c:out value="${fd.requestedadvance}"/></td>
                </tr>
                <tr>
                    <td  height="30"><font color="red">*</font>Status</td>
                    <td  height="30"><select name="approvestatus" class="form-control" style="width:240px;height:40px">
                            <!--<option value="" >-Select Any One-</option>-->
                            <option value="1">Approved</option>
                            <option value="2">Rejected</option>
                </select></td>
                </tr>


                <tr>
                    <td  height="30"><font color="red">*</font>Approve Remarks</td>
                    <td  height="30"><textarea class="form-control" style="width:240px;height:40px" name="approveremarks"></textarea></td>
                </tr>
</c:forEach>
            </table>
                </c:if>
            <br>
            <center>
                
                <%if(type.equalsIgnoreCase("m")){%>
                <input type="button" id="mySubmit" value="Submit"  class="btn btn-success" onClick="submitManualPage();">
                <%}else{%>
                <!--<input type="button" id="mySubmit" value="Submit"  class="btn btn-success" onClick="submitPage();">-->
                <%}%>
                &emsp;<input type="reset"  class="btn btn-success" value="Clear">
            </center>



        </form>
    </body>
</div>
	</div>
        </div>
        <%@ include file="/content/common/NewDesign/settings.jsp" %>
