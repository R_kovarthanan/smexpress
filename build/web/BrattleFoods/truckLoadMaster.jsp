<%-- 
    Document   : truckLoadMaster
    Created on : Oct 27, 2013, 5:04:09 PM
    Author     : Throttle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <script type="text/javascript">
        function submitPage(){
            document.truckLoad.action = '/throttle/BrattleFoods/truckLoadMaster.jsp';
            document.truckLoad.submit();
        }
    </script>
    <body>
         <% String menuPath = "Truck  >>  Add Truck Load Detail";
        request.setAttribute("menuPath", menuPath);
        %>
        <form name="truckLoad"  method="post" >
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                    <!-- pointer table -->
                    <table width="700" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                            </td></tr></table>
                    <!-- pointer table -->

                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="400" id="bg" class="border">
                <tr align="center">
                    <td colspan="2" align="center" class="contenthead" height="30"><div class="contenthead">Truck Load Master</div></td>
                </tr>
                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Load Type</td>
                    <td class="text1" height="30">
                        <select name="loadType" id="loadType">
                            <option value="0">--select--</option>
                            <option value="1">FTL</option>
                            <option value="2">LTL</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Capacity</td>
                    <td class="text2" height="30"><input type="text" name="capacity" class="textbox"></td>
                </tr>
                <tr>
                    <td class="text1" height="30">Description</td>
                    <td class="text1" height="30"><textarea id="desc"></textarea></td>
                </tr>
                <tr>
                    <td class="text2" height="30" colspan="2" align="center"><input align="center" type="button" class="button" value="Save" onclick="submitPage()"></td>
                </tr>
            </table>

            <br>
            <br>
            <br>
            <br>
            <table width="90%" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>Load Type</h3></th>
                        <th><h3>Capacity</h3></th>
                        <th><h3>Description</h3></th>
                        <th><h3>Edit</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <tr height="30">
                        <td align="left" class="text2">1</td>
                        <td align="left" class="text2">FTL</td>
                        <td align="left" class="text2">10MT</td>
                        <td align="left" class="text2">10MT</td>
                        <td align="left" class="text2"><input type="checkbox" name="edit" id="edit" /></td>
                    </tr>
                    <tr height="30">
                        <td align="left" class="text2">2</td>
                        <td align="left" class="text2">LTL</td>
                        <td align="left" class="text2">5MT</td>
                        <td align="left" class="text2">5MT</td>
                        <td align="left" class="text2"><input type="checkbox" name="edit" id="edit" /></td>
                    </tr>
                </tbody>
            </table>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table",1);
            </script>
        </form>
    </body>
</html>
