<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $( "#datepicker" ).datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $( ".datepicker" ).datepicker({

            /*altField: "#alternate",
                        altFormat: "DD, d MM, yy"*/
            changeMonth: true,changeYear: true
        });

    });
</script>

    </head>
    <script language="javascript">
        function submitPage(){
            if(textValidation(document.approve.paidamt,'Paid Amount')){
                return;
            }       

            document.approve.action = '/throttle/saveTripAmount.do';
            document.approve.submit();
        }
        function setFocus(){
            document.approve.approveamt.focus();
        }
    </script>


    <body onload="setFocus();">
        <form name="approve"  method="post" >
            <%
            request.setAttribute("menuPath","Trip >> Payment");
            String tripid = request.getParameter("tripid");
            String tripday = request.getParameter("tripday");            
            String tobepaidtoday = request.getParameter("tobepaidtoday");
            %>
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="400" id="bg" class="border">
                <tr align="center">
                    <td colspan="2" align="center" class="contenthead" height="30">
                        <div class="contenthead">Trip Payment</div></td>
                </tr>

                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Trip Day</td>
                    <td class="text2" height="30"><input name="tripday" type="text" class="textbox" value="<%=tripday%>" readonly></td>
                </tr>
                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Estimated Amount</td>
                    <td class="text2" height="30"><input name="tobepaidtoday" type="text" class="textbox" value="<%=tobepaidtoday%>" readonly></td>
                </tr>
                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Paid Amt.</td>
                    <td class="text2" height="30"><input name="paidamt" type="text" class="textbox" value=""></td>
                </tr>

                <input type="hidden" name="paidstatus" value="1"/>
                <input type="hidden" name="tripid" value="<%=tripid%>"/>
                
          
            </table>
            <br>
            <center>
                <input type="button" value="Add" class="button" onClick="submitPage();">
                &emsp;<input type="reset" class="button" value="Clear">
            </center>
        </form>
    </body>
</html>
